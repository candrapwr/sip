/** START OF UPLOADER PROFILE PICTURE */
$(function () {

    $("#wizard").on('dragover dragenter', '#upload-drop-zone', function () {
        $(this).addClass("dragover");
    })

    $("#wizard").on('dragleave dragend drop', '#upload-drop-zone', function () {
        $('#upload-drop-zone').removeClass("dragover");
    })

    $("#wizard").on('drop', '#upload-drop-zone', function () {
        $('#upload-drop-zone').removeClass("dragover");
    })

    $("#wizard").on('change', '#upload-input', function (e) {
        var files = e.target.files;
        handleFiles(files);
    })

    function handleFiles(files) {
        var imageType = /^image\//;

        if (!imageType.test(files[0].type)) {
            Swal.fire(
                'Format tidak didukung?',
                'Pastikan gambar berformat jpg atau png?',
                'error'
            )
            return;
        }

        var reader = new FileReader();
        reader.onload = function () {
            var img = new Image();
            img.onload = function () {
                $("#file-preview").attr("src", img.src);
                $(".file-view").show();
            };

            img.src = reader.result;
        };
        reader.readAsDataURL(files[0]);
    }

})

$(function () {
    
    $("#wizard").on('dragover dragenter', '#upload-drop-zone-2', function () {
        $(this).addClass("dragover");
    })

    $("#wizard").on('dragleave dragend drop', '#upload-drop-zone-2', function () {
        $('#upload-drop-zone-2').removeClass("dragover");
    })

    $("#wizard").on('drop', '#upload-drop-zone-2', function () {
        $('#upload-drop-zone-2').removeClass("dragover");
    })

    $("#wizard").on('change', '#upload-input-2', function (e) {
        var files = e.target.files;
        handleFiles2(files);
    })

    function handleFiles2(files) {
        var imageType = /^image\//;

        if (!imageType.test(files[0].type)) {
            Swal.fire(
                'Format tidak didukung?',
                'Pastikan gambar berformat jpg atau png?',
                'error'
            )
            return;
        }

        var reader = new FileReader();
        reader.onload = function () {
            var img = new Image();
            img.onload = function () {
                $("#file-preview-2").attr("src", img.src);
                $(".file-view").show();
            };

            img.src = reader.result;
        };
        reader.readAsDataURL(files[0]);
    }

})

/** END OF UPLOADER PROFILE PICTURE */