<?php
class Mylib
{

    function change_date($tgldb, $lang)
    {

        if ($lang == "en") {
            $ori = strtotime($tgldb);
            $newformat = date('F d, Y', $ori);
            return $newformat;
        } else {
            $bul = array("", "Januari", "Feburari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $ori = strtotime($tgldb);
            $bul_format = date("n");
            $newformat = date('d, Y', $ori);
            return $bul[$bul_format] . ' ' . $newformat;
        }
    }

    function change_datetime($tgldb, $lang)
    {

        if ($lang == "en") {
            $ori = strtotime($tgldb);
            $newformat = date('d F Y', $ori);
            return $newformat;
        } else {
            $bul = array("", "Januari", "Feburari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $ori = strtotime($tgldb);
            $bul_format = date("n");
            $newformat = date('Y', $ori);
            return date('d', $ori) . " " . $bul[$bul_format] . ' ' . $newformat;
        }
    }

    function pagination($url, $page, $total_row)
    {
        $CI = &get_instance();
        $CI->load->library('pagination');

        // config
        $config['base_url'] = $url;
        $config['total_rows'] = $total_row;
        $config['per_page'] = $page;
        $config['num_links'] = 4;

        // styling
        $config['full_tag_open'] = '<nav class="pb-5" aria-label="Page navigation example"><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav>';

        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li">';

        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li">';

        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li">';

        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li">';

        $config['cur_tag_open'] = '<li class="page-item active" aria-current="page"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li">';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li">';
        $config['use_page_numbers'] = TRUE;
        $config['enable_query_strings'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';

        $config['attributes'] = array('class' => 'page-link');

        $CI->pagination->initialize($config);

        return $CI->pagination->create_links();
    }

    function get_url($url)
    {
        $CI = &get_instance();
        if (strpos($url, 'http') !== false || strpos($url, 'https') != false) {
            return $url;
        } else {
            return base_url() . $CI->langlib->city() . "/" . $url;
        }
    }

    function slugify($text, $id)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text . "-" . $id;
    }
}
