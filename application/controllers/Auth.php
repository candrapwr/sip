<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('secure');
    }


    //DO NOT TOUCH OR EXECUTE
    public function grabAlokasiDMOSimirah()
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '2048M');
        $api = array(
            'endpoint'      => 'getSummarySimirah',
            'method'        => 'POST',
            'controller'    => 'migor',
            'post_field'    => array(
                'tgl_awal' =>  '2022-06-01',
                'tgl_akhir' => date('Y-m-d', strtotime('-1 days')),
                'alokasi' =>  'true',
                'email' => 'System',
                'limit' => 1,
                'offset' => 0
            )
        );
        $response = $this->serviceAPI($api);
        echo date('Y-m-d H:i:s') . ' - ' . json_encode($response) . PHP_EOL;
    }

    //DO NOT TOUCH OR EXECUTE
    function kirimAlokasi()
    {
        ini_set('max_execution_time', '1000');
        ini_set('memory_limit', '2048M');
        $alokasi_id = $this->input->post('alokasi_id');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $kirim_output = array('kode' => 400);
        // if($tgl_awal == '' || $tgl_akhir == ''){
        //     echo json_encode($kirim_output); die();
        //     $alokasi_id = 48;
        // $tgl_awal = '2022-06-01';
        // $tgl_akhir = '2022-09-06';
        // }


        $api_cekstatus = array(
            'endpoint'      => 'getPeriodeAlokasiDMO',
            'method'        => 'POST',
            'controller'    => 'migor',
            'post_field'    => array(
                'periode_awal' =>   date('Y-m-d', strtotime('-1 days')),
                'periode_akhir' =>  date('Y-m-d', strtotime('-1 days')),
                'limit' => 1,
                'offset' => 0
            )
        );

        $cek_status = $this->serviceAPI($api_cekstatus);

        if ($cek_status['kode'] == 200) {
            if ($cek_status['data'][0]['status_kirim'] == 1) {
                echo date('Y-m-d H:i:s') . ' - ' . json_encode(array('kode' => 400, 'keterangan' => 'Data sudah dikirim sebelumnya'));
                die();
            }
        }


        $api = array(
            'endpoint'      => 'getSummaryAlokasiSimirah',
            'method'        => 'POST',
            'controller'    => 'migor',
            'post_field'    => array(
                'tgl_awal' =>  '2022-06-01',
                'tgl_akhir' =>  date('Y-m-d', strtotime('-1 days')),
                'series' =>  2,
                'value_only' =>  'true',
                'alokasi_kirim' =>  'true',
                'limit' => 10000,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);
        $probably_zero = 0;
        //handling tidak ada perubahan
        if ($response['kode'] == 404) {
            $api = array(
                'endpoint'      => 'getSummaryAlokasiSimirah',
                'method'        => 'POST',
                'controller'    => 'migor',
                'post_field'    => array(
                    'tgl_awal' =>  '2022-06-01',
                    'tgl_akhir' =>  date('Y-m-d', strtotime('-1 days')),
                    'series' =>  2,
                    'alokasi_kirim' =>  'true',
                    'limit' => 10000,
                    'offset' => 0
                )
            );

            $response = $this->serviceAPI($api);
            $probably_zero = 1;
            if ($response['kode'] == 200) {
                //check if today == yesterday
                $grab_jml = array_column($response['data'], 'jml');
                $numericValues = array_map('floatval', $grab_jml);
                $sum = array_sum($numericValues);
                if ($sum == 0) {
                    $probably_zero = 2;
                }
            }
        }


        if ($response['kode'] == 200) {
            $data_alokasi = array();
            $data_payload_group = array();
            $periode_alokasi_id = '';

            foreach ($response['data'] as $k => $v) {
                if ($periode_alokasi_id == '') {
                    $periode_alokasi_id = $v['alokasi_id'];
                }
                $data_alokasi[] = array(
                    'kdIjin' => $v['kd_izin'],
                    'nib' => $v['nib'],
                    'namaPerusahaan' => $v['nama'],
                    'jumlahDmo' => $v['jml'],
                    'kdSatuan' => $v['satuan'],
                    'keterangan' => ''
                );
                $data_payload_group[$v['type']][] =  array(
                    'kdIjin' => $v['kd_izin'],
                    'nib' => $v['nib'],
                    'namaPerusahaan' => $v['nama'],
                    'jumlahDmo' => $v['jml'],
                    'kdSatuan' => $v['satuan'],
                    'keterangan' => ''
                );
            }

            $parse_id = explode(',', $periode_alokasi_id);
            $alokasi_id = $parse_id[1];

            if (is_numeric($alokasi_id)) {
                $final_data_alokasi = array(
                    'noKeputusan' => $alokasi_id . '/SISP' . '/' . date('m') . '/' . date('Y'),
                    'tglKeputusan' => date('Y-m-d'),
                    'dataDmo' => ($probably_zero == 0) ? $data_alokasi : []
                );


                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://ws.kemendag.go.id/insw/sendAlokasiDMOMigor',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($final_data_alokasi),
                    CURLOPT_HTTPHEADER => array(
                        'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
                        'Content-Type: application/json'
                    ),
                ));

                $response_output = curl_exec($curl);

                curl_close($curl);

                $response_output = json_decode($response_output);
                if ($response_output->code == 01) {
                    $api = array(
                        'endpoint'      => 'setAlokasiKirim',
                        'method'        => 'POST',
                        'controller'    => 'migor',
                        'post_field'    => array(
                            'alokasi_id' =>  $alokasi_id,
                            'status_kirim' =>  1,
                            'request_body' =>  json_encode($final_data_alokasi),
                            'sendon' =>  date('Y-m-d H:i:s'),
                            'request_output' =>  json_encode($response_output),
                            'payload' =>  json_encode($data_payload_group),
                            'email' => 'System',
                            'periode_alokasi_id' => $periode_alokasi_id,
                        )
                    );

                    $response = $this->serviceAPI($api);
                    $kirim_output = array('kode' => 200);
                } else {
                    $api = array(
                        'endpoint'      => 'setAlokasiKirim',
                        'method'        => 'POST',
                        'controller'    => 'migor',
                        'post_field'    => array(
                            'alokasi_id' =>  $alokasi_id,
                            'status_kirim' =>  99,
                            'request_body' =>  json_encode($final_data_alokasi),
                            'sendon' =>  date('Y-m-d H:i:s'),
                            'request_output' =>  json_encode($response_output),
                            'payload' =>  json_encode($data_payload_group),
                            'email' => 'System',
                            'periode_alokasi_id' => $periode_alokasi_id,
                        )
                    );

                    $response = $this->serviceAPI($api);
                }
            }
        }

        echo date('Y-m-d H:i:s') . ' - ' . json_encode($kirim_output) . PHP_EOL;
    }

    public function loginForm()
    {
        if ($this->session->userdata('token')) {
            (strtolower($this->session->userdata('role')) == 'administrator') ? redirect('administrator') : redirect('dashboard');
        }

        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if (!$this->form_validation->run()) {
            //iNiT Layout NeW SiSP
            $this->data['title'] = 'Masuk Akun';
            $this->data['menu'] = 'signin';
            $this->data['subtitle'] = 'Berikut adalah Halaman Login SISP Keementerian Perdagangan Republik Indonesia';
            $this->data['namafile'] = 'signin';
            $this->data['css'] = false;
            $this->data['js'] = true;
            $this->load->view('frontend/structure/index', $this->data);
        } else {
            $this->_login();
        }
    }

    public function registerForm()
    {
        if ($this->session->userdata('token')) {
            (strtolower($this->session->userdata('role')) == 'administrator') ? redirect('administrator') : redirect('dashboard');
        }

        $this->form_validation->set_rules('tipe_pengguna', 'Tipe Pengguna', 'required|trim');
        $this->form_validation->set_rules('name', 'Nama Lengkap', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('dinas', 'Dinas', 'required|trim');
        $this->form_validation->set_rules('instansi', 'Instansi', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]|min_length[8]');
        $this->form_validation->set_rules('password_confirm', 'Konfirmasi Password', 'required|matches[password]|min_length[8]');
        $this->form_validation->set_rules('phone', 'Nomor Handphone', 'required|trim|numeric');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required|trim');
        $this->form_validation->set_rules('kelurahan', 'Kelurahan', 'required|trim');
        $this->form_validation->set_rules('address', 'Alamat', 'required|trim');

        $split_tipe = explode('-', $this->input->post('tipe_pengguna'));

        if (!empty($this->input->post('tipe_pengguna')) && $split_tipe[0] != 2 && $split_tipe[0] != 12) {
            $this->form_validation->set_rules('nip', 'NIP', 'required|integer|trim');
        }

        if (!empty($this->input->post('tipe_pengguna')) && $split_tipe[0] != 12) {
            $this->form_validation->set_rules('nik', 'NIK', 'required|integer|trim');
        }

        if (!$this->form_validation->run()) {


            if (!$this->session->userdata('tipe_pengguna')) {
                $api = array(
                    'endpoint'      => 'getTipePengguna',
                    'method'        => 'POST',
                    'post_field'    => array('flag_publik' => 'Y')
                );

                $service = $this->serviceAPI($api);

                if ($service && $service['kode'] == 200) {
                    $user_types = $service['data'];

                    $this->session->set_userdata('tipe_pengguna', $user_types);
                }
            }

            if (!$this->session->userdata('master_provinsi')) {
                $api = array(
                    'endpoint'      => 'getMasterDaerah',
                    'method'        => 'POST',
                    'post_field'    => array('daerah_id' => 1)
                );

                $province_api = $this->serviceAPI($api);

                if ($province_api && $province_api['kode'] == 200) {
                    $province = $province_api['data'];

                    $this->session->set_userdata('master_provinsi', $province);
                }
            }

            $this->data['user_types'] = $this->session->userdata('tipe_pengguna');
            $this->data['provinces'] = $this->session->userdata('master_provinsi');

            if (isset($_POST['submit'])) {
                $this->session->set_flashdata('register_error', 'Pengisian form kurang sesuai, silakan periksa dan lengkapi kembali data Anda.');
            }

            //iNiT Layout NeW SiSP
            $this->data['title'] = 'Pendaftaran Akun';
            $this->data['menu'] = 'register';
            $this->data['subtitle'] = 'Berikut adalah Halaman Pendaftaran SISP Keementerian Perdagangan Republik Indonesia';
            $this->data['namafile'] = 'register';
            $this->data['css'] = true;
            $this->data['js'] = true;
            $this->load->view('frontend/structure/index', $this->data);
        } else {

            $checkExist = $this->checkUserExist($this->input->post('email', true));
            $checkExistNIK = $this->checkNIKExist($this->input->post('nik', true));

            $check = true;

            if ($split_tipe[0] != 2) {
                $checkExistNIP = $this->checkNIPExist($this->input->post('nip', true));

                if ($checkExistNIP) {
                    $this->session->set_flashdata('email_terdaftar', 'NIP Anda sudah terdaftar.');

                    $check = false;

                    if (!empty($this->session->userdata('login'))) {
                        redirect('register');
                    }
                }
            } else {
                if ($checkExist || $checkExistNIK) {
                    if ($checkExist[0]['status'] == 'Aktif') {
                        $this->session->set_flashdata('email_terdaftar', 'Email Anda sudah terdaftar.');

                        $check = false;

                        if (!empty($this->session->userdata('login'))) {
                            redirect('register');
                        }
                    } else if ($checkExist[0]['status'] == 'Belum Verifikasi') {
                        $this->session->set_flashdata('email_belum_verifikasi', $this->secure->encrypt_url($this->input->post('email', true)));
                        $check = false;
                    }

                    if ($checkExistNIK) {
                        $this->session->set_flashdata('email_terdaftar', 'NIK Anda sudah terdaftar.');
                        $check = false;

                        if (!empty($this->session->userdata('login'))) {
                            redirect('register');
                        }
                    }
                }
            }

            if ($check) {
                $split_tipe = explode('-', $this->input->post('tipe_pengguna'));

                if ($this->input->post('dinas', true) == 1) {
                    $dinas = 'Dinas Provinsi';
                } else if ($this->input->post('dinas', true) == 2) {
                    $dinas = 'Dinas Kabupaten/Kota';
                } else if ($this->input->post('dinas', true) == 3) {
                    $dinas = 'Lainnya';
                }

                $post_field = array(
                    'nama_lengkap' => $this->input->post('name', true),
                    'nip'   => $this->input->post('nip'),
                    'nik'   => $this->input->post('nik'),
                    'email' => $this->input->post('email', true),
                    'tipe_pengguna' => $split_tipe[1],
                    'no_hp' => $this->input->post('phone', true),
                    'password' => $this->input->post('password', true),
                    'confirm_password' => $this->input->post('password_confirm', true),
                    'alamat' => $this->input->post('address', true),
                    'daerah_id' => $this->input->post('kelurahan', true),
                    'nama_dinas' => $dinas,
                    'instansi'  => $this->input->post('instansi', true),
                    'sp2kp' => $this->input->post('sp2kp'),
                    'token' => $this->token
                );

                $api = array(
                    'endpoint'      => 'daftar',
                    'method'        => 'POST',
                    'post_field'    => $post_field
                );

                $service = $this->serviceAPI($api);

                if ($service && $service['kode'] == 200) {
                    $this->session->set_flashdata('register_success', 'Registrasi berhasil, Silahkan cek Email Anda untuk aktivasi akun.');
                } else if ($service && $service['kode'] == 404) {
                    $this->session->set_flashdata('register_error', 'Gagal melakukan pendaftaran. Silahkan ulangi kembali.');
                }
            }

            redirect('user/sign_in');
        }
    }

    private function _login()
    {
        $post_field = array(
            'email' => $this->input->post('email', true),
            'password'  => $this->input->post('password', true)
        );

        $api = array(
            'endpoint'      => 'checkLogin',
            'method'        => 'POST',
            'post_field'    => $post_field
        );


        $captcha_response = $this->getResponseCaptcha($this->input->post('g-recaptcha-response'));

        if ($captcha_response['success']) {
            $service = $this->serviceAPI($api);

            //var_dump($service);die();
            if ($service && $service['kode'] == 200) {

                $data = $service['data'][0];

                $email  = sha1($data['email']);
                $pass   = sha1($this->input->post('password'));

                $token  = str_replace($email, '', $data['token']);
                $token  = str_replace($pass, '', $token);
                $roleLain = [];
                $iRole = 0;
                foreach(explode(',',$data['tipe_pengguna_lainnya']) as $itemRole){
                    $roleLain[$itemRole] = explode(',',$data['tipe_pengguna_lainnya_nama'])[$iRole];
                    $iRole = $iRole+1;
                }
                $session = array(
                    'pengguna_id'   => $data['pengguna_id'],
                    'nama'          => $data['nama_lengkap'],
                    'daerah_id'     => $data['daerah_id'],
                    'prov_latlong'     => $data['prov_latlong'],
                    'kab_latlong'     => $data['kab_latlong'],
                    'tipe_pengguna_id'     => $data['tipe_pengguna_id'],
                    'role'          => $data['tipe_pengguna'],
                    'email'         => $data['email'],
                    'token'         => $token,
                    'kab_kota'      => $data['kab_kota'],
                    'provinsi'  => $data['provinsi'],
                    'role_lainnya' => $roleLain
                );

                $this->token = $token;

                $this->session->set_userdata($session);

                if (strtolower($data['tipe_pengguna']) == 'administrator') {

                    $api = array(
                        'endpoint'      => 'getMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'jenis' => 'Back end',
                            'parent_null' => 'Y'
                        )
                    );

                    $response = $this->serviceAPI($api);

                    foreach ($response['data'] as $k => $v) {
                        $api_ = array(
                            'endpoint'      => 'getMenu',
                            'method'        => 'POST',
                            'controller'   => 'api',
                            'post_field'    => array(
                                'jenis' => 'Back end',
                                'parent_id' => $v['menu_id'],
                                'urutan' => 'ASC'
                            )
                        );

                        $response_ = $this->serviceAPI($api_);

                        $response['data'][$k]['sub_menu'] = $response_;
                    }

                    if ($response && $response['kode'] == 200) {
                        usort($response['data'], array($this, 'sort'));
                        $this->session->set_userdata('menu', $response);
                    }

                    redirect('web/dashboard');
                } else {

                    $api = array(
                        'endpoint'      => 'getTipePenggunaMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'tipe_pengguna_id' => $data['tipe_pengguna_id'],
                            'parent_null' => 'Y',
                            'urutan' => 'ASC',
                            'jenis' => 'Back end'
                        )
                    );

                    $response = $this->serviceAPI($api);

                    foreach ($response['data'] as $k => $v) {
                        $api_ = array(
                            'endpoint'      => 'getTipePenggunaMenu',
                            'method'        => 'POST',
                            'controller'   => 'api',
                            'post_field'    => array(
                                'tipe_pengguna_id' => $data['tipe_pengguna_id'],
                                'parent_id' => $v['menu_id'],
                                'urutan' => 'ASC',
                                'jenis' => 'Back end'
                            )
                        );

                        $response_ = $this->serviceAPI($api_);

                        $response['data'][$k]['sub_menu'] = $response_['data'];
                    }

                    if ($response && $response['kode'] == 200) {
                        $this->session->set_userdata('menu', $response);
                        $menu = array();
                        $sub_menu = array();
                        foreach ($response['data'] as $k => $v) {
                            $menu[$v['menu_id']] = array(
                                'create' => $v['create'],
                                'update' => $v['update'],
                                'delete' => $v['delete']
                            );
                            foreach ($response['data'][$k]['sub_menu'] as $kk => $vv) {
                                $sub_menu[$vv['menu_id']] = array(
                                    'create' => $vv['create'],
                                    'update' => $vv['update'],
                                    'delete' => $vv['delete']
                                );
                            }
                        }
                        $this->session->set_userdata('perms_menu', $menu);
                        $this->session->set_userdata('perms_sub_menu', $sub_menu);
                    }


                    if (strtolower($data['tipe_pengguna']) == 'kontributor') {
                        redirect('404_override');
                    } else {
                        redirect('web/dashboard');
                    }
                }
            } elseif ($service && $service['kode'] == 96) {
                $data = $service['data'][0];

                if ($data['status'] == 'Tidak Aktif') {
                    $this->session->set_flashdata('login_error', 'Silahkan login menggunakan email <b>' . $this->obfuscate_email($data['email']) . '</b> dan password yang sudah di perbarui.');
                    redirect('user/sign_in');
                } else {
                    $session = array(
                        'login'         => 'SP2KP',
                        'users_id'   => $data['users_sp2kp_id'],
                        'username'   => $data['username'],
                        'kab_kota'      => $data['kab_kota'],
                        'provinsi'  => $data['provinsi'],
                        'pasar'  => $data['pasar'],
                        'flex'   => $data['flex'],
                    );

                    $this->session->set_userdata($session);
                    redirect('register');
                }
            } else {
                $this->session->set_flashdata('login_error', 'Email atau Password Anda tidak valid.');
                redirect('user/sign_in');
            }
        } else {
            $this->session->set_flashdata('login_error', 'Oopsss, Captcha validation failed. ');
            redirect('user/sign_in');
        }
    }

    public function verify($token = null)
    {
        $token = $this->security->xss_clean($token);

        if ($token != null) {
            $api = array(
                'endpoint'      => 'cekTempToken',
                'method'        => 'POST',
                'post_field'    => array('temp_token' => $token)
            );

            $service = $this->serviceAPI($api);

            //var_dump($service);die();

            if ($service['kode'] == 200) {
                if (date('Y-m-d H:i:s') <= date('Y-m-d H:i:s', strtotime($service['data'][0]['expiredon']))) {

                    $status = (strtolower($service['data'][0]['tipe_pengguna']) == 'kontributor') ? 'Menunggu Verifikasi' :  'Aktif';

                    $api = array(
                        'endpoint'      => 'setStatus',
                        'method'        => 'POST',
                        'post_field'    => array(
                            'token' => '',
                            'email' => $service['data'][0]['email'],
                            'table_name'    => 'tbl_pengguna',
                            'key'   => $service['data'][0]['email'],
                            'key_name'  => 'email',
                            'status'    => $status,
                            'keterangan_perubahan'  => 'Verifikasi Email Akun'
                        )
                    );

                    $this->serviceAPI($api);

                    if (strtolower($service['data'][0]['tipe_pengguna']) == 'kontributor' && $service['data'][0]['sp2kp'] == 0) {
                        $this->session->set_flashdata('register_success', 'Verifikasi akun Anda telah berhasil. Untuk dapat menggunakan Akun Anda mohon menunggu verifikasi dari Kementerian Perdagangan');
                    } else {
                        $this->session->set_flashdata('register_success', 'Verifikasi akun Anda telah berhasil. Silahkan Sign In dengan Email Anda.');
                    }
                } else {
                    $this->session->set_flashdata('verify_expired', 'Maaf, sesi aktivasi akun Anda telah habis.');
                    $this->session->set_flashdata('verify_id', $this->secure->encrypt_url($service['email']));
                }
            } else {
                $this->session->set_flashdata('verify_expired', 'Maaf, sesi aktivasi akun Anda telah habis.');
                $this->session->set_flashdata('verify_id', $this->secure->encrypt_url($service['email']));
            }

            redirect('user/sign_in');
        } else {
            redirect('404_override');
        }
    }

    public function regenerateToken($email)
    {
        $email = $this->secure->decrypt_url($email);

        if ($email != null) {
            $api = array(
                'endpoint'      => 'regenerateTempToken',
                'method'        => 'POST',
                'post_field'    => array(
                    'email' => $email
                )
            );

            $service = $this->serviceAPI($api);

            if ($service['kode'] == 200) {
                $this->session->set_flashdata('register_success', 'Terimakasih. Silahkan cek Email Anda untuk aktivasi kembali.');
            }
        }

        redirect('user/sign_in');
    }

    public function logout()
    {
        $this->session->unset_userdata('token');
        $this->session->sess_destroy();

        if (strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting') {
            redirect('https://intra.kemendag.go.id');
        } else {
            redirect(site_url());
        }
    }

    private function checkUserExist($email)
    {
        $api = array(
            'endpoint'      => 'getPengguna',
            'method'        => 'POST',
            'post_field'    => array(
                'limit' => 1,
                'offset' => 0,
                'email' => $email
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            return $service['data'];
        } else {
            return false;
        }
    }

    private function checkNIKExist($nik)
    {
        $api = array(
            'endpoint'      => 'getPengguna',
            'method'        => 'POST',
            'post_field'    => array(
                'limit' => 1,
                'offset' => 0,
                'nik' => $nik
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            return $service['data'];
        } else {
            return false;
        }
    }

    private function checkNIPExist($nip)
    {
        $api = array(
            'endpoint'      => 'getPengguna',
            'method'        => 'POST',
            'post_field'    => array(
                'limit' => 1,
                'offset' => 0,
                'nip' => $nip
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            return $service['data'];
        } else {
            return false;
        }
    }

    function sort($a, $b)
    {
        return $a['urutan'] - $b['urutan'];
    }

    function obfuscate_email($email)
    {
        $em   = explode("@", $email);
        $name = implode('@', array_slice($em, 0, count($em) - 1));
        $len  = floor(strlen($name) / 2);
        return substr($name, 0, 1) . str_repeat('*', $len) . substr($name, (strlen($name)) - 3, 3) . "@" . end($em);
    }

    public function checkNIK()
    {
        $response = array('success' => true);

        $jenis = $this->input->post('jenis');
        $nik = $this->input->post('nik');
        $nip = $this->input->post('nip');

        if ($jenis == 1) {
            if (!empty($nik)) {
                $api = array(
                    'endpoint'      => 'getPengguna',
                    'method'        => 'POST',
                    'post_field'    => array(
                        'limit' => 1,
                        'offset' => 0,
                        'nik' => $nik
                    )
                );

                $service = $this->serviceAPI($api);

                if ($service && $service['kode'] == 200) {
                    $response['success'] = false;
                }
            }
        }

        if ($jenis == 2) {
            if (!empty($nip)) {
                $api = array(
                    'endpoint'      => 'getPengguna',
                    'method'        => 'POST',
                    'post_field'    => array(
                        'limit' => 1,
                        'offset' => 0,
                        'nip' => $nip
                    )
                );

                $service = $this->serviceAPI($api);

                if ($service && $service['kode'] == 200) {
                    $response['success'] = false;
                }
            }
        }

        echo json_encode($response);
    }

    function google_signin()
    {
        $post_field = array(
            'email' => $this->input->post('email'),
            'login_google' => 'Y',
        );

        $api = array(
            'endpoint'      => 'checkLogin',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            $data = $service['data'][0];

            $email  = sha1($data['email']);
            $pass   = sha1('login_google');

            $token  = str_replace($email, '', $data['token']);
            $token  = str_replace($pass, '', $token);

            $session = array(
                'pengguna_id'       => $data['pengguna_id'],
                'nama'              => $data['nama_lengkap'],
                'daerah_id'         => $data['daerah_id'],
                'prov_latlong'      => $data['prov_latlong'],
                'kab_latlong'       => $data['kab_latlong'],
                'tipe_pengguna_id'  => $data['tipe_pengguna_id'],
                'role'              => $data['tipe_pengguna'],
                'email'             => $data['email'],
                'token'             => $token,
                'kab_kota'          => $data['kab_kota'],
                'provinsi'          => $data['provinsi']
            );

            $this->token = $token;

            $this->session->set_userdata($session);

            echo json_encode(array('kode' => 200, 'keterangan' => 'Login Berhasil'));
        }else{
            echo json_encode(array('kode' => 400, 'keterangan' => 'Login Gagal'));
        }
    }
}
