<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bantuan extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();
    }

    public function index()
    {
        //iNiT Layout NeW SiSP
        $this->data['title'] = 'Bantuan';
        $this->data['menu'] = 'bantuan';
        $this->data['subtitle'] = 'Berikut adalah Halaman Bantuan SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'bantuan';
        $this->data['css'] = false;
        $this->data['js'] = false;
        $this->load->view('frontend/structure/index', $this->data);
    }
}
