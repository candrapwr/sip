<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Migor extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->helper('file');
        $this->is_login();


        if (strtolower($this->session->userdata('role')) != 'eksekutif pdn' && strtolower($this->session->userdata('role')) != 'bapokting' && strtolower($this->session->userdata('role')) != 'pengawas migor' && strtolower($this->session->userdata('role')) != 'kontributor' && strtolower($this->session->userdata('role')) != 'administrator' && strtolower($this->session->userdata('role')) != 'eksekutif' && strtolower($this->session->userdata('role')) != 'eksekutif sardislog' && strtolower($this->session->userdata('role')) != 'eksekutif bapokting' && strtolower($this->session->userdata('role')) != 'dinas provinsi' && strtolower($this->session->userdata('role')) != 'dinas perdagangan' && strtolower($this->session->userdata('role')) != strtolower('kontributor sp2kp (provinsi)') && strtolower($this->session->userdata('role')) != strtolower('kontributor sp2kp (kabupaten/kota)') && strtolower($this->session->userdata('role')) != strtolower('kementerian/lembaga')) {
            redirect('404_override');
        }
    }

    public function index()
    {
        $this->data['title'] = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardMigor/DashboardMigor';
        $this->data['token'] = $this->getTableauToken('pdsi');
        $this->templateGovCMS('migor/index', $this->data);
    }

    public function curah()
    {
        $this->data['title'] = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardMigorCurah/Main';
        $this->data['token'] = $this->getTableauToken('pdsikemendag');
        $this->templateGovCMS('migor/index', $this->data);
    }

    public function pe_pasal18()
    {
        $this->data['title'] = 'Dashboard PE Skema Pasal 18';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardPECPO1_2/Pasal18';
        $this->data['token'] = $this->getTableauToken('pdsikemendag');
        $this->templateGovCMS('migor/index', $this->data);
    }

    public function pe_percepatan()
    {
        $this->data['title'] = 'Dashboard PE Peserta Program Percepatan';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardPECPO1_2/Percepatan';
        $this->data['token'] = $this->getTableauToken('pdsikemendag');
        $this->templateGovCMS('migor/index', $this->data);
    }

    public function pe_cpo_inatrade()
    {
        $this->data['title'] = 'Dashboard PE CPO Inatrade';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/PECPOInatrade/DashboardPEMigor';
        $this->data['token'] = $this->getTableauToken('pdsikemendag');
        $this->templateGovCMS('migor/index', $this->data);
    }

    public function pe_mgcr()
    {
        $this->data['title'] = 'Dashboard PE Peserta Program MGCR';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardPECPO-SIMIRAHII/SummaryPenerbitanMGCRAgustus2022';
        $this->data['token'] = $this->getTableauToken('pdsikemendag');
        $this->templateGovCMS('migor/index', $this->data);
    }



    function transaksi_curah()
    {
        //var_dump($this->token);die();
        $this->data['title'] = 'Transaksi Minyak Goreng Curah';
        $this->data['menu'] = 'transaksi_migor';
        $this->data['js'] = 'transaksi_migor_js';

        $this->templateGovCMS('migor/transaksi_migor', $this->data);
    }

    function getTansaksicurah()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('transaksi_curah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('transaksi_curah')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('transaksi_curah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'kontributor') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $api = array(
                'endpoint' => 'getTransaksiKonsumen',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'start_date' => $this->input->post('start_date'),
                    'end_date' => $this->input->post('end_date'),
                    'platform' => $this->input->post('platform'),
                    'kode_pengecer' => $this->input->post('kode_pengecer'),
                    'nib' => $this->input->post('nib'),
                    'kode_daerah' => $kode_daerah,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $data['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {

                $data = array();

                foreach ($response['data'] as $k => $v) {

                    if ($this->input->post('platform') == 'gurih') {
                        $no_telp = $v['no_telpon_pengecer'];
                        $nama_penanggung_jawab = $v['nama_penanggung_jawab_pengecer'];
                    } else {
                        $no_telp = $v['no_telepon_pengecer'];
                        $nama_penanggung_jawab = $v['nama_penanggungjawab_pengecer'];
                    }

                    $data[] = array(
                        'transaksi_konsumen_id' => $v['transaksi_konsumen_id'],
                        'kode_transaksi' => $v['kode_transaksi'],
                        'nama_pengecer' => $v['nama_pengecer'],
                        'no_telp' => $no_telp,
                        'nama_penanggung_jawab' => $nama_penanggung_jawab,
                        'nik' => $v['nik'],
                        'file_nik' => $v['file_nik'],
                        'nama_produsen' => $v['nama_produsen'],
                        'nib_supplier' => $v['nib_supplier'],
                        'kode_pengecer' => $v['kode_pengecer'],
                        'nama_pengecer' => $v['nama_pengecer'],
                        'jumlah_transaksi' => $v['jumlah_transaksi'],
                        'tanggal_transaksi' => $v['tanggal_transaksi'],
                        'nomor_nota_penjualan' => $v['nomor_nota_penjualan']
                    );
                }

                $session = $this->session->all_userdata();
                $session['transaksi_curah'][$pagenumber] = array(
                    'kode' => 200,
                    'keterangan' => 'Transaksi Konsumen ditemukan',
                    'recordsTotal' => $response['recordsTotal'],
                    'recordsFiltered' => $response['recordsFiltered'],
                    'data' => $data
                );
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('transaksi_curah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function pengecer_curah()
    {
        //var_dump($this->token);die();
        $this->data['title'] = 'Pengecer Minyak Goreng';
        $this->data['menu'] = 'pengecer_migor';
        $this->data['js'] = 'pengecer_migor_js';

        $this->templateGovCMS('migor/pengecer_migor', $this->data);
    }

    function getPengecer()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pengecer_migor')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pengecer_migor')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('pengecer_migor')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'kontributor') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $api = array(
                'endpoint' => 'getPengecer',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pengecer_migor'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pengecer_migor')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function produsen_curah()
    {
        $this->data['title'] = 'Produsen Minyak Goreng';
        $this->data['menu'] = 'produsen_migor';
        $this->data['js'] = 'produsen_migor_js';

        $this->templateGovCMS('migor/produsen_migor', $this->data);
    }

    function getProdusen()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('produsen_migor')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('produsen_migor')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('produsen_migor')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if (strtolower($this->session->userdata('role')) == 'kontributor') {
                $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
            } else {
                $kode_daerah = $this->input->post('kode_daerah');
            }

            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'kontributor') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $api = array(
                'endpoint' => 'getProdusen',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            if (!empty($this->input->post('search')['value'])) {
                $api['post_field']['search'] = $this->input->post('search')['value'];
            }

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['produsen_migor'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('produsen_migor')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    public function dashboard_simirah()
    {
        $this->data['title'] = 'Dashboard SIMIRAH';
        $this->data['menu'] = 'dashboard';
        //  $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardSIMIRAH/MainDashboard';
        $this->data['token'] = $this->getTableauToken('pdsi');
        $this->templateGovCMS('migor/dashboard_simirah', $this->data);
    }

    

    public function dashboard_daerah()
    {
        
        // $data['menu_id'] = 67;
        // is_allowed($data['menu_id']);
        $this->data['title'] = 'Dashboard Distribusi Minyak Goreng Rakyat';
        $this->data['menu'] = 'dashboard';
        //  $this->data['js'] = 'migor_js'

        if (in_array($this->session->userdata('role'), ['Administrator', 'Eksekutif PDN', 'Eksekutif', 'Pengawas Migor'])) {
            $this->data['urlparams'] = ':showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link&%3F%3Aembed=yes&%3Atoolbar=n&:dataDetails=no&:showDataDetails=yes&:refresh=true&:alerts=no&:customViews=no&:device=desktop&:showAskData=false&:showShareOptions=false&:subscriptions=no';
            $this->data['view_tableau'] = '/views/DistribusiDMO/MonitoringDMO';
        } else {
            $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no&:refresh=true&Prov=' . substr($this->session->userdata('daerah_id'), 0, 2);
            $this->data['view_tableau'] = '/views/DistribusiDMODaerah/MonitoringDMO';
        }

        $this->data['token'] = $this->getTableauToken('sisp.explorer');
        $this->templateGovCMS('migor/dashboard_simirah', $this->data);
    }

    public function dashboard_dmo()
    {
        $this->data['title'] = 'Dashboard Distribusi DMO';
        $this->data['menu'] = 'dashboard';
        //  $this->data['js'] = 'migor_js';
        $this->data['urlparams'] = ':embed=yes&:showShareOptions=false&:showAskData=false&:customViews=no';
        $this->data['view_tableau'] = '/views/DashboardDistribusiDMO/PendistribusianDMO';
        $this->data['token'] = $this->getTableauToken('pdsi');
        $this->templateGovCMS('migor/index', $this->data);
    }

    function exportir_simirah()
    {
        $this->data['title'] = 'Exportir Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'exportir_simirah';
        $this->data['js'] = 'exportir_simirah_js';

        $this->templateGovCMS('migor/exportir_simirah', $this->data);
    }

    function produsen_simirah()
    {
        // is_allowed(44);
        $this->data['title'] = 'Produsen Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'produsen_simirah';
        $this->data['js'] = 'produsen_simirah_js';

        $this->templateGovCMS('migor/produsen_simirah', $this->data);
    }

    function d1_simirah()
    {
        $this->data['title'] = 'Distributor D1 Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'd1_simirah';
        $this->data['js'] = 'd1_simirah_js';

        $this->templateGovCMS('migor/d1_simirah', $this->data);
    }

    function d2_simirah()
    {
        $this->data['title'] = 'Distributor D2 Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'd2_simirah';
        $this->data['js'] = 'd2_simirah_js';

        $this->templateGovCMS('migor/d2_simirah', $this->data);
    }

    function pengecer_simirah()
    {
        $this->data['title'] = 'Pengecer Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'pengecer_simirah';
        $this->data['js'] = 'pengecer_simirah_js';

        $this->templateGovCMS('migor/pengecer_simirah', $this->data);
    }

    function trx_cooperation_simirah()
    {
        $this->data['title'] = 'Transaksi Kerjasama Exportir -> Produsen CPO / Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_cooperation_simirah';
        $this->data['js'] = 'trx_cooperation_simirah_js';

        $this->templateGovCMS('migor/trx_cooperation_simirah', $this->data);
    }

    function trx_produsencpo_simirah()
    {
        $this->data['title'] = 'Transaksi Produsen CPO -> Produsen Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_produsencpo_simirah';
        $this->data['js'] = 'trx_produsencpo_simirah_js';

        $this->templateGovCMS('migor/trx_produsencpo_simirah', $this->data);
    }

    function trx_produsenmigor_simirah()
    {
        $this->data['title'] = 'Transaksi Produsen Minyak Goreng -> Distributor (D1) SIMIRAH';
        $this->data['menu'] = 'trx_produsenmigor_simirah';
        $this->data['js'] = 'trx_produsenmigor_simirah_js';

        $this->templateGovCMS('migor/trx_produsenmigor_simirah', $this->data);
    }

    function trx_d1_simirah()
    {
        $this->data['title'] = 'Transaksi Distributor (D1) -> Distributor (D2) / Pengecer Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_d1_simirah';
        $this->data['js'] = 'trx_d1_simirah_js';

        $this->templateGovCMS('migor/trx_d1_simirah', $this->data);
    }

    function trx_d2_simirah()
    {
        $this->data['title'] = 'Transaksi Distributor (D2) -> Pengecer Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_d2_simirah';
        $this->data['js'] = 'trx_d2_simirah_js';

        $this->templateGovCMS('migor/trx_d2_simirah', $this->data);
    }

    function trx_pengecer_simirah()
    {
        $this->data['title'] = 'Transaksi Pengecer / D2 -> Konsumen Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_pengecer_simirah';
        $this->data['js'] = 'trx_pengecer_simirah_js';

        $this->templateGovCMS('migor/trx_pengecer_simirah', $this->data);
    }

    function summary_simirah()
    {

        $this->data['title'] = 'Summary Distribusi SIMIRAH';
        $this->data['menu'] = 'summary_simirah';
        $this->data['js'] = 'summary_simirah_js';
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        $this->templateGovCMS('migor/summary_simirah', $this->data);
    }

    function pengawasan_distribusi_simirah()
    {

        $this->data['title'] = 'Pemantauan Distribusi Minyak Goreng';
        $this->data['menu'] = 'pengawasan_distribusi_simirah';
        $this->data['js'] = 'pengawasan_distribusi_simirah_js';
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        $this->templateGovCMS('migor/pengawasan_distribusi_simirah', $this->data);
    }

    function alokasi_simirah()
    {
        $this->data['title'] = 'Alokasi Distribusi SIMIRAH';
        $this->data['menu'] = 'alokasi_simirah';
        $this->data['js'] = 'alokasi_simirah_js';

        $api = array(
            'endpoint' => 'getPeriodeAlokasiDMO',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'token' => $this->token,
                'limit' => 10000,
                'offset' => 0
            )
        );
        $this->data['periode'] = $this->serviceAPI($api);
        $this->data['menu_id'] = 58;
        $this->templateGovCMS('migor/alokasi_simirah', $this->data);
    }

    function laporkan_distribusi()
    {

        //   var_dump($this->input->post());
        // die();
        $api = array(
            'endpoint' => 'setPemantauanMigor',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'email' => $this->session->userdata('email'),
                'tgl' => $this->input->post('tgl_pemantauan'),
                'daerah_id' => ((empty($this->input->post('daerah_id'))) ? ($this->input->post('provinsi_id') . '00') : $this->input->post('daerah_id')),
                'petugas' => $this->input->post('petugas'),
                'nama_pelaku_usaha' => $this->input->post('nama_pu'),
                'terdaftar_simirah' => $this->input->post('terdaftar'),
                'nib_pu' => $this->input->post('nib'),
                'nama_pu' => $this->input->post('nama_pu'),
                'jenis_pu' => $this->input->post('jenis_pu'),
                'alamat_pu' => $this->input->post('alamat_pu'),
                'nama_pemilik' => $this->input->post('nama_pemilik'),
                'telp_pemilik' => $this->input->post('telp_pemilik'),
                'catatan' => $this->input->post('catatan'),
                'pemantauan_migor_id' => $this->input->post('pemantauan_migor_id'),
            )
        );

        $response = $this->serviceAPI($api);
       // var_dump($this->input->post());die();
        if ($response && $response['kode'] == 200) {
            $this->session->set_flashdata('message', 'sukses');
            redirect(base_url() . 'web/pengawasan-distribusi-simirah');
        } else {
            redirect(base_url() . 'web/pengawasan-distribusi-simirah');
        }
    }

    function penerimaan_distribusi()
    {

        $data = array(
            'email' => $this->session->userdata('email'),
            'periode' => date('Y-m-d'),
            'pemantauan_migor_id' => $this->input->post('pemantauan_migor_id'),
            'terdaftar_simirah' => $this->input->post('terdaftar_simirah'),
            'no_dokumen' => $this->input->post('no_dokumen'),
            'tgl_do' => $this->input->post('tgl_do'),
            'tgl_status' => $this->input->post('tgl_status'),
            'nib_pengirim' => $this->input->post('nib_pengirim'),
            'nama_pengirim' => $this->input->post('nama_pengirim'),
            'jenis_pengirim' => $this->input->post('jenis_pengirim'),
            'jml_distribusi' => $this->input->post('jml_distribusi'),
            'harga_pembelian' => $this->input->post('harga_pembelian'),
            'jml_distribusi_rill' => $this->input->post('jml_distribusi_rill'),
            'harga_pembelian_rill' => $this->input->post('harga_pembelian_rill'),
            'harga_penjualan_rill' => $this->input->post('harga_penjualan_rill'),
            'stok' => $this->input->post('stok'),
            'keterangan' => $this->input->post('keterangan'),
            'kesesuaian' => $this->input->post('kesesuaian'),
            'pemantauan_migor_detail_id' => $this->input->post('pemantauan_migor_detail_id'),
        );

        if ($_FILES['lampiran']['tmp_name'] != '') {
            $data['lampiran'] = new CurlFile($_FILES['lampiran']['tmp_name'], $_FILES['lampiran']['type'], $_FILES['lampiran']['name']);
        }


        if ($this->input->post('terdaftar_simirah') == 'Tidak') {
            $data['jml_distribusi'] = '-';
            $data['harga_pembelian'] = '-';
        }


        $api = array(
            'endpoint' => 'setPemantauanMigorDetail',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => $data
        );

        $response = $this->serviceAPI($api);
        // var_dump($response);die();
        if ($response && $response['kode'] == 200) {
            $this->session->set_flashdata('message', 'sukses');
            redirect(base_url() . 'web/pengawasan-distribusi-simirah');
        } else {
            redirect(base_url() . 'web/pengawasan-distribusi-simirah');
        }
    }

    function getPemantauanMigor()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pemantauan_migor')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pemantauan_migor')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('pemantauan_migor')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }
            $exclude_jenis_pu = '';
            // if(strtolower($this->session->userdata('role')) != 'administrator'){
            //     $exclude_jenis_pu = 'pengecer';
            // }


            $api = array(
                'endpoint' => 'getPemantauanMigor',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    // 'tgl_awal' => $this->input->post('tgl_awal'),
                    // 'tgl_akhir' => $this->input->post('tgl_akhir'),
                    'kode_daerah' => $kode_daerah,
                    'exclude_jenis_pu' => $exclude_jenis_pu,
                    // 'nib' => $this->input->post('nib'),
                    // 'nama_pu' => $this->input->post('nama_pu'),
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pemantauan_migor'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pemantauan_migor')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getPemantauanMigorDetail()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pemantauan_migor_detail')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pemantauan_migor_detail')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('pemantauan_migor_detail')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }


            $api = array(
                'endpoint' => 'getPemantauanMigorDetail',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    // 'tgl_awal' => $this->input->post('tgl_awal'),
                    // 'tgl_akhir' => $this->input->post('tgl_akhir'),
                     'kode_daerah' => $kode_daerah,
                    // 'nib' => $this->input->post('nib'),
                    'pemantauan_migor_id' => $this->input->post('pemantauan_migor_id'),
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pemantauan_migor_detail'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pemantauan_migor_detail')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function mirah_trx_export($data, $type, $tgl_awal, $tgl_akhir)
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        //  pre($data);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $row = 3;
        $periode = '';


        if ($tgl_awal != '' && $tgl_awal != null) {
            if ($type == 'Produsen CPO ke Produsen Migor') {
                $sheet->mergeCells('A1:R1');
                $sheet->mergeCells('A2:R2');
            } else {
                $sheet->mergeCells('A1:Q1');
                $sheet->mergeCells('A2:Q2');
            }
            $row = 4;
            $periode = 'Periode Pengiriman ' . date('d F Y', strtotime($tgl_awal)) . ' s/d ' . date('d F Y', strtotime($tgl_akhir));
        } else {
            if ($type == 'Produsen CPO ke Produsen Migor') {
                $sheet->mergeCells('A1:R1');
            } else {
                $sheet->mergeCells('A1:Q1');
            }
        }


        $spreadsheet->getProperties()
            ->setCreator("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
            ->setLastModifiedBy("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
            ->setTitle('Rekap Distribusi ' . $type . ' SIMIRAH diterima ' . $periode . '')
            ->setDescription(
                "Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")"
            );

        $sheet->setCellValue('A1', 'Rekap Distribusi ' . $type . ' SIMIRAH diterima');
        $sheet->setCellValue('A2', $periode);


        $sheet->getStyle('A1')->getFont()->setBold(true);
        $sheet->getStyle('A1')->getFont()->setSize(16);;
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A2')->getFont()->setBold(true);
        $sheet->getStyle('A2')->getFont()->setSize(16);;
        $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');

        $range = range('A', 'R');
        foreach ($range as $k => $v) {
            $sheet->getColumnDimension($v)->setAutoSize(true);
            $sheet->getStyle($v . $row)->getFont()->setBold(true);
        }



        $sheet->setCellValue('A' . $row, 'NO');
        $sheet->setCellValue('B' . $row, 'NIB Pengirim');
        $sheet->setCellValue('C' . $row, 'Nama Pengirim');
        $sheet->setCellValue('D' . $row, 'Tujuan');
        $sheet->setCellValue('E' . $row, 'Tanggal Input');
        $sheet->setCellValue('F' . $row, 'No DO');
        $sheet->setCellValue('G' . $row, 'Tanggal Kirim');
        $sheet->setCellValue('H' . $row, 'Jumlah Kirim');
        $sheet->setCellValue('I' . $row, 'Satuan');
        $sheet->setCellValue('J' . $row, 'Harga');
        if ($type == 'Distributor (D1) ke D2 atau Pengecer') {
            $sheet->setCellValue('K' . $row, 'NIB / NPWP Penerima');
            $sheet->setCellValue('L' . $row, 'Nama Penerima');
        } else if ($type == 'Distributor (D2) ke Pengecer') {
            $sheet->setCellValue('K' . $row, 'NPWP Penerima');
            $sheet->setCellValue('L' . $row, 'Nama Penerima');
        } else {
            $sheet->setCellValue('K' . $row, 'NIB Penerima');
            $sheet->setCellValue('L' . $row, 'Nama Penerima');
        }

        $sheet->setCellValue('M' . $row, 'Tanggal Terima');
        $sheet->setCellValue('N' . $row, 'Jumlah Terima');
        $sheet->setCellValue('O' . $row, 'Satuan');
        $sheet->setCellValue('P' . $row, 'Harga');
        $sheet->setCellValue('Q' . $row, 'Status');
        if ($type == 'Produsen CPO ke Produsen Migor') {
            $sheet->setCellValue('R' . $row, 'Jumlah Konversi');
        }

        $row = $row + 1;
        $no = 1;
        $total = 0;
        foreach ($data as $k => $v) {
            $sheet->setCellValue('A' . $row, $no);
            $sheet->setCellValueExplicit('B' . $row, $v['nib_pengirim'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $sheet->setCellValue('C' . $row, $v['nama_pengirim']);
            $sheet->setCellValue('D' . $row, $v['provinsi']);
            $sheet->setCellValue('E' . $row, $v['waktu_terbit']);
            $sheet->setCellValue('F' . $row, $v['no_dokumen']);
            $sheet->setCellValue('G' . $row, $v['tgl_pengiriman']);
            $sheet->setCellValue('H' . $row, $v['jumlah']);
            $sheet->setCellValue('I' . $row, $v['satuan']);
            $sheet->setCellValue('J' . $row, $v['harga']);
            if ($type == 'Distributor (D1) ke D2 atau Pengecer') {
                if ($v['id_d2'] != '') {
                    $sheet->setCellValueExplicit('K' . $row, $v['nib_d2_penerima'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('L' . $row, $v['nama_d2_penerima']);
                } else if ($type == 'Distributor (D2) ke Pengecer') {
                    $sheet->setCellValueExplicit('K' . $row, $v['npwp_pengecer_penerima'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('L' . $row, $v['nama_pengecer_penerima']);
                } else {
                    $sheet->setCellValueExplicit('K' . $row, $v['npwp_pengecer_penerima'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $sheet->setCellValue('L' . $row, $v['nama_pengecer_penerima']);
                }
            } else {
                $sheet->setCellValueExplicit('K' . $row, $v['nib_penerima'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('L' . $row, $v['nama_penerima']);
            }
            $sheet->setCellValue('M' . $row, $v['date_received']);
            $sheet->setCellValue('N' . $row, $v['volume_received']);
            $sheet->setCellValue('O' . $row, $v['satuan']);
            $sheet->setCellValue('P' . $row, $v['price_received']);
            $sheet->setCellValue('Q' . $row, $v['status']);
            if ($type == 'Produsen CPO ke Produsen Migor') {
                $sheet->setCellValue('R' . $row, $v['volume_conversion']);
            }
            $row++;
            $no++;
        }


        //NOTE
        $sheet->setCellValue('A' . ($row + 3), "*Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")");
        $sheet->getStyle('A' . ($row + 3))->getFont()->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('A' . ($row + 3))->getAlignment()->setWrapText(true);


        if ($type == 'Produsen CPO ke Produsen Migor') {
            $sheet->mergeCells('A' . ($row + 3) . ':R' . ($row + 3));
        } else {
            $sheet->mergeCells('A' . ($row + 3) . ':Q' . ($row + 3));
        }


        $writer = new Xlsx($spreadsheet);
        $filename = 'Rekap Distribusi ' . $type . ' SIMIRAH diterima _' . date('YmdHis');
        $writer->save('./assets/res/file/' . $filename . '.xlsx');
        $this->session->set_userdata('export_status', 'success');
        $this->session->set_userdata('export_file', 'assets/res/file/' . $filename . '.xlsx');
    }

    function alokasidmo_export()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($this->input->post('produsen_cpo'));
        $spreadsheet->getActiveSheet()->setTitle("Produsen CPO");
        $spreadsheet->getActiveSheet()->getStyle('C')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet_migor = $reader->loadFromString($this->input->post('produsen_migor'));
        $spreadsheet_migor->getActiveSheet()->setTitle("Produsen Migor");
        $spreadsheet_migor->getActiveSheet()->getStyle('C')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $spreadsheet_migor->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet_migor->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet_migor->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet_migor->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->addExternalSheet($spreadsheet_migor->getActiveSheet());

        $spreadsheet_migor_cpo = $reader->loadFromString($this->input->post('produsen_migor_cpo'));
        $spreadsheet_migor_cpo->getActiveSheet()->setTitle("Produsen Migor (CPO)");
        $spreadsheet_migor_cpo->getActiveSheet()->getStyle('C')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $spreadsheet_migor_cpo->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet_migor_cpo->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet_migor_cpo->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet_migor_cpo->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->addExternalSheet($spreadsheet_migor_cpo->getActiveSheet());

        $spreadsheet_exportir_cpo = $reader->loadFromString($this->input->post('exportir_cpo'));
        $spreadsheet_exportir_cpo->getActiveSheet()->setTitle("Exportir - Produsen CPO");
        $spreadsheet_exportir_cpo->getActiveSheet()->getStyle('C')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $spreadsheet_exportir_cpo->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet_exportir_cpo->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet_exportir_cpo->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet_exportir_cpo->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->addExternalSheet($spreadsheet_exportir_cpo->getActiveSheet());

        $spreadsheet_exportir_migor = $reader->loadFromString($this->input->post('exportir_migor'));
        $spreadsheet_exportir_migor->getActiveSheet()->setTitle("Exportir - Produsen Migor");
        $spreadsheet_exportir_migor->getActiveSheet()->getStyle('C')
            ->getNumberFormat()
            ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
        $spreadsheet_exportir_migor->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet_exportir_migor->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet_exportir_migor->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet_exportir_migor->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

        $spreadsheet->addExternalSheet($spreadsheet_exportir_migor->getActiveSheet());

        $writer = new Xlsx($spreadsheet);
        $filename = $this->input->post('title') . '_' . date('YmdHis');
        $writer->save('./assets/res/file/' . $filename . '.xlsx');
        $this->session->set_userdata('export_status', 'success');
        $this->session->set_userdata('export_file', 'assets/res/file/' . $filename . '.xlsx');
    }

    function summary_alokasi_dmo_export($type = null)
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($this->input->post('data'));
        $spreadsheet->getActiveSheet()->setTitle('Alokasi');

        if ($type == 'stat') {
            $spreadsheet->getActiveSheet()->getStyle('D2')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('E')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        } else if ($type == 'perusahaan') {
            $spreadsheet->getActiveSheet()->getStyle('B')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('E')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
            $spreadsheet->getActiveSheet()->getStyle('F')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        } else {
            $spreadsheet->getActiveSheet()->getStyle('B')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('C')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER);
            $spreadsheet->getActiveSheet()->getStyle('E')
                ->getNumberFormat()
                ->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER_00);
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);


        $writer = new Xlsx($spreadsheet);
        $filename = $this->input->post('title') . '_' . date('YmdHis');
        $writer->save('./assets/res/file/' . $filename . '.xlsx');
        $this->session->set_userdata('export_status', 'success');
        $this->session->set_userdata('export_file', 'assets/res/file/' . $filename . '.xlsx');
    }


    function mirah_export()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }

        $kode_daerah = $this->input->post('kode_daerah');
        $status = $this->input->post('status');
        $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $type = urldecode($this->input->post('type'));

        $api = array(
            'endpoint' => 'getSummarySimirah',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'platform' => $this->input->post('platform'),
                'kode_daerah' => $kode_daerah,
                'status' => $status,
                'type' => $type,
                'tgl_awal' => $tgl_awal,
                'tgl_akhir' => $tgl_akhir,
                'nama_pengirim' => $nama_pengirim,
                'limit' => 1000000000000,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);
        if ($response['kode'] == 200) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $row = 3;
            $tanpa_tanggal = true;
            $periode = '';

            if ($tgl_awal == '' || $tgl_awal == null) {
                $tanpa_tanggal = false;
                $sheet->mergeCells('A1:L1');
            } else {
                $sheet->mergeCells('A1:K1');
                $sheet->mergeCells('A2:K2');
                $row = 4;
                $periode = 'Periode Pengiriman ' . date('d F Y', strtotime($tgl_awal)) . ' s/d ' . date('d F Y', strtotime($tgl_akhir));
            }


            $spreadsheet->getProperties()
                ->setCreator("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
                ->setLastModifiedBy("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
                ->setTitle('Rekap Jumlah Distribusi SIMIRAH diterima ' . $periode . '')
                ->setDescription(
                    "Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")"
                );

            $sheet->setCellValue('A1', 'Rekap Jumlah Distribusi SIMIRAH diterima');
            $sheet->setCellValue('A2', $periode);


            $sheet->getStyle('A1')->getFont()->setBold(true);
            $sheet->getStyle('A1')->getFont()->setSize(16);;
            $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2')->getFont()->setBold(true);
            $sheet->getStyle('A2')->getFont()->setSize(16);;
            $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');

            $sheet->getColumnDimension('A')->setAutoSize(true);
            $sheet->getColumnDimension('B')->setAutoSize(true);
            $sheet->getColumnDimension('C')->setAutoSize(true);
            $sheet->getColumnDimension('D')->setAutoSize(true);
            $sheet->getColumnDimension('E')->setAutoSize(true);
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->getColumnDimension('G')->setAutoSize(true);
            $sheet->getColumnDimension('H')->setAutoSize(true);
            $sheet->getColumnDimension('I')->setAutoSize(true);
            $sheet->getColumnDimension('J')->setAutoSize(true);
            $sheet->getColumnDimension('K')->setAutoSize(true);
            $sheet->getColumnDimension('L')->setAutoSize(true);

            $sheet->setCellValue('A' . $row, 'NO');
            $sheet->setCellValue('B' . $row, 'NIB');
            $sheet->setCellValue('C' . $row, 'Nama');
            $sheet->setCellValue('D' . $row, 'Type');
            $sheet->setCellValue('E' . $row, 'Jenis');
            $sheet->setCellValue('F' . $row, 'Provinsi');
            $sheet->setCellValue('G' . $row, 'Jumlah Distribusi');
            $sheet->setCellValue('H' . $row, 'Pengali Regional');
            $sheet->setCellValue('I' . $row, 'Pengali Kemasan');
            $sheet->setCellValue('J' . $row, 'Jumlah DMO');
            $sheet->setCellValue('K' . $row, 'Satuan');
            if (!$tanpa_tanggal) {
                $sheet->setCellValue('L' . $row, 'Tanggal');
            }



            $sheet->getStyle('A' . $row)->getFont()->setBold(true);
            $sheet->getStyle('B' . $row)->getFont()->setBold(true);
            $sheet->getStyle('C' . $row)->getFont()->setBold(true);
            $sheet->getStyle('D' . $row)->getFont()->setBold(true);
            $sheet->getStyle('E' . $row)->getFont()->setBold(true);
            $sheet->getStyle('F' . $row)->getFont()->setBold(true);
            $sheet->getStyle('G' . $row)->getFont()->setBold(true);
            $sheet->getStyle('H' . $row)->getFont()->setBold(true);
            $sheet->getStyle('I' . $row)->getFont()->setBold(true);
            $sheet->getStyle('J' . $row)->getFont()->setBold(true);
            $sheet->getStyle('K' . $row)->getFont()->setBold(true);
            $sheet->getStyle('L' . $row)->getFont()->setBold(true);


            $row = $row + 1;
            $no = 1;
            $total = 0;
            foreach ($response['data'] as $k => $v) {
                $sheet->setCellValue('A' . $row, $no);
                $sheet->setCellValueExplicit('B' . $row, $v['nib'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('C' . $row, $v['nama']);
                $sheet->setCellValue('D' . $row, $v['type']);
                $sheet->setCellValue('E' . $row, $v['packaging']);
                $sheet->setCellValue('F' . $row, $v['provinsi']);
                $sheet->setCellValue('G' . $row, $v['jml_distribusi']);
                $sheet->setCellValue('H' . $row, $v['pengali_regional']);
                $sheet->setCellValue('I' . $row, $v['pengali_kemasan']);
                $sheet->setCellValue('J' . $row, ((fmod((float) $v['jml'], 1) != 0) ? round((float) $v['jml'], 2) : $v['jml']));
                $sheet->setCellValue('K' . $row, $v['satuan']);
                $total = $total + $v['jml'];
                if (!$tanpa_tanggal) {
                    $sheet->setCellValue('L' . $row, $v['date']);
                }
                $row++;
                $no++;
            }

            //SUMMARY
            $sheet->setCellValue('A' . ($row), "Total");
            $sheet->getStyle('A' . ($row))->getFont()->setBold(true);

            //NOTE
            $sheet->setCellValue('A' . ($row + 3), "*Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")");
            $sheet->getStyle('A' . ($row + 3))->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('A' . ($row + 3))->getAlignment()->setWrapText(true);


            if ($tgl_awal == '' || $tgl_awal == null) {
                $sheet->mergeCells('A' . ($row) . ':I' . ($row));
                $sheet->mergeCells('A' . ($row + 3) . ':L' . ($row + 3));
                $sheet->setCellValue('J' . ($row), $total);
                $sheet->getStyle('J' . ($row))->getFont()->setBold(true);
            } else {
                $sheet->mergeCells('A' . ($row) . ':I' . ($row));
                $sheet->mergeCells('A' . ($row + 3) . ':K' . ($row + 3));
                $sheet->setCellValue('J' . ($row), $total);
                $sheet->getStyle('J' . ($row))->getFont()->setBold(true);
            }


            $writer = new Xlsx($spreadsheet);
            $filename = $this->input->post('title') . '_' . date('YmdHis');
            $writer->save('./assets/res/file/' . $filename . '.xlsx');
            $this->session->set_userdata('export_status', 'success');
            $this->session->set_userdata('export_file', 'assets/res/file/' . $filename . '.xlsx');
        } else {
            $this->session->set_userdata('export_status', 'fail');
        }
    }


    function pemantauan_export()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }

        $kode_daerah = '';
        if (!empty($this->input->post('kode_daerah'))) {
            $kode_daerah = $this->input->post('kode_daerah');
        } else {
            if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
                $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
            } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
            }
        }


        $api = array(
            'endpoint' => 'getPemantauanMigorDetail',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'kode_daerah' => $kode_daerah,
                'limit' => 20000,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $row = 3;
            $filename = 'Rekap Pemantauan Distribusi Minyak Goreng SIMIRAH';

            $spreadsheet->getProperties()
                ->setCreator("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
                ->setLastModifiedBy("Pusat Data dan Sistem Informasi Kementerian Perdagangan")
                ->setTitle($filename)
                ->setDescription(
                    "Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")"
                );

            $sheet->setCellValue('A1',  $filename);

            $spreadsheet->getActiveSheet()->mergeCells('A1:Y1');



            $sheet->getStyle('A1')->getFont()->setBold(true);
            $sheet->getStyle('A1')->getFont()->setSize(16);;
            $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
            $sheet->getStyle('A2')->getFont()->setBold(true);
            $sheet->getStyle('A2')->getFont()->setSize(16);;
            $sheet->getStyle('A2')->getAlignment()->setHorizontal('center');

            // echo '<pre>';
            // var_dump($response['data']);die();


            $range = range('A', 'Y');
            foreach ($range as $k => $v) {
                $sheet->getColumnDimension($v)->setAutoSize(true);
                $sheet->getStyle($v . $row)->getFont()->setBold(true);
            }


            $sheet->setCellValue('A' . $row, 'NO');
            $sheet->setCellValue('B' . $row, 'NIB Pengirim');
            $sheet->setCellValue('C' . $row, 'Nama Pengirim');
            $sheet->setCellValue('D' . $row, 'No DO');
            $sheet->setCellValue('E' . $row, 'Jenis Pengirim');
            $sheet->setCellValue('F' . $row, 'Terdaftar Di SIMIRAH');
            $sheet->setCellValue('G' . $row, 'Tanggal DO');
            $sheet->setCellValue('H' . $row, 'Tanggal Status');
            $sheet->setCellValue('I' . $row, 'Jumlah Distribusi');
            $sheet->setCellValue('J' . $row, 'Harga Pembelian');
            $sheet->setCellValue('K' . $row, 'Jumlah Distribusi Sebenarnya');
            $sheet->setCellValue('L' . $row, 'Harga Pembelian Sebenarnya');
            $sheet->setCellValue('M' . $row, 'Harga Penjualan');
            $sheet->setCellValue('N' . $row, 'Stok');
            $sheet->setCellValue('O' . $row, 'Lampiran');
            $sheet->setCellValue('P' . $row, 'Kesesuaian');
            $sheet->setCellValue('Q' . $row, 'Selisih Jumlah Distribusi');
            $sheet->setCellValue('R' . $row, 'Selisih Harga Pembelian');
            $sheet->setCellValue('S' . $row, 'Keterangan Pemantauan DO');
            $sheet->setCellValue('T' . $row, 'NIB Perusahaan Pemantauan');
            $sheet->setCellValue('U' . $row, 'Nama Perusahaan Pemantauan');
            $sheet->setCellValue('V' . $row, 'Jenis Perusahaan Pemantauan');
            $sheet->setCellValue('W' . $row, 'Catatan Perusahaan Pemantauan');
            $sheet->setCellValue('X' . $row, 'Provinsi Pemantauan');
            $sheet->setCellValue('Y' . $row, 'Kabupaten / Kota Pemantauan');
            $sheet->setCellValue('Z' . $row, 'Periode Pengawasan');
            $sheet->setCellValue('AA' . $row, 'Pelaksana Pengawasan');
            $sheet->setCellValue('AB' . $row, 'Pelaksana Instansi');
            $sheet->setCellValue('AC' . $row, 'Pelaksana Nama Dinas');



            $row = $row + 1;
            $no = 1;
            foreach ($response['data'] as $k => $v) {
                $sheet->setCellValue('A' . $row, $no);
                $sheet->setCellValueExplicit('B' . $row, $v['nib_pengirim'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('C' . $row, $v['nama_pengirim']);
                $sheet->setCellValue('D' . $row, $v['no_dokumen']);
                $sheet->setCellValue('E' . $row, $v['jenis_pengirim']);
                $sheet->setCellValue('F' . $row, $v['terdaftar_simirah']);
                $sheet->setCellValue('G' . $row, $v['tgl_do']);
                $sheet->setCellValue('H' . $row, $v['tgl_status']);
                $sheet->setCellValue('I' . $row, $v['jml_distribusi']);
                $sheet->setCellValue('J' . $row, $v['harga_pembelian']);
                $sheet->setCellValue('K' . $row, $v['jml_distribusi_rill']);
                $sheet->setCellValue('L' . $row, $v['harga_pembelian_rill']);
                $sheet->setCellValue('M' . $row, $v['harga_penjualan_rill']);
                $sheet->setCellValue('N' . $row, $v['stok']);
                if(!empty($v['lampiran'])){
                    $sheet->setCellValue('O' . $row, 'Klik Untuk Unduh Lampiran');
                    $spreadsheet->getActiveSheet()->getCell('O' . $row)->getHyperlink()->setUrl(('https://sisp.kemendag.go.id/services/download/migor/pelaporan/'. $v['lampiran']));
                }else{
                    $sheet->setCellValue('O' . $row, '-');
                }
                $sheet->setCellValue('P' . $row, ucwords($v['kesesuaian']));
                $sheet->setCellValue('Q' . $row, (($v['terdaftar_simirah'] == 'Ya') ? ((float)$v['jml_distribusi_rill'] -  (float)$v['jml_distribusi']) : '0'));
                $sheet->setCellValue('R' . $row, (($v['terdaftar_simirah'] == 'Ya') ? ((float)$v['harga_pembelian_rill'] -  (float)$v['harga_pembelian']) : '0'));
                $sheet->setCellValue('S' . $row, $v['keterangan']);
                $sheet->setCellValueExplicit('T' . $row, $v['nib_pu'], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $sheet->setCellValue('U' . $row, $v['nama_pu']);
                $sheet->setCellValue('V' . $row, $v['jenis_pu']);
                $sheet->setCellValue('W' . $row, $v['catatan']);
                $sheet->setCellValue('X' . $row, $v['provinsi']);
                $sheet->setCellValue('Y' . $row, $v['kab_kota']);
                $sheet->setCellValue('Z' . $row, $v['periode']);
                $sheet->setCellValue('AA' . $row, $v['pelaksana_pemantauan']);
                $sheet->setCellValue('AB' . $row, $v['pelaksana_instansi']);
                $sheet->setCellValue('AC' . $row, $v['pelaksana_nama_dinas']);
             
                $row++;
                $no++;
            }


            //NOTE
            $spreadsheet->getActiveSheet()->mergeCells('A' . ($row + 3) . ':Y' . ($row + 3) . '');
            $sheet->setCellValue('A' . ($row + 3), "*Data yang ada dalam dokumen ini bersumber dari sistem SIMIRAH Kementerian Perindustrian dan diolah oleh Pusat Data dan Sistem Informasi Kementerian Perdagangan melalui sistem SISP (" . date('Y') . ")");
            $sheet->getStyle('A' . ($row + 3))->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('A' . ($row + 3))->getAlignment()->setWrapText(true);

            $writer = new Xlsx($spreadsheet);
            $filename = $filename . '_' . date('YmdHis');
            $writer->save('./assets/res/file/' . $filename . '.xlsx');
            $this->session->set_userdata('export_status', 'success');
            $this->session->set_userdata('export_file', 'assets/res/file/' . $filename . '.xlsx');
        } else {
            $this->session->set_userdata('export_status', 'fail');
        }
    }

    public function mirah_download()
    {
        $this->load->helper('download');
        force_download('./' . $this->session->userdata('export_file'), null);
    }

    function trx_tracedo_simirah()
    {
        //  var_dump($this->input->post('no_dokumen'));

        $this->data['title'] = 'Tracing DO Minyak Goreng SIMIRAH';
        $this->data['menu'] = 'trx_tracedo_simirah';
        $this->data['js'] = 'trx_tracedo_simirah_js';

        $this->data['no_dokumen'] = $this->input->post('no_dokumen');
        $api = array(
            'endpoint' => 'getTrxProdusenCPOSimirah',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'no_dokumen' => $this->data['no_dokumen'],
                'limit' => 1,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);
        $this->data['cpo'] = $response['data'][0];

        $this->templateGovCMS('migor/trx_tracedo_simirah', $this->data);
    }

    function getExportirSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('exportir_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('exportir_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('exportir_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $nib = $this->input->post('nib');
            $nama = $this->input->post('nama');

            $api = array(
                'endpoint' => 'getExportirSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'nib' => $nib,
                    'nama' => $nama,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['exportir_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('exportir_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getProdusenSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('produsen_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('produsen_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('produsen_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (empty($this->input->post('filter'))) {
                    if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || $this->session->userdata('role') == 'Kontributor SP2KP (Provinsi)') {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                    } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || $this->session->userdata('role') == 'Kontributor SP2KP (Kabupaten/Kota)') {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                    }
                }
            }

            $nib = $this->input->post('nib');
            $nama = $this->input->post('nama');

            $api = array(
                'endpoint' => 'getProdusenSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'nib' => $nib,
                    'nama' => $nama,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['produsen_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                if (!empty($this->input->post('selectize'))) {
                    echo json_encode($this->session->userdata('produsen_simirah')[$pagenumber]['data']);
                } else {
                    echo json_encode($this->session->userdata('produsen_simirah')[$pagenumber]);
                }
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getD1SIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('d1_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('d1_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('d1_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (empty($this->input->post('filter'))) {
                    if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                    } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                    }
                }
            }

            $nib = $this->input->post('nib');
            $nama = $this->input->post('nama');
            $api = array(
                'endpoint' => 'getD1Simirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'nib' => $nib,
                    'nama' => $nama,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['d1_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                if (!empty($this->input->post('selectize'))) {
                    echo json_encode($this->session->userdata('d1_simirah')[$pagenumber]['data']);
                } else {
                    echo json_encode($this->session->userdata('d1_simirah')[$pagenumber]);
                }
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getD2SIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('d2_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('d2_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('d2_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (empty($this->input->post('filter'))) {
                    if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                    } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                    }
                }
            }

            $nib = $this->input->post('nib');
            $nama = $this->input->post('nama');
            $api = array(
                'endpoint' => 'getD2Simirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'nib' => $nib,
                    'nama' => $nama,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['d2_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                if (!empty($this->input->post('selectize'))) {
                    echo json_encode($this->session->userdata('d2_simirah')[$pagenumber]['data']);
                } else {
                    echo json_encode($this->session->userdata('d2_simirah')[$pagenumber]);
                }
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getPengecerSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pengecer_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pengecer_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('pengecer_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (empty($this->input->post('filter'))) {
                    if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                    } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                        $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                    }
                }
            }

            $nik = $this->input->post('nik');
            $nama = $this->input->post('nama');
            $api = array(
                'endpoint' => 'getPengecerSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'nik' => $nik,
                    'nama' => $nama,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pengecer_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                if (!empty($this->input->post('selectize'))) {
                    echo json_encode($this->session->userdata('pengecer_simirah')[$pagenumber]['data']);
                } else {
                    echo json_encode($this->session->userdata('pengecer_simirah')[$pagenumber]);
                }
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getTrxCooperationSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_cooperation_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_cooperation_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('trx_cooperation_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $no_dokumen = $this->input->post('no_dokumen');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');

            $api = array(
                'endpoint' => 'getTrxCooperationSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'status' => $status,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'type' => $type,
                    'nama_pengirim' => $nama_pengirim,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['trx_cooperation_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('trx_cooperation_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getTrxProdusenCpoSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_produsencpo_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_produsencpo_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah')) && empty($this->input->post('export'))) {
            $session_existing = $this->session->userdata('trx_produsencpo_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $no_dokumen = $this->input->post('no_dokumen');
            $nib = $this->input->post('nib');
            $nib_penerima = $this->input->post('nib_penerima');
            $nib_exportir = $this->input->post('nib_exportir');
            $no_dokumen_parent = $this->input->post('no_dokumen_parent');
            $no_dokumen_cooperation = $this->input->post('no_dokumen_cooperation');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');
            $api = array(
                'endpoint' => 'getTrxProdusenCPOSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'nib' => $nib,
                    'nib_exportir' => $nib_exportir,
                    'nib_penerima' => $nib_penerima,
                    'no_dokumen_parent' => $no_dokumen_parent,
                    'no_dokumen_cooperation' => $no_dokumen_cooperation,
                    'status' => $status,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'type' => $type,
                    'nama_pengirim' => $nama_pengirim,
                    'limit' => (($this->input->post('export') == "true") ? 100000000000 : $this->input->post('length')),
                    'offset' => ((empty($this->input->post('start')) || $this->input->post('export') == "true") ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                if ($this->input->post('export') == "true") {
                    $this->mirah_trx_export($response['data'], 'Produsen CPO ke Produsen Migor', $tgl_awal, $tgl_akhir);
                } else {
                    $session = $this->session->all_userdata();
                    $session['trx_produsencpo_simirah'][$pagenumber] = $response;
                    $this->session->set_userdata($session);
                    $this->session->set_userdata('export_status', 'success');
                    echo json_encode($this->session->userdata('trx_produsencpo_simirah')[$pagenumber]);
                }
            } else {
                if ($this->input->post('export') == "true") {
                    $this->session->set_userdata('export_status', 'fail');
                } else {
                    echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
                }
            }
        }
    }

    function getTrxProdusenMigorSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_produsenmigor_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_produsenmigor_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah')) && empty($this->input->post('export'))) {
            $session_existing = $this->session->userdata('trx_produsenmigor_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $nib = $this->input->post('nib');
            $nib_penerima = $this->input->post('nib_penerima');
            $nib_exportir = $this->input->post('nib_exportir');
            $no_dokumen = $this->input->post('no_dokumen');
            $no_dokumen_parent = $this->input->post('no_dokumen_parent');
            $no_dokumen_cooperation = $this->input->post('no_dokumen_cooperation');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');


            $api = array(
                'endpoint' => 'getTrxProdusenMigorSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'nib' => $nib,
                    'nib_penerima' => $nib_penerima,
                    'nib_exportir' => $nib_exportir,
                    'no_dokumen_parent' => $no_dokumen_parent,
                    'no_dokumen_cooperation' => $no_dokumen_cooperation,
                    'status' => $status,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'type' => $type,
                    'nama_pengirim' => $nama_pengirim,
                    'limit' => (($this->input->post('export') == "true") ? 100000000000 : $this->input->post('length')),
                    'offset' => ((empty($this->input->post('start')) || $this->input->post('export') == "true") ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                if ($this->input->post('export') == "true") {
                    $this->mirah_trx_export($response['data'], 'Produsen Migor ke Distributor (D1)', $tgl_awal, $tgl_akhir);
                } else {
                    foreach ($response['data'] as $k => $v) {
                        $response['data'][$k]['waktu_terbit'] = date('Y-m-d H:i:s', strtotime($v['waktu_terbit']));
                    }

                    $session = $this->session->all_userdata();
                    $session['trx_produsenmigor_simirah'][$pagenumber] = $response;
                    $this->session->set_userdata($session);
                    echo json_encode($this->session->userdata('trx_produsenmigor_simirah')[$pagenumber]);
                }
            } else {
                if ($this->input->post('export') == "true") {
                    $this->session->set_userdata('export_status', 'fail');
                } else {

                    echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
                }
            }
        }
    }

    function getTrxD1SIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_d1_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_d1_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah')) && empty($this->input->post('export'))) {
            $session_existing = $this->session->userdata('trx_d1_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $no_dokumen = $this->input->post('no_dokumen');
            $nib = $this->input->post('nib');
            $nib_penerima = $this->input->post('nib_penerima');
            $no_dokumen_parent = $this->input->post('no_dokumen_parent');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');
            $jenis_penerima = $this->input->post('jenis_penerima');

            $api = array(
                'endpoint' => 'getTrxD1Simirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'nib' => $nib,
                    'nib_penerima' => $nib_penerima,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'jenis_penerima' => $jenis_penerima,
                    'no_dokumen_parent' => $no_dokumen_parent,
                    'status' => $status,
                    'type' => $type,
                    'nama_pengirim' => $nama_pengirim,
                    'limit' => (($this->input->post('export') == "true") ? 100000000000 : $this->input->post('length')),
                    'offset' => ((empty($this->input->post('start')) || $this->input->post('export') == "true") ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                if ($this->input->post('export') == "true") {
                    $this->mirah_trx_export($response['data'], 'Distributor (D1) ke D2 atau Pengecer', $tgl_awal, $tgl_akhir);
                } else {
                    $session = $this->session->all_userdata();
                    $session['trx_d1_simirah'][$pagenumber] = $response;
                    $this->session->set_userdata($session);
                    echo json_encode($this->session->userdata('trx_d1_simirah')[$pagenumber]);
                }
            } else {
                if ($this->input->post('export') == "true") {
                    $this->session->set_userdata('export_status', 'fail');
                } else {
                    echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
                }
            }
        }
    }

    function getTrxD2SIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_d2_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_d2_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('trx_d2_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $no_dokumen = $this->input->post('no_dokumen');
            $nib = $this->input->post('nib');
            $no_dokumen_parent = $this->input->post('no_dokumen_parent');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');

            $api = array(
                'endpoint' => 'getTrxD2Simirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'no_dokumen_parent' => $no_dokumen_parent,
                    'status' => $status,
                    'type' => $type,
                    'nama_pengirim' => $nama_pengirim,
                    'nib_pengirim' => $nib,
                    'limit' => (($this->input->post('export') == "true") ? 100000000000 : $this->input->post('length')),
                    'offset' => ((empty($this->input->post('start')) || $this->input->post('export') == "true") ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                if ($this->input->post('export') == "true") {
                    $this->mirah_trx_export($response['data'], 'Distributor (D2) ke Pengecer', $tgl_awal, $tgl_akhir);
                } else {
                    $session = $this->session->all_userdata();
                    $session['trx_d2_simirah'][$pagenumber] = $response;
                    $this->session->set_userdata($session);
                    echo json_encode($this->session->userdata('trx_d2_simirah')[$pagenumber]);
                }
            } else {
                if ($this->input->post('export') == "true") {
                    $this->session->set_userdata('export_status', 'fail');
                } else {
                    echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
                }
            }
        }
    }

    function getTrxPengecerSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('trx_pengecer_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('trx_pengecer_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('trx_pengecer_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $no_dokumen = $this->input->post('no_dokumen');
            $no_dokumen_parent = $this->input->post('no_dokumen_parent');
            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = $this->input->post('type');

            $api = array(
                'endpoint' => 'getTrxPengecerSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'no_dokumen' => $no_dokumen,
                    'no_dokumen_parent' => $no_dokumen_parent,
                    'status' => $status,
                    'type' => $type,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'nama_penjual' => $nama_pengirim,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['trx_pengecer_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('trx_pengecer_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getSummarySIMIRAH()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('summary_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('summary_simirah')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah'))) {
            $session_existing = $this->session->userdata('summary_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $status = $this->input->post('status');
            $nama_pengirim = urldecode($this->input->post('nama_pengirim'));
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $type = urldecode($this->input->post('type'));

            $api = array(
                'endpoint' => 'getSummarySimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'platform' => $this->input->post('platform'),
                    'kode_daerah' => $kode_daerah,
                    'status' => $status,
                    'type' => $type,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'nama_pengirim' => $nama_pengirim,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );


            if ($this->input->post('alokasi') == "true") {
                $api['post_field']['alokasi'] = $this->input->post('alokasi');
                $api['post_field']['email'] = $this->session->userdata('email');
                $api['post_field']['limit'] = 1;
                $api['post_field']['offset'] = 0;
            }

            //var_dump($api);die();

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {

                if ($this->input->post('alokasi') == 'true') {
                    echo json_encode($response);
                } else {
                    $session = $this->session->all_userdata();
                    $session['summary_simirah'][$pagenumber] = $response;
                    $this->session->set_userdata($session);
                    echo json_encode($this->session->userdata('summary_simirah')[$pagenumber]);
                }
            } else {
                if ($this->input->post('alokasi') == 'true') {
                    echo json_encode($response);
                } else {
                    echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
                }
            }
        }
    }

    function getPeriodeAlokasiSIMIRAH()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('periode_alokasi_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('periode_alokasi_simirah')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('periode_alokasi_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $periode_awal = $this->input->post('periode_awal');
            $periode_akhir = $this->input->post('periode_akhir');

            $api = array(
                'endpoint' => 'getPeriodeAlokasiDMO',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'periode_awal' => $periode_awal,
                    'periode_akhir' => $periode_akhir,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            //var_dump($response);die();
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['periode_alokasi_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('periode_alokasi_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getAlokasiPerusahaanSIMIRAH()
    {
        if (!empty($this->session->userdata('export_file'))) {
            if (file_exists('./' . $this->session->userdata('export_file'))) {
                unlink('./' . $this->session->userdata('export_file'));
            }
        }
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('alokasi_perusahaan_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('alokasi_perusahaan_simirah')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('alokasi_perusahaan_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $nib = $this->input->post('perusahaan_nib');
            $nama = $this->input->post('perusahaan_nama');
            $periode_awal = $this->input->post('perusahaan_periode_awal');
            $periode_akhir = $this->input->post('perusahaan_periode_akhir');

            $api = array(
                'endpoint' => 'getAlokasiPerusahaan',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'nib' => $nib,
                    'nama' => $nama,
                    'tgl_awal' => $periode_awal,
                    'tgl_akhir' => $periode_akhir,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            //var_dump($response);die();
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['alokasi_perusahaan_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('alokasi_perusahaan_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getSummaryAlokasiSIMIRAH()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('summary_alokasi_simirah')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('summary_alokasi_simirah')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('summary_alokasi_simirah')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $tgl_awal = $this->input->post('tgl_awal');
            $tgl_akhir = $this->input->post('tgl_akhir');
            $series = $this->input->post('series');
            $value_only = ($this->input->post('value_only') == 'true') ? true : null;

            $api = array(
                'endpoint' => 'getSummaryAlokasiSimirah',
                'method' => 'POST',
                'controller' => 'migor',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'tgl_awal' => $tgl_awal,
                    'tgl_akhir' => $tgl_akhir,
                    'series' => $series,
                    'value_only' => $value_only,
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            //var_dump($response);die();
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['summary_alokasi_simirah'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('summary_alokasi_simirah')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function kirimAlokasi()
    {

        $alokasi_id = $this->input->post('alokasi_id');
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $kirim_output = array('kode' => 400);
        if ($tgl_awal == '' || $tgl_akhir == '') {
            echo json_encode($kirim_output);
            die();
            $alokasi_id = 48;
            $tgl_awal = '2022-06-01';
            $tgl_akhir = '2022-09-06';
        }

        $api = array(
            'endpoint' => 'getSummaryAlokasiSimirah',
            'method' => 'POST',
            'controller' => 'migor',
            'post_field' => array(
                'tgl_awal' => $tgl_awal,
                'tgl_akhir' => $tgl_akhir,
                'series' => 2,
                'value_only' => 'true',
                'alokasi_kirim' => 'true',
                'limit' => 10000,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $data_alokasi = array();
            $data_payload_group = array();
            $periode_alokasi_id = '';
            foreach ($response['data'] as $k => $v) {
                if ($periode_alokasi_id == '') {
                    $periode_alokasi_id = $v['alokasi_id'];
                }
                $data_alokasi[] = array(
                    'kdIjin' => $v['kd_izin'],
                    'nib' => $v['nib'],
                    'namaPerusahaan' => $v['nama'],
                    'jumlahDmo' => $v['jml'],
                    'kdSatuan' => $v['satuan'],
                    'keterangan' => ''
                );
                $data_payload_group[$v['type']][] = array(
                    'kdIjin' => $v['kd_izin'],
                    'nib' => $v['nib'],
                    'namaPerusahaan' => $v['nama'],
                    'jumlahDmo' => $v['jml'],
                    'kdSatuan' => $v['satuan'],
                    'keterangan' => ''
                );
            }
            $final_data_alokasi = array(
                'noKeputusan' => $alokasi_id . '/SISP' . '/' . date('m') . '/' . date('Y'),
                'tglKeputusan' => date('Y-m-d'),
                'dataDmo' => $data_alokasi
            );

            $curl = curl_init();

            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_URL => 'https://ws.kemendag.go.id/insw/sendAlokasiDMOMigor',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => json_encode($final_data_alokasi),
                    CURLOPT_HTTPHEADER => array(
                        'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
                        'Content-Type: application/json'
                    ),
                )
            );

            $response_output = curl_exec($curl);

            curl_close($curl);

            $response_output = json_decode($response_output);
            if ($response_output->code == 01) {
                $api = array(
                    'endpoint' => 'setAlokasiKirim',
                    'method' => 'POST',
                    'controller' => 'migor',
                    'post_field' => array(
                        'alokasi_id' => $alokasi_id,
                        'status_kirim' => 1,
                        'request_body' => json_encode($final_data_alokasi),
                        'sendon' => date('Y-m-d H:i:s'),
                        'request_output' => json_encode($response_output),
                        'payload' => json_encode($data_payload_group),
                        'email' => $this->session->userdata('email'),
                        'periode_alokasi_id' => $periode_alokasi_id,
                    )
                );

                $response = $this->serviceAPI($api);
                $kirim_output = array('kode' => 200);
            } else {
                $api = array(
                    'endpoint' => 'setAlokasiKirim',
                    'method' => 'POST',
                    'controller' => 'migor',
                    'post_field' => array(
                        'alokasi_id' => $alokasi_id,
                        'status_kirim' => 99,
                        'request_body' => json_encode($final_data_alokasi),
                        'sendon' => date('Y-m-d H:i:s'),
                        'request_output' => json_encode($response_output),
                        'payload' => json_encode($data_payload_group),
                        'email' => $this->session->userdata('email'),
                        'periode_alokasi_id' => $periode_alokasi_id,
                    )
                );

                $response = $this->serviceAPI($api);
            }
        }

        echo json_encode($kirim_output);
    }

    function deleteAlokasi()
    {
        $post_field = array(
            'token' => $this->token,
            'email' => $this->session->userdata('email'),
            'db' => 'mirah',
            'table_name' => 'alokasi',
            'key_name' => 'alokasi_id',
            'key' => $this->input->post('alokasi_id'),
            'status' => 'Deleted',
            'keterangan_perubahan' => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
        );

        $api = array(
            'endpoint' => 'setStatus',
            'controller' => '',
            'method' => 'POST',
            'post_field' => $post_field
        );

        $response = $this->serviceAPI($api);

        echo json_encode($response);
    }
}
