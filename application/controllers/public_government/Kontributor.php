<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kontributor extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = 'Management Kontributor';
        $this->data['js']     = 'kontributor_js';
        $this->data['menu'] = 'kontributor';

        $this->templateGovCMS('kontributor/index', $this->data);
    }

    public function kontributor_json()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('kontributor')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('kontributor')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('kontributor')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPengguna',
                'method'        => 'POST',
                'post_field'    => array('token' => $this->token, 'tipe_pengguna' => 'Kontributor', 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')))
            );

            if($this->input->post('status') != ' ')
                $api['post_field']['status'] = $this->input->post('status');

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['kontributor'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('kontributor')[$pagenumber]);
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_pengguna()
    {
        $this->form_validation->set_rules('tipe_pengguna', 'Tipe Pengguna', 'required|trim');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

        if (empty($this->input->post('pengguna_id'))) {
            $this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]|min_length[8]');
            $this->form_validation->set_rules('password_confirm', 'Password', 'required|matches[password]|min_length[8]');
        }

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            redirect(base_url() . 'pengguna');
        }
        $post_field = array(
            'token' => $this->token,
            'pengguna_id' => $this->input->post('pengguna_id', true),
            'nama_lengkap' => $this->input->post('nama_lengkap', true),
            'nip' => $this->input->post('nip', true),
            'email' => $this->input->post('email', true),
            'tipe_pengguna' => $this->input->post('tipe_pengguna', true),
            'no_hp' => $this->input->post('no_hp', true),
            'password' => $this->input->post('password', true),
            'confirm_password' => $this->input->post('password_confirm', true),
            'alamat' => $this->input->post('alamat', true),
            'daerah_id' => $this->input->post('kecamatan', true),
            'token' => $this->token
        );

        $api = array(
            'endpoint'      => 'daftar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {

            $this->session->unset_userdata('pengguna');

            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $response['keterangan']);
        }
        redirect(base_url() . 'pengguna');
    }

    public function approve()
    {
        $response = array('success' => false, 'message' => '');

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');
        $data = $this->session->userdata('kontributor')[$pagenumber]['data'][$this->input->post('action_index')];

        $api = array(
            'endpoint'      => 'setStatus',
            'method'        => 'POST',
            'post_field'    => array(
                'token' => $this->token,
                'email' => $data['email'],
                'table_name'    => 'tbl_pengguna',
                'key'   => $data['email'],
                'key_name'  => 'email',
                'status'    => 'Aktif',
                'keterangan_perubahan'  => 'Verifikasi Kontributor oleh Bapokting'
            )
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {

            $api = array(
                'endpoint'  => 'sendMail',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'email' => $data['email'],
                    'subject'   => 'Verifikasi Email Kontributor',
                    'template'  => 'email/veriffikasi_kontributor',
                    'data[nama]'  => $data['nama_lengkap']
                )
            );

            $service = $this->serviceAPI($api);

            $response['success'] = true;
        } else {
            $response['message'] = $service['keterangan'];
        }

        echo json_encode($response);
    }

    public function assign_json()
    {
        $response = array('userID' => '', 'email' => '', 'data_pasar' => array(), 'pasar_id' => array());

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');
        $data = $this->session->userdata('kontributor')[$pagenumber]['data'][$this->input->post('action_index')];

        $api = array(
            'endpoint'  => 'getContributorAssign',
            'method'        => 'POST',
            'post_field'    => array(
                'token' => $this->token,
                'pengguna_id' => $data['pengguna_id']
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            foreach ($service['data'] as $key => $row) {
                $response['pasar_id'][$key] = $row['pasar_id'];
            }

            $response['userID'] = $service['data'][0]['pengguna_id'];
            $response['email'] = $service['data'][0]['email'];
        } else {
            $response['userID'] = $data['pengguna_id'];
            $response['email'] = $data['email'];
        }

        $api = array(
            'endpoint'  => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array(
                'limit' => 600,
                'offset' => 0,
                'status'    => 'Aktif',
                'daerah_id' => substr($data['daerah_id'], 0, 4)
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            $response['data_pasar'] = $service['data'];
        }

        echo json_encode($response);
    }

    public function save()
    {
        $response = array('data' => array(), 'success' => false);

        if (!empty($this->input->post('assign_pasar'))) {

            $api = array(
                'endpoint'  => 'setContributorAssign',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'pengguna_id' => $this->input->post('pengguna_id'),
                    'email'    => $this->input->post('email')
                )
            );

            foreach ($this->input->post('assign_pasar') as $key => $value) {
                $api['post_field']['pasar_id[' . $key . ']'] = $value;
            }

            $service = $this->serviceAPI($api);

            if ($service && $service['kode'] == 200) {
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }
}
