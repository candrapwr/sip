<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();

        if (strtolower($this->session->userdata('role')) == 'bapokting') {
            redirect(site_url() . 'dashboard/perdagangan/absensi');
        }

        $current_year = date('Y');
        $latest_year = $current_year - 5;
        $this->years = [];
        for ($i = $current_year; $i >= $latest_year; $i--) {
            $this->years[] = $i;
        }
    }

    public function index()
    {

        $this->data['js'] = 'monitoring_tabel_js';
        $this->data['years'] = $this->years;

        $this->templateGovCMS('monitoring/tabel_pemantauan', $this->data);
    }

    public function tabel_ajax()
    {
        if ($this->input->post('post_year') != '' && $this->input->post('daerah_id') == '') {
            $api = array(
                'endpoint'      => 'getSummaryProduksiKonsumsi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'tahun' => $this->input->post('post_year'))
            );

            $response = @$this->serviceAPI($api);
            @$this->data['data'] = $response['data'];
        } elseif ($this->input->post('post_year') != '' && $this->input->post('daerah_id') != '') {
            $api = array(
                'endpoint'      => 'getSummaryProduksiKonsumsi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'tahun' => $this->input->post('post_year'), 'daerah_id' => $this->input->post('daerah_id'))
            );

            $response = @$this->serviceAPI($api);
            @$this->data['data'] = $response['data'];
            $this->data['vInput'] = $this->cekNilai($this->data['data'][0]['jan']) + $this->cekNilai($this->data['data'][0]['feb']) + $this->cekNilai($this->data['data'][0]['mar']) + $this->cekNilai($this->data['data'][0]['apr']) + $this->cekNilai($this->data['data'][0]['mei']) + $this->cekNilai($this->data['data'][0]['jun']) + $this->cekNilai($this->data['data'][0]['jul']) + $this->cekNilai($this->data['data'][0]['agu']) + $this->cekNilai($this->data['data'][0]['sep']) + $this->cekNilai($this->data['data'][0]['okt']) + $this->cekNilai($this->data['data'][0]['nov']) + $this->cekNilai($this->data['data'][0]['des']) ;
            $this->data['vTidakInput'] = 12 - $this->data['vInput'];
        }
        $this->load->view('backend/public_gov/contents/monitoring/ajax_tabel', $this->data);
    }

    private function cekNilai($data)
    {   
        if((int) $data > 0)
            return 1;
        else 
            return 0;
    }

    public function grafik()
    {
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_grafik_js';

        $this->templateGovCMS('monitoring/grafik_pemantauan', $this->data);
    }

    public function profil()
    {
        $this->getMaster();

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_profil_js';

        $this->templateGovCMS('monitoring/profil_pemantauan', $this->data);
    }

    public function peta()
    {
        $this->getMaster();

        $this->data['komoditi_bapok'] = $this->session->userdata('master_komoditi_bapok');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_peta_js';

        $this->templateGovCMS('monitoring/peta_pemantauan', $this->data);
    }

    public function pasar()
    {

        if (!has_access(23, 'show')) {
            redirect(base_url() . '404_override');
        }

        $this->getMaster();

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_kelengkapanpasar_js';

        $this->templateGovCMS('monitoring/kelengkapan_pasar', $this->data);
    }


    public function datapasar()
    {
        if (!has_access(22, 'show')) {
            redirect(base_url() . '404_override');
        }

        $this->getMaster();

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_datapasar_js';

        $this->templateGovCMS('monitoring/data_pasar', $this->data);
    }

    private function getMaster()
    {
        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        if (!$this->session->userdata('master_komoditi_bapok')) {
            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'method'        => 'POST',
                'controller'    => 'pasar'
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                foreach ($response['data'] as $key => $row) {
                    if ($row['kelompok'] == 'Bapok') {
                        $komoditi_bapok[] = array(
                            'id' => $row['jenis_komoditi_id'],
                            'name' => $row['jenis_komoditi']
                        );
                    }
                }
                $this->session->set_userdata('master_komoditi_bapok', $komoditi_bapok);
            }
        }
    }

    public function json($str)
    {
        if ($str == 'getdata') {
            $api = array(
                'endpoint'      => 'getSummaryProduksiKonsumsi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'tahun' => $this->input->post('post_year'))
            );

            $response = @$this->serviceAPI($api);

            $arr_series = [];
            $arr_input =  array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            $arr_notinput =  array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            if ($response) {
                foreach ($response['data'] as $value) {
                    if ($value['jan'] != "0") $arr_input[0]++;
                    else $arr_notinput[0]++;
                    if ($value['feb'] != "0") $arr_input[1]++;
                    else $arr_notinput[1]++;
                    if ($value['mar'] != "0") $arr_input[2]++;
                    else $arr_notinput[2]++;
                    if ($value['apr'] != "0") $arr_input[3]++;
                    else $arr_notinput[3]++;
                    if ($value['mei'] != "0") $arr_input[4]++;
                    else $arr_notinput[4]++;
                    if ($value['jun'] != "0") $arr_input[5]++;
                    else $arr_notinput[5]++;
                    if ($value['jul'] != "0") $arr_input[6]++;
                    else $arr_notinput[6]++;
                    if ($value['agu'] != "0") $arr_input[7]++;
                    else $arr_notinput[7]++;
                    if ($value['sep'] != "0") $arr_input[8]++;
                    else $arr_notinput[8]++;
                    if ($value['okt'] != "0") $arr_input[9]++;
                    else $arr_notinput[9]++;
                    if ($value['nov'] != "0") $arr_input[10]++;
                    else $arr_notinput[10]++;
                    if ($value['des'] != "0") $arr_input[11]++;
                    else $arr_notinput[11]++;
                }
            }

            $arr_series['input'] = $arr_input;
            $arr_series['notinput'] = $arr_notinput;
            $arr_series['tahun'] = $this->input->post('post_year');

            $json_out = json_encode($arr_series);
            echo $json_out;

        }elseif ($str == 'getpasar') {

            $api = array(
                'endpoint'      => 'getSummaryPasar',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'daerah_id' => $this->input->post('post_daerah'))
            );

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;

        }elseif ($str == 'profil') {

            $api = array(
                'endpoint'      => 'getSummaryStokKomoditi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'daerah_id' => $this->input->post('post_daerah'), 'tahun' => $this->input->post('post_year'), 'kelompok' => $this->input->post('post_kelompok'))
            );

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 404;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;
        }elseif ($str == 'peta') {

            $api = array(
                'endpoint'      => 'getSummaryStokKomoditi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'tahun' => $this->input->post('post_year'), 'jenis_komoditi_id' => $this->input->post('post_komoditi'))
            );

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;
        }elseif ($str == 'datapasar') {

            $start_date = $this->input->post('post_year') . '-' . $this->input->post('post_month') . '-01';
            $end_date = $this->input->post('post_year') . '-' . $this->input->post('post_month') . '-31';

            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'token' => $this->token, 
                    'daerah_id' => $this->input->post('post_daerah'), 
                    'limit' => $this->input->post('length'), 
                    'offset' => $this->input->post('start'), 
                    'start_date' => $start_date, 
                    'end_date' => $end_date
                )
            );

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $response['draw'] = $this->input->post('draw');
                } elseif ($response['kode'] == 404) {
                    $response = array(
                        'draw' => 1,
                        'recordsTotal' => 0,
                        'recordsFiltered' => 0,
                        'data'  => []
                    );
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($response);
            echo $json_out;

        }elseif ($str == 'datapasar_export') {

            $start_date = $this->input->post('post_year') . '-01-01';
            $end_date = $this->input->post('post_year') . '-12-31';

            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('token' => $this->token, 'daerah_id' => $this->input->post('post_daerah'), 'limit' => 100000, 'offset' => 0)
            );

            if($this->input->post('post_year') != ''){
                $api['post_field']['start_date'] = $start_date;
                $api['post_field']['end_date'] = $end_date;
            }

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $response['draw'] = $this->input->post('draw');
                } elseif ($response['kode'] == 404) {
                    $response = array(
                        'draw' => 1,
                        'recordsTotal' => 0,
                        'recordsFiltered' => 0,
                        'data'  => []
                    );
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($response);
            echo $json_out;

        }elseif ($str == 'kelengkapanpasar_export') {

            $api = array(
                'endpoint'      => 'getSummaryPasar',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token)
            );

            if(!empty($this->input->post('post_daerah'))){
                $api['post_field']['daerah_id'] = $this->input->post('post_daerah');
            }

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;

        }elseif ($str == 'tabel_export') {

            $api = array(
                'endpoint'      => 'getSummaryProduksiKonsumsi',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'tahun' => $this->input->post('post_year'))
            );

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;

        }elseif ($str == 'produksikonsumsi_export') {

            if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'sardislog'){
                $daerah_id = $this->input->post('post_daerah');
            }else{
                $daerah_id = substr($this->session->userdata('daerah_id'), 0, 2);
            }

            $api = array(
                'endpoint'      => 'getStok',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'limit' => 1000000, 'offset' => 0, 'email' => $this->session->userdata('email'), 'daerah_id' => $daerah_id, 'tahun' => $this->input->post('post_year'))
            );

            if($this->input->post('post_bulan') != '-'){
                $api['post_field']['bulan'] = $this->input->post('post_bulan');
            }

            $response = @$this->serviceAPI($api);

            if ($response) {
                if ($response['kode'] == 200) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = $response['data'];
                } elseif ($response['kode'] == 404) {
                    $dataOut['kode'] = 200;
                    $dataOut['data'] = [];
                } else {
                    $dataOut['kode'] = 403;
                }
            } else {
                $dataOut['kode'] = 403;
            }

            $json_out = json_encode($dataOut);
            echo $json_out;
        }
    }


    public function inputHarga()
    {

        if (!has_access(21, 'show')) {
            redirect(base_url() . '404_override');
        }

        $this->data['title']  = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'dashboard_js';

        $current_year = date('Y');
        $latest_year = $current_year - 5;

        $years = [];

        for ($i = $current_year; $i >= $latest_year; $i--) {
            $years[] = $i;
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $years;
        // $this->data['jenis_komoditi'] = $data_jenis_komoditi;

        $this->templateGovCMS('dashboard/index', $this->data);
    }

    public function inputHargaJSON()
    {
        $response = array('success' => false, 'result' => array('data' => array(), 'date'), 'headTitle' => '', 'title' => '', 'code' => 200);

        $start_date = date('Y') . '-' . date('m') . '-01';
        $end_date = date("Y-m-t", strtotime($start_date));

        $start_day = date('d', strtotime($start_date));
        $end_day = date('d', strtotime($end_date));

        $daerah_id = 31;
        $province_title = 'Provinsi DKI Jakarta';

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == 'kontributor sp2kp (provinsi)') {
            $daerah_id = substr($this->session->userdata('daerah_id'), 0, 2);
            $province_title = 'Provinsi '.$this->session->userdata('provinsi');
        }

        if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'kontributor sp2kp (kabupaten/kota)') {
            $daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
            $province_title = $this->session->userdata('kab_kota');
        }

        if (!empty($this->input->post('province_id'))) {
            $split_province = explode('|', $this->input->post('province_id'));
            $daerah_id = $split_province[0];
            $province_title = $split_province[1];

            if(!empty($this->input->post('post_kab'))){
                $split_province = explode('|', $this->input->post('post_kab'));
                $daerah_id = $split_province[0];
                $province_title = $split_province[1];
            }
        }

        $response['result']['title'] = $province_title;

        if (!empty($this->input->post('month')) && !empty($this->input->post('year'))) {
            $start_date = $this->input->post('year') . '-' . $this->input->post('month') . '-01';
            $end_date = date("Y-m-t", strtotime($start_date));

            $start_date = date('Y-m-d', strtotime($start_date));

            $start_day = date('d', strtotime($start_date));
            $end_day = date('d', strtotime($end_date));
        }

        $api = array(
            'endpoint'      => 'getSummaryHargaPasar',
            'method'        => 'POST',
            'controller'    => 'data',
            'post_field'    => array(
                'token' => $this->token,
                'daerah_id' => $daerah_id,
                'start_date'    => $start_date,
                'end_date'  => $end_date
            )
        );

        $service = @$this->serviceAPI($api);

        $day_array = [];

        for ($i = $start_day; $i <= $end_day; $i++) {
            $day_array[] = (int) $i;
        }

        $response['result']['date'] = $day_array;

        $response['result']['headTitle'] = date_indo($start_date, 'm Y');

        if (!$service) {
            $response['code'] = 401;
        } else if ($service && $service['kode'] == 200) {
            $response['result']['data'] = $service['data'];
            $response['success'] = true;
        } else {
            $response['success'] = true;
        }

        echo json_encode($response);
    }

    public function statbapok()
    {
        $this->getMaster();

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_stat_bapok_js';

        $this->templateGovCMS('monitoring/statistik_bapok', $this->data);
    }

    public function statbating()
    {
        $this->getMaster();

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $this->years;
        $this->data['js'] = 'monitoring_stat_bating_js';

        $this->templateGovCMS('monitoring/statistik_bating', $this->data);
    }

    function rekap_pasar(){

    }
}
