<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->is_login();
	}

	public function index()
	{
		redirect(base_url() . 'e-komplain');
	}

	public function detail($id = null)
	{
		if ($id != null) {
			if (array_key_exists($id, $this->session->userdata('temp_complaints'))) {


				$this->data['detail_complaint']  = $this->session->userdata('temp_complaints')[$id];

				//iNiT Layout NeW SiSP
				$this->data['title'] = 'Detail E-Komplain';
				$this->data['menu'] = 'detailekomplain';
				$this->data['subtitle'] = 'Berikut adalah Halaman Detail E-Komplain SISP Keementerian Perdagangan Republik Indonesia';
				$this->data['namafile'] = 'ekomplain_detail';
				$this->data['css'] = false;
				$this->data['js'] = true;
				$this->load->view('frontend/structure/index', $this->data);
			} else {
				redirect('404_override');
			}
		} else {
			redirect('404_override');
		}
	}

	public function save()
	{
		$response = array('success' => false, 'validation' => null, 'code' => 200);

		$this->form_validation->set_rules('status', 'Status', 'required|trim');
		$this->form_validation->set_rules('tanggapan', 'Tanggapan', 'required|trim');
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if (!$this->form_validation->run()) {
			foreach ($this->input->post() as $key => $value) {
				$response['validation'][$key] = form_error($key);
			}
		} else {
			$status = 'CLOSE';

			if ($this->input->post('status') == 1) {
				$status = 'OPEN';
			} else if ($this->input->post('status') == 2) {
				$status = 'RESOLVED';
			}

			$post_field = array(
				'no_tiket' => $this->input->post('tiket'),
				'status'    => $status,
				'keterangan'    => $this->input->post('tanggapan'),
				'email' => $this->session->userdata('email'),
				'flag_pengelola'    => 'Y'
			);

			$api = array(
				'endpoint'      => 'setTracking',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => $post_field
			);

			$response_pasar = $this->serviceAPI($api);

			if (!$response_pasar) {
				$response['code'] = 401;
			} else {
				$api = array(
					'endpoint'      => 'getKomplain',
					'method'        => 'POST',
					'controller'    => 'pasar',
					'post_field'    => array('no_tiket' => $this->input->post('tiket'))
				);

				$get_komplain = $this->serviceAPI($api);

				if ($get_komplain['kode'] == 200) {
					$api = array(
						'endpoint'  => 'sendMail',
						'method'        => 'POST',
						'post_field'    => array(
							'email' => $get_komplain['data'][0]['email'],
							'subject'   => 'Pemberitahuan Tanggapan Komplain',
							'template'  => 'email/notifikasi_komplain_pemohon',
							'data[ticket]'  => $get_komplain['data'][0]['no_tiket'],
							'data[nama_penerima]'  => $get_komplain['data'][0]['nama'],
							'data[tanggapan]'  => $this->input->post('tanggapan', true),
							'data[nama_pasar]'  => $get_komplain['data'][0]['nama_pasar'],
							'data[url]'  => slugify($get_komplain['data'][0]['komplain'], $get_komplain['data'][0]['komplain_id'])
						)
					);

					$send_mail_petugas = $this->serviceAPI($api);

					$response['data'] = array(
						'status' => $status,
						'keterangan'    => $this->input->post('tanggapan'),
						'email' => $this->session->userdata('email'),
						'email_in' => substr($this->session->userdata('email'), 0, 1),
						'date'  => full_date(date('Y-m-d'))
					);

					$response['success'] = true;
				}
			}
		}

		echo json_encode($response);
	}

	public function getTracking()
	{
		$response = array('data' => array());

		$id = $this->input->post('id', true);

		if ($id != null) {
			if ($this->session->userdata('temp_complaints')) {
				$no_ticket = $this->session->userdata('temp_complaints')[$id]['no_tiket'];

				$api = array(
					'endpoint'      => 'getKomplain',
					'method'        => 'POST',
					'controller'    => 'pasar',
					'post_field'    => array('no_tiket' => $no_ticket)
				);

				$service = $this->serviceAPI($api);

				if ($service['kode'] == 200) {

					$tracking = [];

					foreach ($service['data'][0]['tracking'] as $key => $row) {
						if ($key != 0) {

							$keterangan = ($row['keterangan'] != null) ? $row['keterangan'] : '';

							$tracking[] = [
								'flag_pengelola' => $row['flag_pengelola'],
								'createdby' => $row['createdby'],
								'createdby_in' => substr($row['createdby'], 0, 1),
								'createdon' => full_date($row['createdon']),
								'keterangan' => $keterangan,
								'status' => $row['status']
							];
						}
					}

					$response['data'] = $tracking;
				}
			}
		}

		echo json_encode($response);
	}
}
