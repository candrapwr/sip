<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Perdagangan extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();

        if (strtolower($this->session->userdata('role')) == 'eksekutif') {
            redirect(site_url() . 'dashboard/perdagangan/cpo-skema-pasal-18');
        }else if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'bapokting'|| strtolower($this->session->userdata('role')) == 'kontributor' || strtolower($this->session->userdata('role')) == 'eksekutif sardislog' || strtolower($this->session->userdata('role')) == 'eksekutif bapokting') {
            redirect(site_url() . 'dashboard/perdagangan/minyak-goreng-curah');
        } else if (strtolower($this->session->userdata('role')) == 'sardislog') {
            redirect(site_url() . 'dashboard/perdagangan/monitoring-input-harga');
        } else if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
            redirect(site_url() . 'dashboard/perdagangan/produksi-konsumsi');
        }
    }   

    public function index()
    {
    }
}
