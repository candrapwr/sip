<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Harga extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->token = $this->session->userdata('token');
    }

    public function getHargaJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_harga')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        $start_date = date('Y-m-d');

        if (isset($this->session->userdata('detail_harga')[$pagenumber]) && empty($this->input->post('refresh')) && $this->input->post('start_date') == null) {
            $session_existing = $this->session->userdata('detail_harga')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if ($this->input->post('start_date') != null) {
                $start_date = $this->input->post('start_date');
            }

            $api = array(
                'endpoint'      => 'getHargaKomoditi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'createdon' => $start_date,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_harga'][$pagenumber] = $response;

                foreach ($session['detail_harga'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_harga'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
                    $session['detail_harga'][$pagenumber]['data'][$key]['harga'] = rupiah($row['harga']);

                    $session['detail_harga'][$pagenumber]['data'][$key]['action'] = false;
                    $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['detail_harga'][$pagenumber]['data'][$key]['action'] = true;
                    }

                    if ($this->session->userdata('email') == $row['createdby'] || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        if (date('Y-m-d', strtotime($row['createdon'])) == date('Y-m-d')) {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }

                        if (strtolower($this->session->userdata('role')) == 'teknis pdn') {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }
                    }
                }

                $session['detail_harga'][$pagenumber]['date'] = full_date($start_date);

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_harga')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => [],
                    'date' => full_date($start_date)
                );

                echo json_encode($response);
            }
        }
    }

    public function getHargaBahanPokok()
    {
        $response = array();

        $data = array();

        $tmp_komoditi_id = array();

        $date1 = date('Y-m-d', strtotime('last weekday'));
        $date2 =date('Y-m-d', strtotime('last weekday', strtotime('yesterday')));


        $kab = 1;

        $nama_daerah = 'Nasional';

        if ($this->input->post('provinsi', true) != null) {
            $exp = explode('|', $this->input->post('provinsi', true));
            $kab = $exp[0];

            if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                $nama_daerah = $exp[1];
            } else {
                $nama_daerah = 'Provinsi ' . $exp[1];
            }
        }

        if (!empty($this->input->post('date1'))) {
            $date1 = $this->input->post('date1', true);
        }

        if (!empty($this->input->post('date2'))) {
            $date2 = $this->input->post('date2', true);
        }

        if ($this->input->post('kab', true) != null && !empty($this->input->post('kab', true))) {
            $exp = explode('|', $this->input->post('kab', true));

            $kab = $exp[0];

            if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') {
                if (strpos($exp[1], 'kab') >= 0) {
                    $kab1 = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $exp[1]);

                    $nama_daerah = 'Kabupaten ' . $kab1;
                } else if (strpos($exp[1], 'kota') >= 0) {
                    $kab1 = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $exp[1]);

                    $nama_daerah = 'Kota ' . $kab1;
                }
            } else {
                $nama_daerah = $exp[1];
            }
        }

        $api = array(
            'endpoint'      => 'getPerbandinganHargaKomoditi',
            'method'        => 'POST',
            'controller'    => 'data',
            'post_field'    => array('daerah_id' => $kab, 'token' => '', 'start_date' => $date1, 'end_date' => $date2, 'main_filter' => true, 'kelompok' => 'Bapok')
        );

        $response_harga = $this->serviceAPI($api);

        foreach ($response_harga['data'] as $key => $row) {
            $data[] = array(
                'jenis_komoditi' => $row['varian_komoditi'],
                'satuan_komoditi'    => $row['satuan_komoditi'],
                'date1' => ($row['harga_awal'] != null) ? rupiah($row['harga_awal']) : 0,
                'date2' => ($row['harga_akhir'] != null) ? rupiah($row['harga_akhir']) : 0
            );

            $date1 = $row['tgl_awal'];
            $date2 = $row['tgl_akhir'];
        }

        foreach ($data as $key => $row) {
            if ($row['date2'] > $row['date1']) {
                $data[$key]['ket'] = 'up';
            } else if ($row['date2'] < $row['date1']) {
                $data[$key]['ket'] = 'down';
            } else if ($row['date2'] == $row['date1']) {
                $data[$key]['ket'] = 'equals';
            }

            $harga_date1 = ($row['date1'] != null) ? str_replace('.', '', $row['date1']) : 0;
            $harga_date2 = ($row['date2'] != null) ? str_replace('.', '', $row['date2']) : 0;

            if ($harga_date1 != 0 && $harga_date2 != 0) {
                $persen = ((int) $harga_date2 - (int) $harga_date1) / (int) $harga_date2;
                $data[$key]['percentage'] = round($persen * 100, 2);
            } else {
                $data[$key]['percentage'] = 100;
            }
        }

        // $api = array(
        //     'endpoint'      => 'getHargaKomoditi',
        //     'method'        => 'POST',
        //     'controller'    => 'pasar',
        //     'post_field'    => array('daerah_id' => $kab, 'token' => $this->token, 'createdon' => $date1, 'limit' => 600, 'offset' => 0, 'main_filter' => true)
        // );

        // if ($this->input->post('pasar', true) != null && !empty($this->input->post('pasar', true))) {
        //     $exp = explode('$', $this->input->post('pasar', true));

        //     $api['post_field']['pasar_id'] = $exp[0];

        //     $nama_daerah = $exp[1];
        // }

        // $response_harga1 = $this->serviceAPI($api);

        // $tmp = array();

        // $tmp_komoditi = array();

        // if ($response_harga1 && $response_harga1['kode'] == 200) {
        //     $tmp = $response_harga1['data'];
        // } else if (!$response_harga1) {
        //     $response['code'] = 401;
        // }

        // $api = array(
        //     'endpoint'      => 'getHargaKomoditi',
        //     'method'        => 'POST',
        //     'controller'    => 'pasar',
        //     'post_field'    => array('daerah_id' => $kab, 'token' => $this->token, 'createdon' => $date2, 'limit' => 600, 'offset' => 0, 'main_filter' => true)
        // );

        // if ($this->input->post('pasar', true) != null && !empty($this->input->post('pasar', true))) {
        //     $exp = explode('$', $this->input->post('pasar', true));

        //     $api['post_field']['pasar_id'] = $exp[0];

        //     $nama_daerah = $exp[1];
        // }

        // $response_harga2 = $this->serviceAPI($api);

        // $tmp2 = array();
        // $test = array();

        // if ($response_harga2 && $response_harga2['kode'] == 200) {
        //     $tmp2 = $response_harga2['data'];
        // }

        // if ($tmp != null && $tmp2 != null) {

        //     foreach ($tmp as $harga_sebelum) {
        //         if ($harga_sebelum['jenis_komoditi'] == 'Beras' && $harga_sebelum['varian_komoditi_id'] != null) {
        //             if (!in_array($harga_sebelum['varian_komoditi'], $tmp_komoditi)) {
        //                 $tmp_komoditi[] = $harga_sebelum['varian_komoditi'];
        //             }
        //         } else if ($harga_sebelum['jenis_komoditi'] != 'Beras') {
        //             if (!in_array($harga_sebelum['jenis_komoditi'], $tmp_komoditi)) {
        //                 $tmp_komoditi[] = $harga_sebelum['jenis_komoditi'];
        //             }
        //         }
        //     }

        //     foreach ($tmp2 as $harga_sekarang) {
        //         if ($harga_sekarang['jenis_komoditi'] == 'Beras' && $harga_sekarang['varian_komoditi_id'] != null) {
        //             if (!in_array($harga_sekarang['varian_komoditi'], $tmp_komoditi)) {
        //                 $tmp_komoditi[] = $harga_sekarang['varian_komoditi'];
        //             }
        //         } else if ($harga_sekarang['jenis_komoditi'] != 'Beras') {
        //             if (!in_array($harga_sekarang['jenis_komoditi'], $tmp_komoditi)) {
        //                 $tmp_komoditi[] = $harga_sekarang['jenis_komoditi'];
        //             }
        //         }
        //     }

        //     foreach ($tmp_komoditi as $key => $komoditi) {
        //         $test[$key] = array(
        //             'jenis_komoditi' => '',
        //             'satuan_komoditi' => '',
        //             'date1' => 0,
        //             'date2' => 0
        //         );
        //     }

        //     foreach ($tmp as $harga_sebelum) {
        //         if ($harga_sebelum['jenis_komoditi'] == 'Beras' && $harga_sebelum['varian_komoditi'] != null) {
        //             if (in_array($harga_sebelum['varian_komoditi'], $tmp_komoditi)) {
        //                 $key = array_search($harga_sebelum['varian_komoditi'], $tmp_komoditi);
        //                 $test[$key]['jenis_komoditi'] = $harga_sebelum['varian_komoditi'];
        //                 $test[$key]['satuan_komoditi'] = $harga_sebelum['satuan_komoditi'];
        //                 $test[$key]['date1'] = rupiah($harga_sebelum['harga']);
        //             }
        //         } else if ($harga_sebelum['jenis_komoditi'] != 'Beras') {
        //             $key = array_search($harga_sebelum['jenis_komoditi'], $tmp_komoditi);
        //             $test[$key]['jenis_komoditi'] = $harga_sebelum['jenis_komoditi'];
        //             $test[$key]['satuan_komoditi'] = $harga_sebelum['satuan_komoditi'];
        //             $test[$key]['date1'] = rupiah($harga_sebelum['harga']);
        //         }
        //     }

        //     foreach ($tmp2 as $harga_sekarang) {
        //         if ($harga_sekarang['jenis_komoditi'] == 'Beras' && $harga_sekarang['varian_komoditi'] != null) {
        //             if (in_array($harga_sekarang['varian_komoditi'], $tmp_komoditi)) {
        //                 $key = array_search($harga_sekarang['varian_komoditi'], $tmp_komoditi);
        //                 $test[$key]['jenis_komoditi'] = $harga_sekarang['varian_komoditi'];
        //                 $test[$key]['satuan_komoditi'] = $harga_sekarang['satuan_komoditi'];
        //                 $test[$key]['date2'] = rupiah($harga_sekarang['harga']);
        //             }
        //         } else if ($harga_sekarang['jenis_komoditi'] != 'Beras') {
        //             $key = array_search($harga_sekarang['jenis_komoditi'], $tmp_komoditi);
        //             $test[$key]['jenis_komoditi'] = $harga_sekarang['jenis_komoditi'];
        //             $test[$key]['satuan_komoditi'] = $harga_sekarang['satuan_komoditi'];
        //             $test[$key]['date2'] = rupiah($harga_sekarang['harga']);
        //         }
        //     }

        //     foreach ($test as $key => $row) {
        //         if ($row['date2'] > $row['date1']) {
        //             $test[$key]['ket'] = 'up';
        //         } else if ($row['date2'] < $row['date1']) {
        //             $test[$key]['ket'] = 'down';
        //         } else if ($row['date2'] == $row['date1']) {
        //             $test[$key]['ket'] = 'equals';
        //         }

        //         $harga_date1 = ($row['date1'] != null) ? str_replace('.', '', $row['date1']) : 0;
        //         $harga_date2 = ($row['date2'] != null) ? str_replace('.', '', $row['date2']) : 0;

        //         if ($harga_date1 != 0 && $harga_date2 != 0) {
        //             $persen = ((int) $harga_date2 - (int) $harga_date1) / (int) $harga_date2;
        //             $test[$key]['percentage'] = round($persen * 100, 2);
        //         } else {
        //             $test[$key]['percentage'] = 100;
        //         }
        //     }
        // } else {
        //     if ($tmp2 != null) {
        //         $key = 0;
        //         foreach ($tmp2 as $harga_sekarang) {
        //             if ($harga_sekarang['jenis_komoditi'] == 'Beras' && $harga_sekarang['varian_komoditi'] != null) {
        //                 $test[$key]['jenis_komoditi'] = $harga_sekarang['varian_komoditi'];

        //                 $test[$key]['satuan_komoditi'] = $harga_sekarang['satuan_komoditi'];
        //                 $test[$key]['date1'] = 0;
        //                 $test[$key]['date2'] = rupiah($harga_sekarang['harga']);

        //                 $key++;
        //             } else if ($harga_sekarang['jenis_komoditi'] != 'Beras') {
        //                 $test[$key]['jenis_komoditi'] = $harga_sekarang['jenis_komoditi'];

        //                 $test[$key]['satuan_komoditi'] = $harga_sekarang['satuan_komoditi'];
        //                 $test[$key]['date1'] = 0;
        //                 $test[$key]['date2'] = rupiah($harga_sekarang['harga']);

        //                 $key++;
        //             }
        //         }

        //         foreach ($test as $key => $row) {
        //             if ($row['date2'] > $row['date1']) {
        //                 $test[$key]['ket'] = 'up';
        //             } else if ($row['date2'] < $row['date1']) {
        //                 $test[$key]['ket'] = 'down';
        //             } else if ($row['date2'] == $row['date1']) {
        //                 $test[$key]['ket'] = 'equals';
        //             }
        //             $test[$key]['percentage'] = 100;
        //         }
        //     }

        //     if ($tmp != null) {
        //         $key = 0;
        //         foreach ($tmp as $harga_sebelum) {
        //             if ($harga_sebelum['jenis_komoditi'] == 'Beras' && $harga_sebelum['varian_komoditi'] != null) {
        //                 $test[$key]['jenis_komoditi'] = $harga_sebelum['varian_komoditi'];

        //                 $test[$key]['satuan_komoditi'] = $harga_sebelum['satuan_komoditi'];
        //                 $test[$key]['date1'] = rupiah($harga_sebelum['harga']);
        //                 $test[$key]['date2'] = 0;
        //                 $key++;
        //             } else if ($harga_sebelum['jenis_komoditi'] != 'Beras') {
        //                 $test[$key]['jenis_komoditi'] = $harga_sebelum['jenis_komoditi'];

        //                 $test[$key]['satuan_komoditi'] = $harga_sebelum['satuan_komoditi'];
        //                 $test[$key]['date1'] = rupiah($harga_sebelum['harga']);
        //                 $test[$key]['date2'] = 0;
        //                 $key++;
        //             }
        //         }

        //         foreach ($test as $key => $row) {
        //             if ($row['date2'] > $row['date1']) {
        //                 $test[$key]['ket'] = 'up';
        //             } else if ($row['date2'] < $row['date1']) {
        //                 $test[$key]['ket'] = 'down';
        //             } else if ($row['date2'] == $row['date1']) {
        //                 $test[$key]['ket'] = 'equals';
        //             }
        //             $test[$key]['percentage'] = 100;
        //         }
        //     }
        // }

        // $response['data'] = $test;

        $response['data'] = $data;

        $response['date1'] = date('d M Y', strtotime($date1));
        $response['date2'] = date('d M Y', strtotime($date2));
        $response['region'] = $nama_daerah;

        echo json_encode($response);
    }

    public function save()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('name', 'Nama Pasar', 'trim|required');
        $this->form_validation->set_rules('jenis_komoditi', 'Jenis Komoditi', 'trim|required');
        $this->form_validation->set_rules('varian_komoditi', 'Varian Komoditi', 'trim|required');
        $this->form_validation->set_rules('satuan_komoditi', 'Satuan Komoditi', 'trim|required');
        $this->form_validation->set_rules('harga', 'harga', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
        } else {
            $pasar_harga_komoditi_id = (!empty($this->input->post('harga_komoditi_id'))) ? $this->input->post('harga_komoditi_id') : '';

            $split_jenis_komoditi = explode('|', $this->input->post('jenis_komoditi'));

            $post_field = array(
                'pasar_id'  => $this->input->post('pasar_id', true),
                'jenis_komoditi_id' => $split_jenis_komoditi[0],
                'nama_pasar'    => $this->input->post('name', true),
                'nama_komoditi' => $split_jenis_komoditi[1],
                'varian_komoditi_id'   => $this->input->post('varian_komoditi', true),
                'satuan_komoditi_id'    => $this->input->post('satuan_komoditi', true),
                'harga' => str_replace('.', '', $this->input->post('harga', true)),
                'email' => $this->session->userdata('email'),
                'token' => $this->token,
                'pasar_harga_komoditi_id' => $pasar_harga_komoditi_id,
                'jumlah'    => 1
            );

            $api = array(
                'endpoint'      => 'setHargaKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['isUpdate'] = (!empty($this->input->post('harga_komoditi_id'))) ? true : false;
                $this->data['response']['success'] = true;
            }
        }

        echo json_encode($this->data['response']);
    }

    function deleteHarga()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {
            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_harga_komoditi',
                'key_name'  => 'pasar_harga_komoditi_id',
                'key'   => $this->session->userdata('detail_harga')[$pagenumber]['data'][$id]['pasar_harga_komoditi_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }


        echo json_encode($this->data['response']);
    }

    public function getHargaBahanPokokEWS()
    {
        $response = array('success' => false, 'data' => array());

        $date1 = ($this->input->post('date2', true) != null) ? $this->input->post('date2', true) : date('Y-m-d', strtotime('last weekday'));
        $date2 = ($this->input->post('date1', true) != null) ? $this->input->post('date1', true) : date('Y-m-d', strtotime('last weekday', strtotime('yesterday')));

       

        $provinsi = ($this->input->post('provinsi', true) != null) ? $this->input->post('provinsi', true) : 0;
        $nama_provinsi = 'Nasional';
        $provinsi_id = 0;

        if ($this->input->post('provinsi', true) != null) {
            $exp = explode('|', $this->input->post('provinsi', true));

            $provinsi_id = $exp[0];

            if ($provinsi_id != 0) {
                $nama_provinsi = $exp[1];
            }
        }

        $api = array(
            'endpoint'      => 'getHargaRataRata',
            'method'        => 'POST',
            'post_field'    => array('lokasi' => $provinsi_id, 'tgl_awal' => $date1, 'tgl_akhir' => $date2)
        );

        $service = $this->ewsAPI($api);

        if ($service['kode'] == 200) {
            foreach ($service['data'] as $key => $row) {
                $date1 = $row['date_2'];
                $date2 = $row['date_1'];
            }
        }

        $response['data'] = ($service['kode'] == 200) ? $service['data'] : array();
        $response['date1'] = date('d M Y', strtotime($date1));
        $response['date2'] = date('d M Y', strtotime($date2));
        $response['region'] = $nama_provinsi;
        $response['success'] = true;

        echo json_encode($response);
    }
}
