<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Revitalisasi extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->helper('file');
        $this->load->library('encryption');
        $this->load->model('Baseben_Model', 'baseben');
        $this->is_login();
        $this->encryption->initialize(
            array(
                'cipher' => 'aes-256',
                'mode' => 'ctr',
                'key' => 'Safest key in the world'
            )
        );
    }

    function index()
    {

        has_access(2, 'show');
        $this->data['title'] = 'Revitalisasi - Proposal';
        $this->data['menu'] = 'proposal';
        $this->data['js'] = 'revitalisasi/proposal_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/proposal', $this->data);
    }

    function lelang($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);

        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getProposalLelang',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 10,
                'offset' => 0
            )
        );

        $this->data['total_pagu_digunakan'] = 0;
        $this->data['proposal_lelang_id_pagu'] = [];
        $this->data['nilai_pagu'] = [];
        $get_lelang = $this->serviceAPI($api);
        if (isset($get_lelang['data']) && count($get_lelang['data'])) {
            $this->data['total_pagu_digunakan'] = array_sum(array_column($get_lelang['data'], 'pagu'));
            $this->data['proposal_lelang_id_pagu'] = array_column($get_lelang['data'], 'proposal_lelang_id');
            $this->data['nilai_pagu'] = array_column($get_lelang['data'], 'pagu');
        }



        $this->data['title'] = 'Revitalisasi - Lelang ' . $this->data['proposal']['judul'] . '';
        $this->data['menu'] = 'lelang';
        $this->data['js'] = 'revitalisasi/lelang_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/lelang', $this->data);
    }

    function kontrak($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);

        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];

        $this->data['title'] = 'Revitalisasi - Kontrak ' . $this->data['proposal']['judul'] . '';
        $this->data['menu'] = 'kontrak';
        $this->data['js'] = 'revitalisasi/kontrak_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/kontrak', $this->data);
    }

    function jadwal($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);


        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getProposalJadwal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal_jadwal'] = $this->serviceAPI($api)['data'][0];

        if (isset($this->data['proposal_jadwal']['kurva'])) {
            $master_jadwal_id = array_column($this->data['proposal_jadwal']['kurva'], 'master_jadwal_id');
            $this->data['kurva_bobot'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'bobot'));
            $this->data['kurva_biaya'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'biaya'));
            $this->data['kurva_pengerjaan'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'pengerjaan'));
            $this->data['kurva_pengerjaan'] = array_map(function ($json) {
                return json_decode($json, true);
            }, $this->data['kurva_pengerjaan']);
            $this->data['kurva_id'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'proposal_jadwal_kurva_id'));
        }

        //   pre($this->data['proposal_jadwal']);

        $api = array(
            'endpoint' => 'getProposalKontrak',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'tipe_pengadaan' => 'Pelaksanaan',
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['kontrak'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getMasterJadwal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'limit' => 30,
                'offset' => 0
            )
        );

        $this->data['ms_jadwal'] = $this->serviceAPI($api)['data'];
        // pre($this->data['ms_jadwal']);

        $this->data['title'] = 'Revitalisasi - Jadwal ' . $this->data['proposal']['judul'] . '';
        $this->data['menu'] = 'jadwal';
        $this->data['js'] = 'revitalisasi/jadwal_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/jadwal', $this->data);
    }

    function penetapan()
    {
        $this->data['title'] = 'Revitalisasi - Penetapan';
        $this->data['menu'] = 'penetapan';
        $this->data['js'] = 'revitalisasi/penetapan_js';
        $this->data['menu_id'] = 80;

        $api = array(
            'endpoint' => 'getPenetapan',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'tahun_anggaran' =>  date('Y', strtotime('+1 year')),
                'limit' => 1,
                'offset' => 0
            )
        );
        $this->data['penetapan'] = $this->serviceAPI($api)['kode'] == 200 ? $this->serviceAPI($api)['data'] : [];



        $this->templateGovCMS('revitalisasi/penetapan', $this->data);
    }
    function penetapan_detail($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['penetapan_id'] = $this->encryption->decrypt($uuid);

        $api = array(
            'endpoint' => 'getPenetapan',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'penetapan_id' =>  $this->data['penetapan_id'],
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['penetapan'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getTipePasar',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
            )
        );

        $this->data['tipe_pasar'] = json_encode($this->serviceAPI($api)['data']);

        $this->data['title'] = 'Revitalisasi - Penetapan ' . $this->data['penetapan']['no_permendag'];
        $this->data['menu'] = 'penetapan';
        $this->data['js'] = 'revitalisasi/penetapan_detail_js';
        $this->data['menu_id'] = 80;
        $this->templateGovCMS('revitalisasi/penetapan_detail', $this->data);
    }

    function proposal_detail($uuid)
    {

        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);

        if (is_k1_ketuatim() == 1) {
            $api = array(
                'endpoint' => 'getPengguna',
                'method' => 'POST',
                'controller' => 'api',
                'post_field' => array(
                    'token' => $this->token,
                    'tipe_pengguna_id' => 21,
                    'limit' => 1000,
                    'offset' => 0
                )
            );

            $this->data['stafk1'] = $this->serviceAPI($api)['data'];
        }


        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];


        $api = array(
            'endpoint' => 'getProposalDokumen',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'jenis_dokumen' => 'revitalisasi_proposal_foto_pasar_1',
                'limit' => 100,
                'offset' => 0
            )
        );

        $this->data['proposal_foto_pasar'] = $this->serviceAPI($api)['data'];

        foreach ($this->data['proposal_foto_pasar'] as $k => $v) {

            $file_data = file_get_contents('./services/res/upload/revitalisasi/proposal/' . $v['file']);
            $mime_type = mime_content_type('./services/res/upload/revitalisasi/proposal/' . $v['file']);

            $this->data['proposal_foto_pasar'][$k]['file_base64'] = base64_encode($file_data);
            $this->data['proposal_foto_pasar'][$k]['file_mime'] = 'data:' . $mime_type . ';base64,';
        }

        $api = array(
            'endpoint' => 'getPasar',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
                'pasar_id' => $this->data['proposal']['pasar_id'],
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['pasar'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getDetailPasar',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
                'pasar_id' => $this->data['proposal']['pasar_id'],
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['pasar_detail'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getPasar_pengelola',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
                'pasar_id' => $this->data['proposal']['pasar_id'],
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['pengelola_pasar'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getAnggaranPasar',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
                'pasar_id' => $this->data['proposal']['pasar_id'],
                'program_pasar_id' => 1,
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['dana_tp'] = $this->serviceAPI($api);

        $api = array(
            'endpoint' => 'getAnggaranPasar',
            'method' => 'POST',
            'controller' => 'pasar',
            'post_field' => array(
                'token' => $this->token,
                'pasar_id' => $this->data['proposal']['pasar_id'],
                'program_pasar_id' => 2,
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['dana_dak'] = $this->serviceAPI($api);


        $this->data['pasar_id'] = $this->data['proposal']['pasar_id'];
        $this->data['nama_pasar'] = $this->data['proposal']['nama_pasar'];


        $this->data['title'] = 'Revitalisasi - Proposal ' . $this->data['nama_pasar'];
        $this->data['menu'] = 'proposal';
        $this->data['js'] = 'revitalisasi/proposal_detail_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/proposal_detail', $this->data);
    }

    function setProposal($step = null)
    {

        $data_insert = [
            'token' => $this->token,
            'email' => $this->session->userdata('email'),
            'pasar_id' => $this->input->post('pasar_id'),
            'nama_pasar' => $this->input->post('nama_pasar'),
            'tgl' => date('Y-m-d', strtotime($this->input->post('tgl'))),
            'judul' => $this->input->post('judul'),
            'latar_belakang' => $this->input->post('latar_belakang'),
            'maksud_tujuan' => $this->input->post('maksud_tujuan'),
            'pengusul' => $this->input->post('pengusul'),
            'tingkat_pengusul' => $this->input->post('tingkat_pengusul'),
            'nama_dinas' => $this->input->post('nama_dinas'),
            'tahun_anggaran' => date('Y', strtotime('+1 year')),
            'anggaran' => (float)$this->input->post('anggaran'),
            'kode_satker' => $this->input->post('kode_satker'),
            'kode_lokasi' => $this->input->post('kode_lokasi'),
            'kode_kppn' => $this->input->post('kode_kppn'),
            'proposal_id' => $this->input->post('proposal_id')
        ];

        if ($step == 'sk') {
            $api = array(
                'endpoint' => 'getProposal',
                'method' => 'POST',
                'controller' => 'revitalisasi',
                'post_field' => array(
                    'token' => $this->token,
                    'proposal_id' => $this->input->post('proposal_id'),
                    'limit' => 1,
                    'offset' => 0
                )
            );

            $response = $this->serviceAPI($api);
            $proposal = $response['data'][0];

            $data_insert = [
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'pasar_id' => $proposal['pasar_id'],
                'nama_pasar' => $proposal['nama_pasar'],
                'tgl' => date('Y-m-d', strtotime($proposal['tgl'])),
                'judul' => $proposal['judul'],
                'latar_belakang' => $proposal['latar_belakang'],
                'maksud_tujuan' => $proposal['maksud_tujuan'],
                'pengusul' => $proposal['pengusul'],
                'tingkat_pengusul' => $proposal['tingkat_pengusul'],
                'nama_dinas' => $proposal['nama_dinas'],
                'tahun_anggaran' => $proposal['tahun_anggaran'],
                'anggaran' => (float)$proposal['anggaran'],
                'kode_satker' => $proposal['kode_satker'],
                'kode_lokasi' => $proposal['kode_lokasi'],
                'kode_kppn' => $proposal['kode_kppn'],
                'proposal_id' => $proposal['proposal_id'],
                'tgl_sk_efektif' => date('Y-m-d', strtotime($this->input->post('tgl_sk_efektif'))),
                'tgl_sk_selesai' => date('Y-m-d', strtotime($this->input->post('tgl_sk_selesai'))),
            ];
        }

        $api = array(
            'endpoint' => 'setProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => $data_insert
        );

        $response = $this->serviceAPI($api);
        if ($step == 'sk') {
            redirect(base_url() . 'web/revitalisasi/proposal');
        }
        echo json_encode($response);
    }

    function setProposalDokumen()
    {

        $api = array(
            'endpoint' => 'setProposalDokumen',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_id' => $this->input->post('proposal_id'),
                'dokumen_id' => $this->input->post('dokumen_id'),
                'proposal_dokumen_id' => $this->input->post('proposal_dokumen_id'),
            )
        );

        if ($_FILES['file']['name'] != '') {
            $api['post_field']['file'] = new CurlFile($_FILES['file']['tmp_name'], $_FILES['file']['type'], $_FILES['file']['name']);
        }


        $response = $this->serviceAPI($api);
        echo json_encode($response);
    }

    function setProposalPengelola()
    {

        $api = array(
            'endpoint' => 'setProposalPengelola',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_id' => $this->input->post('proposal_id'),
                'sk_pengelola' => $this->input->post('sk_operator'),
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'jabatan' => $this->input->post('jabatan'),
                'proposal_pengelola_id' => $this->input->post('proposal_pengelola_id'),
            )
        );
        if ($_FILES['lampiran_usulan']['tmp_name'] != '') {
            $api['post_field']['lampiran_usulan'] = new CurlFile($_FILES['lampiran_usulan']['tmp_name'], $_FILES['lampiran_usulan']['type'], $_FILES['lampiran_usulan']['name']);
        }

        $response = $this->serviceAPI($api);

        redirect(base_url() . 'web/revitalisasi/proposal/' . $this->input->post('uuid') . '#sk_pengelola');
    }

    function tugaskanStaf()
    {


        $api = array(
            'endpoint' => 'setProposalDokumen',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'staf_ditugaskan' => $this->input->post('staf_ditugaskan'),
                'proposal_id' => $this->input->post('proposal_id'),
                'dokumen_id' => $this->input->post('dokumen_id'),
                'proposal_dokumen_id' => $this->input->post('proposal_dokumen_id'),
            )
        );

        $this->serviceAPI($api);

        redirect(base_url() . 'web/revitalisasi/proposal/' . $this->input->post('uuid') . '#dokumen_lanjutan');
    }

    function getProposal()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('revitalisasi_proposal')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('revitalisasi_proposal')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah')) && empty($this->input->post('proposal_id'))) {
            $session_existing = $this->session->userdata('revitalisasi_proposal')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
                } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                    $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 4);
                }
            }

            $api = array(
                'endpoint' => 'getProposal',
                'method' => 'POST',
                'controller' => 'revitalisasi',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'daerah_id' => $kode_daerah,
                    'pasar_id' => $this->input->post('pasar_id'),
                    'proposal_id' => $this->input->post('proposal_id'),
                    'status_penetapan' => $this->input->post('status_penetapan'),
                    'tahun_anggaran' => $this->input->post('tahun_anggaran'),
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            if ($this->input->post('status') == 'Ditetapkan') {
                $api['post_field']['status_penetapan'] = $this->input->post('status');
            } else {
                $api['post_field']['status'] = $this->input->post('status');
            }

            $response = $this->serviceAPI($api);

            foreach ($response['data'] as $k => $v) {
                $response['data'][$k]['uuid'] = base64_encode($this->encryption->encrypt($v['proposal_id']));
                $response['data'][$k]['dokumen_reject'] = '';
                if ($v['jml_dokumen_reject'] > 0) {

                    $api_proposal_dokumen = array(
                        'endpoint' => 'getProposalDokumen',
                        'method' => 'POST',
                        'controller' => 'revitalisasi',
                        'post_field' => array(
                            'token' => $this->token,
                            'proposal_id' => $v['proposal_id'],
                            'status' => 'Ditolak',
                            'limit' => 10,
                            'offset' => 0
                        )
                    );

                    $response['data'][$k]['dokumen_reject'] = implode(', ', array_column($this->serviceAPI($api_proposal_dokumen)['data'], 'judul_dokumen'));
                }
            }
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['revitalisasi_proposal'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('revitalisasi_proposal')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getPasar()
    {

        if ((empty($this->input->post('refresh')) && isset($this->session->userdata('revitalisasi_pasar_proposal')[$this->input->post('kode_daerah')])) && empty($this->input->post('pasar_id')) && empty($this->input->post('verify_kelengkapan'))) {
            echo json_encode($this->session->userdata('revitalisasi_pasar_proposal')[$this->input->post('kode_daerah')]);
        } else {

            $kode_daerah = '';
            if (!empty($this->input->post('kode_daerah'))) {
                $kode_daerah = $this->input->post('kode_daerah');
            } else {
                $kode_daerah = substr($this->session->userdata('daerah_id'), 0, 2);
            }


            $api = array(
                'endpoint' => 'getPasar',
                'method' => 'POST',
                'controller' => 'pasar',
                'post_field' => array(
                    'token' => $this->token,
                    'daerah_id' => $kode_daerah,
                    'pasar_id' =>  $this->input->post('pasar_id'),
                    'verify_kelengkapan' =>  $this->input->post('verify_kelengkapan'),
                    'limit' => (empty($this->input->post('limit')) ? 20000 : $this->input->post('limit')),
                    'offset' => (empty($this->input->post('offset')) ? 0 : $this->input->post('offset'))
                )
            );

            $response = $this->serviceAPI($api);


            if ($response && $response['kode'] == 200) {
                if (!empty($this->input->post('pasar_id'))) {
                    $api = array(
                        'endpoint' => 'getDetailPasar',
                        'method' => 'POST',
                        'controller' => 'pasar',
                        'post_field' => array(
                            'token' => $this->token,
                            'pasar_id' =>  $this->input->post('pasar_id')
                        )
                    );

                    $detail_response = $this->serviceAPI($api);
                    $response['data'][0]['detail'] = $detail_response['data'][0];

                    $api = array(
                        'endpoint' => 'getAnggaranPasar',
                        'method' => 'POST',
                        'controller' => 'pasar',
                        'post_field' => array(
                            'token' => $this->token,
                            'pasar_id' =>  $this->input->post('pasar_id')
                        )
                    );

                    $anggaran_response = $this->serviceAPI($api);
                    $dana_tp = [];
                    $dana_dak = [];
                    if ($anggaran_response['kode'] == 200) {
                        foreach ($anggaran_response['data'] as $k => $v) {
                            if ($v['program_pasar_id'] == 1 && count($dana_tp) == 0) {
                                $dana_tp = $v;
                            } else if ($v['program_pasar_id'] == 2 && count($dana_dak) == 0) {
                                $dana_dak = $v;
                            }
                        }
                    }


                    $response['data'][0]['dana_tp'] = $dana_tp;
                    $response['data'][0]['dana_dak'] = $dana_dak;

                    echo json_encode($response);
                } else if (!empty($this->input->post('verify_kelengkapan'))) {
                    echo json_encode($response);
                } else {
                    $session = $this->session->all_userdata();
                    $session['revitalisasi_pasar_proposal'][$this->input->post('kode_daerah')] = $response;
                    $this->session->set_userdata($session);
                    echo json_encode($this->session->userdata('revitalisasi_pasar_proposal')[$this->input->post('kode_daerah')]);
                }
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function getProposalDokumen()
    {
        $api = array(
            'endpoint' => 'getDokumen',
            'method' => 'POST',
            'controller' => 'master',
            'post_field' => array(
                'token' => $this->token,
                'jenis' => $this->input->post('jenis'),
                'dokumen_id' =>  $this->input->post('dokumen_id'),
            )
        );


        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {
            $mimes = get_mimes();

            foreach ($response['data'] as $k => $v) {
                $mimes_now = explode(',', $v['accept']);
                $mimes_accepted = [];
                foreach ($mimes_now as $kk => $vv) {

                    $mimes_accepted[] = (is_array($mimes[$vv]) ? implode(',', $mimes[$vv]) : $mimes[$vv]);
                }

                $response['data'][$k]['mimes_accepted']  = implode(',', $mimes_accepted);
                if (!empty($this->input->post('proposal_id'))) {
                    $api_proposal_dokumen = array(
                        'endpoint' => 'getProposalDokumen',
                        'method' => 'POST',
                        'controller' => 'revitalisasi',
                        'post_field' => array(
                            'token' => $this->token,
                            'proposal_id' => $this->input->post('proposal_id'),
                            'dokumen_id' =>  $v['dokumen_id'],
                            'limit' => 1,
                            'offset' => 0
                        )
                    );
                    if ($this->session->userdata('tipe_pengguna_id') == '21' && in_array($this->input->post('jenis'), ['revitalisasi_proposal_dokumen_2'])) {
                        // $api_proposal_dokumen['post_field']['staf_ditugaskan'] = $this->session->userdata('email');
                    }


                    $proposal_dokumen = $this->serviceAPI($api_proposal_dokumen);
                    if ($proposal_dokumen && $proposal_dokumen['kode'] == 200) {
                        $response['data'][$k]['file_uploaded'] = true;
                        $response['data'][$k]['file'] = base_url() . 'download/revitalisasi/proposal/' . $proposal_dokumen['data'][0]['file'];
                        $response['data'][$k]['file_data'] = $proposal_dokumen['data'][0]['file'];
                        $response['data'][$k]['file_keterangan'] = $proposal_dokumen['data'][0]['keterangan'];
                        $response['data'][$k]['file_status'] = $proposal_dokumen['data'][0]['status'];
                        $response['data'][$k]['proposal_dokumen_id'] = $proposal_dokumen['data'][0]['proposal_dokumen_id'];
                        $response['data'][$k]['staf_ditugaskan'] = $proposal_dokumen['data'][0]['staf_ditugaskan'];
                        $response['data'][$k]['staf_ditugaskan_nama'] = $proposal_dokumen['data'][0]['staf_ditugaskan_nama'];
                    } else {
                        $response['data'][$k]['file_uploaded'] = false;
                        if ($this->session->userdata('tipe_pengguna_id') == '21' && in_array($this->input->post('jenis'), ['revitalisasi_proposal_dokumen_2'])) {
                            unset($response['data'][$k]);
                        }
                    }
                } else {
                    $response['data'][$k]['file_uploaded'] = false;
                }
            }

            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function getProposalPengelola()
    {

        $api = array(
            'endpoint' => 'getProposalPengelola',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'proposal_id' => $this->input->post('proposal_id'),
                'nip' => $this->input->post('nip'),
                'proposal_pengelola_id' => $this->input->post('proposal_pengelola_id'),
                'status' => $this->input->post('status'),
                'limit' => 20,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {
            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function submitProposal()
    {
        if (!is_verifikator_proposal() && $this->input->post('status') != 'Diajukan') {
            echo json_encode(['kode' => 403]);
            die();
        }
        $api = array(
            'endpoint' => 'verifyProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'jenis' => $this->input->post('jenis'),
                'status' => $this->input->post('status'),
                'keterangan' => $this->input->post('keterangan'),
                'proposal_dokumen_id' => $this->input->post('proposal_dokumen_id'),
                'proposal_id' => $this->input->post('proposal_id')
            )
        );

        $response = $this->serviceAPI($api);
        echo json_encode($response);
    }

    function getPenetapan()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('revitalisasi_penetapan')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('revitalisasi_penetapan')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('kode_daerah')) && empty($this->input->post('penetapan_id'))) {
            $session_existing = $this->session->userdata('revitalisasi_penetapan')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {


            $api = array(
                'endpoint' => 'getPenetapan',
                'method' => 'POST',
                'controller' => 'revitalisasi',
                'post_field' => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'tahun_anggaran' => $this->input->post('tahun_anggaran'),
                    'status' => $this->input->post('status'),
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            foreach ($response['data'] as $k => $v) {
                $response['data'][$k]['uuid'] = base64_encode($this->encryption->encrypt($v['penetapan_id']));
            }
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['revitalisasi_penetapan'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('revitalisasi_penetapan')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function setPenetapan()
    {

        $api = array(
            'endpoint' => 'setPenetapan',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'tgl' => $this->input->post('tgl'),
                'tahun_anggaran' => $this->input->post('tahun_anggaran'),
                'no_permendag' => $this->input->post('no_permendag'),
                'keterangan' => $this->input->post('deskripsi'),
                'penetapan_id' => $this->input->post('penetapan_id'),
            )
        );


        $response = $this->serviceAPI($api);
        if ($response['kode'] == 200) {
            redirect(base_url() . 'web/revitalisasi/penetapan/' . base64_encode($this->encryption->encrypt($response['data']['penetapan_id'])));
        } else {
            redirect(base_url() . 'web/revitalisasi/penetapan');
        }
    }

    function getProposalPasar()
    {
        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'daerah_id' =>  $this->input->post('kode_daerah'),
                'status' => 'Selesai Review',
                'limit' => 200,
                'offset' => 0
            )
        );

        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {
            $pasar = [];
            foreach ($response['data'] as $k => $v) {
                $pasar[] = [
                    'pasar_id' => $v['pasar_id'],
                    'nama_pasar' => $v['nama_pasar']
                ];
            }
            echo json_encode($pasar);
        } else {
            echo json_encode(array());
        }
    }

    function getPenetapanProposal()
    {

        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'daerah_id' => $this->input->post('daerah_id'),
                'pasar_id' => $this->input->post('pasar_id'),
                'status' => 'Selesai Review',
                'limit' => $this->input->post('length'),
                'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
            )
        );

        $response = $this->serviceAPI($api);

        foreach ($response['data'] as $k => $v) {
            $response['data'][$k]['is_ditetapkan'] = false;
            $api_penetapan_proposal = array(
                'endpoint' => 'getPenetapanProposal',
                'method' => 'POST',
                'controller' => 'revitalisasi',
                'post_field' => array(
                    'token' => $this->token,
                    'proposal_id' => $v['proposal_id'],
                    'limit' => 1,
                    'offset' => 0
                )
            );
            $response_penetapan_proposal = $this->serviceAPI($api_penetapan_proposal);
            if ($response_penetapan_proposal['kode'] == 200) {
                $response['data'][$k]['is_ditetapkan'] = true;
                $response['data'][$k]['penetapan_proposal_id'] =  $response_penetapan_proposal['data'][0]['penetapan_proposal_id'];
                $response['data'][$k]['penetapan_tipe_pasar_id'] =  $response_penetapan_proposal['data'][0]['tipe_pasar_id'];
                $response['data'][$k]['penetapan_tipe_pasar'] =  $response_penetapan_proposal['data'][0]['tipe_pasar'];
                $response['data'][$k]['penetapan_anggaran'] =  $response_penetapan_proposal['data'][0]['anggaran'];
                $response['data'][$k]['penetapan_status'] =  $response_penetapan_proposal['data'][0]['status'];
            }
        }
        $response['draw'] = $this->input->post('draw');
        if ($response && $response['kode'] == 200) {
            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function setPenetapanProposal()
    {
        if ($this->input->post('is_ditetapkan') == 'true') {

            $api = array(
                'endpoint' => 'setPenetapanProposal',
                'method' => 'POST',
                'controller' => 'revitalisasi',
                'post_field' => array(
                    'token' => $this->token,
                    'email' => $this->session->userdata('email'),
                    'penetapan_id' => $this->input->post('penetapan_id'),
                    'proposal_id' => $this->input->post('proposal_id'),
                    'tipe_pasar_id' => $this->input->post('tipe_pasar_id'),
                    'anggaran' => $this->input->post('anggaran'),
                    'penetapan_proposal_id' => $this->input->post('penetapan_proposal_id'),
                )
            );


            $response = $this->serviceAPI($api);
        } else {
            $api = array(
                'endpoint'      => 'setStatus',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'email' => $this->session->userdata('email'),
                    'table_name'    => 'revitalisasi.tbl_penetapan_proposal',
                    'key'   => $this->input->post('penetapan_proposal_id'),
                    'key_name'  => 'penetapan_proposal_id',
                    'status'    => 'Deleted',
                    'keterangan_perubahan' => 'Penghapusan penetapan'
                )
            );

            $response = $this->serviceAPI($api);
        }

        echo json_encode($response);
    }

    function getProposalLelang()
    {

        $api = array(
            'endpoint' => 'getProposalLelang',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'proposal_id' => $this->input->post('proposal_id'),
                'proposal_lelang_id' => $this->input->post('proposal_lelang_id'),
                'limit' => $this->input->post('length'),
                'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
            )
        );

        $response = $this->serviceAPI($api);
        $response['draw'] = $this->input->post('draw');
        if ($response && $response['kode'] == 200) {
            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function setProposalLelang()
    {

        //   pre($this->input->post());
        $api = array(
            'endpoint' => 'setProposalLelang',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_id' => $this->input->post('proposal_id'),
                'tipe_pengadaan' => $this->input->post('tipe_pengadaan'),
                'kategori' => 'Lelang',
                'judul' => $this->input->post('judul'),
                'lingkup' => $this->input->post('lingkup'),
                'kode' => $this->input->post('kode'),
                'instansi' => $this->input->post('instansi'),
                'tgl' => date('Y-m-d', strtotime($this->input->post('tgl'))),
                'tahun' => $this->input->post('tahun_anggaran'),
                'hps' => (float)$this->input->post('hps'),
                'pagu' => (float)$this->input->post('pagu'),
                'keterangan' => $this->input->post('keterangan'),
                'status' => 'Perlu Verifikasi',
                'proposal_lelang_id' => $this->input->post('proposal_lelang_id'),
            )
        );

        if ($_FILES['dokumen']['name'] != '') {
            $api['post_field']['dokumen'] = new CurlFile($_FILES['dokumen']['tmp_name'], $_FILES['dokumen']['type'], $_FILES['dokumen']['name']);
        }


        $response = $this->serviceAPI($api);

        $proposal_lelang_id = $response['data']['proposal_lelang_id'];
        if ($response['kode'] == 200) {
            foreach ($this->input->post('tahapan') as $k => $v) {
                $api = array(
                    'endpoint' => 'setProposalLelangTahapan',
                    'method' => 'POST',
                    'controller' => 'revitalisasi',
                    'post_field' => array(
                        'token' => $this->token,
                        'email' => $this->session->userdata('email'),
                        'proposal_lelang_id' =>  $proposal_lelang_id,
                        'lelang_tahapan_id' => $k,
                        'tgl' => date('Y-m-d', strtotime($v)),
                        'proposal_lelang_tahapan_id' => (!empty($this->input->post('proposal_lelang_tahapan_id')[$k]) ? $this->input->post('proposal_lelang_tahapan_id')[$k] : '')
                    )
                );
                $response = $this->serviceAPI($api);
            }
        }
        redirect(base_url() . 'web/revitalisasi/lelang/' . $this->input->post('uuid'));
    }

    function getProposalLelangTahapan()
    {
        $api = array(
            'endpoint' => 'getLelangTahapan',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'limit' => 10,
                'offset' => 0,
                'order_by' =>  json_encode(['order', 'asc']),
            )
        );


        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {

            foreach ($response['data'] as $k => $v) {
                if (!empty($this->input->post('proposal_lelang_id'))) {
                    $api_proposal_lelang_tahapan = array(
                        'endpoint' => 'getProposalLelangTahapan',
                        'method' => 'POST',
                        'controller' => 'revitalisasi',
                        'post_field' => array(
                            'token' => $this->token,
                            'lelang_tahapan_id' => $v['lelang_tahapan_id'],
                            'proposal_lelang_id' => $this->input->post('proposal_lelang_id'),
                            'limit' => 1,
                            'offset' => 0
                        )
                    );


                    $proposal_lelang_tahapan = $this->serviceAPI($api_proposal_lelang_tahapan);
                    if ($proposal_lelang_tahapan && $proposal_lelang_tahapan['kode'] == 200) {
                        $response['data'][$k]['is_filled'] = true;
                        $response['data'][$k]['proposal_lelang_tahapan_id'] = $proposal_lelang_tahapan['data'][0]['proposal_lelang_tahapan_id'];
                        $response['data'][$k]['tgl'] = $proposal_lelang_tahapan['data'][0]['tgl'];
                        $response['data'][$k]['ket'] = $proposal_lelang_tahapan['data'][0]['ket'];
                        $response['data'][$k]['proposal_lelang_status'] = $proposal_lelang_tahapan['data'][0]['status'];
                    } else {
                        $response['data'][$k]['is_filled'] = false;
                    }
                } else {
                    $response['data'][$k]['is_filled'] = false;
                }
            }

            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function setProposalKontrak()
    {

        $api = array(
            'endpoint' => 'setProposalKontrak',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_lelang_id' => $this->input->post('proposal_lelang_id'),
                'tgl_awal' => date('Y-m-d', strtotime($this->input->post('tgl_awal'))),
                'tgl_akhir' => date('Y-m-d', strtotime($this->input->post('tgl_akhir'))),
                'no_kontrak' => $this->input->post('no_kontrak'),
                'tgl_dipa' => date('Y-m-d', strtotime($this->input->post('tgl_dipa'))),
                'no_dipa' => $this->input->post('no_dipa'),
                'pemenang' => $this->input->post('pemenang'),
                'nilai' => (float)$this->input->post('nilai'),
                'keterangan' => $this->input->post('keterangan'),
                'status' => 'Draft',
                'proposal_kontrak_id' => $this->input->post('proposal_kontrak_id'),
            )
        );

        if ($_FILES['dok_kontrak']['name'] != '') {
            $api['post_field']['dok_kontrak'] = new CurlFile($_FILES['dok_kontrak']['tmp_name'], $_FILES['dok_kontrak']['type'], $_FILES['dok_kontrak']['name']);
        }

        if ($_FILES['dok_jaminan']['name'] != '') {
            $api['post_field']['dok_jaminan'] = new CurlFile($_FILES['dok_jaminan']['tmp_name'], $_FILES['dok_jaminan']['type'], $_FILES['dok_jaminan']['name']);
        }


        $response = $this->serviceAPI($api);

        redirect(base_url() . 'web/revitalisasi/kontrak/' . $this->input->post('uuid'));
    }

    function getProposalLelangKontrak()
    {

        $api = array(
            'endpoint' => 'getProposalLelang',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'search' => $this->input->post('search')['value'],
                'proposal_id' => $this->input->post('proposal_id'),
                'proposal_lelang_id' => $this->input->post('proposal_lelang_id'),
                'limit' => $this->input->post('length'),
                'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
            )
        );

        $response = $this->serviceAPI($api);
        $response['draw'] = $this->input->post('draw');
        if ($response && $response['kode'] == 200) {
            foreach ($response['data'] as $k => $v) {
                $api_proposal_kontrak = array(
                    'endpoint' => 'getProposalKontrak',
                    'method' => 'POST',
                    'controller' => 'revitalisasi',
                    'post_field' => array(
                        'token' => $this->token,
                        'proposal_lelang_id' => $v['proposal_lelang_id'],
                        'limit' => 1,
                        'offset' => 0
                    )
                );


                $proposal_kontrak = $this->serviceAPI($api_proposal_kontrak);
                if ($proposal_kontrak && $proposal_kontrak['kode'] == 200) {
                    $response['data'][$k]['is_filled'] = true;
                    $response['data'][$k]['kontrak'] = $proposal_kontrak['data'][0];
                    // $response['data'][$k]['kontrak']['dok_kontrak'] = base_url().'download/revitalisasi/proposal/kontrak/'.$response['data'][$k]['kontrak']['dok_kontrak'];
                } else {
                    $response['data'][$k]['is_filled'] = false;
                }
            }
            echo json_encode($response);
        } else {
            echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
        }
    }

    function setProposalJadwal()
    {

        //  pre($this->input->post());

        $api = array(
            'endpoint' => 'setProposalJadwal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_id' => $this->input->post('proposal_id'),
                'status' => (($this->input->post('submit_draft') == 'Y') ? 'Draft' : 'Perlu Verifikasi'),
                'proposal_jadwal_id' => $this->input->post('proposal_jadwal_id'),
            )
        );

        if ($_FILES['dok_kurva']['name'] != '') {
            $api['post_field']['dok_kurva'] = new CurlFile($_FILES['dok_kurva']['tmp_name'], $_FILES['dok_kurva']['type'], $_FILES['dok_kurva']['name']);
        }


        $response = $this->serviceAPI($api);


        if ($response && $response['kode'] == 200) {

            foreach ($this->input->post('bobot') as $k => $v) {
                $api = array(
                    'endpoint' => 'setProposalJadwalKurva',
                    'method' => 'POST',
                    'controller' => 'revitalisasi',
                    'post_field' => array(
                        'token' => $this->token,
                        'email' => $this->session->userdata('email'),
                        'proposal_jadwal_kurva_id' => $this->input->post('kurva_id')[$k],
                        'proposal_jadwal_id' => $response['data']['proposal_jadwal_id'],
                        'master_jadwal_id' => $k,
                        'bobot' => $v,
                        'biaya' => $this->input->post('biaya')[$k],
                        'pengerjaan' => json_encode(($this->input->post('week')[$k]))
                    )
                );

                $this->serviceAPI($api);
            }
        }
        redirect(base_url() . 'web/revitalisasi/jadwal/' . $this->input->post('uuid'));
    }

    function pantauanFisik($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);

        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];

        $this->data['title'] = 'Revitalisasi - Pantauan Fisik ' . $this->data['proposal']['judul'] . '';
        $this->data['menu'] = 'pantauan_fisik';
        $this->data['js'] = 'revitalisasi/pantauan_fisik_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/pantauan_fisik', $this->data);
    }

    function pantauanFisikAdd($uuid)
    {
        $this->data['uuid'] = $uuid;
        $uuid = base64_decode($uuid);
        $this->data['proposal_id'] = $this->encryption->decrypt($uuid);


        $api = array(
            'endpoint' => 'getProposal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['proposal'] = $this->serviceAPI($api)['data'][0];

        // $api = array(
        //     'endpoint' => 'getProposalJadwal',
        //     'method' => 'POST',
        //     'controller' => 'revitalisasi',
        //     'post_field' => array(
        //         'token' => $this->token,
        //         'proposal_id' => $this->encryption->decrypt($uuid),
        //         'limit' => 0,
        //         'offset' => 0
        //     )
        // );

        // $this->data['proposal_pantauan_fisik'] = $this->serviceAPI($api)['data'][0];

        // $con = array(
		// 	'table_name'	=> 'revitalisasi.tbl_proposal_pantauan_fisik',
		// 	'where'	=> array(
		// 		'proposal_id'	=> $this->encryption->decrypt($uuid)
		// 	),
		// );
		// $data = $this->baseben->get($con);

        // if (isset($this->data['proposal_jadwal']['kurva'])) {
        //     $master_jadwal_id = array_column($this->data['proposal_jadwal']['kurva'], 'master_jadwal_id');
        //     $this->data['kurva_bobot'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'bobot'));
        //     $this->data['kurva_biaya'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'biaya'));
        //     $this->data['kurva_pengerjaan'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'pengerjaan'));
        //     $this->data['kurva_pengerjaan'] = array_map(function ($json) {
        //         return json_decode($json, true);
        //     }, $this->data['kurva_pengerjaan']);
        //     $this->data['kurva_id'] = array_combine($master_jadwal_id, array_column($this->data['proposal_jadwal']['kurva'], 'proposal_jadwal_kurva_id'));
        // }
//-----
        $con = array(
			'table_name'	=> 'revitalisasi.tbl_proposal_pantauan_fisik',
			'where'	=> array(
				'proposal_id'	=> $this->encryption->decrypt($uuid)
			),
		);
		$this->data['proposal_pantauan_fisik'] = $this->baseben->get($con);
        
        $conk = array(
			'table_name'	=> 'revitalisasi.tbl_proposal_pantauan_fisik_kurva',
			'where'	=> array(
				'proposal_pantauan_fisik_id'	=> $this->data['proposal_pantauan_fisik'][0]['proposal_pantauan_fisik_id']
			),
		);
        
		$this->data['proposal_pantauan_fisik']['kurva']  = $this->baseben->get($conk);
        // $this->data['proposal_pantauan_fisik'] = $this->serviceAPI($api)['data'][0];

        if (isset($this->data['proposal_pantauan_fisik'])) {
            $master_jadwal_id = array_column($this->data['proposal_pantauan_fisik']['kurva'], 'master_jadwal_id');
            $this->data['kurva_bobot'] = array_combine($master_jadwal_id, array_column($this->data['proposal_pantauan_fisik']['kurva'], 'bobot'));
            $this->data['kurva_biaya'] = array_combine($master_jadwal_id, array_column($this->data['proposal_pantauan_fisik']['kurva'], 'biaya'));
            $this->data['kurva_pengerjaan'] = array_combine($master_jadwal_id, array_column($this->data['proposal_pantauan_fisik']['kurva'], 'pengerjaan'));
            $this->data['kurva_pengerjaan'] = array_map(function ($json) {
                return json_decode($json, true);
            }, $this->data['kurva_pengerjaan']);
            $this->data['kurva_id'] = array_combine($master_jadwal_id, array_column($this->data['proposal_pantauan_fisik'], 'proposal_jadwal_kurva_id'));
        }
        //------
        $api = array(
            'endpoint' => 'getProposalKontrak',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'proposal_id' => $this->encryption->decrypt($uuid),
                'tipe_pengadaan' => 'Pelaksanaan',
                'limit' => 1,
                'offset' => 0
            )
        );

        $this->data['kontrak'] = $this->serviceAPI($api)['data'][0];

        $api = array(
            'endpoint' => 'getMasterJadwal',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'limit' => 30,
                'offset' => 0
            )
        );

        $this->data['ms_jadwal'] = $this->serviceAPI($api)['data'];

        $this->data['title'] = 'Revitalisasi - Pantuan Fisik ' . $this->data['proposal']['judul'] . '';
        $this->data['menu'] = 'pantuanFisik';
        $this->data['js'] = 'revitalisasi/pantuan_fisik_add_js';
        $this->data['menu_id'] = 77;
        $this->templateGovCMS('revitalisasi/pantuan_fisik_add', $this->data);
    }
    
    
}
