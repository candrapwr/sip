<?php

use function PHPSTORM_META\map;

defined('BASEPATH') or exit('No direct script access allowed');

class Pasar extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();
    }

    public function getPasarJSON()
    {
        $response = array('data' => null, 'code' => 200, 'success' => false);

        if ($this->input->post('action_index') != null) {
            $response['data'] = $this->session->userdata('index_pasar');
            $response['action_index'] = $this->input->post('action_index');
            $response['success'] = true;

            echo json_encode($response);
            exit();
        }

        $exp = explode('|', $this->input->post('provinsi_id'));
        $id = $exp[0];

        if ($id != 1) {
            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('daerah_id' => $id, 'token' => $this->token, 'limit' =>  600, 'offset' => 0)
            );

            $response_pasar = $this->serviceAPI($api);

            $temp = array();

            if (!$response_pasar) {
                $response['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $temp[$key]['pasar_id'] = $row['pasar_id'];
                    $temp[$key]['nama'] = $row['nama'];
                }

                $response['data'] = $temp;
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getPengelolaJSON()
    {
        $response = array('data' => null, 'code' => 200, 'success' => false);

        $prov_id = $this->input->post('provinsi_id');

        if ($prov_id != null) {
            $api = array(
                'endpoint'      => 'getPengguna',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => $prov_id, 'token' => $this->token, 'limit' =>  600, 'offset' => 0, 'tipe_pengguna' => 'Pengelola/Petugas')
            );

            $response_pasar = $this->serviceAPI($api);

            $temp = array();

            if (!$response_pasar) {
                $response['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $temp[$key]['email'] = $row['email'];
                    $temp[$key]['nama_lengkap'] = $row['nama_lengkap'];
                }

                $response['data'] = $temp;
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function saveDetail()
    {

        // var_dump($this->token);die();

        $this->form_validation->set_rules('phone', 'Nomor Telephone', 'trim|numeric');
        $this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');
        $this->form_validation->set_rules('tipe_pasar', 'Tipe Pasar', 'trim');
        $this->form_validation->set_rules('klasifikasi', 'Klasifikasi', 'trim|required');
        $this->form_validation->set_rules('bentuk_pasar', 'Bentuk Pasar', 'trim|required');
        $this->form_validation->set_rules('kepemilikan', 'Kepemilikan', 'trim|required');
        $this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required');
        $this->form_validation->set_rules('pengelola', 'Pengelola Pasar', 'trim|required');
        $this->form_validation->set_rules('waktu_operasional[]', 'Waktu Operasional', 'trim');
        $this->form_validation->set_rules('jam_buka', 'Jam Buka', 'trim');
        $this->form_validation->set_rules('jam_tutup', 'Jam Tutup', 'trim');
        $this->form_validation->set_rules('jam_sibuk_awal', 'Jam Mulai Sibuk', 'trim');
        $this->form_validation->set_rules('jam_sibuk_akhir', 'Jam Berakhir Sibuk', 'trim');
        $this->form_validation->set_rules('pekerja_tetap', 'Pekerja Tetap', 'trim|required');
        $this->form_validation->set_rules('pekerja_nontetap', 'Pekerja Non Tetap', 'trim|required');
        $this->form_validation->set_rules('luas_bangunan', 'Luas Bangunan', 'trim|required');
        $this->form_validation->set_rules('luas_tanah', 'Luas Tanah', 'trim|required');
        $this->form_validation->set_rules('jumlah_lantai', 'Jumlah Lantai', 'trim|required|integer');
        $this->form_validation->set_rules('tahun_bangun', 'Tahun Dibangun', 'trim|required|integer|exact_length[4]');
        $this->form_validation->set_rules('tahun_renovasi', 'Tahun Renovasi', 'trim|required|integer|exact_length[4]');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');



        $waktu_operasional_array = null;

        if ($this->input->post('waktu_operasional') != null) {
            foreach ($this->input->post('waktu_operasional', true) as $key => $value) {
                if ($value == 1) {
                    $waktu_operasional_array[] = 'Senin';
                } else if ($value == 2) {
                    $waktu_operasional_array[] = 'Selasa';
                } else if ($value == 3) {
                    $waktu_operasional_array[] = 'Rabu';
                } else if ($value == 4) {
                    $waktu_operasional_array[] = 'Kamis';
                } else if ($value == 5) {
                    $waktu_operasional_array[] = 'Jumat';
                } else if ($value == 6) {
                    $waktu_operasional_array[] = 'Sabtu';
                } else if ($value == 7) {
                    $waktu_operasional_array[] = 'Minggu';
                }
            }

            $waktu_operasional_array = implode(',', $waktu_operasional_array);
        }

        if ($this->input->post('bentuk_pasar') == '1') {
            $bentuk_pasar = 'Permanen';
        } else if ($this->input->post('bentuk_pasar') == '2') {
            $bentuk_pasar = 'Semi Permanen';
        } else if ($this->input->post('bentuk_pasar') == '3') {
            $bentuk_pasar = 'Tanpa Bangunan';
        }

        // if ($this->input->post('kepemilikan') == '1') {
        //     $kepemilikan = 'Pemerintah Daerah';
        // } else if ($this->input->post('kepemilikan') == '2') {
        //     $kepemilikan = 'Desa Adat';
        // } else if ($this->input->post('kepemilikan') == '3') {
        //     $kepemilikan = 'Swasta';
        // }

        if ($this->input->post('kondisi') == '1') {
            $kondisi = 'Baik';
        } else if ($this->input->post('kondisi') == '2') {
            $kondisi = 'Rusak Berat';
        } else if ($this->input->post('kondisi') == '3') {
            $kondisi = 'Rusak Ringan';
        } else if ($this->input->post('kondisi') == '4') {
            $kondisi = 'Rusak Sedang';
        }

        // if ($this->input->post('pengelola') == '1') {
        //     $pengelola = 'Pemerintah';
        // } else if ($this->input->post('pengelola') == '2') {
        //     $pengelola = 'BUMD';
        // } else if ($this->input->post('pengelola') == '3') {
        //     $pengelola = 'Swasta';
        // }

        $post_field = array(
            'token' => $this->token,
            'nama_pasar'    => $this->input->post('pasar_nama'),
            'pasar_id'  => $this->input->post('pasar_id'),
            'no_telp'   => $this->input->post('phone'),
            'no_fax'    => $this->input->post('fax'),
            'jenis_pasar'   => $this->input->post('klasifikasi'),
            'tipe_pasar_id' => $this->input->post('tipe_pasar'),
            'bentuk_pasar'  => $bentuk_pasar,
            // 'kepemilikan'   => $kepemilikan,
            'kondisi'   => $kondisi,
            // 'pengelola_pasar'   => $pengelola,
            'waktu_operasional' => $waktu_operasional_array,
            'jam_operasional_awal'  => ($this->input->post('jam_buka') != '') ? date("H:i:s", strtotime($this->input->post('jam_buka'))) : null,
            'jam_operasional_akhir' => ($this->input->post('jam_tutup') != '') ? date("H:i:s", strtotime($this->input->post('jam_tutup'))) : null,
            'jam_sibuk_awal'    => ($this->input->post('jam_sibuk_awal') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_awal'))) : null,
            'jam_sibuk_akhir'  => ($this->input->post('jam_sibuk_akhir') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_akhir'))) : null,
            'jumlah_pekerja_tetap'  => str_replace('.', '', $this->input->post('pekerja_tetap')),
            'jumlah_pekerja_nontetap'   => str_replace('.', '', $this->input->post('pekerja_nontetap')),
            'luas_bangunan' => str_replace('.', '', $this->input->post('luas_bangunan')),
            'luas_tanah'    => str_replace('.', '', $this->input->post('luas_tanah')),
            'jumlah_lantai' => str_replace('.', '', $this->input->post('jumlah_lantai')),
            'tahun_bangun'  => $this->input->post('tahun_bangun'),
            'tahun_renovasi' => $this->input->post('tahun_renovasi'),
            'jumlah_pengunjung' => $this->input->post('jumlah_pengunjung'),
            'jarak_toko_modern' => $this->input->post('jarak_toko_modern'),
            'penerapan_digitalisasi_pasar' => $this->input->post('penerapan_digitalisasi_pasar'),
            'metode_penerapan_digital_pasar' => is_array($this->input->post('metode_penerapan_digital_pasar')) ? implode(',', $this->input->post('metode_penerapan_digital_pasar')) : $this->input->post('metode_penerapan_digital_pasar'),
            'pembiayaan_lk' => $this->input->post('pembiayaan_lembaga_keuangan'),
            'lembaga_keuangan' => empty($this->input->post('lembaga_keuangan')) ? null : $this->input->post('lembaga_keuangan'),
            'sertifikasi_sni' => $this->input->post('sertifikasi_sni'),
            'tahun_sni_pasar_rakyat' => empty($this->input->post('tahun_sni_pasar_rakyat')) ? null : $this->input->post('tahun_sni_pasar_rakyat'),
            'sarana_prasarana' => $this->input->post('sarana_prasarana'),
            'angkutan_umum' => $this->input->post('angkutan_umum'),
            'jarak_pemukiman' => $this->input->post('dekat_pemukiman'),
            'email' => $this->session->userdata('email')
        );

        if (!empty($this->input->post('pasar_detail'))) {
            $post_field['pasar_detail_id'] = $this->input->post('pasar_detail');
        }

        $api = array(
            'endpoint'      => 'setDetailPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        //var_dump($service);die();

        if (!$service) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $service['keterangan']);
        } else if ($service && $service['kode'] != 200) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $service['keterangan']);
        } else {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', $service['keterangan']);
        }

        redirect(base_url() . 'dashboard/pasar/detail/' . $this->input->post('action_index'));
    }

    public function saveKios()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('nama', 'Nama Pasar', 'trim|required');
        $this->form_validation->set_rules('jumlah_bangunan', 'Jumlah Bangunan', 'trim|required|numeric');
        $this->form_validation->set_rules('jumlah_pedagang', 'Jumlah Pedagang', 'trim|required|numeric');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {
            if ($this->input->post('jenis_bangunan') == '1') {
                $jenis_bangunan = 'Los';
            } else if ($this->input->post('jenis_bangunan') == '2') {
                $jenis_bangunan = 'Kios';
            } else if ($this->input->post('jenis_bangunan') == '3') {
                $jenis_bangunan = 'Dasaran';
            }

            $pasar_bangunan_id = (!empty($this->input->post('pasar_bangunan_id'))) ? $this->input->post('pasar_bangunan_id') : '';

            $temp_kios = array();
            $temp_bangunan_id = array();

            if ($this->session->userdata('data_kios')) {
                $session_kios = $this->session->userdata('data_kios')[1]['data'];

                foreach ($session_kios as $kios) {
                    $temp_kios[] = $kios['jenis'];
                    $temp_bangunan_id[] = $kios['pasar_bangunan_id'];
                }
            }

            $this->data['response']['available'] = false;

            if (in_array($jenis_bangunan, $temp_kios)) {
                $key = array_search($jenis_bangunan, $temp_kios);
                $pasar_bangunan_id = $temp_bangunan_id[$key];
            }

            $post_field = array(
                'token' => $this->token,
                'jenis'    => $jenis_bangunan,
                'jumlah'  => str_replace('.', '', $this->input->post('jumlah_bangunan', true)),
                'jumlah_pedagang'   => str_replace('.', '', $this->input->post('jumlah_pedagang', true)),
                'nama_pasar'    => $this->input->post('nama'),
                'pasar_id'   => $this->input->post('pasar_id'),
                'email' => $this->session->userdata('email'),
                'pasar_bangunan_id' => $pasar_bangunan_id
            );

            $api = array(
                'endpoint'      => 'setBangunanPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function saveAnggaran()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'trim|required|numeric|exact_length[4]');
        $this->form_validation->set_rules('omset_anggaran', 'Omset', 'trim|required');

        if ($this->input->post('jenis_anggaran') == 'pasar') {
            $this->form_validation->set_rules('jumlah_anggaran', 'Jumlah Anggaran', 'trim|required');
            $this->form_validation->set_rules('program_pasar', 'Program Pasar', 'trim|required');
        } else {
            $this->form_validation->set_rules('bulan_anggaran', 'Bulan Anggaran', 'trim|required');
        }

        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_anggaran_id = (!empty($this->input->post('pasar_anggaran_id'))) ? $this->input->post('pasar_anggaran_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'nama_pasar'  => $this->input->post('nama', TRUE),
                'tahun'   => $this->input->post('tahun_anggaran', TRUE),
                'omset'    => str_replace('.', '', $this->input->post('omset_anggaran', TRUE)),
                'email' => $this->session->userdata('email'),
                'jenis_anggaran' => $this->input->post('jenis_anggaran'),
            );

            if ($this->input->post('jenis_anggaran') == 'pasar') {
                $post_field['anggaran'] = str_replace('.', '', $this->input->post('jumlah_anggaran', TRUE));
                $post_field['program_pasar_id'] = $this->input->post('program_pasar', TRUE);
                $post_field['pasar_anggaran_id'] = $pasar_anggaran_id;
            } else {
                $post_field['bulan'] = $this->input->post('bulan_anggaran', TRUE);
                $post_field['pasar_anggaran_pedagang_id'] = $pasar_anggaran_id;
            }

            $api = array(
                'endpoint'      => 'setAnggaranPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function sebaran()
    {
        $kab_id = 1;

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        } else {
            $kab_id = 0;
        }

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('daerah_id' => $kab_id, 'token' => $this->token, 'limit' => 20000, 'offset' => 0)
        );

        if (!empty($this->input->post('pengelola'))) {
            $api['post_field']['email'] = $this->input->post('pengelola');
            $this->session->set_flashdata('filter_search_pengelola', $this->input->post('pengelola'));
        }

        if (!empty($this->input->post('kondisi'))) {
            $kondisi = '';
            if ($this->input->post('kondisi') == 1) {
                $kondisi = 'Baik';
            } else if ($this->input->post('kondisi') == 2) {
                $kondisi = 'Rusak Berat';
            } else if ($this->input->post('kondisi') == 3) {
                $kondisi = 'Rusak Ringan';
            }

            $api['post_field']['kondisi'] = $kondisi;
            $this->session->set_flashdata('filter_search_kondisi', $this->input->post('kondisi'));
        }

        if (!empty($this->input->post('kepemilikan'))) {
            $kepemilikan = '';
            if ($this->input->post('kepemilikan') == 1) {
                $kepemilikan = 'Pemerintah Daerah';
            } else if ($this->input->post('kepemilikan') == 2) {
                $kepemilikan = 'Desa Adat';
            } else if ($this->input->post('kepemilikan') == 3) {
                $kepemilikan = 'Swasta';
            }

            $api['post_field']['kepemilikan'] = $kepemilikan;
            $this->session->set_flashdata('filter_search_kepemilikan', $this->input->post('kepemilikan'));
        }

        if (!empty($this->input->post('bentuk'))) {
            $bentuk = '';
            if ($this->input->post('bentuk') == 1) {
                $bentuk = 'Permanen';
            } else if ($this->input->post('bentuk') == 2) {
                $bentuk = 'Semi Permanen';
            } else if ($this->input->post('bentuk') == 3) {
                $bentuk = 'Tanpa Bangunan';
            }

            $api['post_field']['bentuk_pasar'] = $bentuk;

            $this->session->set_flashdata('filter_search_bentuk', $this->input->post('bentuk'));
        }

        if (!empty($this->input->post('provinsi'))) {
            $api['post_field']['daerah_id'] = $this->input->post('provinsi');

            $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
        }

        if (!empty($this->input->post('kabupaten'))) {
            $api['post_field']['daerah_id'] = $this->input->post('kabupaten');
            $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
        }

        if (!empty($this->input->post('tipe_pasar'))) {
            $api['post_field']['tipe_pasar_id'] = $this->input->post('tipe_pasar');
            $this->session->set_flashdata('filter_search_tipe', $this->input->post('tipe_pasar'));
        }

        $response_pasar = $this->serviceAPI($api);

        foreach ($response_pasar['data'] as $k => $v) {
            $response_pasar['data'][$k]['pasar_id'] = $this->jwtEncode($v['pasar_id']);
        }

        $this->data['maps_pasar'] = null;

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->data['maps_pasar'] = $response_pasar['data'];
        }
        // echo"<pre>";
        // print_r($this->data['maps_pasar']);die();

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
            $this->data['kab'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('prov_latlong');
        } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {

            if (strpos(strtolower($this->session->userdata('kab_kota')), 'kab') !== false) {
                $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kabupaten ' . $kab;
            } else if (strpos(strtolower($this->session->userdata('kab_kota')), 'kota') !== false) {
                $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kota ' . $kab;
            }

            $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('kab_latlong');
        } else {
            $this->data['latlong_map'] = '-0.789275, 113.921327';
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        if (!$this->session->userdata('tipe_pasar')) {
            $api = array(
                'endpoint'      => 'getTipePasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response['data']);
            }
        }

        //iNiT Layout NeW SiSP
        $this->data['title'] = 'Sebaran Pasar';
        $this->data['menu'] = 'sebaranpasar';
        $this->data['subtitle'] = 'Berikut adalah Halaman Sebaran Pasar SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'sebaran_pasar';
        $this->data['css'] = true;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function detail($id)
    {

        $split_url = explode('-', $id);
        $length = count($split_url) - 1;
        $pasar_id = $split_url[0];



        $uri_segment = 0;
        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('pasar_id' => $pasar_id, 'limit' => 1, 'offset' => 0, 'token' => $this->token)
        );

        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {
            if (strtolower($this->uri->segment(1)) == 'sebaran-pasar') {
                $uri_segment = 2;
            } else {
                $uri_segment = 1;
            }


            $this->session->set_userdata('index_pasar', $response['data'][0]);

            $api = array(
                'endpoint'      => 'getDetailPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->session->userdata('index_pasar')['pasar_id'], 'token' => $this->token)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('detail_pasar', $response_pasar['data']);
            } else if ($response_pasar && $response_pasar['kode'] == 404) {
                $this->session->set_userdata('detail_pasar', null);
            }

            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
            );

            $response_jenis = $this->serviceAPI($api);

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('jenis_komoditi', $response_jenis['data']);
            }

            $api = array(
                'endpoint'      => 'getTipePasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array()
            );

            $response_tipe = $this->serviceAPI($api);

            if (!$response_tipe) {
                redirect('user/sign_out');
            } else if ($response_tipe && $response_tipe['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response_tipe['data']);
            }

            $api = array(
                'endpoint'      => 'getProgramPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array()
            );

            $response_program_pasar = $this->serviceAPI($api);

            if (!$response_program_pasar) {
                redirect('user/sign_out');
            } else if ($response_program_pasar && $response_program_pasar['kode'] == 200) {
                $this->session->set_userdata('program_pasar', $response_program_pasar['data']);
            }

            $api = array(
                'endpoint'      => 'getPasar_pengelola',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->session->userdata('index_pasar')['pasar_id'],
                    'token' => $this->token,
                    'limit' => 1,
                    'offset' => 0
                )
            );

            $response_pengelola = $this->serviceAPI($api);

            if (!$response_pengelola) {
                redirect('user/sign_out');
            } else if ($response_pengelola && $response_pengelola['kode'] == 200) {
                $this->session->set_userdata('pasar_pengelola', $response_pengelola['data']);
            } else if ($response_pengelola && $response_pengelola['kode'] == 404) {
                $this->session->set_userdata('pasar_pengelola', null);
            }

            $api = array(
                'endpoint'      => 'getPasar_paguyupan',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->session->userdata('index_pasar')['pasar_id'],
                    'token' => $this->token,
                    'limit' => 1,
                    'offset' => 0
                )
            );

            $response_paguyuban = $this->serviceAPI($api);

            //var_dump($response_paguyuban);die();

            if (!$response_paguyuban) {
                redirect('user/sign_out');
            } else if ($response_paguyuban && $response_paguyuban['kode'] == 200) {
                $this->session->set_userdata('pasar_paguyuban', $response_paguyuban['data']);
            } else if ($response_paguyuban && $response_paguyuban['kode'] == 404) {
                $this->session->set_userdata('pasar_paguyuban', null);
            }

            $api = array(
                'endpoint'      => 'getPedagang',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->session->userdata('index_pasar')['pasar_id'],
                    'token' => $this->token,
                    'limit' => 1,
                    'offset' => 0
                )
            );

            $response_pedagang = $this->serviceAPI($api);

            if (!$response_pedagang) {
                redirect('user/sign_out');
            } else if ($response_pedagang && $response_pedagang['kode'] == 200) {
                $this->session->set_userdata('pasar_pedagang', $response_pedagang['data']);
            } else if ($response_pedagang && $response_pedagang['kode'] == 404) {
                $this->session->set_userdata('pasar_pedagang', null);
            }

            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => 'api',
                'post_field'    => array(
                    'daerah_id' => substr($response['data'][0]['daerah_id'], 0, 4),
                )
            );

            $response_kecamatan = $this->serviceAPI($api);

            if (!$response_kecamatan) {
                redirect('user/sign_out');
            } else if ($response_kecamatan && $response_kecamatan['kode'] == 200) {
                $this->session->set_userdata('data_kecamatan', $response_kecamatan['data']);
            } else if ($response_kecamatan && $response_kecamatan['kode'] == 404) {
                $this->session->set_userdata('data_kecamatan', null);
            }

            $this->data['index_pasar'] = $this->session->userdata('index_pasar');
            $this->data['detail_pasar'] = $this->session->userdata('detail_pasar');
            $this->data['jenis_komoditi'] = $this->session->userdata('jenis_komoditi');
            $this->data['tipe_pasar'] = $this->session->userdata('tipe_pasar');
            $this->data['program_pasar'] = $this->session->userdata('program_pasar');
            $this->data['pasar_pengelola'] = $this->session->userdata('pasar_pengelola');
            $this->data['pasar_paguyuban'] = $this->session->userdata('pasar_paguyuban');
            $this->data['pasar_pedagang'] = $this->session->userdata('pasar_pedagang');
            $this->data['active_index'] = $id;
            $this->data['maps_pasar'] = $this->session->userdata('maps_pasar');
            $this->data['kecamatan'] = $this->session->userdata('data_kecamatan');
            $this->data['uri_segment'] = $uri_segment;

            //iNiT Layout NeW SiSP
            $this->data['title'] = $this->data['index_pasar']['nama'];
            $this->data['menu'] = 'pasar';
            $this->data['subtitle'] = 'Berikut adalah Halaman Detail ' . $this->data['index_pasar']['nama'];
            $this->data['namafile'] = 'pasar_detail';
            $this->data['css'] = true;
            $this->data['js'] = true;
            $this->load->view('frontend/structure/index', $this->data);
        }
    }

    public function update()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('name_update', 'Nama', 'trim|required');
        $this->form_validation->set_rules('description_update', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('address_update', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('post_code_update', 'Kode Pos', 'trim|required|integer');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }

            $this->data['response']['success'] = false;
        } else {
            $post_field = array(
                'token' => $this->token,
                'pasar_id'  => $this->input->post('pasar_id_updatee'),
                'email' => $this->session->userdata('email', true),
                'nama'  => $this->input->post('name_update', true),
                'deskripsi'  => $this->input->post('description_update'),
                'alamat'  => $this->input->post('address_update'),
                'latitude'  => $this->input->post('latitude_update'),
                'longitude'  => $this->input->post('longitude_update'),
                'daerah_id'  => $this->input->post('daerah_id_update'),
                'kode_pos'  => $this->input->post('post_code_update'),
            );


            if ($_FILES['image_font_update']['tmp_name'] != '')
                $post_field['foto_depan'] = new CurlFile($_FILES['image_font_update']['tmp_name'], $_FILES['image_font_update']['type'], $_FILES['image_font_update']['name']);

            if ($_FILES['image_inside_update']['tmp_name'] != '')
                $post_field['foto_dalam'] = new CurlFile($_FILES['image_inside_update']['tmp_name'], $_FILES['image_inside_update']['type'], $_FILES['image_inside_update']['name']);


            $api = array(
                'endpoint'      => 'setPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {

                $api = array(
                    'endpoint'      => 'getPasar',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => array('token' => $this->token, 'limit' => 1, 'offset' => 0, 'status' => 'Aktif', 'pasar_id' => $this->input->post('pasar_id_updatee'))
                );

                $service = $this->serviceAPI($api);

                if (!$service) {
                    $this->data['response']['code'] = 401;
                    $this->data['response']['success'] = false;
                } else if ($service && $service['kode'] == 200) {
                    if ($this->input->post('uri_type') == 1) {
                        $sess_pasar_active = $this->session->userdata('data_pasar')[$this->session->userdata('page_pasar')]['data'];

                        $temp[$this->session->userdata('page_pasar')]['data'] = array();

                        foreach ($sess_pasar_active as $key => $row) {
                            if ($key == $this->input->post('pasar_update_index')) {
                                $temp[$this->session->userdata('page_pasar')]['data'][$key] = $service['data'][0];

                                $temp[$this->session->userdata('page_pasar')]['data'][$key]['action'] = $row['action'];
                            } else {
                                $temp[$this->session->userdata('page_pasar')]['data'][$key] = $row;
                            }
                        }

                        $this->session->set_userdata('data_pasar', $temp);

                        $this->data['response']['success'] = true;
                        $this->data['response']['action_index'] = $this->input->post('pasar_update_index');
                        $this->data['response']['uri_segment'] = $this->input->post('uri_type');
                    } else if ($this->input->post('uri_type') == 2) {
                        // $this->session->set_userdata('maps_pasar');

                        $kab_id = 1;

                        if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
                            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
                        } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
                        } else {
                            $kab_id = 0;
                        }

                        $api = array(
                            'endpoint'      => 'getPasar',
                            'method'        => 'POST',
                            'controller'    => 'pasar',
                            'post_field'    => array('daerah_id' => $kab_id, 'token' => $this->token, 'limit' => 20000, 'offset' => 0)
                        );

                        $response_pasar = $this->serviceAPI($api);

                        $this->session->set_userdata('maps_pasar', $response_pasar['data']);

                        $this->data['response']['success'] = true;
                        $this->data['response']['action_index'] = $this->input->post('pasar_update_index');
                        $this->data['response']['uri_segment'] = $this->input->post('uri_type');
                    }
                }
            }
        }

        echo json_encode($this->data['response']);
    }

    public function getDetailJSON()
    {
        $this->data['data'] = array();

        $index = $this->input->post('id');

        if ($index != null) {

            $api = array(
                'endpoint'      => 'getDetailPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $index, 'token' => $this->token)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                $this->data['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('detail_pasar', $response_pasar['data']);
            } else if ($response_pasar && $response_pasar['kode'] == 404) {
                $this->session->set_userdata('detail_pasar', null);
            }

            if ($this->session->userdata('detail_pasar') != null) {
                $this->data['data'] = $this->session->userdata('detail_pasar')[0];

                $this->data['data']['jam_operasional_awal'] = ($this->data['data']['jam_operasional_awal'] != null) ? date('h:i A', strtotime($this->data['data']['jam_operasional_awal'])) : null;
                $this->data['data']['jam_operasional_akhir'] = ($this->data['data']['jam_operasional_akhir'] != null) ? date('h:i A', strtotime($this->data['data']['jam_operasional_akhir'])) : null;
                $this->data['data']['jam_sibuk_awal'] = ($this->data['data']['jam_sibuk_awal'] != null) ? date('h:i A', strtotime($this->data['data']['jam_sibuk_awal'])) : null;
                $this->data['data']['jam_sibuk_akhir'] = ($this->data['data']['jam_sibuk_akhir'] != null) ? date('h:i A', strtotime($this->data['data']['jam_sibuk_akhir'])) : null;
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);
                $this->data['data']['jumlah_pekerja_nontetap'] = rupiah($this->data['data']['jumlah_pekerja_nontetap']);
                $this->data['data']['luas_bangunan'] = rupiah($this->data['data']['luas_bangunan']);
                $this->data['data']['luas_tanah'] = rupiah($this->data['data']['luas_tanah']);
                $this->data['data']['jumlah_lantai'] = rupiah($this->data['data']['jumlah_lantai']);
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);

                $split_operasional = explode(',', $this->data['data']['waktu_operasional']);

                $waktu_operasional_array = array();

                foreach ($split_operasional as $key => $value) {
                    if (strtolower($value) == 'senin') {
                        $waktu_operasional_array[] = 1;
                    } else if (strtolower($value) == 'selasa') {
                        $waktu_operasional_array[] = 2;
                    } else if (strtolower($value) == 'rabu') {
                        $waktu_operasional_array[] = 3;
                    } else if (strtolower($value) == 'kamis') {
                        $waktu_operasional_array[] = 4;
                    } else if (strtolower($value) == 'jumat' || strtolower($value) == "jum'at") {
                        $waktu_operasional_array[] = 5;
                    } else if (strtolower($value) == 'sabtu') {
                        $waktu_operasional_array[] = 6;
                    } else if (strtolower($value) == 'minggu') {
                        $waktu_operasional_array[] = 7;
                    }
                }

                $this->data['data']['waktu_operasional'] = $waktu_operasional_array;
            }
        }

        echo json_encode($this->data);
    }


    public function getKiosJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != null) {
            echo json_encode($this->session->userdata('data_kios')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('data_kios')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('data_kios')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getBangunanPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->input->post('id'), 'token' => $this->token, 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')))
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['data_kios'][$pagenumber] = $response;

                foreach ($session['data_kios'][$pagenumber]['data'] as $key => $row) {
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah_pedagang'] = rupiah($row['jumlah_pedagang']);
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);

                    $session['data_kios'][$pagenumber]['data'][$key]['action'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['data_kios'][$pagenumber]['data'][$key]['action'] = true;
                    }
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('data_kios')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function getAnggaranJSON()
    {
        $code = 200;
        $data = array();

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_anggaran')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_anggaran')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_anggaran')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getAnggaranPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'jenis_anggaran' => $this->input->post('jenis_anggaran'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_anggaran'][$pagenumber] = $response;

                foreach ($session['detail_anggaran'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_anggaran'][$pagenumber]['data'][$key]['omset'] = rupiah($row['omset']);
                    $session['detail_anggaran'][$pagenumber]['data'][$key]['anggaran'] = rupiah($row['anggaran']);

                    $session['detail_anggaran'][$pagenumber]['data'][$key]['action'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['detail_anggaran'][$pagenumber]['data'][$key]['action'] = true;
                    }
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_anggaran')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    function deleteKios()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {

            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_bangunan',
                'key_name'  => 'pasar_bangunan_id',
                'key'   => $this->session->userdata('data_kios')[$pagenumber]['data'][$id]['pasar_bangunan_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }

        echo json_encode($this->data['response']);
    }

    function deleteAnggaran()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {
            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_anggaran',
                'key_name'  => 'pasar_anggaran_id',
                'key'   => $this->session->userdata('detail_anggaran')[$pagenumber]['data'][$id]['pasar_anggaran_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }

        echo json_encode($this->data['response']);
    }

    public function getVarianKomoditi()
    {
        $response = array();

        if ($this->input->post('jenis_komoditi_id') != null) {
            $split_jenis_komoditi = explode('|', $this->input->post('jenis_komoditi_id'));

            $api = array(
                'endpoint'      => 'getVarianKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $split_jenis_komoditi[0])
            );

            $response_varian = $this->serviceAPI($api);

            if (!$response_varian) {
                $response['success'] = false;
                $response['code'] = 401;
            } else if ($response_varian && $response_varian['kode'] == 200) {
                $this->session->set_userdata('varian_komoditi', $response_varian['data']);
                $response['data'] = $this->session->userdata('varian_komoditi');
                $response['success'] = true;
            }
        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function getSatuanKomoditi()
    {
        $response = array();

        if ($this->input->post('jenis_komoditi_id') != null) {
            $split_jenis_komoditi = explode('|', $this->input->post('jenis_komoditi_id'));

            $api = array(
                'endpoint'      => 'getSatuanKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $split_jenis_komoditi[0])
            );

            $response_satuan = $this->serviceAPI($api);

            if (!$response_satuan) {
                $response['success'] = false;
                $response['code'] = 401;
            } else if ($response_satuan && $response_satuan['kode'] == 200) {
                $this->session->set_userdata('satuan_komoditi', $response_satuan['data']);
                $response['data'] = $this->session->userdata('satuan_komoditi');
                $response['success'] = true;
            }
        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function saveFoto()
    {

        $apiGet = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('pasar_id' => $this->input->post('pasar_id_ft'), 'limit' => 1, 'offset' => 0, 'token' => $this->token)
        );

        $getDataP = $this->serviceAPI($apiGet);


        $foto_lain = [];
        foreach ($_FILES['files']['tmp_name'] as $key => $value) {
            //echo $key;
            $foto_lain[] = new CurlFile($value, $_FILES['files']['type'][$key], $_FILES['files']['name'][$key]);
        }

        if (count(@$getDataP['data']) == 1) {
            $post_field = array(
                'token' => $this->token,
                'pasar_id'  => $this->input->post('pasar_id_ft'),
                'email' => $this->session->userdata('email', true),
                'nama'  => @$getDataP['data'][0]['nama'],
                'deskripsi'  => @$getDataP['data'][0]['deskripsi'],
                'alamat'  => @$getDataP['data'][0]['alamat'],
                'latitude'  => @$getDataP['data'][0]['latitude'],
                'longitude'  => @$getDataP['data'][0]['longitude'],
                'daerah_id'  => @$getDataP['data'][0]['daerah_id'],
                'kode_pos'  => @$getDataP['data'][0]['kode_pos'],
            );

            foreach ($_FILES['files']['tmp_name'] as $key => $value) {

                $post_field['foto_lainnya[]'] = new CurlFile($value, $_FILES['files']['type'][$key], $_FILES['files']['name'][$key]);

                $api = array(
                    'endpoint'      => 'setPasar',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => $post_field
                );

                $service = $this->serviceAPI($api);
                //print_r($service);          
            }
        }
    }

    public function delFoto()
    {

        $api = array(
            'endpoint'      => 'setStatus',
            'method'        => 'POST',
            'post_field'    => array(
                'email' => $this->session->userdata('email'),
                'token' => $this->token,
                'table_name' => 'tbl_pasar_foto',
                'key_name' => 'pasar_foto_id',
                'key' => $this->input->post('foto_id'),
                'status' => 'Deleted',
                'keterangan_perubahan' => 'Hapus foto pasar lainya'
            )
        );

        $response = $this->serviceAPI($api);

        echo json_encode($response);
    }

    public function getBongkar_muat()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_bongkar_muat')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_bongkar_muat')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_bongkar_muat')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getBongkar_muat',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'pasar_bongkar_muat_id' => $this->input->post('pasar_bongkar_muat_id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_bongkar_muat'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_bongkar_muat')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function saveBongkar_muat()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required|numeric|exact_length[4]');
        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_bongkar_muat_id = (!empty($this->input->post('pasar_bongkar_muat_id'))) ? $this->input->post('pasar_bongkar_muat_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'nama_pasar'  => $this->input->post('nama', TRUE),
                'bulan'   => $this->input->post('bulan', TRUE),
                'tahun'   => $this->input->post('tahun', TRUE),
                'jumlah'    => str_replace('.', '', $this->input->post('jumlah', TRUE)),
                'email' => $this->session->userdata('email'),
                'pasar_bongkar_muat_id' => $pasar_bongkar_muat_id
            );

            $api = array(
                'endpoint'      => 'setBongkar_muat',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    function deleteBongkar_muat()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {
            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_bongkar_muat',
                'key_name'  => 'pasar_bongkar_muat_id',
                'key'   => $this->session->userdata('detail_bongkar_muat')[$pagenumber]['data'][$id]['pasar_bongkar_muat_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }

        echo json_encode($this->data['response']);
    }

    public function savePasar_pengelola()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('klasifikasi_pengelola', 'Klasifikasi Pengelola', 'trim|required');
        $this->form_validation->set_rules('nama_pengelola', 'Nama Pengelola', 'trim|required');
        $this->form_validation->set_rules('no_telp_pengelola', 'No Telp Pengelola', 'trim|required');
        $this->form_validation->set_rules('tingkat_pendidikan', 'Tingkat Pendidikan', 'trim|required');
        $this->form_validation->set_rules('jumlah_staf', 'Jumlah Staf', 'trim|required');
        $this->form_validation->set_rules('nama_koperasi', 'Nama Koperasi', 'trim|required');
        $this->form_validation->set_rules('kategori_koperasi', 'Kategori Koperasi', 'trim|required');
        $this->form_validation->set_rules('jumlah_anggota_koperasi', 'Jumlah Anggota Koperasi', 'trim|required');
        $this->form_validation->set_rules('no_badan_hukum_koperasi', 'No Badan Hukum Koperasi', 'trim|required');
        $this->form_validation->set_rules('no_induk_koperasi', 'No Induk Koperasi', 'trim|required');
        $this->form_validation->set_rules('besaran_retribusi_kios', 'Besaran Retribusi Kios', 'trim|required');
        $this->form_validation->set_rules('besaran_retribusi_los', 'Besaran Retribusi Los', 'trim|required');
        $this->form_validation->set_rules('besaran_retribusi_dasaran', 'Besaran Retribusi Dasaran', 'trim|required');
        $this->form_validation->set_rules('pendapatan_tahunan_retribusi', 'Pendapatan Tahunan Retribusi', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        //var_dump($this->input->post());die();

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_pengelola_id = (!empty($this->input->post('pasar_pengelola_id'))) ? $this->input->post('pasar_pengelola_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id', TRUE),
                'klasifikasi_pengelola'  => $this->input->post('klasifikasi_pengelola', TRUE),
                'nama_pengelola'   => $this->input->post('nama_pengelola', TRUE),
                'no_telp_pengelola'   => $this->input->post('no_telp_pengelola', TRUE),
                'tingkat_pendidikan'   => $this->input->post('tingkat_pendidikan', TRUE),
                'jumlah_staf'   => $this->input->post('jumlah_staf', TRUE),
                'nama_koperasi'   => $this->input->post('nama_koperasi', TRUE),
                'kategori_koperasi'   => $this->input->post('kategori_koperasi', TRUE),
                'no_telp_koperasi'   => $this->input->post('no_telp_koperasi', TRUE),
                'jumlah_anggota_koperasi'   => $this->input->post('jumlah_anggota_koperasi', TRUE),
                'no_badan_hukum_koperasi'   => $this->input->post('no_badan_hukum_koperasi', TRUE),
                'no_induk_koperasi'   => $this->input->post('no_induk_koperasi', TRUE),
                'besaran_retribusi_kios'   =>  str_replace('.', '', $this->input->post('besaran_retribusi_kios', TRUE)),
                'besaran_retribusi_los'   =>  str_replace('.', '', $this->input->post('besaran_retribusi_los', TRUE)),
                'besaran_retribusi_dasaran'   =>  str_replace('.', '', $this->input->post('besaran_retribusi_dasaran', TRUE)),
                'pendapatan_tahunan_retribusi'   =>  str_replace('.', '', $this->input->post('pendapatan_tahunan_retribusi', TRUE)),
                'email' => $this->session->userdata('email'),
                'pasar_pengelola_id' => $pasar_pengelola_id
            );

            if ($_FILES['struktur_pengelola']['tmp_name'] != '') {
                $post_field['struktur_pengelola'] = new CurlFile($_FILES['struktur_pengelola']['tmp_name'], $_FILES['struktur_pengelola']['type'], $_FILES['struktur_pengelola']['name']);
            }

            $api = array(
                'endpoint'      => 'setPasar_pengelola',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function savePaguyuban()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('nama_paguyuban', 'Nama Paguyuban', 'trim|required');
        $this->form_validation->set_rules('nama_ketua', 'Nama Ketua', 'trim|required');
        $this->form_validation->set_rules('no_telp_ketua', 'No. Telp Ketua Paguyuban', 'trim|required');
        $this->form_validation->set_rules('asosiasi_perdagangan_pasar', 'Asosiasi Perdagangan Pasar', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_paguyuban_id = (!empty($this->input->post('pasar_paguyuban_id'))) ? $this->input->post('pasar_paguyuban_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'nama_paguyuban'  => $this->input->post('nama_paguyuban', TRUE),
                'nama_ketua'   => $this->input->post('nama_ketua', TRUE),
                'asosiasi_perdagangan_pasar'   => $this->input->post('asosiasi_perdagangan_pasar', TRUE),
                'no_telp_ketua'   => $this->input->post('no_telp_ketua', TRUE),
                'email' => $this->session->userdata('email'),
                'pasar_paguyuban_id' => $pasar_paguyuban_id
            );

            $api = array(
                'endpoint'      => 'setPasar_paguyuban',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function getPasokan_bapok()
    {
        $code = 200;
        $data = array();

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_pasokan_bapok')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_pasokan_bapok')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_pasokan_bapok')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPasokan_bapok',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            //var_dump($response);die();

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_pasokan_bapok'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_pasokan_bapok')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function savePasokan_bapok()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
        $this->form_validation->set_rules('jenis_komoditi_id', 'Jenis Komoditi', 'trim|required');
        $this->form_validation->set_rules('varian_komoditi_id', 'Varian Komoditi', 'trim|required');
        $this->form_validation->set_rules('satuan_komoditi_id', 'Satuan Komoditi', 'trim|required');
        $this->form_validation->set_rules('jumlah', 'Jumlah', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_pasokan_bapok_id = (!empty($this->input->post('pasar_pasokan_bapok_id'))) ? $this->input->post('pasar_pasokan_bapok_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'bulan'  => $this->input->post('bulan', TRUE),
                'tahun'   => $this->input->post('tahun', TRUE),
                'jenis_komoditi_id'   => $this->input->post('jenis_komoditi_id', TRUE),
                'varian_komoditi_id'   => $this->input->post('varian_komoditi_id', TRUE),
                'jumlah'   => $this->input->post('jumlah', TRUE),
                'satuan_id'   => $this->input->post('satuan_komoditi_id', TRUE),
                'email' => $this->session->userdata('email'),
                'pasar_pasokan_bapok_id' => $pasar_pasokan_bapok_id
            );

            $api = array(
                'endpoint'      => 'setPasar_pasokan_bapok',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function getRegulasi()
    {
        $code = 200;
        $data = array();

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_regulasi')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_regulasi')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getRegulasi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_regulasi'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function saveRegulasi()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('no_regulasi', 'No. Regulasi', 'trim|required');
        $this->form_validation->set_rules('nama_regulasi', 'Nama Regulasi', 'trim|required');
        $this->form_validation->set_rules('pengesah_regulasi', 'Pengesah Regulasi', 'trim|required');
        $this->form_validation->set_rules('tgl_regulasi', 'Tanggal Regulasi', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_regulasi_id = (!empty($this->input->post('pasar_regulasi_id'))) ? $this->input->post('pasar_regulasi_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'no_regulasi'  => $this->input->post('no_regulasi', TRUE),
                'nama_regulasi'   => $this->input->post('nama_regulasi', TRUE),
                'pengesah_regulasi'   => $this->input->post('pengesah_regulasi', TRUE),
                'tgl_regulasi'   => $this->input->post('tgl_regulasi', TRUE),
                'email' => $this->session->userdata('email'),
                'pasar_regulasi_id' => $pasar_regulasi_id
            );

            if ($_FILES['file_regulasi']['tmp_name'] != '') {
                $post_field['file_regulasi'] = new CurlFile($_FILES['file_regulasi']['tmp_name'], $_FILES['file_regulasi']['type'], $_FILES['file_regulasi']['name']);
            }

            $api = array(
                'endpoint'      => 'setRegulasi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function getPedagang()
    {
        $code = 200;
        $data = array();

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_pedagang')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_pedagang')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_pedagang')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPedagang',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_pedagang'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_pedagang')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function savePedagang()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('file_pedagang', 'File Pedagang', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_pedagang_id = (!empty($this->input->post('pasar_pedagang_id'))) ? $this->input->post('pasar_pedagang_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'email' => $this->session->userdata('email'),
                'pasar_pedagang_id' => $pasar_pedagang_id
            );

            if ($_FILES['file_pedagang']['tmp_name'] != '') {
                $post_field['file_pedagang'] = new CurlFile($_FILES['file_pedagang']['tmp_name'], $_FILES['file_pedagang']['type'], $_FILES['file_pedagang']['name']);
            }

            $api = array(
                'endpoint'      => 'setPedagang',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    function update_data()
    {
        $data_post = $this->input->post();

        $id = $data_post['id'];

        if ($id != null) {
            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => $data_post['table_name'],
                'key_name'  => $data_post['key_name'],
                'key'   => $id,
                'status'    => $data_post['status'],
                'keterangan_perubahan'  => 'Set status ' . $data_post['status'] . ' atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }

        echo json_encode($this->data['response']);
    }
}
