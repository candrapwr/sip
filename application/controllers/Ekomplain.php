<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekomplain extends MY_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if (!has_access(81, 'show')) {
            redirect(base_url() . '404_override');
        }

		$this->data['title']  = 'Daftar E-komplain';
		$this->data['js']     = 'ekomplain_js';
		$this->data['menu'] = 'e-komplain';
		$this->data['menu_id'] = 81;
		$this->templatebackend('ekomplain/index', $this->data);
	}

	function getEkomplain()
	{
		$pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

		if ($this->input->post('action_index') != '') {
			echo json_encode($this->session->userdata('ekomplain')[$pagenumber]['data'][$this->input->post('action_index')]);
			exit();
		}

		if (isset($this->session->userdata('ekomplain')[$pagenumber]) && empty($this->input->post('refresh'))) {
			$session_existing = $this->session->userdata('ekomplain')[$pagenumber];
			$session_existing['draw'] = $this->input->post('draw');
			echo json_encode($session_existing);
		} else {

			if ($this->session->userdata('tipe_pengguna_id') == 2) {
				$daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
			} else if (in_array($this->session->userdata('tipe_pengguna_id'), [9, 19])) {
				$daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
			} else if (in_array($this->session->userdata('tipe_pengguna_id'), [8, 18])) {
				$daerah_id = substr($this->session->userdata('daerah_id'), 0, 2);
			} else {
				$daerah_id = 0;
			}

			if (!empty($this->input->post('daerah_id'))) {
				$daerah_id = $this->input->post('daerah_id');
			}

			$api = array(
				'endpoint'      => 'getKomplain',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => array('daerah_id' => $daerah_id)
			);

			$response = $this->serviceAPI($api);
			$response['draw'] = $this->input->post('draw');

			if ($response && $response['kode'] == 200) {
				$session = $this->session->all_userdata();
				$session['ekomplain'][$pagenumber] = $response;
				foreach ($session['ekomplain'][$pagenumber]['data'] as $key => $row) {
					$session['ekomplain'][$pagenumber]['data'][$key]['tanggal'] = full_date($row['createdon']).', '.date('H:i:s', strtotime($row['createdon']));
					$session['ekomplain'][$pagenumber]['data'][$key]['token'] = $this->jwtEncode($row['no_tiket']);
				}
				$this->session->set_userdata($session);
				echo json_encode($this->session->userdata('ekomplain')[$pagenumber]);
			} else {
				echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
			}
		}
	}

	function detail($id)
	{
		$this->data['title']  = 'Detail E-komplain';
		$this->data['js']     = 'ekomplain_js';
		$this->data['menu'] = 'e-komplain';
		$this->data['menu_id'] = 81;
		$id = $this->jwtDecode($id);

		$api = array(
			'endpoint'      => 'getKomplain',
			'method'        => 'POST',
			'controller'    => 'pasar',
			'post_field'    => array('no_tiket' => $id)
		);

		$response = $this->serviceAPI($api);
		$this->data['data'] = $response['data'];

		$this->templatebackend('ekomplain/detail', $this->data);
	}

	function save_tanggapan()
	{
		$data_post = $this->input->post();

		if (!empty($data_post['spam'])) {
			$this->session->set_flashdata('alert', 'error');
			$this->session->set_flashdata('message', 'Data gagal disimpan');
		} else {

			$post_field = array(
				'no_tiket' => $data_post['tiket'],
				'status'    => $data_post['status'],
				'keterangan'    => $data_post['tanggapan'],
				'email' => $this->session->userdata('email'),
				'flag_pengelola'    => 'Y'
			);

			$api = array(
				'endpoint'      => 'setTracking',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => $post_field
			);

			$response = $this->serviceAPI($api);

			if ($response['kode'] == 200) {
				$this->session->set_flashdata('alert', 'success');
				$this->session->set_flashdata('message', 'Data berhasil disimpan');
			} else {
				$this->session->set_flashdata('alert', 'error');
				$this->session->set_flashdata('message', 'Data gagal disimpan');
			}
		}

		redirect(base_url() . 'web/e-komplain/detail/' . $this->jwtEncode($data_post['tiket']));
	}
}
