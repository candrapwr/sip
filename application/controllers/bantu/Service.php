<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Service extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Baseben_Model', 'baseben');
		$this->load->database(); 
	}

	public function coba()
	{
			$con = array(
				'table_name'	=> 'revitalisasi.tbl_proposal_log',
				// 'where'	=> array(
				// 	'pegawai_id'	=> $data_post['id']
				// ),
			);

			$get = $this->baseben->get($con);
			
			echo json_encode(array('data' => $get));
		
	}
	function setPantauanFisikAdd()
    {
        //  pre($this->input->post());
        // var_dump($this->input->post());die;
        $apis = array(
            // 'endpoint' => 'setPantauanFisikAdd',
            // 'method' => 'POST',
            // 'controller' => 'revitalisasi',
            // 'post_field' => array(
            //     'token' => $this->token,
            //     'email' => $this->session->userdata('email'),
            //     'proposal_id' => $this->input->post('proposal_id'),
            //     'status' => (($this->input->post('submit_draft') == 'Y') ? 'Draft' : 'Perlu Verifikasi'),
            //     'proposal_jadwal_id' => $this->input->post('proposal_jadwal_id'),
            // )
            'table_name' => 'revitalisasi.tbl_proposal_pantauan_fisik',
            'proposal_id' => $this->input->post('proposal_id'),
            'tgl_pengajuan' => $this->input->post('tgl_pengajuan'),
            'status' => (($this->input->post('submit_draft') == 'Y') ? 'Draft' : 'Perlu Verifikasi'),
        );
        // if ($_FILES['dok_kurva']['name'] != '') {
        //     $apis['dok_kurva'] = new CurlFile($_FILES['dok_kurva']['tmp_name'], $_FILES['dok_kurva']['type'], $_FILES['dok_kurva']['name']);
        // }        
        if (!empty($this->input->post('proposal_pantauan_fisik_id'))) {
            $apis['proposal_pantauan_fisik_id'] = $this->input->post('proposal_pantauan_fisik_id');
        }
        $response = $this->baseben->insert_id($apis);;
		
		$con = array(
			'table_name'	=> 'revitalisasi.tbl_proposal_pantauan_fisik',
			'where'	=> array(
				'proposal_pantauan_fisik_id'	=> $response
			),
		);
		$data = $this->baseben->get($con);
        // $response = $this->serviceAPI($api);

        if ($response) {			
            foreach ($this->input->post('bobot') as $k => $v) {
                $api = array(
                //     'endpoint' => 'setPantauanFisikAddKurva',
                //     'method' => 'POST',
                //     'controller' => 'revitalisasi',
                //     'post_field' => array(
                //         'token' => $this->token,
                //         'email' => $this->session->userdata('email'),
                //         'proposal_jadwal_kurva_id' => $this->input->post('kurva_id')[$k],
                //         'proposal_jadwal_id' => $response['data']['proposal_jadwal_id'],
                //         'master_jadwal_id' => $k,
                //         'bobot' => $v,
                //         'biaya' => $this->input->post('biaya')[$k],
                //         'pengerjaan' => json_encode(($this->input->post('week')[$k]))
                //     )
                'table_name' => 'revitalisasi.tbl_proposal_pantauan_fisik_kurva',                
                'proposal_pantauan_fisik_id' => $data[0]['proposal_pantauan_fisik_id'],
                'master_jadwal_id' => $k,
                'bobot' => $v,
                'biaya' => $this->input->post('biaya')[$k],
                'pengerjaan' => json_encode(($this->input->post('week')[$k]))
                );
				if (!empty($this->input->post('kurva_id')[$k])) {				
					$api['proposal_pantauan_fisik_kurva_id'] = $this->input->post('kurva_id')[$k];
				}
                $response = $this->baseben->insert($api);
                // $this->serviceAPI($api);
            }
        }
        var_dump($this->encryption->decrypt($this->input->post('uuid')));die;
        redirect(base_url() . 'web/revitalisasi/pantauan_fisik/' . $this->input->post('uuid'));
    }
    function setPantauanFisik()
    {

        //  pre($this->input->post());

        $api = array(
            'endpoint' => 'setPantauanFisik',
            'method' => 'POST',
            'controller' => 'revitalisasi',
            'post_field' => array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'proposal_id' => $this->input->post('proposal_id'),
                'status' => (($this->input->post('submit_draft') == 'Y') ? 'Draft' : 'Perlu Verifikasi'),
                'proposal_jadwal_id' => $this->input->post('proposal_jadwal_id'),
            )
        );

        if ($_FILES['dok_kurva']['name'] != '') {
            $api['post_field']['dok_kurva'] = new CurlFile($_FILES['dok_kurva']['tmp_name'], $_FILES['dok_kurva']['type'], $_FILES['dok_kurva']['name']);
        }


        $response = $this->serviceAPI($api);


        if ($response && $response['kode'] == 200) {

            foreach ($this->input->post('bobot') as $k => $v) {
                $api = array(
                    'endpoint' => 'setPantauanFisikAddKurva',
                    'method' => 'POST',
                    'controller' => 'revitalisasi',
                    'post_field' => array(
                        'token' => $this->token,
                        'email' => $this->session->userdata('email'),
                        'proposal_pantauan_fisik_kurva_id' => $this->input->post('kurva_id')[$k],
                        'proposal_pantauan_fisik_id' => $response['data']['proposal_pantauan_fisik_id'],
                        'master_jadwal_id' => $k,
                        'bobot' => $v,
                        'biaya' => $this->input->post('biaya')[$k],
                        'pengerjaan' => json_encode(($this->input->post('week')[$k]))
                    )
                );

                $this->serviceAPI($api);
            }
        }
        redirect(base_url() . 'web/revitalisasi/jadwal/' . $this->input->post('uuid'));
    }
}
