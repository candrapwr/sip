<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('cookie');

        if (empty($this->session->userdata('token'))) {
            //SSO Intra Baru
            $token = get_cookie('sso_token');
            if (!empty($token)) {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://intra.kemendag.go.id/auth/cekToken',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 10,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'POST',
                    CURLOPT_POSTFIELDS => array('token' => $token),
                    CURLOPT_HTTPHEADER => array(
                        'x-api-key: fmH0W9NpOtj4WHoxROGKuv3HC7wmshQ0UkG3baCt'
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                $response = json_decode($response);

                if ($response != null) {
                    if ($response->kode == 200) {
                        $this->createSession($response->data->nip);
                    }
                }
            }
        }
    }

    public function index()
    {

        $api = array(
            'endpoint'      => 'getMasterDaerah',
            'method'        => 'POST',
            'controller'    => '',
            'post_field'    => array('daerah_id' => 1)
        );

        $response = $this->serviceAPI($api);

        $date1 = date('Y-m-d', strtotime('last weekday'));
        $date2 =date('Y-m-d', strtotime('last weekday', strtotime('yesterday')));

        $api = array(
            'endpoint'      => 'getMasterProvinsi',
            'method'        => 'POST',
            'post_field'    => array()
        );

        $master_provinsi_ews = $this->ewsAPI($api);

        $this->data['provinces'] = ($response['kode'] == 200) ? $response['data'] : array();
        $this->data['date1'] = date('d M Y', strtotime($date1));
        $this->data['date2'] = date('d M Y', strtotime($date2));
        $this->data['val_date1'] = $date1;
        $this->data['val_date2'] = $date2;

        $this->data['ews_provinces'] = ($master_provinsi_ews['kode'] == 200) ? $master_provinsi_ews['data'] : array();

        $this->data['urlparams'] = ':?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no&:origin=false&:showShareOptions=false&:toolbar=no';
        $this->data['view_tableau'] = '/views/SebaranPasar/SebaranPasar';
        $this->data['token'] = $this->getTableauToken('PDSIView');


        $kab_id = 0;

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('daerah_id' => $kab_id, 'limit' => 20000, 'offset' => 0)
        );


        if (!empty($this->input->post('provinsi'))) {
            $api['post_field']['daerah_id'] = $this->input->post('provinsi');

            $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
        }

        if (!empty($this->input->post('kabupaten'))) {
            $api['post_field']['daerah_id'] = $this->input->post('kabupaten');
            $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
        }

        $response_pasar = $this->serviceAPI($api);

        $this->data['maps_pasar'] = null;

        if (!$response_pasar) {
            // redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->data['maps_pasar'] = $response_pasar['data'];
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                //  redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        //iNiT Layout NeW SiSP
        $this->data['title'] = 'Beranda';
        $this->data['menu'] = 'beranda';
        $this->data['subtitle'] = 'Berikut adalah Halaman Beranda SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'beranda';
        $this->data['css'] = true;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }


    function createSession($nip)
    {
        $post_field = array(
            'nipx' => $nip
        );

        $api = array(
            'endpoint'      => 'checkLogin',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);
        // var_dump($api);die();
        if ($service && $service['kode'] == 200) {

            $data = $service['data'][0];

            $email  = sha1($data['email']);

            $token  = str_replace($email, '', $data['token']);
            $token  = substr($token, 0, 40);

            $role_lainnya = [];
            $role_lainnya_nama = explode(',', $data['tipe_pengguna_lainnya_nama']);
            foreach (explode(',', $data['tipe_pengguna_lainnya']) as $k => $v) {
                $role_lainnya[$v] = $role_lainnya_nama[$k];
            }

            $session = array(
                'pengguna_id'   => $data['pengguna_id'],
                'nama'          => $data['nama_lengkap'],
                'daerah_id'     => $data['daerah_id'],
                'prov_latlong'     => $data['prov_latlong'],
                'kab_latlong'     => $data['kab_latlong'],
                'role'          => $data['tipe_pengguna'],
                'nama_dinas'          => $data['nama_dinas'],
                'tipe_pengguna_id'     => $data['tipe_pengguna_id'],
                'email'         => $data['email'],
                'token'         => $token,
                'kab_kota'      => $data['kab_kota'],
                'provinsi'  => $data['provinsi'],
                'role_lainnya'  => $role_lainnya,
            );

            $this->token = $token;
            $this->session->set_userdata($session);

            if (strtolower($data['tipe_pengguna']) == 'administrator') {

                $api = array(
                    'endpoint'      => 'getMenu',
                    'method'        => 'POST',
                    'controller'   => 'api',
                    'post_field'    => array(
                        'jenis' => 'Back end',
                        'parent_null' => 'Y',
                        'urutan' => 'ASC'
                    )
                );

                $response = $this->serviceAPI($api);

                foreach ($response['data'] as $k => $v) {
                    $api_ = array(
                        'endpoint'      => 'getMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'jenis' => 'Back end',
                            'parent_id' => $v['menu_id'],
                            'urutan' => 'ASC'
                        )
                    );

                    $response_ = $this->serviceAPI($api_);

                    $response['data'][$k]['sub_menu'] = $response_;
                }

                if ($response && $response['kode'] == 200) {
                    $this->session->set_userdata('menu', $response);
                }

                redirect('web/dashboard');
            } else {

                $api = array(
                    'endpoint'      => 'getTipePenggunaMenu',
                    'method'        => 'POST',
                    'controller'   => 'api',
                    'post_field'    => array(
                        'tipe_pengguna_id' => $data['tipe_pengguna_id'],
                        'parent_null' => 'Y',
                        'urutan' => 'ASC'
                    )
                );

                $response = $this->serviceAPI($api);

                foreach ($response['data'] as $k => $v) {
                    $api_ = array(
                        'endpoint'      => 'getTipePenggunaMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'tipe_pengguna_id' => $data['tipe_pengguna_id'],
                            'parent_id' => $v['menu_id'],
                            'urutan' => 'ASC'
                        )
                    );

                    $response_ = $this->serviceAPI($api_);

                    $response['data'][$k]['sub_menu'] = $response_['data'];
                }

                if ($response && $response['kode'] == 200) {
                    $this->session->set_userdata('menu', $response);
                    $menu = array();
                    $sub_menu = array();
                    foreach ($response['data'] as $k => $v) {
                        $menu[$v['menu_id']] = array(
                            'create' => $v['create'],
                            'update' => $v['update'],
                            'delete' => $v['delete']
                        );
                        foreach ($response['data'][$k]['sub_menu'] as $kk => $vv) {
                            $sub_menu[$vv['menu_id']] = array(
                                'create' => $vv['create'],
                                'update' => $vv['update'],
                                'delete' => $vv['delete']
                            );
                        }
                    }
                    $this->session->set_userdata('perms_menu', $menu);
                    $this->session->set_userdata('perms_sub_menu', $sub_menu);
                }


                if (strtolower($data['tipe_pengguna']) == 'kontributor') {
                    redirect('404_override');
                } else {
                    redirect('dashboard');
                }
            }

            redirect(base_url());
        }
    }

    function getTableauToken($username)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://analitik.kemendag.go.id/trusted/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'username=' . $username,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    function kebijakan_privasi()
    {
        //iNiT Layout NeW SiSP
        $this->data['title']  = 'Kebijakan Privasi';
        $this->data['menu'] = 'kebijakan-privasi';
        $this->data['subtitle'] = 'Berikut adalah Halaman Kebijakan Privasi SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'kebijakan_privasi';
        $this->data['css'] = false;
        $this->data['js'] = false;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function buntu()
    {
        //iNiT Layout NeW SiSP
        $this->data['title'] = '404 Halaman Tidak Ditemukan';
        $this->data['menu'] = '404';
        $this->data['subtitle'] = 'Berikut adalah Halaman 404 SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'sebatasteman';
        $this->data['css'] = false;
        $this->data['js'] = false;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function emailexample()
    {
        $this->data['title']  = 'Contoh Email Verifikasi';
        $this->data['menu'] = 'contohemail';
        $this->load->view('email/contoh', $this->data);
    }

    public function emailexample2()
    {
        $this->data['title']  = 'Verifikasi Email Berhasil';
        $this->data['menu'] = 'contohemail2';
        $this->load->view('email/contoh2', $this->data);
    }

    public function emailexample3()
    {
        $this->data['title']  = 'Email HERO';
        $this->data['menu'] = 'contohemail3';
        $this->load->view('email/contoh3', $this->data);
    }

    function getDaerah()
    {
        $data_post = $this->input->post();

        if (!empty($data_post['daerah_id'])) {
            $data = $data_post['daerah_id'];
        } else {
            $data = 1;
        }

        $api = array(
            'endpoint'      => 'getMasterDaerah',
            'method'        => 'POST',
            'post_field'    => array('daerah_id' => $data)
        );

        $data = $this->serviceAPI($api);

        echo json_encode($data);
    }

    function reset_password($token = null)
    {
        if ($token != null) {
            //iNiT Layout NeW SiSP
            $this->data['title'] = 'Reset Password Baru';
            $this->data['subtitle'] = 'Berikut adalah Halaman Reset Password Baru SISP Kementerian Perdagangan Republik Indonesia';
            $this->data['namafile'] = 'resetpassword_token';
        } else {
            //iNiT Layout NeW SiSP
            $this->data['title'] = 'Reset Password';
            $this->data['subtitle'] = 'Berikut adalah Halaman Reset Password SISP Kementerian Perdagangan Republik Indonesia';
            $this->data['namafile'] = 'resetpassword';
        }
        $this->data['menu'] = 'resetpassword';
        $this->data['css'] = false;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }

    function cek_reset_password()
    {
        $data_post = $this->input->post();
        $captcha = $this->input->post('g-recaptcha-response');
        $secretKey = "6Lco8w4gAAAAAN35Sn7k7tRH4E3FbfEx1hQTMv2Y";
        $ip = $_SERVER['REMOTE_ADDR'];
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $captcha . "&remoteip=" . $ip);
        $responseKeys = json_decode($response, true);

        if (intval($responseKeys["success"]) !== 1) {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', 'Reset Password Gagal, silahkan coba lagi');
            redirect(base_url() . 'reset-password/');
        }

        if (!empty($data_post['xyz'])) {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', 'Reset Password Gagal, silahkan coba lagi');
            redirect(base_url() . 'reset-password');
        }

        $api = array(
            'endpoint'      => 'lupaPassword',
            'method'        => 'POST',
            'post_field'    => array(
                'email' => $data_post['email'],
            )
        );

        $service = $this->serviceAPI($api);

        if ($service && $service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Reset Password Berhasil, silahkan cek email anda <b>' . $data_post['email'] . '</b>');
            redirect(base_url() . 'reset-password');
        } else {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', 'Reset Password Gagal, silahkan coba lagi');
            redirect(base_url() . 'reset-password');
        }
    }

    function save_password()
    {
        $data_post = $this->input->post();
        $captcha = $this->input->post('g-recaptcha-response');
        $secretKey = "6Lco8w4gAAAAAN35Sn7k7tRH4E3FbfEx1hQTMv2Y";
        $ip = $_SERVER['REMOTE_ADDR'];
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $secretKey . "&response=" . $captcha . "&remoteip=" . $ip);
        $responseKeys = json_decode($response, true);

        if (intval($responseKeys["success"]) !== 1) {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', 'Reset Password Gagal, silahkan coba lagi aaaa');
            redirect(base_url() . 'reset-password/' . $data_post['token']);
        }

        if (!empty($data_post['xyz'])) {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', 'Reset Password Gagal, silahkan coba lagi bbbb');
            redirect(base_url() . 'reset-password/' . $data_post['token']);
        }

        $api = array(
            'endpoint'      => 'setPassword',
            'method'        => 'POST',
            'post_field'    => array(
                'token' => $data_post['token'],
                'password' => $data_post['password'],
                'confirm_password'  => $data_post['confirm_password']
            )
        );

        $service = $this->serviceAPI($api);

        ///var_dump($service);die();

        if ($service && $service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Reset Password Berhasil, silahkan login menggunakan password baru anda');
            redirect(base_url() . 'user/sign_in');
        } else {
            $this->session->set_flashdata('alert', 'warning');
            $this->session->set_flashdata('message', $service['keterangan']);
            redirect(base_url() . 'reset-password/' . $data_post['token']);
        }
    }
}
