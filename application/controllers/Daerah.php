<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daerah extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKabupaten()
    {
        $response = array('success' => false, 'data' => null, 'code' => 200);

        $province_id = $this->input->post('provinsi_id');

        if ($province_id != null) {

            $exp = explode('|', $province_id);

            $province_id = $exp[0];

            if ($province_id != 1) {
                $api = array(
                    'endpoint'      => 'getMasterDaerah',
                    'method'        => 'POST',
                    'post_field'    => array('daerah_id' => $province_id)
                );

                $kabupaten_api = $this->serviceAPI($api);

                if (!$kabupaten_api) {
                    $response['code'] = 401;
                } else if ($kabupaten_api && $kabupaten_api['kode'] == 200) {
                    $response['data'] = $kabupaten_api['data'];
                    $response['success'] = true;
                }
            }
        }

        echo json_encode($response);
    }

    public function getKecamatan()
    {
        $response = array('success' => false, 'data' => null);

        $kabupaten_id = $this->input->post('kabupaten_id');

        if ($kabupaten_id != null) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => $kabupaten_id)
            );

            $kecamatan_api = $this->serviceAPI($api);

            if ($kecamatan_api && $kecamatan_api['kode'] == 200) {
                $response['data'] = $kecamatan_api['data'];
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getKabupaten1()
    {
        $response = array('success' => false, 'data' => null, 'code' => 200);

        $province_id = $this->input->post('province_id');

        if ($province_id != null) {

            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => $province_id)
            );

            $kabupaten_api = $this->serviceAPI($api);

            if (!$kabupaten_api) {
                $response['code'] = 401;
            } else if ($kabupaten_api && $kabupaten_api['kode'] == 200) {
                $response['data'] = $kabupaten_api['data'];
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getKecamatan1()
    {
        $response = array('success' => false, 'data' => null);

        $kabupaten_id = $this->input->post('kabupaten_id');

        if ($kabupaten_id != null) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => $kabupaten_id)
            );

            $kecamatan_api = $this->serviceAPI($api);

            if ($kecamatan_api && $kecamatan_api['kode'] == 200) {
                $response['data'] = $kecamatan_api['data'];
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getEWSKabupaten()
    {
        $response = array('success' => false, 'data' => null);

        $province_id = $this->input->post('provinsi_id');

        if ($province_id != null) {
            $api = array(
                'endpoint'      => 'getMasterKota',
                'method'        => 'POST',
                'post_field'    => array('provinsi_id' => $province_id)
            );

            $kabupaten_api = $this->ewsAPI($api);

            if ($kabupaten_api['kode'] == 200) {
                foreach ($kabupaten_api['data'] as $key => $row) {
                    $kabupaten_api['data'][$key]['id'] = strtolower($row['locationname']);
                }
            }

            $response['data'] = ($kabupaten_api['kode'] == 200) ? $kabupaten_api['data'] : array();
            $response['success'] = true;
        }

        echo json_encode($response);
    }

    function getDaerah()
    {
        $response = array('data' => []);


        $data_post = $this->input->post();

        if (!empty($data_post['daerah_id'])) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => $data_post['daerah_id'])
            );

            $data = $this->serviceAPI($api);

            if ($data['kode'] == 200) {
                $response['data'] = $data['data'];
            }
        }

        echo json_encode($response);
    }
}
