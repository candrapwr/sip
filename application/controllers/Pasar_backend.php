<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pasar_backend extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_admin();
        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = 'Management Pasar';
        $this->data['js']     = 'backend/js/pasar_backend_js';
        $this->data['menu'] = 'pasar_backend';

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => 1)
            );

            $province_api = $this->serviceAPI($api);

            if ($province_api && $province_api['kode'] == 200) {
                $province = $province_api['data'];

                $this->session->set_userdata('master_provinsi', $province);
            }
        }

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->templatebackend('backend/pasar_backend', $this->data);
    }

    public function pasar_json()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pasar_backend')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pasar_backend')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('pasar_backend')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('token' => $this->token, 'daerah_id' => $this->input->post('daerah_id'), 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')))
            );
            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pasar_backend'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pasar_backend')[$pagenumber]);
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    public function detailpasar_json()
    {

        if (empty($this->session->userdata('refresh'))) {
            $api = array(
                'endpoint'      => 'getDetailPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->input->post('pasar_id'), 'token' => $this->token)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('detail_pasar', $response_pasar['data']);
            } else if ($response_pasar && $response_pasar['kode'] == 404) {
                $this->session->set_userdata('detail_pasar', $response_pasar['keterangan']);
            }
        } else {
            $this->session->userdata('detail_pasar');
        }

        $this->data['data'] = array();
        if ($this->session->userdata('detail_pasar')) {
            $this->data['data'] = $this->session->userdata('detail_pasar')[0];
            if (!empty($this->data['data']['pasar_id'])) {
                $this->data['data']['pasar_id'] = $this->data['data']['pasar_id'];
                // $this->data['data']['detail_pasar_id'] = $this->data['data']['detail_pasar_id'];
                $this->data['data']['jam_operasional_awal'] = date('h:i A', strtotime($this->data['data']['jam_operasional_awal']));
                $this->data['data']['jam_operasional_akhir'] = date('h:i A', strtotime($this->data['data']['jam_operasional_akhir']));
                $this->data['data']['jam_sibuk_awal'] = date('h:i A', strtotime($this->data['data']['jam_sibuk_awal']));
                $this->data['data']['jam_sibuk_akhir'] = date('h:i A', strtotime($this->data['data']['jam_sibuk_akhir']));
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);
                $this->data['data']['waktu_operasional'] = $this->data['data']['waktu_operasional'];
                $this->data['data']['jumlah_pekerja_nontetap'] = rupiah($this->data['data']['jumlah_pekerja_nontetap']);
                $this->data['data']['luas_bangunan'] = rupiah($this->data['data']['luas_bangunan']);
                $this->data['data']['luas_tanah'] = rupiah($this->data['data']['luas_tanah']);
                $this->data['data']['jumlah_lantai'] = rupiah($this->data['data']['jumlah_lantai']);
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);
            } else {
                $this->data['code'] = 404;
            }
        } else {
            $this->data['code'] = 401;
        }

        echo json_encode($this->data);
    }
}
