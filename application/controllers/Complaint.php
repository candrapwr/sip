<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->is_login();
	}

	public function index()
	{
		$this->data['title']  = 'E-Komplain';
		$this->data['menu'] = 'ekomplain';
		$this->data['js'] = 'public_government/js/ekomplain';

		$daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);

		$api = array(
			'endpoint'      => 'getKomplain',
			'method'        => 'POST',
			'controller'    => 'pasar',
			'post_field'    => array('daerah_id' => $daerah_id)
		);

		$response = $this->serviceAPI($api);

		$complaints = array();

		if (!$response) {
			redirect('user/sign_out');
		} else if ($response && $response['kode'] == 200) {
			$complaints = $response['data'];

			$temp_complaints = array();

			foreach ($complaints as $key => $complaint) {
				$slug = slugify($complaint['komplain'], $complaint['komplain_id']);

				$temp_complaints[$slug] = $complaint;
			}

			$this->session->set_userdata('temp_complaints', $temp_complaints);
		} else {
			$this->session->set_userdata('temp_complaints', null);
		}

		$this->data['complaints'] = $complaints;

		$this->template('public_government/page/ekomplain', $this->data);
	}

	public function detail($id = null)
	{
		if ($id != null) {
			if (array_key_exists($id, $this->session->userdata('temp_complaints'))) {
				$this->data['title']  = 'Detail E-Komplain';
				$this->data['menu'] = 'detailekomplain';
				$this->data['js'] = 'public_government/js/detailekomplain';

				$detail_complaint = $this->session->userdata('temp_complaints')[$id];

				$this->data['detail_complaint'] = $detail_complaint;

				$this->session->set_userdata('trackings', $detail_complaint['tracking']);

				$this->template('public_government/page/detailekomplain', $this->data);
			} else {
				redirect('404_override');
			}
		} else {
			redirect('404_override');
		}
	}

	public function save()
	{
		$response = array('success' => false, 'validation' => null, 'code' => 200);

		$this->form_validation->set_rules('status', 'Status', 'required|trim');
		$this->form_validation->set_rules('tanggapan', 'Tanggapan', 'required|trim');
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if (!$this->form_validation->run()) {
			foreach ($this->input->post() as $key => $value) {
				$response['validation'][$key] = form_error($key);
			}
		} else {
			$status = 'CLOSE';

			if ($this->input->post('status') == 1) {
				$status = 'OPEN';
			} else if ($this->input->post('status') == 2) {
				$status = 'RESOLVED';
			}

			$post_field = array(
				'no_tiket' => $this->input->post('tiket'),
				'status'    => $status,
				'keterangan'    => $this->input->post('tanggapan'),
				'email' => $this->session->userdata('email'),
				'flag_pengelola'    => 'Y'
			);

			$api = array(
				'endpoint'      => 'setTracking',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => $post_field
			);

			$response_pasar = $this->serviceAPI($api);

			if (!$response_pasar) {
				$response['code'] = 401;
			} else {
				$response['data'] = array(
					'status' => $status,
					'keterangan'    => $this->input->post('tanggapan'),
					'email' => $this->session->userdata('email'),
					'email_in' => substr($this->session->userdata('email'), 0, 1),
					'date'  => full_date(date('Y-m-d'))
				);

				$response['success'] = true;
			}
		}

		echo json_encode($response);
	}

	public function getTracking()
	{
		$response = array('data' => array());

		$id = $this->input->post('id', true);

		if ($id != null) {
			if (array_key_exists($id, $this->session->userdata('temp_complaints'))) {
				$no_ticket = $this->session->userdata('temp_complaints')[$id]['no_tiket'];

				$api = array(
					'endpoint'      => 'getKomplain',
					'method'        => 'POST',
					'controller'    => 'pasar',
					'post_field'    => array('no_tiket' => $no_ticket)
				);

				$service = $this->serviceAPI($api);

				if ($service['kode'] == 200) {

					foreach ($service['data'][0]['tracking'] as $key => $row) {
						$service['data'][0]['tracking'][$key]['createdby_in'] = substr($row['createdby'], 0, 1);
						$service['data'][0]['tracking'][$key]['createdon'] = full_date($row['createdon']);

						if ($row['keterangan'] == null) {
							$service['data'][0]['tracking'][$key]['keterangan'] = '';
						}
					}

					$response['data'] = $service['data'][0]['tracking'];
				}
			}
		}

		echo json_encode($response);
	}
}
