<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (empty($this->token)) {
            redirect(base_url());
        }
    }

    public function index()
    {

        if (empty($this->input->get('filter')) && empty($this->input->get('page'))) {
            $array_items = array('filter_pasar');
            $this->session->unset_userdata($array_items);
        } elseif ($this->input->get('filter') != sha1(md5($this->session->userdata('username'))) && $this->input->get('page') == '') {
            redirect(base_url() . 'dashboard');
        }

        $this->data['title']  = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'dashboard_js';

        $batas = 6;
        $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;

        $pagenumber = ($halaman_awal + $batas) / $batas;

        $this->session->set_userdata('refresh', true);

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('token' => $this->token, 'limit' => $batas, 'offset' => $halaman_awal)
        );

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
            $api['post_field']['daerah_id'] = substr($this->session->userdata('daerah_id'), 0, 2);
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
            $api['post_field']['daerah_id'] = substr($this->session->userdata('daerah_id'), 0, 4);
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        } else {
            $daerah_id = 0;
            $kab_id = 0;

            if (!empty($this->session->userdata('filter_pasar')['filter_search_provinsi'])) {
                $daerah_id = $this->session->userdata('filter_pasar')['filter_search_provinsi'];

                if (!empty($this->session->userdata('filter_pasar')['filter_search_kabupaten'])) {
                    $daerah_id = $this->session->userdata('filter_pasar')['filter_search_kabupaten'];
                }
            }

            $api['post_field']['daerah_id'] = $daerah_id;
        }

        if ($this->input->post('submit')) {

            $ses = array();

            if (!empty($this->input->post('pengelola'))) {
                $api['post_field']['email'] = $this->input->post('pengelola');
                $ses['filter_search_pengelola'] = $this->input->post('pengelola');
            }

            if (!empty($this->input->post('kondisi'))) {
                $kondisi = '';
                if ($this->input->post('kondisi') == 1) {
                    $kondisi = 'Baik';
                } else if ($this->input->post('kondisi') == 2) {
                    $kondisi = 'Rusak Berat';
                } else if ($this->input->post('kondisi') == 3) {
                    $kondisi = 'Rusak Ringan';
                }

                $api['post_field']['kondisi'] = $kondisi;
                $ses['filter_search_kondisi'] = $this->input->post('kondisi');
            }

            if (!empty($this->input->post('kepemilikan'))) {
                $kepemilikan = '';
                if ($this->input->post('kepemilikan') == 1) {
                    $kepemilikan = 'Pemerintah Daerah';
                } else if ($this->input->post('kepemilikan') == 2) {
                    $kepemilikan = 'Desa Adat';
                } else if ($this->input->post('kepemilikan') == 3) {
                    $kepemilikan = 'Swasta';
                }

                $api['post_field']['kepemilikan'] = $kepemilikan;
                $ses['filter_search_kepemilikan'] = $this->input->post('kepemilikan');
            }

            if (!empty($this->input->post('bentuk'))) {
                $bentuk = '';
                if ($this->input->post('bentuk') == 1) {
                    $bentuk = 'Permanen';
                } else if ($this->input->post('bentuk') == 2) {
                    $bentuk = 'Semi Permanen';
                } else if ($this->input->post('bentuk') == 3) {
                    $bentuk = 'Tanpa Bangunan';
                }

                $api['post_field']['bentuk_pasar'] = $bentuk;
                $ses['filter_search_bentuk'] = $this->input->post('bentuk');
            }

            if (!empty($this->input->post('provinsi'))) {
                $api['post_field']['daerah_id'] = $this->input->post('provinsi');
                $ses['filter_search_provinsi'] = $this->input->post('provinsi');
            }

            if (!empty($this->input->post('kabupaten'))) {
                $api['post_field']['daerah_id'] = $this->input->post('kabupaten');
                $ses['filter_search_kabupaten'] = $this->input->post('kabupaten');
            }

            if (!empty($this->input->post('tipe_pasar'))) {
                $api['post_field']['tipe_pasar_id'] = $this->input->post('tipe_pasar');
                $ses['filter_search_tipe'] = $this->input->post('tipe_pasar');
            }

            $filter_session = array('filter_pasar' => $ses);
            $this->session->set_userdata($filter_session);
        } elseif (!empty($this->session->userdata('filter_pasar')['filter_search_pengelola']) || !empty($this->session->userdata('filter_pasar')['filter_search_kondisi']) || !empty($this->session->userdata('filter_pasar')['filter_search_kepemilikan']) || !empty($this->session->userdata('filter_pasar')['filter_search_bentuk']) || !empty($this->session->userdata('filter_pasar')['filter_search_tipe'])) {

            if (!empty($this->session->userdata('filter_pasar')['filter_search_pengelola'])) {
                $api['post_field']['email'] = $this->session->userdata('filter_pasar')['filter_search_pengelola'];
            }

            if (!empty($this->session->userdata('filter_pasar')['filter_search_kondisi'])) {
                $api['post_field']['kondisi'] = $this->session->userdata('filter_pasar')['filter_search_kondisi'];
            }

            if (!empty($this->session->userdata('filter_pasar')['filter_search_kepemilikan'])) {

                $api['post_field']['kepemilikan'] = $this->session->userdata('filter_pasar')['filter_search_kepemilikan'];
            }

            if (!empty($this->session->userdata('filter_pasar')['filter_search_bentuk'])) {
                $api['post_field']['bentuk_pasar'] = $this->session->userdata('filter_pasar')['filter_search_bentuk'];
            }

            if (!empty($this->session->userdata('filter_pasar')['filter_search_tipe'])) {
                $api['post_field']['tipe_pasar_id'] = $this->session->userdata('filter_pasar')['filter_search_tipe'];
            }
        }

        //Di dasboard tidak terpakai
        /*
        $response_pasar = $this->serviceAPI($api);

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            foreach ($response_pasar['data'] as $key => $row) {
                $response_pasar['data'][$key]['action'] = false;

                if ($row['createdby'] == $this->session->userdata('email')) {
                    $response_pasar['data'][$key]['action'] = true;
                }
            }

            $total_rows = $response_pasar['recordsFiltered'];
            $this->data['pagination'] = $this->mylib->pagination(site_url('dashboard'), $batas, $total_rows);

            $session['data_pasar'][$pagenumber] = $response_pasar;

            $this->session->set_userdata($session);
        } else {
            $this->session->set_userdata('data_pasar', null);
        }

        $this->session->set_userdata('page_pasar', $pagenumber);

        $api['post_field']['limit'] = 20000;
        $api['post_field']['offset'] = 0;

        $response_pasar = $this->serviceAPI($api);

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->session->set_userdata('maps_pasar', $response_pasar['data']);
        } else {
            $this->session->set_userdata('maps_pasar', null);
        }
        */

        if (strtolower($this->session->userdata('role')) != 'pengelola/petugas') {
            $date = date('Y-m-d');

            $kab = 'kosong';

            if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
                $kab = substr($this->session->userdata('daerah_id'), 0, 2);
            } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                $kab = substr($this->session->userdata('daerah_id'), 0, 4);
            } else {
                $kab = 0;
            }

            //Ganti Ajax
            //$this->data['harga_lengkap'] = $this->getSummaryLaporHarga($kab, $date, 'Lengkap');
            //$this->data['harga_belum_lengkap'] = $this->getSummaryLaporHarga($kab, $date, 'Belum Lengkap');
            //$this->data['harga_belum_lapor'] = $this->getSummaryLaporHarga($kab, $date, 'Belum Lapor');

            if (strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota' && strtolower($this->session->userdata('role')) != 'pengelola/petugas') {
                if (!$this->session->userdata('master_kabkota')) {
                    $api = array(
                        'endpoint'      => 'getMasterDaerah',
                        'method'        => 'POST',
                        'controller'    => '',
                        'post_field'    => array('daerah_id' => substr($this->session->userdata('daerah_id'), 0, 2))
                    );

                    $response = $this->serviceAPI($api);

                    if (!$response) {
                        redirect('user/sign_out');
                    } else {
                        $this->session->set_userdata('master_kabkota', $response['data']);
                    }
                }

                if (!$this->session->userdata('master_provinsi')) {
                    $api = array(
                        'endpoint'      => 'getMasterDaerah',
                        'method'        => 'POST',
                        'controller'    => '',
                        'post_field'    => array('daerah_id' => 1)
                    );

                    $response = $this->serviceAPI($api);

                    if (!$response) {
                        redirect('user/sign_out');
                    } else {
                        $this->session->set_userdata('master_provinsi', $response['data']);
                    }
                }
            }
        }

        $hari_ini = date('Y-m-d');

        //Ganti Ajax
        //$this->data['pasar_hari_ini'] = $this->summaryPasar(1, $hari_ini);
        //$this->data['pasar_bulan_ini'] = $this->summaryPasar(2);
        //$this->data['pasar_tahun_ini'] = $this->summaryPasar(3);
        $this->data['pasar'] = $this->session->userdata('data_pasar');
        $this->data['page'] = $pagenumber;
        $this->data['maps_pasar'] = $this->session->userdata('maps_pasar');

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
            $this->data['kab'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('prov_latlong');
        } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'pengelola/petugas') {

            if (strpos(strtolower($this->session->userdata('kab_kota')), 'kab') !== false) {
                $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kabupaten ' . $kab;
            } else if (strpos(strtolower($this->session->userdata('kab_kota')), 'kota') !== false) {
                $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kota ' . $kab;
            }

            $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('kab_latlong');
        } else {
            $this->data['latlong_map'] = '-0.789275, 113.921327';
        }

        if (!$this->session->userdata('tipe_pasar')) {
            $api = array(
                'endpoint'      => 'getTipePasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response['data']);
            }
        }

        if (!$this->session->userdata('pengelola_pasar')) {

            $api = array(
                'endpoint'      => 'getPengguna',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('limit' => 1000, 'offset' => 0, 'daerah_id' => $kab_id, 'status' => 'Aktif', 'tipe_pengguna' => 'Pengelola/Petugas', 'token' => $this->token)

            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('pengelola_pasar', $response['data']);
            }
        }

        // GET PERBANDINGAN HARGA

        $date2 = date('Y-m-d');
        $date1 = date('Y-m-d', strtotime($date2) - 86400);

        $this->data['date1'] = date('d M Y', strtotime($date1));
        $this->data['date2'] = date('d M Y', strtotime($date2));
        $this->data['val_date1'] = $date1;
        $this->data['val_date2'] = $date2;

        if (strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif') {
            // GET SUM PROGRAM PASAR

            //Ganti Ajax
            /*
            $get_sum_program = $this->getSummaryProgram();

            if ($get_sum_program == 401) {
                redirect('user/sign_out');
            } else {
                $this->data['pasar_tp'] = ($get_sum_program != null) ? $get_sum_program : null;
            }
            */

            if (strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                // GET SUM PENGELOLA

                //Ganti Ajax
                /*
                $total_pengelola_prov = $this->getSummaryPengelolaProv();
                $total_pengelola_kab = $this->getSummaryPengelolaKab();

                $this->data['total_pengelola'] = $total_pengelola_prov['semua_pengelola'] + $total_pengelola_kab['semua_pengelola'];
                $this->data['total_pengelola_prov'] = $total_pengelola_prov['count'];
                $this->data['total_pengelola_kab'] = $total_pengelola_kab['count'];
                */

                $filter_tahun_tp = array();

                $date_end = date('Y') - 4;
                for ($i = date('Y'); $i >= $date_end; $i--) {
                    $filter_tahun_tp[] = $i;
                }

                $this->data['filter_tp'] = $filter_tahun_tp;
            }
        }

        $this->templatebackend('dashboard/index', $this->data);
    }

    function switch_role($salt)
    {

        $role_available = $this->session->userdata('role_lainnya');
        $hash_role_available = [];
        foreach ($role_available as $k => $v) {
            $hash_role_available[sha1(date('Y-m-d') . $v)] = $k;
        }


        if (isset($hash_role_available[$salt])) {


            $role_available[$this->session->userdata('tipe_pengguna_id')] = $this->session->userdata('role');



            if (strtolower($role_available[$hash_role_available[$salt]]) == 'administrator') {

                $api = array(
                    'endpoint'      => 'getMenu',
                    'method'        => 'POST',
                    'controller'   => 'api',
                    'post_field'    => array(
                        'jenis' => 'Back end',
                        'parent_null' => 'Y',
                        'urutan' => 'ASC'
                    )
                );

                $response = $this->serviceAPI($api);

                foreach ($response['data'] as $k => $v) {
                    $api_ = array(
                        'endpoint'      => 'getMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'jenis' => 'Back end',
                            'parent_id' => $v['menu_id'],
                            'urutan' => 'ASC'
                        )
                    );

                    $response_ = $this->serviceAPI($api_);

                    $response['data'][$k]['sub_menu'] = $response_;
                }

                if ($response && $response['kode'] == 200) {
                    $this->session->set_userdata('menu', $response);
                }
                $role_available_except = $role_available;

                unset($role_available_except[$hash_role_available[$salt]]);
                $change_session = [
                    'tipe_pengguna_id' => $hash_role_available[$salt],
                    'role' => $role_available[$hash_role_available[$salt]],
                    'role_lainnya' => $role_available_except
                ];
                $this->session->set_userdata($change_session);

                redirect('web/dashboard');
            } else {

                $api = array(
                    'endpoint'      => 'getTipePenggunaMenu',
                    'method'        => 'POST',
                    'controller'   => 'api',
                    'post_field'    => array(
                        'tipe_pengguna_id' => $hash_role_available[$salt],
                        'parent_null' => 'Y',
                        'urutan' => 'ASC'
                    )
                );

                $response = $this->serviceAPI($api);





                foreach ($response['data'] as $k => $v) {
                    $api_ = array(
                        'endpoint'      => 'getTipePenggunaMenu',
                        'method'        => 'POST',
                        'controller'   => 'api',
                        'post_field'    => array(
                            'tipe_pengguna_id' => $hash_role_available[$salt],
                            'parent_id' => $v['menu_id'],
                            'urutan' => 'ASC'
                        )
                    );

                    $response_ = $this->serviceAPI($api_);

                    $response['data'][$k]['sub_menu'] = $response_['data'];
                }

                if ($response && $response['kode'] == 200) {
                    $this->session->set_userdata('menu', $response);
                    $menu = array();
                    $sub_menu = array();
                    foreach ($response['data'] as $k => $v) {
                        $menu[$v['menu_id']] = array(
                            'create' => $v['create'],
                            'update' => $v['update'],
                            'delete' => $v['delete']
                        );
                        foreach ($response['data'][$k]['sub_menu'] as $kk => $vv) {
                            $sub_menu[$vv['menu_id']] = array(
                                'create' => $vv['create'],
                                'update' => $vv['update'],
                                'delete' => $vv['delete']
                            );
                        }
                    }
                    $this->session->set_userdata('perms_menu', $menu);
                    $this->session->set_userdata('perms_sub_menu', $sub_menu);
                }


                if (strtolower($role_available[$hash_role_available[$salt]]) == 'kontributor') {
                    redirect('404_override');
                } else {
                    $role_available_except = $role_available;

                    unset($role_available_except[$hash_role_available[$salt]]);

                    $change_session = [
                        'tipe_pengguna_id' => $hash_role_available[$salt],
                        'role' => $role_available[$hash_role_available[$salt]],
                        'role_lainnya' => $role_available_except
                    ];
                    $this->session->set_userdata($change_session);
                    redirect('dashboard');
                }
            }
        }
        redirect(base_url() . 'web/dashboard');
    }

    public function getSummaryProgramJSON()
    {
        $response = array('code' => 200, 'data' => null);

        $tahun = $this->input->post('year', true);

        $tahun = ($tahun == null) ? '2021' : $tahun;

        if (strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif') {
            $get_sum_program = $this->getSummaryProgram($tahun);

            if ($get_sum_program == 401) {
                $response['code'] = 401;
            } else if ($get_sum_program == null) {
                $response['data'] = array();
            } else {
                $response['data'] = $get_sum_program;
            }
        }

        echo json_encode($response);
    }

    public function getSummaryProgram($tahun = null)
    {
        $tahun = ($tahun != null) ? $tahun : date('Y');

        $api = array(
            'endpoint'      => 'getSummaryProgramPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('token' => $this->token, 'tahun' => $tahun)
        );

        $response = $this->serviceAPI($api);

        if (!$response) {
            return 401;
        } else if ($response && $response['kode'] == 200) {
            return $response['data'];
        } else {
            return null;
        }
    }

    public function getSummaryPengelolaProv()
    {
        $data = array('semua_pengelola' => 0, 'count' => 0);

        $api = array(
            'endpoint'      => 'getSummaryPengelolaProv',
            'method'        => 'POST',
            'controller'    => '',
            'post_field'    => array('token' => $this->token)
        );

        $response = $this->serviceAPI($api);

        if (!$response) {
            redirect('user/sign_out');
        } else if ($response && $response['kode'] == 200) {

            $total_pengelola = 0;

            foreach ($response['data'] as $row) {
                $total_pengelola += $row['jml_pengelola'];
            }

            $data = array(
                'semua_pengelola' => $total_pengelola,
                'count' => count($response['data'])
            );
        }

        return $data;
    }

    public function getSummaryPengelolaKab()
    {
        $data = array('semua_pengelola' => 0, 'count' => 0);

        $api = array(
            'endpoint'      => 'getSummaryPengelolaKab',
            'method'        => 'POST',
            'controller'    => '',
            'post_field'    => array('token' => $this->token)
        );

        $response = $this->serviceAPI($api);

        if (!$response) {
            redirect('user/sign_out');
        } else if ($response && $response['kode'] == 200) {
            $total_pengelola = 0;

            foreach ($response['data'] as $row) {
                $total_pengelola += $row['jml_pengelola'];
            }

            $data = array(
                'semua_pengelola' => $total_pengelola,
                'count' => count($response['data'])
            );
        }

        return $data;
    }

    public function summaryPasar($type_date, $date_start = null)
    {
        $kab_id = 0;

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'pengelola/petugas') {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        }

        $post_field = array(
            'daerah_id' => $kab_id,
            'token' => $this->token,
            'limit' => 20000,
            'offset' => 0
        );

        if ($type_date == 2) {
            $month_start = date('Y-m-01');
            $month_end = date('Y-m-t');

            $post_field['start_date'] = $month_start;
            $post_field['end_date'] = $month_end;
        } else if ($type_date == 3) {
            $month_start = date('Y-01-01');
            $month_end = date('Y-12-t');

            $post_field['start_date'] = $month_start;
            $post_field['end_date'] = $month_end;
        }

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => $post_field
        );

        $response_pasar = $this->serviceAPI($api);

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            if ($response_pasar['data'] != null) {
                return count($response_pasar['data']);
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }



    public function tableau()
    {

        if (!has_access(3, 'show')) {
            redirect(base_url() . '404_override');
        }


        $this->data['urlparams'] = ':?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no&:origin=false&:showShareOptions=false&:toolbar=no';
        $this->data['view_tableau'] = '/views/SISaranaPerdagangan/PetaInputPasar';
        $this->data['token'] = $this->getTableauToken('PDSIView');

        $this->data['title']  = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->templatebackend('pasar/tableau', $this->data);
    }

    private function getSummaryLaporHarga($kab, $date, $status)
    {
        $post_field = array(
            'token' => $this->token,
            'tanggal' => $date,
            'limit'    => 20000,
            'offset'  => 0,
            'status_lapor' => $status
        );

        if ($kab != 0) {
            $post_field['daerah_id'] = $kab;
        }

        $api = array(
            'endpoint'      => 'getSummaryPasarLaporHarga',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if (!$response) {
            return false;
        } else if ($response && $response['kode'] != 200) {
            return 0;
        } else if ($response && $response['kode'] == 200) {
            return count($response['data']);
        }
    }

    //getToken
    function getTableauToken($username)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://analitik.kemendag.go.id/trusted/',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'username=' . $username,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    function downloadAsset()
    {

        $parse = array_diff_key(explode('/', $this->uri->uri_string()), array(0 => ''));
        $dir = implode('/', array_diff_key($parse, array(count($parse) => '')));
        $filename = $parse[count($parse)];
        // pre($parse);
        download($dir, $filename, implode('_', array_diff_key($parse, array(count($parse) => ''))) . '_' . (explode('.', $filename))[0]);
    }
}
