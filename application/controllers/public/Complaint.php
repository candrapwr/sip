<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Complaint extends Public_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->form_validation->set_rules('province', 'Provinsi', 'required|trim');
		$this->form_validation->set_rules('region', 'Kabupaten / Kota', 'required|trim');
		$this->form_validation->set_rules('market', 'Pasar', 'required|trim');
		$this->form_validation->set_rules('name', 'Nama', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
		$this->form_validation->set_rules('phone', 'Handphone', 'required|numeric|trim');
		$this->form_validation->set_rules('address', 'Alamat', 'required|max_length[255]|trim');
		$this->form_validation->set_rules('complaint', 'Isi Komplain', 'required|trim');

		if (!$this->form_validation->run()) {



			if (!empty($this->session->userdata('token'))) {

				$daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);

				$api = array(
					'endpoint'      => 'getKomplain',
					'method'        => 'POST',
					'controller'    => 'pasar',
					'post_field'    => array('daerah_id' => $daerah_id)
				);

				$response = $this->serviceAPI($api);

				$complaints = array();

				if (!$response) {
					redirect('user/sign_out');
				} else if ($response && $response['kode'] == 200) {
					$complaints = $response['data'];

					$temp_complaints = array();

					foreach ($complaints as $key => $complaint) {
						$slug = slugify($complaint['komplain'], $complaint['komplain_id']);

						$temp_complaints[$slug] = $complaint;
					}

					$this->session->set_userdata('temp_complaints', $temp_complaints);
				} else {
					$this->session->set_userdata('temp_complaints', null);
				}

				$this->data['complaints'] = $complaints;
			} else {

				$api = array(
					'endpoint'      => 'getMasterDaerah',
					'method'        => 'POST',
					'controller'    => '',
					'post_field'    => array('daerah_id' => 1)
				);

				$response = $this->serviceAPI($api);

				$this->data['provinces'] = ($response['kode'] == 200) ? $response['data'] : array();

				$this->session->set_flashdata('session_flashdata_kabupaten', $this->input->post('region'));
				$this->session->set_flashdata('session_flashdata_pasar', $this->input->post('market'));
			}

			//iNiT Layout NeW SiSP
			$this->data['title'] = 'E-Komplain';
			$this->data['menu'] = 'ekomplain';
			$this->data['subtitle'] = 'Berikut adalah Halaman E-Komplain SISP Keementerian Perdagangan Republik Indonesia';
			$this->data['namafile'] = 'ekomplain';
			$this->data['css'] = false;
			$this->data['js'] = true;
			$this->load->view('frontend/structure/index', $this->data);
		} else {
			$pasar = $this->input->post('market');

			$split = explode('|', $pasar);

			$post_field = array(
				'daerah_id' => $this->input->post('region', true),
				'pasar_id'   => $split[0], //masih dummy
				'nama_pasar'   => $split[1], //masih dummy
				'nama' => $this->input->post('name', true),
				'alamat' => $this->input->post('address', true),
				'email' => $this->input->post('email', true),
				'hp' => $this->input->post('phone', true),
				'komplain' => $this->input->post('complaint', true)
			);

			$api = array(
				'endpoint'      => 'setKomplain',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => $post_field
			);

			$response = $this->serviceAPI($api);

			if ($response && $response['kode'] == 200) {
				$api = array(
					'endpoint'  => 'sendMail',
					'method'        => 'POST',
					'post_field'    => array(
						'email' => $this->input->post('email', true),
						'subject'   => 'Pemberitahuan Nomor Tiket E-Komplain',
						'template'  => 'email/nomor_tiket_komplain',
						'data[ticket]'  => $response['data']['no_tiket'],
						'data[nama]'  => $this->input->post('name', true)
					)
				);

				$send_mail_pemohon = $this->serviceAPI($api);

				// $api = array(
				// 	'endpoint'      => 'getPengguna',
				// 	'method'        => 'POST',
				// 	'controller'    => 'api',
				// 	'post_field'    => array('limit' => 1000, 'offset' => 0, 'status' => 'Aktif', 'tipe_pengguna' => 'Pengelola/Petugas', 'daerah_id' => $this->input->post('region'))
				// );

				// $get_pengguna = $this->serviceAPI($api);

				// if ($get_pengguna['kode'] == 200) {

				// 	$api = array(
				// 		'endpoint'  => 'getKomplain',
				// 		'method'        => 'POST',
				// 		'controller'    => 'pasar',
				// 		'post_field'    => array('no_tiket' => $response['data']['no_tiket'])
				// 	);

				// 	$get_komplain = $this->serviceAPI($api);

				// 	foreach ($get_pengguna['data'] as $row) {
				// 		if ($row['email'] != '' && $row['email'] != '-') {
				// 			$api = array(
				// 				'endpoint'  => 'sendMail',
				// 				'method'        => 'POST',
				// 				'post_field'    => array(
				// 					'email' => $row['email'],
				// 					'subject'   => 'Pemberitahuan Komplain Pasar',
				// 					'template'  => 'email/notifikasi_komplain_petugas',
				// 					'data[ticket]'  => $response['data']['no_tiket'],
				// 					'data[nama_penerima]'  => $row['nama_lengkap'],
				// 					'data[nama_pemohon]'  => $this->input->post('name', true),
				// 					'data[alamat_pemohon]'  => $this->input->post('address', true),
				// 					'data[komplain]'  => $this->input->post('complaint', true),
				// 					'data[nama_pasar]'  => $split[1],
				// 					'data[url]'  => slugify($this->input->post('complaint', true), $get_komplain['data'][0]['komplain_id'])
				// 				)
				// 			);

				// 			$send_mail_petugas = $this->serviceAPI($api);
				// 		}
				// 	}
				// }

				$this->session->set_flashdata('submit_success', 'Terimakasih. Pengajuan komplain Anda telah terkirim, Silahkan cek Email Anda untuk melihat nomor tiket Anda.');
			} else {
				$this->session->set_flashdata('submit_failed', 'Pengajuan komplain gagal terkirim.');
			}

			redirect(site_url() . 'e-komplain');
		}
	}



	public function search()
	{
		$ticket = $this->input->post('keyword', true);

		if ($ticket != '') {
			$api = array(
				'endpoint'      => 'getKomplain',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => array('no_tiket' => $ticket)
			);

			$service = $this->serviceAPI($api);

			if ($service['kode'] == 200) {
				$slug = slugify($service['data'][0]['komplain'], $service['data'][0]['komplain_id']);

				$this->session->set_userdata('ticket', $ticket);

				redirect(site_url() . 'e-komplain/search/' . $slug);
			} else {
				redirect(site_url() . 'e-komplain');
			}
		} else {
			redirect(site_url() . 'e-komplain');
		}
	}

	public function generateTicket($ticket = null)
	{
		$ticket = $this->security->xss_clean($ticket);

		if ($ticket != null) {
			$ticket = $this->session->userdata('ticket');

			$api = array(
				'endpoint'      => 'getKomplain',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => array('no_tiket' => $ticket)
			);

			$service = $this->serviceAPI($api);

			if ($service && $service['kode'] == 200) {
				$this->data['complaint'] = ($service['kode'] == 200) ? $service['data'][0] : array();

				$enableTanggapan = true;

				if ($service['data'][0]['tracking'] != null) {
					end($service['data'][0]['tracking']);

					$lastIndex = key($service['data'][0]['tracking']);

					if ($service['data'][0]['tracking'][$lastIndex]['status'] == 'RESOLVED') {
						$enableTanggapan = false;
					}
				}

				$this->data['tanggapan'] = $enableTanggapan;


				//iNiT Layout NeW SiSP
				$this->data['title'] = 'Detail E-Komplain';
				$this->data['menu'] = 'detailekomplain';
				$this->data['subtitle'] = 'Berikut adalah Halaman Detail E-Komplain SISP Keementerian Perdagangan Republik Indonesia';
				$this->data['namafile'] = 'ekomplain_detail';
				$this->data['css'] = false;
				$this->data['js'] = true;
				$this->load->view('frontend/structure/index', $this->data);
			} else {
				redirect(site_url() . 'e-komplain');
			}
		} else {
			redirect(site_url() . 'e-komplain');
		}
	}

	public function save()
	{
		$response = array('success' => false, 'validation' => null, 'code' => 200);

		$this->form_validation->set_rules('tanggapan', 'Tanggapan', 'required|trim');
		$this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

		if (!$this->form_validation->run()) {
			foreach ($this->input->post() as $key => $value) {
				$response['validation'][$key] = form_error($key);
			}
		} else {
			$post_field = array(
				'no_tiket' => $this->input->post('tiket'),
				'status'    => 'OPEN',
				'keterangan'    => $this->input->post('tanggapan'),
				'email' => $this->input->post('email'),
				'flag_pengelola'    => 'M'
			);

			$api = array(
				'endpoint'      => 'setTracking',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => $post_field
			);

			$response_pasar = $this->serviceAPI($api);

			if ($response_pasar) {

				$api = array(
					'endpoint'      => 'getKomplain',
					'method'        => 'POST',
					'controller'    => 'pasar',
					'post_field'    => array('no_tiket' => $this->input->post('tiket'))
				);

				$get_komplain = $this->serviceAPI($api);

				if ($get_komplain['kode'] == 200) {
					$email_petugas = null;

					foreach ($get_komplain['data'][0]['tracking'] as $tracking) {
						if ($tracking['flag_pengelola'] == 'Y') {
							$email_petugas = $tracking['createdby'];
							break;
						}
					}

					if ($email_petugas != null) {
						$api = array(
							'endpoint'      => 'getPengguna',
							'method'        => 'POST',
							'post_field'    => array('limit' => 1, 'offset' => 0, 'email' => $email_petugas)
						);

						$get_data_petugas = $this->serviceAPI($api);

						if ($get_data_petugas['kode'] == 200) {
							$api = array(
								'endpoint'  => 'sendMail',
								'method'        => 'POST',
								'post_field'    => array(
									'email' => $email_petugas,
									'subject'   => 'Pemberitahuan Tanggapan Komplain',
									'template'  => 'email/notifikasi_tanggapan_pemohon',
									'data[nama_penerima]'  => $get_data_petugas['data'][0]['nama_lengkap'],
									'data[pemohon]'  => $get_komplain['data'][0]['nama'],
									'data[alamat_pemohon]'  => $get_komplain['data'][0]['alamat'],
									'data[tanggapan]'  => $this->input->post('tanggapan', true),
									'data[nama_pasar]'  => $get_komplain['data'][0]['nama_pasar'],
									'data[url]'  => slugify($get_komplain['data'][0]['komplain'], $get_komplain['data'][0]['komplain_id'])
								)
							);

							$send_mail_petugas = $this->serviceAPI($api);
						}
					}
				}

				$response['data'] = array(
					'status' => 'OPEN',
					'keterangan'    => $this->input->post('tanggapan'),
					'email' => $this->input->post('email'),
					'date'  => full_date(date('Y-m-d'))
				);

				$response['success'] = true;
			} else {
				$response['code'] = 401;
			}
		}

		echo json_encode($response);
	}

	public function getTracking()
	{
		$response = array('data' => array());

		$id = $this->input->post('id', true);

		if ($id != null) {
			$api = array(
				'endpoint'      => 'getKomplain',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => array('no_tiket' => $id)
			);

			$service = $this->serviceAPI($api);

			if ($service['kode'] == 200) {

				foreach ($service['data'][0]['tracking'] as $key => $row) {
					$service['data'][0]['tracking'][$key]['createdby_in'] = substr($row['createdby'], 0, 1);
					$service['data'][0]['tracking'][$key]['createdon'] = full_date($row['createdon']);

					if ($row['keterangan'] == null) {
						$service['data'][0]['tracking'][$key]['keterangan'] = '';
					}

					if ($row['flag_pengelola'] == 'Y') {
						$service['data'][0]['tracking'][$key]['createdby'] = $service['data'][0]['nama_pasar'];
					}
				}

				$response['data'] = $service['data'][0]['tracking'];
			}
		}

		echo json_encode($response);
	}

	public function getPasarJSON()
	{
		$response = array('data' => null, 'code' => 200, 'success' => false);

		$id = $this->input->post('region_id');

		if ($id != '') {
			$api = array(
				'endpoint'      => 'getPasar',
				'method'        => 'POST',
				'controller'    => 'pasar',
				'post_field'    => array('daerah_id' => $id, 'limit' =>  600, 'offset' => 0)
			);

			$response_pasar = $this->serviceAPI($api);

			$temp = array();

			if (!$response_pasar) {
				$response['code'] = 401;
			} else if ($response_pasar && $response_pasar['kode'] == 200) {
				foreach ($response_pasar['data'] as $key => $row) {
					$temp[$key]['pasar_id'] = $row['pasar_id'];
					$temp[$key]['nama'] = $row['nama'];
				}

				$response['data'] = $temp;
				$response['success'] = true;
			}
		}

		echo json_encode($response);
	}
}
