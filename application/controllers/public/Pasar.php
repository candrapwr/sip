<?php

use function PHPSTORM_META\map;

defined('BASEPATH') or exit('No direct script access allowed');

class Pasar extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }

    public function getPasarJSON()
    {
        $response = array('data' => null, 'code' => 200, 'success' => false);

        if ($this->input->post('action_index') != null) {
            $response['data'] = $this->session->userdata('index_pasar');
            $response['action_index'] = $this->input->post('action_index');
            $response['success'] = true;

            echo json_encode($response);
            exit();
        }

        $exp = explode('|', $this->input->post('provinsi_id'));
        $id = $exp[0];

        if ($id != 1) {
            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('daerah_id' => $id, 'limit' =>  600, 'offset' => 0)
            );

            $response_pasar = $this->serviceAPI($api);

            $temp = array();

            if (!$response_pasar) {
                $response['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $temp[$key]['pasar_id'] = $row['pasar_id'];
                    $temp[$key]['nama'] = $row['nama'];
                }

                $response['data'] = $temp;
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function detail($id = null)
    {
        if ($id != null) {
            $split_url = explode('-', $id);
            $length = count($split_url) - 1;
            $pasar_id = $split_url[0];



            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $pasar_id, 'status' => 'Aktif', 'limit' => 1, 'offset' => 0)
            );

            $response = $this->serviceAPI($api);

            if ($response['kode'] == 200) {
                $this->data['pasar'] = $response['data'][0];


                $api = array(
                    'endpoint'      => 'getDetailPasar',
                    'method'        => 'POST',
                    'controller'    => 'pasar',
                    'post_field'    => array('pasar_id' => $pasar_id)
                );

                $response = $this->serviceAPI($api);

                if ($response['kode'] == 200) {
                    $this->data['detail_pasar'] = $response['data'];
                } else {
                    $this->data['detail_pasar'] = null;
                }

                $this->data['index_pasar'] = $this->data['pasar'];
                $this->data['active_index'] = $id;


                //iNiT Layout NeW SiSP
                $this->data['title'] = $this->data['index_pasar']['nama'];
                $this->data['menu'] = 'pasar';
                $this->data['subtitle'] = 'Berikut adalah Halaman Detail ' . $this->data['index_pasar']['nama'];
                $this->data['namafile'] = 'pasar_detail';
                $this->data['css'] = true;
                $this->data['js'] = true;
                $this->load->view('frontend/structure/index', $this->data);
            }
        }
    }

    // public function getKiosJSON()
    // {
    //     $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

    //     if ($this->input->post('action_index') != null) {
    //         echo json_encode($this->session->userdata('data_kios')[$pagenumber]['data'][$this->input->post('action_index')]);
    //         exit();
    //     }

    //     if (isset($this->session->userdata('data_kios')[$pagenumber]) && empty($this->input->post('refresh'))) {
    //         $session_existing = $this->session->userdata('data_kios')[$pagenumber];
    //         $session_existing['draw'] = $this->input->post('draw');
    //         echo json_encode($session_existing);
    //     } else {
    //         $api = array(
    //             'endpoint'      => 'getBangunanPasar',
    //             'method'        => 'POST',
    //             'controller'    => 'pasar',
    //             'post_field'    => array('pasar_id' => $this->input->post('id'), 'token' => $this->token, 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')))
    //         );

    //         $response = $this->serviceAPI($api);

    //         $response['draw'] = $this->input->post('draw');

    //         if ($response && $response['kode'] == 200) {
    //             $session = $this->session->all_userdata();
    //             $session['data_kios'][$pagenumber] = $response;

    //             foreach ($session['data_kios'][$pagenumber]['data'] as $key => $row) {
    //                 $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
    //                 $session['data_kios'][$pagenumber]['data'][$key]['jumlah_pedagang'] = rupiah($row['jumlah_pedagang']);
    //                 $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);

    //                 $session['data_kios'][$pagenumber]['data'][$key]['action'] = false;

    //                 if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
    //                     $session['data_kios'][$pagenumber]['data'][$key]['action'] = true;
    //                 }
    //             }

    //             $this->session->set_userdata($session);
    //             echo json_encode($this->session->userdata('data_kios')[$pagenumber]);
    //         } else {
    //             $response = array(
    //                 'draw' => 1,
    //                 'recordsTotal' => 0,
    //                 'recordsFiltered' => 0,
    //                 'data'  => []
    //             );

    //             echo json_encode($response);
    //         }
    //     }
    // }

    public function sebaran_bps()
    {


        $kab_id = 1;

        // if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        // } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        // } else {
        $kab_id = 0;
        //  }

        if (empty($this->session->userdata('maps_pasar'))) {

            $api = array(
                'endpoint'      => 'getPasarMgcrBPS',
                'method'        => 'POST',
                'controller'    => 'migor',
                'post_field'    => array('daerah_id' => $kab_id, 'limit' => 10000, 'offset' => 0)
            );

            if (!empty($this->input->post('provinsi'))) {
                $api['post_field']['kode_daerah'] = $this->input->post('provinsi');

                $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
            }

            if (!empty($this->input->post('kabupaten'))) {
                $api['post_field']['kode_daerah'] = $this->input->post('kabupaten');
                $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
            }

            $response_pasar = $this->serviceAPI($api);

            $this->data['maps_pasar'] = null;


            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('maps_pasar', $response_pasar['data']);
                $this->data['maps_pasar'] = $this->session->userdata('maps_pasar');
            }
        } else {
            $data_pasar = $this->session->userdata('maps_pasar');
            $hasilFilter = array();
            if (!empty($this->input->post('provinsi'))) {
                if (!empty($data_pasar)) {
                    foreach ($data_pasar as $pasar) {
                        if (substr($pasar['kode_daerah'], 0, 2) == $this->input->post('provinsi')) {
                            $hasilFilter[] = $pasar;
                        }
                    }
                }

                $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
            }

            if (!empty($this->input->post('kabupaten'))) {
                if (!empty($data_pasar)) {
                    foreach ($data_pasar as $pasar) {
                        if (substr($pasar['kode_daerah'], 0, 4) == $this->input->post('kabupaten')) {
                            $hasilFilter[] = $pasar;
                        }
                    }
                }
                $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
            }

            if (!empty($this->input->post('provinsi')) || !empty($this->input->post('kabupaten'))) {
                $data_pasar = $hasilFilter;
            }


            $this->data['maps_pasar'] = $data_pasar;
        }

        

        if (strpos(strtolower((string)$this->session->userdata('kab_kota')), 'kab') !== false) {
            $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kabupaten ' . $kab;
        } else if (strpos(strtolower((string)$this->session->userdata('kab_kota')), 'kota') !== false) {
            $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kota ' . $kab;
        }

        $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
        // $this->data['latlong_map'] = $this->session->userdata('kab_latlong');


        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        //iNiT Layout NeW SiSP
        $this->data['title']  = 'Sebaran Pasar 10.000 Titik Minyak Goreng Curah Rakyat';
        $this->data['menu'] = 'sebaran';
        $this->data['subtitle'] = 'Berikut adalah Halaman Sebaran Pasar 10.000 Titik Minyak Goreng Curah Rakyat SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'sebaran_bps';
        $this->data['css'] = true;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function sebaran_pujle()
    {

        $kab_id = 1;

        // if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        // } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        // } else {
        $kab_id = 0;
        //  }

        if (empty($this->session->userdata('migor_14rb'))) {

            $api = array(
                'endpoint'      => 'getPengecerPUJLE',
                'method'        => 'POST',
                'controller'    => 'migor',
                'post_field'    => array('daerah_id' => $kab_id, 'limit' => 10000, 'offset' => 0)
            );

            if (!empty($this->input->post('provinsi'))) {
                $api['post_field']['kode_daerah'] = $this->input->post('provinsi');
                $this->data['prov_isi'] = $this->input->post('provinsi');
                $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
            }

            if (!empty($this->input->post('kabupaten'))) {
                $this->data['kab_isi'] = $this->input->post('kabupaten');
                $api['post_field']['kode_daerah'] = $this->input->post('kabupaten');
                $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
            }

            $response_pasar = $this->serviceAPI($api);
            //  var_dump($response_pasar);die();
            $this->data['maps_pasar'] = null;

            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('migor_14rb', $response_pasar['data']);
                $this->data['maps_pasar'] = $this->session->userdata('migor_14rb');
            }
        } else {
            $data_pasar = $this->session->userdata('migor_14rb');
            $hasilFilter = array();
            if (!empty($this->input->post('provinsi'))) {
                if (!empty($data_pasar)) {
                    foreach ($data_pasar as $pasar) {
                        if (substr($pasar['kode_daerah'], 0, 2) == $this->input->post('provinsi')) {
                            $hasilFilter[] = $pasar;
                        }
                    }
                }

                $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
            }

            if (!empty($this->input->post('kabupaten'))) {
                if (!empty($data_pasar)) {
                    foreach ($data_pasar as $pasar) {
                        if (substr($pasar['kode_daerah'], 0, 4) == $this->input->post('kabupaten')) {
                            $hasilFilter[] = $pasar;
                        }
                    }
                }
                $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
            }

            if (!empty($this->input->post('provinsi')) || !empty($this->input->post('kabupaten'))) {
                $data_pasar = $hasilFilter;
            }


            $this->data['maps_pasar'] = $data_pasar;
        }
       


        if (strpos(strtolower((string)$this->session->userdata('kab_kota')), 'kab') !== false) {
            $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kabupaten ' . $kab;
        } else if (strpos(strtolower((string)$this->session->userdata('kab_kota')), 'kota') !== false) {
            $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kota ' . $kab;
        }

        $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
        // $this->data['latlong_map'] = $this->session->userdata('kab_latlong');


        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        //iNiT Layout NeW SiSP
        $this->data['title']  = 'Sebaran Titik Penjual Minyak Goreng Curah 14.000';
        $this->data['menu'] = 'sebaran';
        $this->data['subtitle'] = 'Berikut adalah Halaman Sebaran Titik Penjual Minyak Goreng Curah 14.000 SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'sebaran_pujle';
        $this->data['css'] = true;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function sebaran_simirah()
    {


        $kab_id = 1;

        // if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        // } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
        //     $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        // } else {
        $kab_id = 0;
        //  }

        $api = array(
            'endpoint'      => 'getPengecerSimirah',
            'method'        => 'POST',
            'controller'    => 'migor',
            'post_field'    => array('daerah_id' => $kab_id, 'limit' => 100000, 'offset' => 0)
        );

        if (!empty($this->input->post('provinsi'))) {
            $api['post_field']['kode_daerah'] = $this->input->post('provinsi');
            $this->data['prov_isi'] = $this->input->post('provinsi');
            $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
        } else {
            $api['post_field']['kode_daerah'] = 16;
            $this->data['prov_isi'] = 32;
            $this->session->set_flashdata('filter_search_provinsi', 32);
        }

        if (!empty($this->input->post('kabupaten'))) {
            $this->data['kab_isi'] = $this->input->post('kabupaten');
            $api['post_field']['kode_daerah'] = $this->input->post('kabupaten');
            $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
        }

        $response_pasar = $this->serviceAPI($api);
        //  var_dump($response_pasar);die();
        $this->data['maps_pasar'] = null;


        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->data['maps_pasar'] = $response_pasar['data'];
        }


        if (strpos(strtolower($this->session->userdata('kab_kota')), 'kab') !== false) {
            $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kabupaten ' . $kab;
        } else if (strpos(strtolower($this->session->userdata('kab_kota')), 'kota') !== false) {
            $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

            $this->data['kab'] = 'Kota ' . $kab;
        }

        $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
        // $this->data['latlong_map'] = $this->session->userdata('kab_latlong');


        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        //iNiT Layout NeW SiSP
        $this->data['title']  = 'Titik Jual Minyak Goreng Rp 14.000 (SIMIRAH)';
        $this->data['menu'] = 'sebaran';
        $this->data['subtitle'] = 'Berikut adalah Halaman Titik Jual Minyak Goreng Rp 14.000 (SIMIRAH) SISP Keementerian Perdagangan Republik Indonesia';
        $this->data['namafile'] = 'sebaran_simirah';
        $this->data['css'] = false;
        $this->data['js'] = true;
        $this->load->view('frontend/structure/index', $this->data);
    }

    public function getRegulasi()
    {
        $code = 200;
        $data = array();

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_regulasi')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_regulasi')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getRegulasi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_regulasi'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function getHargaJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_harga')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        $start_date = date('Y-m-d');

        if (isset($this->session->userdata('detail_harga')[$pagenumber]) && empty($this->input->post('refresh')) && $this->input->post('start_date') == null) {
            $session_existing = $this->session->userdata('detail_harga')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if ($this->input->post('start_date') != null) {
                $start_date = $this->input->post('start_date');
            }

            $api = array(
                'endpoint'      => 'getHargaKomoditi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'createdon' => $start_date,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_harga'][$pagenumber] = $response;

                foreach ($session['detail_harga'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_harga'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
                    $session['detail_harga'][$pagenumber]['data'][$key]['harga'] = rupiah($row['harga']);

                    $session['detail_harga'][$pagenumber]['data'][$key]['action'] = false;
                    $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['detail_harga'][$pagenumber]['data'][$key]['action'] = true;
                    }

                    if ($this->session->userdata('email') == $row['createdby'] || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        if (date('Y-m-d', strtotime($row['createdon'])) == date('Y-m-d')) {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }

                        if (strtolower($this->session->userdata('role')) == 'teknis pdn') {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }
                    }
                }

                $session['detail_harga'][$pagenumber]['date'] = full_date($start_date);

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_harga')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => [],
                    'date' => full_date($start_date)
                );

                echo json_encode($response);
            }
        }
    }

    public function getKiosJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != null) {
            echo json_encode($this->session->userdata('data_kios')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('data_kios')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('data_kios')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getBangunanPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->input->post('id'), 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')))
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['data_kios'][$pagenumber] = $response;

                foreach ($session['data_kios'][$pagenumber]['data'] as $key => $row) {
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah_pedagang'] = rupiah($row['jumlah_pedagang']);
                    $session['data_kios'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);

                    $session['data_kios'][$pagenumber]['data'][$key]['action'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['data_kios'][$pagenumber]['data'][$key]['action'] = true;
                    }
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('data_kios')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }
}
