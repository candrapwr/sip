<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Email extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        echo '<h1>Buah Mangga Buah Semangka<br>
        Chat terus tapi cuma jadi teman semata<br>
        chuaks</h1>';
    }

    public function confirmemail()
    {
        $this->data['title'] = 'Konfirmasi Email';
        $this->data['menu'] = 'email';
        $this->data['subtitle'] = 'Email Konfirmasi Email SISP Kementerian Perdagangan RI';
        $this->load->view('frontend/contents/email/konfirmasi', $this->data);
    }

    public function siapdownload()
    {
        $this->data['title'] = 'File Sudah Siap Didownload';
        $this->data['menu'] = 'email';
        $this->data['subtitle'] = 'Email Download File SISP Kementerian Perdagangan RI';
        $this->load->view('frontend/contents/email/siapdownload', $this->data);
    }

    public function resetpassword()
    {
        $this->data['title'] = 'Reset Password';
        $this->data['menu'] = 'email';
        $this->data['subtitle'] = 'Email Reset Password SISP Kementerian Perdagangan RI';
        $this->load->view('frontend/contents/email/resetpassword', $this->data);
    }

    public function pemberitahuan()
    {
        $this->data['title'] = 'Pemberitahuan';
        $this->data['menu'] = 'email';
        $this->data['subtitle'] = 'Email Pemberitahuan SISP Kementerian Perdagangan RI';
        $this->load->view('frontend/contents/email/pemberitahuan', $this->data);
    }

    public function sukses()
    {
        $this->data['title'] = 'Sukses';
        $this->data['menu'] = 'email';
        $this->data['subtitle'] = 'Email Sukses SISP Kementerian Perdagangan RI';
        $this->load->view('frontend/contents/email/sukses', $this->data);
    }
}
