<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();
    }

    public function tipe_pengguna()
    {
        // echo '<pre>';
        // var_dump($this->session->userdata('menu'));die();
        $this->data['title']  = 'Master Tipe Pengguna';
        $this->data['js']     = 'tipe_pengguna_js';
        $this->data['menu'] = 'tipe_pengguna';
        $this->templatebackend('pengguna/tipe_pengguna', $this->data);
    }

    public function tipe_pengguna_json()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_tipe_pengguna')['data'][$this->input->post('action_index')]);
            exit();
        }

        if ($this->session->userdata('master_tipe_pengguna') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_tipe_pengguna'));
        } else {
            $api = array(
                'endpoint'      => 'getTipePengguna',
                'method'        => 'POST',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);

            foreach ($response['data'] as $k => $v) {
                $api_ = array(
                    'endpoint'      => 'getTipePenggunaMenu',
                    'method'        => 'POST',
                    'controller'   => 'api',
                    'post_field'    => array(
                        'tipe_pengguna_id' => $v['tipe_pengguna_id']
                    )
                );
                $data = $this->serviceAPI($api_);
                if ($data && $data['kode'] == 200) {
                    $response['data'][$k]['menu'] = $data['data'];
                } else {
                    $response['data'][$k]['menu'] = array();
                }
            }

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_tipe_pengguna', $response);
                echo json_encode($this->session->userdata('master_tipe_pengguna'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_tipe_pengguna()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('tipe_pengguna', 'Tipe Pengguna', 'trim|required');
        $this->form_validation->set_rules('flag_publik', 'Flag Publik', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'tipe_pengguna_id' => $data_post['tipe_pengguna_id'],
            'tipe_pengguna' => $data_post['tipe_pengguna'],
            'flag_publik' => $data_post['flag_publik'],
            'email' => $this->session->userdata('email', true)
        );

        $api = array(
            'endpoint'      => 'setTipePengguna',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'getTipePengguna',
                'method'        => 'POST',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_tipe_pengguna', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/tipe-pengguna');
    }


    //komoditi
    //komoditi_jenis
    public function jenis_komoditi()
    {
        $this->data['title']  = 'Jenis Komoditi';
        $this->data['js']     = 'jenis_komoditi_js';
        $this->data['menu'] = 'jenis-komoditi';
        $this->templatebackend('jenis_komoditi', $this->data);
    }

    public function jenis_komoditi_json()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_jenis_komoditi')['data'][$this->input->post('action_index')]);
            exit();
        }

        if ($this->session->userdata('master_jenis_komoditi') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_jenis_komoditi'));
        } else {
            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);

            //            var_dump($response['kode']);die();

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_jenis_komoditi', $response);
                echo json_encode($this->session->userdata('master_jenis_komoditi'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_jenis_komoditi()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('jenis_komoditi', 'Jenis Komoditi', 'trim|required');
        $this->form_validation->set_rules('kelompok', 'Kelompok Komoditi', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'jenis_komoditi_id' => $data_post['jenis_komoditi_id'],
            'jenis_komoditi' => $data_post['jenis_komoditi'],
            'kelompok' => $data_post['kelompok'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setJenisKomoditi',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'method'        => 'POST',
                'controller'   => 'pasar',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            //                    var_dump($response['kode']);die();
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_jenis_komoditi', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/jenis-komoditi');
    }


    //varian komoditi
    public function varian_komoditi()
    {
        $this->data['title']  = 'Varian Komoditi';
        $this->data['js']     = 'varian_komoditi_js';
        $this->data['menu'] = 'varian_komoditi';

        if (!$this->session->userdata('jenis_komoditi')) {
            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
            );

            $response_jenis = $this->serviceAPI($api);

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('jenis_komoditi', $response_jenis['data']);
            }
        }

        $this->data['jenis_komoditi'] = $this->session->userdata('jenis_komoditi');
        $this->templatebackend('varian_komoditi', $this->data);
    }

    public function varian_komoditi_json()
    {
        // var_dump($this->input->post('jenis_komoditi_id'));die();
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_varian_komoditi')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('master_varian_komoditi') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_varian_komoditi'));
        } else {

            $api = array(
                'endpoint'      => 'getVarianKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $this->input->post('jenis_komoditi_id'))
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_varian_komoditi', $response);
                echo json_encode($this->session->userdata('master_varian_komoditi'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }


    function cu_varian_komoditi()
    {
        $data_post = $this->input->post();
        // var_dump($data_post );die();
        $this->form_validation->set_rules('varian_komoditi', 'Varian Komoditi', 'trim|required');
        $this->form_validation->set_rules('jenis_komoditi', 'Jenis Komoditi', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'varian_komoditi_id' => $data_post['varian_komoditi_id'],
            'jenis_komoditi_id' => $data_post['jenis_komoditi'],
            'varian_komoditi' => $data_post['varian_komoditi'],
            'keterangan' => $data_post['keterangan'],
            'email' => $this->session->userdata('email', true)
        );
        //    var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setVarianKomoditi',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'getVarianKomoditi',
                'method'        => 'POST',
                'controller'   => 'pasar',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            //                    var_dump($response['kode']);die();
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_varian_komoditi', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/varian-komoditi');
    }

    //satuan komoditi
    public function satuan_komoditi()
    {
        $this->data['title']  = 'Satuan Komoditi';
        $this->data['js']     = 'satuan_komoditi_js';
        $this->data['menu'] = 'satuan_komoditi';

        if (!$this->session->userdata('jenis_komoditi')) {
            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
            );

            $response_jenis = $this->serviceAPI($api);

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('jenis_komoditi', $response_jenis['data']);
            }
        }
        if (!$this->session->userdata('satuan')) {
            $api = array(
                'endpoint'      => 'getSatuan',
                'controller'    => 'master',
                'method'        => 'POST',
            );

            $response_jenis = $this->serviceAPI($api);

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('satuan', $response_jenis['data']);
            }
        }

        $this->data['jenis_komoditi'] = $this->session->userdata('jenis_komoditi');
        $this->data['satuan'] = $this->session->userdata('satuan');
        $this->templatebackend('satuan_komoditi', $this->data);
    }

    public function satuan_komoditi_json()
    {
        // var_dump($this->input->post('jenis_komoditi_id'));die();
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_satuan_komoditi')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('master_satuan_komoditi') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_satuan_komoditi'));
        } else {
            $api = array(
                'endpoint'      => 'getSatuanKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $this->input->post('jenis_komoditi_id'))
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_satuan_komoditi', $response);
                echo json_encode($this->session->userdata('master_satuan_komoditi'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }


    function cu_satuan_komoditi()
    {
        $data_post = $this->input->post();
        // var_dump($data_post );die();
        $this->form_validation->set_rules('satuan', 'Satuan Komoditi', 'trim|required');
        $this->form_validation->set_rules('jenis_komoditi', 'Jenis Komoditi', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'satuan_komoditi_id' => $data_post['satuan_komoditi_id'],
            'jenis_komoditi_id' => $data_post['jenis_komoditi'],
            'satuan_komoditi' => $data_post['satuan'],
            'email' => $this->session->userdata('email', true)
        );


        $api = array(
            'endpoint'      => 'setSatuanKomoditi',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'getSatuanKomoditi',
                'method'        => 'POST',
                'controller'   => 'pasar',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            //                    var_dump($response['kode']);die();
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_satuan_komoditi', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'master/satuan_komoditi');
    }

    //satuan
    public function satuan()
    {
        $this->data['title']  = 'Master Satuan';
        $this->data['js']     = 'backend/js/master/satuan_js';
        $this->data['menu'] = 'satuan';
        $this->templatebackend('backend/master/satuan', $this->data);
    }


    public function satuan_json()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_satuan')['data'][$this->input->post('action_index')]);
            exit();
        }

        if ($this->session->userdata('master_satuan') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_satuan'));
        } else {
            $api = array(
                'endpoint'      => 'getSatuan',
                'controller'    => 'master',
                'method'        => 'POST',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);

            //            var_dump($response['kode']);die();

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_satuan', $response);
                echo json_encode($this->session->userdata('master_satuan'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_satuan()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('satuan', 'Satuan', 'trim|required');
        $this->form_validation->set_rules('singkatan', 'Singkatan', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'satuan_id' => $data_post['satuan_id'],
            'satuan' => $data_post['satuan'],
            'singkatan' => $data_post['singkatan'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setSatuan',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'getSatuan',
                'method'        => 'POST',
                'controller'   => 'master',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_satuan', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/satuan');
    }

    function getDaerah()
    {
        $data_post = $this->input->post();

        if (!empty($data_post['daerah_id'])) {
            $daerah_id = $data_post['daerah_id'];
        } else {
            $daerah_id = 1;
        }
        if (isset($this->session->userdata('ms_daerah')[$daerah_id])) {
            echo json_encode($this->session->userdata('ms_daerah')[$daerah_id]);
        } else {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => $daerah_id)
            );

            $data = $this->serviceAPI($api);
            if ($data && $data['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['ms_daerah'][$daerah_id] = $data;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('ms_daerah')[$daerah_id]);
            } else {
                echo json_encode($data);
            }
        }
    }

    public function klasifikasi()
    {
        $this->data['title']  = 'Klasifikasi';
        $this->data['js']     = 'klasifikasi_js';
        $this->data['menu'] = 'klasifikasi';
        $this->templatebackend('klasifikasi', $this->data);
    }

    function getKlasifikasi()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_klasifikasi')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('master_klasifikasi') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_klasifikasi'));
        } else {
            $api = array(
                'endpoint'      => 'getKlasifikasi',
                'controller'    => 'master',
                'method'        => 'POST',
                'post_field'    => array(
                    'klasifikasi_id' => $this->input->post('klasifikasi_id'),
                    'status' => $this->input->post('status')
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_klasifikasi', $response);
                echo json_encode($this->session->userdata('master_klasifikasi'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_klasifikasi()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('klasifikasi', 'Klasifikasi', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'klasifikasi_id' => $data_post['klasifikasi_id'],
            'klasifikasi' => $data_post['klasifikasi'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setKlasifikasi',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'setKlasifikasi',
                'method'        => 'POST',
                'controller'   => 'master',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('klasifikasi', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/klasifikasi');
    }

    public function sarana_prasarana()
    {
        $this->data['title']  = 'Sarana & Prasarana';
        $this->data['js']     = 'sarana_prasarana_js';
        $this->data['menu'] = 'sarana-prasarana';
        $this->templatebackend('sarana_prasarana', $this->data);
    }

    function getSarana_prasarana()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('master_sarana_prasarana')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('master_sarana_prasarana') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('master_sarana_prasarana'));
        } else {
            $api = array(
                'endpoint'      => 'getSarana_prasarana',
                'controller'    => 'master',
                'method'        => 'POST',
                'post_field'    => array(
                    'sarana_prasarana_id' => $this->input->post('sarana_prasarana_id'),
                    'status' => $this->input->post('status')
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('master_sarana_prasarana', $response);
                echo json_encode($this->session->userdata('master_sarana_prasarana'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_sarana_prasarana()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('sarana_prasarana', 'Sarana & Prasarana', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'sarana_prasarana_id' => $data_post['sarana_prasarana_id'],
            'sarana_prasarana' => $data_post['sarana_prasarana'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setSarana_prasarana',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'setSarana_prasarana',
                'method'        => 'POST',
                'controller'   => 'master',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('sarana_prasarana', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/sarana-prasarana');
    }

    public function program_pasar()
    {
        $this->data['title']  = 'Program Pasar';
        $this->data['js']     = 'program_pasar_js';
        $this->data['menu'] = 'program-pasar';
        $this->templatebackend('program_pasar', $this->data);
    }

    function getProgram_pasar()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('program_pasar')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('program_pasar') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('program_pasar'));
        } else {
            $api = array(
                'endpoint'      => 'getProgramPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array(
                    'program_pasar_id' => $this->input->post('program_pasar_id'),
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('program_pasar', $response);
                echo json_encode($this->session->userdata('program_pasar'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_program_pasar()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('program_pasar', 'Program Pasar', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'program_pasar_id' => $data_post['program_pasar_id'],
            'program_pasar' => $data_post['program_pasar'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setProgramPasar',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'setProgramPasar',
                'method'        => 'POST',
                'controller'   => 'master',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('program_pasar', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/program-pasar');
    }

    public function tipe_pasar()
    {
        $this->data['title']  = 'Tipe Pasar';
        $this->data['js']     = 'tipe_pasar_js';
        $this->data['menu'] = 'tipe-pasar';
        $this->templatebackend('tipe_pasar', $this->data);
    }

    function getTipe_pasar()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('tipe_pasar')['data'][$this->input->post('action_index')]);
            exit();
        }
        if ($this->session->userdata('tipe_pasar') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('tipe_pasar'));
        } else {
            $api = array(
                'endpoint'      => 'getTipePasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array(
                    'tipe_pasar_id' => $this->input->post('tipe_pasar_id'),
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response);
                echo json_encode($this->session->userdata('tipe_pasar'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function cu_tipe_pasar()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('tipe_pasar', 'Tipe Pasar', 'trim|required');
        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }
        $post_field = array(
            'token' => $this->token,
            'tipe_pasar_id' => $data_post['tipe_pasar_id'],
            'tipe_pasar' => $data_post['tipe_pasar'],
            'email' => $this->session->userdata('email', true)
        );
        //        var_dump($post_field);die();


        $api = array(
            'endpoint'      => 'setTipePasar',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        // var_dump($response);die();

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'setTipePasar',
                'method'        => 'POST',
                'controller'   => 'master',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/tipe-pasar');
    }

    public function filter_harga()
    {
        $this->data['title']  = 'Filter Harga';
        $this->data['js']     = 'filter_harga_js';
        $this->data['menu'] = 'filter-harga';
        $this->templatebackend('filter_harga', $this->data);
    }

    function getFilter_harga()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            //var_dump($this->session->userdata('filter_harga')[$pagenumber]);die();
            echo json_encode($this->session->userdata('filter_harga')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('filter_harga')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('filter_harga')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getKomoditiTampil',
                'controller'    => 'api',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')),
                    'komoditi_tampil_id' => $this->input->post('komoditi_tampil_id'),
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['filter_harga'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('filter_harga')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function cu_filter_harga()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('jenis_komoditi', 'Jenis Komoditi', 'trim|required');
        $this->form_validation->set_rules('varian_komoditi', 'Varian Komoditi', 'trim|required');
        $this->form_validation->set_rules('flag_tampil', 'Flag Tampil', 'trim|required');

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            exit();
        }

        $post_field = array(
            'token' => $this->token,
            'komoditi_tampil_id' => $data_post['komoditi_tampil_id'],
            'jenis_komoditi_id' => $data_post['jenis_komoditi'],
            'varian_komoditi_id' => $data_post['varian_komoditi'],
            'flag_tampil' => $data_post['flag_tampil'],
            'email' => $this->session->userdata('email', true)
        );

        $api = array(
            'endpoint'      => 'setKomoditiTampil',
            'method'        => 'POST',
            'controller'   => 'api',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $api = array(
                'endpoint'      => 'setKomoditiTampil',
                'method'        => 'POST',
                'controller'   => 'api',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('filter_harga', $response);
            }
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/filter-harga');
    }


    public function dokumen()
    {

        $this->data['title']  = 'Dokumen';
        $this->data['js']     = 'master/dokumen_js';
        $this->data['menu'] = 'dokumen';
        $this->data['menu_id'] = 79;
        $this->templatebackend('master/dokumen', $this->data);
    }

    function getDokumen()
    {
       
        if (empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('master_dokumen');
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getDokumen',
                'controller'    => 'master',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => (empty($this->input->post('length')) ? 10 : $this->input->post('length')),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')),
                    'dokumen_id' => $this->input->post('dokumen_id'),
                    'jenis' => $this->input->post('jenis'),
                )
            );

            $response = $this->serviceAPI($api);


            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['master_dokumen'] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('master_dokumen'));
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function cu_dokumen()
    {
        $data_post = $this->input->post();
        $this->form_validation->set_rules('jenis', 'Jenis Dokumen', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul Dokumen', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi Dokumen', 'trim|required');
        $this->form_validation->set_rules('required', 'Mandatori', 'trim|required');
        $this->form_validation->set_rules('acceptx[]', 'Accept', 'required');
        $this->form_validation->set_rules('max', 'Max Size (MB)', 'required');

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            pre(validation_errors());
            redirect(base_url() . 'web/master/dokumen');
        }

        $post_field = array(
            'token' => $this->token,
            'email' => $this->session->userdata('email', true),
            'dokumen_id' => $data_post['dokumen_id'],
            'jenis' => $data_post['jenis'],
            'judul' => $data_post['judul'],
            'deskripsi' => $data_post['deskripsi'],
            'required' => $data_post['required'],
            'max_size' => $data_post['max'],
            'accept' => implode(',', $data_post['acceptx'])

        );

        $api = array(
            'endpoint'      => 'setDokumen',
            'method'        => 'POST',
            'controller'   => 'master',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data Gagal Disimpan');
        }
        redirect(base_url() . 'web/master/dokumen');
    }
}
