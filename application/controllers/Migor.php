<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migor extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_admin();

        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = 'Dashboard';
        $this->data['menu'] = 'dashboard';
        $this->data['js'] = 'admin/js/migor_js';
        $this->data['urlparams'] = ':showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link&%3F%3Aembed=yes&%3Atoolbar=yes&:dataDetails=no&:showDataDetails=yes&:alerts=no&:customViews=no&:device=desktop&:showAskData=false&:showShareOptions=false&:subscriptions=no';
        $this->data['view_tableau'] = '/views/DashboardMigor/DashboardMigor';
        $this->data['token'] = $this->getTableauToken('pdsi');
        $this->templatebackend('migor/index', $this->data);
    }
}

