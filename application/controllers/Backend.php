<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Backend extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = 'Backend';
        $this->data['menu'] = 'backend';
        $this->templatebackend('backend/master/backend', $this->data);
    }
}
