<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = $this->session->userdata('nama');
        $this->data['js'] = 'profil_js';
        $this->data['menu'] = 'profil';

        $api = array(
            'endpoint'      => 'getPengguna',
            'method'        => 'POST',
            'post_field'    => array(
                'pengguna_id' => $this->session->userdata('pengguna_id'),
                'offset' => 0,
                'limit' => 1
            )
        );
        $service = $this->serviceAPI($api);
        $this->data['pengguna'] = $service['data'];

        $this->templatebackend('pengguna/profil', $this->data);
    }

    public function save_password()
    {

        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        $this->form_validation->set_rules('konfirmasi_password', 'Konfirmasi Password', 'required|trim');

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
			$this->session->set_flashdata('message', 'Password Gagal diganti');
        } else {

            if($this->input->post('password') != $this->input->post('konfirmasi_password')){
                $this->session->set_flashdata('alert', 'error');
			    $this->session->set_flashdata('message', 'Password dan Konfirmasi Password tidak sama');
            }

            $params = array(
                'endpoint'      => 'setPassword',
                'method'        => 'POST',
                'post_field'    => array(
                    'email' => $this->session->userdata('email'),
                    'password' => $this->input->post('password'),
                    'confirm_password' => $this->input->post('konfirmasi_password'),
                    'token' => $this->token,
                )
            );

            $insert = $this->serviceAPI($params);

            if (!$insert) {
                $this->session->set_flashdata('alert', 'error');
			    $this->session->set_flashdata('message', $insert['keterangan']);
            } else if ($insert && $insert['kode'] == 200) {
                $this->data['response']['success'] = true;
                $this->data['response']['keterangan'] = 'Data Berhasil disimpan';
                $this->session->set_flashdata('alert', 'success');
			    $this->session->set_flashdata('message', 'Password berhasil diubah');
            } else {
                $this->session->set_flashdata('alert', 'error');
			    $this->session->set_flashdata('message', $insert['keterangan']);
            }
        }

        redirect(base_url().'web/profil');
    }
}
