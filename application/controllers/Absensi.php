<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_admin();
        $this->is_login();
    }

    public function rekap()
    {
        $this->data['title']  = 'Rekap Absensi';
        $this->data['js']     = 'rekap_absensi_js';
        $this->data['menu'] = 'pengguna';

        $this->templatebackend('absensi/rekap', $this->data);
    }

    public function rekapHarga()
    {
        $this->data['title']  = 'Absensi Lapor Harga';
        $this->data['js']     = 'rekap_absensi_harga_js';
        $this->data['menu'] = 'pengguna';

        $this->templatebackend('absensi/rekap', $this->data);
    }

    public function rekapJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('rekap_absensi')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('rekap_absensi')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('rekap_absensi')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getAbsensi',
                'method'        => 'POST',
                'controller'    => 'contributor',
                'post_field'    => array('token' => $this->token, 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')), 'rekap' => true)
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['rekap_absensi'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('rekap_absensi')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    function cu_pengguna()
    {
        $this->form_validation->set_rules('tipe_pengguna', 'Tipe Pengguna', 'required|trim');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

        if (empty($this->input->post('pengguna_id'))) {
            $this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]|min_length[8]');
            $this->form_validation->set_rules('password_confirm', 'Password', 'required|matches[password]|min_length[8]');
        }

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            redirect(base_url() . 'pengguna');
        }
        $post_field = array(
            'token' => $this->token,
            'pengguna_id' => $this->input->post('pengguna_id', true),
            'nama_lengkap' => $this->input->post('nama_lengkap', true),
            'nip' => $this->input->post('nip', true),
            'email' => $this->input->post('email', true),
            'tipe_pengguna' => $this->input->post('tipe_pengguna', true),
            'no_hp' => $this->input->post('no_hp', true),
            'password' => $this->input->post('password', true),
            'confirm_password' => $this->input->post('password_confirm', true),
            'alamat' => $this->input->post('alamat', true),
            'daerah_id' => $this->input->post('kecamatan', true),
            'token' => $this->token
        );

        $api = array(
            'endpoint'      => 'daftar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {

            $this->session->unset_userdata('pengguna');

            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $response['keterangan']);
        }
        redirect(base_url() . 'pengguna');
    }
}
