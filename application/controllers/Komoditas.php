<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Komoditas extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->is_login();

        $this->is_admin();
    }

    public function index()
    {
        $this->data['title']  = 'Data Provinsi Perdagangan';
        $this->data['menu'] = 'dataprovperdagangan';

        if (strtolower($this->session->userdata('role')) != 'dinas provinsi') {
            $this->data['js'] = 'admin/js/komoditas_provinsi_js';
        } else {
            $this->data['js'] = 'admin/js/komoditas_provinsi_dinas_js';
        }

        $current_year = date('Y');
        $latest_year = $current_year - 5;

        $years = [];

        for ($i = $current_year; $i >= $latest_year; $i--) {
            $years[] = $i;
        }

        $api = array(
            'endpoint'      => 'getJenisKomoditi',
            'method'        => 'POST',
            'controller'    => 'pasar'
        );

        $get_jenis_komoditi = $this->serviceAPI($api);

        $data_jenis_komoditi[0]['kelompok'] = 'Barang Kebutuhan Pokok';
        $data_jenis_komoditi[1]['kelompok'] = 'Barang Penting';

        foreach ($get_jenis_komoditi['data'] as $key => $row) {
            if ($row['kelompok'] == 'Bapok') {
                $data_jenis_komoditi[0]['sub'][] = array(
                    'id_jenis'  => $row['jenis_komoditi_id'],
                    'jenis_komoditi'    => $row['jenis_komoditi']
                );
            }

            if ($row['kelompok'] == 'Banting') {
                $data_jenis_komoditi[1]['sub'][] = array(
                    'id_jenis'  => $row['jenis_komoditi_id'],
                    'jenis_komoditi'    => $row['jenis_komoditi']
                );
            }
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['years'] = $years;
        $this->data['jenis_komoditi'] = $data_jenis_komoditi;

        $this->templatebackend('perdagangan/komoditas_provinsi', $this->data);
    }

    public function getCommodityJSON()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('komoditas_provinsi')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('komoditas_provinsi')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('komoditas_provinsi')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            $daerah_id = (strtolower($this->session->userdata('role')) == 'dinas provinsi') ? substr($this->session->userdata('daerah_id'), 0, 2) : '';

            if (!empty($this->input->post('province'))) {
                $daerah_id = $this->input->post('province');
            }

            $month = (!empty($this->input->post('month'))) ? $this->input->post('month') : '';
            $year = (!empty($this->input->post('year'))) ? $this->input->post('year') : date('Y');

            $api = array(
                'endpoint'      => 'getStok',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array('token' => $this->token, 'search' => $this->input->post('search')['value'], 'limit' => $this->input->post('length'), 'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start')), 'email' => $this->session->userdata('email'), 'daerah_id' => $daerah_id, 'bulan' => $month, 'tahun' => $year)
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();

                foreach ($response['data'] as $key => $row) {
                    $response['data'][$key]['tanggal'] = $row['bulan'] . '/' . $row['tahun'];
                    $response['data'][$key]['produksi'] = rupiah($row['produksi']);
                    $response['data'][$key]['konsumsi'] = rupiah($row['konsumsi']);
                }

                $session['komoditas_provinsi'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('komoditas_provinsi')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function save()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('date', 'Tanggal', 'trim|required');
        $this->form_validation->set_rules('commodity', 'Komoditi', 'trim|required');
        $this->form_validation->set_rules('unit', 'Satuan', 'trim|required');
        $this->form_validation->set_rules('produksi', 'Produksi', 'trim|required');
        $this->form_validation->set_rules('konsumsi', 'Konsumsi', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (strtolower($this->session->userdata('role')) != 'dinas provinsi') {
            $this->form_validation->set_rules('province', 'Provinsi', 'trim|required');
        }

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $daerah_id = (strtolower($this->session->userdata('role')) != 'dinas provinsi') ? $this->input->post('province') : substr($this->session->userdata('daerah_id'), 0, 2);

            $api = array(
                'endpoint'      => 'setStok',
                'method'        => 'POST',
                'controller'    => 'data',
                'post_field'    => array(
                    'token' => $this->token,
                    'daerah_id' => $daerah_id,
                    'jenis_komoditi_id' => $this->input->post('commodity'),
                    'satuan_komoditi_id'    => $this->input->post('unit'),
                    'bulan' => date('m', strtotime($this->input->post('date'))),
                    'tahun' => date('Y', strtotime($this->input->post('date'))),
                    'produksi'  => $this->input->post('produksi'),
                    'konsumsi'  => $this->input->post('konsumsi'),
                    'email' => $this->session->userdata('email')
                )
            );

            $save = $this->serviceAPI($api);

            if (!$save) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($save && $save['kode'] != 200) {
                $this->data['response']['code'] = $save['kode'];
                $this->data['response']['message'] = $save['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $save['kode'];
                $this->data['response']['message'] = $save['keterangan'];
                $this->data['response']['success'] = true;
            }
        }

        echo json_encode($this->data['response']);
    }
}
