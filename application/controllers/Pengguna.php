<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna extends Admin_Controller
{
    public function __construct()
    {
        
        parent::__construct();
        $this->is_admin();
        $this->is_login();
    }

    public function index()
    {
        $this->data['title']  = 'Management Pengguna';
        $this->data['js']     = 'pengguna_js';
        $this->data['menu'] = 'pengguna';


        if (!$this->session->userdata('tipe_pengguna')) {
            $api = array(
                'endpoint'      => 'getTipePengguna',
                'method'        => 'POST',
            );

            $service = $this->serviceAPI($api);

            if ($service && $service['kode'] == 200) {
                $user_types = $service['data'];

                $this->session->set_userdata('tipe_pengguna', $user_types);
            }
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'post_field'    => array('daerah_id' => 1)
            );

            $province_api = $this->serviceAPI($api);

            if ($province_api && $province_api['kode'] == 200) {
                $province = $province_api['data'];

                $this->session->set_userdata('master_provinsi', $province);
            }
        }

        if (!$this->session->userdata('master_role_internal')) {
            $params = array(
                'endpoint'      => 'getTipePengguna',
                'method'        => 'POST',
                'post_field'    => array('flag_publik' => 'N')
            );

            $service = $this->serviceAPI($params);

            if ($service && $service['kode'] == 200) {
                $role_internal = $service['data'];

                $this->session->set_userdata('master_role_internal', $role_internal);
            }
        }

        $params = array(
            'endpoint'      => 'getTipePengguna',
            'method'        => 'POST',
            'post_field'    => array('flag_publik' => 'N')
        );

        $service = $this->serviceAPI($params);

        if ($service && $service['kode'] == 200) {
            $role_internal = $service['data'];

            $this->session->set_userdata('master_role_internal', $role_internal);
        }

        $this->data['user_types'] = $this->session->userdata('tipe_pengguna');
        $this->data['provinces'] = $this->session->userdata('master_provinsi');
        $this->data['roles'] = $this->session->userdata('master_role_internal');
        $this->templatebackend('pengguna/pengguna', $this->data);
    }

    public function pengguna_json()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pengguna')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pengguna')[$pagenumber]) && empty($this->input->post('refresh')) && empty($this->input->post('tipe_pengguna'))) {
            $session_existing = $this->session->userdata('pengguna')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPengguna',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'tipe_pengguna' => $this->input->post('tipe_pengguna'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);
            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['pengguna'][$pagenumber] = $response;
                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pengguna')[$pagenumber]);
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    public function save()
    {
        $this->data['response'] = array('success' => false, 'validation' => array(), 'code' => 200);

        $this->form_validation->set_rules('role', 'Role', 'required|trim');
        $this->form_validation->set_rules('name', 'Nama', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('hp', 'No. HP', 'required|trim');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
        } else {
            $params = array(
                'endpoint'      => 'daftar',
                'method'        => 'POST',
                'post_field'    => array(
                    'nama_lengkap' => $this->input->post('name', true),
                    'nip' => empty($this->input->post('nip')) ? null : $this->input->post('nip', true),
                    'email' => $this->input->post('email', true),
                    'no_hp' => $this->input->post('hp', true),
                    'daerah_id' => $this->input->post('daerah_id').'0000',
                    'alamat'    => 'Jl. M. I. Ridwan Rais No. 5',
                    'tipe_pengguna' => $this->input->post('role'),
                    'tipe_pengguna_lainnya' => (empty($this->input->post('role_lainnya'))) ? null : implode(',',$this->input->post('role_lainnya')),
                    'instansi' => $this->input->post('instansi'),
                    'status'    => 'Aktif',
                    'flag'  => 'backend',
                    'token' => $this->token,
                )
            );
            if(!empty($this->input->post('password'))){
                $params['post_field']['password'] = $this->input->post('password');
                $params['post_field']['confirm_password'] = $this->input->post('password');
            }

            if (!empty($this->input->post('pengguna_id'))) {
                $params['post_field']['pengguna_id'] = $this->input->post('pengguna_id');
            }

            $insert = $this->serviceAPI($params);
            //var_dump($insert['keterangan']);die();

            if (!$insert) {
                $this->data['response']['code'] = 401;
                $this->data['response']['keterangan'] = $insert['keterangan'];
            } else if ($insert && $insert['kode'] == 200) {
                $this->data['response']['success'] = true;
                $this->data['response']['keterangan'] = 'Data Berhasil disimpan';
            } else {
                $this->data['response']['code'] = 404;
                $this->data['response']['keterangan'] = $insert['keterangan'];
            }
        }

        echo json_encode($this->data['response']);
    }

    function cu_pengguna()
    {
        $this->form_validation->set_rules('tipe_pengguna', 'Tipe Pengguna', 'required|trim');
        $this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|trim');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|trim');
        $this->form_validation->set_rules('provinsi', 'Provinsi', 'required|trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'required|trim');
        $this->form_validation->set_rules('kecamatan', 'Kecamatan', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

        if (empty($this->input->post('pengguna_id'))) {
            $this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]|min_length[8]');
            $this->form_validation->set_rules('password_confirm', 'Password', 'required|matches[password]|min_length[8]');
        }

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Mohon cek inputan anda');
            redirect(base_url() . 'pengguna');
        }
        $post_field = array(
            'token' => $this->token,
            'pengguna_id' => $this->input->post('pengguna_id', true),
            'nama_lengkap' => $this->input->post('nama_lengkap', true),
            'nip' => $this->input->post('nip', true),
            'email' => $this->input->post('email', true),
            'tipe_pengguna' => $this->input->post('tipe_pengguna', true),
            'no_hp' => $this->input->post('no_hp', true),
            'password' => $this->input->post('password', true),
            'confirm_password' => $this->input->post('password_confirm', true),
            'alamat' => $this->input->post('alamat', true),
            'daerah_id' => $this->input->post('kecamatan', true),
            'token' => $this->token
        );

        $api = array(
            'endpoint'      => 'daftar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        if ($response['kode'] == 200) {

            $this->session->unset_userdata('pengguna');

            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $response['keterangan']);
        }
        redirect(base_url() . 'pengguna');
    }

    public function searchNIP()
    {
        $response = array('success' => false, 'data' => array(), 'message' => 'Data tidak ditemukan');

        $nip = $this->input->post('nip');

        if ($nip != null) {

            $params = array(
                'token' => $this->token,
                'endpoint'  => 'getPengguna',
                'method'    => 'POST',
                'post_field'    => array(
                    'nip'   => $nip,
                    'limit' => 1,
                    'offset'    => 0
                )
            );

            $service = $this->serviceAPI($params);

            if ($service['kode'] == 200) {
                $response['message'] = 'Pengguna sudah terdaftar';
            } else {
                $check = $this->apiPegawai($nip);

                if ($check['kode'] == 200 && $check['data'] != null) {
                    $response['success'] = true;
                    $response['data']['nip'] = $check['data'][0]['nip'];
                    $response['data']['nama'] = ucwords(strtolower($check['data'][0]['nama']));
                    $response['data']['email'] = $check['data'][0]['email'];
                    $response['message'] = '';
                }
            }
        } else {
            $response['message'] = '';
        }

        echo json_encode($response);
    }

    private function apiPegawai($nip = null)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://intra.kemendag.go.id/pegawai/cariPegawai',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('keyword' => $nip),
            CURLOPT_HTTPHEADER => array(
                'x-api-key: O5zkqEa3AR5CP7phG1vUjr0XdBX9yNT4EdEDhAmg'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $data = json_decode($response, true);

        return $data;
    }

    function permission()
    {
        //var_dump($this->token);die();
        $this->data['title']  = 'Data Permission';
        $this->data['js']     = 'permission_js';
        $this->data['menu'] = 'permission';

        $api = array(
            'endpoint'      => 'getMenu',
            'method'        => 'POST',
            'post_field'    => array(
                'parent_null' => 'Y',
                'parent_id' => 'Aktif',
            )
        );

        $this->data['parent'] = $this->serviceAPI($api);

        $this->templatebackend('pengguna/permission', $this->data);
    }

    public function getPermission()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('permission')['data'][$this->input->post('action_index')]);
            exit();
        }

        if ($this->session->userdata('permission') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('permission'));
        } else {
            $api = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'status' => $this->input->post('status'),
                    'jenis' => $this->input->post('jenis'),
                )
            );

            $response = $this->serviceAPI($api);

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('permission', $response);
                echo json_encode($this->session->userdata('permission'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function crud_permission()
    {
        $data_post = $this->input->post();

        $post_field = array(
            'token' => $this->token,
            'menu_id' => $data_post['menu_id'],
            'menu' => $data_post['nama_menu'],
            'url' => $data_post['controller'],
            'icon' => $data_post['icon'],
            'jenis' => $data_post['jenis'],
            'urutan' => $data_post['urutan'],
            'publik' => $data_post['publik'],
            'email' => $this->session->userdata('email', true)
        );

        $api = array(
            'endpoint'      => 'setMenu',
            'method'        => 'POST',
            'controller'   => 'api',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($api);

        //var_dump($response);die();

        if ($response['kode'] == 200) {

            $api = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array()
            );

            $response = $this->serviceAPI($api);
            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('permission', $response);
            }

            echo json_encode(array('kode' => 200, 'keterangan' => 'Data berhasil disimpan', 'jenis' => $data_post['jenis']));
        } else {
            echo json_encode(array('kode' => 400, 'keterangan' => 'Data gagal disimpan'));
        }
    }

    public function getTipe_pengguna_permission()
    {
        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('tipe_pengguna_permission')['data'][$this->input->post('action_index')]);
            exit();
        }

        if ($this->session->userdata('tipe_pengguna_permission') && empty($this->input->post('refresh'))) {
            echo json_encode($this->session->userdata('tipe_pengguna_permission'));
        } else {
            $api = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'status' => $this->input->post('status'),
                    'urutan'=>'ASC',
                    'parent_null'=>'true'
                )
            );

            $response = $this->serviceAPI($api);

            $api = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'status' => $this->input->post('status'),
                    'urutan'=>'ASC',
                    'parent_null'=>'false'
                )
            );

            $response1 = $this->serviceAPI($api);

            $response['data'] = array_merge($response['data'], $response1['data']);
       
            $api_ = array(
                'endpoint'      => 'getTipePenggunaMenu',
                'method'        => 'POST',
                'controller'   => 'api',
                'post_field'    => array(
                    'tipe_pengguna_id' => $this->input->post('tipe_pengguna_id')
                    
                )
            );
            $data = $this->serviceAPI($api_);
            $tipePenggunaMenu = [];
            foreach($data['data'] as $k=>$v){
                $tipePenggunaMenu[$v['menu_id']] = [
                    'tipe_pengguna_menu_id' => $v['tipe_pengguna_menu_id'],
                    'create' => $v['create'],
                    'update' => $v['update'],
                    'delete' => $v['delete'],
                ];
            }
          
            foreach ($response['data'] as $k => $v) {
    
                if(isset($tipePenggunaMenu[$v['menu_id']])){
                    $response['data'][$k]['tipe_pengguna_menu_id'] = $tipePenggunaMenu[$v['menu_id']]['tipe_pengguna_menu_id'];
                    $response['data'][$k]['view'] = 'Y';
                    $response['data'][$k]['create'] =$tipePenggunaMenu[$v['menu_id']]['create'];
                    $response['data'][$k]['update'] = $tipePenggunaMenu[$v['menu_id']]['update'];
                    $response['data'][$k]['delete'] = $tipePenggunaMenu[$v['menu_id']]['delete'];
                }else{
                    $response['data'][$k]['tipe_pengguna_menu_id'] = '';
                    $response['data'][$k]['view'] = 'N';
                    $response['data'][$k]['create'] = 'N';
                    $response['data'][$k]['update'] = 'N';
                    $response['data'][$k]['delete'] = 'N';
                }
            }

            if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pengguna_permission', $response);
                echo json_encode($this->session->userdata('tipe_pengguna_permission'));
            } else {
                echo json_encode(array('data' => array()));
            }
        }
    }

    function getTipe_pengguna_menu(){
        $api = array(
            'endpoint'      => 'getTipePenggunaMenu',
            'method'        => 'POST',
            'controller'   => 'api',
            'post_field'    => array(
                'tipe_pengguna_id' => $this->input->post('tipe_pengguna_id'),
                'menu_id' => $this->input->post('menu_id'),
            )
        );
        $response = $this->serviceAPI($api);
        

        if ($response && $response['kode'] == 200) {
            $this->session->set_userdata('tipe_pengguna_menu', $response);
            echo json_encode($this->session->userdata('tipe_pengguna_menu'));
        } else {
            echo json_encode(array('data' => array()));
        }
    }

    function crud_tipe_pengguna_permission()
    {
        $data_post = $this->input->post();

        $tipe_pengguna_menu_id = array_keys($data_post['aksi']);
        if(isset($data_post['all'])){
        $deleted = array_diff($data_post['all'],$tipe_pengguna_menu_id);

        // echo '<pre>';
        // var_dump($data_post['aksi']);
        // die();

        foreach($deleted as $k=>$v){
            $delete_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_tipe_pengguna_menu',
                'key_name'  => 'tipe_pengguna_menu_id',
                'key'   => $v,
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );
    
            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $delete_field
            );
    
            $delete_pasar = $this->serviceAPI($api);
        }
    }

        foreach ($data_post['aksi'] as $k => $v) {
            $post_field = array(
                'token' => $this->token,
                'menu_id' => $v,
                'tipe_pengguna_id' => $data_post['tipe_pengguna_id'],
                'tipe_pengguna_menu_id' => $data_post['all'][$k],
                'create' => empty($data_post['create'][$v]) ? 'N':$data_post['create'][$v],
                'update' => empty($data_post['update'][$v]) ? 'N':$data_post['update'][$v],
                'delete' => empty($data_post['hapus'][$v]) ? 'N':$data_post['hapus'][$v],
                'email' => $this->session->userdata('email', true)
            );
          
            $api = array(
                'endpoint'      => 'setTipePenggunaMenu',
                'method'        => 'POST',
                'controller'   => 'api',
                'post_field'    => $post_field
            );

            $response = $this->serviceAPI($api);
        }

        //var_dump($this->token);die();

        
        if ($response['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Akses Berhasil Di Ubah');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Akses Gagal Di Ubah');
        }
        redirect(base_url() . 'web/tipe-pengguna');
    }

    function getUrutan_menu()
	{

        $api = array(
            'endpoint'      => 'getMenu',
            'method'        => 'POST',
            'post_field'    => array(
                'status' => 'Aktif',
                'jenis' => $this->input->post('jenis'),
                'urutan' => 'ASC',
                'parent_null' => 'Yes'
            )
        );
        $response = $this->serviceAPI($api);

        foreach ($response['data'] as $k => $v) {
            $sub_menu = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'status' => 'Aktif',
                    'urutan' => 'ASC',
                    'jenis' => $this->input->post('jenis'),
                    'parent_id' => $v['menu_id']
                )
            );
            $get = $this->serviceAPI($sub_menu);

            if($get['kode'] == 200){
                $response['data'][$k]['parent'] = $get['data'];
            }else{
                $response['data'][$k]['parent'] = [];
            }

        }

        if ($response && $response['kode'] == 200) {
            echo json_encode($response);
        } else {
            echo json_encode(array('data' => array()));
        }
	}

    function update_urutan()
	{
		$data = $this->input->post('after');
        $before = $this->input->post('before');
        // echo '<pre>';
        // var_dump($before);
        // echo '<br>';
        // var_dump($data);die();
        $update_menu = array();
        $update_sub_menu = array();
       // var_dump($data[2]['children'][0]['id']);
        foreach($data as $k=>$v){

            if (isset($v['children'])) {
                foreach ($v['children'] as $a => $b) {
                    
                    if($b['id'] != $before[$k]['children'][$a]['id']){
                        $update_sub_menu[] = array(
                            'id' => $b['id'],
                            'parent_id' => $v['id'],
                            'urutan' => $a+1
                        );
                    }

                }
            }

            if($v['id'] != $before[$k]['id']){
                $update_menu[] = array(
                    'id' => $v['id'],
                    'urutan' => $k+1
                );
            }
        }

        // echo '<pre> Menu<br>';
        // var_dump($update_menu);
        // echo '<pre> sub <br>';
        // var_dump($update_sub_menu);
        // die();

        // Update Menu 
        $count_menu = 0;
        foreach ($update_menu as $k => $v) {
            $con = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'menu_id' => $v['id']
                )
            );
            $get = $this->serviceAPI($con);

            $post_field = array(
                'token' => $this->token,
                'menu_id' => $get['data'][0]['menu_id'],
                'menu' => $get['data'][0]['menu'],
                'url' => $get['data'][0]['url'],
                'icon' => $get['data'][0]['icon'],
                'jenis' => $get['data'][0]['jenis'],
                'urutan' => $v['urutan'],
                'publik' => $get['data'][0]['publik'],
                'parent_id' => null,
                'email' => $this->session->userdata('email', true)
            );
    
            $api = array(
                'endpoint'      => 'setMenu',
                'method'        => 'POST',
                'controller'   => 'api',
                'post_field'    => $post_field
            );
    
            $response = $this->serviceAPI($api);

            if ($response['kode'] == 200) {
                $count_menu++;
            }
        }

         // Update Sub Menu 
        $count_sub_menu = 0;
        foreach ($update_sub_menu as $k => $v) {
            $con = array(
                'endpoint'      => 'getMenu',
                'method'        => 'POST',
                'post_field'    => array(
                    'menu_id' => $v['id']
                )
            );
            $get = $this->serviceAPI($con);

            $post_field = array(
                'token' => $this->token,
                'menu_id' => $get['data'][0]['menu_id'],
                'menu' => $get['data'][0]['menu'],
                'url' => $get['data'][0]['url'],
                'icon' => $get['data'][0]['icon'],
                'jenis' => $get['data'][0]['jenis'],
                'urutan' => $v['urutan'],
                'publik' => $get['data'][0]['publik'],
                'parent_id' => $v['parent_id'],
                'email' => $this->session->userdata('email', true)
            );
    
            $api = array(
                'endpoint'      => 'setMenu',
                'method'        => 'POST',
                'controller'   => 'api',
                'post_field'    => $post_field
            );
    
            $response = $this->serviceAPI($api);

            if ($response['kode'] == 200) {
                $count_sub_menu++;
            }
        }

		if ($count_menu > 0 || $count_sub_menu > 0) {
			echo json_encode(array('kode' => 200, 'keterangan' => 'Urutan sukses disimpan'));
		} else {
			echo json_encode(array('kode' => 400, 'keterangan' => 'Urutan gagal disimpan'));
		}
	}

}
