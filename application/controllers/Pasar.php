<?php

use \Firebase\JWT\JWT;
use function PHPSTORM_META\map;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

defined('BASEPATH') or exit('No direct script access allowed');

class Pasar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login();
    }

    public function index()
    {
        if (!has_access(2, 'show')) {
            redirect(base_url() . '404_override');
        }

        $this->data['title']  = 'Data Pasar';
        $this->data['js']     = 'pasar_js';
        $this->data['css']    = 'pasar_css';
        $this->data['menu'] = 'pasar';
        $this->data['menu_id'] = 2;

        if ($this->session->userdata('tipe_pengguna_id') == 2) {
            $daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
        } else if (in_array($this->session->userdata('tipe_pengguna_id'), [9, 19])) {
            $daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
        } else if (in_array($this->session->userdata('tipe_pengguna_id'), [8, 18])) {
            $daerah_id = substr($this->session->userdata('daerah_id'), 0, 2);
        } else {
            $daerah_id = 0;
        }

        $this->data['daerah_id'] = $daerah_id;

        $this->templatebackend('pasar/index', $this->data);
    }

    function getPasar()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('pasar')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('pasar')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('pasar')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if ($this->session->userdata('tipe_pengguna_id') == 2) {
                $daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
            } else if (in_array($this->session->userdata('tipe_pengguna_id'), [9, 19])) {
                $daerah_id = substr($this->session->userdata('daerah_id'), 0, 4);
            } else if (in_array($this->session->userdata('tipe_pengguna_id'), [8, 18])) {
                $daerah_id = substr($this->session->userdata('daerah_id'), 0, 2);
            } else {
                $daerah_id = 0;
            }

            if (!empty($this->input->post('daerah_id'))) {
                $daerah_id = $this->input->post('daerah_id');
            }

            $tipe_pasar = '';
            if (!empty($this->input->post('tipe_pasar_id'))) {
                $tipe_pasar = $this->input->post('tipe_pasar_id');
            }

            $api = array(
                'endpoint'      => 'getPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array(
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'daerah_id' => $daerah_id,
                    'tipe_pasar_id' => $tipe_pasar,
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            if (!empty($this->input->post('verify_coordinate'))) {
                $api['post_field']['verify_coordinate'] = $this->input->post('verify_coordinate');
            }

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');
            if ($response && $response['kode'] == 200) {

                $session = $this->session->all_userdata();
                $session['pasar'][$pagenumber] = $response;

                foreach ($session['pasar'][$pagenumber]['data'] as $key => $row) {
                    $session['pasar'][$pagenumber]['data'][$key]['token'] = $this->jwtEncode($row['pasar_id']);
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('pasar')[$pagenumber]);
            } else {
                echo json_encode(array('recordsTotal' => 0, 'recordsFiltered' => 0, 'data' => array()));
            }
        }
    }

    function save_pasar()
    {
        $data_post  = $this->input->post();

        $data_pasar = array(
            'token' => $this->token,
            'pasar_id'  => $data_post['pasar_id'],
            'email' => $this->session->userdata('email'),
            'nama'  => $data_post['nama_pasar'],
            'deskripsi'  => $data_post['deskripsi'],
            'alamat'  => $data_post['alamat'],
            'latitude'  => $data_post['latitude'],
            'longitude'  => $data_post['longtitude'],
            'daerah_id'  => $data_post['daerah_id'],
            'kode_pos'  => $data_post['kodepos'],
            'asal'  => $data_post['asal'],
        );


        if ($_FILES['foto_depan']['tmp_name'] != '') {
            $data_pasar['foto_depan'] = new CurlFile($_FILES['foto_depan']['tmp_name'], $_FILES['foto_depan']['type'], $_FILES['foto_depan']['name']);
        }

        if ($_FILES['foto_dalam']['tmp_name'] != '') {
            $data_pasar['foto_dalam'] = new CurlFile($_FILES['foto_dalam']['tmp_name'], $_FILES['foto_dalam']['type'], $_FILES['foto_dalam']['name']);
        }

        if (empty($data_post['simple'])) {
            $data_pasar['verify_kelengkapan'] = date('Y-m-d H:i:s');
        }


        $api_pasar = array(
            'endpoint'      => 'setPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $data_pasar
        );

        $service = $this->serviceAPI($api_pasar);
        //var_dump($service);die();
        if ($service['kode'] == 200) {

            if (empty($data_post['simple'])) {
                // Input Data Detail Pasar
                $api_detail = array(
                    'endpoint'      => 'setDetailPasar',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => array(
                        'token' => $this->token,
                        'pasar_detail_id'   => $data_post['pasar_detail_id'],
                        'nama_pasar'    => $data_post['nama_pasar'],
                        'pasar_id'  => !empty($data_post['pasar_id']) ? $data_post['pasar_id'] : $service['data']['pasar_id'],
                        'no_telp'   => $data_post['no_telp'],
                        'no_fax'    => $data_post['no_fax'],
                        'jenis_pasar'   => $data_post['klasifikasi'],
                        'tipe_pasar_id' => $data_post['tipe_pasar'],
                        'bentuk_pasar'  => $data_post['bentuk_pasar'],
                        'kepemilikan'   => $data_post['kepemilikan'],
                        'kondisi'   => $data_post['kondisi_pasar'],
                        // 'pengelola_pasar'   => $pengelola,
                        'waktu_operasional' => implode(',', $data_post['waktu_operasional']),
                        'jam_operasional_awal'  => date("H:i:s", strtotime($data_post['jam_buka'])),
                        'jam_operasional_akhir' => date("H:i:s", strtotime($data_post['jam_tutup'])),
                        'jam_sibuk_awal'    => date("H:i:s", strtotime($data_post['jam_mulai_sibuk'])),
                        'jam_sibuk_akhir'   => date("H:i:s", strtotime($data_post['jam_berakhir_sibuk'])),
                        'jumlah_pekerja_tetap'  => str_replace('.', '', $data_post['pekerja_tetap']),
                        'jumlah_pekerja_nontetap'   => str_replace('.', '', $data_post['pekerja_nontetap']),
                        'luas_bangunan' => str_replace('.', '', $data_post['luas_bangunan']),
                        'luas_tanah'    => str_replace('.', '', $data_post['luas_tanah']),
                        'jumlah_lantai' => str_replace('.', '', $data_post['jumlah_lantai']),
                        'tahun_bangun'  => $data_post['tahun_dibangun'],
                        'tahun_renovasi' => $data_post['tahun_renovasi'],
                        'jumlah_pengunjung' => str_replace('.', '', $data_post['jumlah_pengunjung']),
                        'jarak_toko_modern' => $data_post['jarak_toko_modern'],
                        'penerapan_digitalisasi_pasar' => $data_post['penerapan_digitalisasi_pasar'],
                        'metode_penerapan_digital_pasar' => implode(',', $data_post['metode_penerapan_digital_pasar']),
                        'pembiayaan_lk' => $data_post['pembiayaan_lembaga_keuangan'],
                        'lembaga_keuangan' => $data_post['lembaga_keuangan'],
                        'sertifikasi_sni' => $data_post['sertifikasi_sni'],
                        'tahun_sni_pasar_rakyat' => $data_post['tahun_sni_pasar_rakyat'],
                        'sarana_prasarana' => implode(',', $data_post['sarana_prasarana']),
                        'angkutan_umum' => $data_post['angkutan_umum'],
                        'jarak_pemukiman' => $data_post['dekat_pemukiman'],
                        'komoditas_dijual' => implode(',', $data_post['komoditas']),
                        'omzet_sebelumnya' => str_replace('.', '', $data_post['omzet']),
                        'tata_kelola' => $data_post['tata_kelola'],
                        'email' => $this->session->userdata('email')
                    )
                );

                $detail = $this->serviceAPI($api_detail);
                //var_dump($detail);die();


                // Input Data Anggaran Pasar
                //var_dump($data_post['program_pasar']);die();
                foreach ($data_post['program_pasar'] as $a => $b) {
                    $data_anggaran = array(
                        'token' => $this->token,
                        'pasar_id'    => !empty($data_post['pasar_id']) ? $data_post['pasar_id'] : $service['data']['pasar_id'],
                        'nama_pasar'  => $data_post['nama_pasar'],
                        'tahun'   => $data_post['tahun_anggaran'][$a],
                        'omset'    => str_replace('.', '', $data_post['omset_anggaran'][$a]),
                        'email' => $this->session->userdata('email'),
                        'jenis_anggaran' => 'pasar',
                        'anggaran' => str_replace('.', '', $data_post['jumlah_anggaran'][$a]),
                        'program_pasar_id'   => $data_post['program_pasar'][$a],
                        'pasar_anggaran_id'   => $data_post['pasar_anggaran_id'][$a],
                    );

                    $api_anggaran = array(
                        'endpoint'      => 'setAnggaranPasar',
                        'controller'    => 'pasar',
                        'method'        => 'POST',
                        'post_field'    => $data_anggaran
                    );

                    $anggaran = $this->serviceAPI($api_anggaran);
                }

                // Input Data Bangunan Pasar
                foreach ($data_post['jenis'] as $k => $v) {
                    $data_bangunan = array(
                        'token' => $this->token,
                        'jenis'    => $data_post['jenis'][$k],
                        'jumlah'  => str_replace('.', '', $data_post['jumlah'][$k]),
                        'jumlah_pedagang'   => str_replace('.', '', $data_post['jumlah_pedagang'][$k]),
                        'nama_pasar'    => $data_post['nama_pasar'],
                        'pasar_id'   => !empty($data_post['pasar_id']) ? $data_post['pasar_id'] : $service['data']['pasar_id'],
                        'email' => $this->session->userdata('email'),
                        'pasar_bangunan_id' => $data_post['pasar_bangunan_id'][$k]
                    );

                    $api_bangunan = array(
                        'endpoint'      => 'setBangunanPasar',
                        'controller'    => 'pasar',
                        'method'        => 'POST',
                        'post_field'    => $data_bangunan
                    );

                    $this->serviceAPI($api_bangunan);
                }

                // Input Data Pengelola Pasar
                $data_pengelola = array(
                    'token' => $this->token,
                    'pasar_id'    => !empty($data_post['pasar_id']) ? $data_post['pasar_id'] : $service['data']['pasar_id'],
                    'klasifikasi_pengelola'  => $data_post['pengelola'],
                    'nama_pengelola'   => $data_post['nama_pengelola'],
                    'jabatan_pengelola'   => $data_post['jabatan_pengelola'],
                    'no_telp_pengelola'   => $data_post['no_telp_pengelola'],
                    'email_pengelola'   => $data_post['email_pengelola'],
                    'tingkat_pendidikan'   => $data_post['tingkat_pendidikan'],
                    'jumlah_staf'   => $data_post['jumlah_staf'],
                    'nama_koperasi'   => $data_post['nama_koperasi'],
                    'kategori_koperasi'   => $data_post['kategori_koperasi'],
                    'no_telp_koperasi'   => $data_post['no_telp_koperasi'],
                    'jumlah_anggota_koperasi'   => $data_post['jumlah_anggota_koperasi'],
                    'no_badan_hukum_koperasi'   => $data_post['no_badan_hukum_koperasi'],
                    'no_induk_koperasi'   => $data_post['no_induk_koperasi'],
                    'besaran_retribusi_kios'   =>  str_replace('.', '', $data_post['besaran_retribusi_kios']),
                    'besaran_retribusi_los'   =>  str_replace('.', '', $data_post['besaran_retribusi_los']),
                    'besaran_retribusi_dasaran'   =>  str_replace('.', '', $data_post['besaran_retribusi_dasaran']),
                    'pendapatan_tahunan_retribusi'   =>  str_replace('.', '', $data_post['pendapatan_tahunan_retribusi']),
                    'email' => $this->session->userdata('email'),
                    'pasar_pengelola_id' => $data_post['pasar_pengelola_id']
                );

                if ($_FILES['struktur_pengelola']['tmp_name'] != '') {
                    $data_pengelola['struktur_pengelola'] = new CurlFile($_FILES['struktur_pengelola']['tmp_name'], $_FILES['struktur_pengelola']['type'], $_FILES['struktur_pengelola']['name']);
                }

                $api_pengelola = array(
                    'endpoint'      => 'setPasar_pengelola',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => $data_pengelola
                );

                $pengelola = $this->serviceAPI($api_pengelola);
            }

            //var_dump($pengelola);die();

            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data berhasil disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar');
    }

    public function saveFoto()
    {

        $apiGet = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('pasar_id' => $this->input->post('pasar_id'), 'limit' => 1, 'offset' => 0, 'token' => $this->token)
        );

        $getDataP = $this->serviceAPI($apiGet);


        $foto_lain = [];
        foreach ($_FILES['foto_pasar']['tmp_name'] as $key => $value) {
            //echo $key;
            $foto_lain[] = new CurlFile($value, $_FILES['foto_pasar']['type'][$key], $_FILES['foto_pasar']['name'][$key]);
        }

        if (count(@$getDataP['data']) == 1) {
            $post_field = array(
                'token'         => $this->token,
                'pasar_id'      => $this->input->post('pasar_id'),
                'email'         => $this->session->userdata('email', true),
                'nama'          => @$getDataP['data'][0]['nama'],
                'deskripsi'     => @$getDataP['data'][0]['deskripsi'],
                'alamat'        => @$getDataP['data'][0]['alamat'],
                'latitude'      => @$getDataP['data'][0]['latitude'],
                'longitude'     => @$getDataP['data'][0]['longitude'],
                'daerah_id'     => @$getDataP['data'][0]['daerah_id'],
                'kode_pos'      => @$getDataP['data'][0]['kode_pos'],
            );

            $count = 0;
            foreach ($_FILES['foto_pasar']['tmp_name'] as $key => $value) {

                $post_field['foto_lainnya[]'] = new CurlFile($value, $_FILES['foto_pasar']['type'][$key], $_FILES['foto_pasar']['name'][$key]);

                $api = array(
                    'endpoint'      => 'setPasar',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => $post_field
                );

                $service = $this->serviceAPI($api);

                if ($service['kode'] == 200) {
                    $count++;
                }

                //print_r($service);          
            }

            if ($count > 0) {
                $this->session->set_flashdata('alert', 'success');
                $this->session->set_flashdata('message', 'Data berhasil disimpan');
            } else {
                $this->session->set_flashdata('alert', 'error');
                $this->session->set_flashdata('message', 'Data gagal disimpan');
            }
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($this->input->post('pasar_id')));
    }

    public function delFoto()
    {

        $api = array(
            'endpoint'      => 'setStatus',
            'method'        => 'POST',
            'post_field'    => array(
                'email' => $this->session->userdata('email'),
                'token' => $this->token,
                'table_name' => 'tbl_pasar_foto',
                'key_name' => 'pasar_foto_id',
                'key' => $this->input->post('id'),
                'status' => 'Deleted',
                'keterangan_perubahan' => 'Hapus foto pasar lainya'
            )
        );

        $response = $this->serviceAPI($api);

        echo json_encode($response);
    }

    function save_identitas()
    {
        $data_post  = $this->input->post();

        $data_pasar = array(
            'token' => $this->token,
            'pasar_id'  => $data_post['pasar_id'],
            'email' => $this->session->userdata('email'),
            'nama'  => $data_post['nama_pasar'],
            'deskripsi'  => $data_post['deskripsi'],
            'alamat'  => $data_post['alamat'],
            'latitude'  => $data_post['latitude'],
            'longitude'  => $data_post['longtitude'],
            'daerah_id'  => $data_post['daerah_id'],
            'kode_pos'  => $data_post['kodepos'],
        );

        if ($_FILES['foto_depan']['tmp_name'] != '') {
            $data_pasar['foto_depan'] = new CurlFile($_FILES['foto_depan']['tmp_name'], $_FILES['foto_depan']['type'], $_FILES['foto_depan']['name']);
        }

        if ($_FILES['foto_dalam']['tmp_name'] != '') {
            $data_pasar['foto_dalam'] = new CurlFile($_FILES['foto_dalam']['tmp_name'], $_FILES['foto_dalam']['type'], $_FILES['foto_dalam']['name']);
        }

        $api_pasar = array(
            'endpoint'      => 'setPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $data_pasar
        );

        $service = $this->serviceAPI($api_pasar);

        //var_dump($service);die();
        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function save_informasi()
    {
        $data_post = $this->input->post();
        $post_field = array(
            'token' => $this->token,
            'nama_pasar'    => $this->input->post('nama_pasar'),
            'pasar_id'  => $this->input->post('pasar_id'),
            'no_telp'   => $this->input->post('no_telp'),
            'no_fax'    => $this->input->post('no_fax'),
            'jenis_pasar'   => $this->input->post('klasifikasi'),
            'tipe_pasar_id' => $this->input->post('tipe_pasar'),
            'bentuk_pasar'  => $this->input->post('bentuk_pasar'),
            // 'kepemilikan'   => $kepemilikan,
            'kondisi'   => $this->input->post('kondisi'),
            // 'pengelola_pasar'   => $pengelola,
            'waktu_operasional' => implode(',', $this->input->post('waktu_operasional')),
            'jam_operasional_awal'  => ($this->input->post('jam_buka') != '') ? date("H:i:s", strtotime($this->input->post('jam_buka'))) : null,
            'jam_operasional_akhir' => ($this->input->post('jam_tutup') != '') ? date("H:i:s", strtotime($this->input->post('jam_tutup'))) : null,
            'jam_sibuk_awal'    => ($this->input->post('jam_sibuk_awal') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_awal'))) : null,
            'jam_sibuk_akhir'  => ($this->input->post('jam_sibuk_akhir') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_akhir'))) : null,
            'jumlah_pekerja_tetap'  => str_replace('.', '', $this->input->post('pekerja_tetap')),
            'jumlah_pekerja_nontetap'   => str_replace('.', '', $this->input->post('pekerja_nontetap')),
            'luas_bangunan' => str_replace('.', '', $this->input->post('luas_bangunan')),
            'luas_tanah'    => str_replace('.', '', $this->input->post('luas_tanah')),
            'jumlah_lantai' => str_replace('.', '', $this->input->post('jumlah_lantai')),
            'tahun_bangun'  => $this->input->post('tahun_bangun'),
            'tahun_renovasi' => $this->input->post('tahun_renovasi'),
            'jumlah_pengunjung' => str_replace('.', '', $this->input->post('jumlah_pengunjung')),
            'jarak_toko_modern' => str_replace('.', '', $this->input->post('jarak_toko_modern')),
            'penerapan_digitalisasi_pasar' => $this->input->post('penerapan_digitalisasi_pasar'),
            'metode_penerapan_digital_pasar' => is_array($this->input->post('metode_penerapan_digital_pasar')) ? implode(',', $this->input->post('metode_penerapan_digital_pasar')) : $this->input->post('metode_penerapan_digital_pasar'),
            'pembiayaan_lk' => $this->input->post('pembiayaan_lembaga_keuangan'),
            'lembaga_keuangan' => empty($this->input->post('lembaga_keuangan')) ? null : $this->input->post('lembaga_keuangan'),
            'sertifikasi_sni' => $this->input->post('sertifikasi_sni'),
            'tahun_sni_pasar_rakyat' => empty($this->input->post('tahun_sni_pasar_rakyat')) ? null : $this->input->post('tahun_sni_pasar_rakyat'),
            'sarana_prasarana' => implode(',', $this->input->post('sarana_prasarana')),
            'angkutan_umum' => $this->input->post('angkutan_umum'),
            'jarak_pemukiman' => $this->input->post('dekat_pemukiman'),
            'komoditas_dijual' => implode(',', $this->input->post('komoditas')),
            'omzet_sebelumnya' => str_replace('.', '', $this->input->post('omzet_sebelumnya')),
            'email' => $this->session->userdata('email')
        );

        if (!empty($this->input->post('pasar_detail_id'))) {
            $post_field['pasar_detail_id'] = $this->input->post('pasar_detail_id');
        }

        $api = array(
            'endpoint'      => 'setDetailPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function save_bangunan()
    {
        $data_post = $this->input->post();

        $count = 0;
        foreach ($data_post['jenis'] as $k => $v) {
            $post_field = array(
                'token' => $this->token,
                'jenis'    => $data_post['jenis'][$k],
                'jumlah'  => str_replace('.', '', $data_post['jumlah'][$k]),
                'jumlah_pedagang'   => str_replace('.', '', $data_post['jumlah_pedagang'][$k]),
                'nama_pasar'    => $data_post['nama_pasar'],
                'pasar_id'   => $data_post['pasar_id'],
                'email' => $this->session->userdata('email'),
                'pasar_bangunan_id' => $data_post['pasar_bangunan_id'][$k]
            );

            $api = array(
                'endpoint'      => 'setBangunanPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if ($service['kode'] == 200) {
                $count++;
            }
        }

        if ($count > 0) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getPengelola($id)
    {
        $api = array(
            'endpoint'      => 'getPasar_pengelola',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array(
                'pasar_id' => $id,
                'token' => $this->token,
                'offset' => 0,
                'limit' => 10
            )
        );

        $response = $this->serviceAPI($api);

        return $response;
    }

    public function save_pengelola()
    {
        $data_post = $this->input->post();

        $pasar_pengelola_id = (!empty($data_post['pasar_pengelola_id'])) ? $data_post['pasar_pengelola_id'] : '';

        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'klasifikasi_pengelola'  => $data_post['klasifikasi_pengelola'],
            'nama_pengelola'   => $data_post['nama_pengelola'],
            'jabatan_pengelola'   => $data_post['jabatan_pengelola'],
            'no_telp_pengelola'   => $data_post['no_telp_pengelola'],
            'email_pengelola'   => $data_post['email_pengelola'],
            'tingkat_pendidikan'   => $data_post['tingkat_pendidikan'],
            'jumlah_staf'   => $data_post['jumlah_staf'],
            'nama_koperasi'   => $data_post['nama_koperasi'],
            'kategori_koperasi'   => $data_post['kategori_koperasi'],
            'no_telp_koperasi'   => $data_post['no_telp_koperasi'],
            'jumlah_anggota_koperasi'   => $data_post['jumlah_anggota_koperasi'],
            'no_badan_hukum_koperasi'   => $data_post['no_badan_hukum_koperasi'],
            'no_induk_koperasi'   => $data_post['no_induk_koperasi'],
            'besaran_retribusi_kios'   =>  str_replace('.', '', $data_post['besaran_retribusi_kios']),
            'besaran_retribusi_los'   =>  str_replace('.', '', $data_post['besaran_retribusi_los']),
            'besaran_retribusi_dasaran'   =>  str_replace('.', '', $data_post['besaran_retribusi_dasaran']),
            'pendapatan_tahunan_retribusi'   =>  str_replace('.', '', $data_post['pendapatan_tahunan_retribusi']),
            'email' => $this->session->userdata('email'),
            'pasar_pengelola_id' => $pasar_pengelola_id
        );

        if ($_FILES['struktur_pengelola']['tmp_name'] != '') {
            $post_field['struktur_pengelola'] = new CurlFile($_FILES['struktur_pengelola']['tmp_name'], $_FILES['struktur_pengelola']['type'], $_FILES['struktur_pengelola']['name']);
        }

        $api = array(
            'endpoint'      => 'setPasar_pengelola',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getHarga()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_harga')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        $start_date = date('Y-m-d');

        if (isset($this->session->userdata('detail_harga')[$pagenumber]) && empty($this->input->post('refresh')) && $this->input->post('start_date') == null) {
            $session_existing = $this->session->userdata('detail_harga')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {

            if ($this->input->post('start_date') != null) {
                $start_date = $this->input->post('start_date');
            }

            $api = array(
                'endpoint'      => 'getHargaKomoditi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'createdon' => $start_date,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_harga'][$pagenumber] = $response;

                foreach ($session['detail_harga'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_harga'][$pagenumber]['data'][$key]['jumlah'] = rupiah($row['jumlah']);
                    $session['detail_harga'][$pagenumber]['data'][$key]['harga'] = rupiah($row['harga']);

                    $session['detail_harga'][$pagenumber]['data'][$key]['action'] = false;
                    $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        $session['detail_harga'][$pagenumber]['data'][$key]['action'] = true;
                    }

                    if ($this->session->userdata('email') == $row['createdby'] || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        if (date('Y-m-d', strtotime($row['createdon'])) == date('Y-m-d')) {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }

                        if (strtolower($this->session->userdata('role')) == 'teknis pdn') {
                            $session['detail_harga'][$pagenumber]['data'][$key]['action_button'] = true;
                        }
                    }
                }

                $session['detail_harga'][$pagenumber]['date'] = full_date($start_date);

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_harga')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => [],
                    'date' => full_date($start_date)
                );

                echo json_encode($response);
            }
        }
    }

    public function save_harga()
    {
        $data_post = $this->input->post();

        $pasar_harga_komoditi_id = (!empty($data_post['harga_komoditi_id'])) ? $data_post['harga_komoditi_id'] : '';
        $split_jenis_komoditi = explode('|', $data_post['jenis_komoditi']);

        $post_field = array(
            'pasar_id'  => $data_post['pasar_id'],
            'jenis_komoditi_id' => $split_jenis_komoditi[0],
            'nama_pasar'    => $data_post['nama_pasar'],
            'nama_komoditi' => $split_jenis_komoditi[1],
            'varian_komoditi_id'   => $data_post['varian_komoditi'],
            'satuan_komoditi_id'    => $data_post['satuan_komoditi'],
            'harga' => str_replace('.', '', $data_post['harga']),
            'email' => $this->session->userdata('email'),
            'token' => $this->token,
            'pasar_harga_komoditi_id' => $pasar_harga_komoditi_id,
            'jumlah'    => 1
        );

        $api = array(
            'endpoint'      => 'setHargaKomoditi',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        //var_dump($service);die();

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getAnggaran()
    {
        $code = 200;
        $data = array();
        $pagenumber = 0;
        if (!empty($this->input->post('length'))) {
            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            if ($this->input->post('action_index') != '') {
                echo json_encode($this->session->userdata('detail_anggaran')[$pagenumber]['data'][$this->input->post('action_index')]);
                exit();
            }
        }

        if (isset($this->session->userdata('detail_anggaran')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_anggaran')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getAnggaranPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token
                )
            );

            if (!empty($this->input->post('program_pasar_id'))) {
                $api['post_field']['program_pasar_id'] = $this->input->post('program_pasar_id');
            }

            if (!empty($this->input->post('jenis_anggaran'))) {
                $api['post_field']['jenis_anggaran'] = $this->input->post('jenis_anggaran');
            }

            if (!empty($this->input->post('length'))) {
                $api['post_field']['search'] = $this->input->post('search')['value'];
                $api['post_field']['limit'] = $this->input->post('length');
                $api['post_field']['offset'] = (empty($this->input->post('start')) ? 0 : $this->input->post('start'));
            }

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_anggaran'][$pagenumber] = $response;

                foreach ($session['detail_anggaran'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_anggaran'][$pagenumber]['data'][$key]['omset'] = rupiah($row['omset']);
                    $session['detail_anggaran'][$pagenumber]['data'][$key]['anggaran'] = rupiah($row['anggaran']);

                    $session['detail_anggaran'][$pagenumber]['data'][$key]['action'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') {
                        $session['detail_anggaran'][$pagenumber]['data'][$key]['action'] = true;
                    }
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_anggaran')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function save_anggaran()
    {
        $data_post = $this->input->post();

        $pasar_anggaran_id = (!empty($data_post['pasar_anggaran_id'])) ? $data_post['pasar_anggaran_id'] : '';

        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'nama_pasar'  => $data_post['nama_pasar'],
            'tahun'   => $data_post['tahun_anggaran'],
            'omset'    => str_replace('.', '', $data_post['omset_anggaran']),
            'email' => $this->session->userdata('email'),
            'jenis_anggaran' => $data_post['jenis_anggaran'],
        );

        if ($data_post['jenis_anggaran'] == 'pasar') {
            $post_field['anggaran'] = str_replace('.', '', $data_post['jumlah_anggaran']);
            $post_field['program_pasar_id'] = $data_post['program_pasar'];
            $post_field['pasar_anggaran_id'] = $pasar_anggaran_id;
        } else {
            $post_field['bulan'] = $data_post['bulan_anggaran'];
            $post_field['pasar_anggaran_pedagang_id'] = $pasar_anggaran_id;
        }

        $api = array(
            'endpoint'      => 'setAnggaranPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getBongkar_muat()
    {

        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_bongkar_muat')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_bongkar_muat')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_bongkar_muat')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getBongkar_muat',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'pasar_bongkar_muat_id' => $this->input->post('pasar_bongkar_muat_id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_bongkar_muat'][$pagenumber] = $response;

                foreach ($session['detail_bongkar_muat'][$pagenumber]['data'] as $key => $row) {
                    $session['detail_bongkar_muat'][$pagenumber]['data'][$key]['omset'] = rupiah($row['omset']);
                    $session['detail_bongkar_muat'][$pagenumber]['data'][$key]['anggaran'] = rupiah($row['anggaran']);

                    $session['detail_bongkar_muat'][$pagenumber]['data'][$key]['action'] = false;

                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') {
                        $session['detail_bongkar_muat'][$pagenumber]['data'][$key]['action'] = true;
                    }
                }

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_bongkar_muat')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function saveBongkar_muat()
    {
        $data_post = $this->input->post();
        $pasar_bongkar_muat_id = (!empty($data_post['pasar_bongkar_muat_id'])) ? $data_post['pasar_bongkar_muat_id'] : '';

        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'nama_pasar'  => $data_post['nama_pasar'],
            'bulan'   => $data_post['bulan'],
            'tahun'   => $data_post['tahun'],
            'jumlah'    => str_replace('.', '', $data_post['jumlah']),
            'email' => $this->session->userdata('email'),
            'pasar_bongkar_muat_id' => $pasar_bongkar_muat_id
        );

        $api = array(
            'endpoint'      => 'setBongkar_muat',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function savePaguyuban()
    {
        $data_post = $this->input->post();
        $pasar_paguyuban_id = (!empty($data_post['pasar_paguyuban_id'])) ? $data_post['pasar_paguyuban_id'] : '';

        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'nama_paguyuban'  => $data_post['nama_paguyuban'],
            'nama_ketua'   => $data_post['nama_ketua'],
            'asosiasi_perdagangan_pasar'   => $data_post['asosiasi_perdagangan_pasar'],
            'no_telp_ketua'   => $data_post['no_telp_ketua'],
            'email' => $this->session->userdata('email'),
            'pasar_paguyuban_id' => $pasar_paguyuban_id
        );

        $api = array(
            'endpoint'      => 'setPasar_paguyuban',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getRegulasi_pasar()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_regulasi')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_regulasi')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getRegulasi',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_regulasi'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_regulasi')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function saveRegulasi()
    {
        $data_post = $this->input->post();
        $pasar_regulasi_id = (!empty($data_post['pasar_regulasi_id'])) ? $data_post['pasar_regulasi_id'] : '';

        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'no_regulasi'  => $data_post['no_regulasi'],
            'nama_regulasi'   => $data_post['nama_regulasi'],
            'pengesah_regulasi'   => $data_post['pengesah_regulasi'],
            'tgl_regulasi'   => $data_post['tgl_regulasi'],
            'email' => $this->session->userdata('email'),
            'pasar_regulasi_id' => $pasar_regulasi_id
        );

        if ($_FILES['file_regulasi']['tmp_name'] != '') {
            $post_field['file_regulasi'] = new CurlFile($_FILES['file_regulasi']['tmp_name'], $_FILES['file_regulasi']['type'], $_FILES['file_regulasi']['name']);
            // var_dump($post_field['file_regulasi']);die();
        }

        $api = array(
            'endpoint'      => 'setRegulasi',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function getPasokanBapok_pasar()
    {
        $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

        if ($this->input->post('action_index') != '') {
            echo json_encode($this->session->userdata('detail_pasokan_bapok')[$pagenumber]['data'][$this->input->post('action_index')]);
            exit();
        }

        if (isset($this->session->userdata('detail_pasokan_bapok')[$pagenumber]) && empty($this->input->post('refresh'))) {
            $session_existing = $this->session->userdata('detail_pasokan_bapok')[$pagenumber];
            $session_existing['draw'] = $this->input->post('draw');
            echo json_encode($session_existing);
        } else {
            $api = array(
                'endpoint'      => 'getPasokan_bapok',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->input->post('id'),
                    'token' => $this->token,
                    'search' => $this->input->post('search')['value'],
                    'limit' => $this->input->post('length'),
                    'offset' => (empty($this->input->post('start')) ? 0 : $this->input->post('start'))
                )
            );

            $response = $this->serviceAPI($api);

            $response['draw'] = $this->input->post('draw');

            if ($response && $response['kode'] == 200) {
                $session = $this->session->all_userdata();
                $session['detail_pasokan_bapok'][$pagenumber] = $response;

                $this->session->set_userdata($session);
                echo json_encode($this->session->userdata('detail_pasokan_bapok')[$pagenumber]);
            } else {
                $response = array(
                    'draw' => 1,
                    'recordsTotal' => 0,
                    'recordsFiltered' => 0,
                    'data'  => []
                );

                echo json_encode($response);
            }
        }
    }

    public function savePasokan_bapok()
    {
        $data_post = $this->input->post();
        $pasar_pasokan_bapok_id = (!empty($data_post['pasar_pasokan_bapok_id'])) ? $data_post['pasar_pasokan_bapok_id'] : '';
        $split_jenis_komoditi = explode('|', $data_post['jenis_komoditi']);
        $post_field = array(
            'token' => $this->token,
            'pasar_id'    => $data_post['pasar_id'],
            'bulan'  => $data_post['bulan'],
            'tahun'   => $data_post['tahun'],
            'jenis_komoditi_id'   => $split_jenis_komoditi[0],
            'varian_komoditi_id'   => $data_post['varian_komoditi_id'],
            'jumlah'   => $data_post['jumlah'],
            'satuan_id'   => $data_post['satuan_komoditi_id'],
            'email' => $this->session->userdata('email'),
            'pasar_pasokan_bapok_id' => $pasar_pasokan_bapok_id
        );

        $api = array(
            'endpoint'      => 'setPasar_pasokan_bapok',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $service = $this->serviceAPI($api);

        if ($service['kode'] == 200) {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', 'Data Berhasil Disimpan');
        } else {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', 'Data gagal disimpan');
        }

        redirect(base_url() . 'web/pasar/detail/' . $this->jwtEncode($data_post['pasar_id']));
    }

    public function sebaran()
    {
        $this->data['title']  = 'Sebaran Pasar';
        $this->data['menu'] = 'sebaran';
        $this->data['js']     = 'sebaran_js';

        $kab_id = 1;

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
        } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
        } else {
            $kab_id = 0;
        }

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('daerah_id' => $kab_id, 'token' => $this->token, 'limit' => 20000, 'offset' => 0)
        );

        if (!empty($this->input->post('pengelola'))) {
            $api['post_field']['email'] = $this->input->post('pengelola');
            $this->session->set_flashdata('filter_search_pengelola', $this->input->post('pengelola'));
        }

        if (!empty($this->input->post('kondisi'))) {
            $kondisi = '';
            if ($this->input->post('kondisi') == 1) {
                $kondisi = 'Baik';
            } else if ($this->input->post('kondisi') == 2) {
                $kondisi = 'Rusak Berat';
            } else if ($this->input->post('kondisi') == 3) {
                $kondisi = 'Rusak Ringan';
            }

            $api['post_field']['kondisi'] = $kondisi;
            $this->session->set_flashdata('filter_search_kondisi', $this->input->post('kondisi'));
        }

        if (!empty($this->input->post('kepemilikan'))) {
            $kepemilikan = '';
            if ($this->input->post('kepemilikan') == 1) {
                $kepemilikan = 'Pemerintah Daerah';
            } else if ($this->input->post('kepemilikan') == 2) {
                $kepemilikan = 'Desa Adat';
            } else if ($this->input->post('kepemilikan') == 3) {
                $kepemilikan = 'Swasta';
            }

            $api['post_field']['kepemilikan'] = $kepemilikan;
            $this->session->set_flashdata('filter_search_kepemilikan', $this->input->post('kepemilikan'));
        }

        if (!empty($this->input->post('bentuk'))) {
            $bentuk = '';
            if ($this->input->post('bentuk') == 1) {
                $bentuk = 'Permanen';
            } else if ($this->input->post('bentuk') == 2) {
                $bentuk = 'Semi Permanen';
            } else if ($this->input->post('bentuk') == 3) {
                $bentuk = 'Tanpa Bangunan';
            }

            $api['post_field']['bentuk_pasar'] = $bentuk;

            $this->session->set_flashdata('filter_search_bentuk', $this->input->post('bentuk'));
        }

        if (!empty($this->input->post('provinsi'))) {
            $api['post_field']['daerah_id'] = $this->input->post('provinsi');

            $this->session->set_flashdata('filter_search_provinsi', $this->input->post('provinsi'));
        }

        if (!empty($this->input->post('kabupaten'))) {
            $api['post_field']['daerah_id'] = $this->input->post('kabupaten');
            $this->session->set_flashdata('filter_search_kabupaten', $this->input->post('kabupaten'));
        }

        if (!empty($this->input->post('tipe_pasar'))) {
            $api['post_field']['tipe_pasar_id'] = $this->input->post('tipe_pasar');
            $this->session->set_flashdata('filter_search_tipe', $this->input->post('tipe_pasar'));
        }

        $response_pasar = $this->serviceAPI($api);

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->session->set_userdata('maps_pasar', $response_pasar['data']);
        } else {
            $this->session->set_userdata('maps_pasar', null);
        }

        if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)')) {
            $this->data['kab'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('prov_latlong');
        } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {

            if (strpos(strtolower($this->session->userdata('kab_kota')), 'kab') !== false) {
                $kab = str_replace(array('Kab. ', 'Kab. ', 'Kab ', 'kab '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kabupaten ' . $kab;
            } else if (strpos(strtolower($this->session->userdata('kab_kota')), 'kota') !== false) {
                $kab = str_replace(array('Kota. ', 'Kota. ', 'Kota ', 'kota '), '', $this->session->userdata('kab_kota'));

                $this->data['kab'] = 'Kota ' . $kab;
            }

            $this->data['provinsi'] = 'Provinsi ' . $this->session->userdata('provinsi');
            $this->data['latlong_map'] = $this->session->userdata('kab_latlong');
        } else {
            $this->data['latlong_map'] = '-0.789275, 113.921327';
        }

        if (!$this->session->userdata('master_provinsi')) {
            $api = array(
                'endpoint'      => 'getMasterDaerah',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => 1)
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else {
                $this->session->set_userdata('master_provinsi', $response['data']);
            }
        }

        if (!$this->session->userdata('tipe_pasar')) {
            $api = array(
                'endpoint'      => 'getTipePasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
            );

            $response = $this->serviceAPI($api);

            if (!$response) {
                redirect('user/sign_out');
            } else if ($response && $response['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response['data']);
            }
        }

        $this->data['maps_pasar'] = $this->session->userdata('maps_pasar');

        $this->templatebackend('pasar/sebaran', $this->data);
    }

    public function detail($id)
    {
        $this->data['menu'] = 'pasar';
        $this->data['js']     = 'pasar_detail_js';
        $id = $this->jwtDecode($id);

        $uri_segment = 0;
        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array('pasar_id' => $id, 'limit' => 1, 'offset' => 0, 'token' => $this->token)
        );

        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {
            $this->data['title']  = $response['data'][0]['nama'];

            if (strtolower($this->uri->segment(1)) == 'sebaran-pasar') {
                $uri_segment = 2;
            } else {
                $uri_segment = 1;
            }


            $this->session->set_userdata('index_pasar', $response['data'][0]);

            $api = array(
                'endpoint'      => 'getDetailPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->session->userdata('index_pasar')['pasar_id'], 'token' => $this->token)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('detail_pasar', $response_pasar['data']);
            } else if ($response_pasar && $response_pasar['kode'] == 404) {
                $this->session->set_userdata('detail_pasar', null);
            }

            $api = array(
                'endpoint'      => 'getJenisKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
            );

            $response_jenis = $this->serviceAPI($api);

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('jenis_komoditi', $response_jenis['data']);
            }

            $api = array(
                'endpoint'      => 'getTipePasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array()
            );

            $response_tipe = $this->serviceAPI($api);

            if (!$response_tipe) {
                redirect('user/sign_out');
            } else if ($response_tipe && $response_tipe['kode'] == 200) {
                $this->session->set_userdata('tipe_pasar', $response_tipe['data']);
            }

            $api = array(
                'endpoint'      => 'getProgramPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array()
            );

            $response_program_pasar = $this->serviceAPI($api);

            if (!$response_program_pasar) {
                redirect('user/sign_out');
            } else if ($response_program_pasar && $response_program_pasar['kode'] == 200) {
                $this->session->set_userdata('program_pasar', $response_program_pasar['data']);
            }

            $api = array(
                'endpoint'      => 'getBangunanPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $this->session->userdata('index_pasar')['pasar_id'], 'token' => $this->token)
            );

            $response_bangunan = $this->serviceAPI($api);

            if (!$response_bangunan) {
                redirect('user/sign_out');
            } else if ($response_bangunan && $response_bangunan['kode'] == 200) {
                $this->session->set_userdata('bangunan_pasar', $response_bangunan['data']);
            }

            $api = array(
                'endpoint'      => 'getPasar_paguyupan',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'pasar_id' => $this->session->userdata('index_pasar')['pasar_id'],
                    'limit' => 1,
                    'offset' => 0,
                    'token' => $this->token
                )
            );
            $response_paguyuban = $this->serviceAPI($api);
            $this->data['paguyuban_pasar'] = $response_paguyuban;

            if (!$response_jenis) {
                redirect('user/sign_out');
            } else if ($response_jenis && $response_jenis['kode'] == 200) {
                $this->session->set_userdata('jenis_komoditi', $response_jenis['data']);
            }

            $this->data['index_pasar'] = $this->session->userdata('index_pasar');
            $this->data['detail_pasar'] = $this->session->userdata('detail_pasar');
            $this->data['jenis_komoditi'] = $this->session->userdata('jenis_komoditi');
            $this->data['tipe_pasar'] = $this->session->userdata('tipe_pasar');
            $this->data['program_pasar'] = $this->session->userdata('program_pasar');
            $this->data['bangunan_pasar'] = $this->session->userdata('bangunan_pasar');
            $this->data['pengelola_pasar'] = $this->getPengelola($this->session->userdata('index_pasar')['pasar_id']);
            $this->data['active_index'] = $id;
            $this->data['maps_pasar'] = $this->session->userdata('maps_pasar');
            $this->data['uri_segment'] = $uri_segment;

            $this->templatebackend('pasar/detail', $this->data);
        }
    }

    public function getPasarJSON()
    {
        $response = array('data' => null, 'code' => 200, 'success' => false);

        if ($this->input->post('action_index') != null) {
            $response['data'] = $this->session->userdata('index_pasar');
            $response['action_index'] = $this->input->post('action_index');
            $response['success'] = true;

            echo json_encode($response);
            exit();
        }

        $exp = explode('|', $this->input->post('provinsi_id'));
        $id = $exp[0];

        if ($id != 1) {
            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array(
                    'daerah_id' => $id,
                    'token' => $this->token,
                    'limit' =>  600,
                    'offset' => 0
                )
            );

            $response_pasar = $this->serviceAPI($api);

            $temp = array();

            if (!$response_pasar) {
                $response['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $temp[$key]['pasar_id'] = $row['pasar_id'];
                    $temp[$key]['nama'] = $row['nama'];
                }

                $response['data'] = $temp;
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getPengelolaJSON()
    {
        $response = array('data' => null, 'code' => 200, 'success' => false);

        $prov_id = $this->input->post('provinsi_id');

        if ($prov_id != null) {
            $api = array(
                'endpoint'      => 'getPengguna',
                'method'        => 'POST',
                'controller'    => '',
                'post_field'    => array('daerah_id' => $prov_id, 'token' => $this->token, 'limit' =>  600, 'offset' => 0, 'tipe_pengguna' => 'Pengelola/Petugas')
            );

            $response_pasar = $this->serviceAPI($api);

            $temp = array();

            if (!$response_pasar) {
                $response['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $temp[$key]['email'] = $row['email'];
                    $temp[$key]['nama_lengkap'] = $row['nama_lengkap'];
                }

                $response['data'] = $temp;
                $response['success'] = true;
            }
        }

        echo json_encode($response);
    }

    public function getDetailJSON()
    {
        $this->data['data'] = array();

        $index = $this->input->post('id');

        if ($index != null) {

            $api = array(
                'endpoint'      => 'getDetailPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('pasar_id' => $index, 'token' => $this->token)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                $this->data['code'] = 401;
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                $this->session->set_userdata('detail_pasar', $response_pasar['data']);
            } else if ($response_pasar && $response_pasar['kode'] == 404) {
                $this->session->set_userdata('detail_pasar', null);
            }

            if ($this->session->userdata('detail_pasar') != null) {
                $this->data['data'] = $this->session->userdata('detail_pasar')[0];

                $this->data['data']['jam_operasional_awal'] = ($this->data['data']['jam_operasional_awal'] != null) ? date('h:i A', strtotime($this->data['data']['jam_operasional_awal'])) : null;
                $this->data['data']['jam_operasional_akhir'] = ($this->data['data']['jam_operasional_akhir'] != null) ? date('h:i A', strtotime($this->data['data']['jam_operasional_akhir'])) : null;
                $this->data['data']['jam_sibuk_awal'] = ($this->data['data']['jam_sibuk_awal'] != null) ? date('h:i A', strtotime($this->data['data']['jam_sibuk_awal'])) : null;
                $this->data['data']['jam_sibuk_akhir'] = ($this->data['data']['jam_sibuk_akhir'] != null) ? date('h:i A', strtotime($this->data['data']['jam_sibuk_akhir'])) : null;
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);
                $this->data['data']['jumlah_pekerja_nontetap'] = rupiah($this->data['data']['jumlah_pekerja_nontetap']);
                $this->data['data']['luas_bangunan'] = rupiah($this->data['data']['luas_bangunan']);
                $this->data['data']['luas_tanah'] = rupiah($this->data['data']['luas_tanah']);
                $this->data['data']['jumlah_lantai'] = rupiah($this->data['data']['jumlah_lantai']);
                $this->data['data']['jumlah_pekerja_tetap'] = rupiah($this->data['data']['jumlah_pekerja_tetap']);

                // $split_operasional = explode(',', $this->data['data']['waktu_operasional']);

                // $waktu_operasional_array = array();

                // foreach ($split_operasional as $key => $value) {
                //     if (strtolower($value) == 'senin') {
                //         $waktu_operasional_array[] = 1;
                //     } else if (strtolower($value) == 'selasa') {
                //         $waktu_operasional_array[] = 2;
                //     } else if (strtolower($value) == 'rabu') {
                //         $waktu_operasional_array[] = 3;
                //     } else if (strtolower($value) == 'kamis') {
                //         $waktu_operasional_array[] = 4;
                //     } else if (strtolower($value) == 'jumat' || strtolower($value) == "jum'at") {
                //         $waktu_operasional_array[] = 5;
                //     } else if (strtolower($value) == 'sabtu') {
                //         $waktu_operasional_array[] = 6;
                //     } else if (strtolower($value) == 'minggu') {
                //         $waktu_operasional_array[] = 7;
                //     }
                // }

                // $this->data['data']['waktu_operasional'] = $waktu_operasional_array;
            }
        }

        echo json_encode($this->data);
    }

    public function getBangunan()
    {
        $api = array(
            'endpoint'      => 'getBangunanPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array(
                'pasar_id' => $this->input->post('id'),
                'token' => $this->token
            )
        );

        $response = $this->serviceAPI($api);

        echo json_encode($response);
    }

    function getPengelola_pasar()
    {
        $api = array(
            'endpoint'      => 'getPasar_pengelola',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array(
                'pasar_id' => $this->input->post('pasar_id'),
                'token' => $this->token,
                'offset' => 0,
                'limit' => 10
            )
        );

        $response = $this->serviceAPI($api);

        echo json_encode($response);
    }

    public function getVarianKomoditi()
    {
        $response = array();

        if ($this->input->post('jenis_komoditi_id') != null) {
            $split_jenis_komoditi = explode('|', $this->input->post('jenis_komoditi_id'));

            $api = array(
                'endpoint'      => 'getVarianKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $split_jenis_komoditi[0])
            );

            $response_varian = $this->serviceAPI($api);

            if (!$response_varian) {
                $response['success'] = false;
                $response['code'] = 401;
            } else if ($response_varian && $response_varian['kode'] == 200) {
                $this->session->set_userdata('varian_komoditi', $response_varian['data']);
                $response['data'] = $this->session->userdata('varian_komoditi');
                $response['success'] = true;
            }
        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function getSatuanKomoditi()
    {
        $response = array();

        if ($this->input->post('jenis_komoditi_id') != null) {
            $split_jenis_komoditi = explode('|', $this->input->post('jenis_komoditi_id'));

            $api = array(
                'endpoint'      => 'getSatuanKomoditi',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => array('jenis_komoditi_id' => $split_jenis_komoditi[0])
            );

            $response_satuan = $this->serviceAPI($api);

            if (!$response_satuan) {
                $response['success'] = false;
                $response['code'] = 401;
            } else if ($response_satuan && $response_satuan['kode'] == 200) {
                $this->session->set_userdata('satuan_komoditi', $response_satuan['data']);
                $response['data'] = $this->session->userdata('satuan_komoditi');
                $response['success'] = true;
            }
        } else {
            $response['success'] = false;
        }

        echo json_encode($response);
    }

    public function saveDetail()
    {
        $this->data['response'] = array();
        //var_dump(implode(',', $this->input->post('sarana_prasarana')));die();
        $this->form_validation->set_rules('phone', 'Nomor Telephone', 'trim|numeric');
        $this->form_validation->set_rules('fax', 'Fax', 'trim|numeric');
        $this->form_validation->set_rules('tipe_pasar', 'Tipe Pasar', 'trim');
        $this->form_validation->set_rules('klasifikasi', 'Klasifikasi', 'trim|required');
        $this->form_validation->set_rules('bentuk_pasar', 'Bentuk Pasar', 'trim|required');
        $this->form_validation->set_rules('kepemilikan', 'Kepemilikan', 'trim|required');
        $this->form_validation->set_rules('kondisi', 'Kondisi', 'trim|required');
        // $this->form_validation->set_rules('pengelola', 'Pengelola Pasar', 'trim|required');
        $this->form_validation->set_rules('waktu_operasional[]', 'Waktu Operasional', 'trim');
        $this->form_validation->set_rules('jam_buka', 'Jam Buka', 'trim');
        $this->form_validation->set_rules('jam_tutup', 'Jam Tutup', 'trim');
        $this->form_validation->set_rules('jam_sibuk_awal', 'Jam Mulai Sibuk', 'trim');
        $this->form_validation->set_rules('jam_sibuk_akhir', 'Jam Berakhir Sibuk', 'trim');
        $this->form_validation->set_rules('pekerja_tetap', 'Pekerja Tetap', 'trim|required');
        $this->form_validation->set_rules('pekerja_nontetap', 'Pekerja Non Tetap', 'trim|required');
        $this->form_validation->set_rules('luas_bangunan', 'Luas Bangunan', 'trim|required');
        $this->form_validation->set_rules('luas_tanah', 'Luas Tanah', 'trim|required');
        $this->form_validation->set_rules('jumlah_lantai', 'Jumlah Lantai', 'trim|required|integer');
        $this->form_validation->set_rules('tahun_bangun', 'Tahun Dibangun', 'trim|required|integer|exact_length[4]');
        $this->form_validation->set_rules('tahun_renovasi', 'Tahun Renovasi', 'trim|required|integer|exact_length[4]');
        $this->form_validation->set_rules('jarak_toko_modern', 'Jarak dengan Toko Modern', 'trim');
        $this->form_validation->set_rules('penerapan_digitalisasi_pasar', 'Penerapan Digitalisasi Pasar', 'trim');
        $this->form_validation->set_rules('pembiayaan_lembaga_keuangan', 'Pembiayaan Lembaga Keuangan', 'trim');
        $this->form_validation->set_rules('sertifikasi_sni', 'Sertifikasi SNI Pasar Rakyat', 'trim');
        $this->form_validation->set_rules('sarana_prasarana[]', 'Sarana & Prasarana', 'trim');
        $this->form_validation->set_rules('angkutan_umum', 'Dilalui Angkutan Umum', 'trim');
        $this->form_validation->set_rules('dekat_pemukiman', 'Dekat dengan pemukiman (< 2km)', 'trim');
        $this->form_validation->set_rules('jumlah_pengunjung', 'Jumlah Pengunjung Harian', 'trim');
        $this->form_validation->set_rules('komoditas[]', 'Komoditas yang dijual', 'trim|required');
        $this->form_validation->set_rules('omzet', 'Omzet Pasar Tahun Sebelumnya', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $post_field = array(
                'token' => $this->token,
                'nama_pasar'    => $this->input->post('pasar_nama'),
                'pasar_id'  => $this->input->post('pasar_id'),
                'no_telp'   => $this->input->post('phone'),
                'no_fax'    => $this->input->post('fax'),
                'jenis_pasar'   => $this->input->post('klasifikasi'),
                'tipe_pasar_id' => $this->input->post('tipe_pasar'),
                'bentuk_pasar'  => $this->input->post('bentuk_pasar'),
                // 'kepemilikan'   => $kepemilikan,
                'kondisi'   => $this->input->post('kondisi'),
                // 'pengelola_pasar'   => $pengelola,
                'waktu_operasional' => implode(',', $this->input->post('waktu_operasional')),
                'jam_operasional_awal'  => ($this->input->post('jam_buka') != '') ? date("H:i:s", strtotime($this->input->post('jam_buka'))) : null,
                'jam_operasional_akhir' => ($this->input->post('jam_tutup') != '') ? date("H:i:s", strtotime($this->input->post('jam_tutup'))) : null,
                'jam_sibuk_awal'    => ($this->input->post('jam_sibuk_awal') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_awal'))) : null,
                'jam_sibuk_akhir'  => ($this->input->post('jam_sibuk_akhir') != '') ? date("H:i:s", strtotime($this->input->post('jam_sibuk_akhir'))) : null,
                'jumlah_pekerja_tetap'  => str_replace('.', '', $this->input->post('pekerja_tetap')),
                'jumlah_pekerja_nontetap'   => str_replace('.', '', $this->input->post('pekerja_nontetap')),
                'luas_bangunan' => str_replace('.', '', $this->input->post('luas_bangunan')),
                'luas_tanah'    => str_replace('.', '', $this->input->post('luas_tanah')),
                'jumlah_lantai' => str_replace('.', '', $this->input->post('jumlah_lantai')),
                'tahun_bangun'  => $this->input->post('tahun_bangun'),
                'tahun_renovasi' => $this->input->post('tahun_renovasi'),
                'jumlah_pengunjung' => str_replace('.', '', $this->input->post('jumlah_pengunjung')),
                'jarak_toko_modern' => str_replace('.', '', $this->input->post('jarak_toko_modern')),
                'penerapan_digitalisasi_pasar' => $this->input->post('penerapan_digitalisasi_pasar'),
                'metode_penerapan_digital_pasar' => is_array($this->input->post('metode_penerapan_digital_pasar')) ? implode(',', $this->input->post('metode_penerapan_digital_pasar')) : $this->input->post('metode_penerapan_digital_pasar'),
                'pembiayaan_lk' => $this->input->post('pembiayaan_lembaga_keuangan'),
                'lembaga_keuangan' => empty($this->input->post('lembaga_keuangan')) ? null : $this->input->post('lembaga_keuangan'),
                'sertifikasi_sni' => $this->input->post('sertifikasi_sni'),
                'tahun_sni_pasar_rakyat' => empty($this->input->post('tahun_sni_pasar_rakyat')) ? null : $this->input->post('tahun_sni_pasar_rakyat'),
                'sarana_prasarana' => implode(',', $this->input->post('sarana_prasarana')),
                'angkutan_umum' => $this->input->post('angkutan_umum'),
                'jarak_pemukiman' => $this->input->post('dekat_pemukiman'),
                'email' => $this->session->userdata('email')
            );

            if (!empty($this->input->post('pasar_detail'))) {
                $post_field['pasar_detail_id'] = $this->input->post('pasar_detail');
            }

            $api = array(
                'endpoint'      => 'setDetailPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function saveKios()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('nama', 'Nama Pasar', 'trim|required');
        $this->form_validation->set_rules('jumlah_bangunan', 'Jumlah Bangunan', 'trim|required|numeric');
        $this->form_validation->set_rules('jumlah_pedagang', 'Jumlah Pedagang', 'trim|required|numeric');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {
            if ($this->input->post('jenis_bangunan') == '1') {
                $jenis_bangunan = 'Los';
            } else if ($this->input->post('jenis_bangunan') == '2') {
                $jenis_bangunan = 'Kios';
            } else if ($this->input->post('jenis_bangunan') == '3') {
                $jenis_bangunan = 'Dasaran';
            }

            $pasar_bangunan_id = (!empty($this->input->post('pasar_bangunan_id'))) ? $this->input->post('pasar_bangunan_id') : '';

            $temp_kios = array();
            $temp_bangunan_id = array();

            if ($this->session->userdata('data_kios')) {
                $session_kios = $this->session->userdata('data_kios')[1]['data'];

                foreach ($session_kios as $kios) {
                    $temp_kios[] = $kios['jenis'];
                    $temp_bangunan_id[] = $kios['pasar_bangunan_id'];
                }
            }

            $this->data['response']['available'] = false;

            if (in_array($jenis_bangunan, $temp_kios)) {
                $key = array_search($jenis_bangunan, $temp_kios);
                $pasar_bangunan_id = $temp_bangunan_id[$key];
            }

            $post_field = array(
                'token' => $this->token,
                'jenis'    => $jenis_bangunan,
                'jumlah'  => str_replace('.', '', $this->input->post('jumlah_bangunan', true)),
                'jumlah_pedagang'   => str_replace('.', '', $this->input->post('jumlah_pedagang', true)),
                'nama_pasar'    => $this->input->post('nama'),
                'pasar_id'   => $this->input->post('pasar_id'),
                'email' => $this->session->userdata('email'),
                'pasar_bangunan_id' => $pasar_bangunan_id
            );

            $api = array(
                'endpoint'      => 'setBangunanPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function saveAnggaran()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('tahun_anggaran', 'Tahun', 'trim|required|numeric|exact_length[4]');
        $this->form_validation->set_rules('program_pasar', 'Program Pasar', 'trim|required');
        $this->form_validation->set_rules('omset_anggaran', 'Omset', 'trim|required');
        $this->form_validation->set_rules('jumlah_anggaran', 'Jumlah Anggaran', 'trim|required');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');


        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {

            $pasar_anggaran_id = (!empty($this->input->post('pasar_anggaran_id'))) ? $this->input->post('pasar_anggaran_id') : '';

            $post_field = array(
                'token' => $this->token,
                'pasar_id'    => $this->input->post('pasar_id'),
                'nama_pasar'  => $this->input->post('nama', TRUE),
                'tahun'   => $this->input->post('tahun_anggaran', TRUE),
                'omset'    => str_replace('.', '', $this->input->post('omset_anggaran', TRUE)),
                'anggaran'   => str_replace('.', '', $this->input->post('jumlah_anggaran', TRUE)),
                'program_pasar_id'  => $this->input->post('program_pasar', TRUE),
                'email' => $this->session->userdata('email'),
                'pasar_anggaran_id' => $pasar_anggaran_id
            );

            $api = array(
                'endpoint'      => 'setAnggaranPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = true;
            }
        }


        echo json_encode($this->data['response']);
    }

    public function save()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('name', 'Nama', 'trim|required');
        $this->form_validation->set_rules('description', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('address', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim|required|numeric');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim|required|numeric');
        $this->form_validation->set_rules('post_code', 'Kode Pos', 'trim|required|integer');
        $this->form_validation->set_rules('post_code', 'Kode Pos', 'trim|required|integer');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (empty($this->input->post('pasar_id'))) {
            $this->form_validation->set_rules('file1', 'Foto Depan', 'callback_validate_image1');
            $this->form_validation->set_rules('file2', 'Foto Dalam', 'callback_validate_image2');
        }

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            foreach ($_FILES as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }
            $this->data['response']['success'] = false;
        } else {
            $post_field = array(
                'token' => $this->token,
                'foto_depan'    => new CURLFILE($_FILES['file1']['tmp_name'], $_FILES['file1']['type'], $_FILES['file1']['name']),
                'email' => $this->session->userdata('email', true),
                'nama'  => $this->input->post('name', true),
                'deskripsi'  => $this->input->post('description'),
                'alamat'  => $this->input->post('address'),
                'daerah_id'  => $this->session->userdata('daerah_id'),
                'longitude'  => $this->input->post('longitude', true),
                'latitude'  => $this->input->post('latitude', true),
                'foto_dalam'    => new CURLFILE($_FILES['file2']['tmp_name'], $_FILES['file2']['type'], $_FILES['file2']['name']),
                'kode_pos'  => $this->input->post('post_code'),
            );

            $api = array(
                'endpoint'      => 'setPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['success'] = true;
            }
        }

        echo json_encode($this->data['response']);
    }

    public function update()
    {
        $this->data['response'] = array();

        $this->form_validation->set_rules('name_update', 'Nama', 'trim|required');
        $this->form_validation->set_rules('description_update', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('address_update', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('post_code_update', 'Kode Pos', 'trim|required|integer');
        $this->form_validation->set_error_delimiters('<small class="text-danger">', '</small>');

        if (!$this->form_validation->run()) {
            foreach ($this->input->post() as $key => $value) {
                $this->data['response']['validation'][$key] = form_error($key);
            }

            $this->data['response']['success'] = false;
        } else {
            $post_field = array(
                'token' => $this->token,
                'pasar_id'  => $this->input->post('pasar_id_updatee'),
                'email' => $this->session->userdata('email', true),
                'nama'  => $this->input->post('name_update', true),
                'deskripsi'  => $this->input->post('description_update'),
                'alamat'  => $this->input->post('address_update'),
                'latitude'  => $this->input->post('latitude_update'),
                'longitude'  => $this->input->post('longitude_update'),
                'daerah_id'  => $this->input->post('daerah_id_update'),
                'kode_pos'  => $this->input->post('post_code_update'),
            );

            $api = array(
                'endpoint'      => 'setPasar',
                'controller'    => 'pasar',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {

                $api = array(
                    'endpoint'      => 'getPasar',
                    'controller'    => 'pasar',
                    'method'        => 'POST',
                    'post_field'    => array('token' => $this->token, 'limit' => 1, 'offset' => 0, 'status' => 'Aktif', 'pasar_id' => $this->input->post('pasar_id_updatee'))
                );

                $service = $this->serviceAPI($api);

                if (!$service) {
                    $this->data['response']['code'] = 401;
                    $this->data['response']['success'] = false;
                } else if ($service && $service['kode'] == 200) {
                    if ($this->input->post('uri_type') == 1) {
                        $sess_pasar_active = $this->session->userdata('data_pasar')[$this->session->userdata('page_pasar')]['data'];

                        $temp[$this->session->userdata('page_pasar')]['data'] = array();

                        foreach ($sess_pasar_active as $key => $row) {
                            if ($key == $this->input->post('pasar_update_index')) {
                                $temp[$this->session->userdata('page_pasar')]['data'][$key] = $service['data'][0];

                                $temp[$this->session->userdata('page_pasar')]['data'][$key]['action'] = $row['action'];
                            } else {
                                $temp[$this->session->userdata('page_pasar')]['data'][$key] = $row;
                            }
                        }

                        $this->session->set_userdata('data_pasar', $temp);

                        $this->data['response']['success'] = true;
                        $this->data['response']['action_index'] = $this->input->post('pasar_update_index');
                        $this->data['response']['uri_segment'] = $this->input->post('uri_type');
                    } else if ($this->input->post('uri_type') == 2) {
                        // $this->session->set_userdata('maps_pasar');

                        $kab_id = 1;

                        if (strtolower($this->session->userdata('role')) == 'dinas provinsi') {
                            $kab_id = substr($this->session->userdata('daerah_id'), 0, 2);
                        } else if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') {
                            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);
                        } else {
                            $kab_id = 0;
                        }

                        $api = array(
                            'endpoint'      => 'getPasar',
                            'method'        => 'POST',
                            'controller'    => 'pasar',
                            'post_field'    => array('daerah_id' => $kab_id, 'token' => $this->token, 'limit' => 20000, 'offset' => 0)
                        );

                        $response_pasar = $this->serviceAPI($api);

                        $this->session->set_userdata('maps_pasar', $response_pasar['data']);

                        $this->data['response']['success'] = true;
                        $this->data['response']['action_index'] = $this->input->post('pasar_update_index');
                        $this->data['response']['uri_segment'] = $this->input->post('uri_type');
                    }
                }
            }
        }

        echo json_encode($this->data['response']);
    }


    function deletePasar($id)
    {
        $post_field = array(
            'token' => $this->token,
            'email' => $this->session->userdata('email'),
            'table_name'    => 'tbl_pasar',
            'key_name'  => 'pasar_id',
            'key'   => $id,
            'status'    => 'Deleted',
            'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
        );

        $api = array(
            'endpoint'      => 'setStatus',
            'controller'    => '',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $delete_pasar = $this->serviceAPI($api);

        if (!$delete_pasar) {
            redirect('user/sign_out');
        } else if ($delete_pasar && $delete_pasar['kode'] == 200) {
            $kab_id = substr($this->session->userdata('daerah_id'), 0, 4);

            $batas = 6;
            $halaman = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;


            $pagenumber = ($halaman_awal + $batas) / $batas;

            $api = array(
                'endpoint'      => 'getPasar',
                'method'        => 'POST',
                'controller'    => 'pasar',
                'post_field'    => array('daerah_id' => $kab_id, 'token' => $this->token, 'limit' => 6, 'offset' => 0)
            );

            $response_pasar = $this->serviceAPI($api);

            if (!$response_pasar) {
                redirect('user/sign_out');
            } else if ($response_pasar && $response_pasar['kode'] == 200) {
                foreach ($response_pasar['data'] as $key => $row) {
                    $response_pasar['data'][$key]['action'] = false;

                    if ($row['createdby'] == $this->session->userdata('email')) {
                        $response_pasar['data'][$key]['action'] = true;
                    }
                }

                $total_rows = $response_pasar['recordsFiltered'];

                $this->data['pagination'] = $this->mylib->pagination(site_url('dashboard'), $batas, $total_rows);

                $session['data_pasar'][$pagenumber] = $response_pasar;

                $this->session->set_userdata($session);
            } else {
                $this->session->set_userdata('data_pasar', null);
            }

            redirect(site_url());
        }
    }

    function deleteKios()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {

            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_bangunan',
                'key_name'  => 'pasar_bangunan_id',
                'key'   => $this->session->userdata('data_kios')[$pagenumber]['data'][$id]['pasar_bangunan_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }


        echo json_encode($this->data['response']);
    }

    function deleteAnggaran()
    {
        $id = $this->input->post('id', TRUE);

        if ($id != null) {
            $pagenumber = ($this->input->post('start') + $this->input->post('length')) / $this->input->post('length');

            $post_field = array(
                'token' => $this->token,
                'email' => $this->session->userdata('email'),
                'table_name'    => 'tbl_pasar_anggaran',
                'key_name'  => 'pasar_anggaran_id',
                'key'   => $this->session->userdata('detail_anggaran')[$pagenumber]['data'][$id]['pasar_anggaran_id'],
                'status'    => 'Deleted',
                'keterangan_perubahan'  => 'Set status Deleted atasnama ' . $this->session->userdata('nama')
            );

            $api = array(
                'endpoint'      => 'setStatus',
                'controller'    => '',
                'method'        => 'POST',
                'post_field'    => $post_field
            );

            $service = $this->serviceAPI($api);

            if (!$service) {
                $this->data['response']['code'] = 401;
                $this->data['response']['success'] = false;
            } else if ($service && $service['kode'] != 200) {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['message'] = $service['keterangan'];
                $this->data['response']['success'] = false;
            } else {
                $this->data['response']['code'] = $service['kode'];
                $this->data['response']['success'] = true;
            }
        } else {
            $this->data['response']['success'] = false;
        }


        echo json_encode($this->data['response']);
    }

    function lokasi()
    {
        if (!has_access(75, 'show')) {
            redirect(base_url() . '404_override');
        }

        $this->data['title']  = 'Manajement Lokasi Pasar';
        $this->data['js']     = 'lokasi_js';
        $this->data['css']     = 'lokasi_css';
        $this->data['menu'] = 'lokasi';
        $this->data['menu_id'] = 75;

        $api = array(
            'endpoint'      => 'getPasar',
            'method'        => 'POST',
            'controller'    => 'pasar',
            'post_field'    => array(
                'daerah_id' => 0,
                'token' => $this->token,
                'verify_coordinate' => 'Belum Terverifikasi',
                'limit' => 10000,
                'offset' => 0
            )
        );

        $response_pasar = $this->serviceAPI($api);

        $this->data['maps_pasar'] = null;

        if (!$response_pasar) {
            redirect('user/sign_out');
        } else if ($response_pasar && $response_pasar['kode'] == 200) {
            $this->data['maps_pasar'] = $response_pasar['data'];
        }

        $this->templatebackend('pasar/lokasi', $this->data);
    }

    function verifikasi()
    {
        $data_post = $this->input->post();

        $post_field = array(
            'token'             => $this->token,
            'pasar_id'          => $data_post['pasar_id'],
            'nama'              => $data_post['nama_pasar'],
            'email'             => $data_post['email'],
            'deskripsi'         => $data_post['deskripsi'],
            'alamat'            => $data_post['alamat'],
            'daerah_id'         => $data_post['daerah_id'],
            'latitude'          => $data_post['latitude'],
            'longitude'         => $data_post['longitude'],
            'kode_pos'          => $data_post['kode_pos'],
            'verify_coordinate' => 'Terverifikasi',
            'verify_by'         => $data_post['email'],
        );

        $data = array(
            'endpoint'      => 'setPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => $post_field
        );

        $response = $this->serviceAPI($data);

        if (!$response) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $response['keterangan']);
        } else if ($response && $response['kode'] != 200) {
            $this->session->set_flashdata('alert', 'error');
            $this->session->set_flashdata('message', $response['keterangan']);
        } else {
            $this->session->set_flashdata('alert', 'success');
            $this->session->set_flashdata('message', $response['keterangan']);
        }

        redirect(base_url() . 'web/pasar/lokasi');
    }

    public function validate_image1($file)
    {
        $check = TRUE;
        if ((!isset($_FILES['file1'])) || $_FILES['file1']['size'] == 0) {
            $this->form_validation->set_message('validate_image1', '{field} tidak boleh kosong.');
            $check = FALSE;
        } else if (isset($_FILES['file1']) && $_FILES['file1']['size'] != 0) {
            $allowedExts = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG);
            $extension = pathinfo($_FILES["file1"]["name"], PATHINFO_EXTENSION);
            $detectedType = exif_imagetype($_FILES['file1']['tmp_name']);
            $type = $_FILES['file1']['type'];
            if (!in_array($detectedType, $allowedTypes)) {
                $this->form_validation->set_message('validate_image1', '{field} harus berupa JPG/JPEG/PNG');
                $check = FALSE;
            }
            if (filesize($_FILES['file1']['tmp_name']) > 2000000) {
                $this->form_validation->set_message('validate_image1', 'The Image file size shoud not exceed 20MB!');
                $check = FALSE;
            }
            if (!in_array($extension, $allowedExts)) {
                $this->form_validation->set_message('validate_image1', "{field} harus berupa JPG/JPEG/PNG");
                $check = FALSE;
            }
        }

        return $check;
    }

    public function validate_image2()
    {
        $check = TRUE;
        if ((!isset($_FILES['file2'])) || $_FILES['file2']['size'] == 0) {
            $this->form_validation->set_message('validate_image2', '{field} tidak boleh kosong.');
            $check = FALSE;
        } else if (isset($_FILES['file2']) && $_FILES['file2']['size'] != 0) {
            $allowedExts = array("jpeg", "jpg", "png", "JPG", "JPEG", "PNG");
            $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG);
            $extension = pathinfo($_FILES["file2"]["name"], PATHINFO_EXTENSION);
            $detectedType = exif_imagetype($_FILES['file2']['tmp_name']);
            $type = $_FILES['file2']['type'];
            if (!in_array($detectedType, $allowedTypes)) {
                $this->form_validation->set_message('validate_image2', '{field} harus berupa JPG/JPEG/PNG');
                $check = FALSE;
            }
            if (filesize($_FILES['file2']['tmp_name']) > 2000000) {
                $this->form_validation->set_message('validate_image2', 'The Image file size shoud not exceed 20MB!');
                $check = FALSE;
            }
            if (!in_array($extension, $allowedExts)) {
                $this->form_validation->set_message('validate_image2', "{field} harus berupa JPG/JPEG/PNG");
                $check = FALSE;
            }
        }

        return $check;
    }

    public function doUpload($path = null, $post_name)
    {
        $response = array('success' => false, 'data' => null, 'error' => null);

        $this->load->library('upload');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $config = array(
            'upload_path'   => $path,
            'allowed_types' => "jpg|jpeg|png",
            'overwrite'     => TRUE,
            'max_size'      => 500,
            'encrypt_name'  => true,
        );

        $this->upload->initialize($config);

        if ($this->upload->do_upload($post_name)) {
            $response['data']   = $this->upload->data();
            $response['success'] = true;
        } else {
            $response['error']  = strip_tags($this->upload->display_errors());
        }

        return $response;
    }

    function cari_lokasi()
    {
        $data_post = $this->input->post();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' . str_replace(" ", "%20", $data_post['keyword']) . '&key=AIzaSyCkp-vI9gmy3zT9UfDivn9i_brbpNR8EK8',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    function lokasi_latlong()
    {
        $data_post = $this->input->post();

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://maps.googleapis.com/maps/api/geocode/json?place_id=' . $data_post['place_id'] . '&key=AIzaSyCkp-vI9gmy3zT9UfDivn9i_brbpNR8EK8',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Accept: application/json'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        echo $response;
    }

    function rekap_pasar()
    {

        $api = array(
            'endpoint'      => 'getRekapPasar',
            'controller'    => 'pasar',
            'method'        => 'POST',
            'post_field'    => array(
                'status' => 'Aktif'
            )
        );

        if (in_array($this->session->userdata('tipe_pengguna_id'), [2,  9, 19])) {
            //Kabupaten Kota
            $api['post_field']['daerah_id'] = substr($this->session->userdata('daerah_id'), 0, 4);
        } else if (in_array($this->session->userdata('tipe_pengguna_id'), [8,  18])) {
            // Provinsi
            $api['post_field']['daerah_id'] = substr($this->session->userdata('daerah_id'), 0, 2);
        } else {
            if (!empty($this->input->post('daerah_id'))) {
                $api['post_field']['daerah_id'] = $this->input->post('daerah_id');
            }
        }

        if (!empty($this->input->post('kondisi'))) {
            $api['post_field']['kondisi'] = $this->input->post('kondisi');
        }

        if (!empty($this->input->post('kepemilikan'))) {
            $api['post_field']['kepemilikan'] = $this->input->post('kepemilikan');
        }

        if (!empty($this->input->post('bentuk_pasar'))) {
            $api['post_field']['bentuk_pasar'] = $this->input->post('bentuk_pasar');
        }

        if (!empty($this->input->post('tipe_pasar_id'))) {
            $api['post_field']['tipe_pasar_id'] = $this->input->post('tipe_pasar_id');
        }

        $response = $this->serviceAPI($api);

        if ($response && $response['kode'] == 200) {

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $styleHeader = [
                'font' => [
                    'bold' => true,
                    'size' => 12,
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
                'borders' => [
                    'top' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'right' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'bottom' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                    'left' => [
                        'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    ],
                ],
            ];

            $sheet->setCellValue('A1', "REKAP DATA PASAR SISTEM INFORMASI SARANA PERDAGANGAN");
            $sheet->mergeCells('A1:BS1');
            $sheet->getStyle('A1:BS1')->getFont()->setBold(true);
            $sheet->getStyle('A1:BS1')->getFont()->setSize(16);;
            $sheet->getStyle('A1:BS1')->getAlignment()->setHorizontal('center');

            $sheet->setCellValue('A3', "No");
            $sheet->mergeCells('A3:A4');
            $sheet->getStyle('A3:A3')->applyFromArray($styleHeader);

            $sheet->setCellValue('B3', "Nama Pasar");
            $sheet->mergeCells('B3:B4');
            $sheet->getStyle('B3')->applyFromArray($styleHeader);

            $sheet->setCellValue('C3', "Deskripsi");
            $sheet->mergeCells('C3:C4');
            $sheet->getStyle('C3')->applyFromArray($styleHeader);

            $sheet->setCellValue('D3', "Alamat");
            $sheet->mergeCells('D3:I3');
            $sheet->getStyle('D3:I3')->applyFromArray($styleHeader);

            $sheet->setCellValue('D4', "Alamat");
            $sheet->getStyle('D4')->applyFromArray($styleHeader);

            $sheet->setCellValue('E4', "Kelurahan");
            $sheet->getStyle('E4')->applyFromArray($styleHeader);

            $sheet->setCellValue('F4', "Kecamatan");
            $sheet->getStyle('F4')->applyFromArray($styleHeader);

            $sheet->setCellValue('G4', "Kabupaten/Kota");
            $sheet->getStyle('G4')->applyFromArray($styleHeader);

            $sheet->setCellValue('H4', "Provinsi");
            $sheet->getStyle('H4')->applyFromArray($styleHeader);

            $sheet->setCellValue('I4', "Kodepos");
            $sheet->getStyle('I4')->applyFromArray($styleHeader);

            $sheet->setCellValue('J3', "Latitude");
            $sheet->mergeCells('J3:J4');
            $sheet->getStyle('J3')->applyFromArray($styleHeader);

            $sheet->setCellValue('K3', "Longtitude");
            $sheet->mergeCells('K3:K4');
            $sheet->getStyle('K3')->applyFromArray($styleHeader);

            $sheet->setCellValue('L3', "Kondisi");
            $sheet->mergeCells('L3:L4');
            $sheet->getStyle('L3')->applyFromArray($styleHeader);

            $sheet->setCellValue('M3', "Kepemilikan");
            $sheet->mergeCells('M3:M4');
            $sheet->getStyle('M3')->applyFromArray($styleHeader);

            $sheet->setCellValue('N3', "Bentuk Pasar");
            $sheet->mergeCells('N3:N4');
            $sheet->getStyle('N3')->applyFromArray($styleHeader);

            $sheet->setCellValue('O3', "Tipe Pasar");
            $sheet->mergeCells('O3:O4');
            $sheet->getStyle('O3')->applyFromArray($styleHeader);

            $sheet->setCellValue('P3', "Klasifikasi Pengelola");
            $sheet->mergeCells('P3:P4');
            $sheet->getStyle('P3')->applyFromArray($styleHeader);

            $sheet->setCellValue('Q3', "No. Telp");
            $sheet->mergeCells('Q3:Q4');
            $sheet->getStyle('Q3')->applyFromArray($styleHeader);

            $sheet->setCellValue('R3', "No. Fax");
            $sheet->mergeCells('R3:R4');
            $sheet->getStyle('R3')->applyFromArray($styleHeader);

            $sheet->setCellValue('S3', "Jenis Pasar");
            $sheet->mergeCells('S3:S4');
            $sheet->getStyle('S3')->applyFromArray($styleHeader);

            $sheet->setCellValue('T3', "Pengelola Pasar");
            $sheet->mergeCells('T3:T4');
            $sheet->getStyle('T3')->applyFromArray($styleHeader);

            $sheet->setCellValue('U3', "Waktu Operasional");
            $sheet->mergeCells('U3:U4');
            $sheet->getStyle('U3')->applyFromArray($styleHeader);

            $sheet->setCellValue('V3', "Jam Buka");
            $sheet->mergeCells('V3:V4');
            $sheet->getStyle('V3')->applyFromArray($styleHeader);

            $sheet->setCellValue('W3', "Jam Sibuk");
            $sheet->mergeCells('W3:W4');
            $sheet->getStyle('W3')->applyFromArray($styleHeader);

            $sheet->setCellValue('X3', "Jumlah");
            $sheet->mergeCells('X3:Z3');
            $sheet->getStyle('X3')->applyFromArray($styleHeader);

            $sheet->setCellValue('X4', "Pekerja Tetap");
            $sheet->getStyle('X4')->applyFromArray($styleHeader);

            $sheet->setCellValue('Y4', "Pekerja Tidak Tetap");
            $sheet->getStyle('Y4')->applyFromArray($styleHeader);

            $sheet->setCellValue('Z4', "Pengunjung Harian");
            $sheet->getStyle('Z4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AA3', "Bangunan");
            $sheet->mergeCells('AA3:AK3');
            $sheet->getStyle('AA3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AA4', "Luas Bangunan");
            $sheet->getStyle('AA4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AB4', "Luas Tanah");
            $sheet->getStyle('AB4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AC4', "Jumlah Lantai");
            $sheet->getStyle('AC4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AD4', "Tahun Bangunan");
            $sheet->getStyle('AD4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AE4', "Luas Renovasi");
            $sheet->getStyle('AE4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AF4', "Jumlah Kios");
            $sheet->getStyle('AF4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AG4', "Jumlah Pedagang Kios");
            $sheet->getStyle('AG4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AH4', "Jumlah Los");
            $sheet->getStyle('AH4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AI4', "Jumlah Pedagang Los");
            $sheet->getStyle('AI4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AJ4', "Jumlah Dasaran");
            $sheet->getStyle('AJ4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AK4', "Jumlah Pedagang Dasaran");
            $sheet->getStyle('AK4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AL3', "Jarak Toko Modern");
            $sheet->mergeCells('AL3:AL4');
            $sheet->getStyle('AL3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AM3', "Digitalisasi Pasar");
            $sheet->mergeCells('AM3:AN3');
            $sheet->getStyle('AM3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AM4', "Penerapan");
            $sheet->getStyle('AM4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AN4', "Metode");
            $sheet->getStyle('AN4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AO3', "Lembaga Keuangan");
            $sheet->mergeCells('AO3:AP3');
            $sheet->getStyle('AO3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AO4', "Dapat Pembiayaan");
            $sheet->getStyle('AO4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AP4', "Nama Lembaga");
            $sheet->getStyle('AP4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AQ3', "SNI Pasar Rakyat");
            $sheet->mergeCells('AQ3:AR3');
            $sheet->getStyle('AQ3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AQ4', "Ada Sertifikasi");
            $sheet->getStyle('AQ4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AR4', "Tahun Sertifikasi");
            $sheet->getStyle('AR4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AS3', "Sarana dan Prasarana");
            $sheet->mergeCells('AS3:AS4');
            $sheet->getStyle('AS3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AT3', "Dilalui Angkutan Umum");
            $sheet->mergeCells('AT3:AT4');
            $sheet->getStyle('AT3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AU3', "Dilalui Angkutan Umum");
            $sheet->mergeCells('AU3:AU4');
            $sheet->getStyle('AU3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AV3', "Dekat dengan pemukiman (< 2km)");
            $sheet->mergeCells('AV3:AV4');
            $sheet->getStyle('AV3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AW3', "Komoditas yang dijual");
            $sheet->mergeCells('AW3:AW4');
            $sheet->getStyle('AW3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AX3', "Omzet Pasar Tahun Sebelumnya");
            $sheet->mergeCells('AX3:AX4');
            $sheet->getStyle('AX3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AY3', "Paguyuban");
            $sheet->mergeCells('AY3:BB3');
            $sheet->getStyle('AY3')->applyFromArray($styleHeader);

            $sheet->setCellValue('AY4', "Nama Paguyuban");
            $sheet->getStyle('AY4')->applyFromArray($styleHeader);

            $sheet->setCellValue('AZ4', "Nama Ketua");
            $sheet->getStyle('AZ4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BA4', "No. Telp Ketua Paguyuban");
            $sheet->getStyle('BA4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BB4', "Asosiasi Pedagang Pasar");
            $sheet->getStyle('BB4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BC3', "Pengelola Pasar");
            $sheet->mergeCells('BC3:BS3');
            $sheet->getStyle('BC3')->applyFromArray($styleHeader);

            $sheet->setCellValue('BC4', "Klasifikasi Pengelola");
            $sheet->getStyle('BC4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BD4', "Nama Pengelola");
            $sheet->getStyle('BD4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BE4', "Jabatan");
            $sheet->getStyle('BE4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BF4', "No. Telpon");
            $sheet->getStyle('BF4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BG4', "Email");
            $sheet->getStyle('BG4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BH4', "Tingkat Pendidikan");
            $sheet->getStyle('BH4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BI4', "Jumlah Staf Pengelola");
            $sheet->getStyle('BI4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BJ4', "Struktur Pengelola");
            $sheet->getStyle('BJ4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BK4', "Nama Koperasi");
            $sheet->getStyle('BK4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BL4', "Kategori Koperasi");
            $sheet->getStyle('BL4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BM4', "Jumlah Anggota Koperasi");
            $sheet->getStyle('BM4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BN4', "No. Badan Hukum Koperasi");
            $sheet->getStyle('BN4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BO4', "Nomor Induk Koperasi");
            $sheet->getStyle('BO4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BP4', "Besaran Retribusi Kios");
            $sheet->getStyle('BP4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BQ4', "Besaran Retribusi Los");
            $sheet->getStyle('BQ4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BR4', "Besaran Retribusi Dasaran");
            $sheet->getStyle('BR4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BS4', "Pendapatan Tahunan Retribusi");
            $sheet->getStyle('BS4')->applyFromArray($styleHeader);

            $sheet->setCellValue('BT4', "Status");
            $sheet->getStyle('BT4')->applyFromArray($styleHeader);

            $range = range('A', 'BT');
            foreach ($range as $k => $v) {
                $sheet->getColumnDimension($v)->setAutoSize(true);
            }

            $kolom = 5;
            $nomor = 1;
            foreach ($response['data'] as $v) {
                $nama_bangunan = explode(',', $v['jenis_bangunan']);
                $kios = array_search("Kios", $nama_bangunan);
                $los = array_search("Los", $nama_bangunan);
                $dasaran = array_search("Dasaran", $nama_bangunan);
                $bangunan = explode(',', $v['bangunan_jumlah']);
                $pedagang = explode(',', $v['bangunan_jumlah_pedagang']);


                $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A' . $kolom, $nomor)
                    ->setCellValue('B' . $kolom, ucwords(strtolower($v['nama'])))
                    ->setCellValue('C' . $kolom, $v['deskripsi'])
                    ->setCellValue('D' . $kolom, $v['alamat'])
                    ->setCellValue('E' . $kolom, $v['kelurahan'] == null ? '-' : $v['kelurahan'])
                    ->setCellValue('F' . $kolom, $v['kecamatan'])
                    ->setCellValue('G' . $kolom, $v['kab_kota'])
                    ->setCellValue('H' . $kolom, $v['provinsi'])
                    ->setCellValue('I' . $kolom, $v['kode_pos'])
                    ->setCellValue('J' . $kolom, $v['latitude'])
                    ->setCellValue('K' . $kolom, $v['longitude'])
                    ->setCellValue('L' . $kolom, $v['kondisi'])
                    ->setCellValue('M' . $kolom, $v['kepemilikan'])
                    ->setCellValue('N' . $kolom, $v['bentuk_pasar'])
                    ->setCellValue('O' . $kolom, $v['tipe_pasar'])
                    ->setCellValue('P' . $kolom, $v['klasifikasi_pengelola'])
                    ->setCellValue('Q' . $kolom, $v['no_telp'])
                    ->setCellValue('R' . $kolom, $v['no_fax'])
                    ->setCellValue('S' . $kolom, $v['jenis_pasar'] == null ? '-' : $v['jenis_pasar'])
                    ->setCellValue('T' . $kolom, $v['pengelola_pasar'])
                    ->setCellValue('U' . $kolom, $v['waktu_operasional'])
                    ->setCellValue('V' . $kolom, $v['jam_operasional_awal'] . ' s/d ' . $v['jam_operasional_akhir'])
                    ->setCellValue('W' . $kolom, $v['jam_sibuk_awal'] . ' s/d ' . $v['jam_sibuk_akhir'])
                    ->setCellValue('X' . $kolom, $v['jumlah_pekerja_tetap'])
                    ->setCellValue('Y' . $kolom, $v['jumlah_pekerja_nontetap'])
                    ->setCellValue('Z' . $kolom, $v['jumlah_pengunjung_harian'])
                    ->setCellValue('AA' . $kolom, $v['luas_bangunan'])
                    ->setCellValue('AB' . $kolom, $v['luas_tanah'])
                    ->setCellValue('AC' . $kolom, $v['jumlah_lantai'])
                    ->setCellValue('AD' . $kolom, $v['tahun_bangun'])
                    ->setCellValue('AE' . $kolom, $v['tahun_renovasi'])
                    ->setCellValue('AF' . $kolom, $bangunan[$kios])
                    ->setCellValue('AG' . $kolom, $pedagang[$kios])
                    ->setCellValue('AH' . $kolom, $bangunan[$los])
                    ->setCellValue('AI' . $kolom, $pedagang[$los])
                    ->setCellValue('AJ' . $kolom, $bangunan[$dasaran])
                    ->setCellValue('AK' . $kolom, $pedagang[$dasaran])
                    ->setCellValue('AL' . $kolom, $v['jarak_toko_modern'])
                    ->setCellValue('AM' . $kolom, $v['penerapan_digitalisasi_pasar'] == '' ? '-' : $v['penerapan_digitalisasi_pasar'])
                    ->setCellValue('AN' . $kolom, $v['penerapan_digitalisasi_pasar'] == 'Ya' ? $v['detail_digitalisasi_pasar'] : '-')
                    ->setCellValue('AO' . $kolom, $v['pembiayaan_lk'])
                    ->setCellValue('AP' . $kolom, $v['pembiayaan_lk'] == 'Ya' ? $v['lembaga_keuangan'] : '-')
                    ->setCellValue('AQ' . $kolom, $v['sertifikasi_sni'])
                    ->setCellValue('AR' . $kolom, $v['sertifikasi_sni'] == 'Ya' ? $v['tahun_sertifikasi'] : '-')
                    ->setCellValue('AS' . $kolom, $v['sarana_prasarana'])
                    ->setCellValue('AT' . $kolom, $v['angkutan_umum'])
                    ->setCellValue('AU' . $kolom, $v['angkutan_umum'])
                    ->setCellValue('AV' . $kolom, $v['jarak_pemukiman'])
                    ->setCellValue('AW' . $kolom, $v['komoditas_dijual'])
                    ->setCellValue('AX' . $kolom, $v['omzet_sebelumnya'])
                    ->setCellValue('AY' . $kolom, $v['nama_paguyuban'])
                    ->setCellValue('AZ' . $kolom, $v['nama_ketua'])
                    ->setCellValue('BA' . $kolom, $v['no_telp_ketua'])
                    ->setCellValue('BB' . $kolom, $v['asosiasi_perdagangan_pasar'])
                    ->setCellValue('BC' . $kolom, $v['klasifikasi_pengelola'])
                    ->setCellValue('BD' . $kolom, $v['nama_pengelola'])
                    ->setCellValue('BE' . $kolom, $v['jabatan_pengelola'])
                    ->setCellValue('BF' . $kolom, $v['no_telp_pengelola'])
                    ->setCellValue('BG' . $kolom, $v['email_pengelola'])
                    ->setCellValue('BH' . $kolom, $v['tingkat_pendidikan'])
                    ->setCellValue('BI' . $kolom, $v['jumlah_staf'])
                    ->setCellValue('BJ' . $kolom, $v['struktur_pengelola'])
                    ->setCellValue('BK' . $kolom, $v['nama_koperasi'])
                    ->setCellValue('BL' . $kolom, $v['kategori_koperasi'])
                    ->setCellValue('BM' . $kolom, $v['jumlah_anggota_koperasi'])
                    ->setCellValue('BN' . $kolom, $v['no_badan_hukum_koperasi'])
                    ->setCellValue('BO' . $kolom, $v['no_induk_koperasi'])
                    ->setCellValue('BP' . $kolom, $v['besaran_retribusi_kios'])
                    ->setCellValue('BQ' . $kolom, $v['besaran_retribusi_los'])
                    ->setCellValue('BR' . $kolom, $v['besaran_retribusi_dasaran'])
                    ->setCellValue('BS' . $kolom, $v['pendapatan_tahunan_retribusi'])
                    ->setCellValue('BT' . $kolom, $v['status']);

                $kolom++;
                $nomor++;
            }

            $filename = 'Rekap Pasar SISP';
            $sheet->setTitle('Rekap Pasar SISP');

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=' . $filename . '.xlsx');
            header('Cache-Control: max-age=0');

            $writer = new Xlsx($spreadsheet);

            $writer->save('php://output');
            die();
        }
    }
}
