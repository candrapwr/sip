<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('FILE_ENCRYPTION_BLOCKS', 10000);
use \Firebase\JWT\JWT;
class MY_Controller extends CI_Controller
{
	public $data = [];
	protected $token = '';

	function __construct()
	{
		parent::__construct();
		#MODEL Loads
		$this->load->model('Baseben_Model', 'baseben');
		$this->load->library('encryption');
		$this->token = ($this->session->userdata('token')) ? $this->session->userdata('token') : '';
	}

	function jwt_key()
	{
		return "SISP (*.*) (-.-) (-.*) (-.0) JWT PUSAT DATA DAN SISTEM INFORMASI Kementerian Perdagangan";
	}

	function jwtEncode($id){
		return JWT::encode($id, $this->jwt_key(), 'HS512');
	}

	function jwtDecode($id){
		return JWT::decode($id, $this->jwt_key(), array('HS512'));
	}

	function getDomain($address)
	{
		$parseUrl = parse_url(trim($address));
		$host_names = explode(".", $parseUrl['host']);
		$domain = '';
		for ($i = count($host_names) - 1; $i > 0; $i--) {
			$domain .= $host_names[count($host_names) - $i];
			if ($i != 1) {
				$domain .= '.';
			}
		}
		return $domain;
	}

	function is_login()
	{
		if (empty($this->session->userdata['token'])) {
			redirect(base_url());
		}
	}

	function is_admin()
	{
		if ($this->session->userdata['role'] == '' || $this->session->userdata['role'] != 'Administrator') {
			redirect('404_override');
		}
	}

	function set_cookie()
	{
		$post = $this->input->post();
		if (is_array($post)) {
			foreach ($post as $k => $v) {
				set_cookie($v['name'], $v['value'], $v['expire']);
			}
		}
	}

	function _upload($id_input, $jenis, $name = null)
	{
		$path = './res/upload/' . $jenis . '/' . date('Y') . '/';

		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}

		$this->load->library('upload');

		$config = array(
			'upload_path' => $path,
			'allowed_types' => "png|jpg|jpeg|webp|pdf|docx|doc|ppt|xlsx|xls|pptx",
			'overwrite' => TRUE,
			'encrypt_name' => TRUE,
			'max_size' => "50000"
		);

		if ($name) {
			$config['file_name'] = $name;
		}

		$this->upload->initialize($config);

		if ($this->upload->do_upload($id_input)) {
			return $this->upload->data("file_name");
		} else {
			return $this->upload->display_errors();
		}
	}
	
	function multiple_upload($input_name, $jenis, $files, $id)
	{
		$path = './res/upload/' . $jenis . '/' . date('Y') . '/' . $id . '/';
		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}

		$config = array(
			'upload_path'   => $path,
			'overwrite'     => 1,
			'encrypt_name' => TRUE,
			'allowed_types' => 'gif|jpg|png|docx|doc|xls|xlsx|pdf|zip|rar|jpeg|ppt|pptx'
		);

		$this->load->library('upload', $config);

		$images = array();

		foreach ($files['name'] as $key => $image) {
			if (!empty($files['name'][$key])) {
				$_FILES[$input_name]['name'] = $files['name'][$key];
				$_FILES[$input_name]['type'] = $files['type'][$key];
				$_FILES[$input_name]['tmp_name'] = $files['tmp_name'][$key];
				$_FILES[$input_name]['error'] = $files['error'][$key];
				$_FILES[$input_name]['size'] = $files['size'][$key];
				$fileName = date('YmdHis') . '_' . $image;
				$config['file_name'] = $fileName;

				$this->upload->initialize($config);

				if ($this->upload->do_upload($input_name)) {
					$upload_data = $this->upload->data();
					$images[] = $upload_data['file_name'];
				} else {
					return false;
				}
			}
		}
		return $images;
	}

	public static function slugify($text, string $divider = '-')
	{
		// replace non letter or digits by divider
		$text = preg_replace('~[^\pL\d]+~u', $divider, $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, $divider);

		// remove duplicate divider
		$text = preg_replace('~-+~', $divider, $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}

	function download($dir, $year, $filename,$service = null)
	{
		//var_dump($dir);die();
		$this->load->helper('download');
		$dir = str_replace('-', '/', $dir);
		if($service !=null){
			force_download('../services/res/upload/' . $dir . '/' . $year . '/' . $filename, NULL);
		}else{
			force_download('./res/upload/' . $dir . '/' . $year . '/' . $filename, NULL);
		}
		
	}

	function download_image_url($url, $path, $filename)
	{
		try {
			if (!file_exists($path)) {
				mkdir($path, 0777, true);
			}
			$ch = curl_init($url);
			$fp = fopen($path . '/' . $filename, 'wb');
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
			return $path;
		} catch (Exception $e) {
			return false;
		}
	}

	function send_mail($email_data = NULL)
	{
		if (is_null($email_data)) {
			$email_data = array(
				'from' => $this->input->post('from'),
				'sender' => $this->input->post('sender'),
				'to' => $this->input->post('to'),
				'subject' => $this->input->post('subject'),
				'message' => $this->input->post('message')
			);
		}
		$this->load->library('email');

		$this->email->from($email_data->from, $email_data->sender);
		$this->email->to($email_data->to);
		$this->email->subject($email_data->subject);
		$this->email->message($email_data->message);
		if (count($email_data->attachment) > 0) {
			foreach ($email_data->attachment as $attachment_files) {
				$this->email->attach($attachment_files);
			}
		}

		if ($this->email->send()) {
			return TRUE;
		} else {
			$this->session->set_flashdata('email_error', $this->email->print_debugger());
			return FALSE;
		}
	}

	function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) === $date;
	}

	public function log($type, $body, $nip, $callback = null)
	{
		$this->load->library('user_agent');

		if ($this->agent->is_browser()) {
			$agent = $this->agent->browser() . ' ' . $this->agent->version();
		} elseif ($this->agent->is_robot()) {
			$agent = $this->agent->robot();
		} elseif ($this->agent->is_mobile()) {
			$agent = $this->agent->mobile();
		} else {
			$agent = 'Unidentified User Agent';
		}
		$agent = $agent . '-' . $this->agent->platform();
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if (isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if (isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];

		$con = array(
			'table_name' => 'activity_log',
			'type' => $type,
			'user_agent' => $agent,
			'ip' => $ipaddress,
			'createdon' => date('Y-m-d H:i:s'),
			'nip' => $nip,
			'body' => $body,
			'callback' => $callback
		);
		$this->baseben->insert($con);
	}

	function setActiveMenu(){
		$this->session->set_userdata('active_menu',$this->input->post('active_menu'));
	}
	
	function update_status()
	{
		$data_post = $this->input->post();
		$api = array(
			'endpoint'      => 'setStatus',
			'method'        => 'POST',
			'post_field'    => array(
				'token' => $this->token,
				'email' => $this->session->userdata('email'),
				'table_name' => $data_post['table_name'],
				'key_name' => $data_post['key_name'],
				'key' => $data_post['key'],
				'status' => $data_post['status'],
				'db' => $data_post['db'],
				'keterangan_perubahan' => $data_post['keterangan_perubahan'],
			)
		);

		$response = $this->serviceAPI($api);

		if ($response && $response['kode'] == 200) {
			echo json_encode(array('kode' => 200, 'keterangan' => 'Status berhasil di update'));
		} else {
			echo json_encode(array('kode' => 500, 'keterangan' => 'Status gagal di update'));
		}
	}

	public function serviceAPI($data = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sisp.kemendag.go.id/services/' . (!empty($data['controller']) ? $data['controller'] : 'api') . '/' . $data['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $data['method'],
			CURLOPT_POSTFIELDS => (isset($data['post_field'])) ? $data['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: 2iWZE4XdnTQ0mX5GTEnhhz0T7C9Yz2rjCgKqgw3h',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return ($response['kode'] != 401) ? $response : false;
	}

	public function ewsAPI($params = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://ws.kemendag.go.id/komoditi/' . $params['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $params['method'],
			CURLOPT_POSTFIELDS => (isset($params['post_field'])) ? $params['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}

	public function templatebackend($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;
		$view['rolemenu'] = 'admin/';
		$this->load->view('backend/structure/index', $view);
	}

	public function templateGovCMS($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;
		$view['rolemenu'] = 'public_gov/';

		$this->load->view('backend/structure/index', $view);
	}

	public function template($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;

		$this->load->view('template/main', $view);
	}

	function getTableauToken($username)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://analitik.kemendag.go.id/trusted/',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => 'username=' . $username,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function getResponseCaptcha($secretKey)
	{
		$captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_KEY . '&response=' . $secretKey);

		$return = json_decode($captcha_response, true);

		return $return;
	}
}

class Admin_Controller extends MY_Controller
{
	protected $token = '';

	function __construct()
	{
		parent::__construct();

		$this->token = ($this->session->userdata('token')) ? $this->session->userdata('token') : '';
	}


	function setActiveMenu(){
		$this->session->set_userdata('active_menu',$this->input->post('active_menu'));
	}
	
	function update_status()
	{
		$data_post = $this->input->post();
		$api = array(
			'endpoint'      => 'setStatus',
			'method'        => 'POST',
			'post_field'    => array(
				'token' => $this->token,
				'email' => $this->session->userdata('email'),
				'table_name' => $data_post['table_name'],
				'key_name' => $data_post['key_name'],
				'key' => $data_post['key'],
				'status' => $data_post['status'],
				'db' => $data_post['db'],
				'keterangan' => $data_post['keterangan'],
				'keterangan_perubahan' => $data_post['keterangan_perubahan'],
			)
		);

		$response = $this->serviceAPI($api);

		if ($response && $response['kode'] == 200) {
			echo json_encode(array('kode' => 200, 'keterangan' => 'Status berhasil di update'));
		} else {
			echo json_encode(array('kode' => 500, 'keterangan' => 'Status gagal di update'));
		}
	}

	public function serviceAPI($data = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sisp.kemendag.go.id/services/' . (!empty($data['controller']) ? $data['controller'] : 'api') . '/' . $data['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $data['method'],
			CURLOPT_POSTFIELDS => (isset($data['post_field'])) ? $data['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: 2iWZE4XdnTQ0mX5GTEnhhz0T7C9Yz2rjCgKqgw3h',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return ($response['kode'] != 401) ? $response : false;
	}

	public function ewsAPI($params = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://ws.kemendag.go.id/komoditi/' . $params['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $params['method'],
			CURLOPT_POSTFIELDS => (isset($params['post_field'])) ? $params['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}

	public function templatebackend($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;
		$view['rolemenu'] = 'admin/';
		$this->load->view('backend/structure/index', $view);
	}

	public function templateGovCMS($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;
		$view['rolemenu'] = 'public_gov/';

		$this->load->view('backend/structure/index', $view);
	}

	public function template($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;

		$this->load->view('template/main', $view);
	}

	function getTableauToken($username)
	{
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://analitik.kemendag.go.id/trusted/',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 10,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => 'username=' . $username,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function getResponseCaptcha($secretKey)
	{
		$captcha_response = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . SECRET_KEY . '&response=' . $secretKey);

		$return = json_decode($captcha_response, true);

		return $return;
	}
}

class Public_Controller extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

	}

	public function template($content, $data)
	{
		$view['content'] = $content;
		$view['data'] = $data;

		$this->load->view('public/template/main', $view);
	}

	public function serviceAPI($data = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sisp.kemendag.go.id/services/' . (!empty($data['controller']) ? $data['controller'] : 'api') . '/' . $data['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $data['method'],
			CURLOPT_POSTFIELDS => (isset($data['post_field'])) ? $data['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: 2iWZE4XdnTQ0mX5GTEnhhz0T7C9Yz2rjCgKqgw3h',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}

	public function ewsAPI($params = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://ws.kemendag.go.id/komoditi/' . $params['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $params['method'],
			CURLOPT_POSTFIELDS => (isset($params['post_field'])) ? $params['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}
}
