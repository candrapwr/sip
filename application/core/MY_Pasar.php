<?php
defined('BASEPATH') or exit('No direct script access allowed');
define('FILE_ENCRYPTION_BLOCKS', 10000);
class MY_Pasar extends CI_Controller
{

	function __construct()
	{

		parent::__construct();
		#MODEL Loads
		$this->load->model('Baseben_Model', 'baseben');
		$this->load->library('encryption');
	}

    function setActiveMenu(){
		$this->session->set_userdata('active_menu',$this->input->post('active_menu'));
	}
	
	function update_status()
	{
		$data_post = $this->input->post();
		$api = array(
			'endpoint'      => 'setStatus',
			'method'        => 'POST',
			'post_field'    => array(
				'token' => $this->token,
				'email' => $this->session->userdata('email'),
				'table_name' => $data_post['table_name'],
				'key_name' => $data_post['key_name'],
				'key' => $data_post['key'],
				'status' => $data_post['status'],
				'keterangan_perubahan' => $data_post['keterangan_perubahan'],
			)
		);

		$response = $this->serviceAPI($api);

		if ($response && $response['kode'] == 200) {
			echo json_encode(array('kode' => 200, 'keterangan' => 'Status berhasil di update'));
		} else {
			echo json_encode(array('kode' => 500, 'keterangan' => 'Status gagal di update'));
		}
	}


	public function serviceAPI($data = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://sisp.kemendag.go.id/services/' . (!empty($data['controller']) ? $data['controller'] : 'api') . '/' . $data['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $data['method'],
			CURLOPT_POSTFIELDS => (isset($data['post_field'])) ? $data['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: 2iWZE4XdnTQ0mX5GTEnhhz0T7C9Yz2rjCgKqgw3h',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}

	public function ewsAPI($params = array())
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://ws.kemendag.go.id/komoditi/' . $params['endpoint'],
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_POST => 1,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $params['method'],
			CURLOPT_POSTFIELDS => (isset($params['post_field'])) ? $params['post_field'] : '',
			CURLOPT_HTTPHEADER => array(
				'x-api-key: PFfI5v80q3omUqZNpzNiUVEDUiLS7DlGho7IvEAL',
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);

		$response = json_decode($response, true);

		return $response;
	}
}