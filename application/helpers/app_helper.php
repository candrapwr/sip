<?php


if (!function_exists('pre')) {
    function pre($arr)
    {
        echo "<pre>";
        print_r($arr);
        die();
    }
}

if (!function_exists('rupiah')) {
    function rupiah($angka)
    {
        $hasil_rupiah = number_format($angka, 0, ',', '.');
        return $hasil_rupiah;
    }
}



if (!function_exists('date_indo')) {
    function date_indo($date, $format)
    {
        $month = date('Y-m-d', strtotime($date));
        $exp = explode('-', $month);

        $year = $exp[0];
        $month = $exp[1];
        $day = (int) $exp[2];

        $indo_month = '';
        switch ($month) {
            case '1':
                $indo_month = 'Januari';
                break;
            case '2':
                $indo_month = 'Februari';
                break;
            case '3':
                $indo_month = 'Maret';
                break;
            case '4':
                $indo_month = 'April';
                break;
            case '5':
                $indo_month = 'Mei';
                break;
            case '6':
                $indo_month = 'Juni';
                break;
            case '7':
                $indo_month = 'Juli';
                break;
            case '8':
                $indo_month = 'Agustus';
                break;
            case '9':
                $indo_month = 'September';
                break;
            case '10':
                $indo_month = 'Oktober';
                break;
            case '11':
                $indo_month = 'November';
                break;
            case '12':
                $indo_month = 'Desember';
                break;
            default:
                # code...
                break;
        }

        $return = '';

        if ($format == 'Y-m-d') {
            $return = $year . '-' . $indo_month . '-' . $day;
        } else if ($format == 'd-m-Y') {
            $return = $day . '-' . $indo_month . '-' . $year;
        } else if ($format == 'Y m d') {
            $return = $year . ' ' . $indo_month . ' ' . $day;
        } else if ($format == 'd m Y') {
            $return = $day . ' ' . $indo_month . ' ' . $year;
        } else if ($format == 'm Y') {
            $return = $indo_month . ' ' . $year;
        } else if ($format == 'd m') {
            $return = $day . ' ' . $indo_month;
        }

        // $month = $exp[1];

        // $date = date($format, strtotime($date));

        return $return;
    }
}


if (!function_exists('full_date')) {
    function full_date($date = null)
    {
        $date2      = ($date != null) ? date('Y-m-d', strtotime($date)) : date('Y-m-d');
        $date_int   = date('d', strtotime($date2));

        $date   = ($date != null) ? date('Y-m-D', strtotime($date)) : date('Y-m-D');
        $exp    = explode('-', $date);
        $yd     = $exp[0];
        $md     = $exp[1];
        $dd     = $exp[2];

        $d = null;
        switch ($dd) {
            case 'Sun':
                $d = "Minggu";
                break;
            case 'Mon':
                $d = "Senin";
                break;
            case 'Tue':
                $d = "Selasa";
                break;
            case 'Wed':
                $d = "Rabu";
                break;
            case 'Thu':
                $d = "Kamis";
                break;
            case 'Fri':
                $d = "Jum'at";
                break;
            case 'Sat':
                $d = "Sabtu";
                break;

            default:
                $d = "";
                break;
        }

        $indo_month = '';
        switch ($md) {
            case '1':
                $indo_month = 'Januari';
                break;
            case '2':
                $indo_month = 'Februari';
                break;
            case '3':
                $indo_month = 'Maret';
                break;
            case '4':
                $indo_month = 'April';
                break;
            case '5':
                $indo_month = 'Mei';
                break;
            case '6':
                $indo_month = 'Juni';
                break;
            case '7':
                $indo_month = 'Juli';
                break;
            case '8':
                $indo_month = 'Agustus';
                break;
            case '9':
                $indo_month = 'September';
                break;
            case '10':
                $indo_month = 'Oktober';
                break;
            case '11':
                $indo_month = 'November';
                break;
            case '12':
                $indo_month = 'Desember';
                break;
            default:
                # code...
                break;
        }


        return $d . ', ' . (int) $date_int . ' ' . $indo_month . ' ' . $yd;
    }
}

if (!function_exists('slugify')) {
    function slugify($text, $id)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text . "-" . $id;
    }
}

function menu($jenis = null)
{
    $ci = get_instance();
    $api = array(
        'endpoint'      => 'getMenu',
        'method'        => 'POST',
        'controller'   => 'api',
        'post_field'    => array(
            'jenis' => $jenis,
            'publik' => 'Y',
            'urutan' => 'ASC'
        )
    );

    $response = $ci->serviceAPI($api);

    return $response;
}

function menu_back()
{
    $ci = get_instance();
    $res = $ci->session->userdata('menu')['data'];
    $menu = array('items' => array(), 'parents' => array());
    foreach ($res as $k => $v) {
        $menu['items'][$v['menu_id']] = $v;
        $menu['parents'][$v['parent_id']][] = $v['menu_id'];
    }
    if ($menu) {
        $result = build_main_menu(0, $menu);
        return $result;
    } else {
        return FALSE;
    }
}

function has_access($menu_id, $access)
{
    $ci = get_instance();
    if ($ci->session->userdata('role') == 'Administrator') {
        return true;
    }
    $menu = $ci->session->userdata('perms_menu');
    $sub_menu = $ci->session->userdata('perms_sub_menu');

    $has = false;
    if (in_array($menu_id, array_keys($menu))) {
        if ($access != 'show') {
            if ($menu[$menu_id][$access] == 'Y') {
                $has = true;
            }
        } else {
            $has = true;
        }
    }
    if (in_array($menu_id, array_keys($sub_menu))) {
        if ($access != 'show') {
            if ($sub_menu[$menu_id][$access] == 'Y') {
                $has = true;
            }
        } else {
            $has = true;
        }
    }
    return $has;
}

function build_main_menu($parent, $menu)
{
    $html = "";
    var_dump($menu['parents'][$parent]);
    die();

    if (isset($menu['parents'][$parent])) {
        if ($parent == '0') {
            $html .= "<ul id='side-menu' class='metismenu list-unstyled'>";
        } else {
            $html .= "<ul class='sub-menu'>";
        }
        foreach ($menu['parents'][$parent] as $itemId) {
            if (!isset($menu['parents'][$itemId])) {
                if (preg_match("/^http/", $menu['items'][$itemId]->url)) {
                    $html .= "<li><a href='" . $menu['items'][$itemId]->url . "'>" . $menu['items'][$itemId]->nama_menu . "</a></li>";
                } else {
                    $html .= "<li><a href='" . base_url() . '' . $menu['items'][$itemId]->url . "'>" . $menu['items'][$itemId]->nama_menu . "</a></li>";
                }
            }
            if (isset($menu['parents'][$itemId])) {
                if (preg_match("/^http/", $menu['items'][$itemId]->url)) {
                    $html .= "<li><a class='has-arrow waves-effect' href='" . $menu['items'][$itemId]->url . "'><span>" . $menu['items'][$itemId]->nama_menu . "</span></a>";
                } else {
                    $html .= "<li><a class='has-arrow waves-effect' href='" . base_url() . '' . $menu['items'][$itemId]->url . "'><span>" . $menu['items'][$itemId]->nama_menu . "</span></a>";
                }
                $html .= build_main_menu($itemId, $menu);
                $html .= "</li>";
            }
        }
        $html .= "</ul>";
    }
    //var_dump('ccccc');
    return $html;
}

function komplain($daerah_id)
{
    $ci = get_instance();
    $api = array(
        'endpoint'      => 'getKomplain',
        'method'        => 'POST',
        'controller'   => 'pasar',
        'post_field'    => array(
            'daerah_id' => $daerah_id,
            'status_tiket' => 'OPEN',
        )
    );

    $response = $ci->serviceAPI($api);

    if ($response['kode'] == 404) {
        $data = array();
    } else {
        $data = $response['data'];
    }

    return $data;
}

function is_allowed($menu_id)
{
    $ci = get_instance();
    $perms_session = $ci->session->userdata('menu')['data'];
    $menu = array();

    foreach ($perms_session as $k => $v) {
        $menu[] = array(
            'menu_' . $v['menu_id'] => $v['menu']
        );

        foreach ($v['sub_menu'] as $a => $b) {
            $menu[] = array(
                'menu_' . $b['menu_id'] => $b['menu']
            );
        }
    }
    
    if (!array_key_exists('menu_' . $menu_id, $menu) && $ci->session->userdata('role_id') != 1) {
        redirect(base_url() . '404_override');
    }
}

function is_dinas()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 8, 18, 9, 19, 22]));
}

function is_verifikator_proposal()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 21, 23, 27, 24, 25]));
}

function is_k1_pelaksana()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 21]));
}

function is_k1_ketuatim()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 23]));
}

function is_verifikator1_k5()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 24]));
}

function is_verifikator2_k5()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 25]));
}

function is_verifikator1_lelang()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 24]));
}

function is_verifikator2_lelang()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 25]));
}

function is_verifikator1_k5_kontrak()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 24]));
}

function is_verifikator2_k5_kontrak()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 25]));
}

function is_verifikator1_k5_dokumen()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 24]));
}

function is_verifikator2_k5_dokumen()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 25]));
}

function is_k1_koor()
{
    $ci = get_instance();
    return (in_array($ci->session->userdata('tipe_pengguna_id'), [3, 27]));
}



function download($dir, $filename, $rename = null)
{
    $ci = get_instance();
    $post_data = $ci->security->xss_clean(json_encode(array($dir, $filename, $rename)));
    if (strpos($post_data, '[removed]') !== false) {
        $ci->session->set_flashdata('alert', 'warning');
        $ci->session->set_flashdata('message', '<strong>Download Gagal!</strong>');
        //	redirect(base_url());
    }

    $ci->load->helper('download');
    $dir = str_replace('-', '/', $dir);
    if (!file_exists('./services/res/upload/' . $dir . '/' . $filename)) {
        redirect(base_url() . '404_overide');
    }
    $ext = pathinfo('./services/res/upload/' . $dir . '/' . $filename, PATHINFO_EXTENSION);
    if ($rename != null) {
        force_download($rename . '.' . $ext, file_get_contents('./services/res/upload/' . $dir . '/' . $filename), true);
    } else {
        force_download('./services/res/upload/' . $dir . '/' . $filename, null, true);
    }
}

function tgl_indo($tanggal)
{
    $bulan = bulan($tanggal);
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan . ' ' . $pecahkan[0];
}

function bulan($tanggal)
{
    $bulan = array(
        1 =>   'Januari',
        'Februari',
        'Maret',
        'April',
        'Mei',
        'Juni',
        'Juli',
        'Agustus',
        'September',
        'Oktober',
        'November',
        'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $bulan[(int)$pecahkan[1]];
}

function isValidLatLong($lat, $long){
    if(preg_match('/^(-?\d+(\.\d+)?),?\s*([-+]?\d+(\.\d+)?)$/', $lat) && ($lat >= -90) && ($lat <= 90) ===true  && preg_match('/^(-?\d+(\.\d+)?),?\s*([-+]?\d+(\.\d+)?)$/', $long) && ($long >= -180) && ($long <= 180) === true){
        return true;
    }
    return false;
}
