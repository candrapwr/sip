<!-- Footer -->
<footer class="footer pt-5 pb-4 pb-lg-5">
    <div class="container text-center pt-lg-3">
        <div class="navbar-brand justify-content-center text-dark mb-2 mb-lg-4">
            <img  data-aos="fade-up" src="<?= base_url() ?>assets/brand/logo.svg" class="light-mode-img" width="200" alt="Logo Sistem Informasi Sarana Perdagangan (SISP)">
            <img  data-aos="fade-up" src="<?= base_url() ?>assets/brand/logo-dark.svg" class="dark-mode-img" width="200" alt="Logo Sistem Informasi Sarana Perdagangan (SISP)">
        </div>
        <ul class="nav justify-content-center pt-3 pb-4 pb-lg-5">
            <?php
            $menu = menu('Front end');
            foreach ($menu['data'] as $k => $v) {
                $url = $v['url'];

                if ($v['menu'] == 'Sebaran Pasar' && empty($this->session->userdata('token'))) {
                    $url = '#sec_statistik';
                }
                echo '<li class="nav-item"  data-aos="flip-up"><a href="' . base_url() . $url . '" class="nav-link">' . $v['menu'] . '</a></li>';
            }

            ?>
            <?php if ($this->session->userdata('role') == 'Pengelola/Petugas') { ?>
                <li class="nav-item"  data-aos="flip-up"><a href="<?= base_url() ?>e-komplain" class="nav-link">E-Komplain <span class="badge badge-danger"><?= count(komplain(substr($this->session->userdata('daerah_id'), 0, 4))) ?></span></a></li>
            <?php } else if (empty($this->session->userdata('role'))) { ?>
                <li class="nav-item"  data-aos="flip-up"><a href="<?= base_url() ?>e-komplain" class="nav-link">E-Komplain</a></li>
            <?php } ?>
        </ul>
        <div class="d-flex flex-column flex-sm-row justify-content-center">
            <a  data-aos="fade-up" href="https://apps.apple.com/au/app/sisp-kemendag/id1610988214" target="_blank" class="btn btn-dark btn-lg px-3 py-2 me-sm-4 mb-3">
                <img src="<?= base_url() ?>assets/frontend/img/market/appstore-light.svg" class="light-mode-img" width="124" alt="App Store">
                <img src="<?= base_url() ?>assets/frontend/img/market/appstore-dark.svg" class="dark-mode-img" width="124" alt="App Store">
            </a>
            <a  data-aos="fade-up" href="https://play.google.com/store/apps/details?id=com.kemendag.sisp" target="_blank" class="btn btn-dark btn-lg px-3 py-2 mb-3">
                <img src="<?= base_url() ?>assets/frontend/img/market/googleplay-light.svg" class="light-mode-img" width="139" alt="Google Play">
                <img src="<?= base_url() ?>assets/frontend/img/market/googleplay-dark.svg" class="dark-mode-img" width="139" alt="Google Play">
            </a>
        </div>
        <div class="d-flex justify-content-center pt-4 mt-lg-3">
            <a  data-aos="flip-up" href="https://id-id.facebook.com/contents/Kementerian-Perdagangan/608925165792115" class="btn btn-icon btn-secondary btn-facebook mx-2">
                <i class="bx bxl-facebook"></i>
            </a>
            <a  data-aos="flip-up" href="https://www.instagram.com/kemendag/" class="btn btn-icon btn-secondary btn-instagram mx-2">
                <i class="bx bxl-instagram"></i>
            </a>
            <a  data-aos="flip-up" href="https://twitter.com/Kemendag" class="btn btn-icon btn-secondary btn-twitter mx-2">
                <i class="bx bxl-twitter"></i>
            </a>
            <a  data-aos="flip-up" href="https://www.youtube.com/channel/UCalluXk_8NlP5xxj4cHcMJA" class="btn btn-icon btn-secondary btn-youtube mx-2">
                <i class="bx bxl-youtube"></i>
            </a>
        </div>
        <p class="nav d-block fs-sm text-center pt-5 mt-lg-4 mb-0">
            <span class="opacity-80">&copy; 2022 - <?= date('Y') ?> All rights reserved. Made by </span>
            <a class="nav-link d-inline-block p-0" href="https://www.instagram.com/pdsi.kemendag/" target="_blank" rel="noopener">Pusat Data dan Sistem Informasi Kementerian Perdagangan RI</a>
        </p>
    </div>
</footer>