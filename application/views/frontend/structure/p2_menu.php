<!-- Navbar -->
<!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page -->
<header class="header navbar navbar-expand-lg bg-white navbar-sticky">
    <div class="container px-3">
        <a href="<?= base_url() ?>" class="navbar-brand pe-3">
            <img id="logomiauw" src="<?= base_url() ?>assets/brand/logo.svg" class="light-mode-img" width="200" alt="Logo Sistem Informasi Sarana Perdagangan (SISP)">
            <img id="logogukguk" src="<?= base_url() ?>assets/brand/logo-dark.svg" class="dark-mode-img" width="200" alt="Logo Sistem Informasi Sarana Perdagangan (SISP)">
        </a>
        <div id="navbarNav" class="offcanvas offcanvas-end">
            <div class="offcanvas-header border-bottom">
                <h5 class="offcanvas-title">Menu</h5>
                <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <?php if ($this->session->userdata('role') != 'Pengelola/Petugas' && $this->session->userdata('role') != 'Dinas Provinsi' && $this->session->userdata('role') != 'Dinas Kabupaten/Kota') { ?>
                        <!-- Logic Tombol Dashboard sing riweuh     -->
                        <?php if (!empty($this->session->userdata('token'))) { ?>
                            <li class="nav-item d-block d-md-none text-primary" class="nav-item"><a href="<?= site_url('web/dashboard') ?>" class="nav-link">Dashboard</a></li>
                        <?php } ?>
                        <!-- End Dashboard Logic -->
                    <?php } ?>
                    <?php
                    $menu = menu('Front end');
                    foreach ($menu['data'] as $k => $v) {
                        $url = $v['url'];

                        if ($v['menu'] == 'Sebaran Pasar' && empty($this->session->userdata('token'))) {
                            $url = '#sec_statistik';
                        }
                        echo '<li class="nav-item"><a href="' . base_url() . $url . '" class="nav-link">' . $v['menu'] . '</a></li>';
                    }

                    ?>
                    <?php if ($this->session->userdata('role') == 'Pengelola/Petugas') { ?>
                        <li class="nav-item"><a href="<?= base_url() ?>web/e-komplain" class="nav-link">E-Komplain <span class="badge rounded-pill bg-danger ms-1"><?= count(komplain(substr($this->session->userdata('daerah_id'), 0, 4))) ?></span></a></li>
                    <?php } else if (empty($this->session->userdata('role'))) { ?>
                        <li class="nav-item"><a href="<?= base_url() ?>e-komplain" class="nav-link">E-Komplain</a></li>
                    <?php } ?>
                    <?php if (!empty($this->session->userdata('token'))) { ?>

                        <li class="nav-item dropdown d-block d-lg-none">
                            <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Akun & Lainnya</a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= base_url() ?>bantuan" class="dropdown-item"><i class='bx bx-help-circle me-1'></i>Pusat Bantuan</a></li>
                                <?php
                                if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) != strtolower('Dinas Provinsi') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {

                                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') {
                                        $file = 'Panduan Pengelola Pasar SISP v1.0.pdf';
                                    } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) != strtolower('Dinas Provinsi')) {
                                        $file = 'Panduan Dinas Prov Kab Kota v1.0.pdf';
                                    } else if (strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                                        $file = 'Panduan Kontributor SP2KP-rev-v1.0.pdf';
                                    } ?>
                                    <li><a href="<?= base_url() ?>assets/panduan/<?= $file ?>" target="_blank" class="dropdown-item"><i class='bx bx-download me-1'></i>Panduan</a>
                                    <?php }  ?>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <!-- Button on Mobile -->
            <?php if (!empty($this->session->userdata('token'))) { ?>
                <div class="offcanvas-header">
                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting') : ?>
                        <a href="<?= site_url('user/sign_out') ?>" class="btn btn-danger w-100" rel="noopener">
                            <i class='bx bx-log-out-circle fs-4 lh-1 me-1'></i>
                            &nbsp;Kembali ke Intranet
                        </a>
                    <?php else : ?>
                        <a href="<?= site_url('user/sign_out') ?>" class="btn btn-danger w-100" rel="noopener">
                            <i class='bx bx-log-out-circle fs-4 lh-1 me-1'></i>
                            &nbsp;Keluar Akun
                        </a>
                    <?php endif; ?>
                </div>
            <?php } else { ?>
                <div class="offcanvas-header border-top">
                    <a href="<?= base_url() ?>user/sign_in" class="btn btn-primary w-100" rel="noopener">
                        <i class='bx bx-log-in-circle  fs-4 lh-1 me-1'></i>
                        &nbsp;Masuk Akun
                    </a>
                </div>
            <?php } ?>
            <!-- End Button on Mobile -->
        </div>
        <div class="form-check form-switch mode-switch pe-lg-1 ms-auto me-4" data-bs-toggle="mode">
            <input type="checkbox" class="form-check-input" id="theme-mode">
            <label class="form-check-label d-none d-sm-block" for="theme-mode"><i class='bx bx-sun'></i></label>
            <label class="form-check-label d-none d-sm-block" for="theme-mode"><i class='bx bx-moon'></i></label>
        </div>
        <button type="button" class="navbar-toggler" data-bs-toggle="offcanvas" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Button on Desktop -->
        <?php if (!empty($this->session->userdata('token'))) { ?>
            <!-- Logic Tombol Dashboard sing riweuh     -->
            <a href="<?= site_url('web/dashboard') ?>" class="btn btn-outline-primary btn-sm fs-sm rounded d-none d-lg-inline-flex me-1" rel="noopener" style="padding-top: 10px; padding-bottom: 10px;">
                <i class="bx bx-grid-alt fs-5 lh-1 me-1"></i>CMS
            </a>
            <!-- End Dashboard Logic -->
            <div class="btn-group dropdown  btn-sm fs-sm rounded d-none d-lg-inline-flex">
                <button type="button" class="btn btn-primary dropdown-toggle rounded" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?php
                    $nama = explode(' ', $this->session->userdata('nama'));
                    echo $nama[0];
                    ?>
                </button>
                <div class="dropdown-menu dropdown-menu-end my-1">
                    <a href="<?= base_url() ?>bantuan" class="dropdown-item"><i class='bx bx-help-circle me-1'></i>Pusat Bantuan</a>
                    <?php
                    if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) != strtolower('Dinas Provinsi') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {

                        if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') {
                            $file = 'Panduan Pengelola Pasar SISP v1.0.pdf';
                        } else if (strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) != strtolower('Dinas Provinsi')) {
                            $file = 'Panduan Dinas Prov Kab Kota v1.0.pdf';
                        } else if (strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) != strtolower('Kontributor SP2KP (Kabupaten/Kota)')) {
                            $file = 'Panduan Kontributor SP2KP-rev-v1.0.pdf';
                        }

                    ?>
                        <a href="<?= base_url() ?>assets/panduan/<?= $file ?>" target="_blank" class="dropdown-item"><i class='bx bx-book me-1'></i>Panduan</a>
                    <?php }  ?>
                    <div class="dropdown-divider"></div>
                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting') : ?>
                        <a href="<?= site_url('user/sign_out') ?>" class="dropdown-item text-danger"><i class='bx bx-log-out-circle me-1'></i>Kembali ke Intranet</a>
                    <?php else : ?>
                        <a href="<?= site_url('user/sign_out') ?>" class="dropdown-item text-danger"><i class='bx bx-log-out-circle me-1'></i>Keluar Akun</a>
                    <?php endif; ?>
                </div>
            </div>
        <?php } else { ?>
            <a href="<?= base_url() ?>user/sign_in" class="btn btn-outline-primary btn-rounded btn-sm fs-sm rounded d-none d-lg-inline-flex" rel="noopener">
                <i class="bx bx-log-in-circle fs-5 lh-1 me-1"></i>
                &nbsp;Masuk
            </a>
        <?php } ?>
        <!-- End Button on Desktop -->
    </div>
</header>