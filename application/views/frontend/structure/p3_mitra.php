<!-- Brands (Carousel) -->
<section class="container mt-2 pt-3 pt-lg-5 pb-5">
    <div class="swiper mx-n2" data-swiper-options='{
            "slidesPerView": 2,
            "pagination": {
                "el": ".swiper-pagination",
                "clickable": true
            },
            "breakpoints": {
                "500": {
                "slidesPerView": 3,
                "spaceBetween": 8
                },
                "650": {
                "slidesPerView": 4,
                "spaceBetween": 8
                },
                "900": {
                "slidesPerView": 5,
                "spaceBetween": 8
                },
                "1100": {
                "slidesPerView": 5,
                "spaceBetween": 8
                }
            }
        }'>
        <div class="swiper-wrapper">

            <!-- Item -->
            <div class="swiper-slide py-1" data-aos="flip-up">
                <a href="https://www.kemendag.go.id/" target="_blank" class="card card-body card-hover px-2 mx-2">
                    <img src="<?= base_url() ?>assets/frontend/img/brands/app-kemendag.svg" class="d-block mx-auto my-2" width="154" alt="Brand">
                </a>
            </div>

            <!-- Item -->
            <div class="swiper-slide py-1" data-aos="flip-up">
                <a href="https://hero.kemendag.go.id/" target="_blank" class="card card-body card-hover px-2 mx-2">
                    <img src="<?= base_url() ?>assets/frontend/img/brands/app-hero.svg" class="d-block mx-auto my-2" width="154" alt="Brand">
                </a>
            </div>

            <!-- Item -->
            <div class="swiper-slide py-1" data-aos="flip-up">
                <a href="https://sp2kp.kemendag.go.id/" target="_blank" class="card card-body card-hover px-2 mx-2">
                    <img src="<?= base_url() ?>assets/frontend/img/brands/app-sp2kp.svg" class="d-block mx-auto my-2" width="154" alt="Brand">
                </a>
            </div>

            <!-- Item -->
            <div class="swiper-slide py-1" data-aos="flip-up">
                <a href="https://satudata.kemendag.go.id/" target="_blank" class="card card-body card-hover px-2 mx-2">
                    <img src="<?= base_url() ?>assets/frontend/img/brands/app-satudata.svg" class="d-block mx-auto my-2" width="154" alt="Brand">
                </a>
            </div>

            <!-- Item -->
            <div class="swiper-slide py-1" data-aos="flip-up">
                <a href="https://kudagang.kemendag.go.id/" target="_blank" class="card card-body card-hover px-2 mx-2">
                    <img src="<?= base_url() ?>assets/frontend/img/brands/app-kudagang.svg" class="d-block mx-auto my-2" width="154" alt="Brand">
                </a>
            </div>
        </div>

        <!-- Pagination (bullets) -->
        <div class="swiper-pagination position-relative pt-2 mt-4"></div>
    </div>
</section>


<!-- Download app CTA -->
<section class=" position-relative pt-lg-4 pt-xl-5 overflow-hidden">
    <div class="position-absolute top-0 start-0 w-100 h-100 bg-light"></div>
    <div class="container position-relative zindex-3 pt-5">
        <div class="row pt-2 pt-sm-3 pt-md-4">
            <div class="col-md-6 col-xl-5 col-xxl-4 text-center text-md-start pb-4 pb-sm-5 mb-2 mb-md-3 mb-lg-4 mb-xl-5">
                <h2 class="h1 mb-5" data-aos="fade-up">Unduh <span class="text-primary">SISP Mobile</span> Sekarang</h2>
                <div class="d-flex d-md-block mt-n2 mt-sm-0">
                    <div class="row row-cols-1 row-cols-lg-2 align-items-lg-end me-1 me-md-0 pb-md-4 mb-md-3">
                        <div class="col" data-aos="fade-up">
                            <h3 class="fs-base fw-normal opacity-60 mb-2">App Store</h3>
                            <div class="text-nowrap text-warning pb-1 mb-2">
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                            </div>
                            <h4 class="mb-1">Apple iOS</h4>
                            <p class="mb-0">rating 4.7+</p>
                        </div>
                        <div class="col d-xl-flex justify-content-end" data-aos="fade-up">
                            <a href="https://apps.apple.com/au/app/sisp-kemendag/id1610988214" target="_blank" class="btn btn-dark btn-lg w-xl-100 px-3 py-2 ms-xl-3 mt-3 mt-lg-0">
                                <img src="<?= base_url() ?>assets/frontend/img/market/appstore-light.svg" class="light-mode-img" width="124" alt="App Store">
                                <img src="<?= base_url() ?>assets/frontend/img/market/appstore-dark.svg" class="dark-mode-img" width="124" alt="App Store">
                            </a>
                        </div>
                    </div>
                    <div class="row row-cols-1 row-cols-lg-2 align-items-lg-end">
                        <div class="col" data-aos="fade-up">
                            <h3 class="fs-base fw-normal opacity-60 mb-2">Google Play</h3>
                            <div class="text-nowrap text-warning pb-1 mb-2">
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                                <i class="bx bxs-star"></i>
                            </div>
                            <h4 class="mb-1">Google Android</h4>
                            <p class="mb-0">rating 4.6+</p>
                        </div>
                        <div class="col d-xl-flex justify-content-end" data-aos="fade-up">
                            <a href="https://play.google.com/store/apps/details?id=com.kemendag.sisp" target="_blank" class="btn btn-dark btn-lg w-xl-100 px-3 py-2 ms-xl-3 mt-3 mt-lg-0">
                                <img src="<?= base_url() ?>assets/frontend/img/market/googleplay-light.svg" class="light-mode-img" width="139" alt="Google Play">
                                <img src="<?= base_url() ?>assets/frontend/img/market/googleplay-dark.svg" class="dark-mode-img" width="139" alt="Google Play">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-7 col-xxl-8">
                <img src="<?= base_url() ?>assets/frontend/img/sisp-mobile.png" width="618" class="rellax d-block mx-auto" alt="SISP Mobile Illustration" data-rellax-percentage="0.5" data-rellax-speed="1" data-disable-parallax-down="lg">
                <div class="d-none d-xl-block" style="margin-bottom: -450px;"></div>
                <div class="d-none d-lg-block d-xl-none" style="margin-bottom: -800px;"></div>
                <div class="d-none d-sm-block d-lg-none" style="margin-bottom: -400px;"></div>
                <div class="d-sm-none" style="margin-bottom: -240px;"></div>
            </div>
        </div>
    </div>
</section>