<!DOCTYPE html>
<html lang="id">

<!-- Sistem Informasi Sarana Perdagangan (SISP)
Developed by Pusat Data & Sistem Informasi
Kementerian Perdagangan RI -->

<head>
    <!-- Meta Tag & SEO -->
    <?php $this->load->view('frontend/structure/p1_head'); ?>
</head>
<body>
    <div class="page-loading active">
        <div class="page-loading-inner">
            <div class="page-spinner"></div><span>Loading...</span>
        </div>
    </div>
    <main class="page-wrapper">

        <!-- Header -->
        <?php $this->load->view('frontend/structure/p2_menu'); ?>

        <!-- Konten -->
        <?php $this->load->view('frontend/contents/' . $namafile); ?>

        <?php if ($menu != 'signin' && $menu != 'resetpassword') { ?>
            <!-- Partner -->
            <?php $this->load->view('frontend/structure/p3_mitra'); ?>
        <?php } ?>
    </main>
    <?php if ($menu != 'signin' && $menu != 'resetpassword') { ?>
        <!-- Footer -->
        <?php $this->load->view('frontend/structure/p4_footer'); ?>
    <?php } ?>

    <a href="#top" class="btn-scroll-top" data-scroll>
        <span class="btn-scroll-top-tooltip text-muted fs-sm me-1">Top</span>
        <i class="btn-scroll-top-icon bx bx-chevron-up"></i>
    </a>

    <!-- Hero Button di Halaman Login -->
    <?php if ($menu == 'signin' || $menu == 'resetpassword' || $menu == 'register') { ?>
        <a href="https://hero.kemendag.go.id/create-ticket/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.Ijci.yMedw4R3dGsxCMOlKS0nZLVGWVTbqH-SOivRlcn9lfIuF6lD-L6iEj3TlFXrYBbt943SpC5OWJBavm20oZ1O0w/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.IjU0Ig.frTpbVf1gdtOth2VJzoO841VXnuduvoasm02A38sE9Jq1ygQPgs4jdq1hLrmPhq-i4uboL7aWit_cEQ3WLF4hA" class="floating-whatsapp" id="floating-whatsapp">
            <img class=" floating-whatsapp__icon" width="70" height="70" src="<?= base_url() ?>assets/frontend/img/hero-icon.svg" alt="">
        </a>
        <span class="floating-whatsapp__text">
            <i class="floating-whatsapp__icon floating-whatsapp__label">Help Center <span class="fw-bold"> HERO</span></i>
        </span>
    <?php } ?>

    <!-- JS -->
    <?php $this->load->view('frontend/structure/p5_js'); ?>
</body>

</html>