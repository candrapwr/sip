<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="title" content="<?= (isset($title)) ? ucwords($title) . ' | ' : ''; ?> Sistem Informasi Sarana Perdagangan (SISP)">
<meta name="description" content="Website Sistem Informasi Sarana Perdagangan (SISP) <?= (isset($title)) ? ' Halaman ' . $title : ''; ?>">
<meta name="keywords" content="pasar,dashboard,sisp,kementerian,perdagangan,kemendag<?= (isset($title)) ? ',' . $title : ''; ?>">
<meta name="author" content="Pusat Data dan Sistem Informasi Kementerian Perdagangan Republik Indonesia">
<meta name="robots" content="all,index,follow">
<meta name="language" content="Indonesia">
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Content-Language" content="id-ID">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- Judul Halaman & Favicon -->
<title><?= (isset($title)) ? ucwords($title) . ' | ' : ''; ?> Sistem Informasi Sarana Perdagangan (SISP)</title>
<link rel="shortcut icon" href="<?= base_url() ?>assets/brand/favicon.png" type="image/x-icon">

<!-- ThemeColor -->
<meta name="msapplication-TileColor" content="#E70A2B">
<meta name="msapplication-config" content="<?= base_url() ?>assets/frontend/favicon/browserconfig.xml">
<meta name="theme-color" content="#E70A2B">
<meta name="msapplication-navbutton-color" content="#E70A2B">
<meta name="apple-mobile-web-app-status-bar-style" content="#E70A2B">

<!-- Styling CSS -->
<?php if ($css) { ?>
    <link href="<?= base_url() ?>assets/frontend/css/custom/<?= $namafile ?>.css" rel="stylesheet" type="text/css" />
<?php } else { ?>
    <link href="<?= base_url() ?>assets/frontend/css/custom/index.css" rel="stylesheet" type="text/css" />
<?php } ?>


<!-- Preload JS -->
<script>
    var baseurl_hehe = "<?= base_url(); ?>";
</script>
<script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
<!-- Theme mode -->
<script>
    let mode = window.localStorage.getItem('mode'),
        root = document.getElementsByTagName('html')[0];
    if (mode !== null && mode === 'dark') {
        root.classList.add('dark-mode');
        $("#logomiauw").attr("src", baseurl_hehe + "assets/brand/logo-dark.svg");
        $("#logogukguk").attr("src", baseurl_hehe + "assets/brand/logo-dark.svg");
    } else {
        root.classList.remove('dark-mode');
        $("#logomiauw").attr("src", baseurl_hehe + "assets/brand/logo.svg");
        $("#logogukguk").attr("src", baseurl_hehe + "assets/brand/logo.svg");
    }
</script>

<!-- Page loading scripts -->
<script>
    (function() {
        window.onload = function() {
            const preloader = document.querySelector('.page-loading');
            preloader.classList.remove('active');
            setTimeout(function() {
                preloader.remove();
            }, 1000);
        };
    })();
</script>