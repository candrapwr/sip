<!-- Vendor Scripts -->
<script src="<?= base_url() ?>assets/frontend/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/smooth-scroll/dist/smooth-scroll.polyfills.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/parallax-js/dist/parallax.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/rellax/rellax.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/swiper/swiper-bundle.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/imageuploader/image-uploader.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/smartwizard/js/jquery.smartWizard.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/jpreview/jpreview.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/jpreview/bootstrap-prettyfile.js"></script>

<script src="<?= base_url() ?>assets/frontend/vendor/lightgallery/lightgallery.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/lightgallery/plugins/fullscreen/lg-fullscreen.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/lightgallery/plugins/zoom/lg-zoom.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/lightgallery/plugins/video/lg-video.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/lightgallery/plugins/thumbnail/lg-thumbnail.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.14.0/js/standalone/selectize.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>

<!-- Datatables Bootstrap 5 -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.13.2/af-2.5.2/b-2.3.4/b-colvis-2.3.4/b-html5-2.3.4/b-print-2.3.4/cr-1.6.1/date-1.3.0/fc-4.2.1/fh-3.3.1/kt-2.8.1/r-2.4.0/rg-1.3.0/rr-1.3.2/sc-2.1.0/sb-1.4.0/sp-2.1.1/sl-1.6.0/sr-1.2.1/datatables.min.js"></script>

<!-- Webicons -->
<script src="https://cdn.jsdelivr.net/npm/boxicons/dist/boxicons.min.js"></script>

<!-- Main Theme Script -->
<script src="<?= base_url() ?>assets/frontend/js/theme.min.js"></script>

<!-- Addons Custom JS -->
<?php $this->load->view('frontend/js/index'); ?>
<?php if ($js) {
    $this->load->view('frontend/js/' . $namafile);
} ?>