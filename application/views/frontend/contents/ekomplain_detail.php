<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->
    <section class="container">
        <div class="row">
            <!-- Sidebar (User info + Account menu) -->
            <aside class="col-lg-3 col-md-4 border-end pb-5 mt-n5">
                <div class="position-sticky top-0">
                    <div class="text-center mt-5">
                        <div class="d-table position-relative mx-auto mt-2 mt-lg-4 pt-5 mb-3">
                            <img src="https://ui-avatars.com/api/?name=<?= $detail_complaint['nama'] ?>&background=E70A2B&color=fff&size=512" class="d-block rounded-circle" width="120" alt="<?= $detail_complaint['nama'] ?>">
                            <button type="button" class="btn btn-icon btn-light bg-white btn-sm border rounded-circle shadow-sm position-absolute bottom-0 end-0 mt-4" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $detail_complaint['nama'] ?>">
                                <i class='bx bx-user-voice'></i>
                            </button>
                        </div>
                        <h2 class="h5 mb-1 text-primary"><?= $detail_complaint['nama'] ?></h2>
                        <h5 class="mb-3 pb-3 text-primary fw-normal">#<?= $detail_complaint['no_tiket'] ?></h5>
                        <input type="hidden" id="id" value="<?= slugify($detail_complaint['komplain'], $detail_complaint['komplain_id']) ?>">
                    </div>
                    <hr>
                    <div class="mt-3">
                        <div class="row justify-content-center">
                            <div class="col-10 col-md-12">
                                <h5>E-mail</h5>
                                <p class="mb-4"><a class="text-primary" href="mailto:<?= $detail_complaint['email'] ?>" target="_blank"><?= $detail_complaint['email'] ?></a></p>
                                <h5>Alamat</h5>
                                <p class="mb-4"><?= $detail_complaint['alamat'] ?></p>
                                <h5>Tanggal Dibuat</h5>
                                <p class="mb-4"> <?= full_date($detail_complaint['createdon']) ?></p>
                                <h5>Kontak</h5>
                                <p class="mb-4"><a class="text-primary" href="tel:<?= $detail_complaint['hp'] ?>" target="_blank"><?= $detail_complaint['hp'] ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- Account messages -->
            <div class="col-md-8 col-lg-9 pb-5 mb-lg-2 mb-lg-4 pt-md-5 mt-n3 mt-md-0">
                <div class="ps-md-3 mt-md-2 pt-md-4 pb-md-2">
                    <div class="row g-0 border rounded-3 shadow-sm position-relative overflow-hidden">
                        <!-- Chat window -->
                        <div class="col-lg-12" style="max-height: 712px;">
                            <div class="card h-100 border-0 bg-transparent pb-3">

                                <!-- Header -->
                                <div class="navbar card-header d-flex align-items-center justify-content-between w-100 p-sm-4 p-3">
                                    <div class="d-flex align-items-center pe-3">
                                        <img src="https://ui-avatars.com/api/?name=<?= $detail_complaint['nama_pasar'] ?>&background=1ACBAA&color=fff&size=128" class="rounded-circle" width="40" alt="Albert Flores">
                                        <h6 class="mb-0 px-1 mx-2">Pengelola Aduan & Komplain <?= $detail_complaint['nama_pasar'] ?></h6>
                                        <div class="bg-success rounded-circle" style="width: 8px; height: 8px;"></div>
                                    </div>
                                </div>

                                <!-- Messages -->
                                <div class="card-body swiper scrollbar-hover overflow-hidden w-100 pb-0" data-swiper-options='{
                                "direction": "vertical",
                                "slidesPerView": "auto",
                                "freeMode": true,
                                "scrollbar": {
                                    "el": ".swiper-scrollbar"
                                },
                                "mousewheel": true
                                }'>
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide h-auto">

                                            <!-- Own message -->
                                            <div class="d-flex align-items-start justify-content-end mb-3">
                                                <div class="pe-2 me-1" style="max-width: 348px;">
                                                    <div class="bg-primary text-light p-3 mb-1" style="border-top-left-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                        <?= $detail_complaint['komplain'] ?></div>
                                                    <div class="d-flex justify-content-end align-items-center fs-sm text-muted">
                                                        <?= full_date($detail_complaint['createdon']) ?>
                                                    </div>
                                                </div>
                                                <img src="https://ui-avatars.com/api/?name=<?= $detail_complaint['nama'] ?>&background=E70A2B&color=fff&size=64" class="rounded-circle" width="40" alt="Albert Flores">
                                            </div>

                                            <!-- History Balas Membalas -->
                                            <div id="tracking-list"></div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar end-0"></div>
                                </div>

                                <!-- Footer (Send message form) -->
                                <form class="default-form col-lg-12" id="form-data-komplain">
                                    <div class="card-footer d-sm-flex w-100 border-0 pt-3 pb-3 px-4">
                                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                        <input type="hidden" id="tiket" name="tiket" value="<?= $detail_complaint['no_tiket'] ?>">
                                        <input type="hidden" id="email" name="email" value="<?= $detail_complaint['email'] ?>">
                                        <input type="hidden" id="name" name="nama_pasar" value="<?= $detail_complaint['nama_pasar'] ?>">

                                        <div class="position-relative w-75 me-2 mb-3 mb-sm-0">
                                            <input type="text" autofocus class="<?= (form_error('tanggapan')) ? 'frm-error' : '' ?> form-control form-control-lg" style="padding-right: 85px;" name="tanggapan" id="tanggapan" placeholder="Masukkan Tanggapan Anda Lalu Pilih Status Open/Resolved...">
                                            <?= form_error('tanggapan', '<small class="text-danger">', '</small>') ?>
                                        </div>
                                        <div class="position-relative w-25 me-2 mb-3 mb-sm-0">
                                            <select name="status" class="form-select form-select-lg" id="status">
                                                <option value="1">OPEN</option>
                                                <option value="2">RESOLVED</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-icon btn-lg d-none d-sm-inline-flex ms-1">
                                            <i class="bx bx-send"></i>
                                        </button>
                                        <button type="submit" class="btn btn-primary btn-lg w-100 d-sm-none">
                                            <i class="bx bx-send fs-xl me-1"></i>
                                            Send
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->
    <section class="container">
        <div class="row">
            <!-- Sidebar (User info + Account menu) -->
            <aside class="col-lg-3 col-md-4 border-end pb-5 mt-n5">
                <div class="position-sticky top-0">
                    <div class="text-center pt-5">
                        <div class="d-table position-relative mx-auto mt-2 mt-lg-4 pt-5 mb-3">
                            <img src="https://ui-avatars.com/api/?name=<?= $complaint['nama'] ?>&background=E70A2B&color=fff&size=512" class="d-block rounded-circle" width="120" alt="<?= $complaint['nama'] ?>">
                            <button type="button" class="btn btn-icon btn-light bg-white btn-sm border rounded-circle shadow-sm position-absolute bottom-0 end-0 mt-4" data-bs-toggle="tooltip" data-bs-placement="bottom" title="<?= $complaint['nama'] ?>">
                                <i class='bx bx-user-voice'></i>
                            </button>
                        </div>
                        <h2 class="h5 mb-1 text-primary"><?= $complaint['nama'] ?></h2>
                        <h5 class="mb-3 pb-3 text-primary fw-normal">#<?= $complaint['no_tiket'] ?></h5>
                        <input type="hidden" id="id" value="<?= $complaint['no_tiket'] ?>">
                    </div>
                    <hr>
                    <div class="mt-3">
                        <div class="row justify-content-center">
                            <div class="col-10 col-md-12">
                                <h5>E-mail</h5>
                                <p class="mb-4"><a class="text-primary" href="mailto:<?= $complaint['email'] ?>" target="_blank"><?= $complaint['email'] ?></a></p>
                                <h5>Alamat</h5>
                                <p class="mb-4"><?= $complaint['alamat'] ?></p>
                                <h5>Tanggal Dibuat</h5>
                                <p class="mb-4"> <?= full_date($complaint['createdon']) ?></p>
                                <h5>Kontak</h5>
                                <p class="mb-4"><a class="text-primary" href="tel:<?= $complaint['hp'] ?>" target="_blank"><?= $complaint['hp'] ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- Account messages -->
            <div class="col-md-8 col-lg-9 pb-5 mb-lg-2 mb-lg-4 pt-md-5 mt-n3 mt-md-0">
                <div class="ps-md-3 mt-md-2 pt-md-4 pb-md-2">
                    <div class="row g-0 border rounded-3 shadow-sm position-relative overflow-hidden">
                        <!-- Chat window -->
                        <div class="col-lg-12" style="max-height: 712px;">
                            <div class="card h-100 border-0 bg-transparent pb-3">

                                <!-- Header -->
                                <div class="navbar card-header d-flex align-items-center justify-content-between w-100 p-sm-4 p-3">
                                    <div class="d-flex align-items-center pe-3">
                                        <img src="https://ui-avatars.com/api/?name=<?= $complaint['nama_pasar'] ?>&background=1ACBAA&color=fff&size=128" class="rounded-circle" width="40" alt="Albert Flores">
                                        <h6 class="mb-0 px-1 mx-2">Pengelola Aduan & Komplain <?= $complaint['nama_pasar'] ?></h6>
                                        <div class="bg-success rounded-circle" style="width: 8px; height: 8px;"></div>
                                    </div>
                                </div>

                                <!-- Messages -->
                                <div class="card-body swiper scrollbar-hover overflow-hidden w-100 pb-0" data-swiper-options='{
                                        "direction": "vertical",
                                        "slidesPerView": "auto",
                                        "freeMode": true,
                                        "scrollbar": {
                                            "el": ".swiper-scrollbar"
                                        },
                                        "mousewheel": true
                                        }'>
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide h-auto">

                                            <!-- Own message -->
                                            <div class="d-flex align-items-start justify-content-end mb-3">
                                                <div class="pe-2 me-1" style="max-width: 348px;">
                                                    <div class="bg-primary text-light p-3 mb-1" style="border-top-left-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                        <?= $complaint['komplain'] ?></div>
                                                    <div class="d-flex justify-content-end align-items-center fs-sm text-muted">
                                                        <?= full_date($complaint['createdon']) ?>
                                                    </div>
                                                </div>
                                                <img src="https://ui-avatars.com/api/?name=<?= $complaint['nama'] ?>&background=E70A2B&color=fff&size=64" class="rounded-circle" width="40" alt="Albert Flores">
                                            </div>

                                            <!-- History Balas Membalas -->
                                            <div id="tracking-list"></div>
                                        </div>
                                    </div>
                                    <div class="swiper-scrollbar end-0"></div>
                                </div>

                                <!-- Footer (Send message form) -->
                                <?php if ($tanggapan) : ?>
                                    <form class="default-form col-lg-12" id="form-data-complaint">
                                        <div class="card-footer d-sm-flex w-100 border-0 pt-3 pb-3 px-4">
                                            <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                            <input type="hidden" id="tiket" name="tiket" value="<?= $complaint['no_tiket'] ?>">
                                            <input type="hidden" id="email" name="email" value="<?= $complaint['email'] ?>">
                                            <input type="hidden" id="name" name="nama_pasar" value="<?= $complaint['nama_pasar'] ?>">

                                            <div class="position-relative w-100 me-2 mb-3 mb-sm-0">
                                                <input type="text" autofocus class="<?= (form_error('tanggapan')) ? 'frm-error' : '' ?> form-control form-control-lg" style="padding-right: 85px;" name="tanggapan" id="tanggapan" placeholder="Masukkan Tanggapan Anda Secara Jelas...">
                                                <?= form_error('tanggapan', '<small class="text-danger">', '</small>') ?>
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-icon btn-lg d-none d-sm-inline-flex ms-1">
                                                <i class="bx bx-send"></i>
                                            </button>
                                            <button type="submit" class="btn btn-primary btn-lg w-100 d-sm-none">
                                                <i class="bx bx-send fs-xl me-1"></i>
                                                Send
                                            </button>
                                        </div>
                                    </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>