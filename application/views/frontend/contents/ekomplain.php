<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->

    <!-- Breadcrumb -->
    <?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>
    <!-- Page content -->
    <section class="container mt-4 pt-lg-2 pb-4">

        <!-- Page title + Search form -->
        <div class="row gy-4 align-items-end mb-2 pb-md-3">
            <div class="col-lg-8 col-md-6">
                <h1 class="mb-0">Daftar <span class="text-primary"><?= (isset($title)) ? $title : ''; ?></span></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive" data-aos="fade-up">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Pasar</th>
                                <th>Nama Pengirim</th>
                                <th>Tanggal</th>
                                <th>Handphone</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($complaints as $complaint) : ?>
                                <tr>
                                    <td>
                                        <?= $complaint['nama_pasar'] ?>
                                        <!-- <span class="info"><i class="icon flaticon-map-locator"></i> Pasar Beringharjo</span> -->
                                    </td>

                                    <td><?= $complaint['nama'] ?></td>
                                    <td><?= full_date($complaint['createdon']) ?></td>
                                    <td><?= $complaint['hp'] ?></td>
                                    <td class="status"><span class="badge badge-success"><?= $complaint['status'] ?></span></td>
                                    <td>
                                        <a class="btn btn-success" href="<?= base_url() ?>dashboard/e-komplain/detail/<?= slugify($complaint['komplain'], $complaint['komplain_id']) ?>">
                                            <i class='bx bx-info-circle  fs-lg me-2' ></i>
                                            Detail & Balas
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </section>
<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->

    <!-- Hero -->
    <section class="pt-lg-4 mt-lg-3">
        <div class="position-relative overflow-hidden">
            <!-- Image -->
            <div class="container-fluid position-relative position-lg-absolute top-0 start-0 h-100">
                <div class="row h-100 me-n4 me-n2">
                    <div class="col-lg-7 position-relative">
                        <div class="d-none d-sm-block d-lg-none" style="height: 400px;"></div>
                        <div class="d-sm-none" style="height: 300px;"></div>
                        <div class="jarallax position-absolute top-0 start-0 w-100 h-100 rounded-3 rounded-start-0 overflow-hidden" data-jarallax data-speed="0.3">
                            <div class="jarallax-img" style="background-image: url('<?= base_url() ?>assets/frontend/img/banner-ekomplain.jpg');">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container position-relative zindex-5 pt-lg-5 px-0 px-lg-3">
                <div class="row pt-lg-5 mx-n4 mx-lg-n3">
                    <div class="col-xl-6 col-lg-7 offset-lg-5 offset-xl-6 pb-5">

                        <!-- Card -->
                        <div class="card bg-primary border-light overflow-hidden py-4 px-2 p-sm-4">
                            <span class="position-absolute top-0 start-0 w-100 h-100" style="background-color: rgba(255,255,255,.05);"></span>
                            <div class="card-body position-relative zindex-5">
                                <p class="fs-xl fw-bold text-primary text-uppercase mb-3">SARANA PERDAGANGAN</p>
                                <h1 class="display-5 text-light pb-3 mb-3">E-Komplain</h1>
                                <p class="fs-lg text-light opacity-70 mb-5">Masukkan <strong>Kode Tiket Anda</strong> Untuk Mengecek Status Komplain Anda. Jika Anda belum mempunyai tiket silahkan buat tiket pada form buat tiket <a href="<?= base_url() ?>e-komplain#sec_formtiket" class="text-warning">dibawah ini.</a></p>
                                <!-- Inline form -->
                                <form class="row g-3" method="post" action="<?= site_url() ?>e-komplain/search">
                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                    <div class="col-12 col-md-9">
                                        <input class="form-control form-control-lg" type="text" autocomplete="off" name="keyword" placeholder="Masukkan Kode Tiket Anda Disini...">
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <button class="btn btn-success shadow-primary btn-lg" type="submit"><i class='bx bx-search me-1'></i>Cari</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- COVID banner -->
    <section class="container py-5 mt-md-3 my-lg-5" id="sec_formtiket">
        <div class="card bg-light border-0 px-4 px-lg-0">
            <div class="row align-items-center py-3">
                <div class="col-12 col-lg-10 offset-lg-1 text-center text-md-start py-4 py-lg-5">
                    <h2 class="h1 text-primary">Form Buat Tiket E-Komplain</h2>
                    <p class="mb-4 mb-lg-5">Silahkan isikan form dibawah ini untuk melakukan request komplain.</p>
                    <form class="default-form" action="<?= site_url() ?>e-komplain" method="post">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <div class="row">
                            <div class="col-lg-4 mb-2">
                                <!-- Search Select -->
                                <div class="form-group">
                                    <label class="form-label">Provinsi</label>
                                    <select id="complaint-provinces" name="province">
                                        <option value="">- Pilih Provinsi -</option>

                                        <?php foreach ($provinces as $province) : ?>
                                            <option value="<?= $province['daerah_id'] ?>|<?= $province['provinsi'] ?>"><?= $province['provinsi'] ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                    <?= form_error('province', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <!-- Search Select -->
                                <div class="form-group">
                                    <label class="form-label">Kabupaten / Kota</label>
                                    <select id="complaint-regions" name="region">
                                        <option value="">- Pilih Kab/Kota -</option>
                                    </select>
                                    <?= form_error('region', '<small class="text-danger">', '</small>') ?>
                                    <input type="hidden" id="region-sesflash" value="<?= $this->session->flashdata('session_flashdata_kabupaten') ?>">
                                </div>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <!-- Search Select -->
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <select id="complaint-markets" name="market">
                                        <option value="">- Pilih Pasar -</option>
                                    </select>
                                    <?= form_error('market', '<small class="text-danger">', '</small>') ?>
                                    <input type="hidden" id="market-sesflash" value="<?= $this->session->flashdata('session_flashdata_pasar') ?>">
                                </div>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <div class="form-group">
                                    <label class="form-label">Nama</label>
                                    <input type="text" autocomplete="off" class="<?= (form_error('name')) ? 'frm-error' : '' ?> form-control form-control-lg" id="name" name="name" placeholder="Masukkan Nama Anda..." value="<?= set_value('name') ?>">
                                    <?= form_error('name', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <div class="form-group">
                                    <label class="form-label">E-mail</label>
                                    <input type="email" autocomplete="off" class="<?= (form_error('email')) ? 'frm-error' : '' ?> form-control form-control-lg" id="email" name="email" placeholder="Masukkan Email Anda..." value="<?= set_value('email') ?>">
                                    <?= form_error('email', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                            <div class="col-lg-4 mb-2">
                                <div class="form-group">
                                    <label class="form-label">Handphone</label>
                                    <input type="text" autocomplete="off" class="<?= (form_error('phone')) ? 'frm-error' : '' ?> form-control form-control-lg" id="phone" name="phone" placeholder="Masukkan Handphone Anda..." value="<?= set_value('phone') ?>">
                                    <?= form_error('phone', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-2">
                                <div class="form-group chosen-search">
                                    <label class="form-label">Alamat</label>
                                    <textarea class="<?= (form_error('address')) ? 'frm-error' : '' ?> form-control form-control-lg" placeholder="Masukkan Alamat Anda..." name="address" id="address"></textarea>
                                    <?= form_error('address', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                            <div class="col-lg-6 mb-2">
                                <div class="form-group chosen-search">
                                    <label class="form-label">Isi Komplain</label>
                                    <textarea class="<?= (form_error('complaint')) ? 'frm-error' : '' ?> form-control form-control-lg" placeholder="Masukkan Komplain Anda Secara Detail..." name="complaint" id="complaint"></textarea>
                                    <?= form_error('complaint', '<small class="text-danger">', '</small>') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="column col-lg-12">
                                <button type="submit" class="btn btn-lg btn-primary w-100 w-sm-auto"><i class='bx bx-save me-1'></i>Kirim Komplain</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php } ?>