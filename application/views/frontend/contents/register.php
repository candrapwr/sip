<!-- Breadcrumb -->
<?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>

<!-- Page title -->
<section class="container pb-4 mb-2 mb-lg-3">
    <h1 data-aos="fade-up"><?= $this->session->userdata('login') == 'SP2KP' ? 'Pembaruan Data Akun' : 'Form Pendaftaran' ?></h1>
    <p class="text-muted" data-aos="fade-down">Silahkan isi setiap isian pada Form Pendaftaran dibawah ini dengan tepat dan cermat. Sudah Punya Akun? Silahkan <a href="<?= base_url() ?>user/sign_in">Masuk Sekarang.</a></p>
    <?php if ($this->session->flashdata('register_success')) : ?>
        <div class="alert d-flex alert-success" role="alert">
            <i class='bx bx-check-circle lead me-3'></i>
            <div>
                <?= $this->session->flashdata('register_success'); ?>
            </div>
        </div>
    <?php elseif ($this->session->flashdata('register_error')) : ?>
        <div class="alert d-flex alert-danger" role="alert">
            <i class='bx bx-error-alt  lead me-3'></i>
            <div>
                <?= $this->session->flashdata('register_error'); ?>
            </div>
        </div>
    <?php endif; ?>
</section>
<!-- How we work (Steps) -->
<section class="container mb-md-2 mb-lg-4 mb-xl-5" id="sec_formdaftar">
    <div class="row">
        <div class="col-md-12">
            <!-- Start Wizard -->
            <form id="form-register" action="<?= site_url('register') ?>" method="post">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <input type="hidden" name="sp2kp" value="<?= $this->session->userdata('username') ?>">
                <div id="smartwizard">
                    <ul class="nav">
                        <li>
                            <a class="nav-link" href="#step-1">
                                <h4>Langkah 1</h4>
                                <p><i class='bx bx-shield-quarter me-1'></i>Tipe</p>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#step-2">
                                <h4>Langkah 2</h4>
                                <p><i class='bx bx-user-pin me-1'></i>Informasi Dasar</p>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" href="#step-3">
                                <h4>Langkah 3</h4>
                                <p><i class='bx bx-map-alt me-1'></i>Alamat Instansi</p>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                            <div class="row g-3">
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Tipe Pengguna</label>
                                        <select id="tipe-pengguna" name="tipe_pengguna" class="custom-validation">
                                            <?php if ($this->session->userdata('flex') == 'Kontributor Kab/Kota') { ?>
                                                <option value="19-Kontributor SP2KP (Kabupaten/Kota)" selected>Kontributor SP2KP (Kabupaten/Kota)</option>
                                            <?php } elseif ($this->session->userdata('flex') == 'Kontributor Provinsi') { ?>
                                                <option value="18-Kontributor SP2KP (Provinsi)" selected>Kontributor SP2KP (Provinsi)</option>
                                            <?php } else { ?>
                                                <option selected value="">-- Pilih Tipe Pengguna --</option>
                                                <?php foreach ($user_types as $user_type) : ?>
                                                    <option value="<?= $user_type['tipe_pengguna_id'] ?>-<?= $user_type['tipe_pengguna'] ?>"><?= $user_type['tipe_pengguna'] ?></option>
                                                <?php endforeach; ?>
                                            <?php } ?>

                                        </select>
                                        <?= form_error('tipe_pengguna', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6 icikiwir" style="display: none !important">
                                    <div class="form-grup validation">
                                        <label class="form-label fs-base">NIP</label>
                                        <input type="text" class="nnumber form-control form-control-lg <?= (form_error('nip')) ? 'is-invalid' : '' ?> custom-validation" name="nip" id="nip" placeholder="Masukkan NIP Anda..." autocomplete="off" value="<?= set_value('nip') ?>">
                                        <?= form_error('nip', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12 viavallen">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">NIK</label>
                                        <input type="text" class="number form-control form-control-lg <?= (form_error('nik')) ? 'is-invalid' : '' ?> custom-validation" name="nik" id="nik" placeholder="Masukkan NIK Anda..." autocomplete="off" value="<?= set_value('nik') ?>">
                                        <?= form_error('nik', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Instansi</label>
                                        <select id="dinas" name="dinas" class="custom-validation">

                                            <?php if ($this->session->userdata('flex') == 'Kontributor Kab/Kota') { ?>
                                                <option value="2" <?php echo set_select('dinas', '2'); ?>>Dinas Kabupaten/Kota</option>
                                            <?php } elseif ($this->session->userdata('flex') == 'Kontributor Provinsi') { ?>
                                                <option value="1" <?php echo set_select('dinas', '1'); ?>>Dinas Provinsi</option>
                                            <?php } else { ?>
                                                <option selected value="">-- Pilih Instansi --</option>
                                                <option value="1" <?php echo set_select('dinas', '1'); ?>>Dinas Provinsi</option>
                                                <option value="2" <?php echo set_select('dinas', '2'); ?>>Dinas Kabupaten/Kota</option>
                                                <option value="3" <?php echo set_select('dinas', '3'); ?>>Lainnya</option>
                                            <?php } ?>

                                        </select>
                                        <?= form_error('dinas', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Nama Instansi</label>
                                        <input type="text" class="form-control form-control-lg <?= (form_error('instansi')) ? 'is-invalid' : '' ?> custom-validation" name="instansi" id="instansi" placeholder="Masukkan Nama Instansi Anda..." autocomplete="off" value="<?= set_value('instansi') ?>">
                                        <?= form_error('instansi', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                            <div class="row g-3">
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Nama Lengkap</label>
                                        <input type="text" class="form-control form-control-lg <?= (form_error('name')) ? 'is-invalid' : '' ?> custom-validation" name="name" id="name" placeholder="Masukkan Nama Lengkap Anda..." autocomplete="off" value="<?= set_value('name') ?>">
                                        <?= form_error('name', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Alamat Email <small class="text-secondary">(mohon diinputkan dengan Email yang aktif, untuk dapat melakukan aktivasi akun)</small></label>
                                        <input type="text" class="form-control form-control-lg <?= (form_error('email')) ? 'is-invalid' : '' ?> custom-validation" name="email" id="email" placeholder="Masukkan Email Anda..." autocomplete="off" value="<?= set_value('email') ?>">
                                        <?= form_error('email', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base"><?= $this->session->userdata('login') == 'SP2KP' ? 'Password Baru' : 'Password' ?></label>
                                        <input type="password" class="form-control form-control-lg <?= (form_error('password')) ? 'is-invalid' : '' ?> custom-validation" name="password" id="password" placeholder="Masukkan Password Anda..." autocomplete="off">
                                        <?= form_error('password', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base"><?= $this->session->userdata('login') == 'SP2KP' ? 'Konfirmasi Password Baru' : 'Konfirmasi Password' ?></label>
                                        <input type="password" class="form-control form-control-lg <?= (form_error('password_confirm')) ? 'is-invalid' : '' ?> custom-validation" name="password_confirm" id="password_confirm" placeholder="Masukkan Kembali Password Anda..." autocomplete="off">
                                        <?= form_error('password_confirm', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Nomor Handphone</label>
                                        <input type="number" class="form-control form-control-lg <?= (form_error('phone')) ? 'is-invalid' : '' ?> custom-validation" name="phone" id="phone" placeholder="Masukkan Handphone Anda..." autocomplete="off" value="<?= set_value('phone') ?>">
                                        <?= form_error('phone', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step-3" class="tab-pane" role="tabpanel" aria-labelledby="step-3">
                            <div class="row g-3">
                                <div class="col-6 ">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Provinsi</label>
                                        <?php if ($this->session->userdata('login') == 'SP2KP') { ?>
                                            <input type="hidden" class="form-control form-control-lg custom-validation" name="provinsi" placeholder="Provinsi" autocomplete="off" value="<?= substr($this->session->userdata('pasar')[0]['daerah_id'], 0, 2) ?>">
                                            <input type="text" class="form-control form-control-lg custom-validation" placeholder="Provinsi" autocomplete="off" value="<?= $this->session->userdata('pasar')[0]['provinsi'] ?>" disabled>
                                        <?php } else { ?>
                                            <select id="provinsi" name="provinsi" class="custom-validation">
                                                <option selected value="">Pilih Provinsi</option>

                                                <?php foreach ($provinces as $province) : ?>
                                                    <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php } ?>
                                        <?= form_error('provinsi', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Kota/Kabupaten</label>
                                        <?php if ($this->session->userdata('login') == 'SP2KP' && $this->session->userdata('flex') == 'Kontributor Kab/Kota') { ?>
                                            <input type="hidden" class="form-control form-control-lg custom-validation" name="kabupaten" placeholder="Kabupaten/Kota" autocomplete="off" value="<?= substr($this->session->userdata('pasar')[0]['daerah_id'], 0, 4) ?>">
                                            <input type="text" class="form-control form-control-lg custom-validation" placeholder="kabupaten" autocomplete="off" value="<?= $this->session->userdata('pasar')[0]['kab_kota'] ?>" disabled>
                                        <?php } else { ?>
                                            <select id="kabupaten" name="kabupaten" class="custom-validation">
                                                <option selected value="">-- Pilih Kota/Kabupaten --</option>
                                            </select>
                                        <?php } ?>
                                        <?= form_error('kabupaten', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Kecamatan</label>
                                        <select id="kecamatan" name="kecamatan" class="custom-validation">
                                            <option selected value="">-- Pilih Kecamatan --</option>
                                        </select>
                                        <?= form_error('kecamatan', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Kelurahan</label>
                                        <select id="kelurahan" name="kelurahan" class="custom-validation">
                                            <option selected value="">-- Pilih Kelurahan --</option>
                                        </select>
                                        <?= form_error('kelurahan', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group validation">
                                        <label class="form-label fs-base">Alamat Dinas</label>
                                        <textarea class="form-control form-control-lg <?= (form_error('address')) ? 'is-invalid' : '' ?> custom-validation" name="address" placeholder="Masukkan Alamat Lengkap Dinas Anda..." aria-label="With textarea" autocomplete="off"></textarea>
                                        <?= form_error('address', '<small class="text-danger">', '</small>') ?>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn-lg btn-primary w-100 w-sm-auto" name="submit" id="register" type="button">Daftar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- End Wizard -->
        </div>
    </div>
</section>