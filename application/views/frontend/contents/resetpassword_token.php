<!-- Page content -->
<section class="position-relative h-100 pb-4">

    <!-- Sign in form -->
    <div class="container d-flex flex-wrap justify-content-center justify-content-xl-start h-100">
        <div class="w-100 align-self-end pt-1 pt-md-4 pb-4" style="max-width: 526px;">
            <h1 class="text-center text-xl-start" data-aos="fade-up">Reset Password</h1>
            <p class="text-center text-xl-start pb-3 mb-3" data-aos="fade-up">Sudah Ingat Password Anda? Silahkan <a href="<?= base_url() ?>user/sign_in">Masuk Sekarang.</a></p>
            <!-- Warning alert -->
            <?php if ($this->session->flashdata('alert') == 'success') { ?>
                <div class="alert d-flex alert-success" role="alert">
                    <i class='bx bx-check-circle lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('message'); ?>
                    </div>
                </div>
            <?php } else if ($this->session->flashdata('alert') == 'warning') { ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('message'); ?>
                    </div>
                </div>
            <?php } ?>

            <form class="mb-2" method="post" action="<?= site_url() ?>save_reset_password">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <input type="hidden" name="token" value="<?= $this->uri->segment(2) ?>">
                <input type="hidden" name="xyz" value="">
                <div class="recaptcha-holder"></div>

                <div class="mb-4">
                    <label for="password" class="form-label fs-base"><i class='bx bx-lock-alt me-1'></i>Password</label>
                    <div class="password-toggle">
                        <input type="password" autocomplete="off" class="form-control form-control-lg <?= (form_error('password') ? 'frm-error' : '') ?>" id="password" name="password" placeholder="Masukkan Password Baru...">
                        <label class="password-toggle-btn" aria-label="Show/hide password">
                            <input class="password-toggle-check" type="checkbox">
                            <span class="password-toggle-indicator"></span>
                        </label>
                        <?= form_error('password', '<small class="text-danger">', '</small>') ?>
                    </div>
                </div>

                <div class="mb-4">
                    <label for="konfirmasi_password" class="form-label fs-base"><i class='bx bx-lock-open-alt me-1' ></i>Konfirmasi Password</label>
                    <div class="password-toggle">
                        <input type="password" class="form-control form-control-lg <?= (form_error('konfirmasi_password') ? 'frm-error' : '') ?>" id="confirm_password" name="confirm_password" autocomplete="new-password" placeholder="Masukkan Konfirmasi Password Baru...">
                        <label class="password-toggle-btn" aria-label="Show/hide password">
                            <input class="password-toggle-check" type="checkbox">
                            <span class="password-toggle-indicator"></span>
                        </label>
                        <?= form_error('konfirmasi_password', '<small class="text-danger">', '</small>') ?>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary shadow-primary btn-lg w-100" name="log-in">Reset Sekarang<i class='bx bx-log-in-circle ms-1'></i></button>
            </form>
        </div>
        <div class="w-100 align-self-end">
            <p class="nav d-block fs-xs text-center text-xl-start pb-2 mb-0">
                &copy; 2022 - <?= date('Y') ?> All rights reserved. <br>
                <a class="nav-link d-inline-block p-0" href="https://www.instagram.com/pdsi.kemendag/" target="_blank" rel="noopener">Pusat Data dan Sistem Informasi Kementerian Perdagangan RI</a>
            </p>
        </div>
    </div>

    <!-- Background -->
    <div class="position-absolute top-0 end-0 w-50 h-100 bg-position-center bg-repeat-0 bg-size-cover d-none d-xl-block" style="background-image: url('<?= base_url() ?>assets/frontend/img/illust-resetpassword.svg');"></div>
</section>