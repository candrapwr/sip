<!-- Breadcrumb -->
<?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>

<!-- Page title -->
<h1 class="container pb-4 mt-n1 mt-lg-0" data-aos="fade-up"><?= (isset($title)) ? $title : ''; ?> </h1>


<!-- Service -->
<section class="bg-secondary container mb-md-3 mb-lg-5 p-5">
    <div class="row">
        <div class="col-md-6 pb-2 pb-md-0 mb-4 mb-md-0">
            <div class="pe-lg-5">
                <img src="<?= base_url() ?>assets/frontend/img/banner-pujle.png" class="rounded-3" alt="Mendag Meninjau <?= (isset($title)) ? $title : ''; ?>">
            </div>
        </div>
        <div class="col-md-6">
            <h2 class="h3 mb-sm-4 text-primary">Filter Pencarian</h2>
            <p class="d-md-none d-xl-block pb-2 pb-md-3 mb-3">Temukan Informasi <?= (isset($title)) ? $title : ''; ?> melalui filter pencarian berikut untuk menampilkan sebaran pada peta yang tersedia dibawah ini.</p>
            <form method="post" action="<?= site_url() ?>migor-14rb" class="mb-4 mb-lg-5">
                <div class="row gy-3">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <!-- Form Group -->
                    <div class="col-md-6">
                        <select name="provinsi" id="provinsi">
                            <option value="">Provinsi</option>

                            <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                <option value="<?= $provinsi['daerah_id'] ?>" <?= ($provinsi['daerah_id'] == $this->session->flashdata('filter_search_provinsi')) ? 'selected' : '' ?>><?= $provinsi['provinsi'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <select name="kabupaten" id="kabupaten">
                            <option value="">Kabupaten/Kota</option>
                        </select>
                        <input type="hidden" id="filter-search-kabupaten-flash" value="<?= $this->session->flashdata('filter_search_kabupaten') ?>">
                    </div>

                    <!-- Form Group -->
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success mb-3 mb-sm-0 me-sm-3"><i class="bx bx-search me-1"></i>Cari Pasar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center my-4">
        <div class="col-md-12">
            <div id="map">
            </div>
        </div>
    </div>
    <?php
    if (!empty($prov_isi) || !empty($kab_isi)) {
    ?>
        <div class="row">
            <!-- Content Column -->
            <div class="col-md-12">
                <div class="table-responsive" data-aos="fade-up">
                    <table class="table" id="tbl_pujle" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Toko</th>
                                <th>Alamat</th>
                                <th>Penanggung Jawab</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($maps_pasar as $key => $map) {
                                echo ' <tr>
                                                                <td>' . $no . '</th>
                                                                <td>' . ucwords(strtolower($map['nama_pengecer'])) . '</td>
                                                                <td>' . ucwords(strtolower($map['alamat_pengecer'] . ', ' . $map['kab_kota'] . ', ' . $map['provinsi'])) . '</td>
                                                                <td>' . ucwords(strtolower($map['nama_penanggung_jawab'])) . '</td>
                                                            </tr>';
                                $no++;
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php }
    ?>
</section>