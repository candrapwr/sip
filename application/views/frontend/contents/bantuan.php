<!-- Breadcrumb -->
<?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>

<!-- Page title -->
<section class="container pb-5 mb-md-2 mb-lg-4 mb-xl-5">
    <div class="row pb-5 mb-md-2 mb-lg-4 mb-xl-5">
        <div class="col-lg-6">
            <h1 class="display-2 mb-0" data-aos="fade-up">Help Center<br><span class="text-primary">HERO</span></h1>
        </div>
        <div class="col-lg-6 col-xl-5 offset-xl-1 pt-3 pt-sm-4 pt-lg-3">
            <p class="fs-xl pb-4 mb-1 mb-md-2 mb-xl-3" data-aos="fade-down">Butuh Bantuan Terkait SISP? Silahkan kirim keluhan / aduan anda melalui form Help Center (HERO) dibawah ini. Anda akan mendapatkan tiket keluhan anda dan gunakan tiket tersebut untuk melihat proses aduan anda.</p>
            <a data-aos="fade-down" href="https://hero.kemendag.go.id/create-ticket/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.Ijci.yMedw4R3dGsxCMOlKS0nZLVGWVTbqH-SOivRlcn9lfIuF6lD-L6iEj3TlFXrYBbt943SpC5OWJBavm20oZ1O0w/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.IjU0Ig.frTpbVf1gdtOth2VJzoO841VXnuduvoasm02A38sE9Jq1ygQPgs4jdq1hLrmPhq-i4uboL7aWit_cEQ3WLF4hA" target="_blank" class="btn btn-lg btn-primary shadow-primary">Meminta Bantuan Sekarang<i class='bx bx-chevron-right ms-1'></i></a>
        </div>
    </div>
    <hr>
</section>
<!-- What we offer -->
<section class="overflow-hidden pt-md-2 pt-lg-3 pb-3 pb-md-4 pb-xl-5">
    <div class="container">
        <div class="d-flex flex-column flex-md-row flex-md-nowrap align-items-start">
            <div class="col-md-7 ps-0 pe-lg-4 mb-5 mb-md-0">
                <div style="max-width: 660px;">
                    <h2 class="h1 pb-3 mb-2 mb-md-3" data-aos="fade-up">Masalah Terkait</h2>
                    <div class="row row-cols-1 row-cols-sm-2 gx-lg-5 g-4">
                        <div class="col pt-1 pt-sm-2 pt-lg-3">
                            <i class="bx bx-globe d-block fs-2 text-primary mb-2 mb-sm-3"></i>
                            <h3 class="h5 pb-sm-1 mb-2" data-aos="fade-up">Web SISP</h3>
                            <p class="mb-0" data-aos="fade-up">Anda dapat mengajukan aduan atau keluhan seputar permasalahan yang ada pada Web SISP.</p>
                        </div>
                        <div class="col pt-1 pt-sm-2 pt-lg-3">
                            <i class="bx bxl-android d-block fs-2 text-primary mb-2 mb-sm-3"></i>
                            <h3 class="h5 pb-sm-1 mb-2" data-aos="fade-up">Aplikasi Mobile SISP</h3>
                            <p class="mb-0" data-aos="fade-up">Anda dapat mengajukan aduan atau keluhan seputar permasalahan yang ada pada Aplikasi Mobile iOS dan Android SISP.</p>
                        </div>
                        <div class="col pt-1 pt-sm-2 pt-lg-3">
                            <i class="bx bx-bug d-block fs-2 text-primary mb-2 mb-sm-3"></i>
                            <h3 class="h5 pb-sm-1 mb-2" data-aos="fade-up">Bug dan Saran</h3>
                            <p class="mb-0" data-aos="fade-up">Anda dapat mengirimkan saran atau bug (masalah) agar SISP dalam hal masa depan lebih baik dan optimal.</p>
                        </div>
                        <div class="col pt-1 pt-sm-2 pt-lg-3">
                            <i class="bx bx-user d-block fs-2 text-primary mb-2 mb-sm-3"></i>
                            <h3 class="h5 pb-sm-1 mb-2" data-aos="fade-up">Data, Akses, dan Akun</h3>
                            <p class="mb-0" data-aos="fade-up">Jika anda merasa ada data SISP yang salah atau tidak sesuai silahkan kirimkan Aduan Anda atau Jika anda memiliki masalah seputar akun di SISP segera kirimkan aduan anda ke link tertaut diatas.</p>
                        </div>
                    </div>
                </div>
            </div>
            <img data-aos="fade-down" src="<?= base_url() ?>assets/frontend/img/bantuan-hero.svg" width="836" class="ms-3 ms-sm-5 ms-xl-3" alt="Dashboard">
        </div>
    </div>
</section>