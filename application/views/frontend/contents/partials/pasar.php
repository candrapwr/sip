<?php if ($pasar != null) { ?>
    <!-- Courses grid -->
    <div class="row align-items-center pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
        <div class="col-lg-12 text-center text-md-start">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 gx-3 gx-md-4 mt-n2 mt-sm-0">
                <?php foreach ($pasar[$page]['data'] as $key => $p) : ?>
                    <!-- Item -->
                    <div class="col pb-1 pb-lg-3 mb-4">
                        <article class="card h-100 border-0 shadow-sm">
                            <div class="position-relative">
                                <a href="<?= base_url() ?>dashboard/pasar/detail/<?= $p['pasar_id'] ?>-<?= str_replace(' ', '-', strtolower($p['nama'])) ?>.html" class="d-block position-absolute w-100 top-0 start-0"></a>
                                <span class="badge bg-success position-absolute top-0 start-0 zindex-2 mt-3 ms-3"><?= $p['provinsi'] ?></span>
                                <a href="#" class="btn btn-icon btn-light bg-white border-white btn-sm rounded-circle position-absolute top-0 end-0 zindex-2 me-3 mt-3" data-bs-toggle="tooltip" data-bs-placement="left" title="<?= $p['nama'] ?>">
                                    <i class='bx bx-store-alt'></i>
                                </a>
                                <img src="<?= ($p['foto_depan'] != null) ? $p['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" class="card-img-top" alt="<?= $p['nama'] ?>" style="height:200px; object-fit:cover;">
                            </div>
                            <div class="card-body pb-3">
                                <h3 class="h5 mb-2">
                                    <a href="<?= base_url() ?>dashboard/pasar/detail/<?= $p['pasar_id'] ?>-<?= str_replace(' ', '-', strtolower($p['nama'])) ?>.html"><?= $p['nama'] ?></a>
                                </h3>
                                <p class="fs-sm mb-2"><?= $p['alamat'] . ', ' .  $p['kab_kota']  ?></p>
                            </div>
                            <div class="card-footer d-flex align-items-center fs-sm text-muted py-4">
                                <div class="d-flex align-items-center me-4">
                                    <i class='bx bx-envelope fs-xl me-1'></i>
                                    Kode Pos <?= $p['kode_pos'] ?>
                                </div>
                            </div>
                        </article>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12" id="ajax_links">
            <?= str_replace("#?page=", "javascript:getLoadPasar(", str_replace('" class="page-link"', ');" class="page-link"', $pagination)) ?>
        </div>
    </div>
<?php } else { ?>
    <div class="row row-cols-1 row-cols-sm-2 row-cols-lg-3 gx-3 gx-md-4 mt-n2 mt-sm-0">
        <div class="col-12 text-center">
            <img class="mb-4" src="<?= base_url() ?>assets/frontend/img/default-nodata.svg" style="width:35%" alt="" data-aos="fade-up">
            <h4 data-aos="fade-up">Pasar Tidak Ditemukan</h4>
        </div>
    </div>
<?php } ?>