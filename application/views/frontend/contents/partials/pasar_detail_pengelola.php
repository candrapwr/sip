<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button class="btn btn-warning float-end" onclick="addPengelola()"><i class='bx bxs-edit me-1'></i>Edit Pengelola</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Data Pengelola</h3>
<p class="pb-4 mb-3"> Silahkan edit dan kelola data pengelola pada bagian ini.</p>



<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive border-top mb-0">
                <table class="table align-middle">
                    <tbody>
                        <tr>
                            <th scope="row" class="ps-0 h5">Klasifikasi Pengelola</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1">
                                    <?php
                                    if ($pasar_pengelola != null && $pasar_pengelola[0]['klasifikasi_pengelola'] != null) {
                                        echo $pasar_pengelola[0]['klasifikasi_pengelola'];
                                    } else {
                                        if ($detail_pasar != null && $detail_pasar[0]['kepemilikan'] != null) {
                                            echo $detail_pasar[0]['kepemilikan'];
                                        } else {
                                            echo '-';
                                        }
                                    }
                                    ?>
                                </span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Nama Kepala pengelola</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['nama_pengelola'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">No. Telp Kepala pengelola</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_telp_pengelola'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Tingkat Pendidikan</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['tingkat_pendidikan'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Jumlah Staf Pengelola</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['jumlah_staf'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Struktur Pengelola</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['struktur_pengelola'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Nama Koperasi</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['nama_koperasi'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">No Telp. Ketua Koperasi </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_telp_koperasi'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Jumlah Anggota Koperasi </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['jumlah_anggota_koperasi'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">No. Badan Hukum Koperasi </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_badan_hukum_koperasi'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Nomor Induk Koperasi </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_induk_koperasi'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Besaran Retribusi Kios </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_kios'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Besaran Retribusi Los </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_los'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Besaran Retribusi Dasaran </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_dasaran'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Pendapatan Tahunan Retribusi </th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['pendapatan_tahunan_retribusi'] : '-' ?></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_pasar_pengelola">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Pengelola Pasar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-pasar-pengelola" enctype="multipart/form-data" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_pengelola_id" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['pasar_pengelola_id'] : '' ?>" id="pasar_pengelola_id">
                        <div class="row gy-3">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Klasifikasi Pengelola</label>
                                    <select class="form-select" aria-label="Klasifikasi Pengelola" id="klasifikasi_pengelola" name="klasifikasi_pengelola" required>
                                        <option value="">- Pilih Klasifikasi Pengelola -</option>
                                        <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                        <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                        <option value="Swasta">Swasta</option>
                                        <option value="BUMN">BUMN</option>
                                        <option value="BUMD">BUMD</option>
                                        <option value="Perorangan">Perorangan</option>
                                        <option value="Desa Adat">Desa Adat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Nama Kepala pengelola</label>
                                    <input type="text" class="form-control my-form-control" id="nama_pengelola" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['nama_pengelola'] : '' ?>" name="nama_pengelola" id="nama_pengelola" placeholder="Nama Kepala pengelola">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">No. Telp Kepala pengelola (No. Whatsapp Aktif)</label>
                                    <input type="number" class="form-control my-form-control" id="no_telp_pengelola" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_telp_pengelola'] : '' ?>" name="no_telp_pengelola" id="no_telp_pengelola" placeholder="No. Telp Kepala pengelola">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Tingkat Pendidikan</label>
                                    <select class="form-select" id="tingkat_pendidikan" name="tingkat_pendidikan" required>
                                        <option value="">- Pilih Tingkat Pendidikan -</option>
                                        <option value="SD">SD</option>
                                        <option value="SMP">SMP</option>
                                        <option value="SMA/SMK">SMA/SMK</option>
                                        <option value="D3">D3</option>
                                        <option value="S1">S1</option>
                                        <option value="S2">S2</option>
                                        <option value="S3">S3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah Staf Pengelola</label>
                                    <input type="number" class="form-control my-form-control" id="jumlah_staf" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['jumlah_staf'] : '' ?>" name="jumlah_staf" id="jumlah_staf" placeholder="Jumlah Staf Pengelola">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Struktur Pengelola</label>
                                    <input type="file" class="form-control my-form-control" name="struktur_pengelola" id="struktur_pengelola" placeholder="Struktur Pengelola">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Nama Koperasi</label>
                                    <input type="text" class="form-control my-form-control" id="nama_koperasi" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['nama_koperasi'] : '' ?>" name="nama_koperasi" id="nama_koperasi" placeholder="Nama Koperasi">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Kategori Koperasi</label>
                                    <select class="form-select" aria-label="Kategori Koperasi" id="kategori_koperasi" name="kategori_koperasi" required>
                                        <option value="">- Pilih Kategori Koperasi -</option>
                                        <option value="Pengelola">Pengelola</option>
                                        <option value="Koperasi Pasar Rakyat">Koperasi Pasar Rakyat</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">No Telp. Ketua Koperasi (No. Whatsapp Aktif)</label>
                                    <input type="number" class="form-control my-form-control" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_telp_koperasi'] : '' ?>" name="no_telp_koperasi" id="no_telp_koperasi" placeholder="No Telp. Ketua Koperasi ">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah Anggota Koperasi</label>
                                    <input type="number" class="form-control my-form-control" id="jumlah_anggota_koperasi" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['jumlah_anggota_koperasi'] : '' ?>" name="jumlah_anggota_koperasi" id="jumlah_anggota_koperasi" placeholder="Jumlah Anggota Koperasi">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">No. Badan Hukum Koperasi</label>
                                    <input type="text" class="form-control my-form-control" id="no_badan_hukum_koperasi" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_badan_hukum_koperasi'] : '' ?>" name="no_badan_hukum_koperasi" id="no_badan_hukum_koperasi" placeholder="No. Badan Hukum Koperasi ">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Nomor Induk Koperasi </label>
                                    <input type="text" class="form-control my-form-control" id="no_induk_koperasi" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['no_induk_koperasi'] : '' ?>" name="no_induk_koperasi" id="no_induk_koperasi" placeholder="Nomor Induk Koperasi">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Besaran Retribusi Kios</label>
                                    <input type="number" class="form-control price my-form-control" id="besaran_retribusi_kios" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_kios'] : '' ?>" name="besaran_retribusi_kios" id="besaran_retribusi_kios" placeholder="Besaran Retribusi Kios">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Besaran Retribusi Los</label>
                                    <input type="number" class="form-control price my-form-control" id="besaran_retribusi_los" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_los'] : '' ?>" name="besaran_retribusi_los" id="besaran_retribusi_los" placeholder="Besaran Retribusi Los">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Besaran Retribusi Dasaran</label>
                                    <input type="number" class="form-control price my-form-control" id="besaran_retribusi_dasaran" autocomplete="off" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['besaran_retribusi_dasaran'] : '' ?>" name="besaran_retribusi_dasaran" id="besaran_retribusi_dasaran" placeholder="Besaran Retribusi Dasaran">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Pendapatan Tahunan Retribusi</label>
                                    <input type="number" class="form-control price my-form-control" id="pendapatan_tahunan_retribusi" value="<?= ($pasar_pengelola != null) ? $pasar_pengelola[0]['pendapatan_tahunan_retribusi'] : '' ?>" autocomplete="off" name="pendapatan_tahunan_retribusi" placeholder="Pendapatan Tahunan Retribusi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>