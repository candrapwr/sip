<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button class="btn btn-warning float-end" onclick="addPaguyuban()"><i class='bx bxs-edit me-1'></i>Edit Paguyuban</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Data Paguyuban</h3>
<p class="pb-4 mb-3"> Silahkan edit dan kelola data paguyuban pada bagian ini.</p>



<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive border-top mb-0">
                <table class="table align-middle">
                    <tbody>
                        <tr>
                            <th scope="row" class="ps-0 h5">Nama Paguyuban</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['nama_paguyuban'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Nama Ketua</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['nama_ketua'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">No. Telp Ketua Paguyuban</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['no_telp_ketua'] : '-' ?></span></td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">Asosiasi Pedagang Pasar</th>
                            <td class="pe-0"><span class="d-inline-block my-4 py-1"><?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['asosiasi_perdagangan_pasar'] : '-' ?></span></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_pasar_paguyuban">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Paguyuban Pasar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-paguyuban" enctype="multipart/form-data" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_paguyuban_id" value="<?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['pasar_paguyuban_id'] : '' ?>" id="pasar_paguyuban_id">
                        <div class="row gy-3">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Nama Paguyuban</label>
                                    <input type="text" class="form-control" id="nama_paguyuban" value="<?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['nama_paguyuban'] : '' ?>" name="nama_paguyuban" placeholder="Nama Paguyuban">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Nama Ketua</label>
                                    <input type="text" class="form-control" id="nama_ketua" value="<?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['nama_ketua'] : '' ?>" name="nama_ketua" placeholder="Nama Ketua Paguyuban">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">No. Telp Ketua Paguyuban</label>
                                    <input type="number" class="form-control" id="no_telp_ketua" autocomplete="off" value="<?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['no_telp_ketua'] : '' ?>" name="no_telp_ketua" placeholder="No. Telp Kepala Paguyuban">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Asosiasi Pedagang Pasar</label>
                                    <select class="form-select" id="asosiasi_perdagangan_pasar" name="asosiasi_perdagangan_pasar" required>
                                        <option value="">- Pilih Asosiasi Pedagang Pasar -</option>
                                        <option value="APPSI">APPSI</option>
                                        <option value="APPSINDO">APPSINDO</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>