<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
        <button onclick="addPasokan_bapok()" class="btn btn-primary float-end" id="btn_filter2"><i class="bx bx-plus me-1"></i>Tambah</button>
    <?php endif; ?>

    <h3 class="pb-md-2 pb-lg-3 text-primary">Data Permintaan Pasokan Bahan Pokok</h3>
    <p class="pb-4 mb-3" id="title-date-harga"></p>
<?php endif; ?>


<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_pasokan_bapok" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_pasokan_bapok">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Pasukan Bahan Pokok</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-pasokan-bapok" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="pasar_pasokan_bapok_id" id="pasar_pasokan_bapok_id">
                        <div class="row gy-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" id="name" name="name" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Bulan</label>
                                    <select class="form-select" aria-label="Bulan" id="bulan_pasokan_bapok" name="bulan" required>
                                        <option value="">- Pilih Bulan -</option>
                                        <option value="Januari">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Tahun</label>
                                    <select class="form-select" aria-label="Tahun" id="tahun_pasokan_bapok" name="tahun" required>
                                        <option value="">- Pilih Tahun -</option>
                                        <?php
                                        for ($i = 2020; $i <= date('Y'); $i++) {
                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jenis Komoditas</label>
                                    <select class="form-select" id="jenis_komoditi_pasokan_bapok" name="jenis_komoditi_id">
                                        <option selected="" value="">Pilih Jenis Komoditas</option>
                                        <?php foreach ($jenis_komoditi as $jk) : ?>
                                            <?php if ($jk['jenis_komoditi_id'] == 1 || $jk['jenis_komoditi_id'] == 3 || $jk['jenis_komoditi_id'] == 6) { ?>
                                                <option value="<?= $jk['jenis_komoditi_id'] ?>"><?= $jk['jenis_komoditi'] ?></option>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Varian</label>
                                    <select class="form-select" id="varian_komoditi_pasokan_bapok" name="varian_komoditi_id">
                                        <option selected="" value="">Pilih Varian Komoditas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah</label>
                                    <input type="text" name="jumlah" id="jumlah_pasokan_bapok" autocomplete="off" class="form-control price my-form-control" placeholder="Jumlah...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Satuan</label>
                                    <select id="satuan_komoditi_pasokan_bapok" name="satuan_komoditi_id">
                                        <option selected="" value="">Pilih Satuan Komoditas</option>
                                    </select>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>