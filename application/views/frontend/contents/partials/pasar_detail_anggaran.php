<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#mdl_anggaran"><i class="bx bx-plus me-1"></i>Tambah</button>
<?php endif; ?>



<h3 class="pb-md-2 pb-lg-3 text-primary">Tabel Anggaran Pasar</h3>
<p class="pb-4 mb-3"> Silahkan edit dan kelola data anggaran pasar pada bagian ini.</p>



<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_anggaran" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_anggaran">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Anggaran Pasar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-anggaran" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_anggaran_id" id="pasar_anggaran_id">
                        <input type="hidden" name="jenis_anggaran" value="pasar" id="jenis_anggaran">
                        <div class="row gy-3">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-label">Nama Pasar</label>
                                        <input type="text" class="form-control my-form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" aria-label="Nama Pasar" readonly="readonly">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Tahun</label>
                                        <input type="text" class="form-control my-form-control" autocomplete="off" name="tahun_anggaran" id="tahun_anggaran" placeholder="Tahun" aria-label="Tahun">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Program Pasar</label>
                                        <select aria-label="Jenis Komoditi" name="program_pasar" id="program_pasar">
                                            <option selected="" value="">Program Pasar</option>

                                            <?php foreach ($program_pasar as $pp) : ?>
                                                <option value="<?= $pp['program_pasar_id'] ?>"><?= $pp['program_pasar'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Omset</label>
                                        <input type="text" class="form-control price my-form-control" autocomplete="off" placeholder="Omzet" name="omset_anggaran" id="omset_anggaran" aria-label="Omzet">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Jumlah Anggaran</label>
                                        <input type="text" class="form-control price my-form-control" autocomplete="off" placeholder="Jumlah Anggaran" name="jumlah_anggaran" id="jumlah_anggaran" aria-label="Jumlah Anggaran">
                                    </div>
                                </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>