<?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas') { ?>
    <a class="btn btn-secondary float-end" id="btn_filter2"><i class='bx bx-filter-alt me-1'></i>Filter</a>
<?php } ?>
<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button onclick="add('harga')" class="btn btn-primary float-end me-1"><i class="bx bx-plus-circle me-1"></i>Tambah</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Tabel Harga Komoditi</h3>
<p class="pb-4 mb-3" id="title-date-harga"></p>

<?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas') { ?>
    <div class="container bg-secondary rounded-3 py-4" id="box_filter2">
        <form>
            <div class="row  pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
                <div class="col-md-9">
                    <label for="filter-harga-komoditi-date1" class="d-flex">Tanggal</label>
                    <input type="date" class="form-control" id="filter-harga-komoditi-date1" value="<?= date('Y-m-d') ?>">
                </div>
                <div class="col-md-3">
                    <button type="button" id="filter-harga-komoditi" class="btn btn-primary mt-4 w-100"><i class="bx bx-search me-1"></i>Filter</button>
                </div>
            </div>
        </form>
    </div>
<?php } ?>

<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_harga" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_harga">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Harga Komoditas</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-harga" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="harga_komoditi_id" id="harga_komoditi_id">
                        <div class="row gy-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" id="name" name="name" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jenis Komoditas</label>
                                    <select id="jenis_komoditi" name="jenis_komoditi">
                                        <option selected="" value="">Pilih Jenis Komoditas</option>
                                        <?php foreach ($jenis_komoditi as $jk) : ?>
                                            <option value="<?= $jk['jenis_komoditi_id'] ?>|<?= $jk['jenis_komoditi'] ?>"><?= $jk['jenis_komoditi'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Varian</label>
                                    <select id="varian_komoditi" name="varian_komoditi">
                                        <option selected="" value="">Pilih Varian Komoditas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Harga</label>
                                    <input type="text" name="harga" id="harga" autocomplete="off" class="form-control price my-form-control" placeholder="Masukkan Harga..." aria-label="Harga">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Satuan</label>
                                    <select id="satuan_komoditi" name="satuan_komoditi">
                                        <option selected="" value="">Pilih Satuan Komoditas</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>