<section id="sec_perbandingan" class="container pt-5 pb-2 mt-3 mt-sm-4 mt-xl-5">
    <h2 class="h1 text-center pb-3 pb-lg-4" data-aos="fade-up">Perbandingan <span class="text-primary">Harga Barang Kebutuhan Pokok</span></h2>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs flex-nowrap justify-content-lg-center overflow-auto pb-2 mb-3 mb-lg-4" role="tablist">
        <li class="nav-item" role="presentation" data-aos="flip-up">
            <button class="nav-link text-nowrap  active" id="2-tab" data-bs-toggle="tab" data-bs-target="#tab-2" type="button" role="tab" aria-controls="tab-2" aria-selected="false">
                <i class="bx bx bx-shape-polygon fs-lg opacity-60 me-1"></i>
                SP2KP (Diolah, <?= date('Y') ?>)
            </button>
        </li>
        <li class="nav-item" role="presentation" data-aos="flip-up">
            <button class="nav-link text-nowrap" id="1-tab" data-bs-toggle="tab" data-bs-target="#tab-1" type="button" role="tab" aria-controls="tab-1" aria-selected="true">
                <i class="bx bx-shape-polygon fs-lg opacity-60 me-1"></i>
                Pengelola Pasar tingkat Provinsi/Kabupaten/Kota (Diolah, <?= date('Y') ?>)
            </button>
        </li>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content bg-secondary rounded-3 py-4">
        <!-- Remote Work -->
        <div class="tab-pane fade show active" id="tab-2" role="tabpanel" aria-labelledby="2-tab">
            <div class="row justify-content-center  mt-4 pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
                <div class="col-md-10 text-center text-md-start">
                    <h3 class="mb-lg-4" data-aos="fade-up"> SP2KP (Diolah, <?= date('Y') ?>)</h3>
                    <p data-aos="fade-up">Berikut adalah Harga Perbandingan Bahan Kebutuhan Pokok SP2KP (Diolah, <?= date('Y') ?>) yang telah diolah dalam bentuk tabel. Anda dapat memasukkan filter data untuk menampilkan perbandingan harga yang anda inginkan. <strong>Data dibawah ini mempunyai keterangan sebagai berikut.</strong></p>
                    <ol data-aos="fade-up">
                        <li>Data Harga diinput oleh Kontributor Dinas Perdagangan baik Provinsi/Kabupaten/Kota</li>
                        <li>Wilayah Pantauan terdiri dari 411 titik pantauan yaitu 34 Provinsi dan 377 Kabupaten/Kota</li>
                        <li>Jumlah Pasar Pantauan terdiri dari 537 Pasar Rakyat yaitu 113 Pasar di 34 Ibukota Provinsi dan 424 Pasar di 377 Kabupaten/Kota Lainnya</li>
                    </ol>
                </div>
            </div>
            <div class="row justify-content-center  mt-4 pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
                <div class="col-md-4">
                    <form>
                        <div class="row gy-3">
                            <div class="col-md-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-ews-provinsi" class="form-label fs-base">Provinsi</label>
                                <select id="filter-bahan-pokok-ews-provinsi" class="form-control">
                                    <option selected value="0|Nasional">Nasional</option>

                                    <?php foreach ($ews_provinces as $province) : ?>
                                        <option value="<?= $province['id'] ?>|<?= $province['fulllocationname'] ?>"><?= $province['fulllocationname'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-md-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-ews-date2" class="form-label fs-base">Tanggal Harga</label>
                                <input type="date" value="<?= $val_date2 ?>" class="form-control" id="filter-bahan-pokok-ews-date2">
                            </div>
                            <div class="col-md-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-ews-date1" class="form-label fs-base">Tanggal Perbandingan Harga</label>
                                <input type="date" value="<?= $val_date1 ?>" class="form-control" id="filter-bahan-pokok-ews-date1">
                            </div>
                            <div class="col-12 pt-2 pt-sm-3" data-aos="flip-up">
                                <button type="button" id="search-bahan-pokok-ews" class="btn btn-lg btn-primary w-100 w-sm-auto"><i class="bx bx-search me-1"></i>Filter Pencarian</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6" id="load-bahan-pokok-ews">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" data-aos="fade-up">
                                <table class="table table-bapok">
                                    <thead>
                                        <th class="text-left">Komoditas</th>
                                        <th>Satuan</th>
                                        <th id="date1-ews" class="text-center"><?= $date1 ?> (Rp.)</th>
                                        <th id="date2-ews" class="text-center"><?= $date2 ?> (Rp.)</th>
                                        <th>(%)</th>
                                        <th>Ket</th>
                                    </thead>

                                    <tbody id="harga-bahan-pokok-ews"></tbody>
                                </table>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center" id="loader-bahan-pokok-ews" style="display: none; height:300px;">
                    <lottie-player src="<?= base_url() ?>assets/brand/load-data.json" class="mx-auto d-block align-middle mt-4" background="transparent" speed="1" style="width:100px" loop autoplay></lottie-player>
                </div>
            </div>
        </div>
        <!-- Project Management -->
        <div class="tab-pane fade" id="tab-1" role="tabpanel" aria-labelledby="1-tab">
            <div class="row justify-content-center  mt-4 pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
                <div class="col-md-10 text-center text-md-start">
                    <h3 class="mb-lg-4" data-aos="fade-up">Pengelola Pasar tingkat Provinsi/Kabupaten/Kota (Diolah, <?= date('Y') ?>)</h3>
                    <p data-aos="fade-up">Berikut adalah Harga Perbandingan Bahan Kebutuhan Pokok Pengelola Pasar tingkat Provinsi/Kabupaten/Kota yang telah diolah. Anda dapat memasukkan filter data untuk menampilkan perbandingan harga yang anda inginkan.</p>
                </div>
            </div>
            <div class="row justify-content-center  mt-4 pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
                <div class="col-md-4">
                    <form>
                        <div class="row gy-3">
                            <div class="col-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-provinsi" class="form-label fs-base">Provinsi</label>
                                <select id="filter-bahan-pokok-provinsi" class="form-control">
                                    <option selected value="1|Nasional">Nasional</option>

                                    <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                        <option value="<?= $provinsi['daerah_id'] ?>|<?= $provinsi['provinsi'] ?>"><?= $provinsi['provinsi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-kabupaten" class="form-label fs-base">Kabupaten / Kota</label>
                                <select id="filter-bahan-pokok-kabupaten" class="form-control">
                                    <option value="" selected>-</option>
                                </select>
                            </div>
                            <div class="col-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-pasar" class="form-label fs-base">Pasar</label>
                                <select id="filter-bahan-pokok-pasar" class="form-control">
                                    <option value="" selected>-</option>
                                </select>
                            </div>
                            <div class="col-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-date2" class="form-label fs-base">Tanggal Harga</label>
                                <input type="date" value="<?= $val_date2 ?>" class="form-control" id="filter-bahan-pokok-date2">
                            </div>
                            <div class="col-12" data-aos="flip-up">
                                <label for="filter-bahan-pokok-date1" class="form-label fs-base">Tanggal Perbandingan Harga</label>
                                <input type="date" value="<?= $val_date1 ?>" class="form-control" id="filter-bahan-pokok-date1">
                            </div>
                            <div class="col-12 pt-2 pt-sm-3" data-aos="flip-up">
                                <button type="button" id="search-bahan-pokok" class="btn btn-lg btn-primary w-100 w-sm-auto"><i class="bx bx-search me-1"></i>Filter Pencarian</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6" id="load-bahan-pokok">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" data-aos="fade-up">
                                <table class="table table-bapok">
                                    <thead>
                                        <th class="text-left">Komoditas</th>
                                        <th>Satuan</th>
                                        <th id="date1" class="text-center"><?= $date1 ?></th>
                                        <th id="date2" class="text-center"><?= $date2 ?></th>
                                        <th>(%)</th>
                                        <th>Ket</th>
                                    </thead>
                                    <tbody id="harga-bahan-pokok"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6 text-center" id="loader-bahan-pokok" style="display: none; height:300px;">
                    <lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" class="mx-auto d-block align-middle mt-4" loop autoplay></lottie-player>
                </div>
            </div>
        </div>


    </div>
</section>