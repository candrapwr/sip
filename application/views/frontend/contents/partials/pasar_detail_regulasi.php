<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
        <button onclick="addRegulasi()" class="btn btn-primary float-end" id="btn_filter2"><i class="bx bx-plus me-1"></i>Tambah</button>
    <?php endif; ?>

    <h3 class="pb-md-2 pb-lg-3 text-primary" >Data Regulasi</h3>
    <p class="pb-4 mb-3" id="title-date-harga"></p>
<?php endif; ?>


<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_regulasi" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_regulasi">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Pasukan Bahan Pokok</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-regulasi" enctype="multipart/form-data" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="pasar_regulasi_id" id="pasar_regulasi_id">
                        <div class="row gy-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" id="name" name="name" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">No. Regulasi</label>
                                    <input type="text" name="no_regulasi" id="no_regulasi" autocomplete="off" class="form-control my-form-control" placeholder="No Regulasi...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Nama Regulasi</label>
                                    <input type="text" name="nama_regulasi" id="nama_regulasi" autocomplete="off" class="form-control my-form-control" placeholder="Nama Regulasi...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Pengesah Regulasi</label>
                                    <input type="text" name="pengesah_regulasi" id="pengesah_regulasi" autocomplete="off" class="form-control my-form-control" placeholder="Pengesah Regulasi...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Tanggal Regulasi</label>
                                    <input type="date" name="tgl_regulasi" id="tgl_regulasi" autocomplete="off" class="form-control my-form-control" placeholder="Tanggal Regulasi...">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">File Regulasi</label>
                                    <input type="file" name="file_regulasi" id="file_regulasi" class="form-control my-form-control" placeholder="File Regulasi...">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>