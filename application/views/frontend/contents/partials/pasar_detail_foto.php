<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
    <button class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#mdl_addfoto"><i class="bx bx-plus me-1"></i>Tambah Foto Lainnya</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Foto Galeri</h3>
<p class="pb-4 mb-3"> Silahkan lihat dan kelola foto depan, belakang, dan lainnya pasar pada bagian ini.</p>

<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <!-- Gallery grid with gutters -->
            <div class="gallery row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4" data-video="true">
                <!-- Item -->
                <div class="col">
                    <a href="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" class="gallery-item rounded-3" data-sub-html='<h6 class="fs-sm text-light">Tampak Depan <?= $index_pasar['nama'] ?></h6>'>
                        <img class="foto-pasar" src="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" alt="Tampak Depan <?= $index_pasar['nama'] ?>">
                        <div class="gallery-item-caption fs-sm fw-medium">Tampak Depan <?= $index_pasar['nama'] ?></div>
                    </a>
                </div>
                <!-- Item -->
                <div class="col">
                    <a href="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" class="gallery-item rounded-3" data-sub-html='<h6 class="fs-sm text-light">Tampak Dalam <?= $index_pasar['nama'] ?></h6>'>
                        <img class="foto-pasar" src="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" alt="Tampak Dalam <?= $index_pasar['nama'] ?>">
                        <div class="gallery-item-caption fs-sm fw-medium">Tampak Dalam <?= $index_pasar['nama'] ?></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container pt-4 pb-2">
    <!-- Text -->
    <h3 class="pb-2 pb-md-3 text-primary">Foto Lainnya</h3>
    <p class="fs-xl pb-4 mb-1 mb-md-2 mb-lg-3">Berikut adalah foto-foto lainnya dari <?= $index_pasar['nama'] ?> .</p>

    <div class="row gy-4">

        <?php if ($index_pasar['foto_lainnya']) { ?>
            <div class="col-md-12">
                <!-- Gallery grid with gutters -->
                <div class="gallery row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4" data-video="true">
                    <?php
                    $count = 0;

                    foreach ($index_pasar['foto_lainnya'] as $vFoto) {
                        // if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn') {
                        //     $height = '190px';
                        // } else {
                        //     $height = '150px';
                        // }
                        $count++;
                    ?>
                        <!-- Item -->
                        <div class="col" id="div_ft_<?= $vFoto['pasar_foto_id'] ?>">
                            <a href="<?= $vFoto['foto'] ?>" class="gallery-item rounded-3" data-sub-html='<h6 class="fs-sm text-light">Foto Lainnya Ke - <?= $count ?></h6>'>
                                <img class="foto-pasar" src="<?= $vFoto['foto'] ?>" alt="Foto Lainnya Ke - <?= $count ?>">
                                <div class="gallery-item-caption fs-sm fw-medium">Foto Lainnya Ke - <?= $count ?></div>
                            </a>
                            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn') { ?>
                                <a id="a_ft_<?= $vFoto['pasar_foto_id'] ?>" href="javascript:void(0)" onClick="callHapusFt('<?= $vFoto['pasar_foto_id'] ?>')" class="btn btn-lg btn-outline-danger w-100 mt-4"><i class='bx bx-trash me-1'></i>Delete Photo</a>
                            <?php } ?>
                        </div>
                    <?php }
                    ?>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-md-12 text-center">
                <lottie-player  class=" w-50 mx-auto d-block" src="<?= base_url() ?>assets/frontend/img/pasar-foto.json" background="transparent" speed="1"   loop autoplay></lottie-player>
                <h3 class="mt-0 pt-0">Tidak Ada Foto Lainnya</h3>
            </div>
        <?php } ?>
    </div>
</div>


<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_addfoto">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Unggah Foto Lainya</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <div class="dropzone dz-clickable" id="myDrop">
                                <div id="inputhidde" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px; display: none;"></div>
                                <div class="dz-default dz-message" data-dz-message="">
                                    <span>aaa</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" id="add_file" class="btn btn-primary" name="submit"><i class='bx bx-cloud-upload me-1'></i>Upload File(s)</button>
                            <span id="c_add_file"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>