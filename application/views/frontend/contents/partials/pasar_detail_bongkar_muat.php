<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button class="btn btn-primary float-end" onclick="addBongkar_muat()"><i class="bx bx-plus me-1"></i>Tambah</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Tabel Bongkar Muat</h3>
<p class="pb-4 mb-3"> Silahkan edit dan kelola data bongkar muat pada bagian ini.</p>



<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_bongkar_muat" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_bongkar_muat">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Bongkat Muat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-bongkar-muat" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_bongkar_muat_id" id="pasar_bongkar_muat_id">
                        <div class="row gy-3">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Bulan</label>
                                    <select class="form-select" aria-label="Bulan" id="bulan_bongkar_muat" name="bulan" required>
                                        <option value="">- Pilih Bulan -</option>
                                        <option value="Januari">Januari</option>
                                        <option value="Februari">Februari</option>
                                        <option value="Maret">Maret</option>
                                        <option value="April">April</option>
                                        <option value="Mei">Mei</option>
                                        <option value="Juni">Juni</option>
                                        <option value="Juli">Juli</option>
                                        <option value="Agustus">Agustus</option>
                                        <option value="September">September</option>
                                        <option value="Oktober">Oktober</option>
                                        <option value="November">November</option>
                                        <option value="Desember">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-label">Tahun</label>
                                    <select class="form-select" aria-label="Tahun" id="tahun_bongkar_muat" name="tahun" required>
                                        <option value="">- Pilih Tahun -</option>
                                        <?php
                                        for ($i = 2020; $i <= date('Y'); $i++) {
                                            echo '<option value="' . $i . '">' . $i . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-label">Jumlah</label>
                                    <input type="number" class="form-control price my-form-control" id="jumlah_bongkar_muat" autocomplete="off" name="jumlah" id="jumlah" placeholder="Jumlah Bongkar Muat" aria-label="Harga">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>