<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
    <button onclick="formDetail('<?= $active_index[0] ?>')" class="btn btn-warning float-end"><i class="bx bx-edit me-1"></i>Edit Informasi Dasar</button>
<?php endif; ?>
<!-- Specs -->
<div class="container py-2">
    <h3 class="mb-5  text-primary">Spesifikasi</h3>
    <div class="row pb-md-2 pb-lg-4 pb-xl-5 mb-xxl-2">
        <div class="col-md-12">
            <div class="table-responsive border-top mb-0">
                <table class="table align-middle">
                    <tbody>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Jarak Dengan Toko Modern
                            </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['jarak_toko_modern'] != null) ? $detail_pasar[0]['jarak_toko_modern'] . ' Km' : '-' ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Penerapan Digitalisasi Pasar
                            </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['penerapan_digitalisasi_pasar'] != null) ? $detail_pasar[0]['penerapan_digitalisasi_pasar'] : '-' ?>
                                </span>
                            </td>
                        </tr>
                        <?php if ($detail_pasar[0]['penerapan_digitalisasi_pasar'] != null && $detail_pasar[0]['penerapan_digitalisasi_pasar'] == 'Ya') { ?>
                            <tr>
                                <th scope="row" class="ps-0 h5">
                                    Digitalisasi Pasar
                                </th>
                                <td class="pe-0">
                                    <div class="d-inline-block my-4 py-1">
                                        <?= ($detail_pasar != null) ? $detail_pasar[0]['detail_digitalisasi_pasar'] : '-' ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Pembiayaan Lembaga Keuangan
                            </th>
                            <td class="pe-0">
                                <div class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['pembiayaan_lk'] != null) ? $detail_pasar[0]['pembiayaan_lk'] : '-' ?>
                                </div>
                            </td>
                        </tr>
                        <?php if ($detail_pasar[0]['pembiayaan_lk'] != null && $detail_pasar[0]['pembiayaan_lk'] == 'Ya') { ?>
                            <tr>
                                <th scope="row" class="ps-0 h5">
                                    Nama Lembaga Keuangan
                                </th>
                                <td class="pe-0">
                                    <div class="d-inline-block my-4 py-1">
                                        <?= ($detail_pasar != null && $detail_pasar[0]['lembaga_keuangan'] != null) ? $detail_pasar[0]['lembaga_keuangan'] : '-' ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Sertifikasi SNI Pasar Rakyat
                            </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['sertifikasi_sni'] != null) ? $detail_pasar[0]['sertifikasi_sni'] : '-' ?>
                                </span>
                            </td>
                        </tr>
                        <?php if ($detail_pasar[0]['sertifikasi_sni'] != null && $detail_pasar[0]['sertifikasi_sni'] == 'Ya') { ?>
                            <tr>
                                <th scope="row" class="ps-0 h5">
                                    Tahun Sertifikasi SNI Pasar Rakyat
                                </th>
                                <td class="pe-0">
                                    <span class="d-inline-block my-4 py-1">
                                        <?= ($detail_pasar != null && $detail_pasar[0]['tahun_sertifikasi'] != null) ? $detail_pasar[0]['tahun_sertifikasi'] : '-' ?>
                                    </span>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Sarana dan Prasarana
                            </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['sarana_prasarana'] != null) ? $detail_pasar[0]['sarana_prasarana'] : '-' ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Dilalui Angkutan Umum
                            </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['angkutan_umum'] != null) ? $detail_pasar[0]['angkutan_umum'] : '-' ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row" class="ps-0 h5">
                                Dekat dengan pemukiman (< 2km) </th>
                            <td class="pe-0">
                                <span class="d-inline-block my-4 py-1">
                                    <?= ($detail_pasar != null && $detail_pasar[0]['jarak_pemukiman'] != null) ? $detail_pasar[0]['jarak_pemukiman'] : '-' ?>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="container py-2">
    <!-- Text -->
    <h3 class="pb-2 pb-md-3 text-primary">Dalam Angka</h3>
    <p class="fs-xl pb-4 mb-1 mb-md-2 mb-lg-3">Berikut adalah informasi atau statistik <?= $index_pasar['nama'] ?> dalam angka.</p>

    <div class="row row-cols-3 gy-4 mt-2 mt-xl-4">
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? ($detail_pasar[0]['jam_sibuk_awal'] != null ? date('H:i', strtotime($detail_pasar[0]['jam_sibuk_awal'])) : 0) : '-' ?> - <?= ($detail_pasar != null) ? ($detail_pasar[0]['jam_sibuk_akhir'] != null ? date('H:i', strtotime($detail_pasar[0]['jam_sibuk_akhir'])) : '-') : '-' ?></h3>
            <p>Jam <strong>Sibuk</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_pekerja_tetap']) : '-' ?></h3>
            <p>Jumlah <strong>Pekerja Tetap</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_pekerja_nontetap']) : '-' ?></h3>
            <p>Jumlah <strong>Pekerja Non Tetap</strong></p>
        </div>

        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['luas_bangunan']) : '-' ?> m2</h3>
            <p>Luas <strong>Bangunan</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['luas_tanah']) : '-' ?> m2</h3>
            <p>Luas <strong>Tanah</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_lantai']) : '-' ?></h3>
            <p>Jumlah <strong>Lantai</strong></p>
        </div>

        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? $detail_pasar[0]['tahun_bangun'] : '-' ?></h3>
            <p>Tahun <strong>Dibangun</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? $detail_pasar[0]['tahun_renovasi'] : '-' ?></h3>
            <p>Tahun <strong>Renovasi</strong></p>
        </div>
        <div class="col-md-4">
            <h3 class="h2 mb-2"><?= ($detail_pasar != null) ? $detail_pasar[0]['jumlah_pengunjung_harian'] : '-' ?></h3>
            <p>Jumlah <strong>Pengunjung Harian</strong></p>
        </div>
    </div>
</div>


<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_detail">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Informasi Dasar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="<?= site_url() ?>dashboard/pasar/save-detail" method="POST" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="pasar_nama" value="<?= $index_pasar['nama'] ?>">
                        <input type="hidden" name="action_index" value="<?= $this->uri->segment(4) ?>">
                        <input type="hidden" name="pasar_detail" value="<?= ($detail_pasar != null) ? $detail_pasar[0]['pasar_detail_id'] : '' ?>">
                        <div id="smartwizard">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="#step-0">
                                        <div class="num">1</div>
                                        <div>
                                            <strong>INFORMASI UMUM</strong>
                                            <p>Informasi Umum Pasar</p>
                                        </div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#step-1">
                                        <span class="num">2</span>
                                        <strong>WAKTU OPERASIONAL</strong>
                                        <p>Informasi Waktu Operasional</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#step-2">
                                        <span class="num">3</span>
                                        <strong>LUAS PASAR</strong>
                                        <p>Informasi Luas Pasar</p>
                                    </a>
                                </li>
                            </ul>

                            <!-- Include optional progressbar HTML -->
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="tab-content">
                                <div id="step-0" class="tab-pane" role="tabpanel" aria-labelledby="step-0">
                                    <div class="row mb-3 gy-3">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Klasifikasi</label>
                                                <select aria-label="Klasifikasi" data-placeholder="Pilih Klasifikasi" name="klasifikasi" id="klasifikasi" required>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Klasifikasi tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">No. Telepon</label>
                                                <input type="text" class="form-control my-form-control" autocomplete="off" name="phone" id="phone" placeholder="Masukkan Nomor Telepon..." aria-label="Masukkan No. Telepon..." required>
                                                <div class="invalid-feedback">
                                                    No. telepon tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">No. Fax</label>
                                                <input type="text" class="form-control my-form-control" autocomplete="off" name="fax" id="fax" placeholder="Masukkan No. Fax..." aria-label="Fax">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Tipe Pasar</label>
                                                <select aria-label="Jenis Komoditi" name="tipe_pasar" id="tipe_pasar" required>
                                                    <option selected="" value="">Pilih Tipe Pasar</option>

                                                    <?php foreach ($tipe_pasar as $tp) : ?>
                                                        <option value="<?= $tp['tipe_pasar_id'] ?>"><?= $tp['tipe_pasar'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Tipe pasar tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Bentuk Pasar</label>
                                                <select aria-label="Varian Komoditi" name="bentuk_pasar" id="bentuk_pasar" required>
                                                    <option selected="" value="">Pilih Bentuk Pasar</option>
                                                    <option value="1">Permanen</option>
                                                    <option value="2">Semi Permanen</option>
                                                    <option value="3">Tanpa Bangungan</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Bentuk pasar tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Kondisi</label>
                                                <select aria-label="Satuan Komoditi" name="kondisi" id="kondisi" required>
                                                    <option selected="" value="">Pilih Kondisi</option>
                                                    <option value="1">Baik</option>
                                                    <option value="2">Rusak Berat</option>
                                                    <option value="3">Rusak Ringan</option>
                                                    <option value="4">Rusak Sedang</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Kondisi tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jarak Dengan Toko Modern (Km)</label>
                                                <input type="text" class="form-control my-form-control" autocomplete="off" name="jarak_toko_modern" id="jarak" placeholder="Jarak Dengan Toko Modern (Km)..." aria-label="Jarak">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Penerapan Digitalisasi Pasar</label>
                                                <select class="form-select" data-placeholder="Penerapan Digitalisasi Pasar" aria-label="Penerapan Digitalisasi Pasar" name="penerapan_digitalisasi_pasar" id="penerapan_digital_pasar">
                                                    <option selected="" value="">Pilih Penerapan Digitalisasi Pasar</option>
                                                    <option value="Ya">Ya</option>
                                                    <option value="Tidak">Tidak</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-3" id="penerapan_dp" style="display:none;"></div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Pembiayaan Lembaga Keuangan</label>
                                                <select class="form-select" aria-label="Pembiayaan Lembaga Keuangan" name="pembiayaan_lembaga_keuangan" id="pembiayaan_lembaga_keuangan">
                                                    <option selected="" value="">Pilih Pembiayaan Lembaga Keuangan</option>
                                                    <option value="Ya">Ya</option>
                                                    <option value="Tidak">Tidak</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-3" id="form_lembaga_keuangan" style="display:none;"></div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Sertifikasi SNI Pasar Rakyat</label>
                                                <select class="form-select" aria-label="Sertifikasi SNI Pasar Rakyat" name="sertifikasi_sni" id="sertifikasi_sni">
                                                    <option selected="" value="">Pilih Sertifikasi SNI Pasar Rakyat</option>
                                                    <option value="Ya">Ya</option>
                                                    <option value="Tidak">Tidak</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 mb-3" id="form_tahun_sni_pasar_rakyat" style="display:none;"></div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Sarana dan Prasarana</label>
                                                <select aria-label="Sarana dan Prasarana" data-placeholder="Pilih Sarana dan Prasarana" multiple name="sarana_prasarana[]" id="sarana_prasarana">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Dekat dengan pemukiman (< 2km)</label>
                                                        <select class="form-select" aria-label="Dekat dengan pemukiman" name="dekat_pemukiman" id="dekat_pemukiman">
                                                            <option selected="" value="">Pilih Dekat dengan pemukiman (< 2km)</option>
                                                            <option value="Ya">Ya</option>
                                                            <option value="Tidak">Tidak</option>
                                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 ">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Dilalui Angkutan Umum</label>
                                                <textarea type="text" class="form-control my-form-control" autocomplete="off" name="angkutan_umum" id="angkutan_umum" placeholder="Penjelasan Angkutan Umum..." aria-label="Penjelasan Angkutan Umum"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-1" class="tab-pane" role="tabpanel" aria-labelledby="step-1">
                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Waktu Operasional</label>
                                                <select class="form-select" aria-label="Waktu Operasional" name="waktu_operasional[]" id="waktu_operasional" multiple="multiple" required>
                                                    <option selected="" value="">Pilih Waktu Operasional</option>
                                                    <option value="1">Senin</option>
                                                    <option value="2">Selasa</option>
                                                    <option value="3">Rabu</option>
                                                    <option value="4">Kamis</option>
                                                    <option value="5">Jumat</option>
                                                    <option value="6">Sabtu</option>
                                                    <option value="7">Minggu</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Waktu operasional tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jam Buka</label>
                                                <input type="time" class="timepicker form-control datepicker my-form-control" name="jam_buka" id="jam_buka" placeholder="Masukkan Jam Buka..." required>
                                                <div class="invalid-feedback">
                                                    jam buka tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jam Tutup</label>
                                                <input type="time" class="timepicker form-control datepicker my-form-control" name="jam_tutup" id="jam_tutup" placeholder="Masukkan Jam Tutup..." required>
                                                <div class="invalid-feedback">
                                                    Jam tutup tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jam Mulai Sibuk</label>
                                                <input type="time" class="timepicker form-control datepicker my-form-control" name="jam_sibuk_awal" id="jam_sibuk_awal" placeholder="Masukkan Jam Mulai Sibuk..." required>
                                                <div class="invalid-feedback">
                                                    jam mulai sibuk tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jam Berakhir Sibuk</label>
                                                <input type="time" class="timepicker form-control datepicker my-form-control" name="jam_sibuk_akhir" id="jam_sibuk_akhir" placeholder="Masukkan Jam Berakhir Sibuk..." required>
                                                <div class="invalid-feedback">
                                                    Jam berakhir sibuk tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jumlah Pekerja Tetap</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="pekerja_tetap" id="pekerja_tetap" placeholder="Masukkan Pekerja Tetap..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    Jumlah pekerja tetap tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jumlah Pekerja Non Tetap</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="pekerja_nontetap" id="pekerja_nontetap" placeholder="Masukkan Pekerja Non Tetap..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    jumlah pekerja non tetap tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jumlah Rata-Rata Pengunjung Perhari</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="jumlah_pengunjung" id="jumlah_pengunjung" placeholder="Rata-Rata Pengunjung Perhari..." aria-label="Jumlah Rata Rata Pengunjung">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2" class="tab-pane" role="tabpanel" aria-labelledby="step-2">
                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Luas Bangunan (m2)</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="luas_bangunan" id="luas_bangunan" placeholder="Masukkan Luas Bangunan..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    Luas bangunan tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Luas Tanah (m2)</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="luas_tanah" id="luas_tanah" placeholder="Masukkan Luas Tanah..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    Luas tanah tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row gy-3 mb-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Jumlah Lantai</label>
                                                <input type="text" class="form-control price my-form-control" autocomplete="off" name="jumlah_lantai" id="jumlah_lantai" placeholder="Jumlah Lantai..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    jumlah lantai tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Tahun Dibangun</label>
                                                <input type="text" class="form-control my-form-control" autocomplete="off" name="tahun_bangun" id="tahun_bangun" placeholder="Tahun Dibangun..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    Tahun bangun tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="form-label fw-bold">Tahun Renovasi</label>
                                                <input type="text" class="form-control my-form-control" autocomplete="off" name="tahun_renovasi" id="tahun_renovasi" placeholder="Tahun Revonasi..." aria-label="Harga" required>
                                                <div class="invalid-feedback">
                                                    Tahun renovasi tidak boleh kosong.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" id="submit" style="width: 100%;" class="btn btn-lg btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>