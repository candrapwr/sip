<nav class="container py-4 mb-lg-2" aria-label="breadcrumb">
    <ol class="breadcrumb pt-lg-3 mb-0">
        <li class="breadcrumb-item">
            <a href="<?= base_url() ?>"><i class="bx bx-home-alt fs-lg me-1"></i>Sistem Informasi Sarana Perdagangan</a>
        </li>
        <li class="breadcrumb-item active" aria-current="page"><?= (isset($title)) ? $title : '404 Halaman Tidak Ditemukan'; ?> </li>
    </ol>
</nav>