<?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
    <button class="btn btn-secondary float-end" data-bs-toggle="modal" data-bs-target="#mdl_upload_pedagang"><i class="bx bx-upload me-1"></i> Upload Pedagang</button>
    <button class="btn btn-primary float-end me-1" data-bs-toggle="modal" data-bs-target="#mdl_kios"><i class="bx bx-plus me-1"></i>Tambah</button>
<?php endif; ?>

<h3 class="pb-md-2 pb-lg-3 text-primary">Tabel Pedagang Kios/Los/Dasaran</h3>
<p class="pb-4 mb-3"> Silahkan edit dan kelola data pedagang kios/los/dasaran pada bagian ini.</p>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="alert alert-warning mb-0" role="alert">
        <h4 class="pt-2 alert-heading"><i class='bx bx-info-circle me-1' ></i>Perhatian! Data Pedagang</h4>
        <ul>
            <li>Silakan unggah file xls/xlsx yang memuat data antara lain: nama pedagang (sesuai ktp), no telpon aktif (diutamakan bisa whatsapp), alamat lengkap, jenis kelamin, dan jenis pedagang (kios/los/dasaran)</li>
            <li>Perbarui data dengan mengunggah kembali file yang udah update</li>
        </ul>
        <hr class="opacity-25 my-0" style="color: currentColor;">
        <?php if ($pasar_pedagang != null) { ?>
            <p class="mb-0 justify-content-center"><a href="<?= base_url() ?>services/res/upload/file_pedagang/<?= date('Y', strtotime($pasar_pedagang[0]['createdon'])) ?>/<?= $pasar_pedagang[0]['file_pedagang'] ?>" target="_blank">File Data Pedaganag</a></p>
        <?php } else { ?>
            <p class="mb-0  text-danger justify-content-center"><i>Belum melakukan upload data pedagang</i></p>
        <?php } ?>
    </div>
<?php } ?>

<div class="container py-2">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive" data-aos="fade-up">
                <table id="tbl_kios" class="table table-striped-columns" style="width: 100% !important;">
                </table>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <div class="modal" tabindex="-1" role="dialog" id="mdl_kios">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Kios Pasar</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-kios" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_bangunan_id" id="pasar_bangunan_id">
                        <div class="row gy-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Jenis Bangunan</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="1" checked="checked">
                                                <label class="form-check-label" for="los">
                                                    Los
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="2">
                                                <label class="form-check-label" for="kios">
                                                    Kios
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="3">
                                                <label class="form-check-label" for="dasaran">
                                                    Dasaran
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah Bangunan</label>
                                    <input type="text" class="form-control price my-form-control" autocomplete="off" name="jumlah_bangunan" id="jumlah_bangunan" placeholder="Jumlah Bangunan" aria-label="Harga">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah Pedagang</label>
                                    <input type="text" class="form-control price my-form-control" autocomplete="off" name="jumlah_pedagang" id="jumlah_pedagang" placeholder="Jumlah Pedagang" aria-label="Harga">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="mdl_upload_pedagang">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Form Upload Data Pedagang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form id="form-data-pedagang" class="needs-validation default-form" novalidate>
                    <div class="modal-body">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_bangunan_id" id="pasar_bangunan_id">
                        <div class="row gy-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">Nama Pasar</label>
                                    <input type="text" class="form-control my-form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">File Pedagang</label>
                                    <input type="file" class="form-control price my-form-control" name="file_pedagang" id="file_pedagang" placeholder="File Pedagang" accept=".xls,.xlsx" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                        <button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>