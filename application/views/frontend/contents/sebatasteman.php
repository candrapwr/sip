<!-- Page content -->
<section class="d-flex align-items-center min-vh-100 py-5 bg-light" style="background: radial-gradient(144.3% 173.7% at 71.41% 94.26%, rgba(99, 102, 241, 0.1) 0%, rgba(218, 70, 239, 0.05) 32.49%, rgba(241, 244, 253, 0.07) 82.52%);">
    <div class="container my-5 text-md-start text-center">
        <div class="row align-items-center">

            <!-- Animation -->
            <div class="col-xl-6 col-md-7 order-md-2 ms-n5" data-aos="fade-up">
                <lottie-player src="<?= base_url() ?>assets/frontend/json/animation-404-v1.json" background="transparent" speed="1" loop autoplay></lottie-player>
            </div>

            <!-- Text -->
            <div class="col-md-5 offset-xl-1 order-md-1">
                <h1 class="display-1 mb-sm-4 mt-n4 mt-sm-n5" data-aos="fade-up">404 Halaman Tidak Ditemukan</h1>
                <p class="mb-md-5 mb-4 mx-md-0 mx-auto pb-2 lead" data-aos="fade-up">Waduuh! Sepertinya Kamu Tersesat di Halaman Yang Tidak Pernah Ada. Daripada sedih mending kembali ke Beranda aja Yuk! 😭</p>
                <a href="<?= base_url() ?>" class="btn btn-lg btn-primary shadow-primary w-sm-auto w-100" data-aos="fade-up">
                    <i class="bx bx-home-alt me-2 ms-n1 lead"></i>
                    Kembali Sekarang
                </a>
            </div>
        </div>
    </div>
</section>