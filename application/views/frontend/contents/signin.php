<!-- Page content -->
<section class="position-relative h-100 pb-4">

    <!-- Sign in form -->
    <div class="container d-flex flex-wrap justify-content-center justify-content-xl-start h-100">
        <div class="w-100 align-self-end pt-1 pt-md-4 pb-4" style="max-width: 526px;">
            <h1 class="text-center text-xl-start animate__animated animate__fadeInUp">Masuk Akun</h1>
            <p class="text-center text-xl-start pb-3 mb-3 animate__animated animate__fadeInUp">Silahkan Masukkan E-mail dan Password Anda Untuk Masuk.</p>
            <!-- Warning alert -->
            <?php if ($this->session->flashdata('register_success')) : ?>
                <div class="alert d-flex alert-success" role="alert">
                    <i class='bx bx-check-circle lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('register_success'); ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($this->session->flashdata('verify_expired')) : ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('verify_expired'); ?>Silahkan <a href="<?= site_url() ?>account/token/resend/<?= $this->session->flashdata('verify_id') ?>" style="font-weight: bold">Kirim Ulang</a> kembali.
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($this->session->flashdata('login_error')) : ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('login_error'); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($this->session->flashdata('email_terdaftar')) : ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('email_terdaftar') ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($this->session->flashdata('email_belum_verifikasi')) : ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        Email Anda sudah terdaftar dan belum melakukan verifikasi. Silahkan klik tautan berikut ini untuk mengirim ulang kode verfikasi. <br>
                        <a class="alert-link" href="<?= site_url() ?>account/token/resend/<?= $this->session->flashdata('email_belum_verifikasi') ?>"> Kirim Kode Sekarang</a>
                    </div>
                </div>
            <?php endif; ?>

            <?php if ($this->session->flashdata('register_error')) : ?>
                <div class="alert d-flex alert-danger" role="alert">
                    <i class='bx bx-error-alt  lead me-3'></i>
                    <div>
                        <?= $this->session->flashdata('register_error') ?>
                    </div>
                </div>
            <?php endif; ?>

            <form class="mb-4" method="post" action="<?= site_url() ?>user/sign_in">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <div class="recaptcha-holder"></div>
                <div class="position-relative mb-4 animate__animated animate__fadeInUp">
                    <label for="email" class="form-label fs-base"><i class='bx bx-envelope me-1'></i>Email</label>
                    <input type="text" autocomplete="off" class="form-control form-control-lg <?= (form_error('email') ? 'frm-error' : '') ?>" id="email" name="email" placeholder="Masukkan Email Anda..." value="<?= set_value('email') ?>">
                    <?= form_error('email', '<small class="text-danger">', '</small>') ?>
                </div>
                <div class="mb-4 animate__animated animate__fadeInUp">
                    <label for="password" class="form-label fs-base"><i class='bx bx-lock-alt me-1'></i>Password</label>
                    <div class="password-toggle">
                        <input type="password" class="form-control form-control-lg <?= (form_error('password') ? 'frm-error' : '') ?>" id="password" name="password" autocomplete="new-password" placeholder="Masukkan Password Anda...">
                        <label class="password-toggle-btn" aria-label="Show/hide password">
                            <input class="password-toggle-check" type="checkbox">
                            <span class="password-toggle-indicator"></span>
                        </label>
                        <?= form_error('password', '<small class="text-danger">', '</small>') ?>
                    </div>
                </div>
                <div class="my-2 d-flex justify-content-between animate__animated animate__fadeInUp">
                    <div class="form-check">
                        <input type="checkbox" id="remember" class="form-check-input">
                        <label class="form-check-label" for="remember"> Ingat Saya </label>
                    </div>
                    <a href="<?= base_url() ?>reset-password" class="float-end text-primary fw-bold">Lupa Password?</a>
                </div>

                <button type="submit" class="btn btn-primary btn-rounded shadow-primary btn-lg w-100">Masuk Akun<i class='bx bx-log-in-circle ms-1'></i></button>
            </form>
            <hr class="my-4 animate__animated animate__fadeInUp">
            <h6 class="text-center mb-4 animate__animated animate__fadeInUp">atau masuk menggunakan</h6>
            <div class="row row-cols-1 row-cols-sm-2">
                <div class="col mb-3 animate__animated animate__fadeInUp">
                    <a href="https://intra.kemendag.go.id/?callback=<?php if (strpos(base_url(), 'localhost') != false) {
                                                                        echo urlencode('http:' . base_url());
                                                                    } else {
                                                                        echo urlencode('https:' . base_url());
                                                                    } ?>" class="btn btn-icon btn-secondary btn-rounded  btn-facebook btn-lg w-100">
                        <img src="<?= base_url() ?>assets/brand/logo-ekemendag.svg" width="18" loading="lazy" alt="Login Intranet" class="me-1">
                        Intranet
                    </a>
                </div>
                <div class="col mb-3 animate__animated animate__fadeInUp">
                    <a href="javascript:void(0)" id="customBtn" class="customGPlusSignIn btn btn-icon btn-rounded btn-secondary btn-google btn-lg w-100">
                        <i class="bx bxl-google fs-xl me-1"></i>
                        Google
                    </a>
                </div>
            </div>
            <h6 class="text-center my-2 animate__animated animate__fadeInUp"> Belum Memiliki Akun? <a class="text-primary btn-link" href="<?= base_url() ?>register">Registrasi Akun<i class="ri-arrow-right-line"></i></a></h6>
        </div>
        <div class="w-100 align-self-end">
            <p class="nav d-block fs-xs text-center text-xl-start pb-2 mb-0">
                &copy; 2022 - <?= date('Y') ?> All rights reserved. <br>
                <a class="nav-link d-inline-block p-0" href="https://www.instagram.com/pdsi.kemendag/" target="_blank" rel="noopener">Pusat Data dan Sistem Informasi Kementerian Perdagangan RI</a>
            </p>
        </div>
    </div>

    <!-- Background -->
    <div class="position-absolute top-0 end-0 w-50 h-100 bg-position-center bg-repeat-0 bg-size-cover d-none d-xl-block" style="background-image: url('<?= base_url() ?>assets/frontend/img/illust-login.png');"></div>
</section>