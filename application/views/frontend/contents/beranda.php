<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->

    <!-- Hero -->
    <section class="overflow-hidden" style="background: radial-gradient(116.18% 118% at 50% 100%, rgba(99, 102, 241, 0.1) 0%, rgba(218, 70, 239, 0.05) 41.83%, rgba(241, 244, 253, 0.07) 82.52%);">
        <div class="container pb-5 mb-5">
            <div class="row">
                <div class="col-md-5 d-flex flex-column pb-3 pb-sm-4 ">
                    <h1 class="display-4 text-center text-md-start pt-4 pt-sm-5 pt-xl-0 mt-2 mt-sm-0 mt-lg-auto" data-aos="fade-up"><?= (isset($title)) ? $title : ''; ?></h1>
                    <p class="fs-lg text-center text-md-start pb-2 pb-md-3 mb-4 mb-lg-5" data-aos="fade-up">Selamat datang Kembali di <?= (isset($title)) ? $title : ''; ?> Sistem Informasi Sarana Perdagangan.</p>
                    <div class="position-relative d-inline-flex align-items-center justify-content-center justify-content-md-start mt-auto pt-3 pt-md-4 pb-xl-2">
                        <a href="<?= base_url() ?>dashboard#sec_statistik" class="btn btn-icon btn-light bg-white stretched-link rounded-circle me-3" data-scroll data-scroll-offset="120">
                            <i class="bx bx-chevron-down"></i>
                        </a>
                        <span class="fs-sm">Lihat Selengkapnya</span>
                    </div>
                </div>
                <div class="col-md-7 align-self-end">
                    <div class="position-relative overflow-hidden pb-3  mx-auto me-md-0" style="max-width: 632px;">
                        <div class="ratio ratio-1x1"></div>
                        <!-- <img src="<?= base_url() ?>assets/frontend/img/landing/app-showcase/hero-phone-1.png" class="rellax position-absolute top-0 start-0 zindex-2" data-rellax-speed="1.6" data-disable-parallax-down="md" alt="Phone">
                        <img src="<?= base_url() ?>assets/frontend/img/landing/app-showcase/hero-phone-2.png" class="rellax position-absolute top-0 start-0" data-rellax-speed="2.8" data-disable-parallax-down="md" alt="Phone"> -->
                        <lottie-player src="<?= base_url() ?>assets/frontend/img/banner-petugas.json" background="transparent" speed="1" class="rellax position-absolute top-0 start-0" loop autoplay></lottie-player>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features -->
    <section id="sec_statistik" class="container pt-lg-2 pt-xl-3 pb-3 pb-xl-5 mt-n2 mt-sm-0 mb-2 mb-md-4 mb-lg-5">
        <div class="row pb-xl-3 mt-4">

            <!-- Parallax gfx -->
            <div class="col-lg-5 d-none d-lg-block">
                <div class="position-relative" style="max-width: 348px;">
                    <lottie-player src="<?= base_url() ?>assets/frontend/img/illust-statistic.json" background="transparent" speed="1" loop autoplay></lottie-player>
                </div>
            </div>

            <!-- Feature list -->
            <div class="col-lg-7">
                <h2 class="h1 text-center text-md-start mb-4" data-aos="fade-up">Statistik Pasar</h2>
                <p class="fs-lg text-muted text-center text-md-start mb-4 mb-xl-5">Berikut adalah statistik-statistik pasar yang ada dalam angka di <?= (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) ? $kab  : 'Sistem Informasi Sarana Perdagangan Kementerian Perdagangan RI'; ?>.</p>
                <?php if (strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'teknis/pdn') : ?>
                    <div class="mb-4 row align-items-center" data-aos="fade-up">
                        <div class="col-md-12">
                            <select class="form-select" id="filter-tahun-tpdak">
                                <option value="">Tahun Jumlah Pasar TP/DAK</option>
                                <?php foreach ($filter_tp as $tahun) : ?>
                                    <option value="<?= $tahun ?>"><?= $tahun ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row row-cols-1 row-cols-sm-2 pt-2 pt-sm-3 pt-xl-2">

                    <!-- Item -->
                    <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                        <div class="d-flex align-items-start pe-xl-3">
                            <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                <i class='bx bx-store bx-md' ></i>
                            </div>
                            <div class="ps-4 ps-sm-3 ps-md-4">
                                <h3 class="pb-1 mb-2" id="stat-jumlah-pasar"></h3>
                                <p class="mb-0">Jumlah Pasar</p>
                            </div>
                        </div>
                    </div>
                    <!-- Item -->
                    <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                        <div class="d-flex align-items-start pe-xl-3">
                            <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                <i class='bx bx-calendar-event bx-md' ></i>
                            </div>
                            <div class="ps-4 ps-sm-3 ps-md-4">
                                <h3 class="pb-1 mb-2" id="stat-pasar-input-hari-ini"></h3>
                                <p class="mb-0">Jumlah Pasar Diinput Bulan Ini</p>
                            </div>
                        </div>
                    </div>
                    <!-- Item -->
                    <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                        <div class="d-flex align-items-start pe-xl-3">
                            <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                <i class='bx bx-calendar-heart bx-md' ></i>
                            </div>
                            <div class="ps-4 ps-sm-3 ps-md-4">
                                <h3 class="pb-1 mb-2" id="stat-pasar-input-tahun-ini"></h3>
                                <p class="mb-0">Jumlah Pasar Diinput Tahun ini</p>
                            </div>
                        </div>
                    </div>
                    <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas') : ?>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bx-check-circle bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-pasar-lapor-lengkap"></h3>
                                    <p class="mb-0">Jumlah Pasar Sudah Lapor (lengkap)</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                <i class='bx bx-message-error bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-pasar-lapor-belum-lengkap"></h3>
                                    <p class="mb-0">Jumlah Pasar Sudah Lapor (Belum Lengkap)</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bx-error bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-pasar-belum-lapor"></h3>
                                    <p class="mb-0">Jumlah Pasar Belum Lapor</p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bxs-hand bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="count-tp"></h3>
                                    <p class="mb-0">Jumlah Pasar Tugas Pembantuan (TP)</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                <i class='bx bx-money-withdraw bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="count-dak"></h3>
                                    <p class="mb-0">Jumlah Pasar Dana Alokasi Khusus (DAK)</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bx-notification-off bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="count-nontpdak"></h3>
                                    <p class="mb-0">Jumlah Pasar Non TP/DAK</p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bx-badge-check bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-pengelola-terdaftar"></h3>
                                    <p class="mb-0">Jumlah Pengelola pasar terdaftar</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bxs-area bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-provinsi-pengelola-terdaftar"></h3>
                                    <p class="mb-0">Jumlah Provinsi yang Terdaftar Pengelola</p>
                                </div>
                            </div>
                        </div>
                        <!-- Item -->
                        <div class="col pb-2 pb-xl-0 mb-4 mb-xl-5" data-aos="flip-up">
                            <div class="d-flex align-items-start pe-xl-3">
                                <div class="d-table bg-secondary rounded-3 flex-shrink-0 p-3 mb-3">
                                    <i class='bx bxs-buildings bx-md' ></i>
                                </div>
                                <div class="ps-4 ps-sm-3 ps-md-4">
                                    <h3 class="pb-1 mb-2" id="stat-kabkot-pengelola-terdaftar"></h3>
                                    <p class="mb-0">Jumlah Kota/Kab yang terdaftar pengelola</p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="container pt-5 pb-2 mt-3 mt-sm-4 mt-xl-5 bg-secondary rounded-3 py-4">
        <div class="row align-items-center pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0">
            <div class="col-lg-10 offset-lg-1 text-center text-md-start">
                <h2>Filter Pencarian</h2>
                <p>Untuk mempermudah pencarian Pasar, silahkan cari melalui form berikut ini.</p>
            </div>
        </div>
        <div class="row align-items-center  pt-3 pt-sm-4 pt-md-0 px-3 px-sm-4 px-lg-0 my-4">
            <div class="col-md-10 offset-md-1">
                <form id="idForm">
                    <div class="row gy-3">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="submit" value="submit">
                        <!-- Form Group -->
                        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                            <div class="col-12 col-md-3">
                                <select name="kabupaten" id="kabupaten">
                                    <option value="">Kabupaten/Kota</option>

                                    <?php foreach ($this->session->userdata('master_kabkota') as $kabkota) : ?>
                                        <option value="<?= $kabkota['daerah_id'] ?>"><?= $kabkota['kab_kota'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                            <div class="col-12 col-md-3">
                                <select name="provinsi" id="provinsi">
                                    <option value="">Provinsi</option>

                                    <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                        <option value="<?= $provinsi['daerah_id'] ?>"><?= $provinsi['provinsi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-12 col-md-3">
                                <select name="kabupaten" id="kabupaten">
                                    <option value="">Kabupaten/Kota</option>
                                </select>
                                <input type="hidden" id="filter-search-kabupaten-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_kabupaten']) ? $this->session->userdata('filter_pasar')['filter_search_kabupaten'] : '' ?>">
                            </div>

                            <div class="col-12 col-md-3">
                                <select name="pengelola" id="pengelola">
                                    <option value="">Semua Pengelola</option>

                                    <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                    <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                    <option value="Swasta">Swasta</option>
                                    <option value="BUMN">BUMN</option>
                                    <option value="BUMD">BUMD</option>
                                    <option value="Perorangan">Perorangan</option>
                                </select>
                                <input type="hidden" id="filter-search-pengelola-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_pengelola']) ? $this->session->userdata('filter_pasar')['filter_search_pengelola'] : '' ?>">
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                            <div class="col-12 col-md-3">
                                <select name="pengelola" id="pengelola">
                                    <option value="">Semua Pengelola</option>

                                    <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                    <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                    <option value="Swasta">Swasta</option>
                                    <option value="BUMN">BUMN</option>
                                    <option value="BUMD">BUMD</option>
                                    <option value="Perorangan">Perorangan</option>
                                </select>
                                <input type="hidden" id="filter-search-pengelola-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_pengelola']) ? $this->session->userdata('filter_pasar')['filter_search_pengelola'] : '' ?>">
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                            <div class="col-12 col-md-3">
                                <select name="pengelola" id="pengelola">
                                    <option value="">Semua Pengelola</option>

                                    <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                    <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                    <option value="Swasta">Swasta</option>
                                    <option value="BUMN">BUMN</option>
                                    <option value="BUMD">BUMD</option>
                                    <option value="Perorangan">Perorangan</option>
                                </select>
                            </div>
                        <?php endif; ?>

                        <div class="col-12 col-md-3">
                            <select name="tipe_pasar" id="tipe_pasar">
                                <option value="">Semua Tipe</option>

                                <?php foreach ($this->session->userdata('tipe_pasar') as $key => $row) : ?>
                                    <option value="<?= $row['tipe_pasar_id'] ?>"><?= $row['tipe_pasar'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <select name="bentuk" id="bentuk">
                                <option value="">Semua Bentuk</option>
                                <option value="1">Permanen</option>
                                <option value="2">Semi Permanen</option>
                                <option value="3">Tanpa Bangunan</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <select name="kepemilikan" id="kepemilikan">
                                <option value="">Semua Kepemilikan</option>
                                <option value="1">Pemerintah Daerah</option>
                                <option value="2">Desa Adat</option>
                                <option value="3">Swasta</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <select name="kondisi" id="kondisi">
                                <option value="">Semua Kondisi</option>
                                <option value="1">Baik</option>
                                <option value="2">Rusak Berat</option>
                                <option value="3">Rusak Ringan</option>
                                <option value="4">Rusak Sedang</option>
                            </select>
                        </div>
                        <!-- Form Group -->
                        <div class="form-group col-lg-2 col-md-12 col-sm-12">
                            <button onClick="getLoadPasarFilter()" type="button" class="btn btn-primary"><i class='bx bx-search-alt me-1'></i>Cari Pasar</button>
                        </div>
                    </div>
                </form>

            </div>

        </div>
    </section>
    <section class="container pt-5 pb-2 mt-3 mt-sm-4 mt-xl-5" id="sec_pasar">
        <div class="row justify-content-center text-center pb-3 mb-sm-2 mb-lg-3">
            <div class="col-xl-6 col-lg-7 col-md-9">
                <h2 class="h1 mb-lg-4" data-aos="fade-up">Daftar Pasar</h2>
                <p class="fs-lg text-muted mb-0" data-aos="fade-up">Berikut adalah Daftar Pasar di <?= (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) ? $kab  : 'Sistem Informasi Sarana Perdagangan Kementerian Perdagangan RI'; ?>.</p>
            </div>
        </div>
        <div id="ajaxpasar"></div>

    </section>



<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->
    <!-- Banner -->
    <section id="sec_banner" class="position-relative py-5">

        <!-- Gradient BG -->
        <div class="position-absolute top-0 start-0 w-100 h-100 bg-light"></div>

        <!-- Content -->
        <div class="container position-relative zindex-2 py-lg-4">
            <div class="row">
                <div class="col-lg-5 d-flex flex-column pt-lg-4 pt-xl-5">
                    <h5 class="my-2" data-aos="fade-up">Selamat Datang di Sistem Informasi</h5>
                    <h1 class="display-3 mb-4" data-aos="fade-up">Sarana <span class="text-primary">Perdagangan</span></h1>
                    <p class="fs-lg mb-5" data-aos="fade-up">Dapatkan info Sebaran Pasar dan Harga Bahan Pokok Se-Indonesia dengan Mudah, Cepat, dan Tepat.</p>

                    <!-- form -->
                    <form class="d-sm-block mb-5" method="post" action="<?= site_url() ?>">
                        <div class="input-group d-block input-group-lg me-3">
                            <div class="row gy-4  rounded">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                <div class="col-md-5 px-0">
                                    <select name="provinsi" id="provinsi" class="bg-white py-2 px-2">
                                        <option value="">Provinsi</option>
                                        <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                            <option value="<?= $provinsi['daerah_id'] ?>" <?= ($provinsi['daerah_id'] == $this->session->flashdata('filter_search_provinsi')) ? 'selected' : '' ?>><?= $provinsi['provinsi'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-md-5 px-0">
                                    <select name="kabupaten" id="kabupaten" class="bg-white py-2 px-2 ">
                                        <option value="">Kabupaten/Kota</option>>
                                    </select>
                                </div>
                                <input type="hidden" id="filter-search-kabupaten-flash" value="<?= $this->session->flashdata('filter_search_kabupaten') ?>">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-icon btn-primary btn-lg w-100">
                                        <i class="bx bx-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="d-flex align-items-center mt-auto mb-3 mb-lg-0 pb-4 pb-lg-0 pb-xl-5">
                        <div class="d-flex me-3">
                            <div class="d-flex align-items-center justify-content-center bg-light rounded-circle" style="width: 52px; height: 52px;">
                                <img src="<?= base_url() ?>assets/frontend/img/pasar-banner-1.png" class="rounded-circle" width="48" alt="Avatar">
                            </div>
                            <div class="d-flex align-items-center justify-content-center bg-light rounded-circle ms-n3" style="width: 52px; height: 52px;">
                                <img src="<?= base_url() ?>assets/frontend/img/pasar-banner-2.png" class="rounded-circle" width="48" alt="Avatar">
                            </div>
                            <div class="d-flex align-items-center justify-content-center bg-light rounded-circle ms-n3" style="width: 52px; height: 52px;">
                                <img src="<?= base_url() ?>assets/frontend/img/pasar-banner-3.png" class="rounded-circle" width="48" alt="Avatar">
                            </div>
                        </div>
                        <span class="fs-sm"><span class="text-primary fw-semibold"><?= count($maps_pasar) ?>+</span> Pasar se-Indonesia telah bergabung dengan kami.</span>
                    </div>
                </div>
                <div class="col-lg-7">

                    <!-- Parallax gfx -->
                    <div class="parallax mx-auto me-lg-0" style="max-width: 648px;">
                        <div class="parallax-layer" data-depth="0.1">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer01.png" alt="Layer">
                        </div>
                        <div class="parallax-layer" data-depth="0.13">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer02.png" alt="Layer">
                        </div>
                        <div class="parallax-layer zindex-5" data-depth="-0.12">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer03.svg" alt="Layer">
                        </div>
                        <div class="parallax-layer zindex-3" data-depth="0.27">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer04.svg" alt="Layer">
                        </div>
                        <div class="parallax-layer zindex-1" data-depth="-0.18">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer05.svg" alt="Layer">
                        </div>
                        <div class="parallax-layer zindex-1" data-depth="0.1">
                            <img src="<?= base_url() ?>assets/frontend/img/landing/online-courses/hero/layer06.svg" alt="Layer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Maps -->
    <section id="sec_statistik" class="mb-5 pt-md-2 pt-lg-4 pt-xl-5">
        <div class="container mt-4 pt-2">
            <div class="row row-cols-md-2 row-cols-1 gy-4 mb-5">
                <div class="col">
                    <h2 class="h1 mb-0" data-aos="fade-up">Sebaran Pasar <br><span class="text-primary">Seluruh Indonesia</span></h2>
                </div>
                <div class="col">
                    <p class="mb-0 fs-lg" data-aos="fade-down">Berikut adalah sebaran pasar seluruh Indonesia yang ditampilkan dalam titik poin di Peta Indonesia. Untuk melihat detail Pasar maka klik Marker Lokasi Pasar yang ada di Peta.</p>
                </div>
            </div>
            <div class="position-relative rounded-3 overflow-hidden">
                <div class="listing-maps">
                    <div id="map" class=" rounded" data-aos="fade-up"></div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>



<!-- Perbandingan Harga -->
<?php $this->load->view('frontend/contents/partials/perbandingan'); ?>