<!-- Breadcrumb -->
<?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>

<!-- Page content -->
<section class="container mt-4 pt-lg-2 pb-4">
    <div class="row">
        <div class="col-md-12">
            <h1>Kebijakan Privasi</h1>
            <p>Ini merupakan komitmen kami untuk menghargai dan melindungi setiap Data Pribadi Pengguna Sistem Informasi Sarana Perdagangan (SISP) (selanjutnya disebut sebagai "Aplikasi").</p>
            <p>Kebijakan Privasi ini menetapkan tindakan atas data atau informasi yang Pengguna berikan kepada Kementerian Perdagangan melalui Aplikasi (selanjutnya disebut sebagai "Data Pribadi").</p>
            <p>Dengan mengakses dan menggunakan Aplikasi, Pengguna dianggap telah membaca, memahami, dan memberikan persetujuannya terhadap pengumpulan dan penggunaan Data Pribadi Pengguna sebagaimana dijelaskan dalam Kebijakan Privasi ini.</p>
            <h6>1. Data Pribadi yang dikumpulkan</h6>
            <p>Dalam memberikan layanan publik, Aplikasi mengumpulkan informasi/data berupa Nama Lengkap, Nomor Induk Kependudukan (NIK), Nomor Seluler, Email dan Lokasi.</p>
            <h6>2. Kapan Data Pribadi dikumpulkan</h6>
            <p>Data Pribadi dikumpulkan dari Pengguna pada saat pengguna melakukan registrasi atau pendaftaran akun pada Sistem Informasi Sarana Perdagangan (SISP).</p>
            <h6>3. Pemakaian Data Pribadi Pengguna</h6>
            <p>Data Pribadi Pengguna akan digunakan sebagai data dasar untuk melakukan verifikasi identitas pemohon layanan dan pemberian layanan oleh Kementerian Perdagangan.</p>
            <h6>4. Keamanan Kerahasiaan Data Pribadi Pengguna</h6>
            <p>Kementerian Perdagangan selalu berusaha melindungi Data Pribadi Pengguna dengan menerapkan keamanan sesuai dengan peraturan perundang-undangan yang berlaku.</p>
            <h6>5. Pemutakhiran Kebijakan Privasi</h6>
            <p>Pemutakhiran Kebijakan Privasi ini akan dilakukan dari waktu ke waktu tanpa pemberitahuan sebelumnya. Kementerian Perdagangan menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apa pun. Dengan tetap mengakses dan menggunakan Aplikasi, maka Pengguna dianggap menyetujui perubahan dalam Kebijakan Privasi ini.</p>
        </div>
    </div>
</section>