<!-- Breadcrumb -->
<?php $this->load->view('frontend/contents/partials/breadcrumb'); ?>

<!-- Page title -->
<h1 class="container pb-4 mt-n1 mt-lg-0" data-aos="fade-up"><?= (isset($title)) ? $title : ''; ?> </h1>

<!-- Service -->
<section class="bg-secondary container mb-md-3 mb-lg-5 p-5">
    <div class="row">
        <div class="col-md-12">
            <h2 class="h3 mb-sm-4 text-primary">Filter Pencarian</h2>
            <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Provinsi)') || strtolower($this->session->userdata('role')) == strtolower('Kontributor SP2KP (Kabupaten/Kota)')) { ?>
                <p class="pb-2 pb-md-3 mb-3">Berikut Daftar Peta Sebaran Pasar di <span class="txt_daerah"><?= $kab ?></span>. Anda juga dapat memasukkan filter khusus untuk menampilkan data yang diinginkan.</p>
            <?php } else { ?>
                <p class="pb-2 pb-md-3 mb-3">Masukkan parameter yang sesuai melalui filter pencarian berikut untuk menampilkan sebaran pada peta yang tersedia dibawah ini.</p>
            <?php } ?>
            <form method="post" action="<?= site_url() ?>sebaran-pasar" class="mb-4 mb-lg-5">
                <div class="row gy-3">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                        <div class="col-12 col-md-3">
                            <select name="kabupaten" id="kabupaten">
                                <option value="">Kabupaten/Kota</option>
                                <?php foreach ($this->session->userdata('master_kabkota') as $kabkota) : ?>
                                    <option value="<?= $kabkota['daerah_id'] ?>" <?= ($kabkota['daerah_id'] == $this->session->flashdata('filter_search_kabupaten')) ? 'selected' : '' ?>><?= $kabkota['kab_kota'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif; ?>

                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                        <div class="col-12 col-md-3">
                            <select name="provinsi" id="provinsi">
                                <option value="">Provinsi</option>

                                <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                    <option value="<?= $provinsi['daerah_id'] ?>" <?= ($provinsi['daerah_id'] == $this->session->flashdata('filter_search_provinsi')) ? 'selected' : '' ?>><?= $provinsi['provinsi'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <select name="kabupaten" id="kabupaten">
                                <option value="">Kabupaten/Kota</option>
                            </select>
                            <input type="hidden" id="filter-search-kabupaten-flash" value="<?= $this->session->flashdata('filter_search_kabupaten') ?>">
                        </div>

                        <div class="col-12 col-md-3">
                            <select name="pengelola" id="pengelola">
                                <option value="">Pengelola</option>
                            </select>
                            <input type="hidden" id="filter-search-pengelola-flash" value="<?= $this->session->flashdata('filter_search_pengelola') ?>">
                        </div>
                    <?php endif; ?>
                    <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                        <div class="col-12 col-md-3">
                            <select name="pengelola" id="pengelola">
                                <option value="">Semua Pengelola</option>
                                <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                    <option value="<?= $pp['email'] ?>"><?= $pp['nama_lengkap'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" id="filter-search-pengelola-flash" value="<?= $this->session->flashdata('filter_search_pengelola') ?>">
                        </div>
                    <?php endif; ?>

                    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                        <div class="col-12 col-md-3">
                            <select name="pengelola" id="pengelola">
                                <option value="">Semua Pengelola</option>

                                <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                    <option value="<?= $pp['email'] ?>" <?= ($pp['email'] == $this->session->flashdata('filter_search_pengelola')) ? 'selected' : '' ?>><?= $pp['nama_lengkap'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif; ?>

                    <div class="col-12 col-md-3">
                        <select name="tipe_pasar" id="tipe_pasar">
                            <option value="">Semua Tipe</option>

                            <?php foreach ($this->session->userdata('tipe_pasar') as $key => $row) : ?>
                                <option value="<?= $row['tipe_pasar_id'] ?>" <?= ($row['tipe_pasar_id'] == $this->session->flashdata('filter_search_tipe')) ? 'selected' : '' ?>><?= $row['tipe_pasar'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-12 col-md-3">
                        <select name="bentuk" id="bentuk">
                            <option value="">Semua Bentuk</option>
                            <option value="1" <?= ($this->session->flashdata('filter_search_bentuk') == 1) ? 'selected' : '' ?>>Permanen</option>
                            <option value="2" <?= ($this->session->flashdata('filter_search_bentuk') == 2) ? 'selected' : '' ?>>Semi Permanen</option>
                            <option value="3" <?= ($this->session->flashdata('filter_search_bentuk') == 3) ? 'selected' : '' ?>>Tanpa Bangunan</option>
                        </select>
                    </div>
                    <div class="col-12 col-md-3">
                        <select name="kondisi" id="kondisi">
                            <option value="">Semua Kondisi</option>
                            <option value="1" <?= ($this->session->flashdata('filter_search_kondisi') == 1) ? 'selected' : '' ?>>Baik</option>
                            <option value="2" <?= ($this->session->flashdata('filter_search_kondisi') == 2) ? 'selected' : '' ?>>Rusak Berat</option>
                            <option value="3" <?= ($this->session->flashdata('filter_search_kondisi') == 3) ? 'selected' : '' ?>>Rusak Ringan</option>
                            <option value="4" <?= ($this->session->flashdata('filter_search_kondisi') == 4) ? 'selected' : '' ?>>Rusak Sedang</option>
                        </select>
                    </div>
                    <!-- Form Group -->
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success mb-3 mb-sm-0 me-sm-3"><i class="bx bx-search me-1"></i>Cari Pasar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row justify-content-center my-4" data-aos="fade-up">
        <div class="col-md-12">
            <div id="map">
            </div>
        </div>
    </div>
</section>