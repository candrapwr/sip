<!-- Hero -->
<section class="jarallax dark-mode bg-dark pt-2 pt-lg-3 pb-lg-5" data-jarallax data-speed="0.35">
    <span class="position-absolute top-0 start-0 w-100 h-100 <?= ($index_pasar['foto_depan'] != null) ? 'bg-dark opacity-70' : '' ?> "></span>
    <div class="jarallax-img" style="background: <?= ($index_pasar['foto_depan'] != null) ? 'url(' . $index_pasar['foto_depan'] . ')' : 'url( ' . base_url() . 'assets/frontend/img/default-head-pasar.jpg)' ?> !important;  background-repeat: no-repeat !important; background-position: center !important; background-size: cover !important;"></div>
    <div class="container position-relative zindex-5 pb-5">

        <input type="hidden" id="action_index" value="<?= $active_index ?>">
        <input type="hidden" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">

        <!-- Breadcrumb -->
        <nav class="py-4" aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item">
                    <a href="<?= base_url() ?>"><i class="bx bx-home-alt fs-lg me-1"></i>SISP</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="javascript:history.back()">Pasar</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page"><?= (isset($title)) ? $title : '404 Halaman Tidak Ditemukan'; ?></li>
            </ol>
        </nav>

        <?php if ($detail_pasar != null) : ?>
            <div class="d-flex pt-3 pb-4 py-sm-4 pt-lg-5">
                <a class="badge bg-white text-nav fs-sm text-decoration-none me-1"><i class='bx bx-time-five me-1'></i><?= ($detail_pasar[0]['jam_operasional_awal'] != null && !empty($detail_pasar[0]['jam_operasional_awal'])) ? date('H:i', strtotime($detail_pasar[0]['jam_operasional_awal'])) : '' ?> - <span><?= ($detail_pasar[0]['jam_operasional_akhir'] != null && !empty($detail_pasar[0]['jam_operasional_akhir'])) ? date('H:i', strtotime($detail_pasar[0]['jam_operasional_akhir'])) : '' ?></span>
                    <a href="tel:<?= $detail_pasar[0]['no_telp'] ?>" class="badge bg-white text-nav fs-sm text-decoration-none"><i class='bx bx-phone me-1'></i><?= (!empty($detail_pasar[0]['no_telp']) ? $detail_pasar[0]['no_telp'] : '-') ?></a>
            </div>
        <?php endif; ?>

        <!-- Badges -->


        <!-- Title -->
        <h1 data-aos="fade-up"><?= ucwords($index_pasar['nama']) ?></h1>
        <!-- <p class="fs-lg text-light opacity-70">Egestas feugiat lorem eu neque suspendisse ullamcorper scelerisque
            aliquam mauris.</p> -->

        <!-- Stats -->
        <div class="d-sm-flex pt-1 pb-5">
            <!-- <div class="d-flex border-sm-end pe-sm-3 me-sm-3 mb-2 mb-sm-0">
                <div class="text-nowrap me-1">
                    <i class="bx bxs-star text-warning"></i>
                    <i class="bx bxs-star text-warning"></i>
                    <i class="bx bxs-star text-warning"></i>
                    <i class="bx bxs-star text-warning"></i>
                    <i class="bx bx-star text-muted opacity-75"></i>
                </div>
                <span class="text-light opacity-70">(1.2K reviews)</span>
            </div> -->
            <div class="d-flex border-sm-end pe-sm-3 me-sm-3 mb-2 mb-sm-0">
                <i class="bx bx-map fs-xl text-light opacity-70 me-1"></i>
                <span class="text-light opacity-70"><?= $index_pasar['kecamatan'] ?>, <?= $index_pasar['kab_kota'] ?>, <?= $index_pasar['provinsi'] ?>, <?= $index_pasar['kode_pos'] ?></span>
            </div>
            <div class="d-flex">
                <i class="bx bx-calendar fs-xl text-light opacity-70 me-1"></i>
                <span class="text-light opacity-70">Waktu Operasional : <?php
                                                                        if ($detail_pasar != null) {
                                                                            $waktu = explode(',', $detail_pasar[0]['waktu_operasional']);
                                                                            if (count($waktu) == 7) {
                                                                                echo '<strong>Setiap Hari</strong>';
                                                                            } else {
                                                                                echo '<strong>' . count($waktu) . ' Hari (' . $detail_pasar[0]['waktu_operasional'] . ')</strong>';
                                                                            }
                                                                        } else {
                                                                            echo '-';
                                                                        }
                                                                        ?></span>
            </div>

        </div>
        <p class="pb-4 mb-3"><?= $index_pasar['deskripsi'] ?></p>
        <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
            <div class="row">
                <div class="col-md-12">
                    <button onclick="updatePasar()" class="btn btn-lg btn-success" data-id="<?= $active_index ?>"><i class="bx bx-edit me-1"></i>Edit Pasar</button>
                    <?php if ($this->session->userdata('email') == $index_pasar['createdby']) { ?>
                        <a href="<?= site_url() ?>pasar/delete/<?= $index_pasar['pasar_id'] ?>" class="btn btn-lg btn-danger"><i class="bx bx-trash me-1"></i> Hapus Pasar</a>
                    <?php } ?>
                </div>
            </div>
        <?php endif; ?>

        <!-- Author -->
        <div class="d-flex align-items-center mt-xl-5 pt-2 pt-md-4 pt-lg-5">
            <img src="<?= base_url() ?>assets/frontend/img/illust-user.svg" class="rounded-circle" width="60" alt="Albert Flores">
            <div class="ps-3">
                <div class="text-light opacity-80 mb-1">Klasifikasi Pengelola</div>
                <h6>
                    <?php
                    if ($pasar_pengelola != null && $pasar_pengelola[0]['klasifikasi_pengelola'] != null) {
                        echo $pasar_pengelola[0]['klasifikasi_pengelola'];
                    } else {
                        if ($detail_pasar != null && $detail_pasar[0]['kepemilikan'] != null) {
                            echo $detail_pasar[0]['kepemilikan'];
                        } else {
                            echo '-';
                        }
                    }
                    ?>
                </h6>
            </div>
        </div>
    </div>
</section>


<!-- Course description -->
<section class="container pt-5 mt-2 mt-lg-4 mt-xl-5">
    <div class="row g-4 mb-3 pb-5">
        <!-- Item -->
        <div class="col-6 col-md-3">
            <div class="card shadow border-0 h-100">
                <div class="text-center">
                    <img src="<?= base_url() ?>assets/frontend/img/pasar-market.svg" class="mx-auto d-block m-2" style="width: 25%;" alt="Icon">
                </div>
                <div class="card-body text-center">
                    <h3 class="card-title h4">Klasifikasi</h3>
                    <p class="card-text fs-lg"><?= ($detail_pasar != null && $detail_pasar[0]['jenis_pasar'] != null) ? $detail_pasar[0]['jenis_pasar'] : '-' ?></p>
                </div>
            </div>
        </div>
        <!-- Item -->
        <div class="col-6 col-md-3">
            <div class="card shadow border-0 h-100">
                <div class="text-center">
                    <img src="<?= base_url() ?>assets/frontend/img/pasar-tag.svg" class="mx-auto d-block m-2" style="width: 25%;" alt="Icon">
                </div>
                <div class="card-body text-center">
                    <h3 class="card-title h4">Tipe Pasar</h3>
                    <p class="card-text fs-lg"><?= ($detail_pasar != null && $detail_pasar[0]['tipe_pasar'] != null) ? $detail_pasar[0]['tipe_pasar'] : '-' ?></p>
                </div>
            </div>
        </div>
        <!-- Item -->
        <div class="col-6 col-md-3">
            <div a class="card shadow border-0 h-100">
                <div class="text-center">
                    <img src="<?= base_url() ?>assets/frontend/img/pasar-shape.svg" class="mx-auto d-block m-2" style="width: 25%;" alt="Icon">
                </div>
                <div class="card-body text-center">
                    <h3 class="card-title h4">Bentuk Pasar</h3>
                    <p class="card-text fs-lg"><?= ($detail_pasar != null && $detail_pasar[0]['bentuk_pasar'] != null) ? $detail_pasar[0]['bentuk_pasar'] : '-' ?></p>
                </div>
            </div>
        </div>
        <!-- Item -->
        <div class="col-6 col-md-3">
            <div class="card shadow border-0 h-100">
                <div class="text-center">
                    <img src="<?= base_url() ?>assets/frontend/img/pasar-fix.svg" class="mx-auto d-block m-2" style="width: 25%;" alt="Icon">
                </div>
                <div class="card-body text-center">
                    <h3 class="card-title h4">Kondisi Pasar</h3>
                    <p class="card-text fs-lg"><?= ($detail_pasar != null && $detail_pasar[0]['kondisi'] != null) ? $detail_pasar[0]['kondisi'] : '-' ?></p>
                </div>
            </div>
        </div>
    </div>





    <div class="row">

        <!-- Sidebar (Course summary) -->
        <aside class="col-lg-3 col-md-4 order-md-1 mb-5">
            <!-- Vertical tabs -->
            <ul class="nav nav-tabs flex-column" role="tablist">
                <li class="nav-item" onClick="setAddUrl('li_tab0')" data-aos="fade-up">
                    <a class="nav-link active py-3 my-1" href="#tab0" data-bs-toggle="tab" role="tab">
                        <i class='bx bx-info-circle opacity-70 me-2'></i>
                        Statistik
                    </a>
                </li>
                <li class="nav-item" onClick="setAddUrl('li_tab1')" data-aos="fade-up">
                    <a class="nav-link py-3 my-1" href="#tab1" data-bs-toggle="tab" role="tab">
                        <i class='bx bx-coin-stack opacity-70 me-2'></i>
                        Harga
                    </a>
                </li>
                <li class="nav-item" onClick="setAddUrl('li_tab2')" data-aos="fade-up">
                    <a class="nav-link py-3 my-1" href="#tab2" data-bs-toggle="tab" role="tab">
                        <i class="bx bx-store-alt opacity-70 me-1"></i>
                        Pedagang
                    </a>
                </li>
                <?php if (!empty($this->session->userdata('token'))) { ?>
                    <li class="nav-item" onClick="setAddUrl('li_tab3')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab3" data-bs-toggle="tab" role="tab">
                            <i class="bx bx-money opacity-70 me-1"></i>
                            Anggaran Pasar
                        </a>
                    </li>
                    <li class="nav-item" onClick="setAddUrl('li_tab4')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab4" data-bs-toggle="tab" role="tab">
                            <i class='bx bx-line-chart opacity-70 me-2'></i>
                            Omset Pedagang
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-item" onClick="setAddUrl('li_tab5')" data-aos="fade-up">
                    <a class="nav-link py-3 my-1" href="#tab5" data-bs-toggle="tab" role="tab">
                        <i class='bx bx-image-alt opacity-70 me-2'></i>
                        Foto Pasar
                    </a>
                </li>
                <?php if (!empty($this->session->userdata('token'))) { ?>
                    <li class="nav-item" onClick="setAddUrl('li_tab6')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab6" data-bs-toggle="tab" role="tab">
                            <i class='bx bx-package opacity-70 me-2'></i>
                            Bongkar Muat
                        </a>
                    </li>
                    <li class="nav-item" onClick="setAddUrl('li_tab7')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab7" data-bs-toggle="tab" role="tab">
                            <i class='bx bx-user-circle  opacity-70 me-2'></i>
                            Pengelola
                        </a>
                    </li>
                    <li class="nav-item" onClick="setAddUrl('li_tab8')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab8" data-bs-toggle="tab" role="tab">
                            <i class="bx bx-group opacity-70 me-1"></i>
                            Paguyuban
                        </a>
                    </li>
                    <li class="nav-item" onClick="setAddUrl('li_tab9')" data-aos="fade-up">
                        <a class="nav-link py-3 my-1" href="#tab9" data-bs-toggle="tab" role="tab">
                            <i class="bx bxs-truck opacity-70 me-1"></i>
                            Pasokan Bapok
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-item" onClick="setAddUrl('li_tab10')" data-aos="fade-up">
                    <a class="nav-link py-3 my-1" href="#tab10" data-bs-toggle="tab" role="tab">
                        <i class="bx bx-receipt opacity-70 me-1"></i>
                        Regulasi
                    </a>
                </li>
            </ul>
        </aside>

        <!-- Content -->
        <div class="col-lg-9 col-md-8 order-md-2 mb-5">
            <!-- Tabs content -->
            <div class="tab-content" id="tabs">
                <div class="tab-pane fade show active" id="tab0" role="tabpanel">
                    <?php $this->load->view('frontend/contents/partials/pasar_detail_informasi_umum') ?>
                </div>
                <div class="tab-pane fade" id="tab1" role="tabpanel">
                    <?php $this->load->view('frontend/contents/partials/pasar_detail_harga') ?>
                </div>
                <div class="tab-pane fade" id="tab2" role="tabpanel">
                    <?php $this->load->view('frontend/contents/partials/pasar_detail_kios') ?>
                </div>
                <?php if (!empty($this->session->userdata('token'))) { ?>
                    <div class="tab-pane fade" id="tab3" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_anggaran') ?>
                    </div>
                    <div class="tab-pane fade" id="tab4" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_anggaran_pedagang') ?>
                    </div>
                <?php } ?>
                <div class="tab-pane fade" id="tab5" role="tabpanel">
                    <?php $this->load->view('frontend/contents/partials/pasar_detail_foto') ?>
                </div>
                <?php if (!empty($this->session->userdata('token'))) { ?>
                    <div class="tab-pane fade" id="tab6" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_bongkar_muat') ?>
                    </div>
                    <div class="tab-pane fade" id="tab7" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_pengelola') ?>
                    </div>
                    <div class="tab-pane fade" id="tab8" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_paguyuban') ?>
                    </div>
                    <div class="tab-pane fade" id="tab9" role="tabpanel">
                        <?php $this->load->view('frontend/contents/partials/pasar_detail_pasokan_bapok') ?>
                    </div>
                <?php } ?>
                <div class="tab-pane fade" id="tab10" role="tabpanel">
                    <?php $this->load->view('frontend/contents/partials/pasar_detail_regulasi') ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Edit Pasar -->
<div class="modal" tabindex="-1" role="dialog" id="modalpasaredit">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Informasi Pasar</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" id="form-data-update" enctype="multipart/form-data">
                <div class="modal-body">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="pasar_update_index" value="<?= $active_index ?>" id="pasar_update_index">
                    <input type="hidden" name="pasar_id_updatee" value="<?= $index_pasar['pasar_id'] ?>" id="pasar_id_updatee">
                    <input type="hidden" name="uri_type" id="uri_type" value="<?= $uri_segment ?>">
                    <div class="row gy-3">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nama Pasar</label>
                                <input type="text" id="name_update" name="name_update" value="<?= $index_pasar['nama'] ?>" class="form-control" placeholder="Masukkan Nama Lengkap Pasar..." required>
                            </div>
                        </div>
                        <div class="col col-12 col-md-12">
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea id="description_update" name="description_update" class="form-control" placeholder="Masukkan Deskripsi Lengkap..." rows="10" required><?= $index_pasar['deskripsi'] ?></textarea>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Provinsi</label>
                                <input type="text" id="provinsi_update" name="provinsi_update" value="<?= $index_pasar['provinsi'] ?>" class="form-control" placeholder="Provinsi" disabled required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Kabupaten/Kota</label>
                                <input type="text" id="kab_kota_update" name="kab_kota_update" value="<?= $index_pasar['kab_kota'] ?>" class="form-control" placeholder="Kabupaten/Kota" disabled required>
                                <input type="hidden" id="kab_id_update" name="kab_id_update" value="<?= substr($index_pasar['daerah_id'], 0, 4) ?>" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <select id="kecamatan_update" name="kecamatan_update" required>
                                    <option selected="" value="">Pilih Kecamatan</option>
                                    <?php
                                    foreach ($kecamatan as $k => $v) {
                                        echo '<option value="' . $v['daerah_id'] . '">' . $v['kecamatan'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="form-group chosen-search">
                                <label>Kelurahan</label>
                                <select id="kelurahan_update" name="daerah_id_update" required>
                                    <option selected="" value="">Pilih Kelurahan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-12">
                            <div class="form-group chosen-search">
                                <label>Alamat</label>
                                <textarea id="address_update" name="address_update" class="form-control" placeholder="Masukkan Alamat Pasar..." required><?= $index_pasar['alamat'] ?></textarea>
                            </div>
                        </div>

                        <?php if (strtolower($this->session->userdata('role')) == 'bapokting') : ?>
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Latitude</label>
                                    <input type="text" id="latitude_update" name="latitude_update" value="<?= $index_pasar['latitude'] ?>" class="form-control" placeholder="Masukkan Latitude..." required>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label>Longitude</label>
                                    <input type="text" id="longitude_update" name="longitude_update" value="<?= $index_pasar['longitude'] ?>" class="form-control" placeholder="Masukkan Longitude..." required>
                                </div>
                            </div>
                        <?php else : ?>
                            <input type="hidden" id="latitude_update" value="<?= $index_pasar['latitude'] ?>" name="latitude_update">
                            <input type="hidden" id="longitude_update" value="<?= $index_pasar['longitude'] ?>" name="longitude_update">
                        <?php endif; ?>

                        <div class="col-12">
                            <div class="form-group">
                                <label>Kode Pos</label>
                                <input type="text" id="post_code_update" autocomplete="off" name="post_code_update" value="<?= $index_pasar['kode_pos'] ?>" class="form-control" placeholder="Masukkan Kode Pos..." required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Foto Depan</label>
                                <input type="file" id="image_font_update" autocomplete="off" name="image_font_update" class="form-control" placeholder="">
                            </div>

                            <?php if (!empty($index_pasar['foto_depan'])) { ?>
                                <p style="color: #373737 !important;">Foto depan saat ini: <a href="<?= $index_pasar['foto_depan'] ?>" target="_blank"> <b><i>foto-depan-<?= str_replace(' ', '-', strtolower($index_pasar['nama'])) ?>.jpg</i></b></a></p>
                            <?php } ?>

                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Foto Dalam</label>
                                <input type="file" id="image_inside_update" autocomplete="off" name="image_inside_update" class="form-control" placeholder="">
                            </div>

                            <?php if (!empty($index_pasar['foto_dalam'])) { ?>
                                <p style="color: #373737 !important;">Foto dalam saat ini: <a href="<?= $index_pasar['foto_dalam'] ?>" target="_blank"> <b><i>foto-dalam-<?= str_replace(' ', '-', strtolower($index_pasar['nama'])) ?>.jpg</i></b></a></p>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal"><i class="bx bx-x me-1"></i>Batal</button>
                    <span id="btn_submit_edit"><button type="submit" id="submit" class="btn btn-sm btn-primary"><i class="bx bx-save me-1"></i>Simpan</button></span>
                </div>
            </form>
        </div>
    </div>
</div>