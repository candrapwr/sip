<?php if ($menu != 'signin' && $menu != 'resetpassword' && $menu != 'register') { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-240649515-1"></script>

    <script>
        $(function() {
            console.log('Selamat Datang di SISP Ministry of Trade RI')
        });

        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-240649515-1');

        $('.input-images-1').imageUploader({
            imagesInputName: 'Foto Dalam Pasar',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 1
        });
        $('.input-images-2').imageUploader({
            imagesInputName: 'Foto Dalam Pasar',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 1
        });
        let timerInterval
        $(document).ready(function() {

            $("#btn_filter").click(function() {
                $("#box_filter").slideToggle();
            });


            $('a[href="#modalpasar"]').click(function(event) {
                event.preventDefault();
                $(this).modal({
                    fadeDuration: 250
                });
            });

            // Init 
            $('input[type="file"]').prettyFile({
                text: "<i class='bx bx-upload me-1'></i> Pilih File"
            });
            $('.mekdi').jPreview();

        })

        $.each($('input:not(:checkbox):not(:radio)[required],textarea[required],select[required]').parent().find('label'), function(k, v) {
            $(v).html($(v).html() + ' <b style="color:red">* <small>(Wajib diisi)</small></b>')
        })

        $.each($('input:radio[required],input:checkbox[required]').parent().parent().children('label'), function(k, v) {
            $(v).html($(v).html() + ' <b style="color:red">* <small>(Wajib diisi)</small></b>')
        })
    </script>
<?php } ?>

<script>
    AOS.init();
</script>