<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->

    <script>
        $(function() {
            $('#tipe_pasar').selectize()
            $('#kepemilikan').selectize()
            $('#kondisi').selectize()
            $('#pengelola').selectize()
            $('#kepemilikan').selectize()
            $('#bentuk').selectize()
            $('#provinsi').selectize()
            $('#kabupaten').selectize()
            $('#select-provinsi-bahan-pokok').selectize()

            <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                $('#kabupaten')[0].selectize.disable()
            <?php endif; ?>

            getHargaBahanPokok()
            getHargaBahanPokokEWS()


            $('#search-bahan-pokok-ews').click(function() {
                const prov = $('#filter-bahan-pokok-ews-provinsi').val()
                const date1 = $('#filter-bahan-pokok-ews-date1').val()
                const date2 = $('#filter-bahan-pokok-ews-date2').val()

                getHargaBahanPokokEWS(prov, date1, date2)
            })

            <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                if ($('#provinsi').val() != '') {
                    const provId = $('#provinsi').val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/kab/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#kabupaten').selectize()[0].selectize.setValue('')
                            $('#kabupaten').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#kabupaten')[0].selectize.disable();
                                } else {
                                    $('#kabupaten')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#kabupaten').selectize()[0].selectize.addOption({
                                            value: row.daerah_id,
                                            text: row.kab_kota
                                        })
                                    })

                                    if ($('#filter-search-kabupaten-flash').val() != '') {
                                        $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                                    }
                                }
                            }
                        }
                    })

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                }

                $('#filter-tahun-tpdak').change(function() {
                    const year = $(this).val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/sum-tpdak/json",
                        type: 'post',
                        data: {
                            year: year,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (response.data.length > 0) {
                                    $.each(response.data, function(i, row) {
                                        $('#count-tp > span').text(row.jml_tp)
                                        $('#count-tp > span').attr('data-stop', row.jml_tp)
                                        $('#count-dak > span').text(row.jml_dak)
                                        $('#count-dak > span').attr('data-stop', row.jml_dak)
                                        $('#count-nontpdak > span').text(row.jml_non_dak)
                                        $('#count-nontpdak > span').attr('data-stop', row.jml_non_dak)
                                    })
                                } else {
                                    $('#count-tp > span').text('0')
                                    $('#count-tp > span').attr('data-stop', '0')
                                    $('#count-dak > span').text('0')
                                    $('#count-dak > span').attr('data-stop', '0')
                                    $('#count-nontpdak > span').text('0')
                                    $('#count-nontpdak > span').attr('data-stop', '0')
                                }
                            }
                        }
                    })
                })
            <?php endif; ?>


            $('#search-bahan-pokok').click(function() {
                const prov = $('#filter-bahan-pokok-provinsi').val()
                const date1 = $('#filter-bahan-pokok-date1').val()
                const date2 = $('#filter-bahan-pokok-date2').val()
                const kab = $('#filter-bahan-pokok-kabupaten').val()
                const pasar = $('#filter-bahan-pokok-pasar').val()

                getHargaBahanPokok(prov, date1, date2, kab, pasar)
            })

            showKab($('#filter-bahan-pokok-provinsi').val())

            $('#filter-bahan-pokok-provinsi').change(function() {
                const provId = $(this).val()

                showKab(provId)
            })

            $('#filter-bahan-pokok-kabupaten').change(function() {
                const kabId = $(this).val()

                if (kabId != '') {
                    getPasar(kabId)
                }
            })

            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                $('#filter-bahan-pokok-provinsi').change(function() {
                    const provId = $(this).val()
                    getPasar(provId)
                })
            <?php endif; ?>

            $('#provinsi').change(function() {
                const provId = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/kab/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#kabupaten').selectize()[0].selectize.setValue('')
                        $('#kabupaten').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#kabupaten')[0].selectize.disable();
                            } else {
                                $('#kabupaten')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#kabupaten').selectize()[0].selectize.addOption({
                                        value: row.daerah_id,
                                        text: row.kab_kota
                                    })
                                })
                            }
                        }


                    }
                })
            })

        })

        function showKab(provId) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                success: function(response) {
                    if (response.code === 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out';
                    } else {
                        if (!response.success) {
                            $('#filter-bahan-pokok-kabupaten')
                                .prop('disabled', true)
                                .html('<option value="">Choose...</option>');
                        } else {
                            $('#filter-bahan-pokok-kabupaten').prop('disabled', false);

                            const optionKab = '<option value="">Choose...</option>';
                            const daerahID = <?= $this->session->userdata('daerah_id') ?>;

                            const options = response.data.map(row => {
                                return `<option value="${row.daerah_id}|${row.kab_kota}">${row.kab_kota}</option>`;
                            });

                            $('#filter-bahan-pokok-kabupaten').html(optionKab + options.join(''));
                        }
                    }
                    getPasar(provId);
                }
            })
        }

        function getPasar(provId) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/pasar/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (response.success) {
                            $('#filter-bahan-pokok-pasar').prop('disabled', false)

                            let optionPasar = '<option value="">Choose...</option>'

                            $.each(response.data, function(i, row) {
                                optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                            })

                            $('#filter-bahan-pokok-pasar').html(optionPasar)
                        } else {
                            $('#filter-bahan-pokok-pasar').prop('disabled', true)
                            $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                        }
                    }
                }
            })
        }

        function getHargaBahanPokok(provinsi = null, date1 = null, date2 = null, kab = null, pasar = null) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/harga/bahan-pokok/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    kab: kab,
                    pasar: pasar,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok').hide()
                    $('#loader-bahan-pokok').show()
                },
                complete: function() {
                    $('#load-bahan-pokok').show()
                    $('#loader-bahan-pokok').hide()
                },
                success: function(response) {
                    $('#date1').text(response.date1);
                    $('#date2').text(response.date2);
                    $('#bahan-pokok-title').text(response.region);

                    if (response.code === 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out';
                    } else {
                        let tableData = '';

                        if (response.data.length > 0) {
                            response.data.forEach(row => {
                                let ket = '-';

                                if (row.ket === 'up') {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else if (row.ket === 'down') {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else if (row.ket === 'equals') {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                }

                                tableData += `
                                            <tr>
                                                <td style="font-weight:600;" class="text-left">${row.jenis_komoditi}</td>
                                                <td>${row.satuan_komoditi}</td>
                                                <td class="text-center">${((row.date1) == 0 || row.date1 == '') ? '-' : row.date1}</td>
                                                <td class="text-center">${((row.date2) == 0 || row.date2 == '') ? '-' : row.date2}</td>
                                                <td class="text-center">${((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '') ? '-' : row.percentage + '%'}</td>
                                                <td class="text-center">${((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '') ? '-' : ket}</td>
                                            </tr>`;
                            });
                        } else {
                            tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>';
                        }

                        $('#harga-bahan-pokok').html(tableData);
                    }

                }
            })
        }

        var summary_data = [];

        function showAjax(jenis, div_id, param = []) {
            var url_api = '';
            var div_data = [];

            if (jenis == 'summary-pasar') {
                url_api = '<?= site_url() ?>dashboard/summary/pasar-json';
            } else if (jenis == 'summary-lapor') {
                url_api = '<?= site_url() ?>dashboard/summary/lapor-json';
            } else if (jenis == 'summary-pengelola') {
                url_api = '<?= site_url() ?>dashboard/summary/pengelola-json';
            } else if (jenis == 'summary-program') {
                url_api = '<?= site_url() ?>dashboard/summary/program-json';
            }

            $.ajax({
                url: url_api,
                type: 'post',
                data: {
                    param: param,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#' + div_id).html('<div class="spinner-border text-primary" role="status"></div>');
                },
                complete: function() {},
                success: function(response, textStatus, xhr) {
                    //console.log(response)
                    if (response.kode == 200) {
                        summary_data[div_id] = response.data;
                        $('#' + div_id).html('<span class="count-text" data-speed="3000" data-stop="' + response.data + '">' + response.data + '</span>');
                    } else {
                        //location.reload();
                    }
                },
                error: function(request, status, error) {
                    //location.reload();
                }
            })
        }

        showAjax('summary-pasar', 'stat-jumlah-pasar', [1, '<?= date('Y-m-d') ?>']);
        showAjax('summary-pasar', 'stat-pasar-input-hari-ini', [2, null]);
        showAjax('summary-pasar', 'stat-pasar-input-tahun-ini', [3, null]);

        <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas') : ?>
            // showAjax('summary-lapor', 'stat-pasar-lapor-lengkap', ['Lengkap']);
            // showAjax('summary-lapor', 'stat-pasar-lapor-belum-lengkap', ['Belum Lengkap']);
            // showAjax('summary-lapor', 'stat-pasar-belum-lapor', ['Belum Lapor']);
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
            showAjax('summary-program', 'count-tp', ['jml_tp']);
            showAjax('summary-program', 'count-dak', ['jml_dak']);
            showAjax('summary-program', 'count-nontpdak', ['jml_non_dak']);
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
            showAjax('summary-pengelola', 'stat-provinsi-pengelola-terdaftar', ['prov']);
            showAjax('summary-pengelola', 'stat-kabkot-pengelola-terdaftar', ['kabkot']);
            showAjax('summary-pengelola', 'stat-pengelola-terdaftar', ['total']);
        <?php endif; ?>

        function getLoadPasar(page = '') {
            $.ajax({
                url: '<?= site_url() ?>dashboard/ajax/pasar-dasboard?page=' + page,
                type: 'post',
                data: {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                beforeSend: function() {
                    if (page == '')
                        $('#ajaxpasar').html(`<div class="row mt-n2 mt-sm-0" style="height:400px;">
                        <div class="col-12 text-center">
                        <lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" loop autoplay class="text-center mx-auto d-block"></lottie-player>
                        </div>
                    </div>`);
                    else
                        $('#ajax_links').html('<div class="spinner-grow text-primary" role="status"></div><div class="spinner-grow text-secondary" role="status"></div><div class="spinner-grow text-success" role="status"></div>');
                },
                complete: function() {},
                success: function(response, textStatus, xhr) {
                    $('#ajaxpasar').html(response);
                },
                error: function(request, status, error) {
                    //location.reload();
                }
            })
        }

        getLoadPasar();

        function getLoadPasarFilter() {
            var form = $('#idForm');
            var actionUrl = '<?= site_url() ?>dashboard/ajax/pasar-dasboard?filter=<?= sha1(md5((string)$this->session->userdata('username'))) ?>';
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: form.serialize(),
                beforeSend: function() {
                    $('#ajaxpasar').html(`<div class="row gy-4" style="height:400px">
                        <div class="col-12 text-center">
                            <lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" loop autoplay></lottie-player>
                            <h6 class="text-primary">Sedang Memuat Daftar Pasar...</h6>
                        </div>
                    </div>`);
                },
                success: function(data) {
                    $('#ajaxpasar').html(data);
                },
                error: function(request, status, error) {
                    //location.reload();
                }
            });
        }


        function getHargaBahanPokokEWS(provinsi = null, date1 = null, date2 = null) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/bahan-pokok-ews/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok-ews').hide()
                    $('#loader-bahan-pokok-ews').show()
                },
                complete: function() {
                    $('#load-bahan-pokok-ews').show()
                    $('#loader-bahan-pokok-ews').hide()
                },
                success: function(response) {
                    $('#date1-ews').text(response.date1 + ' (Rp.)')
                    $('#date2-ews').text(response.date2 + ' (Rp.)')
                    $('#bahan-pokok-title').text(response.region)

                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        let tableData = '';

                        if (response.data.length > 0) {
                            response.data.forEach((row) => {
                                let ket = '-';
                                let percentChange = parseFloat(row.PercentChange);

                                if (percentChange > 0) {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else if (percentChange < 0) {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                }

                                tableData += `
                                                <tr>
                                                    <td style="font-weight:600;" class="text-left">${row.commodity_name}</td>
                                                    <td>${row.kuantitas} ${row.measurement}</td>
                                                    <td class="text-center">${((row.price_2) == 0 || row.price_2 == '') ? '-' : row.price_2}</td>
                                                    <td class="text-center">${((row.price_1) == 0 || row.price_1 == '') ? '-' : row.price_1}</td>
                                                    <td class="text-center">${((row.price_1) == 0 || row.price_1 == '' || (row.price_2) == 0 || row.price_2 == '') ? '-' : row.PercentChange}</td>
                                                    <td class="text-center">${((row.price_1) == 0 || row.price_1 == '' || (row.price_2) == 0 || row.price_2 == '') ? '-' : ket}</td>
                                                </tr>`;
                            });
                        } else {
                            tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>';
                        }


                        $('#harga-bahan-pokok-ews').html(tableData)
                    }
                }
            })
        }
    </script>



<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->

    <script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>

    <script>
        $(function() {
            maps()

            if ($('#filter-search-kabupaten-flash').val() != '') {
                getDaerah('kabupaten', '<?= $this->session->flashdata('filter_search_provinsi') ?>')
            }

            $('#provinsi').selectize()
            $('#kabupaten').selectize()
            $('#select-provinsi-bahan-pokok').selectize()

            getHargaBahanPokok()
            getHargaBahanPokokEWS()

            $('#search-bahan-pokok').click(function() {
                const prov = $('#filter-bahan-pokok-provinsi').val()
                const date1 = $('#filter-bahan-pokok-date1').val()
                const date2 = $('#filter-bahan-pokok-date2').val()
                const kab = $('#filter-bahan-pokok-kabupaten').val()
                const pasar = $('#filter-bahan-pokok-pasar').val()

                getHargaBahanPokok(prov, date1, date2, kab, pasar)
            })

            $('#filter-bahan-pokok-provinsi').change(function() {
                const provId = $(this).val()
                showKab(provId)
                getPasar($('#filter-bahan-pokok-kabupaten').val())
            })

            $('#filter-bahan-pokok-kabupaten').change(function() {
                const provId = $(this).val()
                getPasar(provId)
            })

            $('#search-bahan-pokok-ews').click(function() {
                const prov = $('#filter-bahan-pokok-ews-provinsi').val()
                const date1 = $('#filter-bahan-pokok-ews-date1').val()
                const date2 = $('#filter-bahan-pokok-ews-date2').val()

                getHargaBahanPokokEWS(prov, date1, date2)
            })

            $('#provinsi').on('change', function() {
                var prov = $("#provinsi").val();
                getDaerah('kabupaten', prov);
            })
        })

        function showKab(provId) {
            $.ajax({
                url: "<?= site_url() ?>kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#filter-bahan-pokok-kabupaten').prop('disabled', true)
                    $('#filter-bahan-pokok-kabupaten').html('<option value="">-</option>')
                },
                success: function(response) {
                    if (!response.success) {
                        $('#filter-bahan-pokok-kabupaten').prop('disabled', true)
                        $('#filter-bahan-pokok-kabupaten').html('<option value="">-</option>')
                    } else {
                        $('#filter-bahan-pokok-kabupaten').prop('disabled', false)

                        let optionKab = '<option value="">Semua</option>'

                        $.each(response.data, function(i, row) {
                            optionKab += '<option value="' + row.daerah_id + '|' + row.kab_kota + '">' + row.kab_kota + '</option>'
                        })

                        $('#filter-bahan-pokok-kabupaten').html(optionKab)
                    }
                }
            })
        }

        function getPasar(provId) {
            if (provId != '') {
                $.ajax({
                    url: "<?= site_url() ?>pasar/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    beforeSend: function() {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">-</option>')
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (response.success) {
                            $('#filter-bahan-pokok-pasar').prop('disabled', false)

                            let optionPasar = '<option value="">Semua</option>'

                            $.each(response.data, function(i, row) {
                                optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                            })

                            $('#filter-bahan-pokok-pasar').html(optionPasar)
                        } else {
                            $('#filter-bahan-pokok-pasar').prop('disabled', true)
                            $('#filter-bahan-pokok-pasar').html('<option value="">Semua</option>')
                        }
                    }
                })
            } else {
                $('#filter-bahan-pokok-pasar').prop('disabled', true)
                $('#filter-bahan-pokok-pasar').html('<option value="">-</option>')
            }
        }

        function getHargaBahanPokok(provinsi = null, date1 = null, date2 = null, kab = null, pasar = null) {
            $.ajax({
                url: "<?= site_url() ?>public/bahan-pokok/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    kab: kab,
                    pasar: pasar,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok').hide()
                    $('#loader-bahan-pokok').show()
                },
                complete: function() {
                    $('#load-bahan-pokok').show()
                    $('#loader-bahan-pokok').hide()
                },
                success: function(response) {
                    $('#date1').text(response.date1);
                    $('#date2').text(response.date2);
                    $('#bahan-pokok-title').text(response.region);

                    let tableData = '';

                    if (response.data.length > 0) {
                        response.data.forEach(row => {
                            let ket = '-';
                            if (row.ket === 'up') {
                                ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                            } else if (row.ket === 'down') {
                                ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                            } else if (row.ket === 'equals') {
                                ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                            }

                            tableData += `
                                <tr>
                                    <td style="font-weight:600;" class="text-left">${row.jenis_komoditi}</td>
                                    <td>${row.satuan_komoditi}</td>
                                    <td class="text-center">${((row.date1) == 0 || row.date1 == '') ? '-' : row.date1}</td>
                                    <td class="text-center">${((row.date2) == 0 || row.date2 == '') ? '-' : row.date2}</td>
                                    <td class="text-center">${((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '') ? '-' : row.percentage + '%'}</td>
                                    <td class="text-center">${((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '') ? '-' : ket}</td>
                                </tr>`;
                        });
                    } else {
                        tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>';
                    }


                    $('#harga-bahan-pokok').html(tableData)
                }
            })
        }

        function getHargaBahanPokokEWS(provinsi = null, date1 = null, date2 = null) {
            $.ajax({
                url: "<?= site_url() ?>public/bahan-pokok-ews/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok-ews').hide()
                    $('#loader-bahan-pokok-ews').show()
                },
                complete: function() {
                    $('#load-bahan-pokok-ews').show()
                    $('#loader-bahan-pokok-ews').hide()
                },
                success: function(response) {
                    $('#date1-ews').text(response.date1 + ' (Rp.)')
                    $('#date2-ews').text(response.date2 + ' (Rp.)')
                    $('#bahan-pokok-title-ews').text(response.region)

                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        let tableData = '';

                        if (response.data.length > 0) {
                            response.data.forEach(row => {
                                let ket = '-';
                                let percentChange = parseFloat(row.PercentChange);

                                if (percentChange > 0) {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else if (percentChange < 0) {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                } else {
                                    ket = `<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>`;
                                }

                                tableData += `
                                            <tr>
                                                <td style="font-weight:600;" class="text-left">${row.commodity_name}</td>
                                                <td>${row.kuantitas} ${row.measurement}</td>
                                                <td class="text-center">${(row.price_2 == 0 || row.price_2 == '') ? '-' : row.price_2}</td>
                                                <td class="text-center">${(row.price_1 == 0 || row.price_1 == '') ? '-' : row.price_1}</td>
                                                <td class="text-center">${(row.price_1 == 0 || row.price_1 == '' || row.price_2 == 0 || row.price_2 == '') ? '-' : row.PercentChange}</td>
                                                <td class="text-center">${(row.price_1 == 0 || row.price_1 == '' || row.price_2 == 0 || row.price_2 == '') ? '-' : ket}</td>
                                            </tr>`;
                            });
                        } else {
                            tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>';
                        }


                        $('#harga-bahan-pokok-ews').html(tableData)
                    }
                }
            })
        }

        function maps() {
            var locations = [
                <?php
                if ($maps_pasar != null) : foreach ($maps_pasar as $key => $map) { if (isValidLatLong((string)$map['latitude'], (string)$map['longitude']) !== true) {
                    continue;
                } if ($map['latitude'] != null && $map['longitude'] != NULL) : ?>["<?= $map['nama'] ?>", <?= str_replace(',', '.', $map['latitude']) ?>, <?= str_replace(',', '.', $map['longitude']) ?>, "<?= str_replace(array("\r", "\n"), '', str_replace('"', '', ucwords(strtolower((string)$map['alamat'])))) ?>", "<?= $map['kecamatan'] ?>", "<?= $map['kab_kota'] ?>", "<?= $map['provinsi'] ?>", "<?= $map['pasar_id'] ?>"],
                <?php endif;
                };
                endif;

                ?>
            ];

            //console.log(locations)

            var map = L.map('map').setView([-2.548926, 118.0148634], 8);
            mapLink =
                '<a href="http://openstreetmap.org">OpenStreetMap</a>';
            L.tileLayer(
                'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; ' + mapLink + ' Pusat Data & Sistem Informasi Kemendag RI',
                }).addTo(map);

            var icon = L.icon({
                iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
                iconSize: [50, 50],
                iconAnchor: [22, 65],
                popupAnchor: [-3, -55]
            })

            basemap = {
                Default: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    minZoom: 5,
                }).addTo(map),
                Alternate: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                    minZoom: 5,
                    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                }),
                Satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                    minZoom: 5,
                    subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
                })
            }

            L.control.layers(basemap, null, {
                position: 'bottomleft'
            }).addTo(map);

            var markers = L.markerClusterGroup();

            for (var i = 0; i < locations.length; i++) {
                var url = locations[i][0].toLowerCase()
                url = url.split(' ').join('-')
                var popup = `
                    <div class="p-2">
                        <h6>${locations[i][0]}</h6>
                        <p><i class="bx bx-map me-1"></i> ${locations[i][3]}, ${locations[i][4]}, ${locations[i][5]}</p>
                        <a class="btn btn-primary rounded shadow-primary text-white" href="<?= base_url() ?>pasar/p/${locations[i][7]}-${url}.html">Detail Pasar<i class="bx bx-right-arrow-alt ms-1"></i></a>
                    </div>
                    `;


                var lokasi = L.marker([locations[i][1], locations[i][2]], {
                    icon: icon
                }).bindPopup(popup);

                map.scrollWheelZoom.disable();

                markers.addLayer(lokasi);
                map.addLayer(markers);
                map.fitBounds(markers.getBounds());
            }
        }

        function getDaerah(id, daerah_id) {

            var daerah = '';
            if (id == 'provinsi') {
                daerah = '<option value="">- Pilih Provinsi -</option>';
            } else if (id == 'kabupaten') {
                daerah = '<option value="">- Pilih Kabupaten/kota -</option>';
            } else if (id == 'kecamatan') {
                daerah = '<option value="">- Pilih Kecamatan -</option>';
            } else {
                daerah = '<option value="">- Pilih Kelurahan -</option>';
            }

            $.ajax({
                url: '<?= base_url() ?>home/getDaerah',
                type: 'POST',
                data: {
                    daerah_id: daerah_id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                success: function(response) {
                    response = JSON.parse(response)
                    $('#' + id).selectize()[0].selectize.destroy();

                    var dt = '';
                    $.each(response.data, function(k, v) {

                        if (id == 'provinsi') {
                            dt = v.provinsi;
                        } else if (id == 'kabupaten') {
                            dt = v.kab_kota;
                        } else if (id == 'kecamatan') {
                            dt = v.kecamatan;
                        } else {
                            dt = v.kelurahan;
                        }

                        daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                    })

                    $('#' + id).html(daerah);
                    $('#' + id).selectize()

                    if ($('#filter-search-kabupaten-flash').val() != '') {
                        $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                    }
                },
            })
        }
    </script>
<?php } ?>