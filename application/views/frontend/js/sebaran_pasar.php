<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.min.js"></script>

<script>
    $(document).ready(function() {
        maps()
    })

    $(function() {
        $('#tipe_pasar').selectize()
        $('#kepemilikan').selectize()
        $('#kondisi').selectize()
        $('#pengelola').selectize()
        $('#kepemilikan').selectize()
        $('#bentuk').selectize()
        $('#provinsi').selectize()
        $('#kabupaten').selectize()

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
            $('#kabupaten')[0].selectize.disable()
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>
            $('#pengelola')[0].selectize.disable()
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
            if ($('#provinsi').val() != '') {
                const provId = $('#provinsi').val()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/kab/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#kabupaten').selectize()[0].selectize.setValue('')
                        $('#kabupaten').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#kabupaten')[0].selectize.disable();
                            } else {
                                $('#kabupaten')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#kabupaten').selectize()[0].selectize.addOption({
                                        value: row.daerah_id,
                                        text: row.kab_kota
                                    })
                                })

                                if ($('#filter-search-kabupaten-flash').val() != '') {
                                    $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                                }
                            }
                        }
                    }
                })

                $.ajax({
                    url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            }
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
            if ($('#kabupaten').val() != '') {
                const provId = $('#kabupaten').val()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            }

            $('#kabupaten').change(function() {
                const provId = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            })
        <?php endif; ?>

        $('#provinsi').change(function() {
            const provId = $(this).val()

            $.ajax({
                url: "<?= site_url() ?>dashboard/kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                        }
                    }

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                }
            })
        })
    })

    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').prop('disabled', false)

                        let optionPasar = '<option value="">Choose...</option>'

                        $.each(response.data, function(i, row) {
                            optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                        })

                        $('#filter-bahan-pokok-pasar').html(optionPasar)
                    } else {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                    }
                }
            }
        })
    }

    function maps() {
        var locations = [
            <?php
                if ($maps_pasar != null) : foreach ($maps_pasar as $key => $map) { if (isValidLatLong((string)$map['latitude'], (string)$map['longitude']) !== true) {
                    continue;
                } if ($map['latitude'] != null && $map['longitude'] != NULL) : ?>["<?= $map['nama'] ?>", <?= str_replace(',', '.', $map['latitude']) ?>, <?= str_replace(',', '.', $map['longitude']) ?>, "<?= str_replace(array("\r", "\n"), '', str_replace('"', '', ucwords(strtolower((string)$map['alamat'])))) ?>", "<?= $map['kecamatan'] ?>", "<?= $map['kab_kota'] ?>", "<?= $map['provinsi'] ?>", "<?= $map['pasar_id'] ?>"],
                <?php endif;
                };
                endif;

                ?>
        ];

        var map = L.map('map').setView([-2.548926, 118.0148634], 8);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(map);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map);

        var markers = L.markerClusterGroup();

        for (var i = 0; i < locations.length; i++) {
            var url = locations[i][0].toLowerCase()
            url = url.split(' ').join('-')

            var popup = '<h6>' + locations[i][0] + '</h6>' +
                '<b>Lokasi :</b> ' + locations[i][3] + ', ' + locations[i][4] + ', ' + locations[i][5] +
                '<p><a class="btn mt-2 btn-sm btn-primary shadow-primary text-white" href="<?= base_url() ?>web/pasar/detail/' + locations[i][7] + '">Lihat Detail<i class="bx bx-right-arrow-alt ms-1"></i></a></p>'

            var lokasi = L.marker([locations[i][1], locations[i][2]], {
                icon: icon
            }).bindPopup(popup);

            map.scrollWheelZoom.disable();

            markers.addLayer(lokasi);
            map.addLayer(markers);
            map.fitBounds(markers.getBounds());
        }
    }
</script>