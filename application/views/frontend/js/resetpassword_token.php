<script src="https://www.google.com/recaptcha/api.js?onload=renderGoogleInvisibleRecaptcha&render=explicit&hl=id" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?render=<?= SITE_KEY ?>"></script>

<script>
    var renderGoogleInvisibleRecaptcha = function() {
        for (var i = 0; i < document.forms.length; ++i) {
            var form = document.forms[i];
            var holder = form.querySelector('.recaptcha-holder');
            if (null === holder) {
                continue;
            }

            (function(frm) {

                var holderId = grecaptcha.render(holder, {
                    'sitekey': '6Lco8w4gAAAAABSL_su5DYPy5-i0FUpimPY4nmny',
                    'size': 'invisible',
                    'badge': 'bottomleft', // possible values: bottomright, bottomleft, inline
                    'callback': function(recaptchaToken) {
                        HTMLFormElement.prototype.submit.call(frm);
                    }
                });

                frm.onsubmit = function(evt) {
                    if (frm.checkValidity()) {
                        evt.preventDefault();
                        grecaptcha.execute(holderId);
                    }
                    if (!frm.checkValidity()) {
                        console.log('in')
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    frm.classList.add('was-validated')

                };

            })(form);
        }
    };

    let timerInterval
    $(document).ready(function() {

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            Swal.fire({
                title: 'Success!',
                icon: 'success',
                html: '<?= $this->session->flashdata('message') ?> <br><br> I will close in <b></b> milliseconds.',
                timer: ('<?= $this->session->flashdata('timer') ?>' != '') ? <?= $this->session->flashdata('timer') ?>0 : 1000,
                timerProgressBar: true,
                showClass: {
                    popup: 'animate__animated animate__jackInTheBox'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutDown'
                },
                buttonsStyling: false,
                customClass: {
                    popup: 'border-radius-0'
                },
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    //    console.log('I was closed by the timer')
                }
            })
        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                html: '<?= $this->session->flashdata('message') ?><br><br>' + 'I will close in <b></b> milliseconds.',
                timer: 5000,
                timerProgressBar: true,
                showClass: {
                    popup: 'animate__animated animate__jackInTheBox'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutDown'
                },
                buttonsStyling: false,
                customClass: {
                    popup: 'border-radius-0'
                },
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    //    console.log('I was closed by the timer')
                }
            })
        }

    })



    function update(id, status, table, primary, keterangan) {
        if (status == 'Deleted') {
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang telah terhapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '<?= base_url() ?>master/update_status',
                        type: 'POST',
                        data: {
                            'key': id,
                            'key_name': primary,
                            'table_name': table,
                            'status': status,
                            'keterangan_perubahan': keterangan,
                            'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                        },
                        success: function(response) {
                            response = JSON.parse(response)
                            if (response.kode == 200) {
                                Swal.fire({
                                    title: 'Sukses!',
                                    icon: 'success',
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                                after_update()
                            } else {
                                Swal.fire({
                                    title: 'Gagal!',
                                    icon: 'warning',
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                            }
                        }
                    })
                }
            })
        } else {
            $.ajax({
                url: '<?= base_url() ?>master/update_status',
                type: 'POST',
                data: {
                    'key': id,
                    'key_name': primary,
                    'table_name': table,
                    'status': status,
                    'keterangan_perubahan': keterangan,
                    'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                },
                success: function(response) {
                    response = JSON.parse(response)
                    if (response.kode == 200) {
                        Swal.fire({
                            title: 'Sukses!',
                            icon: 'success',
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                        after_update()
                    } else {
                        Swal.fire({
                            title: 'Gagal!',
                            icon: 'warning',
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                    }
                }
            })
        }

    }
</script>