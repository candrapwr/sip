<script src="https://cdn.jsdelivr.net/npm/smartwizard@5.1.1/dist/js/jquery.smartWizard.min.js"></script>

<script>
    let timerInterval
    $(document).ready(function() {

        <?php if ($this->session->userdata('login') == 'SP2KP' && $this->session->userdata('flex') == 'Kontributor Provinsi') { ?>
            getDaerah('kabupaten', '<?= substr($this->session->userdata('pasar')[0]['daerah_id'], 0, 2) ?>')
        <?php } ?>

        <?php if ($this->session->userdata('login') == 'SP2KP' && $this->session->userdata('flex') == 'Kontributor Kab/Kota') { ?>
            getDaerah('kecamatan', '<?= substr($this->session->userdata('pasar')[0]['daerah_id'], 0, 4) ?>')
        <?php } ?>

        $('#register').click(function() {

            let isValid = true

            let currentStep = $('.tab-pane').eq(2)
            let allField = currentStep.find('.custom-validation')

            allField.each(function(i, element) {
                let getFormID = element.getAttribute('id')
                let formID = $('#' + getFormID)
                let formType = element.getAttribute('type')

                formID.removeClass('is-invalid')
                formID.closest('.validation').find('.text-danger').empty()
                formID.next().find('.selectize-input').removeClass('frm-error')

                if (formID.val() == '') {
                    if (formID.hasClass('selectized')) {
                        formID.closest('.validation').find('.selectize-control').find('.selectize-input').addClass('frm-error')

                        let label = formID.closest('.validation').find('.form-label').text()
                        let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                        formID.closest('.validation').append(message)
                    } else {
                        let label = formID.closest('.validation').find('.form-label').text()
                        let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                        formID.closest('.validation').append(message)
                        formID.addClass('is-invalid')
                    }

                    isValid = false
                }
            })

            if (isValid) {
                $('#register').attr('type', 'submit')
                $('#form-register').trigger('submit')
            }
        })

        let validNik = true
        let validNip = true

        $('#nik').change(function() {
            $.ajax({
                url: '<?= base_url() ?>auth/checkNIK',
                type: 'POST',
                data: {
                    'jenis': 1,
                    'nik': $(this).val(),
                    'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                },
                beforeSend: function() {
                    let message = '<small class="text-secondary wait"> Sedang melakukan pengecekan data...</small>'
                    $('#nik').closest('.validation').append(message)

                    $('#nik').closest('.validation').find('.text-danger').remove()
                    $('#nik').removeClass('is-invalid')
                },
                complete: function() {
                    $('#nik').closest('.validation').find('.wait').remove()
                },
                dataType: 'json',
                success: function(response) {
                    if (response.success == false) {
                        let label = $('#nik').closest('.validation').find('.form-label').text()
                        let message = '<small class="text-danger">' + label + ' sudah terdaftar</small>'
                        $('#nik').closest('.validation').append(message)
                        $('#nik').addClass('is-invalid')

                        validNik = false
                    } else {
                        validNik = true
                    }
                }
            })
        })

        $('#nip').change(function() {
            $.ajax({
                url: '<?= base_url() ?>auth/checkNIK',
                type: 'POST',
                data: {
                    'jenis': 2,
                    'nip': $(this).val(),
                    'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                },
                beforeSend: function() {
                    let message = '<small class="text-secondary wait"> Sedang melakukan pengecekan data...</small>'
                    $('#nip').closest('.validation').append(message)

                    $('#nip').closest('.validation').find('.text-danger').remove()
                    $('#nip').removeClass('is-invalid')
                },
                complete: function() {
                    $('#nip').closest('.validation').find('.wait').remove()
                },
                dataType: 'json',
                success: function(response) {
                    if (response.success == false) {
                        let label = $('#nip').closest('.validation').find('.form-label').text()
                        let message = '<small class="text-danger">' + label + ' sudah terdaftar</small>'
                        $('#nip').closest('.validation').append(message)
                        $('#nip').addClass('is-invalid')

                        validNip = false
                    } else {
                        validNip = true
                    }
                }
            })
        })

        $('#smartwizard').smartWizard({
            selected: 0, // Initial selected step, 0 = first step
            theme: 'dots', // theme for the wizard, related css need to include for other than default theme
            justified: true, // Nav menu justification. true/false
            darkMode: false, // Enable/disable Dark Mode if the theme supports. true/false
            autoAdjustHeight: true, // Automatically adjust content height
            cycleSteps: false, // Allows to cycle the navigation of steps
            backButtonSupport: true, // Enable the back button support
            enableURLhash: true, // Enable selection of the step based on url hash
            transition: {
                animation: 'slide-horizontal', // Effect on navigation, none/fade/slide-horizontal/slide-vertical/slide-swing
                speed: '400', // Transion animation speed
                easing: '' // Transition animation easing. Not supported without a jQuery easing plugin
            },
            toolbarSettings: {
                toolbarPosition: 'bottom', // none, top, bottom, both
                toolbarButtonPosition: 'center', // left, right, center
                showNextButton: true, // show/hide a Next button
                showPreviousButton: true, // show/hide a Previous button
                toolbarExtraButtons: [] // Extra buttons to show on toolbar, array of jQuery input/buttons elements
            },
            anchorSettings: {
                anchorClickable: true, // Enable/Disable anchor navigation
                enableAllAnchors: false, // Activates all anchors clickable all times
                markDoneStep: true, // Add done state on navigation
                markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                removeDoneStepOnNavigateBack: false, // While navigate back done step after active step will be cleared
                enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
            },
            keyboardSettings: {
                keyNavigation: true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                keyLeft: [37], // Left key code
                keyRight: [39] // Right key code
            },
            lang: { // Language variables for button
                next: 'Selanjutnya',
                previous: 'Sebelumnya'
            },
            disabledSteps: [], // Array Steps disabled
            errorSteps: [], // Highlight step with errors
            hiddenSteps: [] // Hidden steps
        });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepIndex, stepDirection) {
            let isValid = true

            let currentStep = $('.tab-pane').eq(stepIndex)
            let allField = currentStep.find('.custom-validation')

            allField.each(function(i, element) {
                let getFormID = element.getAttribute('id')
                let formID = $('#' + getFormID)
                let formType = element.getAttribute('type')

                formID.removeClass('is-invalid')
                formID.closest('.validation').find('.text-danger').empty()
                formID.next().find('.selectize-input').removeClass('frm-error')

                if (stepIndex == 0) {

                    const userID = $('#tipe-pengguna').val().split('-')

                    if (formID.val() == '') {
                        if (getFormID == 'nip') {
                            if (userID[0] != 2 && userID[0] != 12) {
                                if (formID.hasClass('selectized')) {
                                    formID.closest('.validation').find('.selectize-control').find('.selectize-input').addClass('frm-error')

                                    let label = formID.closest('.validation').find('.form-label').text()
                                    let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                    formID.closest('.validation').append(message)
                                } else {
                                    let label = formID.closest('.validation').find('.form-label').text()
                                    let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                    formID.closest('.validation').append(message)
                                    formID.addClass('is-invalid')
                                }

                                isValid = false
                            } else {
                                isValid = true
                            }
                        } else if (getFormID == 'nik') {
                            if (userID[0] != 12) {
                                if (formID.hasClass('selectized')) {
                                    formID.closest('.validation').find('.selectize-control').find('.selectize-input').addClass('frm-error')

                                    let label = formID.closest('.validation').find('.form-label').text()
                                    let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                    formID.closest('.validation').append(message)
                                } else {
                                    let label = formID.closest('.validation').find('.form-label').text()
                                    let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                    formID.closest('.validation').append(message)
                                    formID.addClass('is-invalid')
                                }

                                isValid = false
                            } else {
                                isValid = true
                            }
                        } else {
                            if (formID.hasClass('selectized')) {
                                formID.closest('.validation').find('.selectize-control').find('.selectize-input').addClass('frm-error')

                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                formID.closest('.validation').append(message)
                            } else {
                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                                formID.closest('.validation').append(message)
                                formID.addClass('is-invalid')
                            }
                            isValid = false
                        }

                    } else {
                        if (getFormID == 'nik') {

                            if (formID.val().length != 16) {
                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' harus 16 digit angka</small>'
                                formID.closest('.validation').append(message)
                                formID.addClass('is-invalid')

                                isValid = false
                            } else if (!validNik) {
                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' sudah terdaftar</small>'
                                formID.closest('.validation').append(message)
                                formID.addClass('is-invalid')

                                isValid = false
                            }
                        } else if (getFormID == 'nip' && !validNip) {
                            let label = formID.closest('.validation').find('.form-label').text()
                            let message = '<small class="text-danger">' + label + ' sudah terdaftar</small>'
                            formID.closest('.validation').append(message)
                            formID.addClass('is-invalid')

                            isValid = false
                        }
                    }
                } else if (stepIndex == 1 && stepDirection == 2) {
                    if (formID.val() == '') {
                        if (formID.hasClass('selectized')) {
                            formID.closest('.validation').find('.selectize-control').find('.selectize-input').addClass('frm-error')

                            let label = formID.closest('.validation').find('.form-label').text()
                            let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                            formID.closest('.validation').append(message)
                        } else {
                            let label = formID.closest('.validation').find('.form-label').text()
                            let message = '<small class="text-danger">' + label + ' tidak boleh kosong</small>'
                            formID.closest('.validation').append(message)
                            formID.addClass('is-invalid')
                        }

                        isValid = false
                    } else {
                        if (getFormID == 'password' || getFormID == 'password_confirm') {
                            if (formID.val().length < 8) {
                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' harus 8 digit</small>'
                                formID.closest('.validation').append(message)
                                formID.addClass('is-invalid')

                                isValid = false
                            } else if ($('#password').val() != $('#password_confirm').val()) {
                                let label = formID.closest('.validation').find('.form-label').text()
                                let message = '<small class="text-danger">' + label + ' tidak sesuai</small>'
                                formID.closest('.validation').append(message)
                                formID.addClass('is-invalid')

                                isValid = false
                            }
                        }
                    }
                }
            })
            if (!isValid) {
                return false
            }
        });

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            Swal.fire({
                title: 'Success!',
                icon: 'success',
                html: '<?= $this->session->flashdata('message') ?> <br><br> I will close in <b></b> milliseconds.',
                timer: ('<?= $this->session->flashdata('timer') ?>' != '') ? <?= $this->session->flashdata('timer') ?>0 : 1000,
                timerProgressBar: true,
                showClass: {
                    popup: 'animate__animated animate__jackInTheBox'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutDown'
                },
                buttonsStyling: false,
                customClass: {
                    popup: 'border-radius-0'
                },
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    //    console.log('I was closed by the timer')
                }
            })
        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire({
                title: 'Error!',
                icon: 'error',
                html: '<?= $this->session->flashdata('message') ?><br><br>' + 'I will close in <b></b> milliseconds.',
                timer: 5000,
                timerProgressBar: true,
                showClass: {
                    popup: 'animate__animated animate__jackInTheBox'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutDown'
                },
                buttonsStyling: false,
                customClass: {
                    popup: 'border-radius-0'
                },
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    //    console.log('I was closed by the timer')
                }
            })
        }

    })

    function update(id, status, table, primary, keterangan) {
        if (status == 'Deleted') {
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang telah terhapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '<?= base_url() ?>master/update_status',
                        type: 'POST',
                        data: {
                            'key': id,
                            'key_name': primary,
                            'table_name': table,
                            'status': status,
                            'keterangan_perubahan': keterangan,
                            'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                        },
                        success: function(response) {
                            response = JSON.parse(response)
                            if (response.kode == 200) {
                                Swal.fire({
                                    title: 'Sukses!',
                                    icon: 'success',
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                                after_update()
                            } else {
                                Swal.fire({
                                    title: 'Gagal!',
                                    icon: 'warning',
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                            }
                        }
                    })
                }
            })
        } else {
            $.ajax({
                url: '<?= base_url() ?>master/update_status',
                type: 'POST',
                data: {
                    'key': id,
                    'key_name': primary,
                    'table_name': table,
                    'status': status,
                    'keterangan_perubahan': keterangan,
                    'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                },
                success: function(response) {
                    response = JSON.parse(response)
                    if (response.kode == 200) {
                        Swal.fire({
                            title: 'Sukses!',
                            icon: 'success',
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                        after_update()
                    } else {
                        Swal.fire({
                            title: 'Gagal!',
                            icon: 'warning',
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                    }
                }
            })
        }

    }

    $(function() {
        $('#tipe-pengguna').selectize()
        $('#dinas').selectize()
        $('#kabupaten').selectize()
        $('#kecamatan').selectize()
        $('#kelurahan').selectize()

        $(".number").keypress(function(e) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })

        if ('<?= form_error('tipe_pengguna') ?>') {
            $('#tipe-pengguna').closest('.form-group').find('.selectize-input').addClass('frm-error')
        }

        if ('<?= form_error('provinsi') ?>') {
            $('#provinsi').closest('.form-group').find('.selectize-input').addClass('frm-error')
        }

        if ('<?= form_error('kabupaten') ?>') {
            $('#kabupaten').closest('.form-group').find('.selectize-input').addClass('frm-error')
        }

        if ('<?= form_error('kecamatan') ?>') {
            $('#kecamatan').closest('.form-group').find('.selectize-input').addClass('frm-error')
        }

        if ('<?= form_error('kelurahan') ?>') {
            $('#kelurahan').closest('.form-group').find('.selectize-input').addClass('frm-error')
        }

        if ('<?= $this->session->flashdata('register_success') ?>') {
            alert('<?= $this->session->flashdata('register_success') ?>')
        }


        <?php if ($this->session->userdata('flex') == 'Kontributor Provinsi') { ?>
            $('#tipe-pengguna').selectize()[0].selectize.setValue('18-Kontributor SP2KP (Provinsi)')
        <?php } else if ($this->session->userdata('flex') == 'Kontributor Kab/Kota') { ?>
            $('#tipe-pengguna').selectize()[0].selectize.setValue('19-Kontributor SP2KP (Kabupaten/Kota)')
        <?php } else { ?>
            $('#tipe-pengguna').selectize()[0].selectize.setValue('<?= set_value('tipe_pengguna') ?>')
        <?php } ?>

        <?php if ($this->session->userdata('login') == 'SP2KP') { ?>
            //$('#provinsi').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('pasar')[0]['daerah_id'], 0, 2) ?>');
        <?php } else { ?>
            $('#provinsi').selectize()[0].selectize.setValue('<?= set_value('provinsi') ?>')
            $('#kabupaten').selectize()[0].selectize.setValue('<?= set_value('kabupaten') ?>')
        <?php } ?>
        $('#kecamatan').selectize()[0].selectize.setValue('<?= set_value('kecamatan') ?>')

        let valuePengguna = $('#tipe-pengguna').val()

        let split = valuePengguna.split('-')
        let valID = split[0]

        $('#nip').closest('div.icikiwir').slideUp();
        $('#nik').closest('div.viavallen').removeClass('col-md-6');
        $('#nik').closest('div.viavallen').addClass('col-md-12');
        if (valID != 2 && valID != '') {
            $('#nip').closest('div.icikiwir').slideDown();
            $('#nik').closest('div.viavallen').removeClass('col-md-12');
            $('#nik').closest('div.viavallen').addClass('col-md-6');
        }

        $('#tipe-pengguna').change(function() {
            const value = $(this).val()

            let split = value.split('-')

            const valID = split[0]

            $('#nip').val('')
            $('#nik').val('')

            $('#nip').removeClass('is-invalid')
            $('#nip').closest('.validation').find('.text-danger').empty()

            $('#nik').removeClass('is-invalid')
            $('#nik').closest('.validation').find('.text-danger').empty()

            $('#nip').closest('div.icikiwir').slideUp();
            $('#nik').closest('div.viavallen').removeClass('col-md-6');
            $('#nik').closest('div.viavallen').addClass('col-md-12');
            $('#nip').closest('.form-group').removeClass('validation')
            $('#nip').closest('.form-group').removeClass('custom-validation')

            if (valID != 2) {
                $('#nip').closest('.form-group').addClass('validation')
                $('#nip').closest('.form-group').addClass('custom-validation')
                $('#nip').closest('div.icikiwir').slideDown();
                $('#nik').closest('div.viavallen').removeClass('col-md-12');
                $('#nik').closest('div.viavallen').addClass('col-md-6');
            }
        })
    })

    $('#provinsi').change(function() {
        getDaerah('kabupaten', $(this).val())
    })

    $('#kabupaten').change(function() {
        getDaerah('kecamatan', $(this).val())
    })

    $('#kecamatan').change(function() {
        getDaerah('kelurahan', $(this).val())
    })

    function getDaerah(id, daerah_id) {
        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else if (id == 'kabupaten') {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        } else if (id == 'kecamatan') {
            daerah = '<option value="">- Pilih Kecamatan -</option>';
        } else {
            daerah = '<option value="">- Pilih Kelurahan -</option>';
        }

        $.ajax({
            url: "<?= site_url() ?>daerah/getDaerah",
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#' + id).selectize()[0].selectize.destroy();

                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else if (id == 'kabupaten') {
                        dt = v.kab_kota;
                    } else if (id == 'kecamatan') {
                        dt = v.kecamatan;
                    } else {
                        dt = v.kelurahan;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function() {
                $('#' + id).html(daerah);
                $('#' + id).selectize()
            }
        })
    }
</script>