<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.min.js"></script>

<script>
    $(document).ready(function() {
        maps()

        $('#tbl_pujle').DataTable({
            dom: 'Bfrtip'
        });
        $('#provinsi').selectize();
        $('#kabupaten').selectize()
        if ($('#provinsi').val() != '') {
            const provId = $('#provinsi').val()

            $.ajax({
                url: "<?= site_url() ?>kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                            if ($('#filter-search-kabupaten-flash').val() != '') {
                                $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                            }
                        }
                    }
                }
            })
        }
        $('#provinsi').change(function() {
            const provId = $(this).val()

            $.ajax({
                url: "<?= site_url() ?>kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                        }
                    }
                }
            })
        })
    })

    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').prop('disabled', false)

                        let optionPasar = '<option value="">Choose...</option>'

                        $.each(response.data, function(i, row) {
                            optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                        })

                        $('#filter-bahan-pokok-pasar').html(optionPasar)
                    } else {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                    }
                }
            }
        })
    }

    function maps() {
        var locations = [
            <?php
            if ($maps_pasar != null) : foreach ($maps_pasar as $key => $map){ if(isValidLatLong((string)(explode(',',$map['titik_koordinat'])[0]),(string)(explode(',',$map['titik_koordinat'])[1])) !== true){continue;} if (explode(',', $map['titik_koordinat'])[0] != null && explode(',', $map['titik_koordinat'])[1] != NULL) : ?>["<?= $map['nama_pengecer'] ?>", <?= explode(',', $map['titik_koordinat'])[0] ?>, <?= explode(',', $map['titik_koordinat'])[1] ?>, "<?= str_replace(array("\r", "\n"), '', str_replace('"', '', ucwords(strtolower($map['alamat_pengecer'])))) ?>", "<?= $map['kab_kota'] ?>", "<?= $map['provinsi'] ?>", "<?= str_replace(array("\r", "\n"), '', str_replace('"', '', ucwords(strtolower($map['nama_penanggung_jawab'])))) ?>", "<?= $map['no_telepon'] ?>"],
            <?php endif;
            }
            endif;

            ?>
        ];

        var map = L.map('map').setView([-2.548926, 118.0148634], 8);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
            }).addTo(map);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map);

        var markers = L.markerClusterGroup();

        for (var i = 0; i < locations.length; i++) {
            var popup = '<h6 class="mb-4">' + locations[i][0] + '</h6><hr>' +
                '<b>Alamat :</b> ' + locations[i][3] + ', ' + locations[i][4] + ', ' + locations[i][5] +
                '<br/><b>Penanggung Jawab :</b> ' + locations[i][6]

            var lokasi = L.marker([locations[i][1], locations[i][2]], {
                icon: icon
            }).bindPopup(popup);

            map.scrollWheelZoom.disable();
            markers.addLayer(lokasi);
            map.addLayer(markers);
            map.fitBounds(markers.getBounds());
        }
    }
</script>