<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->
    <script>
        $(function() {
            $('#tipe_pasar').selectize()
        })
    </script>

<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->
    <script>
        $(function() {
            <?php if ($this->session->flashdata('submit_success')) : ?>
                Swal.fire({
                    title: '',
                    text: '<?= $this->session->flashdata('submit_success') ?>',
                    confirmButtonText: 'Tutup'
                })
            <?php endif; ?>

            <?php if ($this->session->flashdata('submit_failed')) : ?>
                Swal.fire({
                    title: '',
                    text: '<?= $this->session->flashdata('submit_failed') ?>',
                    confirmButtonText: 'Tutup'
                })
            <?php endif; ?>

            $('#complaint-provinces').selectize()
            $('#complaint-regions').selectize()
            $('#complaint-markets').selectize()

            $('#complaint-provinces').change(function() {
                const provId = $(this).val()

                const regionId = $('#region-sesflash').val()
                getKab(provId, regionId)
            })

            $('#complaint-regions')[0].selectize.disable();
            $('#complaint-markets')[0].selectize.disable();

            $('#complaint-regions').change(function() {
                if ($(this).val() != '') {
                    getPasar($(this).val(), $('#market-sesflash').val())
                }

            })

            if ('<?= form_error('province') ?>') {
                $('#complaint-provinces').closest('.form-group').find('.selectize-input').addClass('frm-error')
            }

            if ('<?= form_error('region') ?>') {
                $('#complaint-regions').closest('.form-group').find('.selectize-input').addClass('frm-error')
            }

            if ('<?= form_error('market') ?>') {
                $('#complaint-markets').closest('.form-group').find('.selectize-input').addClass('frm-error')
            }


            $('#complaint-provinces').selectize()[0].selectize.setValue('<?= set_value('province') ?>')
            $('#complaint-regions').selectize()[0].selectize.setValue('<?= set_value('region') ?>')
            $('#complaint-markets').selectize()[0].selectize.setValue('<?= set_value('market') ?>')
        })

        function getKab(province_id = null, region_id = null) {
            $.ajax({
                url: "<?= site_url() ?>kab/json",
                type: 'post',
                data: {
                    provinsi_id: province_id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#complaint-regions').selectize()[0].selectize.setValue('')
                    $('#complaint-regions').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.success) {
                        $('#complaint-regions')[0].selectize.enable();
                        $.each(response.data, function(key, value) {
                            $('#complaint-regions').selectize()[0].selectize.addOption({
                                value: value.daerah_id,
                                text: value.kab_kota
                            })
                        })
                    } else {
                        $('#complaint-regions')[0].selectize.disable();
                    }

                    if (region_id != null) {
                        $('#complaint-regions').selectize()[0].selectize.setValue(region_id);
                    }
                }
            })
        }

        function getPasar(regionID, marketID) {
            $.ajax({
                url: "<?= site_url() ?>public/markets/json",
                type: 'post',
                data: {
                    region_id: regionID,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#complaint-markets').selectize()[0].selectize.setValue('')
                    $('#complaint-markets').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    $('#complaint-markets')[0].selectize.disable();

                    if (response.success) {
                        $('#complaint-markets')[0].selectize.enable();

                        $.each(response.data, function(key, value) {
                            $('#complaint-markets').selectize()[0].selectize.addOption({
                                value: value.pasar_id + '|' + value.nama,
                                text: value.nama
                            })
                        })

                        if (marketID != null) {
                            $('#complaint-market').selectize()[0].selectize.setValue(marketID);
                        }
                    }
                }
            })
        }
    </script>
<?php } ?>