<script>
    $(document).ready(function() {
        getBongkarMuat('refresh')
    })

    function getBongkarMuat(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_bongkar_muat')) {
            $("#tbl_bongkar_muat").dataTable().fnDestroy();
            $('#tbl_bongkar_muat').empty();
        }

        var table = $('#tbl_bongkar_muat').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getBongkar_muat',
                data: {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Bulan',
                    data: 'bulan',
                    render: function(k, v, r) {
                        return r.bulan + ' ' + r.tahun;
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_bongkar_muat_id',
                        render: function(k, v, r, m) {
                            return '<div class="row">' +
                                '<div class="col-6 px-1">' +
                                '<button onclick="updateBongkarMuat(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-pencil"></i></button>' +
                                '</div>' +

                                '<div class="col-6 px-1">' +
                                '<button onclick="hapus_data(\'' + r.pasar_bongkar_muat_id + '\', \'Deleted\', \'tbl_pasar_bongkar_muat\', \'pasar_bongkar_muat_id\' )" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-trash"></i></button>' +
                                '</div>'

                        }
                    }
                <?php } ?>
            ],
            columnDefs: [{
                    targets: 2,
                    className: 'text-center'
                },
                {
                    targets: [1, 2],
                    className: 'text-right'
                }
            ],
            initComplete: function(settings, json) {
                let code = json.code

                let table = $('#tbl_bongkar_muat').DataTable();

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },

        });
    }

    function addBongkar_muat() {

        var myModal2 = new bootstrap.Modal(document.getElementById('mdl_bongkar_muat'), {})
        myModal2.show();

        $('#pasar_bongkar_muat_id').val('')
        $('#tahun_bongkar_muat').val('')
        $('#bulan_bongkar_muat').val('')
        $('#jumlah_bongkar_muat').val('')
    }

    function updateBongkarMuat(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/getBongkar_muat",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#pasar_bongkar_muat_id').val(response.pasar_bongkar_muat_id)
                $('#tahun_bongkar_muat').val(response.tahun)
                $('#bulan_bongkar_muat').val(response.bulan)
                $('#jumlah_bongkar_muat').val(response.jumlah)

                var myModal1 = new bootstrap.Modal(document.getElementById('mdl_bongkar_muat'), {})
                myModal1.show();
            }
        })
    }

    $('#form-data-bongkar-muat').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>pasar/setBongkar_muat",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })
</script>