<script>
    $(document).ready(function() {

        getPasokan_bapok('refresh')

        $('#jenis_komoditi_pasokan_bapok').change(function() {
            getVarianKomoditi('varian_komoditi_pasokan_bapok', $(this).val())
            getSatuanKomoditi('satuan_komoditi_pasokan_bapok', $(this).val())

        })

    })

    function getPasokan_bapok(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_pasokan_bapok')) {
            $("#tbl_pasokan_bapok").dataTable().fnDestroy();
            $('#tbl_pasokan_bapok').empty();
        }

        var table = $('#tbl_pasokan_bapok').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getPasokan_bapok',
                data: {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Bulan',
                    data: 'bulan',
                    render: function(k, v, r) {
                        return r.bulan + ' ' + r.tahun
                    }
                },
                {
                    title: 'Jenis',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian',
                    data: 'varian_komoditi',
                },

                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_pasokan_bapok_id',
                        render: function(k, v, r, m) {
                            return '<div class="row">' +
                                '<div class="col-6 px-1">' +
                                '<button onclick="updatePasokanBapok(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-pencil"></i></button>' +
                                '</div>' +

                                '<div class="col-6 px-1">' +
                                '<button onclick="hapus_data(\'' + r.pasar_pasokan_bapok_id + '\', \'Deleted\', \'tbl_pasar_pasokan_bapok\', \'pasar_pasokan_bapok_id\' )" class="btn btn-sm button-backend m-1" title="Remove"><i class="la la-trash"></i></button>' +
                                '</div>'

                        }
                    },
                <?php } ?>
            ],
            initComplete: function(settings, json) {
                let code = json.code

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },

        });
    }

    function addPasokan_bapok() {

        var myModal1 = new bootstrap.Modal(document.getElementById('mdl_pasokan_bapok'), {})
        myModal1.show();

        $('#pasar_pasokan_bapok_id').val('')
        $('#bulan_pasokan_bapok').val('')
        $('#tahun_pasokan_bapok').val('')
        $('#jumlah_pasokan_bapok').val('')
        $('#jenis_komoditi_pasokan_bapok').val('')

        getVarianKomoditi('varian_komoditi_pasokan_bapok')
        getSatuanKomoditi('satuan_komoditi_pasokan_bapok')
    }

    function updatePasokanBapok(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/getPasokan_bapok",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#pasar_pasokan_bapok_id').val(response.pasar_pasokan_bapok_id)
                $('#bulan_pasokan_bapok').val(response.bulan)
                $('#tahun_pasokan_bapok').val(response.tahun)
                $('#jumlah_pasokan_bapok').val(response.jumlah)
                $('#jenis_komoditi_pasokan_bapok').val(response.jenis_komoditi_id)

                getVarianKomoditi('varian_komoditi_pasokan_bapok', response.jenis_komoditi_id, response.varian_komoditi_id)
                getSatuanKomoditi('satuan_komoditi_pasokan_bapok', response.jenis_komoditi_id, response.satuan_komoditi_id)

                var myModal2 = new bootstrap.Modal(document.getElementById('mdl_pasokan_bapok'), {})
                myModal2.show();
            }
        })
    }

    $('#form-data-pasokan-bapok').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>pasar/savePasokan_bapok",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })
</script>