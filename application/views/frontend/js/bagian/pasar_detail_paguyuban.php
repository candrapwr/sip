<script>
    $(document).ready(function() {

    })

    function addPaguyuban() {
        var myModal1 = new bootstrap.Modal(document.getElementById('mdl_pasar_paguyuban'), {})
        myModal1.show();
    }

    $('#form-data-paguyuban').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>pasar/savePaguyuban",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })
</script>