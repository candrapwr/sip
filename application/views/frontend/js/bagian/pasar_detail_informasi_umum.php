<script>
    $(document).ready(function() {
        $('#smartwizard').smartWizard("reset");

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }
    })

    function formDetail(id) {
        var myModalkios = new bootstrap.Modal(document.getElementById('mdl_detail'), {})
        myModalkios.show();

        getUpdateDetail(id)
    }


    $('#smartwizard').smartWizard({
        selected: 0,
        theme: 'arrows',
        autoAdjustHeight: true,
        transitionEffect: 'fade',
        toolbarSettings: {
            showNextButton: true,
            position: 'top', // none|top|bottom|both
        },
        keyboardSettings: {
            keyNavigation: false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
        },
        lang: { // Language variables for button
            next: 'SELANJUTNYA',
            previous: ' SEBELUMNYA',
        },
    });

    $('#smartwizard').on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {

        var forms = document.getElementsByClassName('needs-validation');
        var stat = true;

        var validation = Array.prototype.filter.call(forms, function(form) {
            if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                console.log(nextStepIndex)
                //return false;
                if (!IsValid('step-' + (currentStepIndex)) && stepDirection == 'forward') {
                    stat = false;
                    $('#smartwizard').smartWizard('setError', {
                        stepnum: currentStepIndex,
                        iserror: true
                    });
                    $('html, body').stop().animate({
                        scrollTop: ($('#smartwizard').offset().top - 100) //#DIV_ID is an example. Use the id of your destination on the page
                    }, 'slow');
                } else {

                    if (nextStepIndex == 2 && currentStepIndex == 1) //here is the final step: Note: 0,1,2
                    {
                        $('.btn-fnsh').removeClass('d-none');
                        $('.sw-btn-next').addClass('d-none');
                    } else {
                        $('.btn-fnsh').addClass('d-none');
                        $('.sw-btn-next').removeClass('d-none');

                    }
                    stat = true;
                }

            }
            form.classList.add('was-validated');
        });
        return stat;
    });

    function IsValid(divid) {
        var $div = $('#' + divid);
        var result = true;
        var excludeElement = ["reason"]; //<-- EXCLUDED ELEMENTS IDS
        $.each($div.find(":input"), function(i, input) {
            if ($(input).prop('required') == true) {
                if ($.inArray($(input).attr('id'), excludeElement) < 0 && ($(input).val().length == 0 || $.trim($(input).val()) == '')) {
                    result = false;
                    return;
                }
            }

        });

        return result;
    }

    function getUpdateDetail(index) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/get-detail/json",
            type: 'post',
            data: {
                id: index,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const data = response.data

                    // if (data.kepemilikan == 'Pemerintah Daerah') {
                    //     $('#kepemilikan').selectize()[0].selectize.setValue(1)
                    // } else if (data.kepemilikan == 'Desa Adat') {
                    //     $('#kepemilikan').selectize()[0].selectize.setValue(2)
                    // } else if (data.kepemilikan == 'Swasta') {
                    //     $('#kepemilikan').selectize()[0].selectize.setValue(3)
                    // }

                    if (data.bentuk_pasar == 'Permanen') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(1)
                    } else if (data.bentuk_pasar == 'Semi Permanen') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(2)
                    } else if (data.bentuk_pasar == 'Tanpa Bangunan') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(3)
                    }

                    if (data.kondisi == 'Baik') {
                        $('#kondisi').selectize()[0].selectize.setValue(1)
                    } else if (data.kondisi == 'Rusak Berat') {
                        $('#kondisi').selectize()[0].selectize.setValue(2)
                    } else if (data.kondisi == 'Rusak Ringan') {
                        $('#kondisi').selectize()[0].selectize.setValue(3)
                    } else if (data.kondisi == 'Rusak Sedang') {
                        $('#kondisi').selectize()[0].selectize.setValue(4)
                    }

                    // if (data.pengelola_pasar == 'Pemerintah') {
                    //     $('#pengelola').selectize()[0].selectize.setValue(1)
                    // } else if (data.pengelola_pasar == 'BUMD') {
                    //     $('#pengelola').selectize()[0].selectize.setValue(2)
                    // } else if (data.pengelola_pasar == 'Swasta') {
                    //     $('#pengelola').selectize()[0].selectize.setValue(3)
                    // }

                    $('#waktu_operasional').selectize()[0].selectize.setValue(data.waktu_operasional)

                    $('#phone').val(data.no_telp)
                    $('#fax').val(data.no_fax)
                    $('#tipe_pasar').selectize()[0].selectize.setValue(data.tipe_pasar_id)
                    $('#waktu_operasional').val(data.waktu_operasional)
                    $('#jam_buka').val(data.jam_operasional_awal)
                    $('#jam_tutup').val(data.jam_operasional_akhir)
                    $('#jam_sibuk_awal').val(data.jam_sibuk_awal)
                    $('#jam_sibuk_akhir').val(data.jam_sibuk_akhir)
                    $('#pekerja_tetap').val(data.jumlah_pekerja_tetap)
                    $('#pekerja_nontetap').val(data.jumlah_pekerja_nontetap)
                    $('#luas_bangunan').val(data.luas_bangunan)
                    $('#luas_tanah').val(data.luas_tanah)
                    $('#jumlah_lantai').val(data.jumlah_lantai)
                    $('#tahun_bangun').val(data.tahun_bangun)
                    $('#tahun_renovasi').val(data.tahun_renovasi)
                    getKlasifikasi(data.jenis_pasar)
                    getSarana_prasarana(data.sarana_prasarana)
                }
            }
        })
    }
</script>