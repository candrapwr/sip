<script>
    $(document).ready(function() {
        getAnggaranPedagang('refresh')
    })

    function getAnggaranPedagang(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_anggaran_pedagang')) {
            $("#tbl_anggaran_pedagang").dataTable().fnDestroy();
            $('#tbl_anggaran_pedagang').empty();
        }

        var table = $('#tbl_anggaran_pedagang').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/pasar/anggaran/json',
                data: {
                    refresh: refresh,
                    id: id,
                    jenis_anggaran: 'pedagang',
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Bulan',
                    data: 'tahun',
                    render: function(k, v, r) {
                        return r.bulan + ' ' + r.tahun
                    }
                },
                {
                    title: 'Omset',
                    data: 'omset',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_anggaran_id',
                        render: function(k, v, r, m) {
                            return '<div class="row">' +
                                '<div class="col-6 px-1">' +
                                '<button onclick="updateAnggaranPedagang(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-pencil"></i></button>' +
                                '</div>' +

                                '<div class="col-6 px-1">' +
                                '<button onclick="hapus_data(\'' + r.pasar_anggaran_id + '\',\'Deleted\',\'tbl_pasar_anggaran_pedagang\',\'pasar_anggaran_pedagang_id\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-trash"></i></button>' +
                                '</div>'

                        }
                    }

                <?php } ?>
            ],
            initComplete: function(settings, json) {
                let code = json.code

                let table = $('#tbl_anggaran_pedagang').DataTable();

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },

        });
    }

    function addAnggaranPedagang() {
        var myModal2 = new bootstrap.Modal(document.getElementById('mdl_anggaran_pedagang'), {})
        myModal2.show();

        $('#pasar_anggaran_pedagang_id').val('')
        $('#bulan_anggaran_pedagang').val('')
        $('#tahun_anggaaran_pedagang').val('')
        $('#omset_anggaran_pedagang').val('')
    }

    function updateAnggaranPedagang(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/anggaran/json",
            type: 'post',
            data: {
                action_index: index,
                jenis_anggaran: 'pedagang',
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#pasar_anggaran_pedagang_id').val(response.pasar_anggaran_id)
                $('#bulan_anggaran_pedagang').selectize()[0].selectize.setValue(response.bulan)
                $('#tahun_anggaran_pedagang').val(response.tahun)
                $('#omset_anggaran_pedagang').val(response.omset)



                var myModal1 = new bootstrap.Modal(document.getElementById('mdl_anggaran_pedagang'), {})
                myModal1.show();
            }
        })
    }

    function deleteAnggaranPedagang(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/anggaran/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const actionIndex = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                }
            }
        })
    }

    $('#form-data-anggaran_pedagang').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/anggaran/add",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {
                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code != 200) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })
</script>