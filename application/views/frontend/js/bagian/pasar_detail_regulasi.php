<script>
    $(document).ready(function() {
        getRegulasi('refresh')
    })

    function getRegulasi(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_regulasi')) {
            $("#tbl_regulasi").dataTable().fnDestroy();
            $('#tbl_regulasi').empty();
        }

        var table = $('#tbl_regulasi').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getRegulasi',
                data: {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'pasar_regulasi_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Regulasi',
                    data: 'nama_regulasi',
                    render: function(k, v, r) {
                        return '<b>' + r.nama_regulasi + '</b> <br><small> No. ' + r.no_regulasi + '</small>'
                    }
                },
                {
                    title: 'Pengesahan',
                    data: 'pengesah_regulasi',
                },
                {
                    title: 'Tanggal',
                    data: 'tgl_regulasi',
                },
                {
                    title: 'File',
                    data: 'file_regulasi',
                    render: function(k, v, r) {
                        return '<a href="https://sisp.kemendag.go.id/services/res/upload/file_regulasi/' + r.createdon.substring(0, 4) + '/' + r.file_regulasi + '" target="_blank" class="btn btn-sm button-backend m-1" title="' + r.file_regulasi + '"><i class="la la-file-pdf"></i></a>'
                    }
                }

                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_regulasi_id',
                        render: function(k, v, r, m) {
                            return '<div class="row">' +
                                '<div class="col-6 px-1">' +
                                '<button onclick="updateRegulasi(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-pencil"></i></button>' +
                                '</div>' +

                                '<div class="col-6 px-1">' +
                                '<button onclick="hapus_data(\'' + r.pasar_regulasi_id + '\', \'Deleted\', \'tbl_pasar_regulasi\', \'pasar_regulasi_id\' )" class="btn btn-sm button-backend m-1" title="Remove"><i class="la la-trash"></i></button>' +
                                '</div>'

                        }
                    }
                <?php } ?>
            ],
            initComplete: function(settings, json) {
                let code = json.code

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },

        });
    }

    function addRegulasi() {
        var myModal2 = new bootstrap.Modal(document.getElementById('mdl_regulasi'), {})
        myModal2.show();
    }

    function updateRegulasi(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/getRegulasi",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#pasar_regulasi_id').val(response.pasar_regulasi_id)
                $('#no_regulasi').val(response.no_regulasi)
                $('#nama_regulasi').val(response.nama_regulasi)
                $('#pengesah_regulasi').val(response.pengesah_regulasi)
                $('#tgl_regulasi').val(response.tgl_regulasi)

                var myModal1 = new bootstrap.Modal(document.getElementById('mdl_regulasi'), {})
                myModal1.show();
            }
        })
    }

    $('#form-data-regulasi').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>pasar/saveRegulasi",
            type: 'post',
            data: new FormData(this),
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })
</script>