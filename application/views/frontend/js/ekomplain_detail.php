<?php if (!empty($this->session->userdata('token'))) { ?>
    <!-- Iki Tampilan If Panjenengan Sampun Mlebet Nggih -->
    <script>
        $(function() {
            getTracking()

            $('#form-data-komplain').submit(function(e) {
                e.preventDefault()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/e-komplain/reply",
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        let success = response.success
                        let validation = response.validation

                        $('.form-control').removeClass('frm-error')
                        $('.form-control').closest('div.col').find('.text-danger').remove()

                        if (success) {
                            Swal.fire(
                                'Sukses!',
                                'Tanggapan Anda Berhasil Dikirim!',
                                'success'
                            )
                            // Jika Pengelola
                            let html2 = ` <div class="d-flex align-items-start mb-3">
                                                <img src="https://ui-avatars.com/api/?name=` + response.data.createdby + `&background=1ACBAA&color=fff&size=64" class="rounded-circle" width="40" alt="` + response.data.createdby + `">
                                                <div class="ps-2 ms-1" style="max-width: 348px;">
                                                    <div class="bg-secondary p-3 mb-1" style="border-top-right-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + response.data.keterangan + `</div>
                                                    <div class="fs-sm text-muted">` + response.data.date + `<span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + response.data.status + `</span></div>
                                                </div>
                                            </div>`;
                            $('#tracking-list').append(html2);
                            $("#tracking-list").load(location.href + "#tracking-list");

                        } else if (!success) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url('user/sign_out') ?>'
                            }

                            $.each(validation, function(key, value) {
                                const element = $('#' + key);

                                element.closest('div.form-group').find('.text-danger').remove();

                                element.after(value)

                                value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                                value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                            });
                        }
                    }
                })
            })
        })

        function getTracking() {
            const id = $('#id').val()

            $.ajax({
                url: "<?= site_url() ?>dashboard/e-komplain/detail/tracking/json",
                type: 'post',
                data: {
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                success: function(response) {
                    if (response.data.length > 0) {
                        let html = ''

                        $.each(response.data, function(i, row) {
                            if (row.keterangan != '') {
                                if (row.flag_pengelola == 'Y') {
                                    // Jika Pengelola

                                    html += ` <div class="d-flex align-items-start mb-3">
                                                <img src="https://ui-avatars.com/api/?name=` + row.createdby + `&background=1ACBAA&color=fff&size=64" class="rounded-circle" width="40" alt="` + row.createdby + `">
                                                <div class="ps-2 ms-1" style="max-width: 348px;">
                                                    <div class="bg-secondary p-3 mb-1" style="border-top-right-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + row.keterangan + `</div>
                                                    <div class="fs-sm text-muted">` + row.createdon + `<span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + row.status + `</span></div>
                                                </div>
                                            </div>`;
                                } else {
                                    // Jika Pengabdi Setan
                                    html += `<div class="d-flex align-items-start justify-content-end mb-3">
                                                <div class="pe-2 me-1" style="max-width: 348px;">
                                                    <div class="bg-primary text-light p-3 mb-1" style="border-top-left-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + row.keterangan + `</div>
                                                    <div class="d-flex justify-content-end align-items-center fs-sm text-muted">
                                                    ` + row.createdon + ` <span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + row.status + `</span></i>
                                                    </div>
                                                </div>
                                                <img src="https://ui-avatars.com/api/?name=` + row.createdby + `&background=E70A2B&color=fff&size=64" class="rounded-circle" width="40" alt="` + row.createdby + `">
                                            </div>`;
                                }
                            }
                        })

                        $('#tracking-list').html(html)
                    }


                }
            })
        }
    </script>
<?php } else { ?>
    <!-- Iki Nek Gawe Publik Ya Sayang -->
    <script>
        $(function() {
            getTracking()

            $('#form-data-complaint').submit(function(e) {
                e.preventDefault()

                $.ajax({
                    url: "<?= site_url() ?>public/e-komplain/reply",
                    type: 'post',
                    data: $(this).serialize(),
                    dataType: 'json',
                    success: function(response) {
                        let success = response.success
                        let validation = response.validation

                        $('.form-control').removeClass('frm-error')
                        $('.form-control').closest('div.col').find('.text-danger').remove()
                        if (success) {
                            Swal.fire(
                                'Sukses!',
                                'Tanggapan Anda Berhasil Dikirim!',
                                'success'
                            )
                            // Jika Pengabdi Setan
                            let html2 = `<div class="d-flex align-items-start justify-content-end mb-3">
                                                <div class="pe-2 me-1" style="max-width: 348px;">
                                                    <div class="bg-primary text-light p-3 mb-1" style="border-top-left-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + response.data.keterangan + `</div>
                                                    <div class="d-flex justify-content-end align-items-center fs-sm text-muted">
                                                    ` + response.data.date + ` <span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + response.data.status + `</span></i>
                                                    </div>
                                                </div>
                                                <img src="https://ui-avatars.com/api/?name=` + response.data.createdby + `&background=E70A2B&color=fff&size=64" class="rounded-circle" width="40" alt="` + response.data.createdby + `">
                                            </div>`;
                            $('#tracking-list').append(html2);
                            $("#tracking-list").load(location.href + "#tracking-list");



                        } else if (!success) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url('user/sign_out') ?>'
                            }

                            $.each(validation, function(key, value) {
                                const element = $('#' + key);

                                element.closest('div.form-group').find('.text-danger').remove();

                                element.after(value)

                                value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                                value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                            });
                        }
                    }
                })
            })

        })

        function getTracking() {
            const id = $('#id').val()

            $.ajax({
                url: "<?= site_url() ?>public/e-komplain/detail/tracking/json",
                type: 'post',
                data: {
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                // beforeSend: function() {
                //     $('#varian_komoditi').selectize()[0].selectize.setValue('')
                //     $('#varian_komoditi').selectize()[0].selectize.clearOptions()
                // },
                success: function(response) {
                    if (response.data.length > 0) {
                        let html = ''

                        $.each(response.data, function(i, row) {
                            if (row.keterangan != '') {
                                if (row.flag_pengelola == 'Y') {
                                    // Jika Pengelola

                                    html += ` <div class="d-flex align-items-start mb-3">
                                                <img src="https://ui-avatars.com/api/?name=` + row.createdby + `&background=1ACBAA&color=fff&size=64" class="rounded-circle" width="40" alt="` + row.createdby + `">
                                                <div class="ps-2 ms-1" style="max-width: 348px;">
                                                    <div class="bg-secondary p-3 mb-1" style="border-top-right-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + row.keterangan + `</div>
                                                    <div class="fs-sm text-muted">` + row.createdon + `<span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + row.status + `</span></div>
                                                </div>
                                            </div>`;
                                } else {
                                    // Jika Pengabdi Setan
                                    html += `<div class="d-flex align-items-start justify-content-end mb-3">
                                                <div class="pe-2 me-1" style="max-width: 348px;">
                                                    <div class="bg-primary text-light p-3 mb-1" style="border-top-left-radius: .5rem; border-bottom-right-radius: .5rem; border-bottom-left-radius: .5rem;">
                                                    ` + row.keterangan + `</div>
                                                    <div class="d-flex justify-content-end align-items-center fs-sm text-muted">
                                                    ` + row.createdon + ` <span class="badge bg-success shadow-success fs-sm ms-2"><i class='bx bx-purchase-tag me-1'></i>` + row.status + `</span></i>
                                                    </div>
                                                </div>
                                                <img src="https://ui-avatars.com/api/?name=` + row.createdby + `&background=E70A2B&color=fff&size=64" class="rounded-circle" width="40" alt="` + row.createdby + `">
                                            </div>`;
                                }
                            }
                        })

                        $('#tracking-list').html(html)
                    }
                }
            })
        }
    </script>
<?php } ?>