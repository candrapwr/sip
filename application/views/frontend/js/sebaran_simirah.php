<script>
    $(document).ready(function() {
        $('#tbl_pujle').DataTable({
            dom: 'Bfrtip'
        });
        $('#provinsi').selectize();
        $('#kabupaten').selectize()
        if ($('#provinsi').val() != '') {
            const provId = $('#provinsi').val()

            $.ajax({
                url: "<?= site_url() ?>dashboard/kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                            if ($('#filter-search-kabupaten-flash').val() != '') {
                                $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                            }
                        }
                    }
                }
            })
        }
        $('#provinsi').change(function() {
            const provId = $(this).val()

            $.ajax({
                url: "<?= site_url() ?>dashboard/kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                        }
                    }
                }
            })
        })
    })

    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').prop('disabled', false)

                        let optionPasar = '<option value="">Choose...</option>'

                        $.each(response.data, function(i, row) {
                            optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                        })

                        $('#filter-bahan-pokok-pasar').html(optionPasar)
                    } else {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                    }
                }
            }
        })
    }
</script>