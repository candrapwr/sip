<!-- JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/smartwizard@6/dist/js/jquery.smartWizard.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/dropzone/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/dropzone/jquery.fancybox.min.js"></script>
<script src="<?= base_url() ?>assets/frontend/vendor/dropzone/dropzone.js"></script>
<!-- <script src="<?= base_url() ?>assets/res/js/map-script.js"></script> -->


<script>
    var urlGet = window.location.href.split("#");


    $(document).ready(function() {

        $('#asosiasi_perdagangan_pasar').val('<?= ($pasar_paguyuban != null) ? $pasar_paguyuban[0]['asosiasi_perdagangan_pasar'] : '' ?>')

        $("#btn_filter2").click(function() {
            $("#box_filter2").slideToggle();
        });

        if (typeof urlGet[1] !== "undefined") {
            $('#' + urlGet[1]).trigger('click');
        }

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

        getKelurahan('<?= substr($index_pasar['daerah_id'], 0, 6) ?>');
        $('#kecamatan_update').selectize()[0].selectize.setValue('<?= substr($index_pasar['daerah_id'], 0, 6) ?>');


    });



    function updatePasar() {

        // $('#modalpasaredit').modal({
        //     show: 'true',
        //     fadeDuration: 250
        // });

        var myModal = new bootstrap.Modal(document.getElementById('modalpasaredit'), {})
        myModal.show();

        $('#kelurahan_update').selectize()[0].selectize.setValue('<?= $index_pasar['daerah_id'] ?>');
        $('#kecamatan_update').on('change', function() {
            getKelurahan($("#kecamatan_update").val());
        })

    }

    function getKelurahan(daerah_id) {
        var daerah = '<option value="">- Pilih Kelurahan -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/getDaerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#kelurahan_update').selectize()[0].selectize.destroy();

                $.each(response.data, function(k, v) {
                    daerah += '<option value="' + v.daerah_id + '">' + v.kelurahan + '</option>';
                })

                $('#kelurahan_update').html(daerah);
                $('#kelurahan_update').selectize();
            },
        })
    }

    function goBack() {
        window.history.back();
    }

    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#myDrop", {
        paramName: "files",
        addRemoveLinks: true,
        uploadMultiple: true,
        autoProcessQueue: false,
        parallelUploads: 50,
        maxFilesize: 30, // MB
        acceptedFiles: ".png, .jpeg, .jpg, .gif",
        url: "<?= site_url() ?>dashboard/pasar/save-foto",
    });


    /* Add Files Script*/
    myDropzone.on("success", function(file, message) {
        $("#msg").html(message);
        setTimeout(function() {}, 800);
    });

    myDropzone.on("error", function(data) {
        $("#msg").html('<div class="alert alert-danger">There is some thing wrong, Please try again!</div>');
    });

    myDropzone.on("complete", function(file) {
        $('#add_file').html('<i class="bx bx-upload me-1"></i>Upload File(s)')
        myDropzone.removeFile(file);
        location.reload();
    });

    myDropzone.on("sending", function(file, xhr, formData) {
        formData.append('pasar_id_ft', '<?= $index_pasar['pasar_id'] ?>')
        formData.append('csrf_baseben', '<?= $this->security->get_csrf_hash() ?>')
        $('#add_file').html('<div class="spinner-border text-primary" role="status"></div>')
    });

    $("#add_file").on("click", function() {
        myDropzone.processQueue();
    });

    function removeElement(id) {
        var elem = document.getElementById(id);
        return elem.parentNode.removeChild(elem);
    }

    function setAddUrl(str) {
        window.history.replaceState(null, null, "#" + str);
    }

    function callHapusFt(id) {
        Swal.fire({
            title: 'Apa anda yakin untuk menghapus foto ?',
            showCancelButton: true,
            confirmButtonText: 'Hapus',
            cancelButtonText: 'Batal',
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "<?= site_url() ?>dashboard/pasar/del-foto",
                    type: 'post',
                    data: {
                        foto_id: id,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#a_ft_' + id).html('<div class="spinner-border text-primary" role="status"></div>')
                    },
                    complete: function() {},
                    success: function(response, textStatus, xhr) {
                        removeElement('div_ft_' + id)
                    },
                    error: function(request, status, error) {
                        console.log(error)
                    }
                })
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })

    }

    function getKecamatan(kab) {
        var daerah = '<option value="">- Pilih Kecamatan -</option>';

        $.ajax({
            url: '<?= base_url() ?>daerah/getKecamatan',
            type: 'POST',
            data: {
                kabupaten_id: kab,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#kecamatan_update').selectize()[0].selectize.destroy();

                var dt = '';

                $.each(response.data, function(k, v) {
                    daerah += '<option value="' + v.daerah_id + '">' + v.kecamatan + '</option>';
                })

                $('#kecamatan_update').html(daerah);
                $('#kecamatan_update').selectize()
            },
        })
    }

    $('#penerapan_digital_pasar').on('change', function() {
        var select = $('#penerapan_digital_pasar').val();

        if (select == 'Ya') {
            $('#penerapan_dp').show()
            $('#penerapan_dp').html(`<div class="form-group">
                                        <label class="form-label fw-bold"><i class="bx bxl-android me-1"></i>Metode Penerapan Digitalisasi Pasar</label>
                                        <select data-placeholder="Pilih Metode Penerapan Digitalisasi Pasar" multiple name="metode_penerapan_digital_pasar[]" id="metode_penerapan_digital_pasar">
                                            <option selected="" value="">Pilih Metode Penerapan Digitalisasi Pasar</option>
                                            <option value="Pembayaran">Pembayaran (QRis)</option>
                                            <option value="Pembelian">Pembelian</option>
                                            <option value="Penjualan">Penjualan</option>
                                            <option value="Website">Website</option>
                                            <option value="Pemasaran">Pemasaran</option>
                                            <option value="E-Retribusi">E-Retribusi</option>
                                        </select>
                                    </div>`)

            $("#metode_penerapan_digital_pasar").selectize({
                delimiter: ",",
                persist: false,
                create: function(input) {
                    return {
                        value: input,
                        text: input,
                    };
                },
            });

        } else {
            $('#penerapan_dp').hide()
            $('#penerapan_dp').html('')
        }
    });

    $('#pembiayaan_lembaga_keuangan').on('change', function() {
        var select = $('#pembiayaan_lembaga_keuangan').val();

        if (select == 'Ya') {
            $('#form_lembaga_keuangan').show()
            $('#form_lembaga_keuangan').html(`<div class="form-group">
                                        <label class="form-label fw-bold">Lembaga Keuangan</label>
                                        <input type="text" class="form-control my-form-control" autocomplete="off" name="lembaga_keuangan" id="lembaga_keuangan" placeholder="Masukkan Lembaga Keuangan...">
                                    </div>`)
        } else {
            $('#form_lembaga_keuangan').hide()
            $('#form_lembaga_keuangan').html('')
        }
    });

    $('#sertifikasi_sni').on('change', function() {
        var select = $('#sertifikasi_sni').val();

        if (select == 'Ya') {
            $('#form_tahun_sni_pasar_rakyat').show()
            $('#form_tahun_sni_pasar_rakyat').html(`<div class="form-group">
                                        <label class="form-label fw-bold">Tahun Sertifikasi SNI Pasar Rakyat</label>
                                        <input type="number" class="form-control my-form-control" autocomplete="off" name="tahun_sni_pasar_rakyat" id="tahun_sni_pasar_rakyat" placeholder="Masukkan Tahun Sertifikasi SNI Pasar Rakyat...">
                                    </div>`)
        } else {
            $('#form_tahun_sni_pasar_rakyat').hide()
            $('#form_tahun_sni_pasar_rakyat').html('')
        }
    });
</script>

<script>
    $(function() {
        $('#jenis_komoditi').selectize()
        $('#varian_komoditi').selectize()
        $('#satuan_komoditi').selectize()
        $('#tipe_pasar').selectize()
        $('#bentuk_pasar').selectize()
        $('#kondisi').selectize()
        $('#pengelola').selectize()
        $('#bentuk').selectize()
        $('#provinsi').selectize()
        $('#kabupaten').selectize()
        $('#program_pasar').selectize()
        $('#waktu_operasional').selectize()

        // $('#varian_komoditi')[0].selectize.disable();
        // $('#satuan_komoditi')[0].selectize.disable();

        $('#jenis_komoditi').change(function() {
            getVarianKomoditi('varian_komoditi', $(this).val())
            getSatuanKomoditi('satuan_komoditi', $(this).val())

        })

        $(".price").keypress(function(e) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $('.price').keyup(function() {
            let val = $(this).val()
            val = val.replace(/\./g, '')

            $(this).val(formatRupiah(val, ''))
        })

        getHarga(null, 'refresh')
        getKios('refresh')
        getAnggaran('refresh')

        $("#mdl_harga").on('hidden.bs.modal', function() {
            $('#form-data-harga')[0].reset();
            $('#harga_komoditi_id').val('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
        });

        $("#mdl_kios").on('hidden.bs.modal', function() {
            $('#form-data-kios')[0].reset();
            $('#pasar_bangunan_id').val('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
        });

        $('#filter-harga-komoditi').click(function() {
            const date1 = $('#filter-harga-komoditi-date1').val()

            getHarga(date1)
        })

        $('#tabs').on("click", "li", function(event) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });
    })

    function add(aksi) {

        var myModalx = new bootstrap.Modal(document.getElementById('mdl_' + aksi), {})
        myModalx.show();
    }

    $('#form-data-update').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/update",
            type: 'post',
            data: new FormData(this),
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function() {
                $('#btn_submit_edit').html('<div class="spinner-border text-info" role="status"></div>')
            },
            complete: function() {
                $('#btn_submit_edit').html('<button type="submit" id="submit" class="btn btn-lg btn-primary"><i class="bx bx-save me-1"></i>Simpan</button>')
            },
            success: function(response) {
                let success = response.success
                let validation = response.validation

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {
                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: 'success',
                    }).then(function() {
                        const actionIndex = response.action_index

                        if (response.uri_segment == 1) {
                            window.location.href = '<?= site_url() ?>dashboard/pasar/detail/' + actionIndex
                        } else if (response.uri_segment == 2) {
                            window.location.href = '<?= site_url() ?>sebaran-pasar/detail/' + actionIndex
                        }
                    })
                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text' || element.attr('type') == 'file') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    $('#form-data-harga').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/harga/add",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    if (response.isUpdate) {
                        Swal.fire({
                            title: 'Data Berhasil Disimpan',
                        }).then(function() {
                            const actionIndex = $('#action_index').val()
                            window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                        })
                    } else {
                        Swal.fire({
                            title: 'Data berhasil disimpan',
                            text: 'Apakah Anda ingin menambahkan data kembali? ',
                            showDenyButton: true,
                            showCancelButton: false,
                            confirmButtonText: 'Ya',
                            denyButtonText: `Tutup`,
                            allowOutsideClick: false
                        }).then((result) => {
                            if (result.isConfirmed) {
                                getHarga(null, 'refresh')
                                $('#jenis_komoditi')[0].selectize.setValue('')
                                $("#harga").val('')
                            } else if (result.isDenied) {
                                const actionIndex = $('#action_index').val()
                                window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                            }
                        })
                    }
                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    $('#form-data-detail').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/save-detail",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {
                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: 'success',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('pasar/detail/') ?>' + actionIndex
                    })
                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    $('#form-data-kios').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/kios/add",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    $('#form-data-pedagang').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>pasar/savePedagang",
            type: 'post',
            data: new FormData(this),
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    const actionIndex = $('#action_index').val()

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    })

                } else if (!success) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    } else if (response.code == 404) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="bx bx-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }

                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })


    function getHarga(start_date = null, refresh = null) {
        const id = $('#pasar_id').val()
        // var uri = <?= !empty($this->token) ? 'dashboard/harga/json' : 'public/pasar/kios/json' ?>

        if ($.fn.DataTable.isDataTable('#tbl_harga')) {
            $("#tbl_harga").dataTable().fnDestroy();
            $('#tbl_harga').empty();
        }

        var table = $('#tbl_harga').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/harga/json',
                data: {
                    refresh: refresh,
                    id: id,
                    start_date: start_date,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Jenis',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian',
                    data: 'varian_komoditi',
                },
                {
                    title: 'Harga',
                    data: 'harga',
                },
                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Created By',
                    data: 'createdby',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_harga_komoditi_id',
                        render: function(k, v, r, m) {
                            if (r.action_button) {
                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Update"><i class="la la-pencil"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                    '<button onclick="deleteHarga(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Remove"><i class="la la-trash"></i></button>' +
                                    '</div>'
                            } else {
                                return ''
                            }

                        }
                    }
                <?php } ?>
            ],
            initComplete: function(settings, json) {
                let code = json.code

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }

                let table = $('#tbl_harga').DataTable();

                $.each(json.data, function(i, row) {
                    if (row.action == true) {
                        table.column(5).visible(true)
                    } else {
                        table.column(5).visible(false)
                    }
                })

                $('#title-date-harga').text(json.date)
            },
            scrollX: true,
            displayLength: 10
        });

        $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });

    }

    function getKios(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_kios')) {
            $("#tbl_kios").dataTable().fnDestroy();
            $('#tbl_kios').empty();

            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        }

        var table = $('#tbl_kios').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            searching: false,
            ordering: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>public/pasar/kios/json',
                data: {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'Jenis Bangunan',
                    data: 'jenis',
                },
                {
                    title: 'Jumlah Bangunan',
                    data: 'jumlah',
                },
                {
                    title: 'Jumlah Pedagang',
                    data: 'jumlah_pedagang',
                }

                <?php if (!empty($this->session->userdata('token'))) { ?>, {
                        title: 'Aksi',
                        data: 'pasar_bangunan_id',
                        render: function(k, v, r, m) {
                            return '<button onclick="updateKios(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Edit"><i class="la la-pencil"></i></button>' +

                                '<button onclick="deleteKios(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm button-backend m-1" title="Hapus"><i class="la la-trash"></i></button>'

                        }
                    }
                <?php } ?>
            ],
            columnDefs: [{
                    targets: 3,
                    className: 'text-center',
                    width: 100
                },
                {
                    targets: [1, 2],
                    className: 'text-right'
                },
            ],
            initComplete: function(settings, json) {
                let code = json.kode

                let table = $('#tbl_kios').DataTable();

                $.each(json.data, function(i, row) {
                    if (row.action) {
                        table.column(3).visible(true)
                    } else {
                        table.column(3).visible(false)
                    }
                })

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },
            scrollX: true,
            displayLength: 10
        });
    }

    function update(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/harga/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                // jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                const comID = response.jenis_komoditi_id + '|' + response.jenis_komoditi

                $('#jenis_komoditi').selectize()[0].selectize.setValue(comID)

                getVarianKomoditi('varian_komoditi', response.jenis_komoditi_id, response.varian_komoditi_id)
                getSatuanKomoditi('satuan_komoditi', response.jenis_komoditi_id, response.satuan_komoditi_id)

                $('#harga').val(response.harga)
                $('#jumlah').val(response.jumlah)
                $('#harga_komoditi_id').val(response.pasar_harga_komoditi_id)



                var myModalharga = new bootstrap.Modal(document.getElementById('mdl_harga'), {})
                myModalharga.show();
            }
        })
    }

    function updateKios(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/kios/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                $('#pasar_bangunan_id').val(response.pasar_bangunan_id)
                $('#jumlah_bangunan').val(response.jumlah)
                $('#jumlah_pedagang').val(response.jumlah_pedagang)

                if (response.jenis == 'Los') {
                    $('input[name="jenis_bangunan"][value="1"]').prop('checked', true)
                } else if (response.jenis == 'Kios') {
                    $('input[name="jenis_bangunan"][value="2"]').prop('checked', true)
                } else if (response.jenis == 'Dasaran') {
                    $('input[name="jenis_bangunan"][value="3"]').prop('checked', true)
                }


                var myModalkios = new bootstrap.Modal(document.getElementById('mdl_kios'), {})
                myModalkios.show();
            }
        })
    }

    function getKlasifikasi(select = null) {
        var klasifiksi = '<option value="">- Pilih Klasifikasi -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/getKlasifikasi',
            type: 'POST',
            data: {
                status: 'Aktif',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#klasifikasi').selectize()[0].selectize.destroy();

                $.each(response.data, function(k, v) {
                    klasifikasi += '<option value="' + v.klasifikasi + '">' + v.klasifikasi + '</option>';
                })


                $('#klasifikasi').html(klasifikasi);
                $('#klasifikasi').selectize();
                if (select != null) {
                    $('#klasifikasi').selectize()[0].selectize.setValue(select);
                }
            },
        })
    }

    function getSarana_prasarana(select = null) {
        var sarana = ''
        $.ajax({
            url: '<?= base_url() ?>master/getSarana_prasarana',
            type: 'POST',
            data: {
                status: 'Aktif',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#sarana_prasarana').selectize()[0].selectize.destroy();

                $.each(response.data, function(k, v) {
                    sarana += '<option value="' + v.sarana_prasarana_id + '">' + v.sarana_prasarana + '</option>';
                })


                $('#sarana_prasarana').html(sarana);
                $('#sarana_prasarana').selectize();
                if (select != null) {
                    $('#sarana_prasarana').selectize()[0].selectize.setValue(select);
                }
            },
        })
    }

    function deleteHarga(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/harga/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const action_index = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + action_index
                }
            }
        })
    }

    function deleteKios(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/pasar/kios/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const action_index = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + action_index
                }
            }
        })
    }

    function getVarianKomoditi(id, jenis_komoditi, varian_komoditi = null) {
        var varian = '<option value="">- Pilih Varian Komoditi -</option>'
        $.ajax({
            url: "<?= site_url() ?>dashboard/komoditi/varian/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#' + id).selectize()[0].selectize.destroy();

                if (response.success) {

                    $.each(response.data, function(k, v) {
                        varian += '<option value="' + v.varian_komoditi_id + '">' + v.varian_komoditi + '</option>';
                    })

                    $('#' + id).html(varian);
                    $('#' + id).selectize();
                    if (varian_komoditi != null) {
                        $('#' + id).selectize()[0].selectize.setValue(varian_komoditi);
                    }

                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }
                }
            }
        })
    }

    function getSatuanKomoditi(id, jenis_komoditi, satuan_komoditi = null) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/komoditi/satuan/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#' + id).selectize()[0].selectize.setValue('')
                $('#' + id).selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.success) {
                    $('#' + id)[0].selectize.enable();
                    $.each(response.data, function(key, value) {
                        $('#' + id).selectize()[0].selectize.addOption({
                            value: value.satuan_komoditi_id,
                            text: value.satuan_komoditi
                        })
                    })

                    if (satuan_komoditi != null) {
                        $('#' + id).selectize()[0].selectize.setValue(satuan_komoditi);
                    }
                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }

                    $('#' + id)[0].selectize.disable();
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }

    function hapus_data(id, status, table_name, key_name) {
        Swal.fire({
            title: 'Konfirmasi hapus',
            text: "Apa kamu yakin untuk menghapusnya ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pasar/update_data',
                    type: 'POST',
                    data: {
                        id: id,
                        status: status,
                        table_name: table_name,
                        key_name: key_name,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else {
                            const actionIndex = $('#action_index').val()
                            window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                        }
                    }
                })
            }
        })

    }
</script>
<?php $this->load->view('frontend/js/bagian/pasar_detail_informasi_umum') ?>

<?php if (!empty($this->session->userdata('token'))) { ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_anggaran') ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_anggaran_pedagang') ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_pengelola') ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_bongkar_muat') ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_paguyuban') ?>
    <?php $this->load->view('frontend/js/bagian/pasar_detail_pasokan_bapok') ?>
<?php } ?>

<?php $this->load->view('frontend/js/bagian/pasar_detail_regulasi') ?>