<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.16/dist/jquery.mask.min.js"></script>

<?php $this->load->view('backend/js/admin/detail_pasar/pasar_identitas_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_informasi_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_pengelola_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_harga_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_anggaran_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_omset_pedagang_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_bongkar_muat_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_paguyuban_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_regulasi_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_pasokan_bapok_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_foto_js') ?>
<?php $this->load->view('backend/js/admin/detail_pasar/pasar_lokasi_js') ?>
<script>
    (function() {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })()

    $(function() {
        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            let timerInterval
            Swal.fire({
                title: 'Berhasil!',
                icon: 'success',
                html: '<?= $this->session->flashdata('message') ?>',
                timer: 2000,
                timerProgressBar: true,
                showCancelButton: false,
                showConfirmButton: false,
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

        maps(<?= $index_pasar['latitude'] ?>, <?= $index_pasar['longitude'] ?>)

        $('a[data-bs-toggle="pill"]').on('shown.bs.tab', function(e) {
            // https://datatables.net/reference/api/columns.adjust() states that this function is trigger on window resize
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
    })

    function form_bangunan(){
        $('#modal_bangunan').modal('show');
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }

    function getvarian(id, jenis_komoditi, vall = null) {
        var varian = '<option value="">- Pilih Varian Komoditi -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/varian_komoditi_json',
            type: 'POST',
            data: {
                refresh: 'refresh',
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#'+id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    varian += '<option value="' + v.varian_komoditi_id + '">' + v.varian_komoditi + '</option>';
                })
            },
            complete: function() {
                $('#'+id).html(varian);
                $('#'+id).selectize()
                if(vall != null){
                    $('#'+id).selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    function getSatuan(id, jenis_komoditi, vall = null) {
        var satuan = '<option value="">- Pilih Satuan Komoditi -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/satuan_komoditi_json',
            type: 'POST',
            data: {
                refresh: 'refresh',
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#'+id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    satuan += '<option value="' + v.satuan_komoditi_id + '">' + v.satuan_komoditi + '</option>';
                })
            },
            complete: function() {
                $('#'+id).html(satuan);
                $('#'+id).selectize()
                if(vall != null){
                    $('#'+id).selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    function hapus_data(key, table_name, key_name, ket) {
        Swal.fire({
            title: 'Konfirmasi hapus',
            text: "Apa kamu yakin untuk menghapusnya ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pasar/update_status',
                    type: 'POST',
                    data: {
                        'key': key,
                        'table_name': table_name,
                        'status': 'Deleted',
                        'key_name': key_name,
                        'keterangan_perubahan': ket,
                        'db': '',
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Berhasil!",
                                icon: "success",
                                html: response.keterangan + ' <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            if(table_name == 'tbl_pasar_anggaran'){
                                showTableAnggaran()
                            }else if(table_name == 'tbl_pasar_harga_komoditi'){
                                showTable()
                            }else if(table_name == 'tbl_pasar_anggaran_pedagang'){
                                showTableOmzet()
                            }else if(table_name == 'tbl_pasar_regulasi'){
                                showTableRegulasi()
                            }else if(table_name== 'tbl_pasar_pasokan_bapok'){
                                showTableBapok()
                            }
                            
                        } else {
                            Swal.fire(
                                'Gagal!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })
            }
        })
    }
</script>