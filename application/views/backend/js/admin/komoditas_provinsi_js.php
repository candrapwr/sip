<script>
    $(function() {
        $('#commodity-province').selectize()
        $('#commodity-year').selectize()
        $('#commodity-month').selectize()
        $('#province').selectize()
        $('#commodity').selectize()
        $('#unit').selectize()

        showData('refresh')

        $('#filter-commodity').click(function() {
            const province = $('#commodity-province').val()
            const year = $('#commodity-year').val()
            const month = $('#commodity-month').val()

            showData('refresh', province, year, month)
        })

        $('#create-commodity').click(function() {
            $('#modal').modal('show')
        })

        $('#commodity').change(function() {
            getSatuanKomoditi($(this).val())
        })

        $(".number").keypress(function(e) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })

        $('.number').keyup(function() {
            let val = $(this).val()
            val = val.replace(/\./g, '')

            $(this).val(formatRupiah(val, ''))
        })

        $("#modal").on('hidden.bs.modal', function(e) {
            $('#form-tambah')[0].reset();
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
            $('.selectize-input').closest('div.col').find('.text-danger').remove()
        });
    })

    function showData(refresh = null, province = null, year = null, month = null) {
        if ($.fn.DataTable.isDataTable('#table-commodity')) {
            $("#table-commodity").dataTable().fnDestroy();
            $('#table-commodity').empty();
        }

        var table = $('#table-commodity').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>perdagangan/provinsi/json',
                data: {
                    refresh: refresh,
                    province: province,
                    year: year,
                    month: month,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Tanggal',
                    data: 'tanggal',
                }, {
                    title: 'Komoditas',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Produksi',
                    data: 'produksi',
                },
                {
                    title: 'Konsumsi',
                    data: 'konsumsi',
                },
                {
                    title: 'Provinsi',
                    data: 'provinsi',
                },
            ],
            scrollX: true,
            displayLength: 10
        });
    }

    $('#form-tambah').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>perdagangan/provinsi/add",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {
                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: 'success',
                        confirmButtonColor: '#E70A2B',
                        confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        window.location.href = '<?= site_url('perdagangan/provinsi') ?>'
                    })

                } else if (!success) {
                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text' || element.attr('type') == 'date') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    function getSatuanKomoditi(jenis_komoditi, satuan_komoditi = null) {
        $.ajax({
            url: "<?= site_url() ?>komoditi/satuan/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#unit').selectize()[0].selectize.setValue('')
                $('#unit').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.success) {
                    $('#unit')[0].selectize.enable();
                    $.each(response.data, function(key, value) {
                        $('#unit').selectize()[0].selectize.addOption({
                            value: value.satuan_komoditi_id,
                            text: value.satuan_komoditi
                        })
                    })

                    if (satuan_komoditi != null) {
                        $('#unit').selectize()[0].selectize.setValue(satuan_komoditi);
                    }
                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }

                    $('#unit')[0].selectize.disable();
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }
</script>