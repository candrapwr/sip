
<script>
    $(document).ready(function() {
        showTable('refres');
        $('#provinsi_filter, #kabkota_filter').selectize()
        getDaerah('provinsi_filter', 0)

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>web/e-komplain/get',
                data: {
                    daerah_id: ($('#kabkota_filter').val() != null && $('#kabkota_filter').val() != '') ? $('#kabkota_filter').val() : ($('#provinsi_filter').val() != null && $('#provinsi_filter').val() != '') ? $('#provinsi_filter').val() : null,
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'komplain_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama Pengirim',
                    data: 'nama',
                    render: function(k, v, r) {
                        return '<b>' + r.nama + '</b>' +
                            '<br>' + r.nama_pasar
                    }
                },
                {
                    title: 'Tanggal',
                    data: 'tanggal',

                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.status_tiket == 'OPEN') {
                            return '<div class="badge bg-primary bckn-primary">' + r.status_tiket + '</div>'
                        } else if (r.status_tiket == 'RESOLVED') {
                            return '<div class="badge bg-warning">' + r.status_tiket + '</div>'
                        } else {
                            return '<div class="badge bg-success">' + r.status_tiket + '</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'komplain_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        return '<a href="<?= base_url() ?>web/e-komplain/detail/' + r.token + '" class="btn btn-primary btn-sm  m-1"> <i class="ti ti-eye"></i> </a>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function add(action_index) {
        $("#modal").modal('show')
        $('#modal-title').text('Tambah Tipe Pasar')
        $('#tipe_pasar_id').val('')
        $('#tipe_pasar').val('')
    }

    function edit(action_index) {
        $.ajax({
            url: '<?= base_url() ?>master/getTipe_pasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#tipe_pasar_id').val(response.tipe_pasar_id)
                $('#tipe_pasar').val(response.tipe_pasar)
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }

    function hapus_data(id, status, table, key, keterangan) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>master/update_status',
                    type: 'POST',
                    data: {
                        'table_name': table,
                        'key_name': key,
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': keterangan,
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            showTable('refress')

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }

    $('#provinsi_filter').on('change', function() {
        var daerah_id = $("#provinsi_filter").val();
        getDaerah('kabkota_filter', daerah_id)
        //showTable('refresh');

    })

    function getDaerah(id, daerah_id, vall = null) {
        var daerah = '';

        if (id == 'provinsi_filter' || id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else if (id == 'kabkota_filter' || id == 'kab_kota') {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        } else if (id == 'kecamatan') {
            daerah = '<option value="">- Pilih Kecamatan -</option>';
        } else {
            daerah = '<option value="">- Pilih Kelurahan -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi_filter' || id == 'provinsi') {
                        dt = v.provinsi;
                    } else if (id == 'kabkota_filter' || id == 'kab_kota') {
                        dt = v.kab_kota;
                    } else if (id == 'kecamatan') {
                        dt = v.kecamatan;
                    } else {
                        dt = v.kelurahan;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';

                })
            },
            complete: function() {
                $('#' + id).html(daerah);
                $('#' + id).selectize()

                if (vall != null) {
                    $('#' + id).selectize()[0].selectize.setValue(vall)
                    <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
                        $('#' + id).selectize()[0].selectize.disable();
                    <?php } ?>
                } else {
                    $('#' + id).selectize()[0].selectize.setValue('')
                }
            }
        })
    }
</script>