
<script>
    $(document).ready(function() {
        showTable('refres');
        getjenis_komoditi()
        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            processing: true,
            serverSide: true,
            ordering: false,
            searchable: true,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/getFilter_harga',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'komoditi_tampil_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Jenis Komoditi',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian Komoditi',
                    data: 'varian_komoditi',
                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.status == 'Aktif') {
                            return '<div class="badge bg-success">Aktif</div>'
                        } else {
                            return '<div class="badge bg-danger">Tidak Aktif</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'komoditi_tampil_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'
                        var status = 'Tidak Aktif'
                        if (r.status == 'Tidak Aktif') {
                            button_color = 'btn-success'
                            status = 'Aktif'
                        }

                        return '<button class="btn btn-warning btn-sm m-1" onclick="edit(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"> <i class="ti ti-edit"></i></button>' +
                            // '<button class="btn ' + button_color + ' btn-sm  m-1" onclick="update_status(\'' + r.jenis_komoditi_id + '\', \'' + status + '\',\'tbl_jenis_komoditi\',\'jenis_komoditi_id\',\'Hapus Jenis Komoditi `' + ($.trim(r.jenis_komoditi.replace(/[\t\n]+/g, ''))) + '`\')"><i class="ti ti-power"></i></button>' +
                            '<button class="btn btn-danger btn-sm m-1" onclick="hapus_data(\'' + r.komoditi_tampil_id + '\',\'Deleted\',\'tbl_komoditi_tampil\',\'komoditi_tampil_id\',\'Hapus Jenis Komoditi Tampil `' + ($.trim(r.jenis_komoditi.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function getjenis_komoditi( value = null){
        var jenis = '<option value="">- Pilih Jenis Komoditi -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/jenis_komoditi_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#jenis_komoditi').selectize()[0].selectize.destroy();

                $.each(response.data, function(k, v) {
                    jenis += '<option value="' + v.jenis_komoditi_id + '">' + v.jenis_komoditi + '</option>';
                })

            },
            complete: function() {
                $('#jenis_komoditi').html(jenis);
                $('#jenis_komoditi').selectize()
                $('#jenis_komoditi').selectize()[0].selectize.setValue(value)
            }
        })
    }

    function getvarian_komoditi(value=null){
        var varian = '<option value="">- Pilih Varian Komoditi -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/varian_komoditi_json',
            type: 'POST',
            data: {
                refresh: 'refresh',
                jenis_komoditi_id: $('#jenis_komoditi').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#varian_komoditi').selectize()[0].selectize.destroy();

                $.each(response.data, function(k, v) {
                    varian += '<option value="' + v.varian_komoditi_id + '">' + v.varian_komoditi + '</option>';
                })

            },
            complete: function() {
                $('#varian_komoditi').html(varian);
                $('#varian_komoditi').selectize()
                if(value != null){
                    $('#varian_komoditi').selectize()[0].selectize.setValue(value)
                }
                
            }
        })
    }

    $('#jenis_komoditi').on('change', function() {
        getvarian_komoditi()
    })

    function add(action_index) {
        $("#modal").modal('show')
        $('#modal-title').text('Tambah Filter Harga')
        $('#varian_komoditi, #flag_tampil').selectize()
      //  getjenis_komoditi()
    }

    function edit(action_index, start, length) {
        $('#modal-title').text('Edit Filter Harga')
        $.ajax({
            url: '<?= base_url() ?>master/getFilter_harga',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#komoditi_tampil_id').val(response.komoditi_tampil_id)
                $('#jenis_komoditi').selectize()[0].selectize.setValue(response.jenis_komoditi_id)
               // getjenis_komoditi(response.jenis_komoditi_id)
                getvarian_komoditi(response.varian_komoditi_id)
                $('#flag_tampil').selectize()[0].selectize.setValue(response.flag_tampil)
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }

    function hapus_data(id, status, table, key, keterangan) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pengguna/update_status',
                    type: 'POST',
                    data: {
                        'table_name': table,
                        'key_name': key,
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': keterangan,
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            showTable('refress')

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }
</script>