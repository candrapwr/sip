<script src="https://cdn.jsdelivr.net/npm/nestable2@1.6.0/jquery.nestable.js"></script>
<script src="https:////cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>



<script>
    $(document).ready(function() {
        getMenu('refresh', 'Front end')
        $('#jenis').val('Front end')

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

        $('#form_menu').submit(function(e) {
            e.preventDefault()
            var form_data = new FormData($(this)[0]);
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {

                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');

                }, false);

            });

            //console.log($('#jenis').val())

            $.ajax({
                url: "<?= base_url() ?>pengguna/crud_permission",
                type: 'post',
                data: form_data,
                cache: false,
                processData: false,
                contentType: false,
                dataType: 'json',

                success: function(response) {
                    //console.log(response)
                    if (response.kode == 200) {
                        var timerInterval;
                        $('#nama_menu').val('')
                        $("#controller").val('')
                        $('#icon').val('')
                        $("#publik").val('')
                        $('#menu_id').val('')

                        getMenu('refresh', response.jenis)
                        var timerInterval;
                        Swal.fire({
                            title: "Berhasil!",
                            icon: "success",
                            html: 'Data berhasil di simpan <br><b></b> milliseconds.',
                            timer: 2000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                const b = Swal.getHtmlContainer().querySelector('b')
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        })

                    } else {
                        var timerInterval;
                        Swal.fire({
                            title: "Gagal!",
                            icon: "error",
                            html: 'Data gagal di simpan <br><b></b> milliseconds.',
                            timer: 2000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                const b = Swal.getHtmlContainer().querySelector('b')
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        })
                    }
                },
            });
        })


    })

    function update_status(id, status_sebelum, status) {
        $.ajax({
            url: '<?= base_url() ?>pengguna/update_status',
            type: 'POST',
            data: {
                'table_name': 'tbl_menu',
                'key_name': 'menu_id',
                'key': id,
                'status': status,
                'keterangan_perubahan': 'Status Permission ' + status_sebelum + ' menjadi ' + status,
                'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
            },

            success: function(response) {
                response = JSON.parse(response)
                if (response.kode == 200) {
                    var timerInterval;
                    Swal.fire({
                        title: "Berhasil!",
                        icon: "success",
                        html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    })
                    showTable('refress')
                } else {
                    Swal.fire(
                        'Gagal!',
                        response.keterangan,
                        'error'
                    )
                }
            }
        })
    }

    function tab_menu(jenis){
        getMenu('refresh', jenis)
        $('#jenis').val(jenis)
        $('#nama_menu').val('')
        $('#menu_id').val('')
        $('#controller').val('')
        $('#publik').val('')
        $('#urutan').val('999')
        $('#icon').val('')
    }

    function reset(){
        $('#nama_menu').val('')
        $('#menu_id').val('')
        $('#controller').val('')
        $('#publik').val('')
        $('#urutan').val('999')
        $('#icon').val('')
    }

    var anu = ''
    var wes_urut = ''
    var before = []
    function getMenu(refresh = null, jenis = null) {
        var menu = ''
        $.ajax({
            url: '<?= base_url() ?>pengguna/getUrutan_menu',
            type: 'POST',
            data: {
                refresh: refresh,
                jenis: jenis,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },

            beforeSend: function() {
                $("#menu_web").LoadingOverlay("show");
            },
            success: function(response) {
                response = JSON.parse(response)
                anu = response
                $.each(response.data, function(k, v) {
                   

                    var posisi = 'Front end';

                    if (v.jenis == 'Front end') {
                        posisi = 'Back end';
                    }

                    menu += '<li class="dd-item mulus  animate__animated animate__fadeInUp" data-id="' + v.menu_id + '">' +
                        '<div style="cursor:move" class="dd-handle dd3-handle Back"></div>' +
                        '<div class="dd3-content">' +
                        '<span id="label_show ' + v.menu_id + '"> ' + v.menu + '</span> ' +
                        '<span class="span-right">/<span id="link_show ' + v.menu_id + '"> ' + v.url + '</span> &nbsp;&nbsp;  ' +
                        '<a href="javascript:void(0)" onclick="edit_menu(\'' + v.menu_id + '\', \'' + v.menu + '\', \'' + v.url + '\', \'' + v.publik + '\', \'' + v.icon + '\', \'' + v.urutan + '\')"><i class="fas fa-pencil-alt"></i></a>  &nbsp;  ' +
                        '<a href="javascript:void(0)" onclick="hapus_menu(\'' + v.menu_id + '\', \'Deleted\', \'' + v.jenis + '\')"><i class="ti ti-trash text-danger"></i></a></span>  ' +
                        '</div>';
                    if (v.parent.length > 0) {
                        
                        menu += '<ol class="dd-list">';
                        var children = []
                        $.each(v.parent, function(a, b) {
                            children[a]={id:b.menu_id}
                            var icon2 = 'up text-success';
                            var ket2 = 'Pindah ke Frontend Menu';
                            var posisi2 = 'Front end';

                            if (b.jenis == 'Front end') {
                                icon2 = 'down text-danger';
                                ket2 = 'Pindah ke Backend Menu';
                                posisi2 = 'Back end';
                            }


                            menu += '<li class="dd-item mulus  animate__animated animate__fadeInUp" data-id="' + b.menu_id + '">' +
                                '<div style="cursor:move" class="dd-handle dd3-handle Back"></div>' +
                                '<div class="dd3-content">' +
                                '<span id="label_show ' + b.menu_id + '"> ' + b.menu + '</span> ' +
                                '<span class="span-right">/<span id="link_show ' + b.menu_id + '"> ' + b.url + '</span> &nbsp;&nbsp;  ' +
                                '<a href="javascript:void(0)" onclick="edit_menu(\'' + b.menu_id + '\', \'' + b.menu + '\', \'' + b.url + '\', \'' + b.publik + '\', \'' + b.icon + '\', \'' + b.urutan + '\')"><i class="fas fa-pencil-alt"></i></a>  &nbsp;  ' +
                                '<a href="javascript:void(0)" onclick="hapus_menu(\'' + b.menu_id + '\', \'Deleted\', \'' + b.jenis + '\')"><i class="ti ti-trash text-danger"></i></a></span>  ' +
                                '</div>' +
                                '</li>'
                        })
                        before[k]={id:v.menu_id,children:children}
                        menu += '</ol>';
                    }else{
                        before[k]={id:v.menu_id}
                    }
                    menu += '</li>'
                })
            },
            complete: function() {
                $('#nestable .dd-list').html(menu)
                $("#nestable").nestable({
                    group: 1,
                    callback: function(l, e) {
                        wes_urut = $('#nestable.dd').nestable('serialize')
                        // update_urutan($('#nestable.dd').nestable('serialize'))
                        // console.log(anu)
                    }
                })
                console.log(before)
                $("#menu_web").LoadingOverlay("hide", true);
            }
        })
    }

    function edit_menu(id, nama_menu, url, publik, icon, urutan){
        $('#nama_menu').val(nama_menu)
        $('#menu_id').val(id)
        $('#controller').val(url)
        $('#publik').val(publik)
        $('#urutan').val(urutan)
        $('#icon').val(icon)
    }



    function update_urutan() {
        //console.log(urutan)
        $.ajax({
            url: '<?= base_url() ?>pengguna/update_urutan',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                after: wes_urut,
                before: before
            },
            beforeSend: function() {
                $("#menu_web").LoadingOverlay("show");
            },
            success: function(response) {
                response = JSON.parse(response)
                //console.log(response.kode)
                if (response.kode == 200) {
                    Swal.fire({
                        title: "Berhasil!",
                        icon: "success",
                        html: response.keterangan + '<br><b></b> milliseconds.',
                        timer: 2000,
                        timerProgressBar: true,
                        didOpen: () => {
                            Swal.showLoading()
                            const b = Swal.getHtmlContainer().querySelector('b')
                            timerInterval = setInterval(() => {
                                b.textContent = Swal.getTimerLeft()
                            }, 100)
                        },
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                    })

                    getMenu('refresh', 'Back end')

                } else {
                    Swal.fire(
                        'Gagal!',
                        response.keterangan,
                        'error'
                    )
                }
            },
            complete: function() {
                $("#menu_web").LoadingOverlay("hide", true);
            }
        })
    }

    function hapus_menu(id, status, jenis) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pengguna/update_status',
                    type: 'POST',
                    data: {
                        'table_name': 'tbl_menu',
                        'key_name': 'menu_id',
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': 'Deleted Permission',
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            getMenu('refress', jenis)

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }
</script>