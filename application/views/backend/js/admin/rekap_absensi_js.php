<script>
    $(function() {
        showRekap('refresh')
    })


    function showRekap(refresh = null) {
        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            dom: 'Bfrtip',
            buttons: [{
                    "extend": 'excel',
                    "text": '<i class="ti ti-file-spreadsheet"></i>Excel',
                    "titleAttr": 'Excel',
                    "action": newexportaction
                },
                {
                    "extend": 'csv',
                    "text": '<i class="ti ti-file-text"></i>CSV',
                    "titleAttr": 'CSV',
                    "action": newexportaction
                },
            ],

            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>administrator/absensi/rekap/json',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Nama',
                    data: 'nama_lengkap',
                },
                {
                    title: 'Email',
                    data: 'email',
                    "visible": false
                },
                {
                    title: 'Checkin Pasar ID',
                    data: 'checkin_pasar_id',
                    "visible": false
                },
                {
                    title: 'Checkin Pasar',
                    data: 'checkin_nama_pasar',
                },
                {
                    title: 'Checkin',
                    title: 'Checkin Time',
                    data: 'checkin_time',
                },
                {
                    title: 'Checkin Latlong Pasar',
                    data: 'checkin_latlong',
                },
                {
                    title: 'Checkin Radius',
                    data: 'checkin_radius',
                },

                {
                    title: 'Checkout Pasar',
                    data: 'checkout_nama_pasar',
                    "visible": false
                },
                {
                    title: 'Checkout',
                    title: 'Checkout Time',
                    data: 'checkout_time',
                    "visible": false
                },
                {
                    title: 'Checkout Latlong Pasar',
                    data: 'checkout_latlong',
                    "visible": false
                },
                {
                    title: 'Photo',
                    data: 'absensi_id',
                    render: function(k, v, r) {
                        return '<img width="50" height="60" src="' + r.checkin_photo + '">'
                    },
                    className: 'text-center'
                },
                {
                    title: 'Url Photo',
                    data: 'checkin_photo',
                    "visible": false
                }

            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function(e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
            dt.one('preDraw', function(e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function(e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
</script>