<script>
    $(document).ready(function() {
        maps('<?= $index_pasar['latitude'] ?>', '<?= $index_pasar['longitude'] ?>')
    })

    $('#lokasi-tab').on('shown.bs.tab', function(e) {
        maps('<?= $index_pasar['latitude'] ?>', '<?= $index_pasar['longitude'] ?>')
    });

    function searchLocation() {
        let lokasi = '';
        let jml_lokasi = '';
        $.ajax({
            url: '<?= base_url() ?>pasar/cari_lokasi',
            type: 'POST',
            data: {
                keyword: $('#keyword').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend() {
                if ($.fn.DataTable.isDataTable('#tbl_lokasi')) {
                    $("#tbl_lokasi").dataTable().fnDestroy();
                    $('#tbl_lokasi').empty();
                }
            },
            success(response) {
                response = JSON.parse(response);
                console.log(response);
                jml_lokasi = response.predictions.length;

                response.predictions.forEach((v) => {
                    lokasi += `<tr>
                    <td>${v.description}</td>
                    <td class="text-center">
                        <button type="button" onclick="getLokasi('${v.place_id}')" class="btn btn-primary waves-effect waves-light"><i class="ti ti-map-pin me-1"></i>Pilih</button>
                    </td>
                </tr>`;
                });
            },
            complete() {
                $('#modal_lokasi').modal('show');
                $('#modal_title').text(`Pencarian Anda ditemukan ${jml_lokasi} Lokasi`);
                $('#lokasiMaps').html(lokasi);
            }
        });
    }


    function getLokasi(place_id) {
        $('#modal_lokasi').modal('hide')
        var lat = ''
        var long = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/lokasi_latlong',
            type: 'POST',
            data: {
                place_id: place_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {

            },
            success: function(response) {
                response = JSON.parse(response)
                lat = response.results[0].geometry.location.lat
                long = response.results[0].geometry.location.lng
            },
            complete: function() {
                $('#latitude_pasar').val(lat)
                $('#longtitude_pasar').val(long)
                maps(lat, long)
            }
        })
    }

    function maps(lat = null, long = null) {
        document.getElementById('weathermap').innerHTML = "<div id='map'></div>";
        var map = L.map('map').setView([-2.548926, 118.0148634], lat != null ? 8 : 4);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(map);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map);

        if (lat != null & long != null) {
            var markers = L.markerClusterGroup();
            var lokasi = L.marker([lat, long], {
                icon: icon
            })

            map.scrollWheelZoom.disable();

            markers.addLayer(lokasi);
            map.addLayer(markers);
            map.fitBounds(markers.getBounds());
        }
    }
</script>