<script>
    $(document).ready(function() {
        showTableAnggaran()
        $('#omzet_pasar, #jumlah_anggaran').mask('000.000.000.000.000.000.000.000', {
            reverse: true
        });
    })

    function showTableAnggaran() {

        if ($.fn.DataTable.isDataTable('#tbl_anggaran')) {
            $("#tbl_anggaran").dataTable().fnDestroy();
            $('#tbl_anggaran').empty();
        }

        var table = $('#tbl_anggaran').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getAnggaran',
                data: {
                    refresh: 'refresh',
                    id: '<?= $index_pasar['pasar_id'] ?>',
                    start_date: $('#filter_harga_date').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Tahun',
                    data: 'tahun',
                },
                {
                    title: 'Program Pasar',
                    data: 'program_pasar',
                    render: function(k, v, r) {
                        return r.program_pasar + '<br><span class="badge rounded-pill bg-success">oleh ' + r.createdby + '</span>'
                    }
                },
                {
                    title: 'Omzet',
                    data: 'omset',
                    render: function(k, v, r) {
                        return 'Rp ' + r.omset
                    }
                },
                {
                    title: 'Anggaran',
                    data: 'anggaran',
                    render: function(k, v, r) {
                        return 'Rp ' + r.anggaran
                    }
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_anggaran_id',
                        render: function(k, v, r, m) {
                            if (r.action) {
                                var button = `<button class="btn btn-sm btn-light m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>`

                                if(r.createdby == '<?=$this->session->userdata('email')?>' || '<?=strtolower($this->session->userdata('role'))?>'  == 'administrator'){
                                    button = '<button onclick="hapus_data(\'' + r.pasar_anggaran_id + '\', \'tbl_pasar_anggaran\', \'pasar_anggaran_id\', \'Hapus data anggaran dengan id ' + r.pasar_anggaran_id + ' atas <?= $this->session->userdata('email') ?> \')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>'
                                }

                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update_anggaran(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-primary m-1" title="Update"><i class="fa-solid fa-pen-to-square"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                     button+
                                    '</div>'
                            } else {
                                return ''
                            }

                        }
                    }
                <?php } ?>
            ],
            scrollX: true,
            displayLength: 10
        });

    }

    function form_anggaran() {
        $('#modal_anggaran').modal('show');
        $('#tahun_anggaran').selectize()[0].selectize.setValue('<?=date('Y')?>')
        getProgramPasar()
        $('#omzet_pasar').val('')
        $('#pasar_anggaran_id').val('')
        $('#jumlah_anggaran').val('')
    }

    function update_anggaran(index, start, length) {
        $('#modal_anggaran').modal('show');
        var tahun = ''
        var program_pasar = ''
        var omzet = ''
        var anggaran = ''
        var pasar_anggaran_id = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/getAnggaran',
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                tahun = response.tahun
                program_pasar = response.program_pasar_id
                omzet = response.omset
                anggaran = response.anggaran
                pasar_anggaran_id = response.pasar_anggaran_id
            },
            complete: function() {
                $('#tahun_anggaran').selectize()[0].selectize.setValue(tahun)
                getProgramPasar(program_pasar)
                $('#omzet_pasar').val(omzet)
                $('#jumlah_anggaran').val(anggaran)
                $('#pasar_anggaran_id').val(pasar_anggaran_id)
            }
        })
    }

    function getProgramPasar(vall = null) {
        var program = '<option value="">- Pilih Program Pasar -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/getProgram_pasar',
            type: 'POST',
            data: {
                refresh: 'refresh',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#program_pasar').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    program += '<option value="' + v.program_pasar_id + '">' + v.program_pasar + '</option>';
                })
            },
            complete: function() {
                $('#program_pasar').html(program);
                $('#program_pasar').selectize()
                if (vall != null) {
                    $('#program_pasar').selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

</script>