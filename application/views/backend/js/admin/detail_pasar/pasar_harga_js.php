<script>
    $(document).ready(function() {
        showTable()
        $('#harga_komoditi').mask('000.000.000.000.000.000.000.000', {
            reverse: true
        });
    })

    function filter_harga(){
        showTable()
    }

    function showTable() {

        if ($.fn.DataTable.isDataTable('#tbl_harga')) {
            $("#tbl_harga").dataTable().fnDestroy();
            $('#tbl_harga').empty();
        }

        var table = $('#tbl_harga').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getHarga',
                data: {
                    refresh: 'refresh',
                    id: '<?= $index_pasar['pasar_id'] ?>',
                    start_date: $('#filter_harga_date').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Jenis',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian',
                    data: 'varian_komoditi',
                },
                {
                    title: 'Harga',
                    data: 'harga',
                },
                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Created By',
                    data: 'createdby',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_harga_komoditi_id',
                        render: function(k, v, r, m) {
                            if (r.action_button) {
                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update_harga(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-primary m-1" title="Update"><i class="fa-solid fa-pen-to-square"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                    '<button onclick="hapus_data(\'' + r.pasar_harga_komoditi_id + '\', \'tbl_pasar_harga_komoditi\', \'pasar_harga_komoditi_id\', \'Hapus data harga dengan id '+r.pasar_harga_komoditi_id+' atas <?=$this->session->userdata('email')?> \')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>' +
                                    '</div>'
                            } else {
                                return ''
                            }

                        }
                    }
                <?php } ?>
            ],
            scrollX: true,
            displayLength: 10
        });

    }

    var val_varian_komoditi = ''
    var val_satuan_komoditi = ''

    function form_harga() {
        $('#modal_harga').modal('show');
        $('#varian_komoditi').selectize()[0].selectize.setValue('')
        $('#satuan_komoditi').selectize()[0].selectize.setValue('')
        $('#harga_komoditi').val('')
        $('#harga_komoditi_id').val('')
        getKomoditas()
        val_varian_komoditi = null
        val_satuan_komoditi = null
    }

    function update_harga(index, start, length) {
        $('#modal_harga').modal('show');
        var harga = ''
        var harga_komoditi_id = ''
        var jenis_komoditi = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/getHarga',
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                getKomoditas(response.jenis_komoditi_id+'|'+response.jenis_komoditi)
                harga = response.harga
                val_varian_komoditi = response.varian_komoditi_id
                val_satuan_komoditi = response.satuan_komoditi_id
                harga_komoditi_id = response.pasar_harga_komoditi_id
            },
            complete: function() {
                $('#harga_komoditi').val(harga)
                $('#harga_komoditi_id').val(harga_komoditi_id)
            }
        })
    }

    function getKomoditas(vall = null) {
        var jenis_komoditas = '<option value="">- Pilih Komoditi -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/jenis_komoditi_json',
            type: 'POST',
            data: {
                refresh: 'refresh',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#jenis_komoditas_harga').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    jenis_komoditas += '<option value="' + v.jenis_komoditi_id + '|' + v.jenis_komoditi + '">' + v.jenis_komoditi + '</option>';
                })
            },
            complete: function() {
                $('#jenis_komoditas_harga').html(jenis_komoditas);
                $('#jenis_komoditas_harga').selectize()
                if(vall != null){
                    $('#jenis_komoditas_harga').selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    $('#jenis_komoditas_harga').on('change', function() {
        var jenis_komoditi = $("#jenis_komoditas_harga").val();
        jenis_komoditi = jenis_komoditi.split("|")
        getvarian('varian_komoditi',jenis_komoditi[0], val_varian_komoditi)
        getSatuan('satuan_komoditi', jenis_komoditi[0], val_satuan_komoditi)
    })

</script>