<script>
    $('#provinsi').on('change', function() {
        var provinsi = $("#provinsi").val();
        getDaerah('kab_kota', provinsi)
    })

    $('#kab_kota').on('change', function() {
        var kab_kota = $("#kab_kota").val();
        getDaerah('kecamatan', kab_kota)
    })

    $('#kecamatan').on('change', function() {
        var kecamatan = $("#kecamatan").val();
        getDaerah('kelurahan', kecamatan)
    })

    $(function() {
        $('#provinsi').selectize()[0].selectize.setValue()
        $('#kab_kota').selectize()[0].selectize.setValue()
        $('#kecamatan').selectize()[0].selectize.setValue()
        $('#kelurahan').selectize()[0].selectize.setValue()
        getDaerah('provinsi', 0, '<?= substr($index_pasar['daerah_id'], 0, 2) ?>')
        getDaerah('kab_kota', '<?= substr($index_pasar['daerah_id'], 0, 2) ?>', '<?= substr($index_pasar['daerah_id'], 0, 4) ?>')
        getDaerah('kecamatan', '<?= substr($index_pasar['daerah_id'], 0, 4) ?>', '<?= substr($index_pasar['daerah_id'], 0, 6) ?>')
        getDaerah('kelurahan', '<?= substr($index_pasar['daerah_id'], 0, 6) ?>', '<?= $index_pasar['daerah_id'] ?>')
    })

    function form_identitas() {
        $('#modal_identitas').modal('show');
    }

    function getDaerah(id, daerah_id, val = null) {
        var daerah = '';

        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else if (id == 'kab_kota') {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        } else if (id == 'kecamatan') {
            daerah = '<option value="">- Pilih Kecamatan -</option>';
        } else {
            daerah = '<option value="">- Pilih Kelurahan -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else if (id == 'kab_kota') {
                        dt = v.kab_kota;
                    } else if (id == 'kecamatan') {
                        dt = v.kecamatan;
                    } else {
                        dt = v.kelurahan;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';

                })
            },
            complete: function() {
                $('#' + id).html(daerah);
                $('#' + id).selectize()

                if (val != null) {
                    $('#' + id).selectize()[0].selectize.setValue(val)
                }
            }
        })
    }
</script>