<script>
    $(document).ready(function() {
        showTableRegulasi()
        $('#jumlah_bongkar_muat').mask('000.000.000.000.000.000.000.000', {
            reverse: true
        });

        $('#tgl_piker').datepicker({
            default: true,
            language: 'id',
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            endDate: '2023-08-24'
        });
    })

    function showTableRegulasi() {

        if ($.fn.DataTable.isDataTable('#tbl_regulasi')) {
            $("#tbl_regulasi").dataTable().fnDestroy();
            $('#tbl_regulasi').empty();
        }

        var table = $('#tbl_regulasi').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getRegulasi_pasar',
                data: {
                    refresh: 'refresh',
                    id: '<?= $index_pasar['pasar_id'] ?>',
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No. Regulasi',
                    data: 'no_regulasi',
                    render: function(k, v, r) {
                        var file = ''
                        if(r.file_regulasi != null){
                            file ='<a target="_blank" href="'+r.link_file_regulasi+'"><i class="fa-solid fa-file-lines"></i> File Regulasi</a>'
                        }
                        return '<p class="mb-2"><b>'+ r.no_regulasi + '</b></p>'+file
                    }
                },
                {
                    title: 'Nama Regulasi',
                    data: 'nama_regulasi',
                },
                {
                    title: 'Pengesahan',
                    data: 'pengesah_regulasi',
                },
                {
                    title: 'tanggal',
                    data: 'tgl_regulasi',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_regulasi_id',
                        render: function(k, v, r, m) {
                                var button = `<button class="btn btn-sm btn-light m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>`

                                if (r.createdby == '<?= $this->session->userdata('email') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == 'administrator') {
                                    button = '<button onclick="hapus_data(\'' + r.pasar_regulasi_id + '\', \'tbl_pasar_regulasi\', \'pasar_regulasi_id\', \'Hapus data regulasi dengan id ' + r.pasar_regulasi_id + ' atas <?= $this->session->userdata('email') ?> \')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>'
                                }

                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update_regulasi(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-primary m-1" title="Update"><i class="fa-solid fa-pen-to-square"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                    button +
                                    '</div>'

                        }
                    }
                <?php } ?>
            ],
            scrollX: true,
            displayLength: 10
        });

    }

    function form_regulasi() {
        $('#modal_regulasi').modal('show');
        $('#pengesah_regulasi').val('')
        $('#nama_regulasi').val('')
        $('#no_regulasi').val('')
        $('#pasar_regulasi_id').val('')
        $('#tgl_regulasi').val('')
        $("#file_regulasi").attr("required", true);
    }

    function update_regulasi(index, start, length) {
        $('#modal_regulasi').modal('show');
        var no_regulasi = ''
        var pasar_regulasi_id = ''
        var nama_regulasi = ''
        var pengesah_regulasi = ''
        var tgl_regulasi = ''
        var file_regulasi = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/getRegulasi_pasar',
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                console.log(response);
                no_regulasi = response.no_regulasi
                pasar_regulasi_id = response.pasar_regulasi_id
                nama_regulasi = response.nama_regulasi
                pengesah_regulasi = response.pengesah_regulasi
                tgl_regulasi = response.tgl_regulasi
                file_regulasi = response.file_regulasi
            },
            complete: function() {
                $('#pasar_regulasi_id').val(pasar_regulasi_id)
                $('#no_regulasi').val(no_regulasi)
                $('#nama_regulasi').val(nama_regulasi)
                $('#pengesah_regulasi').val(pengesah_regulasi)
                $('#tgl_regulasi').val(tgl_regulasi)

                if(file_regulasi == null && file_regulasi == ''){
                    $("#file_regulasi").attr("required", true);
                }else{
                    $("#file_regulasi").attr("required", false);
                }
            }
        })
    }
</script>