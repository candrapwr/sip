<script>
    $(function() {
        $('#tipe_pasar, #klasifikasi, #penerapan_digital_pasar, #metode_penerapan_digital_pasar, #pembiayaan_lembaga_keuangan, #sertifikasi_sni, #dekat_pemukiman, #bentuk_pasar, #kondisi').selectize()
        $('#waktu_operasional, #sarana_prasarana').selectize({
            create: false,
            delimiter: ',',
        })
        getTipePasar()
        getKlasifikasi('klasifikasi')
        getKomoditi()
        getSarana()

        $('#tahun_renovasi, #tahun_bangun').mask('0000', {
        'translation': {
            0: {
                pattern: /[0-9]/
            }
        }
    });

    $('#omzet, #pekerja_tetap, #pekerja_nontetap, #luas_bangunan, #luas_tanah, #jumlah_lantai, #jumlah_pengunjung').mask('000.000.000.000.000.000.000.000', {reverse: true});

    })


    var waktu = ''
    var komoditi = ''
    var sarana = ''
    function form_informasi() {
        $('#modal_informasi').modal('show');
        $('#tipe_pasar').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['tipe_pasar_id'] ?>')
        $('#klasifikasi').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jenis_pasar'] ?>')
        $('#kondisi').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['kondisi'] ?>')
        $('#bentuk_pasar').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['bentuk_pasar'] ?>')
        
       waktu = '<?= $detail_pasar == '' ? '' : $detail_pasar[0]['waktu_operasional'] ?>'
        $('#waktu_operasional').selectize()[0].selectize.setValue(waktu.split(','))

        komoditi = '<?= $detail_pasar == '' ? '' : $detail_pasar[0]['komoditas_dijual'] ?>'
        $('#komoditas').selectize()[0].selectize.setValue(komoditi.split(','))
        $('#penerapan_digital_pasar').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['penerapan_digitalisasi_pasar'] ?>')
        $('#sertifikasi_sni').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['sertifikasi_sni'] ?>')
        $('#pembiayaan_lembaga_keuangan').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['pembiayaan_lk'] ?>')

        <?php if ($detail_pasar != '') { ?>
            if ('<?= $detail_pasar[0]['penerapan_digitalisasi_pasar'] ?>' == 'Ya') {
                $('#metode_penerapan_digital_pasar').parent().parent().show()
                $('#metode_penerapan_digital_pasar').prop('required', true);
                if ('<?= $detail_pasar[0]['detail_digitalisasi_pasar'] ?>' == null) {
                    $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue('')
                } else {
                    var metode = '<?=$detail_pasar[0]['detail_digitalisasi_pasar']?>'
                    $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue(metode.split(','))
                }
            } else {
                $('#metode_penerapan_digital_pasar').parent().parent().hide()
                $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue('')
                $('#metode_penerapan_digital_pasar').prop('required', false)
            }

            if ('<?= $detail_pasar[0]['sertifikasi_sni'] ?>' == 'Ya') {
                $('#tahun_sni_pasar_rakyat').parent().parent().show()
                $('#tahun_sni_pasar_rakyat').prop('required', true);
                $('#tahun_sni_pasar_rakyat').val('<?= $detail_pasar[0]['tahun_sertifikasi'] ?>')
            } else {
                $('#tahun_sni_pasar_rakyat').parent().parent().hide()
                $('#tahun_sni_pasar_rakyat').val('')
                $('#tahun_sni_pasar_rakyat').prop('required', false)
            }

            if ('<?= $detail_pasar[0]['pembiayaan_lk'] ?>' == 'Ya') {
                $('#lembaga_keuangan').parent().parent().show()
                $('#lembaga_keuangan').prop('required', true);
                $('#lembaga_keuangan').val('<?= $detail_pasar[0]['lembaga_keuangan'] ?>')
            } else {
                $('#lembaga_keuangan').parent().parent().hide()
                $('#lembaga_keuangan').val('')
                $('#lembaga_keuangan').prop('required', false)
            }

            sarana = '<?=$detail_pasar[0]['sarana_prasarana']?>'

        <?php } ?>

        $('#sarana_prasarana').selectize()[0].selectize.setValue(sarana.split(','))
        $('#dekat_pemukiman').selectize()[0].selectize.setValue('<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jarak_pemukiman'] ?>')

    }

    $('#penerapan_digital_pasar').on('change', function() {

        if ($('#penerapan_digital_pasar').val() == 'Ya') {
            $('#metode_penerapan_digital_pasar').parent().parent().show()
            $('#metode_penerapan_digital_pasar').prop('required', true);
        } else {
            $('#metode_penerapan_digital_pasar').parent().parent().hide()
            $('#metode_penerapan_digital_pasar').val('')
            $('#metode_penerapan_digital_pasar').prop('required', false);
        }
    })

    $('#pembiayaan_lembaga_keuangan').on('change', function() {

        if ($('#pembiayaan_lembaga_keuangan').val() == 'Ya') {
            $('#lembaga_keuangan').parent().parent().show()
            $('#lembaga_keuangan').prop('required', true);
        } else {
            $('#lembaga_keuangan').parent().parent().hide()
            $('#lembaga_keuangan').val('')
            $('#lembaga_keuangan').prop('required', false);
        }

    })

    $('#sertifikasi_sni').on('change', function() {

        if ($('#sertifikasi_sni').val() == 'Ya') {
            $('#tahun_sni_pasar_rakyat').parent().parent().show()
            $('#tahun_sni_pasar_rakyat').prop('required', true);
        } else {
            $('#tahun_sni_pasar_rakyat').parent().parent().hide()
            $('#tahun_sni_pasar_rakyat').val('')
            $('#tahun_sni_pasar_rakyat').prop('required', false);
        }

    })

    function getTipePasar() {
        var tipe_pasar = '<option value="">- Pilih Tipe Pasar -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/getTipe_pasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#tipe_pasar').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response, function(k, v) {
                    tipe_pasar += '<option value="' + v.tipe_pasar_id + '">' + v.tipe_pasar + '</option>';
                })
            },
            complete: function() {
                $('#tipe_pasar').html(tipe_pasar);
                $('#tipe_pasar').selectize()
            }
        })
    }

    function getKlasifikasi(id, vall = null) {
        var klasifikasi = '<option value="">- Pilih Klasifikasi -</option>'
        $.ajax({
            url: '<?= base_url() ?>master/getKlasifikasi',
            type: 'POST',
            data: {
                refresh: 'refresh',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#'+id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    klasifikasi += '<option value="' + v.klasifikasi + '">' + v.klasifikasi + '</option>';
                })
            },
            complete: function() {
                $('#'+id).html(klasifikasi);
                $('#'+id).selectize()

                if(vall != null){
                    $('#'+id).selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    function getKomoditi() {
        var komoditas = '<option value="">- Komoditas yang Dijual -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/jenis_komoditi_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#komoditas').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    komoditas += '<option value="' + v.jenis_komoditi + '">' + v.jenis_komoditi + '</option>';
                })
            },
            complete: function() {
                $('#komoditas').html(komoditas);
                $('#komoditas').selectize({
                    create: true,
                    delimiter: ',',
                })
            }
        })
    }

    function getSarana() {
        var sarana = '<option value="">- Sarana & Prasaran -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/getSarana_prasarana',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#sarana_prasarana').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    sarana += '<option value="' + v.sarana_prasarana + '">' + v.sarana_prasarana + '</option>';
                })
            },
            complete: function() {
                $('#sarana_prasarana').html(sarana);
                $('#sarana_prasarana').selectize({
                    create: true,
                    delimiter: ',',
                })
            }
        })
    }

    $('#cu_pasar_detail').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= base_url() ?>cu/pasar/detail",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                console.log(response)

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {

                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: 'success',
                    }).then(function() {
                        window.location.href = '<?=base_url()?>web/pasar/<?= $index_pasar['pasar_id'] ?>/<?= str_replace(' ', '-', strtolower($index_pasar['nama'])) ?>'
                    })
                } else if (!success) {
                    Swal.fire({
                        title: response.message,
                        icon: 'warning',
                    })
                }
            }
        })
    })
</script>