<script>
    $(document).ready(function() {
        $('#no_telp_ketua').mask('00000000000000', {
            reverse: true
        });
    })

    function form_paguyuban() {
        $('#modal_paguyuban').modal('show');
        $('#pasar_paguyuban_id').val('<?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['pasar_paguyuban_id'] : '' ?>')
        $('#nama_ketua').val('<?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['nama_ketua'] : '' ?>')
        $('#asosiasi_perdagangan_pasar').selectize()[0].selectize.setValue('<?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['asosiasi_perdagangan_pasar'] : '' ?>')
        $('#no_telp_ketua').val('<?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['no_telp_ketua'] : '' ?>')
        $('#nama_paguyuban').val('<?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['nama_paguyuban'] : '' ?>')
    }
</script>