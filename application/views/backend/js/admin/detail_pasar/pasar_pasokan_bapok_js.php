<script>
    $(document).ready(function() {
        showTableBapok()
        $('#jumlah_pasokan_bapok').mask('000.000.000.000.000.000.000.000', {
            reverse: true
        });
    })

    function showTableBapok() {

        if ($.fn.DataTable.isDataTable('#tbl_pasokan_bapok')) {
            $("#tbl_pasokan_bapok").dataTable().fnDestroy();
            $('#tbl_pasokan_bapok').empty();
        }

        var table = $('#tbl_pasokan_bapok').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getPasokanBapok_pasar',
                data: {
                    refresh: 'refresh',
                    id: '<?= $index_pasar['pasar_id'] ?>',
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Bulan',
                    data: 'bulan',
                    render: function(k, v, r) {
                        return r.bulan + ' '+r.tahun
                    }
                },
                {
                    title: 'Jenis',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian',
                    data: 'varian_komoditi',
                },
                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_pasokan_bapok_id',
                        render: function(k, v, r, m) {
                                var button = `<button class="btn btn-sm btn-light m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>`

                                if (r.createdby == '<?= $this->session->userdata('email') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == 'administrator') {
                                    button = '<button onclick="hapus_data(\'' + r.pasar_pasokan_bapok_id + '\', \'tbl_pasar_pasokan_bapok\', \'pasar_pasokan_bapok_id\', \'Hapus data pasokan bapok dengan id ' + r.pasar_pasokan_bapok_id + ' atas <?= $this->session->userdata('email') ?> \')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>'
                                }

                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update_regulasi(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-primary m-1" title="Update"><i class="fa-solid fa-pen-to-square"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                    button +
                                    '</div>'

                        }
                    }
                <?php } ?>
            ],
            scrollX: true,
            displayLength: 10
        });

    }

    var val_varian_komoditi_bapok = ''
    var val_satuan_komoditi_bapok = ''

    function form_pasokan_bapok() {
        $('#modal_bapok').modal('show');
        $('#tahun_pasokan_bapok').selectize()[0].selectize.setValue('<?= date('Y') ?>')
        $('#bulan_pasokan_bapok').selectize()[0].selectize.setValue('<?=bulan(date('Y-m-d'))?>')
        $('#jenis_komoditi_pasokan_bapok').selectize()[0].selectize.setValue('')
        $('#varian_komoditi_pasokan_bapok').selectize()[0].selectize.setValue('')
        $('#satuan_komoditi_pasokan_bapok').selectize()[0].selectize.setValue('')
        $('#jumlah_pasokan_bapok').val('')
        $('#pasar_pasokan_bapok_id').val('')
    }

    function update_regulasi(index, start, length) {
        $('#modal_bapok').modal('show');
        var tahun = ''
        var bulan = ''
        var jenis_komoditi = ''
        var jumlah = ''
        var pasar_pasokan_bapok_id = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/getPasokanBapok_pasar',
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                console.log(response);
                val_varian_komoditi_bapok = response.varian_komoditi_id
                val_satuan_komoditi_bapok = response.satuan_komoditi_id
                tahun = response.tahun
                bulan = response.bulan
                jenis_komoditi = response.jenis_komoditi_id+'|'+response.jenis_komoditi
                pasar_pasokan_bapok_id = response.pasar_pasokan_bapok_id
                jumlah = response.jumlah
            },
            complete: function() {
                $('#pasar_regulasi_id').val(pasar_regulasi_id)
                $('#tahun_pasokan_bapok').selectize()[0].selectize.setValue(tahun)
                $('#bulan_pasokan_bapok').selectize()[0].selectize.setValue(bulan)
                $('#jenis_komoditi_pasokan_bapok').selectize()[0].selectize.setValue(jenis_komoditi)
                $('#pasar_pasokan_bapok_id').val(pasar_pasokan_bapok_id)
                $('#jumlah_pasokan_bapok').val(jumlah)
            }
        })
    }

    $('#jenis_komoditi_pasokan_bapok').on('change', function() {
        var jenis_komoditi = $("#jenis_komoditi_pasokan_bapok").val();
        jenis_komoditi = jenis_komoditi.split("|")
        getvarian('varian_komoditi_pasokan_bapok', jenis_komoditi[0], val_varian_komoditi_bapok)
        getSatuan('satuan_komoditi_pasokan_bapok', jenis_komoditi[0], val_satuan_komoditi_bapok)
    })
</script>