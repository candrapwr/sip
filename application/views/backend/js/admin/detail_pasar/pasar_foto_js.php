<script>
    $(document).ready(function() {

    })

    function form_foto(id) {
        if (id == 'foto_depan') {
            $('#form_foto_pasar').show()
            $('#form_foto_lainnya').hide()
            $('#form_foto_depan').show()
            $('#form_foto_dalam').hide()
            $('#modal_foto').modal('show');
            $("#foto_depan").attr("required", true);
            $("#foto_dalam").attr("required", false);
            $("#foto_pasar").attr("required", false);
        } else if (id == 'foto_dalam') {
            $('#form_foto_pasar').show()
            $('#form_foto_lainnya').hide()

            $('#form_foto_depan').hide()
            $('#form_foto_dalam').show()
            $('#modal_foto').modal('show');
            $("#foto_dalam").attr("required", true);
            $("#foto_depan").attr("required", false);
            $("#foto_pasar").attr("required", false);
        } else if (id == 'foto_lainnya') {
            $('#form_foto_pasar').hide()
            $('#form_foto_lainnya').show()
            $('#modal_foto').modal('show');
            $("#foto_pasar").attr("required", true);
            $("#foto_dalam").attr("required", false);
            $("#foto_depan").attr("required", false);
        }

    }

    var i = 1

    function addForm() {
        var html = '';
        i = i + 1
        html += '<div class="input-group mt-2" id="formInputan' + i + '">' +
            '<input type="file" class="form-control" name="foto_pasar[]" id="foto_pasar" accept="image/png, image/jpeg" required>' +
            '<button onclick="removeForm(\'' + i + '\')" class="input-group-text text-white bg-danger"><i class="ti ti-trash"></i></button>' +
            '</div>';

        $('#formAdd').append(html);
    }

    function removeForm(i) {
        document.getElementById('formInputan' + i + '').remove();
        this.remove();
    }

    function hapus_foto(id) {
        Swal.fire({
            title: 'Apakah anda yakin?',
            text: "Data yang telah terhapus tidak bisa dikembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?= base_url() ?>delete/pasar/foto',
                    type: 'POST',
                    data: {
                        'id': id,
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            Swal.fire({
                                title: 'Sukses!',
                                icon: 'success',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutDown'
                                },
                                buttonsStyling: false,
                                customClass: {
                                    popup: 'border-radius-0'
                                },
                                html: 'I will close in <b></b> milliseconds.',
                                timer: 1000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                        const content = Swal.getHtmlContainer()
                                        if (content) {
                                            const b = content.querySelector('b')
                                            if (b) {
                                                b.textContent = Swal.getTimerLeft()
                                            }
                                        }
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then((result) => {
                                /* Read more about handling dismissals below */
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    //    console.log('I was closed by the timer')
                                    window.location.href = '<?=base_url()?>web/pasar/detail/<?=$this->uri->segment(4)?>';
                                }
                            })
                            after_update()
                        } else {
                            Swal.fire({
                                title: 'Gagal!',
                                icon: 'warning',
                                html: 'I will close in <b></b> milliseconds.',
                                timer: 1000,
                                timerProgressBar: true,
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutDown'
                                },
                                buttonsStyling: false,
                                customClass: {
                                    popup: 'border-radius-0'
                                },
                                didOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                        const content = Swal.getHtmlContainer()
                                        if (content) {
                                            const b = content.querySelector('b')
                                            if (b) {
                                                b.textContent = Swal.getTimerLeft()
                                            }
                                        }
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then((result) => {
                                /* Read more about handling dismissals below */
                                if (result.dismiss === Swal.DismissReason.timer) {
                                    //    console.log('I was closed by the timer')
                                }
                            })
                        }
                    }
                })
            }
        })
    }
</script>