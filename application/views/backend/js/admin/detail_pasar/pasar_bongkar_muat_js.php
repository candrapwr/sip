<script>
    $(document).ready(function() {
        showTableBongkarMuat()
        $('#jumlah_bongkar_muat').mask('000.000.000.000.000.000.000.000', {
            reverse: true
        });
    })

    function showTableBongkarMuat() {

        if ($.fn.DataTable.isDataTable('#tbl_bongkar_muat')) {
            $("#tbl_bongkar_muat").dataTable().fnDestroy();
            $('#tbl_bongkar_muat').empty();
        }

        var table = $('#tbl_bongkar_muat').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            lengthChange: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getBongkar_muat',
                data: {
                    refresh: 'refresh',
                    id: '<?= $index_pasar['pasar_id'] ?>',
                    jenis_anggaran: 'pedagang',
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Bulan',
                    data: 'tahun',
                    render: function(k, v, r) {
                        return r.bulan + ' ' + r.tahun
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                }
                <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'administrator') { ?>, {
                        title: 'Aksi',
                        data: 'pasar_bongkar_muat_id',
                        render: function(k, v, r, m) {
                            if (r.action) {
                                var button = `<button class="btn btn-sm btn-light m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>`

                                if (r.createdby == '<?= $this->session->userdata('email') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == 'administrator') {
                                    button = '<button onclick="hapus_data(\'' + r.pasar_bongkar_muat_id + '\', \'tbl_pasar_bongkar_muat\', \'pasar_bongkar_muat_id\', \'Hapus data anggaran dengan id ' + r.pasar_bongkar_muat_id + ' atas <?= $this->session->userdata('email') ?> \')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="fa-solid fa-trash-can"></i></button>'
                                }

                                return '<div class="row">' +
                                    '<div class="col-6 px-1">' +
                                    '<button onclick="update_omzet(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-primary m-1" title="Update"><i class="fa-solid fa-pen-to-square"></i></button>' +
                                    '</div>' +

                                    '<div class="col-6 px-1">' +
                                    button +
                                    '</div>'
                            } else {
                                return ''
                            }

                        }
                    }
                <?php } ?>
            ],
            scrollX: true,
            displayLength: 10
        });

    }

    function form_bongkar_muat() {
        $('#modal_bongkar_muat').modal('show');
        $('#tahun_bongkar_muat').selectize()[0].selectize.setValue('<?= date('Y') ?>')
        $('#bulan_bongkar_muat').selectize()[0].selectize.setValue('<?=bulan(date('Y-m-d'))?>')
        $('#jumlah_bongkar_muat').val('')
        $('#pasar_omzet_pedagang_id').val('')
    }

    function update_omzet(index, start, length) {
        $('#modal_omzet').modal('show');
        var tahun = ''
        var bulan = ''
        var omzet = ''
        var pasar_anggaran_id = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/getAnggaran',
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                jenis_anggaran: 'pedagang',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                tahun = response.tahun
                bulan = response.bulan
                omzet = response.omset
                anggaran = response.anggaran
                pasar_anggaran_id = response.pasar_anggaran_id
            },
            complete: function() {
                $('#tahun_omzet_pedagang').selectize()[0].selectize.setValue(tahun)
                $('#bulan_omzet').selectize()[0].selectize.setValue(bulan)
                $('#omset_pedagang').val(omzet)
                $('#pasar_omzet_pedagang_id').val(pasar_anggaran_id)
            }
        })
    }
</script>