
<script>
    $(document).ready(function() {
        showTable('refresh');

        $('#role_lainnya').selectize();
        $('#create-user').click(function() {
            $('#modal').modal('show')
        })

        $('#form-tambah').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>administrator/pengguna/add",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation
                    let error = response.error

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {
                        var timerInterval;
                        Swal.fire({
                            title: "Berhasil!",
                            icon: "success",
                            html: response.keterangan + '<br><b></b> milliseconds.',
                            timer: 2000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                const b = Swal.getHtmlContainer().querySelector('b')
                                timerInterval = setInterval(() => {
                                    b.textContent = Swal.getTimerLeft()
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then(function() {
                            $('#modal').modal('hide')
                            showTable('refresh')
                            $('#nip').val('')
                            $('#name').val('')
                            $('#instansi').val('')
                            $('#email').val('')
                            $('#daerah_id').val('')
                            $('#hp').val('')
                            $('#password').val('')
                        })


                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire(
                                'Gagal!',
                                response.keterangan,
                                'error'
                            )
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            element.after(value)

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })

      //  pencarian_pengguna()

    })

    filterPengguna

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            processing: true,
            serverSide: true,
            ordering: false,
            searchable: true,
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>pengguna/pengguna_json',
                "data": {
                    refresh: refresh,
                    tipe_pengguna: $('#tipe_pengguna').val(),
                    'search[value]':$('#search_pengguna').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'No',
                    data: 'pengguna_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama',
                    data: 'nama_lengkap',
                    render: function(k, v, r) {
                        return `<div class="d-flex">
                                <div class="flex-grow-1 ms-2">
                                    <h6 class="mb-0 chat-title">` + r.nama_lengkap + `</h6>
                                    <p class="mb-0 chat-msg">` + r.email + `</p>
                                    <p class="mb-0 chat-msg">NIP.` + r.nip + `</p>
                                </div>
                            </div>`
                    },
                    orderable: false
                },

                {
                    title: 'Handphone',
                    data: 'no_hp',
                    orderable: false,
                },
                {
                    title: 'Role',
                    data: 'tipe_pengguna',
                    render: function(k, v, r) {
                        var instansi = r.instansi
                        if (r.instansi == null) {
                            instansi = '-'
                        }
                        return `<div class="d-flex">
                                <div class="flex-grow-1 ms-2">
                                    <p class="mb-0 chat-msg"><b>` + r.tipe_pengguna + `</b></p>
                                    <p class="mb-0 chat-msg">` + instansi + `</p>
                                </div>
                            </div>`
                    },
                    orderable: false,
                    className: 'text-left'
                },
                {
                    title: 'Alamat',
                    data: 'alamat',
                    orderable: false,
                    width: 200,
                    render: function(k, v, r) {
                        return r.alamat + ', ' + r.kecamatan + ', ' + r.kab_kota + ', ' + r.provinsi
                    }
                },
                {
                    title: 'Aksi',
                    data: 'pengguna_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-sm btn-warning m-1" onclick="edit(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"> <i class="ti ti-edit"></i></button>' +
                            '<button class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.pengguna_id + '\',\'Deleted\',\'tbl_pengguna\',\'pengguna_id\',\'Hapus pengguna `' + ($.trim(r.nama_lengkap.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'
                    }
                },
            ],
            columnDefs: [{
                targets: 4,
                width: 100,
                className: 'text-center'
            }],
            "scrollX": true,
            "displayLength": 10
        });

    }


    function tambah() {
        $('#pengguna_id').val('')
        $('#nip').val('')
        $('#nama_lengkap').val('')
        $('#email').val('')
        $('#no_hp').val('')
        $('#alamat').val('')
        $('#daerah_id').val('')
        $('#tipe_pengguna').selectize()[0].selectize.setValue('')
        $('#provinsi').selectize()[0].selectize.setValue('')
        $('#kabupaten').selectize()[0].selectize.setValue('')
        $('#kabupaten').selectize()[0].selectize.setValue('')
        $('#kabupaten')[0].selectize.disable();
        $('#kecamatan')[0].selectize.disable();

        $("#tambahpengguna").modal({
            fadeDuration: 250
        });
    }

    function edit(action_index, start, length) {
        $.ajax({
            url: '<?= base_url() ?>pengguna/pengguna_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#modal').modal('show')
                $('#role').val(response.tipe_pengguna)
                $('#pengguna_id').val(response.pengguna_id)
                $('#instansi').val(response.instansi)
                $('#daerah_id').val(response.daerah_id.toString().substring(0,2))
                $('#nip').val(response.nip)
                $('#name').val(response.nama_lengkap)
                $('#email').val(response.email)
                $('#hp').val(response.no_hp)
                $('#role_lainnya').selectize()[0].selectize.setValue(response.tipe_pengguna_lainnya.toString().split(','));
            }
        })
        $("#tambahpengguna").modal({
            fadeDuration: 250
        });
    }

    function after_update() {
        showTable('refresh');
    }

    function searchNIP() {

        $.ajax({
            url: '<?= base_url() ?>administrator/master/pengguna/nip',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                nip: $('#nip').val()
            },
            dataType: 'json',
            beforeSend: function() {
                $('#loader-nip').show()
                $('#modal').LoadingOverlay("show")
            },
            complete: function() {
                $('#loader-nip').hide()
                $('#modal').LoadingOverlay("hide")
            },
            success: function(response) {
                $('#nip').val('')
                $('#name').val('')
                $('#email').val('')
                $('#hp').val('')

                if (response.success) {
                    $('#nip').val(response.data.nip)
                    $('#name').val(response.data.nama)
                    $('#email').val(response.data.email)
                } else {
                    if (response.message.length > 0) {
                        Swal.fire({
                            title: response.message,
                            icon: 'warning',
                        })
                    }
                }
            }
        })
    }

    function filterPengguna() {
        showTable('refresh');
    }

    // $('#tipe_pengguna').on('change', function() {
    //     var daerah_id = $("#tipe_pengguna").val();
    //     showTable('refresh');

    // })

    function generatePassword(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    $('#refresh_pass').on('click', function() {
        document.getElementById('password').type = 'text';
        $('#password').val(generatePassword(8))
    })
</script>