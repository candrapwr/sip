
<script>
    $(document).ready(function() {
        showTable('refres');

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/getProgram_pasar',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'program_pasar_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Program Pasar',
                    data: 'program_pasar',
                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.status == 'Aktif') {
                            return '<div class="badge bg-success">Aktif</div>'
                        } else {
                            return '<div class="badge bg-danger">Tidak Aktif</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'program_pasar_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'
                        var status = 'Tidak Aktif'
                        if (r.status == 'Tidak Aktif') {
                            button_color = 'btn-success'
                            status = 'Aktif'
                        }

                        return '<button class="btn btn-warning btn-sm m-1" onclick="edit(\'' + m.row + '\')"> <i class="ti ti-edit"></i></button>' +
                            // '<button class="btn ' + button_color + ' btn-sm  m-1" onclick="update_status(\'' + r.jenis_komoditi_id + '\', \'' + status + '\',\'tbl_jenis_komoditi\',\'jenis_komoditi_id\',\'Hapus Jenis Komoditi `' + ($.trim(r.jenis_komoditi.replace(/[\t\n]+/g, ''))) + '`\')"><i class="ti ti-power"></i></button>' +
                            '<button class="btn btn-danger btn-sm m-1" onclick="hapus_data(\'' + r.program_pasar_id + '\',\'Deleted\',\'tbl_program_pasar\',\'program_pasar_id\',\'Hapus Program Pasar `' + ($.trim(r.program_pasar.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function add(action_index) {
        $("#modal").modal('show')
        $('#modal-title').text('Tambah Program Pasar')
        $('#program_pasar_id').val('')
        $('#program_pasar').val('')
    }

    function edit(action_index) {
        $.ajax({
            url: '<?= base_url() ?>master/getProgram_pasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#program_pasar_id').val(response.program_pasar_id)
                $('#program_pasar').val(response.program_pasar)
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }

    function hapus_data(id, status, table, key, keterangan) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pengguna/update_status',
                    type: 'POST',
                    data: {
                        'table_name': table,
                        'key_name': key,
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': keterangan,
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            showTable('refress')

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }
</script>