
<script>
    $(document).ready(function() {
        showTable('refres');

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/jenis_komoditi_json',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'jenis_komoditi_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Jenis Komoditi',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Kolompok',
                    data: 'kelompok',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.kelompok == 'Bapok') {
                            return '<div class="badge bg-primary">Bahan Pokok</div>'
                        } else {
                            return '<div class="badge bg-warning">Bahan Penting</div>'
                        }
                    }
                },
                {
                    title: 'Satuan Komoditi',
                    data: 'jenis_komoditi',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-dark btn-sm m-1" onclick="view(\'' + r.jenis_komoditi + '\', \'' + r.jenis_komoditi_id + '\')"><i class="ti ti-eye me-1"></i>Lihat Satuan Komoditi </button>'
                    }
                },

            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function showTableVarian(refresh, jenis_komoditi_id) {

        if ($.fn.DataTable.isDataTable('#table_view')) {
            $("#table_view").dataTable().fnDestroy();
            $('#table_view').empty();
        }

        var table = $('#table_view').DataTable({

            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/satuan_komoditi_json',
                data: {
                    refresh: refresh,
                    jenis_komoditi_id: jenis_komoditi_id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'satuan_komoditi_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Satuan Komoditi',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.status == 'Aktif') {
                            return '<div class="badge bg-success">Aktif</div>'
                        } else {
                            return '<div class="badge bg-danger">Tidak Aktif</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'satuan_komoditi_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'
                        var status = 'Tidak Aktif'
                        if (r.status == 'Tidak Aktif') {
                            button_color = 'btn-success'
                            status = 'Aktif'
                        }

                        return '<button class="btn btn-warning btn-sm m-1" onclick="edit(\'' + m.row + '\')"> <i class="ti ti-edit"></i></button>' +
                            // '<button class="btn ' + button_color + ' btn-sm  m-1" onclick="update_status(\'' + r.jenis_komoditi_id + '\', \'' + status + '\',\'tbl_jenis_komoditi\',\'jenis_komoditi_id\',\'Hapus Jenis Komoditi `' + ($.trim(r.jenis_komoditi.replace(/[\t\n]+/g, ''))) + '`\')"><i class="ti ti-power"></i></button>' +
                            '<button class="btn btn-danger btn-sm m-1" onclick="hapus_data(\'' + r.satuan_komoditi_id + '\',\'' + r.jenis_komoditi_id + '\',\'Deleted\',\'tbl_satuan_komoditi\',\'varian_satuan_id\',\'Hapus Satuan Komoditi `' + ($.trim(r.satuan_komoditi.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function view(jenis_komoditi, jenis_komoditi_id) {
        showTableVarian('refresh', jenis_komoditi_id)
        $("#modal_satuan_komoditi").modal('show')
        $('#modal_title_v').text(jenis_komoditi)
    }

    function add(action_index) {
        $('#jenis_komoditi').selectize()
        $('#satuan_komoditi_id').val('')
        $('#satuan_komoditi').val('')
        $("#modal").modal('show')
        $('#modal-title').text('Tambah Satuan Komoditi')
    }

    function edit(action_index) {
        $('#modal-title').text('Edit Satuan Komoditi')
        $.ajax({
            url: '<?= base_url() ?>master/satuan_komoditi_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#satuan_komoditi_id').val(response.satuan_komoditi_id)
                $('#satuan_komoditi').val(response.satuan_komoditi)
                $('#jenis_komoditi').selectize()[0].selectize.setValue(response.jenis_komoditi_id)
            },
            complete: function() {
                $("#modal_satuan_komoditi").modal('hide')
                $("#modal").modal('show')
            }
        })
    }

    function hapus_data(id, jenis_komoditi_id, status, table, key, keterangan) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pengguna/update_status',
                    type: 'POST',
                    data: {
                        'table_name': table,
                        'key_name': key,
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': keterangan,
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            showTableVarian('refress', jenis_komoditi_id)

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }
</script>