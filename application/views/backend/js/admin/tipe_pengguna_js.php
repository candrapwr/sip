
<script>
    $(document).ready(function() {
        showTable();

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/tipe_pengguna_json',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'ID Pengguna',
                    data: 'tipe_pengguna_id',
                },
                {
                    title: 'Tipe Pengguna',
                    data: 'tipe_pengguna',
                },
                {
                    title: 'Flag Publik',
                    data: 'flag_publik',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.flag_publik == 'Y') {
                            return '<div class="badge bg-success m-1"><i class="ti ti-circle-check"></i></div>'
                        } else {
                            return '<div class="badge bg-warning m-1"><i class="ti ti-lock"></i></div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'tipe_pengguna_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-dark btn-sm m-1" onclick="view(\'' + r.tipe_pengguna_id + '\', \'' + r.tipe_pengguna + '\')"><i class="ti ti-list-details"></i></button>' +
                            '<button class="btn btn-warning btn-sm m-1" onclick="edit(\'' + m.row + '\')"> <i class="ti ti-edit"></i></button>' +
                            '<button class="btn btn-danger btn-sm m-1" onclick="update(\'' + r.tipe_pengguna_id + '\',\'Deleted\',\'tbl_tipe_pengguna\',\'tipe_pengguna_id\',\'Hapus tipe pengguna `' + ($.trim(r.tipe_pengguna.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function add(action_index) {
        $("#modal").modal('show')
        $('#tipe_pengguna_id').val('')
        $('#tipe_pengguna').val('')
        $('#flag_publik').val('Y')
    }

    function edit(action_index) {
        $.ajax({
            url: '<?= base_url() ?>master/tipe_pengguna_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#tipe_pengguna_id').val(response.tipe_pengguna_id)
                $('#tipe_pengguna').val(response.tipe_pengguna)
                $('#flag_publik').val(response.flag_publik)
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }


    function edit_akses(tipe_pengguna_id, menu_id) {
        $.ajax({
            url: '<?= base_url() ?>pengguna/getTipe_pengguna_menu',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                tipe_pengguna_id: tipe_pengguna_id,
                menu_id : menu_id
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#title_edit_akses').text(response.data[0].menu)
                var create = ''
                var update = ''
                var deleted = ''
                if(response.data[0].create == 'Y'){
                    create = 'checked'
                }
                if(response.data[0].update == 'Y'){
                    update = 'checked'
                }
                if(response.data[0].delete == 'Y'){
                    deleted = 'checked'
                }

                $('#aksi').html(`<div class="col-sm-10">
                                    <div class="form-check mb-3">
                                        <input class="form-check-input" name="tambah" value="Y" type="checkbox" id="tambah" `+create+`>
                                        <label class="form-check-label" for="tambah">
                                            Tambah
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-check mb-3">
                                        <input class="form-check-input" name="update" value="Y" type="checkbox" id="update" `+update+`>
                                        <label class="form-check-label" for="update">
                                            Update
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-10">
                                    <div class="form-check mb-3">
                                        <input class="form-check-input" name="deteled" value="Y" type="checkbox" id="deleted" `+deleted+`>
                                        <label class="form-check-label" for="deteled">
                                            Deleted
                                        </label>
                                    </div>
                                </div>`)
            },
            complete: function() {
                $("#modal_edit_akses").modal('show')
            }
        })
    }

    function view(id, nama) {
        $('#modal_menu_pengguna').modal('show')
        $('#title_tipe_pengguna').text(nama)
        $('#btn_add').html('<div class="row" style="text-align:right"><div class="col-md-12"><button class="btn btn-warning  m-1" onclick="add_permission(\'' + id + '\', \'' + nama + '\')"> <i class="ti ti-plus me-1"></i>Tambah Akses</button></div></div>');

        if ($.fn.DataTable.isDataTable('#table_view')) {
            $("#table_view").dataTable().fnDestroy();
            $('#table_view').empty();
        }

        var table = $('#table_view').DataTable({

            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pengguna/getTipe_pengguna_menu',
                data: {
                    tipe_pengguna_id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'menu_id',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Menu',
                    data: 'menu',
                    render: function(k, v, r) {

                        var color = 'success';
                        if (r.jenis == 'Back end') {
                        color = 'dark';
                        }

                        var parent = '';
                        if ((r.parent_id == '' || r.parent_id == null) && r.jenis == 'Back end') {
                            parent = '<div class="badge bg-warning ms-1" style="margin-left: 5px;">Parent Menu</div>'
                        }
                        
                        return r.menu+
                        '<br><div class="badge bg-'+color+' bckn-'+color+'">'+r.jenis+'</div>' +
                        parent
                    }
                },
                {
                    title: 'CRUD',
                    data: 'menu',
                    render: function(k, v, r) {
                        var color_c = 'danger';
                        var color_u = 'danger';
                        var color_d = 'danger';
                        var checked_c = 'ti ti-x';
                        var checked_u = 'ti ti-x';
                        var checked_d = 'ti ti-x';

                        if (v.create == 'Y') {
                            checked_c = 'mdi-checkbox-marked-circle-outline'
                            color_c = 'success';
                        }

                        if (v.update == 'Y') {
                            checked_u = 'mdi-checkbox-marked-circle-outline'
                            color_u = 'success';
                        }

                        if (v.delete == 'Y') {
                            checked_d = 'mdi-checkbox-marked-circle-outline'
                            color_d = 'success';
                        }

                        return '<div class="badge bg-success m-1"><i class="ti ti-circle-check"></i></div> View<br>' +
                            '<div class="badge bg-' + color_c + ' m-1"><i class="mdi ' + checked_c + '"></i></div> Tambah <br>' +
                            '<div class="badge bg-' + color_u + ' m-1"><i class="mdi ' + checked_u + '"></i></div> Edit <br>' +
                            ' <div class="badge bg-' + color_d + ' m-1"><i class="mdi ' + checked_d + '"></i></div> Hapus'
                    }
                },
                {
                    title: 'Aksi',
                    data: 'menu_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-danger btn-sm m-1" onclick="update(\'' + r.tipe_pengguna_menu_id + '\',\'Deleted\',\'tbl_tipe_pengguna_menu\',\'tipe_pengguna_menu_id\',\'Hapus tipe pengguna menu `' + ($.trim(r.tipe_pengguna_menu_id.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function add_permission(id, nama) {
        var permission = '';
        $('#modal_permission').modal('show')
        $('#modal_menu_pengguna').modal('hide')
        $('#title_modal_permission').text(nama)
        $('#tipe_pengguna_v_id').val(id)
        $.ajax({
            url: '<?= base_url() ?>pengguna/getTipe_pengguna_permission',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                status: 'Aktif',
                refresh: 'refresh',
                tipe_pengguna_id: id
            },
            beforeSend: function() {
                $('#permission').LoadingOverlay("show");
            },
            success: function(response) {
                response = JSON.parse(response)
                $.each(response.data, function(k, v) {
                    var color = 'success';
                    var checked_r = '';
                    var checked_c = '';
                    var checked_u = '';
                    var checked_d = '';
                    if (v.view == 'Y') {
                        checked_r = 'checked'
                    }

                    if (v.create == 'Y') {
                        checked_c = 'checked'
                    }

                    if (v.update == 'Y') {
                        checked_u = 'checked'
                    }

                    if (v.delete == 'Y') {
                        checked_d = 'checked'
                    }

                    if (v.jenis == 'Back end') {
                        color = 'dark';
                    }

                    var parent = '';
                    if ((v.parent_id == '' || v.parent_id == null)) {
                        parent = '<br><div class="badge bg-warning ms-1">Parent Menu</div>'
                    }else{
                        parent = '<br><div class="badge bg-success ms-1">Parent : '+v.parent_menu+'</div>'
                    }

                    permission += '<div class="col-lg-3">' +
                        '<div class="card border border-primary">' +
                        '<div class="card-body text-primary">' +
                        '<div class="checkbox checkbox-primary">' +
                        '<input id="permission-' + v.menu_id + '" ' + checked_r + ' name="aksi[]" value="' + v.menu_id + '" type="checkbox" data-parsley-multiple="permission-' + v.permission_id + '">' +
                        '<input  ' + checked_r + ' name="all[]" value="' + v.tipe_pengguna_menu_id + '" type="checkbox" class="d-none">' +
                        '<label class="text-bold" for="permission-' + v.menu + '">' +
                        '<strong class="ms-5">' + v.menu + '</strong>' +
                        '</label>' +
                        '</div>' +
                        '<div class="badge bg-' + color + '">' + v.jenis + '</div>' +
                        parent +
                        '<hr>' +
                        '<div class="checkbox checkbox-primary">' +
                        '<div class="ms-1">' +
                        '<input id="parent-create" name="create[' + v.menu_id + ']" ' + checked_c + ' value="Y" type="checkbox" data-parsley-multiple="parent-create">' +
                        '<label for="parent-create" class="ms-5"> Tambah </label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="checkbox checkbox-primary">' +
                        '<div class="ms-1">' +
                        '<input id="parent-update" name="update[' + v.menu_id + ']" value="Y" ' + checked_u + ' type="checkbox" data-parsley-multiple="parent-update">' +
                        '<label for="parent-update" class="ms-5"> Update </label>' +
                        '</div>' +
                        '</div>' +
                        '<div class="checkbox checkbox-primary">' +
                        '<div class="ms-1">' +
                        '<input id="parent-hapus" name="hapus[' + v.menu_id + ']" value="Y" ' + checked_d + ' type="checkbox" data-parsley-multiple="parent-hapus">' +
                        '<label for="parent-hapus" class="ms-5"> Hapus </label>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                })
            },
            complete: function() {
                $('#permission').html(permission);
                $('#permission').LoadingOverlay("hide");
            }
        })
    }

    function hapus_data(id, status, ket, flag) {
        Swal.fire({
            title: 'Konfirmasi Hapus',
            text: "Apakah Anda yakin ingin menghapusnya?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url() ?>pengguna/update_status',
                    type: 'POST',
                    data: {
                        'table_name': 'tbl_menu',
                        'key_name': 'menu_id',
                        'key': id,
                        'status': status,
                        'keterangan_perubahan': 'Deleted Permission',
                        'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (response.kode == 200) {
                            var timerInterval;
                            Swal.fire({
                                title: "Succeed!",
                                icon: "success",
                                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                                timer: 2000,
                                timerProgressBar: true,
                                didOpen: () => {
                                    Swal.showLoading()
                                    const b = Swal.getHtmlContainer().querySelector('b')
                                    timerInterval = setInterval(() => {
                                        b.textContent = Swal.getTimerLeft()
                                    }, 100)
                                },
                                willClose: () => {
                                    clearInterval(timerInterval)
                                }
                            })

                            showTable('refress')

                        } else {
                            Swal.fire(
                                'Fail!',
                                response.keterangan,
                                'error'
                            )
                        }
                    }
                })

            }
        })

    }
</script>