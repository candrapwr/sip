<script>
    <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [3, 10, 11, 15, 13, 14])) {  ?>
        $(function() {
            $('#tipe_pasar').selectize()
            $('#kepemilikan').selectize()
            $('#kondisi').selectize()
            $('#pengelola').selectize()
            $('#kepemilikan').selectize()
            $('#bentuk').selectize()
            $('#provinsi').selectize()
            $('#kabupaten').selectize()
            $('#select-provinsi-bahan-pokok').selectize()

            <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                $('#kabupaten')[0].selectize.disable()
            <?php endif; ?>

            <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>
                $('#pengelola')[0].selectize.disable()
            <?php endif; ?>

            getHargaBahanPokok()
            getHargaBahanPokokEWS()


            $('#search-bahan-pokok-ews').click(function() {
                const prov = $('#filter-bahan-pokok-ews-provinsi').val()
                const date1 = $('#filter-bahan-pokok-ews-date1').val()
                const date2 = $('#filter-bahan-pokok-ews-date2').val()

                getHargaBahanPokokEWS(prov, date1, date2)
            })

            <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'pengawas migor' || strtolower($this->session->userdata('role')) == 'sardislog' || strtolower($this->session->userdata('role')) == 'bapokting' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                if ($('#provinsi').val() != '') {
                    const provId = $('#provinsi').val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/kab/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#kabupaten').selectize()[0].selectize.setValue('')
                            $('#kabupaten').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#kabupaten')[0].selectize.disable();
                                } else {
                                    $('#kabupaten')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#kabupaten').selectize()[0].selectize.addOption({
                                            value: row.daerah_id,
                                            text: row.kab_kota
                                        })
                                    })

                                    if ($('#filter-search-kabupaten-flash').val() != '') {
                                        $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                                    }
                                }
                            }
                        }
                    })

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                }

                $('#filter-tahun-tpdak').change(function() {
                    const year = $(this).val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/sum-tpdak/json",
                        type: 'post',
                        data: {
                            year: year,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (response.data.length > 0) {
                                    $.each(response.data, function(i, row) {
                                        $('#count-tp > span').text(row.jml_tp)
                                        $('#count-tp > span').attr('data-stop', row.jml_tp)
                                        $('#count-dak > span').text(row.jml_dak)
                                        $('#count-dak > span').attr('data-stop', row.jml_dak)
                                        $('#count-nontpdak > span').text(row.jml_non_dak)
                                        $('#count-nontpdak > span').attr('data-stop', row.jml_non_dak)
                                    })
                                } else {
                                    $('#count-tp > span').text('0')
                                    $('#count-tp > span').attr('data-stop', '0')
                                    $('#count-dak > span').text('0')
                                    $('#count-dak > span').attr('data-stop', '0')
                                    $('#count-nontpdak > span').text('0')
                                    $('#count-nontpdak > span').attr('data-stop', '0')
                                }
                            }
                        }
                    })
                })
            <?php endif; ?>

            <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                if ($('#kabupaten').val() != '') {
                    const provId = $('#kabupaten').val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                }

                $('#kabupaten').change(function() {
                    const provId = $(this).val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                })
            <?php endif; ?>


            $('#search-bahan-pokok').click(function() {
                const prov = $('#filter-bahan-pokok-provinsi').val()
                const date1 = $('#filter-bahan-pokok-date1').val()
                const date2 = $('#filter-bahan-pokok-date2').val()
                const kab = $('#filter-bahan-pokok-kabupaten').val()
                const pasar = $('#filter-bahan-pokok-pasar').val()

                getHargaBahanPokok(prov, date1, date2, kab, pasar)
            })

            <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>
                $('#filter-bahan-pokok-provinsi').change(function() {
                    const provId = $(this).val()

                    $.ajax({
                        url: "<?= site_url() ?>dashboard/kab/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#filter-bahan-pokok-kabupaten').prop('disabled', true)
                                    $('#filter-bahan-pokok-kabupaten').html('<option value="">Choose...</option>')
                                } else {
                                    $('#filter-bahan-pokok-kabupaten').prop('disabled', false)

                                    let optionKab = '<option value="">Choose...</option>'

                                    $.each(response.data, function(i, row) {
                                        optionKab += '<option value="' + row.daerah_id + '|' + row.kab_kota + '">' + row.kab_kota + '</option>'
                                    })

                                    $('#filter-bahan-pokok-kabupaten').html(optionKab)
                                }
                            }

                            getPasar(provId)
                        }
                    })
                })

                $('#filter-bahan-pokok-kabupaten').change(function() {
                    const kabId = $(this).val()

                    if (kabId != '') {
                        getPasar(kabId)
                    }
                })
            <?php endif; ?>

            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                $('#filter-bahan-pokok-provinsi').change(function() {
                    const provId = $(this).val()
                    getPasar(provId)
                })
            <?php endif; ?>

            $('#provinsi').change(function() {
                const provId = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>dashboard/kab/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#kabupaten').selectize()[0].selectize.setValue('')
                        $('#kabupaten').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#kabupaten')[0].selectize.disable();
                            } else {
                                $('#kabupaten')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#kabupaten').selectize()[0].selectize.addOption({
                                        value: row.daerah_id,
                                        text: row.kab_kota
                                    })
                                })
                            }
                        }

                        $.ajax({
                            url: "<?= site_url() ?>dashboard/pasar/pengelola/json",
                            type: 'post',
                            data: {
                                provinsi_id: provId,
                                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                            },
                            dataType: 'json',
                            beforeSend: function() {
                                $('#pengelola').selectize()[0].selectize.setValue('')
                                $('#pengelola').selectize()[0].selectize.clearOptions()
                            },
                            success: function(response) {
                                if (response.code == 401) {
                                    window.location.href = '<?= site_url() ?>user/sign_out'
                                } else {
                                    if (!response.success) {
                                        $('#pengelola')[0].selectize.disable();
                                    } else {
                                        $('#pengelola')[0].selectize.enable();

                                        $.each(response.data, function(i, row) {
                                            $('#pengelola').selectize()[0].selectize.addOption({
                                                value: row.email,
                                                text: row.nama_lengkap
                                            })
                                        })

                                        if ($('#filter-search-pengelola-flash').val() != '') {
                                            $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                        }
                                    }
                                }
                            }
                        })
                    }
                })
            })

        })

        function getPasar(provId) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/pasar/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (response.success) {
                            $('#filter-bahan-pokok-pasar').prop('disabled', false)

                            let optionPasar = '<option value="">Choose...</option>'

                            $.each(response.data, function(i, row) {
                                optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                            })

                            $('#filter-bahan-pokok-pasar').html(optionPasar)
                        } else {
                            $('#filter-bahan-pokok-pasar').prop('disabled', true)
                            $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                        }
                    }
                }
            })
        }

        function getHargaBahanPokok(provinsi = null, date1 = null, date2 = null, kab = null, pasar = null) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/harga/bahan-pokok/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    kab: kab,
                    pasar: pasar,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok').hide()
                    $('#loader-bahan-pokok').show()
                },
                complete: function() {
                    $('#load-bahan-pokok').show()
                    $('#loader-bahan-pokok').hide()
                },
                success: function(response) {
                    $('#date1').text(response.date1)
                    $('#date2').text(response.date2)
                    $('#bahan-pokok-title').text(response.region)

                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        let tableData = ''

                        if (response.data.length > 0) {
                            $.each(response.data, function(i, row) {
                                let ket = '-';


                                if (row.ket == 'up') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                } else if (row.ket == 'down') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                } else if (row.ket == 'equals') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                }

                                tableData += '<tr>'
                                tableData += '<td style="font-weight:600;" class="text-left">' + row.jenis_komoditi + '</td>'
                                tableData += '<td>' + row.satuan_komoditi + '</td>'
                                tableData += '<td class="text-center">' + ((row.date1) == 0 || row.date1 == '' ? '-' : row.date1) + '</td>'
                                tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' ? '-' : row.date2) + '</td>'
                                tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '' ? '-' : row.percentage + '%') + '</td>'
                                tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '' ? '-' : ket) + '</td>'
                            })
                        } else {
                            tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>'
                        }

                        $('#harga-bahan-pokok').html(tableData)
                    }
                }
            })
        }

        showAjax('summary-pasar', 'stat-jumlah-pasar', [1, '<?= date('Y-m-d') ?>']);
        showAjax('summary-pasar', 'stat-pasar-input-hari-ini', [2, null]);
        showAjax('summary-pasar', 'stat-pasar-input-tahun-ini', [3, 2, null]);

        showAjax('summary-lapor', 'stat-pasar-lapor-lengkap', ['Lengkap']);
        showAjax('summary-lapor', 'stat-pasar-lapor-belum-lengkap', ['Belum Lengkap']);
        showAjax('summary-lapor', 'stat-pasar-belum-lapor', ['Belum Lapor']);

        showAjax('summary-program', 'count-tp', ['jml_tp']);
        showAjax('summary-program', 'count-dak', ['jml_dak']);
        showAjax('summary-program', 'count-nontpdak', ['jml_non_dak']);

        <?php if ($this->session->userdata('tipe_pengguna_id') != 11) : ?>
            showAjax('summary-pengelola', 'stat-provinsi-pengelola-terdaftar', ['prov']);
            showAjax('summary-pengelola', 'stat-kabkot-pengelola-terdaftar', ['kabkot']);
            showAjax('summary-pengelola', 'stat-pengelola-terdaftar', ['total']);
        <?php endif; ?>

        function getLoadPasar(page = '') {
            $.ajax({
                url: '<?= site_url() ?>dashboard/ajax/pasar-dasboard?page=' + page,
                type: 'post',
                data: {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                beforeSend: function() {
                    if (page == '')
                        $('#ajaxpasar').html('<div style="height: 300px;"><div style="top: 0%;position: relative;"><div style="width: 50px;height: 50px" class="spinner-grow text-primary" role="status"></div><div style="width: 50px;height: 50px" class="spinner-grow text-secondary" role="status"></div><div style="width: 50px;height: 50px" class="spinner-grow text-success" role="status"></div><br><br><h5 class="text-primary">SEDANG MEMUAT</h5></div></div>');
                    else
                        $('#ajax_links').html('<div class="spinner-grow text-primary" role="status"></div><div class="spinner-grow text-secondary" role="status"></div><div class="spinner-grow text-success" role="status"></div>');
                },
                complete: function() {},
                success: function(response, textStatus, xhr) {
                    $('#ajaxpasar').html(response);
                },
                error: function(request, status, error) {
                    //location.reload();
                }
            })
        }

        getLoadPasar();

        function getLoadPasarFilter() {
            var form = $('#idForm');
            var actionUrl = '<?= site_url() ?>dashboard/ajax/pasar-dasboard?filter=<?= sha1(md5($this->session->userdata('username'))) ?>';
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: form.serialize(),
                beforeSend: function() {
                    $('#ajaxpasar').html(`<div class="row gy-4" style="height:400px">
                        <div class="col-12 text-center">
                            <lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" loop autoplay></lottie-player>
                            <h6 class="text-primary">Sedang Memuat Daftar Pasar...</h6>
                        </div>
                    </div>`);
                },
                success: function(data) {
                    $('#ajaxpasar').html(data);
                },
                error: function(request, status, error) {
                    //location.reload();
                }
            });
        }


        function getHargaBahanPokokEWS(provinsi = null, date1 = null, date2 = null) {
            $.ajax({
                url: "<?= site_url() ?>dashboard/bahan-pokok-ews/json",
                type: 'post',
                data: {
                    provinsi: provinsi,
                    date1: date1,
                    date2: date2,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#load-bahan-pokok-ews').hide()
                    $('#loader-bahan-pokok-ews').show()
                },
                complete: function() {
                    $('#load-bahan-pokok-ews').show()
                    $('#loader-bahan-pokok-ews').hide()
                },
                success: function(response) {
                    $('#date1-ews').text(response.date1)
                    $('#date2-ews').text(response.date2)
                    $('#bahan-pokok-title').text(response.region)

                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        let tableData = ''

                        if (response.data.length > 0) {
                            $.each(response.data, function(i, row) {
                                let ket = '-';

                                if (row.indicator == 'up') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                } else if (row.indicator == 'down') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                } else if (row.indicator == '') {
                                    ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                                }

                                tableData += '<tr>'
                                tableData += '<td style="font-weight:600;" class="text-left">' + row.commodity_name + '</td>'
                                tableData += '<td>' + row.measurement + '</td>'
                                tableData += '<td class="text-center">' + ((row.price_2) == 0 || row.price_2 == '' ? '-' : row.price_2) + '</td>'
                                tableData += '<td class="text-center">' + ((row.price_1) == 0 || row.price_1 == '' ? '-' : row.price_1) + '</td>'
                                tableData += '<td class="text-center">' + ((row.price_1) == 0 || row.price_1 == '' || (row.price_2) == 0 || row.price_2 == '' ? '-' : row.PercentChange + '') + '</td>'
                                tableData += '<td class="text-center">' + ((row.price_1) == 0 || row.price_1 == '' || (row.price_2) == 0 || row.price_2 == '' ? '-' : ket) + '</td>'
                            })
                        } else {
                            tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>'
                        }

                        $('#harga-bahan-pokok-ews').html(tableData)
                    }
                }
            })
        }
    <?php } else { ?>
        showAjax('summary-pasar', 'stat-jumlah-pasar', [1, '<?= date('Y-m-d') ?>']);
        showAjax('summary-pasar', 'stat-pasar-input-hari-ini', [2, null]);
        showAjax('summary-pasar', 'stat-pasar-input-tahun-ini', [3, 2, null]);

        showAjax('summary-lapor', 'stat-pasar-lapor-lengkap', ['Lengkap']);
        showAjax('summary-lapor', 'stat-pasar-lapor-belum-lengkap', ['Belum Lengkap']);
        showAjax('summary-lapor', 'stat-pasar-belum-lapor', ['Belum Lapor']);
    <?php }  ?>

    var summary_data = [];

    function showAjax(jenis, div_id, param = []) {
        var url_api = '';
        var div_data = [];

        if (jenis == 'summary-pasar') {
            url_api = '<?= site_url() ?>dashboard/summary/pasar-json';
        } else if (jenis == 'summary-lapor') {
            url_api = '<?= site_url() ?>dashboard/summary/lapor-json';
        } else if (jenis == 'summary-pengelola') {
            url_api = '<?= site_url() ?>dashboard/summary/pengelola-json';
        } else if (jenis == 'summary-program') {
            url_api = '<?= site_url() ?>dashboard/summary/program-json';
        }

        $.ajax({
            url: url_api,
            type: 'post',
            data: {
                param: param,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#' + div_id).html('<div class="spinner-border text-primary" role="status"></div>');
            },
            complete: function() {},
            success: function(response, textStatus, xhr) {
                //console.log(response)
                if (response.kode == 200) {
                    summary_data[div_id] = response.data;
                    $('#' + div_id).html('<span class="count-text" data-speed="3000" data-stop="' + response.data + '">' + response.data + '</span>');
                } else {
                    //location.reload();
                }
            },
            error: function(request, status, error) {
                //location.reload();
            }
        })
    }
</script>