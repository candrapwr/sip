<script>
    $(function() {
        showData('refresh')

        $('#assign_pasar').selectize()
    })

    function showData(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>kontributor/kontributor_json',
                "data": {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'Nama',
                    data: 'nama_lengkap',
                    orderable: false
                },
                {
                    title: 'Email',
                    data: 'email',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.email + '<br>' + r.no_hp
                    }
                },
                {
                    title: 'Alamat',
                    data: 'alamat',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.alamat + ', ' + r.kecamatan + ', ' + r.kab_kota + ', ' + r.provinsi
                    }
                },
                {
                    title: 'Aksi',
                    data: 'pengguna_id',
                    render: function(k, v, r, m) {

                        if (r.status == 'Belum Verifikasi') {
                            return ''
                        } else {
                            return r.status == 'Menunggu Verifikasi' ? '<button class="btn btn-sm btn-success m-1" onclick="approve(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"><i class="ti ti-circle-check"></i></button>' : '<button class="btn btn-sm btn-warning m-1" onclick="assign(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"><i class="ti ti-pencil"></i></button>'
                        }
                    }
                },


            ],
            "scrollX": true,
            "displayLength": 5
        });
    }

    $('#form-data-assign').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>kontributor/save",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Data berhasil disimpan.'

                    })
                }
            }
        })
    })


    function assign(action_index, start, length) {

        $.ajax({
            url: '<?= base_url() ?>kontributor/assign_json',
            type: 'POST',
            dataType: 'json',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            beforeSend: function() {
                $('#assign_pasar').selectize()[0].selectize.setValue('')
                $('#assign_pasar').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {

                $("#tambahpengguna").modal('show')

                $.each(response.data_pasar, function(i, row) {
                    $('#assign_pasar').selectize()[0].selectize.addOption({
                        value: row.pasar_id,
                        text: row.nama
                    })

                    $('#assign_pasar').selectize()[0].selectize.setValue(response.pasar_id)
                })

                $('#pengguna_id').val(response.userID)
                $('#email').val(response.email)
            }
        })


    }

    function approve(action_index, start, length) {
        $.ajax({
            url: '<?= base_url() ?>kontributor/approve',
            type: 'POST',
            dataType: 'json',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            success: function(response) {
                if (response.success) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: response.message
                    }).then((result) => {
                        window.location.href = '<?= site_url() ?>dashboard/master/kontributor'
                    })
                } else {
                    Swal.fire({
                        title: 'Error!',
                        icon: 'success',
                        text: response.message
                    })
                }
            }
        })
    }
</script>