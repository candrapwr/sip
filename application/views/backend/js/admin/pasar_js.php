<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-mask-plugin@1.14.16/dist/jquery.mask.min.js"></script>

<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkp-vI9gmy3zT9UfDivn9i_brbpNR8EK8" async defer></script> -->

<script>
    $(document).ready(function() {
        $('#provinsi_filter, #kabkota_filter, #tipe_pasar_filter, #program_pasar_filter, #penerapan_digitalisasi_pasar').selectize()
        showTable('refresh')
        <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
            getDaerah('provinsi_filter', 0, '<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
        <?php } else { ?>
            getDaerah('provinsi_filter', 0)
        <?php } ?>
        //maps()
        getTipePasar('tipe_pasar_filter')

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 2000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    });

    var kabkota = ''
    var kecamatan = ''
    var kelurahan = ''
    var form = $("#pasar_form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            error.insertAfter(element);
        },

        errorClass: "error is-invalid",
        rules: {
            // pasar_id: "required",
        },
        messages: {},
        invalidHandler: function(form, validator) {

        }
    });



    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').prop('disabled', false)

                        let optionPasar = '<option value="">Choose...</option>'

                        $.each(response.data, function(i, row) {
                            optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                        })

                        $('#filter-bahan-pokok-pasar').html(optionPasar)
                    } else {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                    }
                }
            }
        })
    }

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ordering: false,
            searchable: true,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getPasar',
                data: {
                    daerah_id: ($('#kabkota_filter').val() != null && $('#kabkota_filter').val() != '') ? $('#kabkota_filter').val() : ($('#provinsi_filter').val() != null && $('#provinsi_filter').val() != '') ? $('#provinsi_filter').val() : null,
                    tipe_pasar_id: ($('#tipe_pasar_filter').val() != null && $('#tipe_pasar_filter').val() != '') ? $('#tipe_pasar_filter').val() : null,
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'pasar_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama Pasar',
                    data: 'nama',
                    render: function(k, v, r) {
                        var tipe = '-'
                        var kondisi = '-'
                        if (r.tipe_pasar != null) {
                            tipe = r.tipe_pasar
                        }

                        if (r.kondisi != null) {
                            kondisi = r.kondisi
                        }

                        return '<span class="fw-bold text-primary">' + r.nama + '</span>' +
                            '<br>Tipe Pasar : ' + tipe +
                            '<br>Kondisi : ' + kondisi
                    }
                },
                {
                    title: 'Alamat',
                    data: 'nama',
                    render: function(k, v, r) {
                        return '<b>Alamat : </b>' + r.alamat +
                            '<br><b>Kecamatan : </b>' + (r.kecamatan == null ? '-' : r.kecamatan) +
                            '<br><b>Kab/Kota : </b>' + r.kab_kota +
                            '<br><b>Provinsi : </b>' + r.provinsi
                    }
                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.verify_kelengkapan_status == 'Terverifikasi') {
                            return '<div class="badge bg-success"><i class="ti ti-circle-check"></i>' + r.verify_kelengkapan_status + '</div>'
                        } else {
                            return '<div class="badge bg-warning"><i class="ti ti-alert-triangle me-1"></i>' + r.verify_kelengkapan_status + '</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'pasar_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var btn = ''
                        <?php if (has_access(2, 'update')) { ?>
                            if (r.verify_kelengkapan_status != 'Terverifikasi') {
                                btn += '<button class="btn btn-success btn-sm  m-1" onclick="edit(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"><i class="ti ti-circle-check"></i></button>'
                            }
                        <?php } ?>

                        <?php if (has_access(2, 'delete')) { ?>
                            if (r.createdby == '<?= $this->session->userdata('email') ?>') {
                                btn += '<button class="btn btn-danger btn-sm  m-1" onclick="update(\'' + r.pasar_id + '\',\'Deleted\',\'tbl_pasar\', \'pasar_id\', \'Hapus data pasar dengan id ' + r.pasar_id + ' oleh <?= $this->session->userdata('email') ?>\', \'\',  \'Hapus data pasar dengan id ' + r.pasar_id + ' oleh <?= $this->session->userdata('email') ?>\' )"> <i class="ti ti-trash"></i></button>'
                            } else {
                                btn += '<button class="btn btn-danger btn-sm  m-1"> <i class="ti ti-trash"></i></button>'
                            }
                        <?php } ?>
                        return '<a href="<?= base_url() ?>web/pasar/detail/' + r.token + '" class="btn btn-dark btn-sm  m-1"> <i class="ti ti-eye"></i> </a>' +
                            btn

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function filter() {
        showTable('refresh')
    }

    $('#penerapan_digital_pasar').on('change', function() {

        if ($('#penerapan_digital_pasar').val() == 'Ya') {
            $('#mestode_penerapan_digital_pasar').parent().parent().show()
            $('#metode_penerapan_digital_pasar').prop('required', true);
        } else {
            $('#metode_penerapan_digital_pasar').parent().parent().hide()
            $('#metode_penerapan_digital_pasar').val('')
            $('#metode_penerapan_digital_pasar').prop('required', false);
        }

        resizeJquerySteps()
    })

    $('#pembiayaan_lembaga_keuangan').on('change', function() {

        if ($('#pembiayaan_lembaga_keuangan').val() == 'Ya') {
            $('#lembaga_keuangan').parent().parent().show()
            $('#lembaga_keuangan').prop('required', true);
        } else {
            $('#lembaga_keuangan').parent().parent().hide()
            $('#lembaga_keuangan').val('')
            $('#lembaga_keuangan').prop('required', false);
        }

        resizeJquerySteps()

    })

    $('#sertifikasi_sni').on('change', function() {

        if ($('#sertifikasi_sni').val() == 'Ya') {
            $('#tahun_sni_pasar_rakyat').parent().parent().show()
            $('#tahun_sni_pasar_rakyat').prop('required', true);
        } else {
            $('#tahun_sni_pasar_rakyat').parent().parent().hide()
            $('#tahun_sni_pasar_rakyat').val('')
            $('#tahun_sni_pasar_rakyat').prop('required', false);
        }

        resizeJquerySteps()

    })

    $('#jam_buka, #jam_tutup, #jam_mulai_sibuk, #jam_berakhir_sibuk').datetimepicker({
        format: 'HH:mm A',
        icons: {
            up: 'fas fa-chevron-up',
            down: 'fas fa-chevron-down',
            previous: 'fas fa-chevron-left',
            next: 'fas fa-chevron-right'
        }
    })

    $('#tp').change(function() {
        if ($('#tp').prop('checked')) {
            $('#content_tp').show()
            $('#tahun_tp, #anggaran_tp, #omzet_tp').prop('required', true);
        } else {
            $('#content_tp').hide()
            $('#tahun_tp, #anggaran_tp, #omzet_tp').val('')
            $('#tahun_tp, #anggaran_tp, #omzet_tp').prop('required', false);
        }
        resizeJquerySteps()
    });

    $('#dak').change(function() {
        if ($('#dak').prop('checked')) {
            $('#content_dak').show()
            $('#tahun_dak, #anggaran_dak, #omzet_dak').prop('required', true);
        } else {
            $('#content_dak').hide()
            $('#tahun_dak, #anggaran_dak, #omzet_dak').val('')
            $('#tahun_dak, #anggaran_dak, #omzet_dak').prop('required', false);
        }
        resizeJquerySteps()
    });

    $('#tp_dak').change(function() {
        if ($('#tp_dak').prop('checked')) {
            $('#content_tp_dak').show()
            $('#tahun_tp_dak, #anggaran_tp_dak, #omzet_tp_dak').prop('required', true);
        } else {
            $('#content_tp_dak').hide()
            $('#tahun_tp_dak, #anggaran_tp_dak, #omzet_tp_dak').val('')
            $('#tahun_tp_dak, #anggaran_tp_dak, #omzet_tp_dak').prop('required', false);
        }
        resizeJquerySteps()
    });

    $('#no_telp, #fax').mask('0000000000000', {
        'translation': {
            0: {
                pattern: /[0-9]/
            }
        }
    });
    $('#kodepos').mask('00000', {
        'translation': {
            0: {
                pattern: /[0-9]/
            }
        }
    });

    $('#tahun_renovasi, #tahun_dibangun').mask('0000', {
        'translation': {
            0: {
                pattern: /[0-9]/
            }
        }
    });

    $('#jumlah_pengunjung, #jumlah_lantai, #jumlah_kios, #jumlah_los, #jumlah_dasaran, #jumlah_padagang, #jumlah_pedagang_kios, #jumlah_pedagang_los, #jumlah_pedagang_dasaran, #pekerja_tetap, #pekerja_nontetap, #luas_tanah, #luas_bangunan, #omzet, #anggaran_tp, #omzet_tp, #anggaran_dak, #omzet_dak').mask('000.000.000.000.000.000.000.000', {
        reverse: true,
        'translation': {
            0: {
                pattern: /[0-9]/
            }
        }
    });

    var provpinsi = null;
    var kab_kota = null;
    var kecamatan = null;
    var kelurahan = null;

    function add() {
        $('#modal_general_simple').modal('show')
        $('#title_modal').text('Tambah Data Pasar')
        $('#modal_general_simple').on('shown.bs.modal', function() {
            resizeJquerySteps()
            maps2()
            $('#latitude').val('')
            $('#longtitude').val('')
            $('#pasar_id').val('')
            $('#nama_pasar').val('')
            $('#deskripsi').val('')
            $('#a_provinsi').val('')
            $('#a_kab_kota').selectize()[0].selectize.setValue('')
            $('#a_kecamatan').selectize()[0].selectize.setValue('')
            $('#a_kelurahan').selectize()[0].selectize.setValue('')
            $('#alamat').val('')
            $('#kodepos').val('')
            $("#file-preview").attr("src", '<?= base_url() ?>assets/frontend/img/default-market.svg');
            $("#file-preview-2").attr("src", '<?= base_url() ?>assets/frontend/img/default-market.svg');

            <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
                getDaerah('a_provinsi', 0, '<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                provinsi = '<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>'
                kab_kota = '<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>'
            <?php } else { ?>
                getDaerah('a_provinsi', 0)
                provinsi = null;
                kab_kota = null;
            <?php } ?>

        })

    }

    function edit(action_index, start, length) {
        $('#modal_general').modal('show')
        $('#title_modal').text('Edit Data Pasar')
        $.ajax({
            url: '<?= base_url() ?>pasar/getPasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#modal_general').on('shown.bs.modal', function() {
                    $('#struktur_pengelola').prop('required', false);
                    $('#struktur_pengelola').siblings('label').find('b').eq(0).remove()
                    maps(response.latitude, response.longitude)
                    $('#latitude').val(response.latitude)
                    $('#longtitude').val(response.longitude)
                    $('#pasar_id').val(response.pasar_id)
                    $('#nama_pasar').val(response.nama)
                    $('#deskripsi').val(response.deskripsi)
                    getDaerah('provinsi', 0, response.daerah_id.substring(0, 2))
                    kab_kota = response.daerah_id.substring(0, 4)
                    kecamatan = response.daerah_id.substring(0, 6)
                    kelurahan = response.daerah_id.substring(0, 10)
                    // getDaerah('kab_kota', response.daerah_id.substring(0, 2), response.daerah_id.substring(0, 4))
                    // getDaerah('kecamatan', response.daerah_id.substring(0, 4), response.daerah_id.substring(0, 6))
                    // getDaerah('kelurahan', response.daerah_id.substring(0, 6), response.daerah_id.substring(0, 10))
                    $('#alamat').val(response.alamat)
                    $('#kodepos').val(response.kode_pos)
                    getDetail_pasar(response.pasar_id)
                    getBangunan(response.pasar_id)
                    getPengelola(response.pasar_id)
                    getAnggaran(response.pasar_id)
                    getAnggaran('tp', response.pasar_id, 1)
                    getAnggaran('dak', response.pasar_id, 2)

                    $("#file-preview").attr("src", response.foto_depan);
                    $("#file-preview-2").attr("src", response.foto_dalam);
                })
            },
        })
    }

    $("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        saveState: false,
        stepsOrientation: "vertical",

        onInit: function(event, currentIndex, priorIndex) {

        },

        onStepChanging: function(event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,#wizard-p-1 :hidden,#wizard-p-2 :hidden,#wizard-p-3 :hidden,#wizard-p-4 :hidden,#wizard-p-5 :hidden,#wizard-p-6 :hidden";
            if (newIndex > currentIndex) {

                if (form.valid() == true) {
                    return true;
                } else {
                    return false;
                }
            }
            return true

        },

        onStepChanged: function(event, currentIndex, priorIndex) {

        },
        onFinishing: function(event, currentIndex) {
            $('#pasar_form').submit()
            // form.validate().settings.ignore = ":disabled";
            // return form.valid();
        },

        onFinished: function(event, currentIndex) {

        }
    });

    $('#provinsi').on('change', function() {
        var daerah_id = $("#provinsi").val();
        getDaerah('kab_kota', daerah_id, kab_kota)

    })

    $('#a_provinsi').on('change', function() {
        var daerah_id = $("#a_provinsi").val();
        getDaerah('a_kab_kota', daerah_id, kab_kota)

    })

    $('#kab_kota').on('change', function() {
        var daerah_id = $("#kab_kota").val();
        getDaerah('kecamatan', daerah_id, kecamatan)

    })

    $('#a_kab_kota').on('change', function() {
        var daerah_id = $("#a_kab_kota").val();
        getDaerah('a_kecamatan', daerah_id, kecamatan)

    })

    $('#kecamatan').on('change', function() {
        var daerah_id = $("#kecamatan").val();
        getDaerah('kelurahan', daerah_id, kelurahan)

    })

    $('#a_kecamatan').on('change', function() {
        var daerah_id = $("#a_kecamatan").val();
        getDaerah('a_kelurahan', daerah_id, kelurahan)

    })

    function getDetail_pasar(id) {
        $.ajax({
            url: '<?= base_url() ?>pasar/getDetailJSON',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                id: id
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#pasar_detail_id').val(response.data.pasar_detail_id)
                getTipePasar('tipe_pasar', response.data.tipe_pasar_id)
                getklasifikasi(response.data.jenis_pasar)
                $('#no_telp').val(response.data.no_telp)
                $('#no_fax').val(response.data.no_fax)
                $('#jarak_toko_modern').val(response.data.jarak_toko_modern)
                $('#penerapan_digital_pasar').selectize()[0].selectize.setValue(response.data.penerapan_digitalisasi_pasar)

                if (response.data.penerapan_digitalisasi_pasar == 'Ya') {
                    $('#metode_penerapan_digital_pasar').parent().parent().show()
                    $('#metode_penerapan_digital_pasar').prop('required', true);
                    if (response.data.detail_digitalisasi_pasar == null) {
                        $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue('')
                    } else {
                        $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue(response.data.detail_digitalisasi_pasar.toString().split(','))
                    }

                } else {
                    $('#metode_penerapan_digital_pasar').parent().parent().hide()
                    $('#metode_penerapan_digital_pasar').selectize()[0].selectize.setValue('')
                    $('#metode_penerapan_digital_pasar').prop('required', false)
                }

                $('#sertifikasi_sni').selectize()[0].selectize.setValue(response.data.sertifikasi_sni)
                if (response.data.sertifikasi_sni == 'Ya') {
                    $('#tahun_sni_pasar_rakyat').parent().parent().show()
                    $('#tahun_sni_pasar_rakyat').prop('required', true);
                    $('#tahun_sni_pasar_rakyat').val(response.data.tahun_sertifikasi)
                } else {
                    $('#tahun_sni_pasar_rakyat').parent().parent().hide()
                    $('#tahun_sni_pasar_rakyat').val('')
                    $('#tahun_sni_pasar_rakyat').prop('required', false)
                }

                $('#pembiayaan_lembaga_keuangan').selectize()[0].selectize.setValue(response.data.pembiayaan_lk)
                if (response.data.pembiayaan_lk == 'Ya') {
                    $('#lembaga_keuangan').parent().parent().show()
                    $('#lembaga_keuangan').prop('required', true);
                    $('#lembaga_keuangan').val(response.data.detail_digitalisasi_pasar)
                } else {
                    $('#lembaga_keuangan').parent().parent().hide()
                    $('#lembaga_keuangan').val('')
                    $('#lembaga_keuangan').prop('required', false)
                }

                getSarana_prasana(response.data.sarana_prasarana)
                $('#dekat_pemukiman').selectize()[0].selectize.setValue(response.data.jarak_pemukiman)
                $('#angkutan_umum').val(response.data.angkutan_umum)
                $('#waktu_operasional').selectize()[0].selectize.setValue(response.data.waktu_operasional == null ? '' : response.data.waktu_operasional.toString().split(','))
                $('#jam_buka').val(response.data.jam_operasional_awal)
                $('#jam_tutup').val(response.data.jam_operasional_akhir)
                $('#jam_mulai_sibuk').val(response.data.jam_sibuk_awal)
                $('#jam_berakhir_sibuk').val(response.data.jam_sibuk_akhir)
                getKomoditi(response.data.komoditas_dijual)
                $('#omzet').val(response.data.omzet_sebelumnya)
                $('#jumlah_pengunjung').val(response.data.jumlah_pengunjung_harian)
                $('#bentuk_pasar').selectize()[0].selectize.setValue(response.data.bentuk_pasar)
                $('#kepemilikan').selectize()[0].selectize.setValue(response.data.kepemilikan)
                $('#kondisi_pasar').selectize()[0].selectize.setValue(response.data.kondisi)
                $('#tata_kelola').selectize()[0].selectize.setValue(response.data.tata_kelola)
                $('#luas_tanah').val(response.data.luas_tanah)
                $('#luas_bangunan').val(response.data.luas_bangunan)
                $('#tahun_dibangun').val(response.data.tahun_bangun)
                $('#tahun_renovasi').val(response.data.tahun_renovasi)
                $('#pekerja_tetap').val(response.data.jumlah_pekerja_tetap)
                $('#pekerja_nontetap').val(response.data.jumlah_pekerja_nontetap)
                $('#jumlah_lantai').val(response.data.jumlah_lantai)
            },
        })
    }

    function getAnggaran(id, pasar_id, program_pasar_id) {
        $('#' + id).attr('checked', false)
        $('#content_' + id).hide()
        $('#tahun_' + id).val('')
        $('#anggaran_' + id).val('')
        $('#omzet_' + id).val('')
        $('#tahun_' + id).prop('required', false);
        $('#anggaran_' + id).prop('required', false);
        $('#omzet_' + id).prop('required', false);
        $('#pasar_anggaran_' + id + '_id').val('')
        $.ajax({
            url: '<?= base_url() ?>pasar/getAnggaranJSON',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                id: pasar_id,
                refresh: 'refresh',
                program_pasar_id: program_pasar_id
            },
            success: function(response) {
                response = JSON.parse(response)

                if (response.kode == 200) {
                    $('#' + id).attr('checked', true)
                    $('#content_' + id).show()
                    $('#tahun_' + id).prop('required', true);
                    $('#anggaran_' + id).prop('required', true);
                    $('#omzet_' + id).prop('required', true);

                    $('#tahun_' + id).val(response.data[0].tahun)
                    $('#pasar_anggaran_' + id + '_id').val(response.data[0].pasar_anggaran_id)
                    $('#tahun_' + id).val(response.data[0].tahun)
                    $('#anggaran_' + id).val(response.data[0].anggaran)
                    $('#omzet_' + id).val(response.data[0].omset)
                }
            },
            complete: function() {
                resizeJquerySteps()
            }
        })
    }

    function getBangunan(id) {
        $('#pasar_bangunan_kios_id').val('')
        $('#jumlah_pedagang_kios').val('')
        $('#jumlah_kios').val('')
        $('#pasar_bangunan_los_id').val('')
        $('#jumlah_pedagang_los').val('')
        $('#jumlah_los').val('')
        $('#pasar_bangunan_dasaran_id').val('')
        $('#jumlah_pedagang_dasaran').val('')
        $('#jumlah_dasaran').val('')

        $.ajax({
            url: '<?= base_url() ?>pasar/getBangunan',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                id: id
            },
            success: function(response) {
                response = JSON.parse(response)
                $.each(response.data, function(k, v) {

                    if (v.jenis == 'Kios') {
                        $('#pasar_bangunan_kios_id').val(v.pasar_bangunan_id)
                        $('#jumlah_pedagang_kios').val(v.jumlah_pedagang)
                        $('#jumlah_kios').val(v.jumlah)
                    }

                    if (v.jenis == 'Los') {
                        $('#pasar_bangunan_los_id').val(v.pasar_bangunan_id)
                        $('#jumlah_pedagang_los').val(v.jumlah_pedagang)
                        $('#jumlah_los').val(v.jumlah)
                    }

                    if (v.jenis == 'Dasaran') {
                        $('#pasar_bangunan_dasaran_id').val(v.pasar_bangunan_id)
                        $('#jumlah_pedagang_dasaran').val(v.jumlah_pedagang)
                        $('#jumlah_dasaran').val(v.jumlah)
                    }
                })
            }
        })
    }

    function getPengelola(id) {
        $('#pasar_pengelola_id').val('')
        $('#pengelola').selectize()[0].selectize.setValue('')
        $('#nama_pengelola').val('')
        $('#jabatan_pengelola').val('')
        $('#no_telp_pengelola').val('')
        $('#email_pengelola').val('')
        $('#tingkat_pendidikan').selectize()[0].selectize.setValue('')
        $('#jumlah_staf').val('')
        $('#nama_koperasi').val('')
        $('#kategori_koperasi').val('')
        $('#no_telp_koperasi').val('')
        $('#jumlah_anggota_koperasi').val('')
        $('#no_badan_hukum_koperasi').val('')
        $('#no_induk_koperasi').val('')
        $('#besaran_retribusi_kios').val('')
        $('#besaran_retribusi_los').val('')
        $('#besaran_retribusi_dasaran').val('')
        $('#pendapatan_tahunan_retribusi').val('')

        $.ajax({
            url: '<?= base_url() ?>pasar/getPengelola_pasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                pasar_id: id
            },
            success: function(response) {
                response = JSON.parse(response)
                if (response.kode == 200) {
                    $('#pasar_pengelola_id').val(response.data[0].pasar_pengelola_id)
                    $('#pengelola').selectize()[0].selectize.setValue(response.data[0].klasifikasi_pengelola)
                    $('#nama_pengelola').val(response.data[0].nama_pengelola)
                    $('#jabatan_pengelola').val(response.data[0].jabatan_pengelola)
                    $('#no_telp_pengelola').val(response.data[0].no_telp_pengelola)
                    $('#email_pengelola').val(response.data[0].email_pengelola)
                    $('#tingkat_pendidikan').selectize()[0].selectize.setValue(response.data[0].tingkat_pendidikan)
                    $('#jumlah_staf').val(response.data[0].jumlah_staf)
                    $('#nama_koperasi').val(response.data[0].nama_koperasi)
                    $('#kategori_koperasi').val(response.data[0].kategori_koperasi)
                    $('#no_telp_koperasi').val(response.data[0].no_telp_koperasi)
                    $('#jumlah_anggota_koperasi').val(response.data[0].jumlah_anggota_koperasi)
                    $('#no_badan_hukum_koperasi').val(response.data[0].no_badan_hukum_koperasi)
                    $('#no_induk_koperasi').val(response.data[0].no_induk_koperasi)
                    $('#besaran_retribusi_kios').val(response.data[0].besaran_retribusi_kios)
                    $('#besaran_retribusi_los').val(response.data[0].besaran_retribusi_los)
                    $('#besaran_retribusi_dasaran').val(response.data[0].besaran_retribusi_dasaran)
                    $('#pendapatan_tahunan_retribusi').val(response.data[0].pendapatan_tahunan_retribusi)

                    if (response.data[0].struktur_pengelola != null) {
                        $('#file_struktur').html('<p class="mt-2 mb-0 text-danger">File saat ini: <a href="' + response.data[0].file_struktur + '" target="_blank">' + response.data[0].struktur_pengelola + '</a></p>')
                    } else {
                        $('#file_struktur').html('')
                    }
                }

            }
        })
    }

    <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
        kabkota = '<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>'
        getDaerah('kab_kota', '<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>', kabkota)
    <?php } ?>

    $('#provinsi_filter').on('change', function() {
        var daerah_id = $("#provinsi_filter").val();
        getDaerah('kabkota_filter', daerah_id, kabkota)

    })

    // $('#provinsi').on('change', function() {
    //     var daerah_id = $("#provinsi").val();
    //     getDaerah('kab_kota', daerah_id, vall != null ? vall.substring(0, 4) : vall)

    // })

    // $('#kab_kota').on('change', function() {
    //     var daerah_id = $("#kab_kota").val();
    //     getDaerah('kecamatan', daerah_id, vall != null ? vall.substring(0, 6) : vall)

    // })

    // $('#kecamatan').on('change', function() {
    //     var daerah_id = $("#kecamatan").val();
    //     getDaerah('kelurahan', daerah_id, vall != null ? vall.substring(0, 10) : vall)

    // })

    function changeDaerah(vall = null) {

    }

    function getDaerah(id, daerah_id, vall = null) {
        var daerah = '';

        if (id == 'provinsi_filter' || id == 'provinsi' || id == 'a_provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else if (id == 'kabkota_filter' || id == 'kab_kota' || id == 'a_kab_kota') {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        } else if (id == 'kecamatan' || id == 'a_kecamatan') {
            daerah = '<option value="">- Pilih Kecamatan -</option>';
        } else {
            daerah = '<option value="">- Pilih Kelurahan -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                if (id != 'a_provinsi') {
                    $('#' + id).selectize()[0].selectize.destroy();
                }
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi_filter' || id == 'provinsi' || id == 'a_provinsi') {
                        dt = v.provinsi;
                    } else if (id == 'kabkota_filter' || id == 'kab_kota' || id == 'a_kab_kota') {
                        dt = v.kab_kota;
                    } else if (id == 'kecamatan' || id == 'a_kecamatan') {
                        dt = v.kecamatan;
                    } else {
                        dt = v.kelurahan;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';

                })
            },
            complete: function() {
                $('#' + id).html(daerah);

                if (id != 'a_provinsi') {
                    $('#' + id).selectize()

                    if (vall != null) {
                        $('#' + id).selectize()[0].selectize.setValue(vall)
                        <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
                            if (id == 'provinsi_filter' || id == 'provinsi' || id == 'a_provinsi' || id == 'kabkota_filter' || id == 'kab_kota'|| id == 'a_kab_kota') {
                                $('#' + id).selectize()[0].selectize.disable();
                            }
                        <?php } ?>
                    } else {
                        $('#' + id).selectize()[0].selectize.setValue('')
                    }
                } else {
                    <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [2, 9, 19])) { ?>
                        if (id == 'a_provinsi') {
                            $('#' + id).selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                            $('#' + id).selectize()[0].selectize.disable();
                        }
                    <?php }else{ ?>
                        if (vall != null) {
                            $('#' + id).val(vall)
                        } else {
                            $('#' + id).val('')
                        }
                    <?php } ?>
                }
            }
        })
    }

    function getTipePasar(id, vall = null) {
        var tipe_pasar = '<option value="">- Pilih Tipe Pasar -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/getTipe_pasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id).selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response, function(k, v) {
                    tipe_pasar += '<option value="' + v.tipe_pasar_id + '">' + v.tipe_pasar + '</option>';
                })
            },
            complete: function() {
                $('#' + id).html(tipe_pasar);
                $('#' + id).selectize()

                if (vall != null) {
                    $('#' + id).selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    function getklasifikasi(vall = null) {
        var klasifikasi = '<option value="">- Pilih Klasifikasi Pasar -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/getKlasifikasi',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#klasifikasi').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    klasifikasi += '<option value="' + v.klasifikasi + '">' + v.klasifikasi + '</option>';
                })
            },
            complete: function() {
                $('#klasifikasi').html(klasifikasi);
                $('#klasifikasi').selectize()

                if (vall != null) {
                    $('#klasifikasi').selectize()[0].selectize.setValue(vall)
                }
            }
        })
    }

    function getSarana_prasana(vall = null) {
        var sarana_prasarana = '<option value="">- Pilih Sarana dan Prasarana -</option>';
        $.ajax({
            url: '<?= base_url() ?>master/getSarana_prasarana',
            type: 'POST',
            data: {
                status: 'Aktif',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#sarana_prasarana').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    sarana_prasarana += '<option value="' + v.sarana_prasarana + '">' + v.sarana_prasarana + '</option>';
                })
            },
            complete: function() {
                $('#sarana_prasarana').html(sarana_prasarana);
                $('#sarana_prasarana').selectize()

                if (vall != null) {
                    $('#sarana_prasarana').selectize()[0].selectize.setValue(vall.toString().split(','))
                }
            }
        })
    }

    function getKomoditi(vall = null) {
        var komoditas = '<option value="">- Komoditas yang Dijual -</option>';

        $.ajax({
            url: '<?= base_url() ?>master/jenis_komoditi_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#komoditas').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)

                $.each(response.data, function(k, v) {
                    komoditas += '<option value="' + v.jenis_komoditi + '">' + v.jenis_komoditi + '</option>';
                })
            },
            complete: function() {
                $('#komoditas').html(komoditas);
                $('#komoditas').selectize({
                    create: true,
                    delimiter: ',',
                })

                if (vall != null) {
                    $('#komoditas').selectize()[0].selectize.setValue(vall.toString().split(','))
                }
            }
        })
    }

    // function searchLocation() {
    //     var searchQuery = document.getElementById('cari_lokasi').value;

    //     // Create a Geocoder object
    //     var geocoder = new google.maps.Geocoder();

    //     // Geocode the search query
    //     geocoder.geocode({
    //         address: searchQuery
    //     }, function(results, status) {
    //         if (status === 'OK') {
    //             // Get the first result
    //             var location = results[0].geometry.location;
    //         } else {
    //             alert('Geocode was not successful for the following reason: ' + status);
    //         }
    //     });
    // }

    function searchLocation(title = null) {

        //Hide modal
        const siuu = document.getElementById('modal_general');
        siuu.classList.add('d-none');

        var lokasi = '';
        var jml_lokasi = '';
        $.ajax({
            url: '<?= base_url() ?>pasar/cari_lokasi',
            type: 'POST',
            data: {
                keyword: $('#keyword').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                if ($.fn.DataTable.isDataTable('#tbl_lokasi')) {
                    $("#tbl_lokasi").dataTable().fnDestroy();
                    $('#tbl_lokasi').empty();
                }
            },
            success: function(response) {
                response = JSON.parse(response);
                console.log(response);
                jml_lokasi = response.predictions.length;

                $.each(response.predictions, function(k, v) {
                    let description = v.description;
                    const lowercaseDesc = description.toLowerCase();
                    if (lowercaseDesc.includes('market') || (lowercaseDesc.includes('pasar') && !lowercaseDesc.includes('jalan pasar'))) {
                        description = `<span class="fw-bold text-danger">${description}<span class="badge bg-success ms-1"><i class="ti ti-sparkles me-1"></i>Rekomendasi</span></span>`;
                    }

                    lokasi += `<tr>
                        <td>${description}</td>
                        <td class="text-center">
                            <button type="button" onclick="getLokasi('${v.place_id}', '${title}')" class="btn btn-dark waves-effect waves-light">
                                <i class="ti ti-map-pin me-1"></i>Pilih
                            </button>
                        </td>
                    </tr>`;
                });

            },

            complete: function() {
                $('#modal_lokasi').modal('show')
                $('#modal_title').html('Terdapat total ' + jml_lokasi + ' lokasi ditemukan')
                $('#lokasi').html(lokasi)
            }
        })
    }

    //restore modal siuu
    $('#modal_lokasi').on('hidden.bs.modal', function() {
        const siuu = document.getElementById('modal_general');
        siuu.classList.remove('d-none');
    });

    function getLokasi(place_id, title) {
        //restore modal siuu
        const siuu = document.getElementById('modal_general');
        siuu.classList.remove('d-none');
        $('#modal_lokasi').modal('hide')

        var lat = ''
        var long = ''
        $.ajax({
            url: '<?= base_url() ?>pasar/lokasi_latlong',
            type: 'POST',
            data: {
                place_id: place_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {

            },
            success: function(response) {
                response = JSON.parse(response)
                lat = response.results[0].geometry.location.lat
                long = response.results[0].geometry.location.lng
            },
            complete: function() {

                if (title == 'simple') {
                    $('#latitude_pasar').val(lat)
                    $('#longtitude_pasar').val(long)
                    maps2(lat, long)
                } else {
                    $('#latitude').val(lat)
                    $('#longtitude').val(long)
                    maps(lat, long)
                }

            }
        })
    }

    function maps(lat = null, long = null) {
        document.getElementById('weathermap_simple').innerHTML = "";
        document.getElementById('weathermap').innerHTML = "<div id='map'></div>";
        var map = L.map('map').setView([-2.548926, 118.0148634], lat != null ? 8 : 4);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(map);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map);

        if (lat != null & long != null) {
            var markers = L.markerClusterGroup();
            var lokasi = L.marker([lat, long], {
                icon: icon
            })

            map.scrollWheelZoom.disable();

            markers.addLayer(lokasi);
            map.addLayer(markers);
            map.fitBounds(markers.getBounds());
        }
    }

    function maps2(lat = null, long = null) {
        document.getElementById('weathermap_simple').innerHTML = "<div id='map'></div>";
        var map2 = L.map('map').setView([-2.548926, 118.0148634], lat != null ? 8 : 4);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(map2);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map2),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map2);

        if (lat != null & long != null) {
            var markers = L.markerClusterGroup();
            var lokasi = L.marker([lat, long], {
                icon: icon
            })

            map2.scrollWheelZoom.disable();

            markers.addLayer(lokasi);
            map2.addLayer(markers);
            map2.fitBounds(markers.getBounds());
        }
    }
</script>