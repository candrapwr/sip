
<script>
    $(document).ready(function() {
        showTable('refresh');

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {
            var timerInterval;
            Swal.fire({
                title: "Berhasil!",
                icon: "success",
                html: '<?= $this->session->flashdata('message') ?> <br><b></b> milliseconds.',
                timer: 1000,
                timerProgressBar: true,
                didOpen: () => {
                    Swal.showLoading()
                    const b = Swal.getHtmlContainer().querySelector('b')
                    timerInterval = setInterval(() => {
                        b.textContent = Swal.getTimerLeft()
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            })

        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

        $('#acceptx').selectize()
    })

    var jenis = {
        'revitalisasi_proposal_foto_pasar_1':'Revitalisasi Proposal Foto Pasar Tahap 1',
        'revitalisasi_proposal_dokumen_1': 'Revitalisasi Proposal Dokumen Tahap 1',
        'revitalisasi_proposal_dokumen_2': 'Revitalisasi Proposal Dokumen Tahap 2',
        'revitalisasi_proposal_pengawasan_1': 'Revitalisasi Proposal Pengawasan Tahap 1',
        'revitalisasi_proposal_pengawasan_2': 'Revitalisasi Proposal Pengawasan Tahap 2'
    }

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
            responsive: true,
            ordering: false,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>web/master/dokumen/json',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'dokumen_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Judul',
                    data: 'judul',
                    render: function(k, v, r) {
                        return r.judul + ' <b>( '+r.accept+' - <strong class="text-danger">*'+r.max_size+'mb</strong> )</b><br><i class="text-muted">' + r.deskripsi + '</i>'
                    }
                },
                {
                    title: 'Jenis',
                    data: 'jenis',
                    visible: false
                },
                {
                    title: 'Mandatori',
                    data: 'required',
                },
                {
                    title: 'Status',
                    data: 'status',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.status == 'Aktif') {
                            return '<div class="badge bg-success">Aktif</div>'
                        } else {
                            return '<div class="badge bg-danger">Tidak Aktif</div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'dokumen_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'
                        var status = 'Tidak Aktif'
                        if (r.status == 'Tidak Aktif') {
                            button_color = 'btn-success'
                            status = 'Aktif'
                        }


                        var btn_delete = ''
                        var btn_edit = ''
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'delete') : false) ?>' == 1) {
                            btn_delete = '<button class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.dokumen_id + '\',\'Deleted\',\'tbl_dokumen\',\'dokumen_id\',\'Hapus dokumen `' + ($.trim(r.judul.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'
                        }
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1) {

                            btn_edit = '<button class="btn btn-sm btn-warning m-1" onclick="edit(\'' + r.dokumen_id + '\')"><i class="ti ti-pencil"></i></button>'
                            btn_edit += '<button class="btn btn-sm ' + button_color + ' m-1" onclick="update(\'' + r.dokumen_id + '\',\'' + status + '\',\'tbl_dokumen\',\'dokumen_id\',\'Hapus dokumen `' + ($.trim(r.judul.replace(/[\t\n]+/g, ''))) + '`\')"><i class="ti ti-flag"></i></button>'
                        }

                        return btn_edit + btn_delete

                    }
                },


            ],
            scrollX: true,
            displayLength: 10,
            rowGroup: {
                dataSrc: function(row) {
                    return jenis[row.jenis];
                }
            }
        });
    }

    function after_update() {
        showTable('refresh')
    }

    function add(action_index) {
        $("#modal").modal('show')
        $('#modal-title').text('Tambah Master Dokumen')
        $('#dokumen_id').val('')
        $('#judul').val('')
        $('#jenis').val('')
        $('#deskripsi').val('')
        $('#required').val('Ya')
        $('#max').val('')
        $('#acceptx').selectize()[0].selectize.setValue('')
    }

    function edit(dokumen_id) {
        $.ajax({
            url: '<?= base_url() ?>web/master/dokumen/json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                dokumen_id: dokumen_id,
                refresh: 'true'
            },
            success: function(response) {
                response = JSON.parse(response)
                data = response.data[0]
                $('#dokumen_id').val(data.dokumen_id)
                $('#judul').val(data.judul)
                $('#jenis').val(data.jenis)
                $('#deskripsi').val(data.deskripsi)
                $('#required').val(data.required)
                 $('#max').val(data.max_size)
                $('#acceptx').selectize()[0].selectize.setValue((data.accept !=null)?data.accept.toString().split(','):'')
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }
</script>