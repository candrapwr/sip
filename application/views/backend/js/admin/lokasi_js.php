<script src="https://unpkg.com/leaflet@1.0.3/dist/leaflet.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.min.js"></script>
<script>
    $(document).ready(function() {
        maps()
        showTable('refresh')
        $('#cek_lokasi').trigger('click');

        if ('<?= $this->session->flashdata('alert') ?>' == 'success') {

            let timerInterval
            Swal.fire({
                title: 'Berhasil!',
                icon: 'success',
                html: '<?= $this->session->flashdata('message') ?>',
                timer: 2000,
                timerProgressBar: true,
                showCancelButton: false,
                showConfirmButton: false,
                didOpen: () => {
                    Swal.showLoading()
                    timerInterval = setInterval(() => {
                        const content = Swal.getHtmlContainer()
                        if (content) {
                            const b = content.querySelector('b')
                            if (b) {
                                b.textContent = Swal.getTimerLeft()
                            }
                        }
                    }, 100)
                },
                willClose: () => {
                    clearInterval(timerInterval)
                }
            }).then((result) => {
                /* Read more about handling dismissals below */
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        } else if ('<?= $this->session->flashdata('alert') ?>' == 'error') {
            Swal.fire(
                'Gagal!',
                '<?= $this->session->flashdata('message') ?>',
                'error'
            )
        }

    })

    function maps() {
        var locations = [
            <?php
            if ($maps_pasar != null) : foreach ($maps_pasar as $key => $map) : if ($map['latitude'] != null && $map['longitude'] != NULL) : ?>["<?= $map['nama'] ?>", <?= $map['latitude'] ?>, <?= $map['longitude'] ?>, "<?= str_replace(array("\r", "\n"), '', str_replace('"', '', ucwords(strtolower($map['alamat'])))) ?>", "<?= $map['kecamatan'] ?>", "<?= $map['kab_kota'] ?>", "<?= $map['provinsi'] ?>", "<?= $map['pasar_id'] ?>"],
            <?php endif;
                endforeach;
            endif;

            ?>
        ];

        var map = L.map('map').setView([-2.548926, 118.0148634], 8);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(map);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 5,
            }).addTo(map),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 5,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(map);

        var markers = L.markerClusterGroup();

        for (var i = 0; i < locations.length; i++) {
            var url = locations[i][0].toLowerCase()
            url = url.split(' ').join('-')

            var popup = '<h6>' + locations[i][0] + '</h6>' +
                '<b>Lokasi :</b> ' + locations[i][3] + ', ' + locations[i][4] + ', ' + locations[i][5]

            var lokasi = L.marker([locations[i][1], locations[i][2]], {
                icon: icon
            }).bindPopup(popup);

            map.scrollWheelZoom.disable();

            markers.addLayer(lokasi);
            map.addLayer(markers);
            map.fitBounds(markers.getBounds());
        }
    }

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({

            processing: true,
            serverSide: true,
            ordering: false,
            searchable: true,
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>pasar/getPasar',
                data: {
                    refresh: refresh,
                    verify_coordinate: 'Belum Terverifikasi',
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'No',
                    data: 'pasar_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama Pasar',
                    data: 'nama',
                    render: function(k, v, r) {

                        return '<b>' + r.nama + '</b>' +
                            '<br><b>Kab/Kota : </b>' + r.kab_kota +
                            '<br><b>Provinsi : </b>' + r.provinsi
                    }
                },
                {
                    title: 'Lokasi',
                    data: 'nama',
                    render: function(k, v, r) {
                        return '<b>Latitude : </b>' + r.latitude +
                            '<br><b>Longitude : </b>' + r.longitude
                    }
                },
                {
                    title: 'Status',
                    data: 'verify_coordinate',
                    render: function(k, v, r) {
                        var color = 'danger'
                        if(r.verify_coordinate == 'Terverifikasi'){
                            color = 'success'
                        }
                        return `<span class="badge bg-`+color+` rounded me-1>`+r.verify_coordinate+`</span>`
                    }
                },
                {
                    title: 'Aksi',
                    data: 'pasar_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var btn = ''
                        <?php if(has_access(75, 'update')){ ?>
                            btn ='<button onclick="edit(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-success m-1" title="Verifikasi Lokasi"><i class="ti ti-circle-check"></i></button>'
                        <?php } ?>
                        
                        return btn +
                            '<a href="https://www.google.com/maps?daddr=' + r.latitude +',' + r.longitude +'" target="_blank" class="btn btn-info m-1" title="Lihat Lokasi"><i class="ti ti-map-pin"></i></a>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function edit(action_index, start, length) {
        $('#modal_general').modal('show');

        $.ajax({
            url: '<?= base_url() ?>pasar/getPasar',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                refresh: 'refresh',
                start: start,
                length: length
            },
            success: function(response) {
                response = JSON.parse(response)

                $('#nama_pasar').text(response.nama)
                $('#alamat').text(response.alamat)
                $('#prov').text('Kec.' + response.kecamatan + ', ' + response.kab_kota + ', ' + response.provinsi + ' ' + (response.kode_pos == null ? '' : response.kode_pos))
                $('#pasar_id').val(response.pasar_id)
                $('#latitude').val(response.latitude)
                $('#longitude').val(response.longitude)
                $('#daerah_id').val(response.daerah_id)
                $('#nama').val(response.nama)
                $('#deskripsi').val(response.deskripsi)
                $('#alamat_pasar').val(response.alamat)
                $('#kode_pos').val(response.kode_pos)

                var lat = response.latitude == null ? '-6.180942149758281' : response.latitude
                var long = response.longitude == null ? '106.83291836726853' : response.longitude
                maps_pasar(lat, long)
            }
        })
    }

    function cek_maps() {
        var lat = $('#latitude').val()
        var long = $('#longitude').val()
        maps_pasar(lat, long)
    }

    function maps_pasar(lat, long) {
        document.getElementById('weathermap').innerHTML = "<div id='maps_pasar'></div>";
        var maps_pasar = L.map('maps_pasar').setView([lat, long], 17);
        mapLink =
            '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer(
            'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; ' + mapLink + ' PDSI Kementerian Perdagangan RI',
                scrollWheelZoom: false,
            }).addTo(maps_pasar);

        var icon = L.icon({
            iconUrl: '<?= base_url() ?>assets/brand/marker-sisp.svg',
            iconSize: [50, 50],
            iconAnchor: [22, 65],
            popupAnchor: [-3, -55]
        })

        basemap = {
            osm: L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 15,
            }).addTo(maps_pasar),
            roadmap: L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}', {
                minZoom: 15,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            }),
            satellite: L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                minZoom: 15,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3'],
            })
        }

        L.control.layers(basemap, null, {
            position: 'bottomleft'
        }).addTo(maps_pasar);

        var markers = L.markerClusterGroup();

        var lokasi = L.marker([lat, long], {
            icon: icon
        });

        maps_pasar.scrollWheelZoom.disable();

        markers.addLayer(lokasi);
        maps_pasar.addLayer(markers);
        maps_pasar.fitBounds(markers.getBounds());
    }
</script>