<script src="<?= base_url() ?>assets/backend/libs/xls/xlsx.core.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/jhxlsx.js"></script>

<script>
    $(function() {
        $('#post_provinsix').selectize({});
        $('#post_kabx').selectize({});

        var prov = '31'
        if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (provinsi)' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (kabupaten/kota)') {
            prov = '<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>'

        }

        $('#post_provinsi').selectize({
            items: [prov]
        });

        $('#post_kab').selectize();

        if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (provinsi)') {

            $('#post_provinsi').selectize()[0].selectize.disable();
            getCity('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>');
            showAjax('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>', '<?= $this->session->userdata('provinsi') ?>', 1);

        } else if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (kabupaten/kota)') {

            $('#post_provinsi').selectize()[0].selectize.disable();
            getCity('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>');
            showAjax('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>', '<?= $this->session->userdata('kab_kota') ?>', 2);

        } else {

            getCity($('#post_provinsi').val());
            showAjax($('#post_provinsi').val(), $('#post_provinsi').text(), 1);

        }

        $('#post_provinsix').change(function() {
            getCity($(this).val(), 'post_kabx');
        })

        $('#post_provinsi').change(function() {
            $('#btable').hide();
            $('#bloading').show();
            dtTable.clear();
            getCity($(this).val());
        })

        $('#post_kab').change(function() {
            $('#btable').hide();
            $('#bloading').show();
        })
    });

    function filter() {
        $('#btable').hide();
        $('#bloading').show();
        
        var daerah_val = ($('#post_kab').val() == '') ? $('#post_provinsi').val() : $('#post_kab').val();
        var daerah_text = ($('#post_kab').val() == '') ? $('#post_provinsi').text() : $('#post_kab').text();
        var pill = ($('#post_kab').val() != '') ? 2 : ($('#post_provinsi').val() != '' ? 2 : 0);

        showAjax(daerah_val, daerah_text, pill);
    }

    var belumku = '<span class="badge rounded-pill bg-danger"><i class="ti ti-alert-triangle me-1"></i>Belum Lengkap</span>';
    var sudahku = '<span class="badge rounded-pill bg-success"><i class="ti ti-circle-check me-1"></i>Lengkap</span>';
    var dtTable = $('#table').DataTable({

        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "pageLength": 20,
        "autoWidth": true,
        "processing": true,
        dom: 'Bftip',
        buttons: [{
                text: '<i class="ti ti-file-spreadsheet me-1"></i>Export to Excel',
                className: 'btn btn-primary',
                action: function(e, dt, button, config) {
                    get_export();
                }
            }],
        language: {
            "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
        },
        "data": [],
        "columns": [{
                "title": "Nama Pasar",
                "data": "nama",
                render: function(k, v, r) {
                    return '<span>' + r.nama + '</span>';
                }
            },
            {
                "title": "Detail",
                render: function(k, v, r) {
                    if (r.detail_pasar == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
            {
                "title": "Input Harga <br>Hari Ini",
                render: function(k, v, r) {
                    if (r.detail_input_harga_hari_ini == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
            {
                "title": "Bangunan",
                render: function(k, v, r) {
                    if (r.detail_bangunan == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
            {
                "title": "Anggaran",
                render: function(k, v, r) {
                    if (r.detail_anggaran == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
            {
                "title": "Anggaran <br>Tahun Berjalan",
                render: function(k, v, r) {
                    if (r.detail_anggaran_tahun_berjalan == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
            {
                "title": "Foto",
                render: function(k, v, r) {
                    if (r.detail_foto == 'Belum Lengkap')
                        return belumku;
                    else
                        return sudahku;
                }
            },
        ]
    });

    function getCity(provId, target = '') {
        var targetEl = ''

        if (target != '')
            targetEl = target
        else
            targetEl = 'post_kab'

        $.ajax({
            url: "<?= site_url() ?>kab/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#' + targetEl).selectize()[0].selectize.setValue('')
                $('#' + targetEl).selectize()[0].selectize.clearOptions()
                $('#' + targetEl)[0].selectize.lock()
            },
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (!response.success) {
                        $('#' + targetEl)[0].selectize.lock();
                    } else {
                        $('#' + targetEl)[0].selectize.unlock();

                        $('#' + targetEl).selectize()[0].selectize.addOption({
                            value: '',
                            text: 'Semua Kabupaten/Kota'
                        })
                        $.each(response.data, function(i, row) {
                            $('#' + targetEl).selectize()[0].selectize.addOption({
                                value: row.daerah_id,
                                text: row.kab_kota
                            })
                        })
                    }
                }
            },
            complete: function() {
                if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (kabupaten/kota)') {
                    $('#post_kab').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#post_kab').selectize()[0].selectize.disable();
                }
            }
        })
    }

    function showAjax(daerah, str_daerah, pil) {
        if (pil == 1){
            $('#dataTitle').html('<h5 style="font-size: 18px">INFORMASI KELENGKAPAN DATA PASAR </h5> <span>PROVINSI ' + str_daerah.toUpperCase() + '</span>');
        }else if(pil == 2){
            $('#dataTitle').html('<h5 style="font-size: 18px">INFORMASI KELENGKAPAN DATA PASAR </h5> <span> ' + str_daerah.toUpperCase() + '</span>');
        }else{
            $('#dataTitle').html('<h5 style="font-size: 18px">INFORMASI KELENGKAPAN DATA PASAR </h5> <span> SELURUH INDONESIA </span>');
        }
        var postData;
        postData = {
            post_daerah: daerah,
            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
        };
        dtTable.clear();
        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/monitoring-json/getpasar",
            type: 'post',
            data: postData,
            dataType: 'json',
            beforeSend: function() {
                showLoader();
                $('#loader').show()
                $('#content').hide()
            },
            complete: function() {
                hideLoader();
                $('#content').show()
                $('#loader').hide()
            },
            success: function(response, textStatus, xhr) {
                if (response.kode == 200 || response.kode == 404) {
                    $.each(response.data, function(index, value) {
                        dtTable.row.add(value);
                    });
                    dtTable.draw();
                } else {
                    location.reload();
                }
            },
            error: function(request, status, error) {
                location.reload();
            }
        })
    }

    function get_export() {
        var htmlTmp = ''
        $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>dashboard/perdagangan/monitoring-json/kelengkapanpasar_export',
            data: {
                post_daerah: ($('#post_kab').val() == '') ? $('#post_provinsi').val() : $('#post_kab').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#body_kelengkapan').LoadingOverlay('show')
            },
            complete: function() {
                $('#body_kelengkapan').LoadingOverlay('hide')
            },
            success: function(response) {
                if (response.code == 403) {
                    location.reload();
                } else {
                    //console.log(response)
                    var result = []
                    var paramJudul = "Rekap Data Kelengkapan Detail Pasar " + $('#post_kab').text() + " Provinsi " + $('#post_provinsi').text()

                    if($('#post_kab').val() == '' && $('#post_provinsi').val() == ''){
                        paramJudul = "Rekap Data Kelengkapan Detail Pasar Seluruh Inodnesia"
                    }
                    
                    var paramOpt = {
                        "sheetName": "Sheet1"
                    }
                    var label = ['Nama Pasar', 'Provinsi', 'Kabupaten/Kota', 'Detail Pasar', 'Input Harga Harian', 'Detail Bangunan', 'Detail Anggaran', 'Anggaran Tahun Berjalan', 'Detail Foto']
                    var kolom = ['nama', 'provinsi', 'kab_kota', 'detail_pasar', 'detail_input_harga_hari_ini', 'detail_bangunan', 'detail_anggaran', 'detail_anggaran_tahun_berjalan', 'detail_foto']
                    var tmp_field = {}
                    var tmp_data = []
                    var data = []


                    tmp_field = {}
                    tmp_field['merge'] = "0-" + (label.length - 1)
                    tmp_field['style'] = {
                        "font": {
                            "bold": true
                        }
                    }
                    tmp_field['text'] = paramJudul.toUpperCase()
                    tmp_data.push(tmp_field)
                    data.push(tmp_data)

                    tmp_data = []
                    for (var r = 0; r < label.length; r++) {
                        tmp_field = {}
                        tmp_field['style'] = {
                            "font": {
                                "bold": true
                            }
                        }
                        tmp_field['text'] = label[r].toUpperCase()
                        tmp_data.push(tmp_field)
                    }
                    data.push(tmp_data)

                    $.each(response.data, function(i, row) {
                        tmp_data = []
                        for (var r = 0; r < kolom.length; r++) {
                            tmp_field = {}
                            tmp_field['text'] = row[kolom[r]]
                            tmp_data.push(tmp_field)
                        }
                        data.push(tmp_data)
                    })
                    paramOpt['data'] = data
                    result.push(paramOpt)
                    var options = {
                        fileName: paramJudul,
                        header: true,
                    };
                    Jhxlsx.export(result, options);
                    //console.log(JSON.stringify(result));
                }
            }
        })
    }

    if (typeof PerfectScrollbar != 'function') {
        function PerfectScrollbar() {}
    }
</script>