<script>
    $('#platform-name, #produsen-name, #provinsi-name, #kabkota-name').selectize()

    $(document).ready(function() {
        showTable('refresh');
        getProdusen('refresh')
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">- Pilih Kabupaten/Kota -</option>')

        $('#provinsi-name').on('change', function() {
            var daerah_id = $("#provinsi-name").val();
            getDaerah('kabkota', daerah_id)
        })

        $('#kabkota-name').on('change', function() {
            var daerah_id = $("#kabkota-name").val();
        })

        $('#platform-name').on('change', function() {
            getProdusen('refresh')
        })

    })

    function fill() {
        showTable('refresh');
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }

    function getProdusen(refresh = null) {

        var produsen = '<option value="">- Pilih Produsen -</option>';

        $.ajax({
            url: '<?= base_url() ?>perdagangan/migor/getProdusen',
            type: 'POST',
            data: {
                platform: $("#platform-name").val(),
                length: 100,
                start: 0,
                refresh: refresh,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#produsen-name').selectize()[0].selectize.destroy();

                var dt = '';

                $.each(response.data, function(k, v) {
                    produsen += '<option value="' + v.nib_supplier + '">' + v.nama_produsen + '</option>';
                })

                $('#produsen-name').html(produsen);
                $('#produsen-name').selectize()
            },
        })
    }

    function showTable(refresh = null) {

        var platform = $('#platform-name').val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({

            responsive: true,
            processing: true,
            serverSide: true,
            ordering: true,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    platform: platform,
                    refresh: refresh,
                    nib: $('#produsen-name').val(),
                    start_date: $('#start-date').val(),
                    end_date: $('#end-date').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTansaksicurah',
            },
            columns: [{
                    title: 'No',
                    data: 'transaksi_konsumen_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Kode',
                    data: 'kode_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<span class="badge bg-secondary">#' + r.kode_transaksi + '</span>'
                    }
                },
                {
                    title: 'NIK',
                    data: 'nik',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<a href="javascript:void(0)" onclick="detail_nik(\'' + r.nik + '\', \'' + r.file_nik + '\')">' + r.nik + '</a>'
                    }
                },
                {
                    title: 'Produsen',
                    data: 'nama_produsen',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<div>
                            <p><strong>` + r.nama_produsen + `</strong></p>
                            <p><strong>NIB</strong>.` + r.nib_supplier + `</p>
                        </div>`
                    }
                },
                {
                    title: 'Pengecer',
                    data: 'nama_produsen',
                    orderable: false,
                    render: function(k, v, r) {

                        return `<dl class="row mb-0">
                                    <dt class="col-sm-4">Kode</dt>
                                    <dd class="col-sm-8">` + r.kode_pengecer + `</dd>
                                    <dt class="col-sm-4">Pengecer</dt>
                                    <dd class="col-sm-8">` + r.nama_pengecer + `</dd>
                                    <dt class="col-sm-4">Kontak</dt>
                                    <dd class="col-sm-8">` + r.no_telp + `</dd>
                                    <dt class="col-sm-4">Penanggung Jawab</dt>
                                    <dd class="col-sm-8">` + r.nama_penanggung_jawab + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Transaksi',
                    data: 'jumlah_transaksi',
                    orderable: false,
                    visible: true,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Jumlah</dt>
                                    <dd class="col-12">` + r.jumlah_transaksi + `</dd>
                                    <dt class="col-12">Tanggal</dt>
                                    <dd class="col-12">` + r.tanggal_transaksi + `</dd>
                                    <dt class="col-12">No. Nota</dt>
                                    <dd class="col-12">` + r.nomor_nota_penjualan + `</dd>
                                </dl>`
                    }
                },
                // {
                //     title: 'Aksi',
                //     data: 'transaksi_konsumen_id',
                //     orderable: false,
                //     render: function(k, v, r) {
                //         return '<button class="btn btn-warning btn-sm  m-1" title="Detail Data" onclick="view(\'' + r + '\')"> <i class="bx bx-show-alt"></i> </button>'
                //     }
                // },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function detail_nik(nik, file_nik) {
        $('#modal-ktp').modal('show');
        $('#nik').text('NIK.' + nik);
        $('#file_nik').html('<img src="' + file_nik + '" width="100%">');
    }

    function view(index) {
        $('#modal-detail').modal('show');
        $.ajax({
            url: '<?= base_url() ?>perdagangan/migor/getTansaksicurah',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                platform: $('#platform-name').val(),
                refresh: 'refresh',
                start_date: $('#start-date').val(),
                end_date: $('#end-date').val(),
                kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#modal-detail').modal('show')
                $('#modal-detail .modal-title').html('Detail Data')
                $('#modal-detail .modal-body').html(response.data)
            },
        })
    }
</script>