<script>
    $(document).ready(function() {
        showTable('refresh');
        $(' #provinsi-name, #kabkota-name,#kecamatan-name, #kelurahan-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">-- Pilih Kabupaten/Kota --</option>')

    })



    function fill() {
        showTable('refresh');
    }

    $('#provinsi-name').on('change', function() {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)
        $('#kecamatan-name').selectize()[0].selectize.destroy();
        $('#kecamatan-name').html('<option value="">-- Pilih Kecamatan --</option>')
        $('#kecamatan-name').selectize()

    })

    $('#kabkota-name').on('change', function() {
        var daerah_id = $("#kabkota-name").val();
        getDaerah('kecamatan', daerah_id)


    })
    $('#kecamatan-name').on('change', function() {
        var daerah_id = $("#kecamatan-name").val();
        // getDaerah('kelurahan', daerah_id)

    })

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: $('#no_dokumen').val(),
                    nama_pengirim: $('#nama_pengirim').val(),
                    status: $('#f_status').val(),
                    kode_daerah: $('#kecamatan-name').val() ? $('#kecamatan-name').val() : ($('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val()),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTrxPengecerSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Penjual',
                    data: 'nama_penjual',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Nama (` + r.type.toString().toUpperCase() + `)</dt>
                                    <dd class="col-12">` + r.nama_penjual + `</dd>
                                    <dt class="col-12">NPWP </dt>
                                    <dd class="col-12">` + r.npwp_penjual + `</dd>
                                    <dt class="col-12">Alamat</dt>
                                    <dd class="col-12">` + r.kelurahan + `, ` + r.kecamatan + `, ` + r.kab_kota + `, ` + r.provinsi + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                        <dt class="col-12">Tanggal Pembelian</dt>
                                    <dd class="col-12">` + r.tgl_pembelian + `</dd>
                                    <dt class="col-12">Jumlah </dt>
                                    <dd class="col-12">` + ((r.jumlah == null) ? '-' : addCommas(r.jumlah) + ` ` + r.satuan) + `</dd>
                                    <dt class="col-12">Harga</dt>
                                    <dd class="col-12">` + `Rp. ` + addCommas(r.harga) + `</dd>
                                </dl>`
                    },
                },
                {
                    title: 'Total Transaksi',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return 'Rp. ' + ((parseFloat(r.jumlah) * parseFloat(r.harga)).toFixed()).toString();
                    },
                },
                {
                    title: 'NIK',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {

                        return '<p>' + r.nik + '</p>'
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>