<script>
    $(function() {
        $('#table').DataTable({
            ordering: false,
            searching: false,
            lengthChange: false,
            paging: false,
            bInfo: false
        })

        getHargaBahanPokok()

        $('#search-bahan-pokok').click(function() {
            const prov = $('#filter-bahan-pokok-provinsi').val()
            const date1 = $('#filter-bahan-pokok-date1').val()
            const date2 = $('#filter-bahan-pokok-date2').val()
            const kab = $('#filter-bahan-pokok-kabupaten').val()
            const pasar = $('#filter-bahan-pokok-pasar').val()

            getHargaBahanPokok(prov, date1, date2, kab, pasar)
        })

        $('#filter-bahan-pokok-provinsi').selectize()
        $('#filter-bahan-pokok-kabupaten').selectize()
        $('#filter-bahan-pokok-pasar').selectize()

        $('#filter-bahan-pokok-pasar').selectize()[0].selectize.lock()

        <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>

            $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.lock()

            $('#filter-bahan-pokok-provinsi').change(function() {
                const provId = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>kab/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.setValue('')
                        $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.lock()
                            } else {
                                $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.unlock()

                                $.each(response.data, function(i, row) {
                                    $('#filter-bahan-pokok-kabupaten').selectize()[0].selectize.addOption({
                                        value: row.daerah_id,
                                        text: row.kab_kota
                                    })
                                })

                                $('#filter-bahan-pokok-kabupaten').html(optionKab)
                            }
                        }

                        // getPasar(provId)
                    }
                })
            })

            $('#filter-bahan-pokok-kabupaten').change(function() {
                const kabId = $(this).val()

                if (kabId != '') {
                    getPasar(kabId)
                }
            })
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
            $('#filter-bahan-pokok-provinsi').change(function() {
                const provId = $(this).val()
                getPasar(provId)
            })
        <?php endif; ?>

    })

    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#filter-bahan-pokok-pasar').selectize()[0].selectize.setValue('')
                $('#filter-bahan-pokok-pasar').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').selectize()[0].selectize.unlock()

                        $.each(response.data, function(i, row) {
                            $('#filter-bahan-pokok-pasar').selectize()[0].selectize.addOption({
                                value: row.pasar_id,
                                text: row.nama
                            })
                        })

                    } else {
                        $('#filter-bahan-pokok-pasar').selectize()[0].selectize.lock()
                    }
                }
            }
        })
    }

    function getHargaBahanPokok(provinsi = null, date1 = null, date2 = null, kab = null, pasar = null) {
        $.ajax({
            url: "<?= site_url() ?>harga/bahan-pokok/json",
            type: 'post',
            data: {
                provinsi: provinsi,
                date1: date1,
                date2: date2,
                kab: kab,
                pasar: pasar,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#load-bahan-pokok').hide()
                $('#loader-bahan-pokok').show()
            },
            complete: function() {
                $('#load-bahan-pokok').show()
                $('#loader-bahan-pokok').hide()
            },
            success: function(response) {
                $('#date1').text(response.date1)
                $('#date2').text(response.date2)
                $('#bahan-pokok-title').text(response.region)

                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    let tableData = ''

                    if (response.data.length > 0) {
                        $.each(response.data, function(i, row) {
                            let ket = '-';

                            if (row.ket == 'up') {
                                ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-up.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                            } else if (row.ket == 'down') {
                                ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-down.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                            } else if (row.ket == 'equals') {
                                ket = '<lottie-player src="<?= base_url() ?>assets/frontend/img/arrow-right.json" background="transparent" speed="1" style="width: 30px;" loop autoplay></lottie-player>'
                            }

                            tableData += '<tr>'
                            tableData += '<td>' + row.jenis_komoditi + '</td>'
                            tableData += '<td>' + row.satuan_komoditi + '</td>'
                            tableData += '<td class="text-center">' + ((row.date1) == 0 || row.date1 == '' ? '-' : row.date1) + '</td>'
                            tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' ? '-' : row.date2) + '</td>'
                            tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '' ? '-' : row.percentage + '%') + '</td>'
                            tableData += '<td class="text-center">' + ((row.date2) == 0 || row.date2 == '' || (row.date1) == 0 || row.date1 == '' ? '-' : ket) + '</td>'
                        })
                    } else {
                        tableData += '<tr class="odd"><td valign="top" colspan="6" class="dataTables_empty">No data available in table</td></tr>'
                    }

                    $('#harga-bahan-pokok').html(tableData)
                }
            }
        })
    }
</script>