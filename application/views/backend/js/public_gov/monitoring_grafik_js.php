<script src="<?= base_url() ?>assets/backend/libs/apexcharts/apexcharts.min.js"></script>
<script>
	$("#post_year").val("<?= date('Y') ?>").change();
	showAjax("<?= date('Y') ?>");
	$('#post_year').change(function() {
		if ($(this).val() != "") {
			showAjax($(this).val());
		} else {
			$('#chart').html('<center><img src="<?= base_url() ?>assets/frontend/img/default-nodata.svg"></center>');
		}
	});

	function showAjax(tahun) {
		$('#chart').html('<div class="text-center"><lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" loop autoplay></lottie-player></div>');
		$.ajax({
			url: "<?= site_url() ?>dashboard/perdagangan/monitoring-json/getdata",
			type: 'post',
			data: {
				post_year: tahun,
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			},
			dataType: 'json',
			success: function(response) {
				$('#chart').html('');
				showChart(response.input, response.notinput, response.tahun);
			}
		})
	}

	function showChart(input, notinput, tahun) {
		var options = {
			series: [{
					name: "INPUT",
					data: input
				},
				{
					name: "TIDAK INPUT",
					data: notinput
				}
			],
			chart: {
				height: 350,
				type: 'line',
				dropShadow: {
					enabled: true,
					color: '#000',
					top: 18,
					left: 7,
					blur: 10,
					opacity: 0.2
				},
				toolbar: {
					show: false
				}
			},
			colors: ['#77B6EA', '#545454'],
			dataLabels: {
				enabled: true,
			},
			stroke: {
				curve: 'smooth'
			},
			title: {
				text: 'GRAFIK PEMANTAUAN PROVINSI TAHUN ' + tahun,
				align: 'center'
			},
			grid: {
				borderColor: '#e7e7e7',
				row: {
					colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
					opacity: 0.5
				},
			},
			markers: {
				size: 1
			},
			xaxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
				title: {
					text: 'Bulan'
				}
			},
			yaxis: {
				title: {
					text: 'Jumlah'
				},
				min: 0,
				max: 40
			},
			legend: {
				position: 'bottom',
				horizontalAlign: 'center',
				floating: false,
				offsetY: 5,
				offsetX: 0
			}
		};

		var chart = new ApexCharts(document.querySelector("#chart"), options);
		chart.render();
	}
</script>