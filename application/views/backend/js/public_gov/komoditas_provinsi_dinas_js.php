<script src="<?= base_url() ?>assets/backend/libs/xls/xlsx.core.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/jhxlsx.js"></script>
<script>
    $(function() {
        $('#commodity-province').selectize()
        $('#commodity-year').selectize()
        $('#commodity-month').selectize()
        $('#province').selectize()
        $('#commodity').selectize()
        $('#unit').selectize()

        $('#post_provinsix').selectize({
            items: ["<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>"]
        });
        $('#post_yearx').selectize({
            items: ["<?= date('Y') ?>"]
        });
        $('#post_bulanx').selectize({});

        showData('refresh')

        $('#filter-commodity').click(function() {
            const province = $('#commodity-province').val()
            const year = $('#commodity-year').val()
            const month = $('#commodity-month').val()

            showData('refresh', province, year, month)
        })

        $('#create-commodity').click(function() {
            $('#modal').modal('show')
        })

        $('#commodity').change(function() {
            getSatuanKomoditi($(this).val())
        })

        $(".number").keypress(function(e) {
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })

        $('.number').keyup(function() {
            let val = $(this).val()
            val = val.replace(/\./g, '')

            $(this).val(formatRupiah(val, ''))
        })

        var dtToday = new Date();

        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();

        var maxDate = '2021-12';
        //var maxDate = year + '-' + month + '-' + day;

        var date = new Date(),
            y = date.getFullYear(),
            m = date.getMonth();

        var month = 0; // January
        var d = new Date(y, m + 1, 0);
        let lastDate = d.getFullYear() + "-" + (d.getMonth() + 1)
        if (d.getMonth() < 10) {
            lastDate = d.getFullYear() + "-0" + (d.getMonth() + 1)
        }

        $('#date').attr('min', maxDate);
        $('#date').attr('max', lastDate);

        $("#modal").on('hidden.bs.modal', function(e) {
            $('#form-tambah')[0].reset();
            $('#commodity')[0].selectize.setValue('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
            $('.selectize-input').closest('div.col').find('.text-danger').remove()
        });
    })

    function showData(refresh = null, province = null, year = null, month = null) {
        if ($.fn.DataTable.isDataTable('#table-commodity')) {
            $("#table-commodity").dataTable().fnDestroy();
            $('#table-commodity').empty();
        }

        var table = $('#table-commodity').DataTable({

            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/perdagangan/provinsi/json',
                data: {
                    refresh: refresh,
                    province: province,
                    year: year,
                    month: month,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Periode',
                    data: 'tanggal',
                }, {
                    title: 'Komoditas',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Produksi',
                    data: 'produksi',
                },
                {
                    title: 'Konsumsi',
                    data: 'konsumsi',
                },
                {
                    title: 'Aksi',
                    data: 'stok_id',
                    className: '',
                    render: function(k, v, r, m) {
                        var inDate = new Date(r.tahun, r.bulan, 0);
                        var nowDate = new Date();
                        var validInputDate = new Date(2021, 12, 0);
                        if (inDate >= validInputDate) {
                            return '<button onclick="update(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-warning m-1" title="Update"><i class="ti ti-edit"></i></button>' +
                                '<button onclick="remove(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-danger m-1" title="Update"><i class="ti ti-trash"></i></button>'
                        } else {
                            return ''
                        }

                    }
                },
            ],
            initComplete: function() {
                $.fn.dataTable.tables({
                    visible: true,
                    api: true
                }).columns.adjust();
            },
            scrollX: true,
            displayLength: 10
        });
    }

    $('#form-tambah').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/provinsi/add",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                let success = response.success
                let validation = response.validation
                let error = response.error

                $('.form-control').removeClass('frm-error')
                $('.form-control').closest('div.col').find('.text-danger').remove()

                if (success) {
                    Swal.fire({
                        title: 'Data Berhasil Disimpan',
                        icon: 'success',
                        confirmButtonColor: '#E70A2B',
                        confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then(function() {
                        $.fn.dataTable.tables({
                            visible: true,
                            api: true
                        }).columns.adjust();

                        showData('refresh')
                        $('#modal').modal('hide')
                    })

                } else if (!success) {
                    $.each(validation, function(key, value) {
                        const element = $('#' + key);

                        element.closest('div.form-group').find('.text-danger').remove();

                        (element.attr('type') == 'text' || element.attr('type') == 'date') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                        value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                        value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                    });
                }
            }
        })
    })

    function update(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/provinsi/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                $('#komoditas_id').val(response.stok_id)
                $('#commodity')[0].selectize.setValue(response.jenis_komoditi_id);
                getSatuanKomoditi(response.jenis_komoditi_id, response.satuan_komoditi_id)
                $('#produksi').val(response.produksi)
                $('#konsumsi').val(response.konsumsi)

                $('#modal').modal('show')
            }
        })
    }

    function remove(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/provinsi/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    showData('refresh')
                }
            }
        })
    }

    function getSatuanKomoditi(jenis_komoditi, satuan_komoditi = null) {
        $.ajax({
            url: "<?= site_url() ?>dashboard/komoditi/satuan/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#unit').selectize()[0].selectize.setValue('')
                $('#unit').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.success) {
                    $('#unit')[0].selectize.enable();
                    $.each(response.data, function(key, value) {
                        $('#unit').selectize()[0].selectize.addOption({
                            value: value.satuan_komoditi_id,
                            text: value.satuan_komoditi
                        })
                    })

                    if (satuan_komoditi != null) {
                        $('#unit').selectize()[0].selectize.setValue(satuan_komoditi);
                    }
                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }

                    $('#unit')[0].selectize.disable();
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }

    function get_export() {
        var htmlTmp = ''
        $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>dashboard/perdagangan/monitoring-json/produksikonsumsi_export',
            data: {
                post_daerah: $('#post_provinsix').val(),
                post_year: $('#post_yearx').val(),
                post_bulan: $('#post_bulanx').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                htmlTmp = $('#btn_export').html()
                $('#btn_export').html('<div class="spinner-border text-primary" role="status"></div>')
            },
            complete: function() {
                $('#btn_export').html(htmlTmp)
            },
            success: function(response) {
                if (response.code == 403) {
                    location.reload();
                } else {
                    //console.log(response)
                    var bulaText = ''
                    if ($('#post_bulanx').val() != '-')
                        bulaText = ' Bulan ' + $('#post_bulanx').text()
                    else
                        bulaText = ''

                    var result = []
                    var paramJudul = "Rekap Data Produksi Konsumsi " + $('#post_provinsix').text() + " Tahun " + $('#post_yearx').val() + bulaText
                    var paramOpt = {
                        "sheetName": "Sheet1"
                    }
                    var label = ['Provinsi', 'Bulan', 'Tahun', 'Komoditas', 'Produksi', 'Konsumsi', 'Satuan']
                    var kolom = ['provinsi', 'bulan', 'tahun', 'jenis_komoditi', 'produksi', 'konsumsi', 'satuan_komoditi']
                    var tmp_field = {}
                    var tmp_data = []
                    var data = []

                    tmp_field = {}
                    tmp_field['merge'] = "0-" + (label.length - 1)
                    tmp_field['style'] = {
                        "font": {
                            "bold": true
                        }
                    }
                    tmp_field['text'] = paramJudul.toUpperCase()
                    tmp_data.push(tmp_field)
                    data.push(tmp_data)

                    tmp_data = []
                    for (var r = 0; r < label.length; r++) {
                        tmp_field = {}
                        tmp_field['style'] = {
                            "font": {
                                "bold": true
                            }
                        }
                        tmp_field['text'] = label[r].toUpperCase()
                        tmp_data.push(tmp_field)
                    }
                    data.push(tmp_data)

                    $.each(response.data, function(i, row) {
                        tmp_data = []
                        for (var r = 0; r < kolom.length; r++) {
                            tmp_field = {}
                            tmp_field['text'] = row[kolom[r]]
                            tmp_data.push(tmp_field)
                        }
                        data.push(tmp_data)
                    })
                    paramOpt['data'] = data
                    result.push(paramOpt)
                    var options = {
                        fileName: paramJudul,
                        header: true,
                    };
                    Jhxlsx.export(result, options);
                    //console.log(JSON.stringify(result));
                }
            }
        })
    }
</script>