<script>
    $(function() {
        let id = $('#filter-name').val()
        let month = $('#filter-month').val()
        let year = $('#filter-year').val()

        showRekap('refresh', id, month, year)

        $('#filter-name').selectize()
        $('#filter-month').selectize()
        $('#filter-year').selectize()

        $('#filter-search').click(function() {
            let id = $('#filter-name').val()
            let month = $('#filter-month').val()
            let year = $('#filter-year').val()
            showRekap('refresh', id, month, year)
        })
    })


    function showRekap(refresh = null, id = null, month = null, year = null) {
        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/perdagangan/absensi/rekap-absensi-harga/json',
                data: {
                    refresh: refresh,
                    id: id,
                    month: month,
                    year: year,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Email',
                    data: 'email',
                },
                {
                    title: 'Deskripsi',
                    data: 'deskripsi',
                },
                {
                    title: 'Pasar',
                    data: 'nama_pasar',
                },
                {
                    title: 'Photo',
                    render: function(k, v, r) {
                        return '<img width="50" height="60" src="' + r.photo + '">'
                    },
                    className: 'text-center'
                },
            ],
            scrollX: true,
            displayLength: 10
        });
    }
    s
</script>