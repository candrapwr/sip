<script>
    $(function() {
        $('#provinsi').selectize()
        $('#kabupaten').selectize()
        $('#tipe_pasar').selectize()
        $('#kondisi').selectize()
        $('#pengelola').selectize()
        $('#kepemilikan').selectize()
        $('#bentuk').selectize()

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
            $('#kabupaten')[0].selectize.disable()
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>
            $('#pengelola')[0].selectize.disable()
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
            if ($('#provinsi').val() != '') {
                const provId = $('#provinsi').val()

                $.ajax({
                    url: "<?= site_url() ?>kab/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#kabupaten').selectize()[0].selectize.setValue('')
                        $('#kabupaten').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#kabupaten')[0].selectize.disable();
                            } else {
                                $('#kabupaten')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#kabupaten').selectize()[0].selectize.addOption({
                                        value: row.daerah_id,
                                        text: row.kab_kota
                                    })
                                })

                                if ($('#filter-search-kabupaten-flash').val() != '') {
                                    $('#kabupaten').selectize()[0].selectize.setValue($('#filter-search-kabupaten-flash').val());
                                }
                            }
                        }
                    }
                })

                $.ajax({
                    url: "<?= site_url() ?>pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            }

            $('#filter-tahun-tpdak').change(function() {
                const year = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>pasar/sum-tpdak/json",
                    type: 'post',
                    data: {
                        year: year,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (response.data.length > 0) {
                                $.each(response.data, function(i, row) {
                                    $('#count-tp > span').text(row.jml_tp)
                                    $('#count-tp > span').attr('data-stop', row.jml_tp)
                                    $('#count-dak > span').text(row.jml_dak)
                                    $('#count-dak > span').attr('data-stop', row.jml_dak)
                                    $('#count-nontpdak > span').text(row.jml_non_dak)
                                    $('#count-nontpdak > span').attr('data-stop', row.jml_non_dak)
                                })
                            } else {
                                $('#count-tp > span').text('0')
                                $('#count-tp > span').attr('data-stop', '0')
                                $('#count-dak > span').text('0')
                                $('#count-dak > span').attr('data-stop', '0')
                                $('#count-nontpdak > span').text('0')
                                $('#count-nontpdak > span').attr('data-stop', '0')
                            }
                        }
                    }
                })
            })
        <?php endif; ?>

        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
            if ($('#kabupaten').val() != '') {
                const provId = $('#kabupaten').val()

                $.ajax({
                    url: "<?= site_url() ?>pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            }

            $('#kabupaten').change(function() {
                const provId = $(this).val()

                $.ajax({
                    url: "<?= site_url() ?>pasar/pengelola/json",
                    type: 'post',
                    data: {
                        provinsi_id: provId,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    dataType: 'json',
                    beforeSend: function() {
                        $('#pengelola').selectize()[0].selectize.setValue('')
                        $('#pengelola').selectize()[0].selectize.clearOptions()
                    },
                    success: function(response) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url() ?>user/sign_out'
                        } else {
                            if (!response.success) {
                                $('#pengelola')[0].selectize.disable();
                            } else {
                                $('#pengelola')[0].selectize.enable();

                                $.each(response.data, function(i, row) {
                                    $('#pengelola').selectize()[0].selectize.addOption({
                                        value: row.email,
                                        text: row.nama_lengkap
                                    })
                                })

                                if ($('#filter-search-pengelola-flash').val() != '') {
                                    $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                }
                            }
                        }
                    }
                })
            })
        <?php endif; ?>

        $('#provinsi').change(function() {
            const provId = $(this).val()

            $.ajax({
                url: "<?= site_url() ?>kab/json",
                type: 'post',
                data: {
                    provinsi_id: provId,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                dataType: 'json',
                beforeSend: function() {
                    $('#kabupaten').selectize()[0].selectize.setValue('')
                    $('#kabupaten').selectize()[0].selectize.clearOptions()
                },
                success: function(response) {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url() ?>user/sign_out'
                    } else {
                        if (!response.success) {
                            $('#kabupaten')[0].selectize.disable();
                        } else {
                            $('#kabupaten')[0].selectize.enable();

                            $.each(response.data, function(i, row) {
                                $('#kabupaten').selectize()[0].selectize.addOption({
                                    value: row.daerah_id,
                                    text: row.kab_kota
                                })
                            })
                        }
                    }

                    $.ajax({
                        url: "<?= site_url() ?>pasar/pengelola/json",
                        type: 'post',
                        data: {
                            provinsi_id: provId,
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                        },
                        dataType: 'json',
                        beforeSend: function() {
                            $('#pengelola').selectize()[0].selectize.setValue('')
                            $('#pengelola').selectize()[0].selectize.clearOptions()
                        },
                        success: function(response) {
                            if (response.code == 401) {
                                window.location.href = '<?= site_url() ?>user/sign_out'
                            } else {
                                if (!response.success) {
                                    $('#pengelola')[0].selectize.disable();
                                } else {
                                    $('#pengelola')[0].selectize.enable();

                                    $.each(response.data, function(i, row) {
                                        $('#pengelola').selectize()[0].selectize.addOption({
                                            value: row.email,
                                            text: row.nama_lengkap
                                        })
                                    })

                                    if ($('#filter-search-pengelola-flash').val() != '') {
                                        $('#pengelola').selectize()[0].selectize.setValue($('#filter-search-pengelola-flash').val());
                                    }
                                }
                            }
                        }
                    })
                }
            })
        })
    })

    function getPasar(provId) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (response.success) {
                        $('#filter-bahan-pokok-pasar').prop('disabled', false)

                        let optionPasar = '<option value="">Choose...</option>'

                        $.each(response.data, function(i, row) {
                            optionPasar += '<option value="' + row.pasar_id + '$' + row.nama + '">' + row.nama + '</option>'
                        })

                        $('#filter-bahan-pokok-pasar').html(optionPasar)
                    } else {
                        $('#filter-bahan-pokok-pasar').prop('disabled', true)
                        $('#filter-bahan-pokok-pasar').html('<option value="">Choose...</option>')
                    }
                }
            }
        })
    }
</script>