<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/backend/css/jquery.json-viewer.css" />
<script type="text/javascript" src="<?= base_url() ?>assets/backend/js/jquery.json-editor.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@3.9.1/dist/chart.min.js"></script>
<script>
    $(document).ready(function() {
        showTable('refresh');
        showStat('refresh');
        showAlokasiPerusahaan('refresh')
        $('#tgl_awal').selectize();
        $('#tgl_akhir').selectize();
        $('#periode_awal').selectize();
        $('#periode_akhir').selectize();
        $('#perusahaan_periode_awal').selectize();
        $('#perusahaan_periode_akhir').selectize();

    })

    var myChart;

    function fill() {
        showTable('refresh');
        showStat('refresh');
    }

    $('#tgl_awal').on('change', function() {
        var tgl_akhir = '<option value="">Pilih Tgl Akhir</option>';
        $.ajax({
            url: '<?= base_url() ?>perdagangan/migor/getPeriodeAlokasiSIMIRAH',
            type: 'POST',
            beforeSend: function() {
                $('#tgl_akhir').selectize()[0].selectize.destroy();
            },
            data: {
                length: 10000,
                start: 0,
                refresh: 'refresh',
                tgl_awal: $('#tgl_awal').val(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                result = JSON.parse(result);
                $.each(result.data, function(k, v) {
                    tgl_akhir += '<option value="' + v.tgl_akhir + '">' + v.tgl_akhir + '</option>';
                })

                $('#series').prop('max', result.data.length)
                $('#series').prop('readonly', false)

            },
            complete: function() {
                $('#tgl_akhir').html(tgl_akhir);
                $('#tgl_akhir').selectize();
            }
        })
    })


    $('#periode_awal, #periode_akhir').on('change', function() {
        myChart.destroy();
        showStat('refresh')
    })

    function exportExcel() {
        $.ajax({
            url: '<?= base_url() ?>web/alokasidmo-export',
            type: 'POST',
            beforeSend: function() {
                $('#isi_isi').LoadingOverlay('show');
            },
            data: {
                title: 'Alokasi DMO ' + $('#tgl_awal').val() + ' sd ' + $('#tgl_awal').val(),
                refresh: 'refresh',
                produsen_cpo: tbl_produsen_cpo,
                produsen_migor: tbl_produsen_migor,
                produsen_migor_cpo: tbl_produsen_migor_cpo,
                exportir_cpo: tbl_exportir_cpo,
                exportir_migor: tbl_exportir_migor,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                $('#isi_isi').LoadingOverlay('hide');
                window.open('<?= base_url() ?>web/mirah-download', '_blank');
            }
        })
    }


    function exportAlokasi() {
        $.ajax({
            url: '<?= base_url() ?>web/summary-alokasi-export',
            type: 'POST',
            data: {
                title: 'Alokasi DMO ' + $('#summary_periode').html(),
                data: $('#tbl_data_alokasi').html(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                window.open('<?= base_url() ?>web/mirah-download', '_blank');
            }
        })
    }

    // $('#provinsi-name').on('change', function() {
    //     var daerah_id = $("#provinsi-name").val();
    //     getDaerah('kabkota', daerah_id)
    //     showTable('refresh');

    // })

    // $('#kabkota-name').on('change', function() {
    //     var daerah_id = $("#kabkota-name").val();
    //     showTable('refresh');

    // })



    function showTable(refresh = null) {

        var data_post = {
            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
            refresh: refresh,
            kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
            title: 'Rekap Jumlah Distribusi SIMIRAH' + (($('#tgl_awal').val() == '') ? '' : ' ' + $('#tgl_awal').val() + ' - ' + $('#tgl_akhir').val() + '')
        }

        var alokasi = {
            'alokasi': true,
            'refresh': true
        }

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }
        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            // buttons:[{
            //     text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
            //     action: function(e, dt, node, config) {
            //         $.ajax({
            //             url: '<?= base_url() ?>web/mirah-export',
            //             type: 'POST',
            //             data: data_post,
            //             success: function(result) {
            //                 window.open('<?= base_url() ?>web/mirah-download', '_blank');
            //             }
            //         })
            //     }
            // }],
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: $('#no_dokumen').val(),
                    nama_pengirim: $('#nama_pengirim').val(),
                    type: $('#type').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getPeriodeAlokasiSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'tgl_awal',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Periode Penarikan Alokasi',
                    data: 'tgl_awal',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return 'Awal : ' + r.tgl_awal + '<br>' + 'Akhir : ' + r.tgl_akhir
                    }
                },

                {
                    title: 'Penarikan',
                    data: 'createdon',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return '<b>' + r.nama + '</b><br><i style="font-size:10px">' + r.createdon + '</i>'
                    }
                },
                {
                    title: 'Status Kirim',
                    data: 'status_kirim',
                    render: function(k, v, r) {
                        if (r.status_kirim == 1 && r.sendon == null) {
                            return 'Transisi Manual -> Auto'
                        }
                        var status_kirim = '<div class="badge bg-warning"><i class="ti ti-hourglass-high me-1"></i>Belum Kirim</div>';
                        if (r.status_kirim == 1) {
                            status_kirim = '<div class="badge bg-success"><i class="ti ti-circle-check me-1"></i>Sukses Kirim</div><br> <i style="font-size:10px"> ' + r.sendon + '</i><br> <b>No Kep.</b> ' + r.no_keputusan + '<br>' + ' <b>Periode</b> ' + r.periode;
                        } else if (r.status_kirim == 99) {
                            status_kirim = '<div class="badge bg-danger"><i class="ti ti-x me-1"></i>Gagal Kirim</div>';
                        }
                        return status_kirim;
                    }
                },
                {
                    title: 'Aksi',
                    data: 'nama',
                    orderable: false,
                    render: function(k, v, r) {
                        if (r.status_kirim == 1 && r.sendon == null) {
                            return ''
                        }
                        var action = ''
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1) {

                            if (r.status_kirim == 1) {
                                //  action += '<button type="button" class="btn btn-dark m-1" data-payload=\'' + r.payload + '\' data-body=\'' + r.request_body + '\' data-output=\'' + r.request_output + '\' data-periode=\'' + r.periode + '\' onclick="showPayload(this)"><i class="ti ti-send"></i></button>'
                            } else {
                                action += '<button type="button" class="btn btn-success m-1" onclick="showSummary(\'true\',\'' + r.tgl_awal + '\',\'' + r.tgl_akhir + '\',\'' + r.alokasi_id + '\',\'' + r.status_kirim + '\')"><i class="ti ti-list"></i></button>'

                            }
                        }



                        // if (r.status_kirim != 1) {
                        //     if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'delete') : false) ?>' == 1) {
                        //         action += '<button type="button" class="btn btn-danger m-1" onclick="delAlokasi(\'' + r.alokasi_id + '\')"><i class="ti ti-trash"></i></button>'
                        //     }
                        // }



                        return action
                    }
                },




            ],
            "scrollX": true,
            "displayLength": 20
        });
    }

    function delAlokasi(id) {

        Swal.fire({
            title: 'Hapus alokasi DMO ini?',
            text: "Alokasi yang sudah dihapus tidak dapat dikembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?= base_url() ?>web/delete-alokasi',
                    type: 'POST',
                    data: {
                        alokasi_id: id,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

                    },
                    success: function(result) {
                        result = JSON.parse(result)
                        if (result['kode'] == 200) {
                            Swal.fire(
                                'Terhapus!',
                                'Alokasi berhasil dihapus.',
                                'success'
                            )
                            showTable('refresh');
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Alokasi gagal dihapus.',
                                'error'
                            )
                        }

                    }
                })

            }
        })


    }

    function showPayload(th) {
        $('#summary_periode').html('Periode ' + $(th).data('periode'))
        new JsonEditor('#response_body', $(th).data('body'));
        new JsonEditor('#response_output', $(th).data('output'));


        var data = `<tr>
                        <td colspan="5"><b>PENDISTRIBUSIAN DMO CPO OLEH PRODUSEN CPO / EKSPORTIR CPO</b></td>
                    </tr>
                    <tr style="font-weight:bold">
                        <td>No</td>
                        <td>Kode Izin</td>
                        <td>NIB</td>
                        <td>Nama Perusahaan</td>
                        <td>Jumlah DMO</td>
                        <td>Satuan</td>
                    </tr>`
        var no = 1;
        var total_cpo = 0;
        $.each($(th).data('payload')['Produsen CPO'], function(k, v) {
            data += `
                    <tr>
                        <td>` + no + `</td>
                        <td>` + v.kdIjin + `</td>
                        <td>` + v.nib + `</td>
                        <td>` + v.namaPerusahaan + `</td>
                        <td>` + toFloat(v.jumlahDmo) + `</td>
                        <td>` + v.kdSatuan + `</td>
                    </tr>
            `
            total_cpo = total_cpo + toFloat(v.jumlahDmo)
            no++;
        })
        data += `
                    <tr style="font-weight:bold">
                        <td colspan="4">TOTAL ALOKASI DMO</td>
                        <td>` + addCommas(toFloat(total_cpo)) + `</td>
                        <td>KGM</td>
                    </tr>
                    <tr style="font-weight:bold">
                        <td colspan="4"></td>
                        <td>` + addCommas(toFloat(total_cpo / 1000)) + `</td>
                        <td>TNE</td>
                    </tr>
                    <tr><td colspan="6"><hr></td></tr>
                `
        data += `<tr>
                        <td colspan="5"><b>PENDISTRIBUSIAN DMO MINYAK GORENG OLEH PRODUSEN MIGOR / EKSPORTIR MIGOR</b></td>
                    </tr>
                    <tr style="font-weight:bold">
                        <td>No</td>
                        <td>Kode Izin</td>
                        <td>NIB</td>
                        <td>Nama Perusahaan</td>
                        <td>Jumlah DMO</td>
                        <td>Satuan</td>
                    </tr>`
        var no = 1;
        var total_non_cpo = 0;
        $.each($(th).data('payload')['Produsen Migor'], function(k, v) {
            data += `
                    <tr>
                        <td>` + no + `</td>
                        <td>` + v.kdIjin + `</td>
                        <td>` + v.nib + `</td>
                        <td>` + v.namaPerusahaan + `</td>
                        <td>` + toFloat(v.jumlahDmo) + `</td>
                        <td>` + v.kdSatuan + `</td>
                    </tr>
            `
            total_non_cpo = total_non_cpo + toFloat(v.jumlahDmo)
            no++;
        })

        data += `
                    <tr style="font-weight:bold">
                        <td colspan="4">TOTAL ALOKASI DMO</td>
                        <td>` + addCommas(toFloat(total_non_cpo)) + `</td>
                        <td>KGM</td>
                    </tr>
                    <tr style="font-weight:bold">
                        <td colspan="4"></td>
                        <td>` + addCommas(toFloat(total_non_cpo / 1000)) + `</td>
                        <td>TNE</td>
                    </tr>
                    <tr><td colspan="6"><hr></td></tr>
                `



        $('#tbl_main').html(data)
        $('#modal_payload').modal('show');
    }

    function kirimAlokasi(id, tgl_awal, tgl_akhir) {

        Swal.fire({
            title: 'Kirim alokasi DMO ini?',
            text: "Alokasi yang sudah dikirim tidak dapat dihapus!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Kirim!',
            cancelButtonText: 'Batal'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?= base_url() ?>perdagangan/migor/kirim-alokasi',
                    type: 'POST',
                    beforeSend: function() {
                        $('#isi_isi').LoadingOverlay('show');
                    },
                    data: {
                        alokasi_id: $('#k_alokasi_id').val(),
                        tgl_awal: $('#k_tgl_awal').val(),
                        tgl_akhir: $('#k_tgl_akhir').val(),
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

                    },
                    success: function(result) {
                        result = JSON.parse(result)
                        if (result['kode'] == 200) {
                            Swal.fire(
                                'Terkirim!',
                                'Alokasi berhasil terkirim.',
                                'success'
                            )
                            $('#modal').modal('hide')
                            showTable('refresh');
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Alokasi gagal terkirim.',
                                'error'
                            )
                        }
                        $('#isi_isi').LoadingOverlay('hide');
                    }
                })

            }
        })


    }

    var tbl_produsen_cpo = '';
    var tbl_produsen_migor = '';
    var tbl_produsen_migor_cpo = '';
    var tbl_exportir_cpo = '';
    var tbl_exportir_migor = '';

    function showSummary(refresh = true, tgl_awal = null, tgl_akhir = null, alokasi_id = null, status_kirim = null,jml_kirim =null) {
        


        if (($('#tgl_awal').val() == '' || $('#tgl_akhir').val() == '') && (tgl_awal == null && tgl_akhir == null)) {
            Swal.fire({
                title: 'Mohon pilih periode awal dan akhir',
                icon: 'error',
                confirmButtonColor: '#E70A2B',
                confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                allowOutsideClick: false,
                allowEscapeKey: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#modal').modal('hide')
                }
            })
            return;
        }

        $('#k_alokasi_id').val(alokasi_id)
        $('#k_tgl_awal').val(tgl_awal)
        $('#k_tgl_akhir').val(tgl_akhir)
        if (status_kirim == 1 || tgl_awal == null || tgl_akhir == null || alokasi_id == null) {
            $('#btn_kirimalokasi').hide();
        } else {
            $('#btn_kirimalokasi').show();
        }


        if (parseInt($('#series').val()) > parseInt($('#series').prop('max'))) {

            $('#series').val($('#series').prop('max'))
        }



        var header = '';
        var subheader = '';
        var body_migor = ''
        $.ajax({
            url: '<?= base_url() ?>perdagangan/migor/getSummaryAlokasiSIMIRAH',
            type: 'POST',
            beforeSend: function() {
                $('#isi_isi').LoadingOverlay('show');
            },
            data: {
                refresh: 'refresh',
                tgl_awal: ((tgl_awal != null) ? tgl_awal : $('#tgl_awal').val()),
                tgl_akhir: ((tgl_akhir != null) ? tgl_akhir : $('#tgl_akhir').val()),
                value_only: ((alokasi_id == null) ? 'false' : ((jml_kirim == 0)?'false':'true')),
                series: (parseInt($('#series').val()) > parseInt($('#series').prop('max'))) ? parseInt($('#series').prop('max')) : parseInt($('#series').val()),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                result = JSON.parse(result);
                if (result['kode'] == 200) {

                    var body_migor = ''
                    var peraturan = ''
                    if (result.data['Produsen Migor'] !== undefined) {
                        $.each(result.data['Produsen Migor'][0]['alokasi'], function(k, v) {
                            header += ` <th colspan="">` + k + `</th>`
                            subheader += `<th>Distribusi</th>
                                    <!--th>X Regional</th>
                                    <th>X Kemasan</th>
                                    <th>Jumlah</th-->`
                            if (peraturan == '') {
                                if (result.data['Produsen Migor'][0]['peraturan'] != '') {
                                    peraturan = result.data['Produsen Migor'][0]['peraturan']
                                }

                            }
                        })
                        subheader += ` <th>X Regional</th>
                            <th>X Kemasan</th>`

                        body_migor += '<tr><td colspan= "' + (4 + (Object.keys(result.data['Produsen Migor'][0]['alokasi']).length * 4)) + '"> <b>Produsen Migor</b></td></tr>';
                        var no = 1;

                        var delta_pmigor = 0;
                        var sum_pmigor = 0;
                        $.each(result.data['Produsen Migor'], function(k, v) {
                            var total_p_migor = 0;
                            var temp_p_migor = 0;
                            var pmigor_x_regional = 0;
                            var pmigor_x_kemasan = 0;
                            body_migor += ` <tr>
                            <td>` + no + `</td>
                            <td>` + v.nama + `</td>
                            <td>` + v.nib + `</td>
                            <td>` + v.provinsi + `</td>
                            <td>` + v.packaging + `</td>`;
                            $.each(result.data['Produsen Migor'][k]['alokasi'], function(kk, vv) {
                                body_migor += ` <td>` + toFloat(vv.jml_distribusi) + `</td>
                                                <!--td>` + vv.pengali_regional + `</td>
                                                <td>` + vv.pengali_kemasan + `</td>
                                                <td>` + vv.jml + `</td-->`
                                pmigor_x_regional = toFloat(vv.pengali_regional);
                                pmigor_x_kemasan = toFloat(vv.pengali_kemasan);
                                total_p_migor = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi)) - temp_p_migor
                                temp_p_migor = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi));
                            })
                            body_migor += `
                        <td>` + total_p_migor + `</td>
                        <td>` + pmigor_x_regional + `</td>
                        <td>` + pmigor_x_kemasan + `</td>
                        <td>` + (total_p_migor * pmigor_x_regional * pmigor_x_kemasan) + `</td>
                        </tr>`
                            delta_pmigor = toFloat(total_p_migor) + toFloat(delta_pmigor);
                            sum_pmigor = toFloat((total_p_migor * pmigor_x_regional * pmigor_x_kemasan)) + toFloat(sum_pmigor);;
                            no = no + 1;
                        })
                        body_migor += `
                    <tr>
                    <td colspan ="7"><b>TOTAL</b></td>
                    <td><b>` + addCommas(delta_pmigor) + `</b></td>
                    <td colspan="2">KGM</td>
                    <td><b>` + addCommas(sum_pmigor) + `</b></td>
                    </tr>
                    <tr>
                    <td colspan ="7"><b></b></td>
                    <td><b>` + addCommas(toFloat(delta_pmigor / 1000)) + `</b></td>
                    <td colspan="2">TNE</td>
                    <td><b>` + addCommas(toFloat(sum_pmigor / 1000)) + `</b></td>
                    </tr>
                    `
                    }

                    var body_cpo = ''
                    if (result.data['Produsen CPO'] !== undefined) {

                        var body_cpo = '<tr><td colspan= "' + (4 + (Object.keys(result.data['Produsen CPO'][0]['alokasi']).length * 4)) + '"><b> Produsen CPO</b></td></tr>';

                        var no_p_cpo = 1
                        var delta_pcpo = 0;
                        var sum_pcpo = 0;
                        $.each(result.data['Produsen CPO'], function(k, v) {
                            var total_p_cpo = 0;
                            var temp_p_cpo = 0;
                            var pcpo_x_regional = 0;
                            var pcpo_x_kemasan = 0;
                            body_cpo += ` <tr>
                            <td>` + no_p_cpo + `</td>
                            <td>` + v.nama + `</td>
                            <td>` + v.nib + `</td>
                            <td>` + v.provinsi + `</td>
                            <td>` + v.packaging + `</td>`;
                            $.each(result.data['Produsen CPO'][k]['alokasi'], function(kk, vv) {
                                body_cpo += ` <td>` + toFloat(vv.jml_distribusi) + `</td>
                                                <!--td>` + vv.pengali_regional + `</td>
                                                <td>` + vv.pengali_kemasan + `</td>
                                                <td>` + vv.jml + `</td-->`
                                pcpo_x_regional = toFloat(vv.pengali_regional);
                                pcpo_x_kemasan = toFloat(vv.pengali_kemasan);
                                total_p_cpo = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi)) - temp_p_cpo
                                temp_p_cpo = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi));
                            })
                            body_cpo += `
                        <td>` + total_p_cpo + `</td>
                        <td>` + pcpo_x_regional + `</td>
                        <td>` + pcpo_x_kemasan + `</td>
                         <td>` + (total_p_cpo * pcpo_x_regional * pcpo_x_kemasan) + `</td></tr>`
                            delta_pcpo = toFloat(total_p_cpo) + toFloat(delta_pcpo);
                            sum_pcpo = toFloat((total_p_cpo * pcpo_x_regional * pcpo_x_kemasan)) + toFloat(sum_pcpo);;
                            no_p_cpo = no_p_cpo + 1;
                        })

                        body_cpo += `
                    <tr>
                    <td colspan ="7"><b>TOTAL</b></td>
                    <td><b>` + addCommas(delta_pcpo) + `</b></td>
                    <td colspan="2">KGM</td>
                    <td><b>` + addCommas(sum_pcpo) + `</b></td>
                    </tr>
                    <tr>
                    <td colspan ="7"><b></b></td>
                    <td><b>` + addCommas(toFloat(delta_pcpo / 1000)) + `</b></td>
                    <td colspan="2">TNE</td>
                    <td><b>` + addCommas(toFloat(sum_pcpo / 1000)) + `</b></td>
                    </tr>
                    `
                    }

                    //HIDE KETIKA PENGIRIMAN , INSENTIF MULAI DIKIRIM 24 OCT data 23 OCT
                    var start = new Date("2022-10-22"),
                        end = new Date(((tgl_akhir != null) ? tgl_akhir : $('#tgl_akhir').val())),
                        diff = new Date(end - start),
                        days = diff / 1000 / 60 / 60 / 24;
                   // console.log(days)
                    var body_migor_cpo = ''
                    if ( /*alokasi_id == null*/ days > 0) {

                        if (result.data['Produsen Migor (CPO)'] !== undefined) {

                            var body_migor_cpo = '<tr><td colspan= "' + (4 + (Object.keys(result.data['Produsen Migor (CPO)'][0]['alokasi']).length * 4)) + '"> <b>Produsen Migor (CPO)</b></td></tr>';


                            var no_p_migor_cpo = 1
                            var delta_pmigorcpo = 0;
                            var sum_pmigorcpo = 0;
                            $.each(result.data['Produsen Migor (CPO)'], function(k, v) {
                                var total_p_migor_cpo = 0;
                                var temp_p_migor_cpo = 0;
                                var pmigorcpo_x_regional = 0;
                                var pmigorcpo_x_kemasan = 0;
                                body_migor_cpo += ` <tr>
                            <td>` + no_p_migor_cpo + `</td>
                            <td>` + v.nama + `</td>
                            <td>` + v.nib + `</td>
                            <td>` + v.provinsi + `</td>
                            <td>` + v.packaging + `</td>`;
                                $.each(result.data['Produsen Migor (CPO)'][k]['alokasi'], function(kk, vv) {
                                    body_migor_cpo += ` <td>` + toFloat(vv.jml_distribusi) + `</td>
                                                <!--td>` + vv.pengali_regional + `</td>
                                                <td>` + vv.pengali_kemasan + `</td>
                                                <td>` + vv.jml + `</td-->`
                                    pmigorcpo_x_regional = toFloat(vv.pengali_regional);
                                    pmigorcpo_x_kemasan = toFloat(vv.pengali_kemasan);
                                    total_p_migor_cpo = toFloat(((vv.jml == '-') ? 0 : vv.jml)) - temp_p_migor_cpo
                                    temp_p_migor_cpo = toFloat(((vv.jml == '-') ? 0 : vv.jml));
                                })
                                body_migor_cpo += ` 
                        <td>` + total_p_migor_cpo + `</td>
                        <td>` + pmigorcpo_x_regional + `</td>
                        <td>` + pmigorcpo_x_kemasan + `</td>
                        <td>` + ((total_p_migor_cpo * pmigorcpo_x_regional * pmigorcpo_x_kemasan) - total_p_migor_cpo) + `</td></tr>`
                                delta_pmigorcpo = toFloat(total_p_migor_cpo) + toFloat(delta_pmigorcpo);
                                sum_pmigorcpo = toFloat(((total_p_migor_cpo * pmigorcpo_x_regional * pmigorcpo_x_kemasan) - total_p_migor_cpo)) + toFloat(sum_pmigorcpo);;
                                no_p_migor_cpo = no_p_migor_cpo + 1;
                            })
                        
                        body_migor_cpo += `
                            <tr>
                            <td colspan ="7"><b>TOTAL</b></td>
                            <td><b>` + addCommas(delta_pmigorcpo) + `</b></td>
                            <td colspan="2">KGM</td>
                            <td><b>` + addCommas(sum_pmigorcpo) + `</b></td>
                            </tr>
                            <tr>
                            <td colspan ="7"><b></b></td>
                            <td><b>` + addCommas(toFloat(delta_pmigorcpo / 1000)) + `</b></td>
                            <td colspan="2">TNE</td>
                            <td><b>` + addCommas(toFloat(sum_pmigorcpo / 1000)) + `</b></td>
                            </tr>
                            `
                        }
                    }


                    var body_exportir_migor = ''
                    if (result.data['Exportir - Produsen Migor'] !== undefined) {

                        var body_exportir_migor = '<tr><td colspan= "' + (4 + (Object.keys(result.data['Exportir - Produsen Migor'][0]['alokasi']).length * 4)) + '"> <b>Exportir - Produsen Migor</b></td></tr>';

                        var no_e_migor = 1
                        var delta_emigor = 0;
                        var sum_emigor = 0;
                        $.each(result.data['Exportir - Produsen Migor'], function(k, v) {
                            var total_e_migor = 0;
                            var temp_e_migor = 0;
                            var emigor_x_regional = 0;
                            var emigor_x_kemasan = 0;
                            body_exportir_migor += ` <tr>
                            <td>` + no_e_migor + `</td>
                            <td>` + v.nama + `</td>
                            <td>` + v.nib + `</td>
                            <td>` + v.provinsi + `</td>
                            <td>` + v.packaging + `</td>`;
                            $.each(result.data['Exportir - Produsen Migor'][k]['alokasi'], function(kk, vv) {
                                body_exportir_migor += ` <td>` + toFloat(vv.jml_distribusi) + `</td>
                                                <!--td>` + vv.pengali_regional + `</td>
                                                <td>` + vv.pengali_kemasan + `</td>
                                                <td>` + vv.jml + `</td-->`
                                emigor_x_regional = toFloat(vv.pengali_regional);
                                emigor_x_kemasan = toFloat(vv.pengali_kemasan);
                                total_e_migor = toFloat(((vv.jml == '-') ? 0 : vv.jml)) - temp_e_migor
                                temp_e_migor = toFloat(((vv.jml == '-') ? 0 : vv.jml));
                            })
                            body_exportir_migor += `
                        <td>` + total_e_migor + `</td>
                        <td>` + emigor_x_regional + `</td>
                        <td>` + emigor_x_kemasan + `</td>
                        <td>` + (total_e_migor * emigor_x_regional * emigor_x_kemasan) + `</td></tr>`
                            delta_emigor = toFloat(total_e_migor) + toFloat(delta_emigor);
                            sum_emigor = toFloat((total_e_migor * emigor_x_regional * emigor_x_kemasan)) + toFloat(sum_emigor);;
                            no_e_migor = no_e_migor + 1;
                        })

                        body_exportir_migor += `
                    <tr>
                    <td colspan ="7"><b>TOTAL</b></td>
                    <td><b>` + addCommas(delta_emigor) + `</b></td>
                    <td colspan="2">KGM</td>
                    <td><b>` + addCommas(sum_emigor) + `</b></td>
                    </tr>
                    <tr>
                    <td colspan ="7"><b></b></td>
                    <td><b>` + addCommas(toFloat(delta_emigor / 1000)) + `</b></td>
                    <td colspan="2">TNE</td>
                    <td><b>` + addCommas(toFloat(sum_emigor / 1000)) + `</b></td>
                    </tr>
                    `
                    }

                    var body_exportir_cpo = ''
                    if (result.data['Exportir - Produsen CPO'] !== undefined) {

                        var body_exportir_cpo = '<tr><td colspan= "' + (4 + (Object.keys(result.data['Exportir - Produsen CPO'][0]['alokasi']).length * 4)) + '"> <b>Exportir - Produsen CPO</b></td></tr>';

                        var no_e_cpo = 1
                        var delta_ecpo = 0;
                        var sum_ecpo = 0;
                        $.each(result.data['Exportir - Produsen CPO'], function(k, v) {
                            var total_e_cpo = 0;
                            var temp_e_cpo = 0;
                            var ecpo_x_regional = 0;
                            var ecpo_x_kemasan = 0;
                            body_exportir_cpo += ` <tr>
                            <td>` + no_e_cpo + `</td>
                            <td>` + v.nama + `</td>
                            <td>` + v.nib + `</td>
                            <td>` + v.provinsi + `</td>
                            <td>` + v.packaging + `</td>`;
                            $.each(result.data['Exportir - Produsen CPO'][k]['alokasi'], function(kk, vv) {
                                body_exportir_cpo += ` <td>` + toFloat(vv.jml_distribusi) + `</td>
                                                <!--td>` + vv.pengali_regional + `</td>
                                                <td>` + vv.pengali_kemasan + `</td>
                                                <td>` + vv.jml + `</td-->`
                                ecpo_x_regional = toFloat(vv.pengali_regional);
                                ecpo_x_kemasan = toFloat(vv.pengali_kemasan);
                                total_e_cpo = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi)) - temp_e_cpo
                                temp_e_cpo = toFloat(((vv.jml_distribusi == '-') ? 0 : vv.jml_distribusi));
                            })
                            body_exportir_cpo += `
                        <td>` + total_e_cpo + `</td>
                        <td>` + ecpo_x_regional + `</td>
                        <td>` + ecpo_x_kemasan + `</td>
                        <td>` + total_e_cpo + `</td></tr>`
                            delta_ecpo = toFloat(total_e_cpo) + toFloat(delta_ecpo);
                            sum_ecpo = toFloat(total_e_cpo) + toFloat(sum_ecpo);
                            no_e_cpo = no_e_cpo + 1;
                        })
                        body_exportir_cpo += `
                    <tr>
                    <td colspan ="7"><b>TOTAL</b></td>
                    <td><b>` + addCommas(delta_ecpo) + `</b></td>
                    <td colspan="2">KGM</td>
                    <td><b>` + addCommas(sum_ecpo) + `</b></td>
                    </tr>
                    <tr>
                    <td colspan ="7"><b></b></td>
                    <td><b>` + addCommas(toFloat(delta_ecpo / 1000)) + `</b></td>
                    <td colspan="2">TNE</td>
                    <td><b>` + addCommas(toFloat(sum_ecpo / 1000)) + `</b></td>
                    </tr>
                    `
                    }

                    tbl_produsen_cpo = `<table class="table table-bordered m-2" id="tbl_produsen_cpo" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">NIB</th>
                                        <th rowspan="2">Provinsi</th>
                                        <th rowspan="2">Jenis</th>
                                        ` + header + `
                                        <th rowspan="2">Delta</th>
                                        <th colspan="2">` + peraturan + `</th>
                                        <th rowspan="2">Jumlah</th>
                                    </tr>
                                    <tr>
                                    ` + subheader + `
                                    </tr>
                                </thead>
                                <tbody>
                                ` + body_cpo + `
                                </tbody>
                            </table>`;

                    tbl_produsen_migor = `<table class="table table-bordered m-2" id="tbl_produsen_migor" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">NIB</th>
                                        <th rowspan="2">Provinsi</th>
                                        <th rowspan="2">Jenis</th>
                                        ` + header + `
                                        <th rowspan="2">Delta</th>
                                        <th colspan="2">` + peraturan + `</th>
                                        <th rowspan="2">Jumlah</th>
                                    </tr>
                                    <tr>
                                    ` + subheader + `
                                    </tr>
                                </thead>
                                <tbody>
                                ` + (body_migor ) + `
                                </tbody>
                            </table>`;

                    tbl_produsen_migor_cpo = `<table class="table table-bordered m-2" id="tbl_produsen_migor_cpo" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">NIB</th>
                                        <th rowspan="2">Provinsi</th>
                                        <th rowspan="2">Jenis</th>
                                        ` + header + `
                                        <th rowspan="2">Delta</th>
                                        <th colspan="2">` + peraturan + `</th>
                                        <th rowspan="2">Jumlah</th>
                                    </tr>
                                    <tr>
                                    ` + subheader + `
                                    </tr>
                                </thead>
                                <tbody>
                                ` + body_migor_cpo + `
                                </tbody>
                            </table>`;

                    tbl_exportir_cpo = `<table class="table table-bordered m-2" id="tbl_exportir_cpo" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">NIB</th>
                                        <th rowspan="2">Provinsi</th>
                                        <th rowspan="2">Jenis</th>
                                        ` + header + `
                                        <th rowspan="2">Delta</th>
                                        <th colspan="2">` + peraturan + `</th>
                                        <th rowspan="2">Jumlah</th>
                                    </tr>
                                    <tr>
                                    ` + subheader + `
                                    </tr>
                                </thead>
                                <tbody>
                                ` + body_exportir_cpo + `
                                </tbody>
                            </table>`;

                    tbl_exportir_migor = `<table class="table table-bordered m-2" id="tbl_exportir_migor" width="100%">
                                <thead>
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Nama</th>
                                        <th rowspan="2">NIB</th>
                                        <th rowspan="2">Provinsi</th>
                                        <th rowspan="2">Jenis</th>
                                        ` + header + `
                                        <th rowspan="2">Delta</th>
                                        <th colspan="2">` + peraturan + `</th>
                                        <th rowspan="2">Jumlah</th>
                                    </tr>
                                    <tr>
                                    ` + subheader + `
                                    </tr>
                                </thead>
                                <tbody>
                                ` + body_exportir_migor + `
                                </tbody>
                            </table>`;

                    $('#produsen-cpo').html(tbl_produsen_cpo);
                    $('#produsen-migor').html(tbl_produsen_migor);
                    $('#produsen-migor-cpo').html(tbl_produsen_migor_cpo);
                    $('#exportir-cpo').html(tbl_exportir_cpo);
                    $('#exportir-migor').html(tbl_exportir_migor);
                    //$('#isi_table').html(table);

                } else {
                    $('#isi_isi').LoadingOverlay('hide');
                    Swal.fire({
                        title: 'Alokasi gagal ditarik , harap pastikan periode sudah dipilih',
                        icon: 'error',
                        confirmButtonColor: '#E70A2B',
                        confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $('#modal').modal('hide')
                        }
                    })
                }
            },
            complete: function() {
                $('#isi_isi').LoadingOverlay('hide');

                // $('#tbl_detail').DataTable()
            }
        })


        $('#modal').modal('show')



    }





    function showChart(title, data_cpo, data_non_cpo) {
        const ctx = document.getElementById('myChart');
        myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: title,
                datasets: [{
                    label: 'CPO',
                    data: data_cpo,
                    backgroundColor: '#d35400',
                    borderColor: '#d35400',
                    borderWidth: 1
                }, {
                    label: 'NON CPO',
                    data: data_non_cpo,
                    backgroundColor: '#f1c40f',
                    borderColor: '#f1c40f',
                    borderWidth: 1
                }]
            },
            options: {
                plugins: {
                    title: {
                        display: true,
                        text: 'Statistik Pengiriman Alokasi dalam Satuan TON'
                    },
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }


    function showStat(refresh) {
        var title = []
        var data_cpo = []
        var data_non_cpo = []
        var data_stat = ''
        $.ajax({
            "type": 'POST',
            "beforeSend": function() {
                $('#statistik').LoadingOverlay('show')
            },
            "data": {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                refresh: refresh,
                start: 0,
                length: (('<?= ((strtotime(date('Y-m-d')) - strtotime('2022-09-08')) / (60 * 60 * 24)) ?>') > 30 ? 30 : ('<?= ((strtotime(date('Y-m-d')) - strtotime('2022-09-08')) / (60 * 60 * 24)) ?>')),

                periode_awal: $('#periode_awal').val(),
                periode_akhir: $('#periode_akhir').val()
            },
            "url": '<?= base_url() ?>perdagangan/migor/getPeriodeAlokasiSIMIRAH',
            success: function(result) {
                result = JSON.parse(result)

                var no = 1;
                $.each(result.data, function(k, v) {
                    if (v.periode == null) {
                        return
                    }
                    title.push(v.tgl_akhir)
                    data_cpo.push(toFloat(v.jml_cpo / 1000))
                    data_non_cpo.push(toFloat(v.jml_non_cpo / 1000))
                    var action = ''
                    if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1) {
                        action += '<button type="button" class="btn btn-success m-1" onclick="showSummary(\'true\',\'' + v.tgl_awal + '\',\'' + v.tgl_akhir + '\',\'' + v.alokasi_id + '\',\'' + v.status_kirim + '\',\''+(parseFloat(v.jml_cpo)+parseFloat(v.jml_non_cpo))+'\')"><i class="ti ti-list"></i></button>'

                        if (v.status_kirim == 1) {
                            action += '<button type="button" class="btn btn-dark m-1" data-payload=\'' + v.payload + '\' data-body=\'' + v.request_body + '\' data-output=\'' + v.request_output + '\' data-periode=\'' + v.periode + '\' onclick="showPayload(this)"><i class="ti ti-send"></i></button>'
                        }
                    }

                    data_stat += `
                    <tr>
                        <td>` + no + `</td>
                        <td>` + v.periode + `</td>
                        <td>` + v.tgl_akhir + `</td>
                        <td>` + addCommas(v.jml_cpo / 1000) + `</td>
                        <td>` + addCommas(v.jml_non_cpo / 1000) + `</td>
                        <td>TON</td>
                        <td class="text-center">` + action + `</td>
                    </tr>
                    `
                    no++;
                })

            },
            complete: function() {
                $('#statistik').LoadingOverlay('hide')
                showChart(title.reverse(), data_cpo.reverse(), data_non_cpo.reverse())
                $('#body_tbl_stat').html(data_stat)

            }
        })
    }

    function showAlokasiPerusahaan(refresh = null) {





        if ($.fn.DataTable.isDataTable('#tbl_alokasi_perusahaan')) {
            $("#tbl_alokasi_perusahaan").dataTable().fnDestroy();
            $('#tbl_alokasi_perusahaan').empty();
        }
        var table = $('#tbl_alokasi_perusahaan').DataTable({
            dom: 'rtip',
            // buttons:[{
            //     text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
            //     action: function(e, dt, node, config) {
            //         $.ajax({
            //             url: '<?= base_url() ?>web/mirah-export',
            //             type: 'POST',
            //             data: data_post,
            //             success: function(result) {
            //                 window.open('<?= base_url() ?>web/mirah-download', '_blank');
            //             }
            //         })
            //     }
            // }],
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    perusahaan_periode_awal: $('#perusahaan_periode_awal').val(),
                    perusahaan_periode_akhir: $('#perusahaan_periode_akhir').val(),
                    perusahaan_nib: $('#perusahaan_nib').val(),
                    perusahaan_nama: $('#perusahaan_nama').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getAlokasiPerusahaanSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'nib',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'NIB',
                    data: 'nib',
                    orderable: false,

                },

                {
                    title: 'Nama',
                    data: 'nama',
                    orderable: false,

                },
                {
                    title: 'Jumlah CPO',
                    data: 'jml_cpo',
                    orderable: false,

                },
                {
                    title: 'Jumlah NON CPO',
                    data: 'jml_non_cpo',
                    orderable: false,

                },

                {
                    title: 'Aksi',
                    data: 'jml_cpo',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<button type="button" class="btn btn-success m-1" onclick="showPerusahaan(\'' + r.nib + '\')"><i class="ti ti-list"></i></button>'
                    }

                },

            ],
            "scrollX": true,
            "displayLength": 20
        });
    }

    function showPerusahaan(nib) {
        var data_stat = `
                    <tr>
                        <td>NO</td>
                        <td>NIB</td>
                        <td>Nama Perusahaan</td>
                        <td>Jumlah CPO</td>
                        <td>Jumlah NON CPO</td>
                        <td>Satuan</td>
                        <td>Tanggal Akhir</td>
                    </tr>
        `
        $.ajax({
            "type": 'POST',
            "beforeSend": function() {
                $('#modal_perusahaan').LoadingOverlay('show')
                $('#modal_perusahaan').modal('show')
            },
            "data": {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                refresh: 'refresh',
                start: 0,
                length: 100000000,
                perusahaan_nib: nib
            },
            "url": '<?= base_url() ?>perdagangan/migor/getAlokasiPerusahaanSIMIRAH',
            success: function(result) {
                result = JSON.parse(result)

                var no = 1;
                var jml_cpo_total = 0
                var jml_noncpo_total = 0
                $.each(result.data, function(k, v) {

                    data_stat += `
                    <tr>
                        <td>` + no + `</td>
                        <td>` + v.nib + `</td>
                        <td>` + v.nama + `</td>
                        <td>` + (v.jml_cpo / 1000) + `</td>
                        <td>` + (v.jml_non_cpo / 1000) + `</td>
                        <td>TON</td>
                        <td>` + v.tgl_akhir + `</td>
                    </tr>
                    `
                    jml_cpo_total = jml_cpo_total + (v.jml_cpo / 1000)
                    jml_noncpo_total = jml_noncpo_total + (v.jml_non_cpo / 1000)

                    no++;
                })
                data_stat += `
                    <tr>
                        <td colspan="3">TOTAL</td>
                        <td>` + jml_cpo_total + `</td>
                        <td>` + jml_noncpo_total + `</td>
                        <td>TON</td>
                        <td></td>
                    </tr>
                    `


            },
            complete: function() {
                $('#modal_perusahaan').LoadingOverlay('hide')

                $('#tbl_perusahaan').html(data_stat)

            }
        })
    }


    function exportStatAlokasi() {
        $.ajax({
            url: '<?= base_url() ?>web/summary-alokasi-export/stat',
            type: 'POST',
            data: {
                title: 'Data Pengiriman Alokasi DMO ',
                data: $('#tbl_stat').html(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                window.open('<?= base_url() ?>web/mirah-download', '_blank');
            }
        })
    }

    function exportPerusahaan() {
        $.ajax({
            url: '<?= base_url() ?>web/summary-alokasi-export/perusahaan',
            type: 'POST',
            data: {
                title: 'Data Alokasi DMO ',
                data: $('#tbl_perusahaan').html(),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',

            },
            success: function(result) {
                window.open('<?= base_url() ?>web/mirah-download', '_blank');
            }
        })
    }
</script>