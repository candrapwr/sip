<script>
    var selected_proposal_id = ''

    function submitSK(proposal_id, judul, tgl) {
        selected_proposal_id = proposal_id
        $('#sk_tgl_efektif').val(tgl)
        $('#sk_proposal_id').val(proposal_id)
        $('#sk_proposal').html(judul)
        showDokumen('revitalisasi_proposal_pengawasan_1', 'tbl_sk', selected_proposal_id, 'refresh')
        $('#modal_sk').modal('show')
    }

    function showDokumen(jenis, table, proposal_id, refresh = null) {

        if ($.fn.DataTable.isDataTable('#' + table)) {
            $("#" + table).dataTable().fnDestroy();
            $('#' + table).empty();
        }

        var table = $('#' + table).DataTable({
            dom: 'rt',
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    jenis: jenis,
                    proposal_id: proposal_id,
                },
                "url": '<?= base_url() ?>web/revitalisasi/proposal/dokumen/json',
            },
            columns: [{
                    title: 'No',
                    data: 'dokumen_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Dokumen',
                    data: 'judul',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.judul + '<b> ' + r.accept + ' - <strong class="text-danger">*' + r.max_size + 'mb</strong></b>' + '<br><i class="text-muted">' + r.deskripsi + '</i>'
                    }

                },
                {
                    title: 'File',
                    data: 'dokumen_id',
                    orderable: false,
                    render: function(k, v, r) {
                        var accept = r.accept.toString().split(',')
                        var file = ''

                        if (r.file_uploaded === false) {
                            $('#btn_v_review').hide();
                        }

                        if (r.file_uploaded === true) {
                            file = '<a class="btn btn-info" href="' + r.file + '"><i class="ti ti-download me-1"></i></a>'
                            if ($.inArray('jpg', accept) !== -1 || $.inArray('png', accept) !== -1 || $.inArray('jpeg', accept) !== -1) {
                                file = '<img class="img-thumbnail" src="' + r.file + '" style="max-width:200px"></a>'
                            }
                            if ($.inArray('mp4', accept) !== -1 || $.inArray('mov', accept) !== -1) {
                                file = `<video width="200" height="100" controls>
                    <source src="` + r.file + `" type="video/mp4">
                    Your browser does not support the video tag.
                    </video>
                    `
                            }
                        }

                        return file
                    },
                    className: 'align-middle text-center'

                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {
                        var status_color = 'warning'
                        var icon = 'spinner'
                        var keterangan = ''
                        var file_status = 'Perlu Di Unggah'
                        var staf_ditugaskan = ''
                        if (r.file_uploaded) {
                            if (jenis == 'revitalisasi_proposal_pengawasan_1') {
                                sk_valid = true;
                            }
                            if (r.file_status == 'Disetujui') {
                                status_color = 'success'
                                icon = 'check'
                            }

                            if (r.file_status == 'Ditolak') {
                                status_color = 'danger'
                                icon = 'times'
                                keterangan = '<br><i class="text-danger"> ' + r.file_keterangan + '</i>'
                            }
                            if (r.file_status == 'Perlu Verifikasi') {
                                file_status = 'Menunggu di Verifikasi Verifikator 1 - K5'
                            } else if(r.file_status == 'Perlu Verifikasi Verifikator 2 - K5') {
                                file_status = 'Menunggu di Verifikasi Verifikator 2 - K5'
                            }else {
                                file_status = r.file_status
                            }

                            if ((r.staf_ditugaskan != null && r.staf_ditugaskan != '') && ('<?= is_verifikator1_k5_dokumen() ?>' == 1 || '<?= is_verifikator2_k5_dokumen() ?>' == 1)) {
                                staf_ditugaskan = '<br><div class="badge bg-info"><i class="ti ti-info-circle me-1"></i>Ditugaskan ke <b>' + r.staf_ditugaskan_nama + '</b></div>'
                            }

                        }
                        return '<div class="badge bg-' + status_color + '"><i class="ti ti-' + icon + '"></i> ' + file_status + '</div>' + keterangan + staf_ditugaskan
                    },
                    className: 'align-middle text-center'
                },
                {
                    title: 'Aksi',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {

                        var btn_verify = ''
                        var btn_upload = ''
                        if (r.file_uploaded === true) {
                            if (r.file_status != 'Disetujui' && ('<?= is_verifikator1_k5_dokumen() ?>' == 1 || '<?= is_verifikator2_k5_dokumen() ?>' == 1)) {
                                if('<?= is_verifikator1_k5_dokumen() ?>' == 1 && r.file_status == 'Perlu Verifikasi'){
                                    btn_verify = `<button class="btn btn-sm btn-success m-1" type="button" onclick="verifyProposalDokumen('` + r.proposal_dokumen_id + `',true,'Perlu Verifikasi Verifikator 2 - K5')"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                }
                                if('<?= is_verifikator2_k5_dokumen() ?>' == 1 && r.file_status == 'Perlu Verifikasi Verifikator 2 - K5'){
                                    btn_verify = `<button class="btn btn-sm btn-success m-1" type="button" onclick="verifyProposalDokumen('` + r.proposal_dokumen_id + `',true)"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                }
                            }
                        }

                        if (r.file_status != 'Disetujui' && ('<?= is_verifikator1_k5_dokumen() ?>' == 1 || '<?= is_verifikator2_k5_dokumen() ?>' == 1 || '<?= is_dinas() ?>' == 1)) {

                            btn_upload = `
            <button type="button" class="btn btn-dark btn-sm  btn_upload"><i class="ti ti-upload"></i></button>
            <input type="file" accept="` + r.mimes_accepted + `" data-ext="` + r.accept + `" data-dokumen_id="` + r.dokumen_id + `" data-maxsize="` + r.max_size + `" data-proposal_dokumen_id = "` + ((r.proposal_dokumen_id == undefined) ? '' : r.proposal_dokumen_id) + `" class="form-control direct_upload d-none" name="dokumen[` + r.dokumen_id + `]"><br>
            `
                        }



                        return btn_upload + btn_verify
                    },
                    className: 'align-middle text-center'
                },

            ],
            "scrollX": true,
            "displayLength": 20
        });
    }

    $('#frm_sk').on('submit', function(e) {

        if (sk_valid === true) {
            $('#sk_error').hide()
            $('#frm_sk').submit()
        } else {
            e.preventDefault();
            $('#sk_error').show()
            $('#sk_error').html('*Mohon pastikan seluruh form terisi dan dokumen sudah di unggah')
        }
    })


    $('#tbl_sk').on('click', '.btn_upload', function() {
        $(this).siblings('input').trigger('click');

    })

    function verifyProposalDokumen(proposal_dokumen_id, reload = false, isi_status = 'Disetujui') {
        Swal.fire({
            title: 'Verifikasi',
            text: 'Silahkan verifikasi data dokumen proposal ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Setujui',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
                verify('proposal_dokumen', proposal_dokumen_id, isi_status, null, reload)
            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return verify('proposal_dokumen', proposal_dokumen_id, 'Ditolak', reason, reload)
                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }

                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }
        });
    }


    $('#tbl_sk').on('change', '.direct_upload', function() {
        var file = $(this).prop('files')[0];
        var validMimeTypes = ($(this).attr('accept')).toString().split(',');
        var maxFileSizeMB = $(this).data('maxsize'); // Maximum file size in megabytes
        var that = this
        if (file) {
            var fileSizeMB = file.size / (1024 * 1024); // Convert file size to MB
            var fileExtension = file.name.toString().split('.').pop().toLowerCase();

            var is_dwg = false;
            var is_valid_mime = false;
            if ($(this).data('ext') == 'dwg') {
                is_dwg = true;
            }
            if ($(this).data('ext') == fileExtension) {
                is_valid_mime = true;
            } else if ((validMimeTypes.includes(file.type)) === true) {
                is_valid_mime = true;
            }

            if (fileSizeMB > maxFileSizeMB === true || is_valid_mime === false) {
                $(this).val('')
                Swal.fire(
                    'Gagal!',
                    'Mohon pastikan ukuran dan jenis file yang diunggah sesuai',
                    'error'
                )
            } else {
                var formData = new FormData();

                formData.append('proposal_id', selected_proposal_id);
                formData.append('dokumen_id', $(this).data('dokumen_id'));
                formData.append('proposal_dokumen_id', $(this).data('proposal_dokumen_id'));
                formData.append('file', file);
                formData.append('csrf_baseben', '<?= $this->security->get_csrf_hash() ?>');
                $.ajax({
                    url: '<?= base_url() ?>web/revitalisasi/proposal/dokumen',
                    type: 'POST',
                    beforeSend: function() {
                        $(that).parent().prevAll().first().find('div').eq(0).removeClass('bg-warning');
                        $(that).parent().prevAll().first().find('div').eq(0).addClass('bg-info');
                        $(that).parent().prevAll().first().find('div').eq(0).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Uploading..')
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        // Upload progress
                        xhr.upload.addEventListener('progress', function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                //$(that).parent().parent().find('.status_file').eq(0).html('<div class="progress"><div class="progress-bar" style="width: ' + percentComplete + '%"> Processing</div></div>')

                            }
                        }, false);
                        return xhr;
                    },
                    success: function(response) {
                        after_update()
                    }
                });
            }
        }
    })

    function verify(jenis, id, status, keterangan = null, reload) {
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/submit',
            type: 'POST',
            data: {
                jenis: jenis,
                proposal_dokumen_id: ((jenis == 'proposal_dokumen') ? id : ''),
                proposal_id: ((jenis == 'proposal') ? id : ''),
                status: status,
                keterangan: keterangan,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                Swal.fire(
                    'Sukses!',
                    'Proposal Dokumen berhasil ' + status,
                    'success'
                )
                if (reload === true) {
                    location.reload();
                } else {
                    after_update()
                }

            }
        })
    }
</script>