<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    var selected_pasar_id = ''

    $(document).ready(function() {
        showTable('refresh');
        var form = $("#form_general");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                var thiselement = element
                if ($(element).attr('id').includes('selectized') === true) {
                    thiselement = $(element).parent().parent().parent().find('select:eq(0)')
                }
                error.insertBefore(thiselement);
            },
            errorClass: "error is-invalid",
            rules: {

            },
            submitHandler: function(form) {

                var total_digunakan = 0
                $.each(<?=json_encode($proposal_lelang_id_pagu)?>,function(k,v){
                    if(v != $('#proposal_lelang_id').val()){
                        total_digunakan =total_digunakan+ parseFloat(<?=json_encode($nilai_pagu)?>[k])
                    }
                })

                if ((parseFloat(total_digunakan) + parseFloat($('#pagu').val())) > parseInt(<?= $proposal['anggaran_ditetapkan'] ?>)) {
                    Swal.fire(
                        'Gagal!',
                        'Total Pagu melebihi anggaran yang ditetapkan',
                        'error'
                    )
                    return false
                } else if (($('input[name*="tahapan["]').filter((i, el) => el.value.trim() == '').length == 0) === false) {
                    $.each($('input[name*="tahapan["]'), function(k, v) {
                        $(v).eq(0).addClass('error is-invalid')
                    })
                    return false
                } else {
                    form.submit();
                }
            },
            messages: {},
            invalidHandler: function(form, validator) {

            }
        });


    })




    function add() {
        showTahapanPengadaan(proposal_lelang_id = null)
        $('#tipe_pengadaan').val('')
        $('#tahun_anggaran').val('<?= $proposal['tahun_anggaran'] ?>')
        $('#kode').val('')
        $('#tgl').val('')
        $('#judul').val('Lelang <?= $proposal['judul'] ?>')
        $('#lingkup').text('')
        $('#instansi').val('<?= $proposal['nama_dinas'] ?>')
        $('#hps').val('')
        $('#pagu').val('')
        $('#proposal_lelang_id').val('')
        $('#last_file').html('')
        $('#dokumen').prop('required', true)
        $('#modal_general').modal('show')
    }

    function edit(proposal_lelang_id) {
        showTahapanPengadaan(proposal_lelang_id)
        $('#modal_general').modal('show')
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/lelang/json',
            type: 'POST',
            data: {
                proposal_lelang_id: proposal_lelang_id,
                start: 0,
                length: 1,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {},
            success: function(response) {
                data = (JSON.parse(response)).data[0]
                $('#tipe_pengadaan').val(data.tipe_pengadaan)
                $('#tahun_anggaran').val(data.tahun)
                $('#kode').val(data.kode)
                $('#tgl').val(data.tgl)
                $('#judul').val(data.judul)
                $('#lingkup').text(data.lingkup)
                $('#instansi').val(data.instansi)
                $('#hps').val(data.hps)
                $('#pagu').val(data.pagu)
                if (data.dokumen != '' && data.dokumen != null) {
                    $('#dokumen').prop('required', false)
                    $('#last_file').html('<a class="btn btn-primary btn-sm" href="<?= base_url() ?>download/revitalisasi/proposal/lelang/' + data.dokumen + '"><i class="ti ti-file me-1"></i>File Sebelumnya</a>')
                }

                $('#proposal_lelang_id').val(data.proposal_lelang_id)

            },
            complete: function() {

            }
        })
    }


    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rt',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    proposal_id: '<?= $proposal_id ?>',

                },
                "url": '<?= base_url() ?>web/revitalisasi/lelang/json',
            },
            columns: [{
                    title: 'No',
                    data: 'proposal_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Tipe Lelang',
                    data: 'tipe_pengadaan',
                    orderable: false,
                    render: function(k, v, r) {
                        var color = 'warning'
                        var icon = 'spinner'
                        var status = r.status
                        var keterangan = ''
                        if (r.status == 'Disetujui' && r.status_tahapan == 'Selesai') {
                            color = 'success'
                            icon = 'check'
                        } else if (r.status == 'Ditolak') {
                            color = 'danger'
                            icon = 'times'
                            keterangan = '<br><i class="text-danger" style="font-size:12px">' + r.ket + '</i>'
                        }

                        if (status == 'Perlu Verifikasi') {
                            status = 'Menunggu Di Verifikasi Verifikator 1 - K5'
                        } else if (status == 'Perlu Verifikasi Verifikator 2 - K5') {
                            status = 'Menunggu Di Verifikasi Verifikator 2 - K5'
                        } else if (status == 'Disetujui' && r.status_tahapan == 'Belum') {
                            status = 'Penyelesaian Lelang'
                        } else if (status == 'Disetujui' && r.status_tahapan == 'Selesai') {
                            status = 'Selesai'
                        }

                        return r.tipe_pengadaan + '<br><div class="badge bg-' + color + '"><i class="ti ti-' + icon + '"></i> ' + status + '</div>' + keterangan
                    }

                },
                {
                    title: 'Kode',
                    data: 'kode',
                    orderable: false

                },
                {
                    title: 'Judul',
                    data: 'judul',
                    orderable: false
                },
                {
                    title: 'Tanggal',
                    data: 'tgl',
                    orderable: false,
                },
                {
                    title: 'HPS & Pagu',
                    data: 'hps',
                    orderable: false,
                    render: function(k, v, r) {
                        return 'Rp. ' + addCommas(r.hps) + '<br>' + 'Rp. ' + addCommas(r.pagu)
                    }

                },
                {
                    title: 'Aksi',
                    data: 'proposal_lelang_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'

                        var btn_delete = ''
                        var btn_edit = ''
                        var btn_verify = ''
                        var btn_selesaikan_lelang = ''
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'delete') : false) ?>' == 1 && r.status != 'Disetujui') {
                            btn_delete = '<button class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.proposal_lelang_id + '\',\'Deleted\',\'tbl_proposal_lelang\',\'proposal_lelang_id\',\'Hapus proposal lelang `' + ($.trim(r.tipe_pengadaan.replace(/[\t\n]+/g, ''))) + '`\',\'revitalisasi\')"> <i class="ti ti-trash"></i></button><br>'
                        }
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1) {

                            if (r.status != 'Disetujui') {
                                btn_edit = '<button class="btn btn-sm btn-warning m-1" onclick="edit(\'' + r.proposal_lelang_id + '\')"><i class="ti ti-pencil"></i></button>'
                            }

                            if ('<?= is_verifikator1_lelang() ?>' == 1 || '<?= is_verifikator2_lelang() ?>' == 1) {
                                if('<?= is_verifikator1_lelang() ?>' == 1 && r.status == 'Perlu Verifikasi'){
                                    btn_verify = `<button class="btn btn-sm btn-success m-1" onclick="verifyLelang('` + r.proposal_lelang_id + `',true,'Perlu Verifikasi Verifikator 2 - K5')"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                }
                                if('<?= is_verifikator2_lelang() ?>' == 1 && r.status == 'Perlu Verifikasi Verifikator 2 - K5'){
                                    btn_verify = `<button class="btn btn-sm btn-success m-1" onclick="verifyLelang('` + r.proposal_lelang_id + `',true)"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                }

                            }

                            if (r.status == 'Disetujui' && r.status_tahapan == 'Belum' && '<?= is_dinas() ?>' == 1) {
                                btn_selesaikan_lelang = `<button class="btn btn-sm btn-warning m-1" onclick="selesaikanLelang('` + r.proposal_lelang_id + `')"><i class="ti ti-edit"></i> Selesaikan</button>`
                            }

                        }

                        return btn_edit + btn_delete + btn_selesaikan_lelang + btn_verify

                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function verifyLelang(proposal_lelang_id, reload = false, isi_status = 'Disetujui') {
        Swal.fire({
            title: 'Verifikasi',
            text: 'Silahkan verifikasi data lelang ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Setujui',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
                update(proposal_lelang_id, isi_status, 'tbl_proposal_lelang', 'proposal_lelang_id', 'Setujui Lelang', 'revitalisasi')
                after_update()
            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return update(proposal_lelang_id, 'Ditolak', 'tbl_proposal_lelang', 'proposal_lelang_id', 'Penolakan Lelang', 'revitalisasi', reason)
                            after_update()
                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }

        });
    }


    function selesaikanLelang(proposal_lelang_id) {
        showTahapanPengadaanPenyelesaian(proposal_lelang_id)
        $('#modal_tahapan_lelang').modal('show')
    }

    function selesaiLelang(proposal_lelang_tahapan_id, proposal_lelang_id) {
        $('#modal_tahapan_lelang').modal('hide')
        Swal.fire({
            title: 'Selesaikan / Gagalkan Lelang',
            text: 'Silahkan selesaikan atau gagalkan data lelang ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Selesai Lelang',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Gagal Lelang',
        }).then((result) => {
            if (result.isConfirmed) {
                update(proposal_lelang_tahapan_id, 'Selesai', 'tbl_proposal_lelang_tahapan', 'proposal_lelang_tahapan_id', 'Selesaikan Tahapan Lelang', 'revitalisasi')
                selesaikanLelang(proposal_lelang_id)
            } else if (result.dismiss === Swal.DismissReason.deny) {

                Swal.fire({
                    title: 'Alasan Gagal Lelang',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan gagal lelang',
                    inputPlaceholder: 'Mohon masukan alasan gagal lelang...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            selesaikanLelang(proposal_lelang_id)
                            return update(proposal_lelang_tahapan_id, 'Gagal', 'tbl_proposal_lelang_tahapan', 'proposal_lelang_tahapan_id', 'Gagal Lelang', 'revitalisasi', reason)

                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }

        });
    }

    function showTahapanPengadaanPenyelesaian(proposal_lelang_id = null) {

        if ($.fn.DataTable.isDataTable('#tbl_tahapan_lelang')) {
            $("#tbl_tahapan_lelang").dataTable().fnDestroy();
            $('#tbl_tahapan_lelang').empty();
        }

        var table = $('#tbl_tahapan_lelang').DataTable({
            dom: 'rt',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    proposal_lelang_id: proposal_lelang_id,

                },
                "url": '<?= base_url() ?>web/revitalisasi/lelang/tahapan/json',
            },
            columns: [{
                    title: 'No',
                    data: 'lelang_tahapan_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Tahapan',
                    data: 'tahapan',
                    orderable: false,
                    render: function(k, v, r) {
                        var color = 'warning'
                        var keterangan = ''
                        if (r.proposal_lelang_status == 'Selesai') {
                            color = 'success'
                        }
                        if (r.proposal_lelang_status == 'Gagal') {
                            color = 'danger'
                            keterangan = '<br><i class="text-danger" style="font-size:12px">'+r.ket+'</i>'
                        }



                        return r.tahapan + '<br><div class="badge bg-' + color + '">' + r.proposal_lelang_status + '</div>'+keterangan
                    }

                },
                {
                    title: 'Tanggal',
                    data: 'tahapan',
                    orderable: false,
                    render: function(k, v, r) {

                        return formatTimestamp(r.tgl)
                    }

                },
                {
                    title: 'Aksi',
                    data: 'lelang_tahapan_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var btn_verify = ''
                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1 && '<?= is_dinas() ?>' == 1 && r.proposal_lelang_status != 'Selesai') {
                            btn_verify = `<button class="btn btn-sm btn-warning m-1" onclick="selesaiLelang('` + r.proposal_lelang_tahapan_id + `','` + proposal_lelang_id + `')"><i class="ti ti-search me-1"></i>Selesaikan / Gagal</button>`
                        }

                        return btn_verify
                    }
                }

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }


    function showTahapanPengadaan(proposal_lelang_id = null) {

        if ($.fn.DataTable.isDataTable('#tbl_tahapan_pengadaan')) {
            $("#tbl_tahapan_pengadaan").dataTable().fnDestroy();
            $('#tbl_tahapan_pengadaan').empty();
        }

        var table = $('#tbl_tahapan_pengadaan').DataTable({
            dom: 'rt',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    proposal_lelang_id: proposal_lelang_id,

                },
                "url": '<?= base_url() ?>web/revitalisasi/lelang/tahapan/json',
            },
            columns: [{
                    title: 'No',
                    data: 'lelang_tahapan_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Tahapan',
                    data: 'tahapan',
                    orderable: false

                },
                {
                    title: 'Tanggal',
                    data: 'tahapan',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<div class="form-group">
                                    <input type="date" class="form-control" name="tahapan[` + r.lelang_tahapan_id + `]" value="` + ((r.is_filled === true) ? r.tgl : '') + `">
                                    <input type="hidden" class="form-control" name="proposal_lelang_tahapan_id[` + r.lelang_tahapan_id + `]" value="` + ((r.is_filled === true) ? r.proposal_lelang_tahapan_id : '') + `">
                                </div>`
                    }

                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }


    function after_update() {
       location.reload()
    }
</script>