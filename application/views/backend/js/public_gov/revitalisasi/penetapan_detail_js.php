<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    $(document).ready(function() {
        showTable('refresh');
        $('#provinsi-name').selectize()
        $('#kabkota-name').selectize()
        $('#f_pasar_id').selectize()
        getDaerah('provinsi', 1);
        getPasar('f_pasar_id', $('#provinsi-name').val());
    })

    function showTable(refresh = null) {

        var is_readonly = ''
        if ('<?= $penetapan['status'] ?>' == 'Ditetapkan') {
            is_readonly = 'disabled'
        }
        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    pasar_id: $('#f_pasar_id').val(),
                    penetapan_id: '<?= $penetapan_id ?>',
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>web/revitalisasi/penetapan/proposal',
            },
            columns: [{
                    title: 'No',
                    data: 'proposal_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pasar',
                    data: 'judul',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.nama_pasar + ' - <b>' + r.tipe_pasar + '</b> <br>' + r.kab_kota_pasar + ', ' + r.provinsi_pasar + '<br><b class="text-warning"><i class="fa fa-pen"></i>' + r.no_permendag + '</b>'
                    }

                },
                {
                    title: 'Proposal',
                    data: 'judul',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.judul + '<br>Anggaran : ' + addCommas(r.anggaran)
                    }
                },
                {
                    title: 'Tipe Pasar',
                    data: 'judul',
                    orderable: false,
                    render: function(k, v, r) {
                        var tipe_pasar = ''
                        $.each(JSON.parse('<?= $tipe_pasar ?>'), function(k, v) {
                            var selected = ''
                            if (r.is_ditetapkan === false && r.tipe_pasar_id == v.tipe_pasar_id) {
                                selected = 'selected'
                            }

                            if (r.is_ditetapkan === true) {
                                if (r.penetapan_tipe_pasar_id == v.tipe_pasar_id) {
                                    selected = 'selected'
                                }
                                if (r.no_permendag == '<?= $penetapan['no_permendag'] ?>' || r.no_permendag != null) {
                                    is_readonly = 'disabled'
                                }

                            }

                            tipe_pasar += '<option value="' + v.tipe_pasar_id + '" ' + selected +'>' + v.tipe_pasar + '</option>'
                        })

                        return `
                        <select ` + is_readonly + ` class="form-control" name="tipe_pasar_id[` + r.proposal_id + `][]">
                            ` + tipe_pasar + `
                        </select>
                        `
                    }
                },
                {
                    title: 'Anggaran',
                    data: 'anggaran',
                    orderable: false,
                    render: function(k, v, r) {
                        var anggaran = r.anggaran
                        if (r.is_ditetapkan === true) {
                            anggaran = r.penetapan_anggaran
                            if (r.no_permendag == '<?= $penetapan['no_permendag'] ?>' || r.no_permendag != null) {
                                is_readonly = 'disabled'
                            }
                        }


                        return '<input ' + is_readonly + ' type="number" class="form-control" step="any" name="anggaran[' + r.proposal_id + '][]" value="' + anggaran + '" >'
                    }
                },
                {
                    title: 'Tetapkan',
                    data: 'judul',
                    orderable: false,
                    render: function(k, v, r) {
                        var checked = ''
                        var penetapan_proposal_id = ''
                
                        if (r.is_ditetapkan === true) {
                            checked = 'checked'
                            penetapan_proposal_id = r.penetapan_proposal_id
                            if (r.no_permendag == '<?= $penetapan['no_permendag'] ?>' || r.no_permendag != null) {
                                is_readonly = 'disabled'
                            }

                        }


                        return `
                        <div class="checkbox checkbox-primary checkbox-lg">
                        <input ` + is_readonly + ` name="tetapkan[` + r.proposal_id + `]" data-penetapan_proposal_id ="` + penetapan_proposal_id + `" data-proposal_id="` + r.proposal_id + `" onclick="checkPenetapan(this)" value="Ditetapkan" type="checkbox" ` + checked + ` >
                        </div>
                        `
                    },
                    className: 'text-center align-middle'
                },



            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    $('#provinsi-name').on('change', function() {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)
        getPasar('f_pasar_id', daerah_id);
        showTable('refresh')

    })

    $('#kabkota-name').on('change', function() {
        var daerah_id = $("#kabkota-name").val();
        getPasar('f_pasar_id', daerah_id);
        showTable('refresh')
    })

    $('#f_pasar_id, #f_status').on('change', function() {
        showTable('refresh')
    })

    function checkPenetapan(that) {

        var tipe_pasar_id = $(that).parent().parent().prevAll().eq(1).find('select').eq(0).val()
        var anggaran = $(that).parent().parent().prevAll().first().find('input').eq(0).val()
        var is_ditetapkan = false
        if (($(that).prop('checked')) === true) {
            is_ditetapkan = true
        }
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/penetapan/tetapkan-proposal',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                proposal_id: $(that).data('proposal_id'),
                penetapan_id: '<?= $penetapan_id ?>',
                tipe_pasar_id: tipe_pasar_id,
                anggaran: anggaran,
                is_ditetapkan: is_ditetapkan,
                penetapan_proposal_id: $(that).data('penetapan_proposal_id')

            },
            beforeSend: function() {},
            success: function(response) {
                showTable('refresh')
            }
        })
    }

    function getPasar(id, daerah_id) {
        var pasar = '';
        pasar = '<option value="">- Pilih Pasar -</option>';

        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/penetapan/pasar',
            type: 'POST',
            data: {
                kode_daerah: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id + '').parent().LoadingOverlay('show')
                $('#' + id + '').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';
                $.each(response, function(k, v) {
                    pasar += '<option value="' + v.pasar_id + '">' + v.nama_pasar + '</option>';
                })
            },
            complete: function() {
                $('#' + id + '').html(pasar);
                $('#' + id + '').selectize()
                $('#' + id + '').parent().LoadingOverlay('hide')

            }
        })
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function() {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()
            }
        })
    }
    var penetapan = 0

    function tetapkanProposal(status) {
        penetapan = 1
        Swal.fire({
            title: ((status === true) ? 'Tetapkan Proposal?' : 'Batalkan Ketetapan?'),
            text: ((status === true) ? 'Setelah ditetapkan maka seluruh proposal yang ditetapkan akan dapat melanjutkan ke tahap selanjutnya' : 'Penetapan akan dibatalkan dan dapat diubah kembali'),
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: ((status === true) ? 'Tetapkan' : 'Batalkan Ketetapan'),
            cancelButtonText: 'Batal',
        }).then((result) => {
            if (result.isConfirmed) {
                if (status === true) {
                    update('<?= $penetapan_id ?>', 'Ditetapkan', 'tbl_penetapan', 'penetapan_id', 'Menetapkan permendag', 'revitalisasi')
                } else {
                    update('<?= $penetapan_id ?>', 'Draft', 'tbl_penetapan', 'penetapan_id', 'Membatalkan penetapan permendag', 'revitalisasi')
                }

            }
        })
    }

    function after_update() {
        if (penetapan == 1) {
            penetapan = 0
            location.reload();
        } else {
            showTable('refresh')
        }

    }
</script>