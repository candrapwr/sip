<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    var selected_pasar_id = ''

    $(document).ready(function() {
        var form = $("#proposal_form");
        $('#platform-name, #provinsi-name, #kabkota-name,#f_pasar_id').selectize()
        getDaerah('provinsi', 1);
        getPasar('pasar_id', $('#provinsi-name').val());
        showTable('refresh');
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                var thiselement = element
                if ($(element).attr('id').includes('selectized') === true) {
                    thiselement = $(element).parent().parent().parent().find('select:eq(0)')
                }
                error.insertBefore(thiselement);
            },
            errorClass: "error is-invalid",
            rules: {
                pasar_id: 'required'
            },
            messages: {},
            invalidHandler: function(form, validator) {

            }
        });


        $('#wizard').steps({
            headerTag: "h2",
            selected: 1,
            bodyTag: "section",
            transitionEffect: "slideLeft",
            stepsOrientation: 'vertical',
            startIndex: 0,
            onInit: function(event, currentIndex, priorIndex) {},
            onStepChanging: function(event, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,#wizard-p-1 :hidden,#wizard-p-2 :hidden";
                if (newIndex > currentIndex) {
                    if (form.valid() == true) {
                        if (newIndex == 2) {
                            saveProposal('wizard-p-1', 'wizard-p-0')
                            getProposalDokumen('revitalisasi_proposal_dokumen_1', 'wizard-p-2')
                        }
                        if (newIndex == 3) {
                            if (parseInt($('#wizard-t-2').find('strong').eq(0).find('div').eq(0).text()) > 0) {
                                return false;
                            } else {
                                getProposalDokumen('revitalisasi_proposal_foto_pasar_1', 'wizard-p-3')
                            }
                        }
                        return true;
                    } else {
                        return false;
                    }
                }
                return true
            },
            onStepChanged: function(event, currentIndex, priorIndex) {},
            onFinishing: function(event, currentIndex) {
                if (parseInt($('#wizard-t-3').find('strong').eq(0).find('div').eq(0).text()) > 0) {
                    return false;
                }
                return true;
            },
            onFinished: function(event, currentIndex) {
                submitProposal('proposal', 'Diajukan', '', $('#proposal_id').val())
                $('#modal_proposal').LoadingOverlay('show')
                return true;
            }
        });
    })





    function saveProposal(id, id_second = null) {

        if (id_second != null) {
            var formdata = $('#' + id + ' :input,#' + id_second + ' :input ').serialize();
        } else {
            var formdata = $('#' + id + ' :input').serialize();
        }
        formdata = formdata + '&' + $.param({
            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
        })
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal',
            type: 'POST',
            data: formdata,
            beforeSend: function() {
                $('#wizard-t-' + (id.toString().substr(-1))).LoadingOverlay('show');
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#wizard #proposal_id').val(response.data.proposal_id)
            },
            complete: function() {
                $('#wizard-t-' + (id.toString().substr(-1))).LoadingOverlay('hide');
            }
        })
    }

    function resetSteps() {
        for (i = 0; i <= $('#wizard').steps('getCurrentIndex'); i++) {
            $('#wizard').steps('previous')
        }
    }

    function add() {
        resetSteps()
        if ($('#pasar_id').selectize()[0].selectize) {
            $('#pasar_id').selectize()[0].selectize.setValue('')
        } else {
            $('#pasar_id').val('')
        }
        $('#proposal_id').val('')
        getProposalDokumen('revitalisasi_proposal_dokumen_1', 'wizard-p-2')
        getProposalDokumen('revitalisasi_proposal_foto_pasar_1', 'wizard-p-3')
        $('#nama_pasar').val('')

        $('#tgl').val('<?= date('Y-m-d') ?>')
        $('#judul').val('')
        $('#latar_belakang').val('')
        $('#maksud_tujuan').val('')
        $('#pengusul').val('<?= $this->session->userdata('nama') ?>')
        $('#tingkat_pengusul').val('<?= ((in_array($this->session->userdata('role'), ['Dinas Provinsi', 'Kontributor SP2KP (Provinsi)'])) ? 'Provinsi' : 'Kabupaten/Kota') ?>')
        $('#anggaran').val('')
        $('#kode_satker').val('')
        $('#kode_lokasi').val('')
        $('#kode_kppn').val('')
        $('#nama_dinas').val('<?= ((in_array($this->session->userdata('role'), ['Dinas Provinsi', 'Kontributor SP2KP (Provinsi)'])) ? 'Dinas Provinsi ' . $this->session->userdata('provinsi') : 'Dinas Kabupaten/Kota ' . $this->session->userdata('kab_kota')) ?>')
        $('#modal_proposal').modal('show')
    }

    function edit(proposal_id, pasar_id) {
        resetSteps()
        selected_pasar_id = pasar_id
        $('#wizard #pasar_id').selectize()[0].selectize.setValue(pasar_id)
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/json',
            type: 'POST',
            data: {
                proposal_id: proposal_id,
                start: 0,
                length: 1,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#wizard-p-1').LoadingOverlay('show')
            },
            success: function(response) {
                data = (JSON.parse(response)).data[0]
                $('#proposal_id').val(data.proposal_id)
                $('#tgl').val(data.tgl)
                $('#judul').val(data.judul)
                $('#latar_belakang').val(data.latar_belakang)
                $('#maksud_tujuan').val(data.maksud_tujuan)
                $('#pengusul').val(data.pengusul)
                $('#tingkat_pengusul').val(data.tingkat_pengusul)
                $('#anggaran').val(data.anggaran)
                $('#kode_satker').val(data.kode_satker)
                $('#kode_lokasi').val(data.kode_lokasi)
                $('#kode_kppn').val(data.kode_kppn)
                $('#nama_dinas').val(data.nama_dinas)
            },
            complete: function() {
                getProposalDokumen('revitalisasi_proposal_dokumen_1', 'wizard-p-2')
                getProposalDokumen('revitalisasi_proposal_foto_pasar_1', 'wizard-p-3')
                $('#wizard-p-1').LoadingOverlay('hide')

            }
        })
        $('#modal_proposal').modal('show')
    }

    function fill() {
        showTable('refresh');
    }

    $('#provinsi-name').on('change', function() {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)
        getPasar('f_pasar_id', daerah_id);

    })

    $('#kabkota-name').on('change', function() {
        var daerah_id = $("#kabkota-name").val();
        getPasar('f_pasar_id', daerah_id);

    })

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    pasar_id: $('#f_pasar_id').val(),
                    status: $('#f_status').val(),
                    tahun_anggaran: $('#f_tahun_anggaran').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>web/revitalisasi/proposal/json',
            },
            columns: [{
                    title: 'No',
                    data: 'proposal_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Judul',
                    data: 'judul',
                    orderable: false,
                    render: (k, v, r) => `<h6 class="text-primary mb-0">"${r.judul}"</h6><br><i class="ti ti-building-store me-1"></i>${r.nama_pasar}<br><i class="ti ti-map-pin me-1"></i>${r.kab_kota_pasar}, Provinsi ${r.provinsi_pasar}`
                },
                {
                    title: 'Anggaran',
                    data: 'tahun_anggaran',
                    orderable: false,
                    render: (k, v, r) => {
                        const anggaran_proposal = `<ul>
                            <li>Proposal : Rp. ${addCommas(r.anggaran)}</li>
                        </ul>`;

                        let anggaran_ditetapkan = '';

                        if (r.status_penetapan == 'Ditetapkan') {
                            anggaran_ditetapkan = `<hr><ul>
                                <li>Ditetapkan oleh <b>"${r.no_permendag}"</b></li>
                                <li>Sebanyak Rp. ${addCommas(r.anggaran_ditetapkan)}</li>
                            </ul>`;
                        }

                        return anggaran_proposal + anggaran_ditetapkan;
                    }
                },

                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        let status_color = 'warning';
                        let icon = 'spinner';
                        let proposal_ditolak = '';
                        let status = r.status;
                        let status_dokumen_reject = '';
                        let btn_pengawasan = '';

                        if (r.status == 'Diundang Review') {
                            status_color = 'success';
                            icon = 'check';
                        } else if (r.status == 'Ditolak') {
                            status_color = 'danger';
                            icon = 'times';
                            proposal_ditolak = `<br><i class="text-danger">${r.keterangan}</i>`;
                        }

                        if (r.status == 'Selesai Review' && r.status_penetapan == 'Ditetapkan') {
                            status = r.status_penetapan;
                            status_color = 'success';
                            icon = 'check-double';

                            if (r.stat_sk_operator == 'OK') {
                                status = 'Proses Lelang';
                                status_color = 'warning';
                                icon = 'spinner';

                                if (r.status_lelang == 'OK') {
                                    status = 'Proses Kontrak';
                                }
                                if (r.status_kontrak != null) {
                                    if (r.status_kontrak.toString().indexOf('Pelaksanaan Disetujui') !== -1 && r.status_kontrak.toString().indexOf('Pengawasan Disetujui') !== -1) {
                                        status = 'Proses Jadwal'
                                        if (r.status_jadwal == 'OK') {
                                            status = 'Proses Pantauan Fisik'
                                        }
                                    }
                                }

                            } else {
                                status_color = 'warning';
                                icon = 'spinner';
                                status = 'Unggah SK Operator';
                            }
                        }

                        if (r.status == 'Selesai Review' && r.status_penetapan == '') {
                            status_color = 'danger';
                            icon = 'times';
                            status = 'Tidak Ditetapkan';
                        }

                        if (r.jml_dokumen_reject > 0 && r.status == 'Diajukan') {
                            status_dokumen_reject = `<br><i class="text-danger"><div class="badge bg-danger"><i class="ti ti-alert-triangle me-1"></i>Dokumen Revisi</div> <br>${r.dokumen_reject.toString().split(',').join('<br>')}</i>`;
                        }

                        return `<div class="badge bg-${status_color}"><i class="ti ti-${icon}"></i> ${status}</div>${proposal_ditolak}${status_dokumen_reject}<br>${btn_pengawasan}`;
                    },
                    className: 'align-middle text-center'
                },
                {
                    title: 'Aksi',
                    data: 'status',
                    orderable: false,
                    width: '15%',
                    render: (k, v, r) => {
                        let btn_detail = `<a href="<?= base_url() ?>web/revitalisasi/proposal/${r.uuid}" class="dropdown-item"><i class="ti ti-list me-1"></i>Proposal</a>`;
                        let btn_edit = '';
                        let btn_delete = '';
                        let btn_pengawasan = '';
                        let btn_sk = '';
                        let btn_lelang = '';
                        let btn_kontrak = '';
                        let btn_jadwal = '';
                        let btn_p_fisik = '';
                        let btn_p_anggaran = '';

                        if ($.inArray(r.status, ['Draft']) !== -1) {
                            btn_detail = '';
                        }
                        if ($.inArray(r.status, ['Draft', 'Diajukan']) !== -1 && '<?= is_verifikator_proposal() ?>' != 1) {
                            btn_detail = '';
                        }

                        if (r.status == 'Selesai Review' && r.status_penetapan == 'Ditetapkan') {
                            btn_sk = `
                                    <button class="dropdown-item text-warning" type="button" onclick="submitSK('${r.proposal_id}','${r.judul}, ${r.kab_kota_pasar}, ${r.provinsi_pasar}','${r.tgl}')">
                                        <i class="ti ti-edit me-1"></i>SK Operator
                                    </button>`;

                            if (r.stat_sk_operator == 'OK') {
                                btn_sk = `
                                        <button class="dropdown-item text-success" type="button" onclick="submitSK('${r.proposal_id}','${r.judul}, ${r.kab_kota_pasar}, ${r.provinsi_pasar}','${r.tgl}')">
                                            <i class="ti ti-circle-check me-1"></i>SK Operator
                                        </button>`;
                                btn_lelang = `<a href="<?= base_url() ?>web/revitalisasi/lelang/${r.uuid}" class="dropdown-item text-warning" type="button"><i class="ti ti-edit me-1"></i>Lelang</a>`;
                            }
                            if (r.status_lelang == 'OK') {
                                btn_kontrak = `<a href="<?= base_url() ?>web/revitalisasi/kontrak/${r.uuid}" class="dropdown-item text-warning" type="button"><i class="ti ti-edit me-1"></i>Kontrak</a>`;
                                btn_lelang = `<a href="<?= base_url() ?>web/revitalisasi/lelang/${r.uuid}" class="dropdown-item text-success" type="button"><i class="ti ti-circle-check me-1"></i>Lelang</a>`;
                            }
                            if (r.status_kontrak != null) {
                                if (r.status_kontrak.toString().indexOf('Pelaksanaan Disetujui') !== -1 && r.status_kontrak.toString().indexOf('Pengawasan Disetujui') !== -1) {
                                    btn_jadwal = '<a class="dropdown-item text-warning" href="<?= base_url() ?>web/revitalisasi/jadwal/' + r.uuid + '"><i class="mdi mdi-circle-edit-outline"></i> Jadwal</a>'
                                    btn_kontrak = '<a href="<?= base_url() ?>web/revitalisasi/kontrak/' + r.uuid + '" class="dropdown-item text-success" type="button"><i class="mdi mdi-check-outline"></i> Kontrak</a>'


                                    if (r.status_jadwal != null) {

                                        if (r.status_jadwal == 'OK') {
                                            btn_jadwal = '<a class="dropdown-item text-success" href="<?= base_url() ?>web/revitalisasi/jadwal/' + r.uuid + '" ><i class="mdi mdi-check-outline"></i> Jadwal</a>'
                                            btn_p_fisik = '<a class="dropdown-item text-warning" href="<?= base_url() ?>web/revitalisasi/pantauan_fisik/' + r.uuid + '" type="button"><i class="mdi mdi-circle-edit-outline"></i> P. Fisik</a>'
                                        }
                                        if (1 == 'OK') {
                                            btn_p_anggaran = '<a class="dropdown-item" type="button"><i class="mdi mdi-circle-edit-outline"></i> P. Anggaran</a>'
                                        }
                                    }
                                }
                            }
                            btn_pengawasan = `${btn_sk}${btn_lelang}${btn_kontrak}${btn_jadwal}${btn_p_fisik}${btn_p_anggaran}`;
                        }

                        if ('<?= (($menu_id !== '')) ? has_access($menu_id, 'update') : false ?>' == 1 && (r.status == 'Draft' || r.status == 'Diajukan')) {
                            btn_edit = `<button class="dropdown-item" onclick="edit('${r.proposal_id}','${r.pasar_id}')" type="button"><i class="ti ti-edit me-1"></i>Edit</button>`;
                        }
                        if ('<?= (($menu_id !== '')) ? has_access($menu_id, 'delete') : false ?>' == 1 && (r.status == 'Draft' || r.status == 'Diajukan')) {
                            btn_delete = `<button class="dropdown-item" onclick="update('${r.proposal_id}','Deleted','tbl_proposal','proposal_id','Hapus proposal ${r.judul.replace(/[\t\n]+/g, '')}','revitalisasi')"> <i class="ti ti-trash me-1"></i>Hapus</button>`;
                        }
                        return `<div class="dropdown mt-3 mt-sm-0">
                                    <button class="btn btn-primary dropdown-toggle d-block" type="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ti ti-settings me-1"></i>Aksi<i class="ti ti-chevron-down ms-1"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        ${btn_detail}${btn_edit}${btn_delete}${btn_pengawasan}
                                    </ul>
                                </div>`;
                    },
                    className: 'align-middle text-center'
                }


            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function after_update() {
        showTable()
        showDokumen('revitalisasi_proposal_pengawasan_1', 'tbl_sk', selected_proposal_id, 'refresh')
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function() {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi') {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    if ('<?= is_dinas() ?>' == 1) {
                        $('#' + id + '-name').selectize()[0].selectize.disable();
                    }

                }
            }
        })
    }



    function getPasar(id, daerah_id) {
        var pasar = '';
        pasar = '<option value="">- Pilih Pasar -</option>';

        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/pasar',
            type: 'POST',
            data: {
                kode_daerah: daerah_id,
                verify_kelengkapan: ((id == 'pasar_id') ? 'Terverifikasi' : null),
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id + '').parent().LoadingOverlay('show')
                $('#' + id + '').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';
                $.each(response.data, function(k, v) {
                    pasar += '<option value="' + v.pasar_id + '">' + v.nama + '</option>';
                })
            },
            complete: function() {
                $('#' + id + '').html(pasar);
                $('#' + id + '').selectize()
                $('#' + id + '').parent().LoadingOverlay('hide')
                if (selected_pasar_id != '') {
                    $('#' + id + '').selectize()[0].selectize.setValue(selected_pasar_id)
                }
            }
        })
    }

    $("#wizard").on('change', '#pasar_id', function() {
        var html = ''
        if ($('#pasar_id').val() != '') {
            $.ajax({
                url: '<?= base_url() ?>web/revitalisasi/proposal/pasar',
                type: 'POST',
                beforeSend: function() {
                    $('.wizard .content').LoadingOverlay('show')
                },
                data: {
                    kode_daerah: $('#provinsi-name').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    pasar_id: $('#pasar_id').val(),
                },
                success: function(response) {
                    response = JSON.parse(response)

                    var result = response.data[0]

                    html = `
                    <div class="card timbul">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Deskripsi</label><br>
                                <input type="hidden" name="nama_pasar" value="` + result.nama + `">
                                <strong>` + result.deskripsi + `</strong>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>Alamat</label><br>
                                <strong>` + result.alamat + `, ` + result.kecamatan + `, ` + result.kab_kota + `, ` + result.provinsi + `, ` + result.kode_pos + `</strong>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Tipe Pasar</label><br>
                                        <strong>` + result.tipe_pasar + `</strong>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Kepemilikan</label><br>
                                        <strong>` + result.kepemilikan + `</strong>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Kondisi</label><br>
                                        <strong>` + result.kondisi + `</strong>
                                    </div>
                                 </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Bentuk Pasar</label><br>
                                        <strong>` + result.bentuk_pasar + `</strong>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>No Telp</label><br>
                                        <strong>` + isThisEmpty(result.detail.no_telp) + `</strong>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Fax</label><br>
                                        <strong>` + isThisEmpty(result.detail.no_fax) + `</strong>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Waktu Operasional</label><br>
                                        <strong>` + isThisEmpty(result.detail.waktu_operasional).toString().split(',').join(', ') + `<br> ` + result.detail.jam_operasional_awal.toString().substr(0, 5) + `-` + result.detail.jam_operasional_akhir.toString().substr(0, 5) + `</strong>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                     <div class="form-group">
                                        <label>Jam Sibuk</label><br>
                                        <strong>` + result.detail.jam_sibuk_awal.toString().substr(0, 5) + `-` + result.detail.jam_sibuk_akhir.toString().substr(0, 5) + `</strong>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Komoditas yang dijual</label><br>
                                        <strong>` + isThisEmpty(result.detail.komoditas_dijual) + `</strong>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Omset</label><br>
                                        <strong>` + isThisEmpty(result.detail.omzet_sebelumnya) + `</strong>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Dana TP (Sebelumnya)</label><br>
                                            <strong>` + ((!$.isArray(result.dana_tp)) ? `Tahun ` + result.dana_tp.tahun + `, Rp. ` + addCommas(result.dana_tp.anggaran) : `-`) + `</strong>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label>Dana DAK (Sebelumnya)</label><br>
                                        <strong>` + ((!$.isArray(result.dana_dak)) ? `Tahun ` + result.dana_dak.tahun + `, Rp. ` + addCommas(result.dana_dak.anggaran) : `-`) + `</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `
                },
                complete: function() {
                    $('#container_detail_pasar').html(html)
                    resizeJquerySteps()
                    $('.wizard .content').LoadingOverlay('hide')
                }
            })
        } else {
            $('#container_detail_pasar').html(`
                <div class="card timbul">
                    <div class="card-body text-center">
                        <h5>Silahkan pilih pasar yang akan diajukan Revitalisasi</h5>
                        <h6>Jika data pasar tidak ada , Mohon pastikan Pasar yang akan diajukan proposal sudah <strong>Terverifikasi</strong> di menu <strong> Pasar</strong></h6>
                    </div>
                </div>
            `)
        }
    })

    function getProposalDokumen(jenis, container_id) {
        var dokumen_list = ''
        var jumlah_belum = 0
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/dokumen/json',
            type: 'POST',
            data: {
                proposal_id: $('#proposal_id').val(),
                jenis: jenis,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + container_id + ' .document_container').LoadingOverlay('show')
            },
            success: function(response) {
                response = JSON.parse(response)
                jumlah_belum = response.data.length
                $.each(response.data, function(k, v) {

                    var dokumen_act = `
                        <div class="col-md-3 col-12 d-flex align-items-center justify-content-center">
                            <div class="badge bg-danger status_file"><i class="ti ti-alert-triangle me-1"></i> Perlu Di Unggah</div>
                        </div>
                        <div class="col-md-3 col-12 d-flex align-items-center justify-content-center">
                            <button type="button" class="btn btn-dark  btn_upload"><i class="ti ti-upload"></i></button>
                            <input type="file" accept="` + v.mimes_accepted + `" data-dokumen_id="` + v.dokumen_id + `" data-maxsize="` + v.max_size + `" data-container_id = "` + container_id + `" data-proposal_dokumen_id = "` + ((v.proposal_dokumen_id == undefined) ? '' : v.proposal_dokumen_id) + `" class="form-control direct_upload d-none" name="dokumen[` + v.dokumen_id + `]">
                        </div>
                    `

                    if (v.file_uploaded !== false) {
                        if (v.file_status != 'Ditolak') {
                            jumlah_belum = jumlah_belum - 1
                        }


                        var color_status = 'bg-warning'
                        var icon_status = 'fa-spinner'
                        var status_file = ``
                        if (v.file_status == 'Disetujui') {
                            color_status = 'bg-success'
                            icon_status = 'fa-check'
                        } else if (v.file_status == 'Ditolak') {
                            status_file = `
                            <div class="col-md-3 col-12">
                                <div class="badge bg-danger  status_file"><i class="ti ti-alert-triangle"></i>Revisi</div><br><i class="text-danger fs-10">` + v.file_keterangan + `</i>
                            </div>
                            `
                        }

                        if (v.file_status != 'Ditolak') {
                            var file_status = v.file_status

                            if ('<?= is_dinas() ?>' !== false && v.file_status == 'Perlu Verifikasi') {
                                file_status = 'Menunggu Di Verifikasi Pelaksana - K1'
                            }
                            status_file = `
                            <div class="col-md-3 col-12 d-flex align-items-center justify-content-center">
                                <div class="badge ` + color_status + ` status_file"><i class="fa ` + icon_status + `"></i> ` + file_status + `</div>
                            </div>
                            `
                        }



                        dokumen_act = status_file + `
                            <div class="col-md-3 col-12 d-flex align-items-center justify-content-center">
                                <a href="` + v.file + `" class="btn  btn-info m-1"><i class="ti ti-file-type-pdf"></i></a>
                                <button type="button" class="btn btn-dark  btn_upload"><i class="ti ti-upload"></i></button>
                                <input type="file" accept="` + v.mimes_accepted + `" data-dokumen_id="` + v.dokumen_id + `" data-maxsize="` + v.max_size + `" data-container_id = "` + container_id + `" data-proposal_dokumen_id = "` + ((v.proposal_dokumen_id == undefined) ? '' : v.proposal_dokumen_id) + `" class="form-control direct_upload d-none" name="dokumen[` + v.dokumen_id + `]">
                            </div>
                        `
                    }


                    dokumen_list += `
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label>` + (k + 1) + `. ` + v.judul + `<b> ` + v.accept + ` - <strong class="text-danger">*` + v.max_size + `mb</strong></b></label><br>
                                <label class="text-muted">` + v.deskripsi + `</label>
                            </div>
                        </div>
                        ` + dokumen_act + `
                    </div>
                    <hr>
                    `
                })
            },
            complete: function() {
                var icon_jml = 'fa-exclamation-triangle'
                var color_jml = 'bg-danger'
                if (jumlah_belum == 0) {
                    icon_jml = 'fa-check'
                    color_jml = 'bg-success'
                }
                $('#wizard-t-' + container_id.toString().substr(-1)).find('strong').eq(0).find('div').remove()

                $('#wizard-t-' + container_id.toString().substr(-1)).find('strong').eq(0).html($('#wizard-t-' + container_id.toString().substr(-1)).find('strong').eq(0).text() + '<div class="badge badge-pill ' + color_jml + '"><i class="fa ' + icon_jml + '"></i> ' + jumlah_belum + '</div>')
                $('#' + container_id + ' .document_container').LoadingOverlay('hide')
                $('#' + container_id + ' .document_container').html(dokumen_list)
                resizeJquerySteps()
            }

        })
    }

    $('#wizard').on('click', '.btn_upload', function() {
        $(this).siblings('input').trigger('click');

    })


    $('#wizard').on('change', '.direct_upload', function() {
        var file = $(this).prop('files')[0];
        var validMimeTypes = ($(this).attr('accept')).toString().split(',');
        var maxFileSizeMB = $(this).data('maxsize'); // Maximum file size in megabytes
        var that = this
        if (file) {
            var fileSizeMB = file.size / (1024 * 1024); // Convert file size to MB
            if (fileSizeMB > maxFileSizeMB === true || (validMimeTypes.includes(file.type)) === false) {
                $(this).val('')
                Swal.fire(
                    'Gagal!',
                    'Mohon pastikan ukuran dan jenis file yang diunggah sesuai',
                    'error'
                )
            } else {
                var formData = new FormData();

                formData.append('proposal_id', $('#proposal_id').val());
                formData.append('dokumen_id', $(this).data('dokumen_id'));
                formData.append('proposal_dokumen_id', $(this).data('proposal_dokumen_id'));
                formData.append('file', file);
                formData.append('csrf_baseben', '<?= $this->security->get_csrf_hash() ?>');
                $.ajax({
                    url: '<?= base_url() ?>web/revitalisasi/proposal/dokumen',
                    type: 'POST',
                    beforeSend: function() {
                        $(that).parent().parent().find('.status_file').eq(0).removeClass('bg-danger');
                        $(that).parent().parent().find('.status_file').eq(0).addClass('bg-info');
                        $(that).parent().parent().find('.status_file').eq(0).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Uploading..')
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        // Upload progress
                        xhr.upload.addEventListener('progress', function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                //$(that).parent().parent().find('.status_file').eq(0).html('<div class="progress"><div class="progress-bar" style="width: ' + percentComplete + '%"> Processing</div></div>')

                            }
                        }, false);
                        return xhr;
                    },
                    success: function(response) {
                        response = JSON.parse(response)

                        if (response['kode'] == 200) {
                            $(that).data('proposal_dokumen_id', response.data.proposal_dokumen_id)
                            $(that).parent().parent().find('.status_file').eq(0).removeClass('bg-info');
                            $(that).parent().parent().find('.status_file').eq(0).addClass('bg-warning');
                            $(that).parent().parent().find('.status_file').eq(0).html('<i class="ti ti-hourglass-high"></i>Menunggu Di Verifikasi Pelaksana - K1')
                            $(that).parent().parent().find('.status_file').eq(0).siblings('i').remove()
                            $(that).parent().html(('<a href="<?= base_url() ?>download/revitalisasi/proposal/' + response.data.file + '" class="btn btn-info m-1"><i class="ti ti-download"></i></a>' + $(that).parent().find('button.btn_upload')[0].outerHTML + $(that).parent().find('input')[0].outerHTML))
                            var jml_belum = parseInt($('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).text())

                            // if ($(that).data('proposal_dokumen_id') == '') {
                            var icon_danger = ''
                            if (jml_belum - 1 <= 0) {
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).removeClass('bg-danger');
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).addClass('bg-success');
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).html('<i class="ti ti-circle-check"></i>' + ((jml_belum != 0) ? (jml_belum - 1) : jml_belum))
                            } else {
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).removeClass('bg-success');
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).addClass('bg-danger');
                                $('#wizard-t-' + $(that).data('container_id').toString().substr(-1)).find('div').eq(0).html('<i class="ti ti-alert-triangle me-1"></i>' + ((jml_belum != 0) ? (jml_belum - 1) : jml_belum))
                            }
                            // }
                        }
                    }
                });
            }
        }
    })

    function submitProposal(jenis, status, keterangan, id) {
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/submit',
            type: 'POST',
            data: {
                jenis: jenis,
                proposal_id: ((jenis == 'proposal' ? id : '')),
                proposal_dokumen_id: ((jenis == 'proposal_dokumen' ? id : '')),
                status: status,
                keterangan: keterangan,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                location.reload()
            }
        })
    }
</script>
<?php
foreach (glob("./application/views/backend/js/public_gov/revitalisasi/proposal/*.php") as $filename) {
    include $filename;
}

?>