<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    $(document).ready(function() {
        showTable('revitalisasi_proposal_dokumen_1', 'tbl_general', 'refresh');
        showTable('revitalisasi_proposal_dokumen_2', 'tbl_dokumen_lanjutan', 'refresh');
        showSKPengelola('refresh')
        // Check if there's a stored active tab
        $('#t_staf_ditugaskan').selectize()
        $('#mainTab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#mainTab a[href="' + hash + '"]').tab('show');
    })

    function addPengelola() {
        $('#proposal_pengelola_id').val('')
        $('#sk_operator').val('')
        $('#nip').val('')
        $('#nama').val('')
        $('#jabatan').val('')
        $('#modal_sk').modal('show')
    }

    function editPengelola(proposal_pengelola_id) {
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/pengelola/json',
            type: 'POST',
            data: {
                proposal_pengelola_id: proposal_pengelola_id,
                proposal_id: '<?= $proposal_id ?>',
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                response = JSON.parse(response)
                var data = response.data[0]
                $('#sk_operator').val(data.sk_pengelola)
                $('#nip').val(data.nip)
                $('#nama').val(data.nama)
                $('#jabatan').val(data.jabatan)
                $('#proposal_pengelola_id').val(proposal_pengelola_id)

            }
        })
        $('#modal_sk').modal('show')
    }


    function showTable(jenis, table, refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#' + table)) {
            $("#" + table).dataTable().fnDestroy();
            $('#' + table).empty();
        }

        var table = $('#' + table).DataTable({
            dom: 'rtip',
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    jenis: jenis,
                    proposal_id: <?= $proposal_id ?>,
                },
                "url": '<?= base_url() ?>web/revitalisasi/proposal/dokumen/json',
            },
            columns: [{
                    title: 'No',
                    data: 'dokumen_id',
                    className: 'text-center',
                    orderable: false,
                    render: (k, v, r, m) => (m.settings._iDisplayStart) + (m.row + 1)
                },
                {
                    title: 'Judul',
                    data: 'judul',
                    orderable: false,
                    render: (k, v, r) => `${r.judul}<b> ${r.accept} - <strong class="text-danger">*${r.max_size}mb</strong></b><br><i class="text-muted">${r.deskripsi}</i>`
                },
                {
                    title: 'File Unggahan',
                    data: 'dokumen_id',
                    orderable: false,
                    render: (k, v, r) => {
                        const accept = r.accept.toString().split(',');
                        let file = '';

                        if (r.file_uploaded === false) {
                            $('#btn_v_review').hide();
                        }

                        if (r.file_uploaded === true) {
                            file = `<a class="btn btn-dark btn-sm" target="_blank" href="${r.file}"><i class="ti ti-download me-1"></i>Unduh File</a>`;

                            if ($.inArray('pdf', accept) !== -1) {
                                file = `<button class="btn btn-dark btn-sm" onclick="lihatPDF('${r.judul}','${r.file_data}','${r.file}')"><i class="ti ti-file-type-pdf me-1"></i>Lihat PDF</button>`;
                            }

                            if ($.inArray('jpg', accept) !== -1 || $.inArray('png', accept) !== -1 || $.inArray('jpeg', accept) !== -1) {
                                file = `<img class="img-thumbnail" src="${r.file}" style="max-width:200px"></a>`;
                            }

                            if ($.inArray('mp4', accept) !== -1 || $.inArray('mov', accept) !== -1) {
                                file = `<button class="btn btn-dark btn-sm" onclick="lihatVideo('${r.judul}','${r.file_data}','${r.file}')"><i class="ti ti-video me-1"></i>Lihat Video</button>`;
                            }
                        }

                        return file;
                    },
                    className: 'align-middle text-center'
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        let status_color = 'warning';
                        let icon = 'spinner';
                        let keterangan = '';
                        let file_status = 'Perlu Di Unggah';
                        let staf_ditugaskan = '';

                        if (r.file_uploaded) {
                            if (r.file_status == 'Disetujui') {
                                status_color = 'success'
                                icon = 'check'
                                file_status = 'Lengkap'
                            }

                            if (r.file_status == 'Ditolak') {
                                status_color = 'danger'
                                icon = 'times'
                                keterangan = '<br><i class="text-danger"> ' + r.file_keterangan + '</i>'
                                file_status = 'Tidak Lengkap'
                            }

                            if (r.file_status == 'Perlu Verifikasi') {
                                file_status = 'Menunggu Di Verifikasi Pelaksana - K1';
                            } else {
                                file_status = r.file_status;
                            }

                            if ((r.staf_ditugaskan !== null && r.staf_ditugaskan !== '') && '<?= is_k1_ketuatim() ?>' == 1) {
                                staf_ditugaskan = `<br><div class="badge bg-info"><i class="ti ti-info-circle me-1"></i>Ditugaskan ke <b>${r.staf_ditugaskan_nama}</b></div>`;
                            }
                        }

                        return `<div class="badge bg-${status_color}"><i class="ti ti-${icon}"></i> ${file_status}</div>${keterangan}${staf_ditugaskan}`;
                    },
                    className: 'align-middle text-center'
                },
                {
                    title: 'Aksi',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        let btn_verify = '';
                        let btn_tugaskan = '';
                        let btn_upload = '';

                        if (r.file_uploaded === true) {
                            if (r.file_status != 'Disetujui' && '<?= is_k1_pelaksana() ?>' == 1) {
                                if(r.staf_ditugaskan != null){
                                    btn_verify = `<button class="btn btn-sm btn-success m-1" onclick="verifyProposalDokumen('${r.proposal_dokumen_id}',true)"><i class="ti ti-search me-1"></i>Verifikasi</button>`;
                                }
                            }

                            if (r.file_status != 'Disetujui' && '<?= is_k1_ketuatim() ?>' == 1) {
                                btn_tugaskan = `<button class="btn btn-sm btn-dark m-1" onclick="tugaskanStaf('${r.proposal_dokumen_id}','${r.dokumen_id}')"><i class="ti ti-users"></i>Tugaskan </button>`;
                            }
                        }

                        if (r.file_status != 'Disetujui' && ('<?= is_k1_ketuatim() ?>' == 1 || '<?= is_dinas() ?>' == 1)) {
                            btn_upload = `
                            <button type="button" class="btn btn-dark btn-sm  btn_upload"><i class="ti ti-upload"></i></button>
                            <input type="file" accept="${r.mimes_accepted}" data-ext="${r.accept}" data-dokumen_id="${r.dokumen_id}" data-maxsize="${r.max_size}" data-proposal_dokumen_id="${((r.proposal_dokumen_id === undefined) ? '' : r.proposal_dokumen_id)}" class="form-control direct_upload d-none" name="dokumen[${r.dokumen_id}]"><br>`;
                        }

                        return btn_upload + btn_verify + btn_tugaskan;
                    },
                    className: 'align-middle text-center'
                }


            ],
            "scrollX": true,
            "displayLength": 20
        });
    }

    function lihatPDF(judul, url_file, url_unduh) {
        $('#lihat_judul').html(judul);
        $('#container_lihat').html(`
            <object data="<?= base_url() ?>/services/res/upload/revitalisasi/proposal/${url_file}" type="application/pdf" width="100%" height="600">
                <p>PDF tidak bisa ditampilkan <br> <a href="${url_unduh}" target="_blank"><i class="ti ti-file-type-pdf me-1"></i>Unduh PDF</a>.</p>
            </object>
            <hr>
            <a class="btn btn-primary " href="${url_unduh}"><i class="ti ti-download me-1"></i>Unduh</a>
        `);
        $('#modal_lihat').modal('show');

    }

    function lihatVideo(judul, url_file, url_unduh) {
        $('#lihat_judul').html(judul);
        $('#container_lihat').html(`
            <video width="100%"  controls>
                <source src="<?= base_url() ?>/services/res/upload/revitalisasi/proposal/${url_file}" type="video/mp4">
                Browser anda tidak support video tag.
            </video>
            <hr>
            <a class="btn btn-primary" href="${url_unduh}"><i class="ti ti-download me-1"></i>Unduh</a>
        `);
        $('#modal_lihat').modal('show');

    }


    function showSKPengelola(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_sk_pengelola')) {
            $("#tbl_sk_pengelola").dataTable().fnDestroy();
            $('#tbl_sk_pengelola').empty();
        }

        var table = $('#tbl_sk_pengelola').DataTable({
            dom: 'rtip',
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    proposal_id: <?= $proposal_id ?>,
                },
                "url": '<?= base_url() ?>web/revitalisasi/proposal/pengelola/json',
            },
            columns: [{
                    title: 'No',
                    data: 'proposal_pengelola_id',
                    className: 'text-center',
                    orderable: false,
                    render: (k, v, r, m) => (m.settings._iDisplayStart) + (m.row + 1)
                },
                {
                    title: 'SK Operator',
                    data: 'sk_pengelola'
                },
                {
                    title: 'NIP',
                    data: 'nip',
                    orderable: false
                },
                {
                    title: 'Nama',
                    data: 'nama',
                    orderable: false
                },
                {
                    title: 'Jabatan',
                    data: 'jabatan',
                    orderable: false
                },
                {
                    title:'Lampiran',
                    data:'lampiran_usulan',
                    orderable:false,
                    render:function(k,v,r){
                        if(r.lampiran_usulan != '' && r.lampiran_usulan != null){
                            return '<a class="btn btn-primary" target="_blank" href="<?= base_url() ?>download/revitalisasi/proposal/sk/'+r.lampiran_usulan+'"> <i class="fa fa-link"></i> Unduh File</a>'
                        }else{
                            return ''
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        let btn_edit = '';
                        let btn_delete = '';

                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1 && ('<?= $proposal['status'] ?>' == 'Diundang Review') && '<?= is_dinas() ?>' == 1) {
                            btn_edit = `<button class="btn btn-warning btn-sm m-1" onclick="editPengelola('${r.proposal_pengelola_id}')" type="button"><i class="ti ti-edit"></i></button>`;
                        }

                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'delete') : false) ?>' == 1 && ('<?= $proposal['status'] ?>' == 'Diundang Review') && '<?= is_dinas() ?>' == 1) {
                            btn_delete = `<button class="btn btn-sm btn-danger m-1" onclick="update('${r.proposal_pengelola_id}','Deleted','tbl_proposal_pengelola','proposal_pengelola_id','Hapus sk pengelola ${($.trim(r.nip.replace(/[\t\n]+/g, '')))}','revitalisasi')"> <i class="ti ti-trash"></i></button>`;
                        }

                        return btn_edit + btn_delete;
                    },
                    className: 'align-middle text-center'
                }

            ],
            "scrollX": true,
            "displayLength": 20
        });
    }


    function verifyProposalDokumen(proposal_dokumen_id, reload = false) {
        Swal.fire({
            title: 'Verifikasi',
            text: 'Silahkan verifikasi data dokumen proposal ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Setujui',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
                verify('proposal_dokumen', proposal_dokumen_id, 'Disetujui', null, reload)
            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return verify('proposal_dokumen', proposal_dokumen_id, 'Ditolak', reason, reload)
                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }
        });
    }



    function verifyProposal(title, status_ok, status_deny, proposal_id, reload = false) {
        Swal.fire({
            title: title,
            text: 'Silahkan verifikasi data proposal ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: status_ok,
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: status_deny,
        }).then((result) => {
            if (result.isConfirmed) {
                var status_approv = status_ok
                if ($.inArray(status_ok, ['Undang Review']) !== -1) {
                    status_approv = 'Diundang Review'
                }
                verify('proposal', proposal_id, status_approv, null, reload)
            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'textarea',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return verify('proposal', proposal_id, 'Ditolak', reason, reload)
                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }

                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }
        });
    }



    function verify(jenis, id, status, keterangan = null, reload) {
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/proposal/submit',
            type: 'POST',
            data: {
                jenis: jenis,
                proposal_dokumen_id: ((jenis == 'proposal_dokumen') ? id : ''),
                proposal_id: ((jenis == 'proposal') ? id : ''),
                status: status,
                keterangan: keterangan,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            success: function(response) {
                Swal.fire(
                    'Sukses!',
                    'Proposal Dokumen berhasil ' + status,
                    'success'
                )
                if (reload === true) {
                    location.reload();
                } else {
                    after_update()
                }

            }
        })
    }


    $('#mainTabContent').on('click', '.btn_upload', function() {
        $(this).siblings('input').trigger('click');

    })



    $('#mainTabContent').on('change', '.direct_upload', function() {
        var file = $(this).prop('files')[0];
        var validMimeTypes = ($(this).attr('accept')).toString().split(',');
        var maxFileSizeMB = $(this).data('maxsize'); // Maximum file size in megabytes
        var that = this
        if (file) {
            var fileSizeMB = file.size / (1024 * 1024); // Convert file size to MB
            var fileExtension = file.name.toString().split('.').pop().toLowerCase();

            var is_dwg = false;
            var is_valid_mime = false;
            if ($(this).data('ext') == 'dwg') {
                is_dwg = true;
            }
            if ($(this).data('ext') == fileExtension) {
                is_valid_mime = true;
            } else if ((validMimeTypes.includes(file.type)) === true) {
                is_valid_mime = true;
            }

            if (fileSizeMB > maxFileSizeMB === true || is_valid_mime === false) {
                $(this).val('')
                Swal.fire(
                    'Gagal!',
                    'Mohon pastikan ukuran dan jenis file yang diunggah sesuai',
                    'error'
                )
            } else {
                var formData = new FormData();

                formData.append('proposal_id', <?= $proposal_id ?>);
                formData.append('dokumen_id', $(this).data('dokumen_id'));
                formData.append('proposal_dokumen_id', $(this).data('proposal_dokumen_id'));
                formData.append('file', file);
                formData.append('csrf_baseben', '<?= $this->security->get_csrf_hash() ?>');
                $.ajax({
                    url: '<?= base_url() ?>web/revitalisasi/proposal/dokumen',
                    type: 'POST',
                    beforeSend: function() {
                        $(that).parent().prevAll().first().find('div').eq(0).removeClass('bg-warning');
                        $(that).parent().prevAll().first().find('div').eq(0).addClass('bg-info');
                        $(that).parent().prevAll().first().find('div').eq(0).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Uploading..')
                    },
                    data: formData,
                    processData: false,
                    contentType: false,
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        // Upload progress
                        xhr.upload.addEventListener('progress', function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = (evt.loaded / evt.total) * 100;
                                //$(that).parent().parent().find('.status_file').eq(0).html('<div class="progress"><div class="progress-bar" style="width: ' + percentComplete + '%"> Processing</div></div>')

                            }
                        }, false);
                        return xhr;
                    },
                    success: function(response) {
                        after_update()
                    }
                });
            }
        }
    })


    function tugaskanStaf(proposal_dokumen_id, dokumen_id) {
        $('#t_proposal_dokumen_id').val(proposal_dokumen_id)
        $('#t_dokumen_id').val(dokumen_id)
        $('#modal_tugaskan').modal('show')
    }

    function after_update() {
        showTable('revitalisasi_proposal_dokumen_1', 'tbl_general', 'refresh')
        showTable('revitalisasi_proposal_dokumen_2', 'tbl_dokumen_lanjutan', 'refresh')
        showSKPengelola('refresh')
    }
</script>