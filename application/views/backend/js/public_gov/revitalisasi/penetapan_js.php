<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    $(document).ready(function() {
        var form = $("#proposal_form");
        showTable('refresh');
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                var thiselement = element
                if ($(element).attr('id').includes('selectized') === true) {
                    thiselement = $(element).parent().parent().parent().find('select:eq(0)')
                }
                error.insertBefore(thiselement);
            },
            errorClass: "error is-invalid",
            rules: {
                pasar_id: 'required'
            },
            messages: {},
            invalidHandler: function(form, validator) {

            }
        });

    })


    function add() {
        $('#modal_proposal').modal('show')
    }

    function edit(penetapan_id) {
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/penetapan/json',
            type: 'POST',
            data: {
                penetapan_id: penetapan_id,
                start: 0,
                length: 1,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {},
            success: function(response) {
                data = (JSON.parse(response)).data[0]
                $('#penetapan_id').val(data.penetapan_id)
                $('#tgl').val(data.tgl)
                $('#tahun_anggaran').val(data.tahun_anggaran)
                $('#no_permendag').val(data.no_permendag)
                $('#deskripsi').val(data.keterangan)

            },
            complete: function() {

            }
        })
        $('#modal_proposal').modal('show')
    }

    function fill() {
        showTable('refresh');
    }

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    status: $('#f_status').val(),
                    tahun_anggaran: $('#f_tahun_anggaran').val(),
                },
                "url": '<?= base_url() ?>web/revitalisasi/penetapan/json',
            },
            columns: [{
                    title: 'No',
                    data: 'penetapan_id',
                    className: 'text-center',
                    orderable: false,
                    render: (k, v, r, m) => {
                        return m.settings._iDisplayStart + m.row + 1;
                    }
                },
                {
                    title: 'Tahun Anggaran',
                    data: 'tahun_anggaran',
                    orderable: false,
                },
                {
                    title: 'Permendag & Ditetapkan',
                    data: null,
                    orderable: false,
                    render: (k, v, r) => {
                        let permendagStatus = '';
                        if (r.status !== 'Ditetapkan') {
                            permendagStatus = 'Belum Permendag';
                        } else {
                            permendagStatus = r.no_permendag;
                        }

                        const formattedDate = r.tgl ? new Date(r.tgl).toLocaleDateString('en-US') : '';

                        return `
                                <div>
                                    <span class="fw-bold">${permendagStatus}</span><br>
                                    <span>Ditetapkan pada </span> ${formattedDate}</span>
                                </div>
                            `;
                    }
                },
                {
                    title: 'Total Anggaran',
                    data: 'jml_tp',
                    orderable: false,
                    render: (k, v, r) => {
                        return addCommas(r.jml_tp);
                    }
                },
                {
                    title: 'Total Daerah',
                    data: 'no_permendag',
                    orderable: false,
                    render: (k, v, r) => {
                        return `
                                <ul class="list-unstyled">
                                    <li>Total Provinsi Dana TP: <span class="badge bg-primary">${r.jml_provinsi}</span></li>
                                    <li>Total Kab/Kota Dana TP: <span class="badge bg-primary">${r.jml_kab_kota}</span></li>
                                    <li>Total Pasar: <span class="badge bg-primary">${r.jml_pasar}</span></li>
                                </ul>
                            `;
                                        }
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        let status_color = 'warning';
                        let icon = 'spinner';
                        let proposal_ditolak = '';
                        if (r.status == 'Ditetapkan') {
                            status_color = 'success';
                            icon = 'check';
                        }
                        return `<div class="badge bg-${status_color}"><i class="ti ti-${icon}"></i> ${r.status}</div>`;
                    }
                },
                {
                    title: 'Aksi',
                    data: 'status',
                    orderable: false,
                    render: (k, v, r) => {
                        const btn_detail = `<a href="<?= base_url() ?>web/revitalisasi/penetapan/${r.uuid}" class="btn btn-dark btn-sm m-1" type="button"><i class="ti ti-list"></i></a>`;
                        let btn_edit = '';
                        let btn_delete = '';
                        if ('<?= (($menu_id !== '')) ? has_access($menu_id, 'update') : false ?>' == 1 && (r.status !== 'Ditetapkan')) {
                            btn_edit = `<button class="btn btn-warning btn-sm m-1" onclick="edit('${r.penetapan_id}')" type="button"><i class="ti ti-edit"></i></button>`;
                        }
                        if ('<?= (($menu_id !== '')) ? has_access($menu_id, 'delete') : false ?>' == 1 && (r.status !== 'Ditetapkan')) {
                            btn_delete = `<button class="btn btn-sm btn-danger m-1" onclick="update('${r.penetapan_id}','Deleted','tbl_penetapan','penetapan_id','Hapus penetapan ${$.trim(r.no_permendag.replace(/[\t\n]+/g, ''))}','revitalisasi')"> <i class="ti ti-trash"></i></button>`;
                        }
                        return btn_detail + btn_edit + btn_delete;
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }



    function after_update() {
        showTable('refresh')
    }
</script>