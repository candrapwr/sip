<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    var selected_pasar_id = ''

    $(document).ready(function() {
        var form = $("#form_general");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                var thiselement = element
                if ($(element).attr('id').includes('selectized') === true) {
                    thiselement = $(element).parent().parent().parent().find('select:eq(0)')
                }
                error.insertBefore(thiselement);
            },
            errorClass: "error is-invalid",
            rules: {

            },
            submitHandler: function(form) {

                form.submit();

            },
            messages: {},
            invalidHandler: function(form, validator) {

            }
        });

        total($('#bobot_total'))
    })

    function total(that) {
        var v_total = []
        $.each($(that).parent().parent().parent().find('tr'), function(k, v) {
            var h_total = 0
            $.each($(v).eq(0).children(), function(kkk, vvv) {

                if (kkk > 1) {

                    if ($(v).eq(0).children().eq(kkk).children().val() != undefined) {

                        v_total[kkk] = (v_total[kkk] == undefined ? 0 : v_total[kkk]) + (($(v).eq(0).children().eq(kkk).children().val() != '') ? parseInt($(v).eq(0).children().eq(kkk).children().val()) : 0)



                        //Horizontal Total
                        if ($(vvv).eq(0).children().eq(0).val() != undefined && $(vvv).eq(0).children().eq(0).val() != '' && kkk != 2 && kkk != 3) {
                            h_total = h_total + parseInt($(vvv).eq(0).children().eq(0).val())
                        }
                    } else {
                        $(v).eq(0).children().eq(($(v).eq(0).children().length - 1)).text(h_total)
                        if (k == ($(that).parent().parent().parent().find('tr').length - 1)) {
                            $(v).eq(0).children().eq(kkk - 1).text(v_total[kkk])
                            // console.log($(v).eq(0).children().eq(kkk - 1))


                            if (kkk == ($(v).eq(0).children().length - 1)) {
                                $(v).eq(0).children().eq(kkk).text(((Object.keys(v_total).slice(2).map(function(key) {
                                    return v_total[key];
                                })).reduce(function(accumulator, currentValue) {
                                    return accumulator + currentValue;
                                }, 0)))
                            }
                        }

                    }
                    // console.log('r '+k+' c'+kkk+' '+(v_total[kkk] == undefined ? 0 : v_total[kkk])+' sum '+ v_total[kkk])

                }
            })
        })
    }

    $('#frm_kurva').submit(function(e) {

        var jadwal = JSON.parse('<?= json_encode($ms_jadwal) ?>');


        $.each(jadwal, function(k, v) {
            if($('input[name="bobot[' + (k + 1) + ']"]').val() != ''){
                if ($('input[name="bobot[' + (k + 1) + ']"]').val() != parseInt($('#h_total_' + (k + 1)).text())) {
                    e.preventDefault();
                    Swal.fire(
                        'Bobot dan total mingguan <br>"Kegiatan ' + (k + 1) + ' ' + v.kegiatan + '"<br> tidak sesuai!',
                        'Total Bobot Mingguan <b>harus ' + $('input[name="bobot[' + (k + 1) + ']"]').val() + '</b>',
                        'error'
                    )
                }

                if ($('input[name="bobot[' + (k + 1) + ']"]').val()  != '' && $('input[name="biaya[' + (k + 1) + ']"]').val() == '') {
                    e.preventDefault();
                    Swal.fire(
                        'Biaya harus di isi!',
                        '"Kegiatan ' + (k + 1) + '. ' + v.kegiatan + '"',
                        'error'
                    )
                }
            }else{
            e.preventDefault();
            Swal.fire(
                'Gagal Menyimpan',
                'Bobot dari uraian kegiatan harus diisi semua!',
                'error'
            )
            }
        })

        if (parseInt($('#bobot_total').text()) != 100) {
            e.preventDefault();
            Swal.fire(
                'Total bobot semua kegiatan tidak sesuai!',
                'Total Bobot <b>harus 100</b>',
                'error'
            )
        }

        if ($('#biaya_total').text() != '<?= $kontrak['nilai'] ?>') {
            e.preventDefault();
            Swal.fire(
                'Biaya semua kegiatan tidak sesuai!',
                'Total Biaya <b>harus sama dengan nilai projek / Nilai Kontrak</b>',
                'error'
            )
        }

        if (parseInt($('#total_all').text()) != 100) {
            e.preventDefault();
            Swal.fire(
                'Total keseluruhan kegiatan bobot tidak sesuai!',
                'Total Bobot Mingguan <b>harus 100%</b>',
                'error'
            )
        }
    })



    function verifyJadwal() {
        Swal.fire({
            title: 'Verifikasi',
            text: 'Silahkan verifikasi data jadwal ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Setujui',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
                update('<?= $proposal_jadwal['proposal_jadwal_id'] ?>', (('<?= $proposal_jadwal['status'] ?>' == 'Disetujui Staff K5') ? 'Disetujui' : 'Disetujui Staff K5'), 'tbl_proposal_jadwal', 'proposal_jadwal_id', 'Setujui Jadwal', 'revitalisasi')

            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return update('<?= $proposal_jadwal['proposal_jadwal_id'] ?>', 'Revisi', 'tbl_proposal_jadwal', 'proposal_jadwal_id', 'Penolakan Kontrak', 'revitalisasi', reason)

                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }

        })
    }

    function after_update() {
        location.reload();
    }
</script>