<script src="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.with.jquery.step.js"></script>
<script>
    var selected_pasar_id = ''

    $(document).ready(function() {
        showTable('refresh');
        var form = $("#form_general");
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                var thiselement = element
                if ($(element).attr('id').includes('selectized') === true) {
                    thiselement = $(element).parent().parent().parent().find('select:eq(0)')
                }
                error.insertBefore(thiselement);
            },
            errorClass: "error is-invalid",
            rules: {

            },
            submitHandler: function(form) {
                if ((parseFloat($('#hps').val()) + parseFloat($('#pagu').val())) > parseInt(<?= $proposal['anggaran_ditetapkan'] ?>)) {
                    Swal.fire(
                        'Gagal!',
                        'Total HPS dan Pagu melebihi anggaran yang ditetapkan',
                        'error'
                    )
                    return false
                } else if (($('input[name*="tahapan["]').filter((i, el) => el.value.trim() == '').length === 0) === false) {
                    $.each($('input[name*="tahapan["]'), function(k, v) {
                        $(v).eq(0).addClass('error is-invalid')
                    })
                    return false
                } else {
                    form.submit();
                }
            },
            messages: {},
            invalidHandler: function(form, validator) {

            }
        });


    })


    function edit(proposal_lelang_id, tipe_pengadaan) {

        $('#title_md').html('Kontrak ' + tipe_pengadaan)
        if (tipe_pengadaan == 'Pelaksanaan') {
            $('#dok_jaminan').parent().show()
            $('#dok_jaminan').prop('required', true)
        } else {
            $('#dok_jaminan').parent().hide()
            $('#dok_jaminan').prop('required', false)
        }
        $.ajax({
            url: '<?= base_url() ?>web/revitalisasi/kontrak/json',
            type: 'POST',
            data: {
                proposal_lelang_id: proposal_lelang_id,
                start: 0,
                length: 1,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {},
            success: function(response) {
                data = (JSON.parse(response)).data[0]
                kontrak = data.kontrak
                if (data.is_filled === true) {
                    $('#tgl_awal').val(kontrak.tgl_awal)
                    $('#tgl_akhir').val(kontrak.tgl_akhir)
                    $('#pemenang').val(kontrak.pemenang)
                    $('#no_kontrak').val(kontrak.no_kontrak)
                    $('#nilai').val(kontrak.nilai)
                    $('#tgl_dipa').val(kontrak.tgl_dipa)
                    $('#no_dipa').val(kontrak.no_dipa)
                    $('#dok_kontrak').val(null)
                    $('#dok_jaminan').val(null)
                    if (kontrak.dok_kontrak != '' && kontrak.dok_kontrak != null) {
                        $('#dok_kontrak').prop('required', false)
                        $('#last_file').html('<a class="btn btn-primary btn-sm" href="<?= base_url() ?>download/revitalisasi/proposal/kontrak/' + kontrak.dok_kontrak + '"><i class="ti ti-file me-1"></i>File Sebelumnya</a>')
                    }
                    if (kontrak.dok_jaminan != '' && kontrak.dok_jaminan != null) {
                        $('#dok_jaminan').prop('required', false)
                        $('#last_file_jaminan').html('<a class="btn btn-primary btn-sm" href="<?= base_url() ?>download/revitalisasi/proposal/kontrak/' + kontrak.dok_jaminan + '"><i class="ti ti-file me-1"></i>File Sebelumnya</a>')
                    }


                    $('#proposal_kontrak_id').val(kontrak.proposal_kontrak_id)
                }
                $('#proposal_lelang_id').val(data.proposal_lelang_id)

            },
            complete: function() {

            }
        })
        $('#modal_general').modal('show')
    }


    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rt',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    proposal_id: '<?= $proposal_id ?>',

                },
                "url": '<?= base_url() ?>web/revitalisasi/kontrak/json',
            },
            columns: [{
                    title: 'No',
                    data: 'proposal_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Tipe Kontrak',
                    data: 'tipe_pengadaan',
                    orderable: false,
                    render: function(k, v, r) {
                        var color = 'danger'
                        var icon = 'spinner'
                        var status = 'Belum Dibuat'
                        var keterangan = ''
                        if (r.is_filled === true) {
                            status = r.kontrak.status
                            color = 'warning'
                            if (r.kontrak.status == 'Disetujui') {
                                color = 'success'
                                icon = 'check'
                            } else if (r.kontrak.status == 'Ditolak') {
                                color = 'danger'
                                icon = 'times'
                                status = 'Revisi'
                                keterangan = '<br><i class="text-danger" style="font-size:12px">' + r.kontrak.keterangan + '</i>'
                            }

                            if (status == 'Perlu Verifikasi') {
                                status = 'Menunggu Di Verifikasi Verfikator 1 - K5'
                            } else if (status == 'Perlu Verifikasi 2') {
                                status = 'Menunggu Di Verifikasi Verfikator 2 - K5'
                            } else if (status == 'Disetujui') {
                                status = 'Selesai'
                            }

                        }

                        return r.tipe_pengadaan + '<br><div class="badge bg-' + color + '"><i class="ti ti-' + icon + '"></i> ' + status + '</div>' + keterangan
                    }

                },
                {
                    title: 'Lelang',
                    data: 'hps',
                    orderable: false,
                    render: function(k, v, r) {

                        return r.kode + '<br>' +
                            r.tgl + '<br>' +
                            r.judul + '<br>' +
                            'HPS Rp. ' + addCommas(r.hps) + '<br>' +
                            'PAGU Rp. ' + addCommas(r.pagu)
                    }

                },
                {
                    title: 'Kontrak',
                    data: 'hps',
                    orderable: false,
                    render: function(k, v, r) {

                        if (r.is_filled === true) {
                            var dok_kontrak = ''
                            var dok_jaminan = ''
                            if (r.kontrak.dok_kontrak != '' && r.kontrak.dok_kontrak != null) {
                                dok_kontrak = '<a href="' + r.kontrak.dok_kontrak + '" ><i class="ti ti-file-type-pdf"></i> Dokumen Kontrak</a><br>'
                            }
                            if (r.kontrak.dok_jaminan != '' && r.kontrak.dok_jaminan != null) {
                                dok_jaminan = '<a href="' + r.kontrak.dok_jaminan + '" ><i class="ti ti-file-type-pdf"></i> Dokumen Jaminan</a><br>'
                            }
                            return r.kontrak.no_kontrak + '<br>' +
                                r.kontrak.nilai + '<br>' +
                                r.kontrak.pemenang + '<br>' +
                                dok_kontrak + dok_jaminan
                        }
                        return '-'
                    }

                },
                {
                    title: 'Aksi',
                    data: 'proposal_lelang_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        var button_color = 'btn-danger'

                        var btn_delete = ''
                        var btn_edit = ''
                        var btn_verify = ''
                        var btn_submit_kontrak = ''

                        if ('<?= ((($menu_id != '')) ? has_access($menu_id, 'update') : false) ?>' == 1) {





                            btn_edit = '<button class="btn btn-sm btn-warning m-1" onclick="edit(\'' + r.proposal_lelang_id + '\',\'' + r.tipe_pengadaan + '\')"><i class="ti ti-pencil"></i></button><br>'


                            if (r.is_filled === true) {
                                if (r.kontrak.status == 'Disetujui') {
                                    btn_edit = ''
                                }

                                if (r.kontrak.status == 'Draft' && '<?= is_dinas() ?>' == 1) {
                                    btn_submit_kontrak = `<button class="btn btn-sm btn-primary m-1" onclick="submitKontrak('` + r.kontrak.proposal_kontrak_id + `')"><i class="ti ti-device-floppy me-1"></i>Submit Kontrak</button><br>`
                                }

                                if ('<?= is_verifikator1_k5_kontrak() ?>' == 1 || '<?= is_verifikator2_k5_kontrak() ?>' == 1) {
                                    var stat = 'Perlu Verifikasi'
                                    if (r.kontrak.status == 'Perlu Verifikasi') {
                                        stat = 'Perlu Verifikasi 2'
                                    }else if (r.kontrak.status == 'Perlu Verifikasi 2') {
                                        stat = 'Disetujui'
                                    }
                                    if('<?= is_verifikator1_k5_kontrak() ?>' == 1 && r.kontrak.status == 'Perlu Verifikasi'){
                                        btn_verify = `<button class="btn btn-sm btn-success m-1" onclick="verifyKontrak('` + r.kontrak.proposal_kontrak_id + `','`+stat+`')"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                    }
                                    if('<?= is_verifikator2_k5_kontrak() ?>' == 1 && r.kontrak.status == 'Perlu Verifikasi 2'){
                                        btn_verify = `<button class="btn btn-sm btn-success m-1" onclick="verifyKontrak('` + r.kontrak.proposal_kontrak_id + `','`+stat+`')"><i class="ti ti-search me-1"></i>Verifikasi</button>`
                                    }

                                }
                            }

                        }

                        return btn_edit + btn_delete + btn_submit_kontrak + btn_verify

                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function verifyKontrak(proposal_kontrak_id, stat) {
        Swal.fire({
            title: 'Verifikasi',
            text: 'Silahkan verifikasi data kontrak ini',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Setujui',
            cancelButtonText: 'Batal',
            showDenyButton: true,
            denyButtonText: 'Tolak',
        }).then((result) => {
            if (result.isConfirmed) {
                update(proposal_kontrak_id, stat, 'tbl_proposal_kontrak', 'proposal_kontrak_id', 'Setujui Kontrak', 'revitalisasi')
                after_update()
            } else if (result.dismiss === Swal.DismissReason.deny) {
                Swal.fire({
                    title: 'Alasan Penolakan',
                    input: 'text',
                    inputLabel: 'Mohon masukan alasan penolakan',
                    inputPlaceholder: 'Mohon masukan alasan penolakan...',
                    showCancelButton: true,
                    confirmButtonText: 'Submit',
                    showLoaderOnConfirm: true,
                    preConfirm: (reason) => {
                        if (reason) {
                            return update(proposal_kontrak_id, 'Ditolak', 'tbl_proposal_kontrak', 'proposal_kontrak_id', 'Penolakan Kontrak', 'revitalisasi', reason)
                            after_update()
                        } else {
                            Swal.showValidationMessage('Mohon masukan alasan penolakan')
                        }
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                });
            }

        });
    }

    function submitKontrak(proposal_kontrak_id) {
        Swal.fire({
            title: 'Submit Kontrak',
            text: 'Kontrak yang sudah di submit tidak dapat diubah kembali',
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            cancelButtonText: 'Batal',
            showDenyButton: false,
            denyButtonText: 'Gagal Lelang',
        }).then((result) => {
            if (result.isConfirmed) {
                update(proposal_kontrak_id, 'Perlu Verifikasi', 'tbl_proposal_kontrak', 'proposal_kontrak_id', 'Submit Kontrak ', 'revitalisasi')
            }
        });
    }


    function after_update() {
        showTable()
    }
</script>