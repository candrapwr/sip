<script src="<?= base_url() ?>assets/backend/libs/highcharts/js/highmaps.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/highcharts/js/exporting_map.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/highcharts/js/id-all.js"></script>
<script>
	var saveTahun = {
		val: '',
		text: ''
	};
	var saveKomoditi = {
		val: '',
		text: ''
	};
	var dataMap = [];
	var saveSatuan = '';
	const dataMapping = {
		11: "id-ac",
		51: "id-ba",
		36: "id-bt",
		17: "id-be",
		34: "id-yo",
		31: "id-jk",
		75: "id-go",
		15: "id-ja",
		32: "id-jr",
		33: "id-jt",
		35: "id-ji",
		61: "id-kb",
		63: "id-ks",
		62: "id-kt",
		64: "id-ki",
		65: "id-ku",
		19: "id-bb",
		21: "id-kr",
		18: "id-1024",
		81: "id-ma",
		82: "id-la",
		52: "id-nb",
		53: "id-nt",
		91: "id-pa",
		92: "id-ib",
		14: "id-ri",
		76: "id-sr",
		73: "id-se",
		72: "id-st",
		74: "id-sg",
		71: "id-sw",
		13: "id-sb",
		16: "id-sl",
		12: "id-su"
	};

	$('#post_year').selectize({
		items: ["<?= date('Y') ?>"]
	});
	$('#post_komoditi').selectize({
		items: ["1"]
	});

	$('#post_year').change(function() {
		saveTahun['val'] = $(this).val();
		saveTahun['text'] = $(this).text();
		showData();
	});
	$('#post_komoditi').change(function() {
		saveKomoditi['val'] = $(this).val();
		saveKomoditi['text'] = $(this).text();
		showData();
	});


	saveTahun['val'] = $('#post_year').val();
	saveTahun['text'] = $('#post_year').text();
	saveKomoditi['val'] = $('#post_komoditi').val();
	saveKomoditi['text'] = $('#post_komoditi').text();
	showData();

	function showAjax() {
		$.ajax({
			url: "<?= site_url() ?>dashboard/perdagangan/monitoring-json/peta",
			type: 'post',
			data: {
				post_year: saveTahun.val,
				post_komoditi: saveKomoditi.val,
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			},
			dataType: 'json',
			beforeSend: function() {
				showLoader();
				$('#loader').show()
				$('#content').hide()
			},
			complete: function() {},
			success: function(response, textStatus, xhr) {
				saveSatuan = '';

				if (response.kode == 200) {
					dataMap = [];
					dataMap.push(['id-3700', 0]);
					var dataTmp = [];
					$.each(response.data, function(index, value) {
						dataTmp = [];

						if (value.stok != null) {
							dataTmp.push(dataMapping[value.daerah_id]);
							dataTmp.push(parseInt(value.stok));
							saveSatuan = '(' + value.satuan_komoditi + ')';
						} else {
							dataTmp.push(dataMapping[value.daerah_id]);
							dataTmp.push(0);
						}

						dataMap.push(dataTmp);
					});
					//console.log(dataMap);
					showPeta();
					hideLoader();;
					$('#loader').hide();
					$('#content').show();

				} else if (response.kode == 404) {
					$('#loader').LoadingOverlay("hide");
					$('#loader').hide();
					$('#content').hide();
					//$('#content').html('<h5 class="text-center">DATA STOK  '+ (saveKomoditi.text).toUpperCase() + ' PROVINSI TAHUN '+saveTahun.val+' </h5>');
				} else {
					location.reload();
				}
			},
			error: function(request, status, error) {
				location.reload();
			}
		})
	}

	function showData() {
		if (saveTahun.val != '' && saveKomoditi.val != '') {
			showAjax();
		}
	}

	function showPeta() {
		Highcharts.setOptions({
			lang: {
				numericSymbols: ["rb", "jt", "M", "T", "P", "E"]
			},
		});

		Highcharts.mapChart('container', {
			chart: {
				map: 'countries/id/id-all'
			},

			title: {
				text: 'DATA STOK ' + (saveKomoditi.text).toUpperCase() + ' PROVINSI TAHUN ' + saveTahun.val
			},

			subtitle: {
				text: '<br>'
			},

			mapNavigation: {
				enabled: true,
				buttonOptions: {
					verticalAlign: 'bottom'
				}
			},

			legend: {
				title: {
					text: 'Stok ' + saveKomoditi.text + ' ' + saveSatuan
				},
				itemDistance: 1,
			},

			colorAxis: {
				min: 0
			},

			series: [{
				data: dataMap,
				name: 'Stok ' + saveKomoditi.text,
				states: {
					hover: {
						color: '#BADA55'
					}
				},
				dataLabels: {
					enabled: true,
					format: '{point.name}'
				}
			}]
		});
	}
	//show loading
	function showLoader() {
		$('#loader').LoadingOverlay('show', {
			background: "rgba(253, 253, 255, 0.8)",
			image: "<?= base_url() ?>assets/brand/load-data.gif",
			imageAnimation: 'none',
			text: "Dalam Proses. Mohon Tunggu...",
			textAutoResize: true,
			textResizeFactor: 0.15,
			textColor: "#E70A2B"
		})
	}

	//HiDE loading
	function hideLoader() {
		$('#loader').LoadingOverlay('hide')
	}
</script>