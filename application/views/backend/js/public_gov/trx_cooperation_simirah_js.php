<script>
    $(document).ready(function() {
        showTable('refresh');
        $('#platform-name, #provinsi-name, #kabkota-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">-- Pilih Kabupaten/Kota --</option>')

    })



    function fill() {
        showTable('refresh');
    }

    $('#provinsi-name').on('change', function() {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)

    })

    // $('#kabkota-name').on('change', function() {
    //     var daerah_id = $("#kabkota-name").val();
    //     showTable('refresh');

    // })

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: $('#no_dokumen').val(),
                    nama_pengirim: $('#nama_pengirim').val(),
                    status: $('#f_status').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTrxCooperationSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pembuat Kontrak',
                    data: 'nama_exportir',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Nama</dt>
                                    <dd class="col-12">` + r.nama_exportir + `</dd>
                                    <dt class="col-12">NIB </dt>
                                    <dd class="col-12">` + r.nib_exportir + `</dd>
                                    <dt class="col-12">Tujuan</dt>
                                    <dd class="col-12">` + r.kab_kota + `, ` + r.provinsi + `</dd>
                                    <dt class="col-12">Tanggal Input</dt>
                                    <dd class="col-12">` + r.waktu_terbit + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Kontrak',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">No Kontrak</dt>
                                    <dd class="col-12">` + r.no_dokumen + `</dd>
                                    <dt class="col-12">Lampiran</dt>
                                    <dd class="col-12"> <a href="` + r.lampiran + `" target="_blank"><i class="ti ti-file me-1"></i>Lampiran</a></dd>
                                   
                                    <dt class="col-12">Jumlah</dt>
                                    <dd class="col-12">` + ((r.jumlah == null) ? '-' : addCommas(r.jumlah) + ` ` + r.satuan) + `</dd>
                                </dl>`
                    },
                },
                {
                    title: 'Produsen',
                    data: 'id_produsen',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Nama</dt>
                                    <dd class="col-12">` + r.nama_produsen + `</dd>
                                    <dt class="col-12">NIB</dt>
                                    <dd class="col-12">` + r.nib_produsen + `</dd>
                                    <dt class="col-12">Type</dt>
                                    <dd class="col-12">` + r.type_produsen.toString().replace('-', ' ').toUpperCase() + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {

                        return '<p>' + r.status + '</p>'
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>