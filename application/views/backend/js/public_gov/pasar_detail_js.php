<script>
    $(function() {
        $('#jenis_komoditi').selectize()
        $('#varian_komoditi').selectize()
        $('#satuan_komoditi').selectize()
        $('#provinsi').selectize()
        $('#kabupaten').selectize()
        $('#tipe_pasar').selectize()
        $('#kondisi').selectize()
        $('#pengelola').selectize()
        $('#kepemilikan').selectize()
        $('#bentuk').selectize()
        $('#program_pasar').selectize()

        // $('#varian_komoditi')[0].selectize.disable();
        // $('#satuan_komoditi')[0].selectize.disable();

        getHarga(null, 'refresh')
        getKios('refresh')
        getAnggaran('refresh')

        $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
            $.fn.dataTable.tables({
                visible: true,
                api: true
            }).columns.adjust();
        });

        $("#mdl_harga").on('hidden.bs.modal', function(e) {
            $('#form-data-harga')[0].reset();
            $('#harga_komoditi_id').val('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
            $('.selectize-input').closest('div.col').find('.text-danger').remove()
        });

        $("#mdl_kios").on('hidden.bs.modal', function() {
            $('#form-data-kios')[0].reset();
            $('#pasar_bangunan_id').val('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
            $('.selectize-input').closest('div.col').find('.text-danger').remove()
        });

        $("#mdl_anggaran").on('hidden.bs.modal', function() {
            $('#form-data-anggaran')[0].reset();
            $('#pasar_anggaran_id').val('')
            $('.form-control').removeClass('frm-error')
            $('.selectize-input').removeClass('frm-error')
            $('.form-control').closest('div.col').find('.text-danger').remove()
            $('.selectize-input').closest('div.col').find('.text-danger').remove()
        });

        $('.price').keyup(function() {
            let val = $(this).val()
            val = val.replace(/\./g, '')

            $(this).val(formatRupiah(val, ''))
        })

        $('#jenis_komoditi').change(function() {
            getVarianKomoditi($(this).val())
            getSatuanKomoditi($(this).val())
        })

        $('#update-pasar').click(function() {
            const activeIndex = $(this).attr('data-id')
            getUpdatePasar(activeIndex)
        })

        $('#update-detail').click(function() {
            const index = $(this).attr('data-id')
            getUpdateDetail(index)
        })

        $('#filter-harga-komoditi').click(function() {
            const date1 = $('#filter-harga-komoditi-date1').val()

            getHarga(date1)
        })

        $('#form-data-update').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>pasar/update",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {
                        Swal.fire({
                            title: 'Data Berhasil Disimpan',
                            icon: 'success',
                        }).then(function() {
                            const actionIndex = response.action_index

                            if (response.uri_segment == 1) {
                                window.location.href = '<?= site_url() ?>dashboard/pasar/detail/' + actionIndex
                            } else if (response.uri_segment == 2) {
                                window.location.href = '<?= site_url() ?>dashboard/sebaran-pasar/detail/' + actionIndex
                            }
                        })
                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire({
                                title: response.message,
                                icon: 'warning',
                            })
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            (element.attr('type') == 'text' || element.attr('type') == 'file') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })

        $('#form-data-detail').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>pasar/detail/add",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation
                    let error = response.error

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {
                        const actionIndex = $('#action_index').val()

                        Swal.fire({
                            title: 'Data Berhasil Disimpan',
                            icon: 'success',
                        }).then(function() {
                            window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                        })
                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire({
                                title: response.message,
                                icon: 'warning',
                            })
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })

        $('#form-data-harga').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>harga/add",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation
                    let error = response.error

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {
                        const actionIndex = $('#action_index').val()
                        window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire({
                                title: response.message,
                                icon: 'warning',
                            })
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })

        $('#form-data-kios').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>pasar/kios/add",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation
                    let error = response.error

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {

                        const actionIndex = $('#action_index').val()

                        Swal.fire({
                            title: 'Data Berhasil Disimpan',
                            icon: 'success',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then(function() {
                            window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                        })

                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire({
                                title: response.message,
                                icon: 'warning',
                                confirmButtonColor: '#E70A2B',
                                confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            })
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })

        $('#form-data-anggaran').submit(function(e) {
            e.preventDefault()

            $.ajax({
                url: "<?= site_url() ?>pasar/anggaran/add",
                type: 'post',
                data: $(this).serialize(),
                dataType: 'json',
                success: function(response) {
                    let success = response.success
                    let validation = response.validation
                    let error = response.error

                    $('.form-control').removeClass('frm-error')
                    $('.form-control').closest('div.col').find('.text-danger').remove()

                    if (success) {
                        const actionIndex = $('#action_index').val()

                        Swal.fire({
                            title: 'Data Berhasil Disimpan',
                            icon: 'success',
                            confirmButtonColor: '#E70A2B',
                            confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then(function() {
                            window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                        })

                    } else if (!success) {
                        if (response.code == 401) {
                            window.location.href = '<?= site_url('user/sign_out') ?>'
                        } else if (response.code == 404) {
                            Swal.fire({
                                title: response.message,
                                icon: 'warning',
                                confirmButtonColor: '#E70A2B',
                                confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            })
                        }

                        $.each(validation, function(key, value) {
                            const element = $('#' + key);

                            element.closest('div.form-group').find('.text-danger').remove();

                            (element.attr('type') == 'text') ? element.after(value): element.closest('.form-group').find('.selectize-input').after(value);

                            value.length > 0 ? element.addClass('frm-error') : element.removeClass('frm-error');
                            value.length > 0 ? element.closest('.form-group').find('.selectize-input').addClass('frm-error') : element.closest('.form-group').find('.selectize-input').removeClass('frm-error');
                        });
                    }
                }
            })
        })
    })

    function updatePasar(index = null, page = null) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                action_index: index,
                page: page,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                $('#pasar_id_update').val(response.pasar_id)
                $('#name').val(response.nama)
                $('#description').val(response.deskripsi)
                $('#address').val(response.alamat)
                $('#post_code').val(response.kode_pos)
                $('#latitude').val(response.latitude)
                $('#longitude').val(response.longitude)
            },
            complete: function() {
                $('#modalpasaredit').modal('show')
            }
        })
    }

    function getUpdatePasar(index) {
        $.ajax({
            url: "<?= site_url() ?>pasar/json",
            type: 'post',
            data: {
                action_index: index,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                $('#pasar_update_index').val(response.action_index)
                $('#pasar_id_updatee').val(response.data.pasar_id)
                $('#name_update').val(response.data.nama)
                $('#description_update').val(response.data.deskripsi)
                $('#address_update').val(response.data.alamat)
                $('#post_code_update').val(response.data.kode_pos)
                $('#latitude_update').val(response.data.latitude)
                $('#longitude_update').val(response.data.longitude)
                $('#daerah_id_update').val(response.data.daerah_id)
            },
            complete: function() {
                $('#modalpasaredit').modal('show')
            }
        })
    }

    function getUpdateDetail(index) {
        $.ajax({
            url: "<?= site_url() ?>pasar/detail/json",
            type: 'post',
            data: {
                id: index,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const data = response.data

                    if (data.kepemilikan == 'Pemerintah Daerah') {
                        $('#kepemilikan').selectize()[0].selectize.setValue(1)
                    } else if (data.kepemilikan == 'Desa Adat') {
                        $('#kepemilikan').selectize()[0].selectize.setValue(2)
                    } else if (data.kepemilikan == 'Swasta') {
                        $('#kepemilikan').selectize()[0].selectize.setValue(3)
                    }

                    if (data.bentuk_pasar == 'Permanen') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(1)
                    } else if (data.bentuk_pasar == 'Semi Permanen') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(2)
                    } else if (data.bentuk_pasar == 'Tanpa Bangunan') {
                        $('#bentuk_pasar').selectize()[0].selectize.setValue(3)
                    }

                    if (data.kondisi == 'Baik') {
                        $('#kondisi').selectize()[0].selectize.setValue(1)
                    } else if (data.kondisi == 'Rusak Berat') {
                        $('#kondisi').selectize()[0].selectize.setValue(2)
                    } else if (data.kondisi == 'Rusak Ringan') {
                        $('#kondisi').selectize()[0].selectize.setValue(3)
                    } else if (data.kondisi == 'Rusak Sedang') {
                        $('#kondisi').selectize()[0].selectize.setValue(4)
                    }

                    if (data.pengelola_pasar == 'Pemerintah') {
                        $('#pengelola').selectize()[0].selectize.setValue(1)
                    } else if (data.pengelola_pasar == 'BUMD') {
                        $('#pengelola').selectize()[0].selectize.setValue(2)
                    } else if (data.pengelola_pasar == 'Swasta') {
                        $('#pengelola').selectize()[0].selectize.setValue(3)
                    }

                    $('#waktu_operasional').selectize()[0].selectize.setValue(data.waktu_operasional)

                    $('#phone').val(data.no_telp)
                    $('#fax').val(data.no_fax)
                    $('#tipe_pasar').selectize()[0].selectize.setValue(data.tipe_pasar_id)
                    $('#waktu_operasional').val(data.waktu_operasional)
                    $('#jam_buka').val(data.jam_operasional_awal)
                    $('#jam_tutup').val(data.jam_operasional_akhir)
                    $('#jam_sibuk_awal').val(data.jam_sibuk_awal)
                    $('#jam_sibuk_akhir').val(data.jam_sibuk_akhir)
                    $('#pekerja_tetap').val(data.jumlah_pekerja_tetap)
                    $('#pekerja_nontetap').val(data.jumlah_pekerja_nontetap)
                    $('#luas_bangunan').val(data.luas_bangunan)
                    $('#luas_tanah').val(data.luas_tanah)
                    $('#jumlah_lantai').val(data.jumlah_lantai)
                    $('#tahun_bangun').val(data.tahun_bangun)
                    $('#tahun_renovasi').val(data.tahun_renovasi)
                }
            },
            complete: function() {
                $("#mdl_pasar_advanced").modal('show');
            }
        })
    }

    function getHarga(start_date = null, refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_harga')) {
            $("#tbl_harga").dataTable().fnDestroy();
            $('#tbl_harga').empty();
        }

        var table = $('#tbl_harga').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>harga/json',
                "data": {
                    refresh: refresh,
                    id: id,
                    start_date: start_date,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Jenis',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Varian',
                    data: 'varian_komoditi',
                },
                {
                    title: 'Harga',
                    data: 'harga',
                },
                {
                    title: 'Satuan',
                    data: 'satuan_komoditi',
                },
                {
                    title: 'Created By',
                    data: 'createdby',
                },
                {
                    title: 'Aksi',
                    data: 'pasar_harga_komoditi_id',
                    render: function(k, v, r, m) {
                        if (r.action_button) {
                            return '<button onclick="update(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-warning m-1" title="Update"><i class="ti ti-edit"></i></button>' +
                                '<button onclick="deleteHarga(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-danger m-1" title="Remove"><i class="ti ti-trash"></i></button>'
                        } else {
                            return ''
                        }

                    }
                },
            ],
            initComplete: function(settings, json) {
                let code = json.code

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }

                let table = $('#tbl_harga').DataTable();

                $.each(json.data, function(i, row) {
                    if (row.action == true) {
                        table.column(5).visible(true)
                    } else {
                        table.column(5).visible(false)
                    }
                })

                $('#title-date-harga').text(json.date)
            },
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getKios(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_kios')) {
            $("#tbl_kios").dataTable().fnDestroy();
            $('#tbl_kios').empty();
        }

        var table = $('#tbl_kios').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            searching: false,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>pasar/kios/json',
                "data": {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'Jenis Bangunan',
                    data: 'jenis',
                },
                {
                    title: 'Jumlah Bangunan',
                    data: 'jumlah',
                },
                {
                    title: 'Jumlah Pedagang',
                    data: 'jumlah_pedagang',
                },
                {
                    title: 'Aksi',
                    data: 'pasar_bangunan_id',
                    render: function(k, v, r, m) {
                        return '<button onclick="updateKios(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-warning m-1" title="Edit"><i class="ti ti-edit"></i></button>' +
                            '<button onclick="deleteKios(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-danger m-1" title="Hapus"><i class="ti ti-trash"></i></button>'

                    }
                },
            ],
            columnDefs: [{
                    targets: 3,
                    className: 'text-center'
                },
                {
                    targets: [1, 2],
                    className: 'text-right'
                }
            ],
            initComplete: function(settings, json) {
                let code = json.kode

                let table = $('#tbl_kios').DataTable();

                $.each(json.data, function(i, row) {
                    if (row.action) {
                        table.column(3).visible(true)
                    } else {
                        table.column(3).visible(false)
                    }
                })

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getAnggaran(refresh = null) {
        const id = $('#pasar_id').val()

        if ($.fn.DataTable.isDataTable('#tbl_anggaran')) {
            $("#tbl_anggaran").dataTable().fnDestroy();
            $('#tbl_anggaran').empty();
        }

        var table = $('#tbl_anggaran').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>pasar/anggaran/json',
                "data": {
                    refresh: refresh,
                    id: id,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Tahun',
                    data: 'tahun',
                },
                {
                    title: 'Omset',
                    data: 'omset',
                },
                {
                    title: 'Jumlah Anggaran',
                    data: 'anggaran',
                },
                {
                    title: 'Aksi',
                    data: 'pasar_anggaran_id',
                    render: function(k, v, r, m) {
                        return '<button onclick="updateAnggaran(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-warning m-1" title="Update"><i class="ti ti-edit"></i></button>' +
                            '<button onclick="deleteAnggaran(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')" class="btn btn-sm btn-danger m-1" title="Update"><i class="ti ti-trash"></i></button>'

                    }
                },
            ],
            columnDefs: [{
                    targets: 3,
                    className: 'text-center'
                },
                {
                    targets: [1, 2],
                    className: 'text-right'
                }
            ],
            initComplete: function(settings, json) {
                let code = json.code

                let table = $('#tbl_anggaran').DataTable();

                $.each(json.data, function(i, row) {
                    if (row.action) {
                        table.column(3).visible(true)
                    } else {
                        table.column(3).visible(false)
                    }
                })

                if (code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                }
            },
            "scrollX": true,
            "displayLength": 10
        });
    }

    function update(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/harga/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                const comID = response.jenis_komoditi_id + '|' + response.jenis_komoditi

                $('#jenis_komoditi').selectize()[0].selectize.setValue(comID)

                getVarianKomoditi(response.jenis_komoditi_id, response.varian_komoditi_id)
                getSatuanKomoditi(response.jenis_komoditi_id, response.satuan_komoditi_id)

                $('#harga').val(response.harga)
                $('#jumlah').val(response.jumlah)
                $('#harga_komoditi_id').val(response.pasar_harga_komoditi_id)

                $("#mdl_harga").modal('show')
            }
        })
    }

    function updateKios(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/kios/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                $('#pasar_bangunan_id').val(response.pasar_bangunan_id)
                $('#jumlah_bangunan').val(response.jumlah)
                $('#jumlah_pedagang').val(response.jumlah_pedagang)

                if (response.jenis == 'Los') {
                    $('input[name="jenis_bangunan"][value="1"]').prop('checked', true)
                } else if (response.jenis == 'Kios') {
                    $('input[name="jenis_bangunan"][value="2"]').prop('checked', true)
                } else if (response.jenis == 'Dasaran') {
                    $('input[name="jenis_bangunan"][value="3"]').prop('checked', true)
                }

                $("#mdl_kios").modal('show')
            }
        })
    }

    function updateAnggaran(index, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/anggaran/json",
            type: 'post',
            data: {
                action_index: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {

                $('#pasar_anggaran_id').val(response.pasar_anggaran_id)
                $('#program_pasar').selectize()[0].selectize.setValue(response.program_pasar_id)
                $('#tahun_anggaran').val(response.tahun)
                $('#omset_anggaran').val(response.omset)
                $('#jumlah_anggaran').val(response.anggaran)

                $("#mdl_anggaran").modal('show')
            }
        })
    }

    function deleteHarga(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/harga/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const action_index = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + action_index
                }
            }
        })
    }

    function deleteKios(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/kios/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const action_index = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + action_index
                }
            }
        })
    }

    function deleteAnggaran(index = null, start, length) {
        $.ajax({
            url: "<?= site_url() ?>pasar/anggaran/delete",
            type: 'post',
            data: {
                id: index,
                start: start,
                length: length,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url('user/sign_out') ?>'
                } else {
                    const actionIndex = $('#action_index').val()
                    window.location.href = '<?= site_url('dashboard/pasar/detail/') ?>' + actionIndex
                }
            }
        })
    }

    function getVarianKomoditi(jenis_komoditi, varian_komoditi = null) {
        $.ajax({
            url: "<?= site_url() ?>komoditi/varian/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#varian_komoditi').selectize()[0].selectize.setValue('')
                $('#varian_komoditi').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.success) {
                    $('#varian_komoditi')[0].selectize.enable();
                    $.each(response.data, function(key, value) {
                        $('#varian_komoditi').selectize()[0].selectize.addOption({
                            value: value.varian_komoditi_id,
                            text: value.varian_komoditi
                        })
                    })

                    if (varian_komoditi != null) {
                        $('#varian_komoditi').selectize()[0].selectize.setValue(varian_komoditi);
                    }
                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }

                    $('#varian_komoditi')[0].selectize.disable();
                }
            }
        })
    }

    function getSatuanKomoditi(jenis_komoditi, satuan_komoditi = null) {
        $.ajax({
            url: "<?= site_url() ?>komoditi/satuan/json",
            type: 'post',
            data: {
                jenis_komoditi_id: jenis_komoditi,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#satuan_komoditi').selectize()[0].selectize.setValue('')
                $('#satuan_komoditi').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {
                if (response.success) {
                    $('#satuan_komoditi')[0].selectize.enable();
                    $.each(response.data, function(key, value) {
                        $('#satuan_komoditi').selectize()[0].selectize.addOption({
                            value: value.satuan_komoditi_id,
                            text: value.satuan_komoditi
                        })
                    })

                    if (satuan_komoditi != null) {
                        $('#satuan_komoditi').selectize()[0].selectize.setValue(satuan_komoditi);
                    }
                } else {
                    if (response.code == 401) {
                        window.location.href = '<?= site_url('user/sign_out') ?>'
                    }

                    $('#satuan_komoditi')[0].selectize.disable();
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? '' + rupiah : '');
    }
</script>