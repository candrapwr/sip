<script src="<?= base_url() ?>assets/backend/libs/xls/xlsx.core.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/jhxlsx.js"></script>
<script>
    $(function() {
        $('#post_provinsix').selectize({});
        $('#post_yearx').selectize({});

        console.log($('#post_year').val());

        var prov = '31'
        if('<?=strtolower($this->session->userdata('role'))?>' == 'dinas provinsi' ||  '<?=strtolower($this->session->userdata('role'))?>' == 'dinas kabupaten/kota' || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (provinsi)' || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (kabupaten/kota)'){
            prov = '<?=substr($this->session->userdata('daerah_id'), 0, 2)?>'
            
        }

        $('#post_provinsi').selectize({
            items: [prov]
        });

        if('<?=strtolower($this->session->userdata('role'))?>' == 'dinas provinsi' || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (provinsi)'){

            $('#post_provinsi').selectize()[0].selectize.disable();
            getCity('<?=substr($this->session->userdata('daerah_id'), 0, 2)?>');
            showAjax('<?=substr($this->session->userdata('daerah_id'), 0, 2)?>', '<?=$this->session->userdata('provinsi')?>', 1);
            
        }else if('<?=strtolower($this->session->userdata('role'))?>' == 'dinas kabupaten/kota' || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (kabupaten/kota)'){

            $('#post_provinsi').selectize()[0].selectize.disable();
            getCity('<?=substr($this->session->userdata('daerah_id'), 0, 2)?>');
            showAjax('<?=substr($this->session->userdata('daerah_id'), 0, 4)?>', '<?=$this->session->userdata('kab_kota')?>', 2);

        }else{
            
            getCity($('#post_provinsi').val());
            showAjax($('#post_provinsi').val(), $('#post_provinsi').text(), 1);

        }

        $('#post_year').selectize()[0].selectize.setValue('<?=date('Y')?>')
        $('#post_month').selectize()[0].selectize.setValue('<?=date('m')?>')
        $('#post_kab').selectize();

        $('#post_provinsi').change(function() {
            getCity($(this).val());
        })

        $('#post_kab').change(function() {
            //showAjax($(this).val(),$(this).text(),2);
        })

        $("#btn_cari").click(function() {
            if ($('#post_year').val() != '' && $('#post_month').val() != '') {
                if ($('#post_kab').val() != '') {
                    showAjax($('#post_kab').val(), $('#post_kab').text(), 2);
                } else {
                    showAjax($('#post_provinsi').val(), $('#post_provinsi').text(), 1);
                }
            } else {
                Swal.fire(
                    'GAGAL',
                    'Bulan dan tahun harus di isi',
                    'warning'
                )
            }

        });
    });


    function getCity(provId) {
        $.ajax({
            url: "<?= site_url() ?>kab/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#post_kab').selectize()[0].selectize.setValue('')
                $('#post_kab').selectize()[0].selectize.clearOptions()
                $('#post_kab')[0].selectize.lock()
            },
            complete: function() {
                $.fn.dataTable.tables({
                    visible: true,
                    api: true
                }).columns.adjust();
            },
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (!response.success) {
                        $('#post_kab')[0].selectize.lock();
                    } else {
                        $('#post_kab')[0].selectize.unlock();

                        $.each(response.data, function(i, row) {
                            $('#post_kab').selectize()[0].selectize.addOption({
                                value: row.daerah_id,
                                text: row.kab_kota
                            })
                        })
                    }
                }
            },
            complete: function() {
                if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota'  || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (kabupaten/kota)') {
                    $('#post_kab').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#post_kab').selectize()[0].selectize.disable();
                }
            }
        })
    }

    function showAjax(daerah, str_daerah, pil) {
        $('#divtable').remove();
        $('#content').append('<div id="divtable"><table id="table" class="table table-striped table-bordered" style="width:100%"></table></div>');

        if (pil == 1)
            $('#dataTitle').html('<h5 style="font-size: 18px">INFORMASI DATA PASAR </h5> <span> PROVINSI ' + str_daerah.toUpperCase() + ' - ' + $('#post_month').text().toUpperCase() + ' ' + $('#post_year').val() + '</span>');
        else
            $('#dataTitle').html('<h5 style="font-size: 18px">INFORMASI DATA PASAR </h5> <span>' + str_daerah.toUpperCase() + ' - ' + $('#post_month :selected').text().toUpperCase() + ' ' + $('#post_year :selected').val() + '</span>');

        var table = $('#table').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/perdagangan/monitoring-json/datapasar',
                data: {
                    post_daerah: daerah,
                    post_year: $('#post_year').val(),
                    post_month: $('#post_month').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                beforeSend: function() {
                    showLoader();
                    $('#loader').show()
                    $('#content').hide()
                },
                complete: function() {
                    hideLoader();
                    $('#loader').hide()
                    $('#content').show()

                    $.fn.dataTable.tables({
                        visible: true,
                        api: true
                    }).columns.adjust();
                }
            },
            columns: [{
                    title: 'Nama Pasar',
                    data: 'nama',
                    render: function(k, v, r) {

                        return '<b class="text-primary">' + r.nama + '</b>';

                    }
                },
                {
                    title: 'Kondisi',
                    data: 'kondisi',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.kondisi == 'Rusak Berat')
                            return '<span class="badge rounded-pill bg-danger text-white"><<i class="ti ti-x me-1"></i>' + r.kondisi + '</span>';
                        else if (r.kondisi == 'Rusak Sedang')
                            return '<span class="badge rounded-pill bg-warning text-dark"><i class="fadeIn animated bx bx-shield-x me-1"></i> ' + r.kondisi + '</span>';
                        else if (r.kondisi == 'Rusak Ringan')
                            return '<span class="badge rounded-pill bg-warning text-dark"><i class="fadeIn animated bx bx-band-aid me-1"></i> ' + r.kondisi + '</span>';
                        else if (r.kondisi == 'Baik')
                            return '<span class="badge rounded-pill bg-success"><i class="fadeIn animated bx bx-shield-alt-2 me-1"></i> ' + r.kondisi + '</span>';
                        else
                            return '<span class="badge rounded-pill bg-primary"><i class="fadeIn animated bx bx-badge-check me-1"></i> ' + r.kondisi + '</span>';
                    }
                },
                {
                    title: 'Kab/Kota',
                    data: 'kab_kota',
                },
                {
                    title: 'Kecamatan',
                    data: 'kecamatan',
                },
                {
                    title: 'Kode Pos',
                    data: 'kode_pos',
                },
                {
                    title: 'Kepemilikan',
                    data: 'kepemilikan',
                },
                {
                    title: 'Bentuk Pasar',
                    data: 'bentuk_pasar',
                },
                {
                    title: 'Deskripsi',
                    data: 'deskripsi',
                },
                {
                    title: 'Alamat',
                    data: 'alamat',
                },
            ],
            scrollX: false,
            displayLength: 10
        });
    }

    function get_export() {
        var htmlTmp = ''
    	$.ajax({
    		type: 'POST',
    		url: '<?= base_url() ?>dashboard/perdagangan/monitoring-json/datapasar_export',
    		data: {
    			post_daerah: $('#post_provinsix').val(),
    			post_year: $('#post_yearx').val(),
    			csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
    		},
    		dataType: 'json',
    		beforeSend: function () {
                htmlTmp = $('#btn_export').html()
                $('#btn_export').html('<div class="spinner-border text-primary" role="status"></div>')
            },
    		complete: function () {
                $('#btn_export').html(htmlTmp)
            },
    		success: function (response) {
    			if (response.code == 403) {
    				location.reload();
    			} else {
                    //console.log(response)
    				var result = []
                    var paramJudul = "Rekap Data Pasar Diinput Provinsi " + $('#post_provinsix').text() + ' Tahun ' + $('#post_yearx').val()
    				var paramOpt = {"sheetName": "Sheet1"}
    				var label = ['Nama Pasar', 'Kecamatan','Kab/Kota','Provinsi','Kondisi','Kepemilikan','Bentuk Pasar','Tipe Pasar','Alamat']
                    var kolom = ['nama', 'kecamatan', 'kab_kota', 'provinsi', 'kondisi','kepemilikan','bentuk_pasar','tipe_pasar','alamat']
    				var tmp_field = {}
    				var tmp_data = []
    				var data = []


                    tmp_field = {}
                    tmp_field['merge'] = "0-" + (label.length - 1)
                    tmp_field['style'] = {"font":{"bold":true}}
                    tmp_field['text'] = paramJudul.toUpperCase()
                    tmp_data.push(tmp_field)
                    data.push(tmp_data)

                    tmp_data = []
                    for (var r = 0; r < label.length; r++) {
                    	tmp_field = {}
                        tmp_field['style'] = {"font":{"bold":true}}
                    	tmp_field['text'] = label[r].toUpperCase()
                    	tmp_data.push(tmp_field)
                    }
                    data.push(tmp_data)

    				$.each(response.data, function (i, row) {
    					tmp_data = []
    					for (var r = 0; r < kolom.length; r++) {
    						tmp_field = {}
    						tmp_field['text'] = row[kolom[r]]
    						tmp_data.push(tmp_field)
    					}
    					data.push(tmp_data)
    				})
    				paramOpt['data'] = data
    				result.push(paramOpt)
    				var options = {
    					fileName: paramJudul,
                        header: true,
    				};
    				Jhxlsx.export(result, options);
    				//console.log(JSON.stringify(result));
    			}
    		}
    	})
    }

    if (typeof PerfectScrollbar != 'function') {
        function PerfectScrollbar() {}
    }
</script>