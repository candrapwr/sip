<script>
    $(function() {
        let provId = $('#filter-province').val()
        let cityId = $('#filter-city').val()
        let id = $('#filter-name').val()
        let startDate = $('#start-date').val()
        let endDate = $('#end-date').val()

        showRekap('refresh', provId, cityId, id, startDate, endDate)

        $('#filter-province').selectize()
        $('#filter-city').selectize()
        $('#filter-name').selectize()
        $('#filter-month').selectize()
        $('#filter-year').selectize()

        $('#filter-search').click(function() {
            let provId = $('#filter-province').val()
            let cityId = $('#filter-city').val()
            let id = $('#filter-name').val()
            let startDate = $('#start-date').val()
            let endDate = $('#end-date').val()

            showRekap('refresh', provId, cityId, id, startDate, endDate)
        })

        $('#filter-province').change(function() {
            const provId = $(this).val()

            getCity(provId)
        })
    })


    function showRekap(refresh = null, provId = null, cityId = null, id = null, start_date = null, end_date = null) {
        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({

            responsive: false,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            dom: 'Bfrtip',
            buttons: [{
                    "extend": 'excel',
                    "text": '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
                    "titleAttr": 'Excel',
                    "action": newexportaction
                },
                {
                    "extend": 'csv',
                    "text": '<i class="ti ti-file-type-csv"></i>CSV',
                    "titleAttr": 'CSV',
                    "action": newexportaction
                },
            ],
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/perdagangan/absensi/rekap/json',
                data: {
                    refresh: refresh,
                    province_id: provId,
                    city_id: cityId,
                    id: id,
                    start_date: start_date,
                    end_date: end_date,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Nama',
                    data: 'nama_lengkap',
                },
                {
                    title: 'Email',
                    data: 'email',
                    "visible": false
                },
                {
                    title: 'Checkin Pasar ID',
                    data: 'checkin_pasar_id',
                    "visible": false
                },
                {
                    title: 'Nama Pasar',
                    data: 'checkin_nama_pasar',
                },
                {
                    title: 'Checkin',
                    data: 'checkin_time',
                },
                {
                    title: 'Checkout',
                    data: 'checkout_time',
                },
                {
                    title: 'Latitude',
                    data: 'latitude',
                },
                {
                    title: 'Longitude',
                    data: 'longitude',
                },
                {
                    title: 'Radius (Km)',
                    render: function(k, v, r) {
                        return (r.checkin_radius / 1000).toFixed(2)
                    },
                },
                {
                    title: 'Checkout Pasar',
                    data: 'checkout_nama_pasar',
                    "visible": false
                },
                {
                    title: 'Checkout Time',
                    data: 'checkout_time',
                    "visible": false
                },
                {
                    title: 'Checkout Latlong Pasar',
                    data: 'checkout_latlong',
                    "visible": false
                },
                {
                    title: 'Foto Checkin',
                    render: function(k, v, r) {
                        return '<img width="50" height="60" src="' + r.checkin_photo + '">'
                    },
                    className: 'text-center',
                    "visible": true
                },
                {
                    title: 'Url Foto Checkin',
                    data: 'checkin_photo',
                    "visible": false
                },
                {
                    title: 'Foto Checkout',
                    render: function(k, v, r) {
                        return '<img width="50" height="60" src="' + r.checkout_photo + '">'
                    },
                    className: 'text-center',
                    "visible": true
                },
                {
                    title: 'Url Foto Checkout',
                    data: 'checkout_photo',
                    "visible": false
                },
                {
                    title: 'Deskripsi Dokumen',
                    data: 'deskripsi',
                },
                {
                    title: 'Foto Dokumen',
                    render: function(k, v, r) {
                        if (r.photo != null)
                            return '<a href="' + r.photo + '" target="_blank"><img width="50" height="60" src="' + r.photo + '"></a>'
                        else
                            return '-'
                    },
                    className: 'text-center',
                    "visible": true
                },
                {
                    title: 'Dok Url',
                    data: 'photo',
                    "visible": false
                },
            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function getCity(provId) {
        $.ajax({
            url: "<?= site_url() ?>kab/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#filter-city').selectize()[0].selectize.setValue('')
                $('#filter-city').selectize()[0].selectize.clearOptions()
                $('#filter-city')[0].selectize.lock()
            },
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (!response.success) {
                        $('#filter-city')[0].selectize.lock();
                    } else {
                        $('#filter-city')[0].selectize.unlock();

                        $.each(response.data, function(i, row) {
                            $('#filter-city').selectize()[0].selectize.addOption({
                                value: row.daerah_id,
                                text: row.kab_kota
                            })
                        })
                    }
                }
            }
        })
    }

    //export
    function newexportaction(e, dt, button, config) {
        var self = this;
        var oldStart = dt.settings()[0]._iDisplayStart;
        dt.one('preXhr', function(e, s, data) {
            // Just this once, load all data from the server...
            data.start = 0;
            data.length = 2147483647;
            dt.one('preDraw', function(e, settings) {
                // Call the original action function
                if (button[0].className.indexOf('buttons-copy') >= 0) {
                    $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-excel') >= 0) {
                    $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-csv') >= 0) {
                    $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
                    $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
                        $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
                        $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
                } else if (button[0].className.indexOf('buttons-print') >= 0) {
                    $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
                }
                dt.one('preXhr', function(e, s, data) {
                    // DataTables thinks the first item displayed is index 0, but we're not drawing that.
                    // Set the property to what it was before exporting.
                    settings._iDisplayStart = oldStart;
                    data.start = oldStart;
                });
                // Reload the grid with the original page. Otherwise, API functions like table.cell(this) don't work properly.
                setTimeout(dt.ajax.reload, 0);
                // Prevent rendering of the full data to the DOM
                return false;
            });
        });
        // Requery the server with the new one-time export settings
        dt.ajax.reload();
    };
</script>