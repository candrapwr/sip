<script>
    $('#platform-name, #produsen-name, #kabkota-name').selectize()

    $(document).ready(function() {
        showTable('refresh');
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">- Pilih Kabupaten/Kota -</option>')

        $('#provinsi-name').on('change', function() {
            var daerah_id = $("#provinsi-name").val();
            getDaerah('kabkota', daerah_id)
        })

    })

    function fill() {
        showTable('refresh');
    }

    function showTable(refresh = null) {

        var platform = $('#platform-name').val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({

            responsive: false,
            processing: true,
            serverSide: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    platform: platform,
                    refresh: refresh,
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getPengecer',
            },
            columns: [{
                    title: 'No',
                    data: 'api_log_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Kode',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<span class="badge bg-secondary">#' + ((platform == 'gurih') ? r.kode_pengecer : r.kode_stockpoint) + '</span>'
                    }
                },
                {
                    title: 'Nama Pengecer',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return ((platform == 'gurih') ? r.nama_pengecer : r.nama_stockpoint)
                    }
                },
                {
                    title: 'Nama Penanggung Jawab',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return ((platform == 'gurih') ? r.nama_penanggung_jawab : r.nama_penanggungjawab)
                    }
                },
                {
                    title: 'No. Telp',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<a href="tel:' + ((platform == 'gurih') ? r.no_telepon : '#') + '">' + ((platform == 'gurih') ? r.no_telepon : '-') + '</a>'
                    }
                },
                {
                    title: 'Alamat Pengecer',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {

                        if (r.titik_koordinat != '' && r.titik_koordinat != null && r.titik_koordinat != '0,0') {
                            return `<p>` + ((platform == 'gurih') ? r.alamat_pengecer : r.alamat_stockpoint) + `</p>
                            <a class="btn btn-sm btn-primary" href="http://maps.google.com/maps?q=` + r.titik_koordinat + `" target="_blank"><i class="ti ti-map-pin me-1"></i>Lihat Google Maps</a>`
                        } else {
                            return '<p>' + ((platform == 'gurih') ? r.alamat_pengecer : r.alamat_stockpoint) + '</p>'
                        }
                    }
                },

            ],
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>