<script type="text/javascript" src="<?= base_url() ?>assets/backend/libs/xls/xlsx.full.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script>
    $(function() {
        $('#filter-province').selectize()
        $('#filter-month').selectize()
        $('#filter-year').selectize()
        $('#post_kab').selectize()
        $('#filter-province').selectize()[0].selectize.disable();

        showData()
        getCity($('#filter-province').val());

        $('#filter-province').change(function() {
            getCity($(this).val());
        })

        $('#filter').click(function() {
            const provId = $('#filter-province').val()
            const month = $('#filter-month').val()
            const year = $('#filter-year').val()

            showData(provId, month, year)
        })
    })

    function showData(provId = null, month = null, year = null) {

        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/inputHarga",
            type: 'post',
            data: {
                province_id: provId,
                month: month,
                year: year,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                showLoader();
                $('#loader').show()
                $('#content').hide()
                $('#thead').empty()
                $('#tbody').empty()
            },
            complete: function() {
                hideLoader();
                $('#content').show()
                $('#loader').hide()
            },
            success: function(response) {

                $('#province-title').text(response.result.title)

                let thead = '<tr>' +
                    '<th width="30%" class="tengah" rowspan="2" style="background-color: #E70A2B; color:white; vertical-align : middle;text-align:center;">NAMA PASAR<i class="ti ti-sort-ascending-2 ms-1"></i></th>' +

                    '<th class="tengah text-center" colspan="31" style="background-color: #E70A2B; color:white;">' + response.result.headTitle + '<i class="ti ti-sort-ascending-2 ms-1"></i></th>' +
                    '</tr>'

                let listDate = ''
                $.each(response.result.date, function(i, date) {
                    listDate += '<th class="tengah" style="background-color: #E70A2B; color:white;">' + date + '<i class="ti ti-sort-ascending-2 ms-1"></i> </th>'
                })

                thead += '<tr>' + listDate + '</tr>'

                $('#thead').html(thead)

                let tbody = ''
                $.each(response.result.data, function(i, row) {
                    tbody += '<tr class="item">' +
                        '<td><b><a href="javascript:void(0);"></a>' + row.nama_pasar + '</b></td>'

                    if (row.status_lapor.length > 0) {
                        console.log(response.result.date.length)
                        $.each(row.status_lapor, function(i, status) {
                            // abang
                            let bgColor = '#F23939'
                            if (status == 'Belum Lengkap') {
                                // kuning
                                bgColor = '#F9AE2B'
                            } else if (status == 'Lengkap') {
                                // ijo
                                bgColor = '#01875f'
                            }

                            tbody += '<td style="background-color: ' + bgColor + ' ;color:#FFF;"></td>'
                        })
                        for (let q = 0; q < (response.result.date.length - row.status_lapor.length); q++) {
                            tbody += '<td style="background-color: #F23939 ;color:#FFF;"></td>'
                        }
                    } else {
                        $.each(response.result.date, function(i, date) {
                            tbody += '<td style="background-color:#E1F3FF;color:#FFF;"></td>'
                        })
                    }

                    tbody += '</tr>'
                })

                $('#tbody').html(tbody)
            }
        })
    }

    function getCity(provId) {
        $.ajax({
            url: "<?= site_url() ?>kab/json",
            type: 'post',
            data: {
                provinsi_id: provId,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: 'json',
            beforeSend: function() {
                $('#post_kab').selectize()[0].selectize.setValue('')
                $('#post_kab').selectize()[0].selectize.clearOptions()
                $('#post_kab')[0].selectize.lock()
            },
            complete: function() {
                $.fn.dataTable.tables({
                    visible: true,
                    api: true
                }).columns.adjust();
            },
            success: function(response) {
                if (response.code == 401) {
                    window.location.href = '<?= site_url() ?>user/sign_out'
                } else {
                    if (!response.success) {
                        $('#post_kab')[0].selectize.lock();
                    } else {
                        $('#post_kab')[0].selectize.unlock();

                        $.each(response.data, function(i, row) {

                            $('#post_kab').selectize()[0].selectize.addOption({
                                value: row.daerah_id + '|' + row.kab_kota,
                                text: row.kab_kota
                            })

                        })
                    }
                }
            },
            complete: function() {
                if ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota'  || '<?=strtolower($this->session->userdata('role'))?>' == 'kontributor sp2kp (kabupaten/kota)') {
                    $('#post_kab').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>|<?= $this->session->userdata('kab_kota') ?>')
                    $('#post_kab').selectize()[0].selectize.disable();
                }
            }
        })
    }

    function html_table_to_excel(type) {
        var data = document.getElementById('main_table');
        var file = XLSX.utils.table_to_book(data, {
            sheet: "sheet1"
        });
        var wbout = XLSX.write(file, {
            bookType: type,
            bookSST: true,
            type: 'binary'
        });
        saveAs(new Blob([s2ab(wbout)], {
            type: "application/octet-stream"
        }), "Tabel Monitoring Input Harga Bahan Pokok " + $('#filter-province').text() + ".xlsx")
    }

    function s2ab(s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i)
            view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
</script>