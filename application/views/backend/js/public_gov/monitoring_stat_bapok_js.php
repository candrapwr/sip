<script src="<?= base_url() ?>assets/backend/libs/apexcharts/apexcharts.min.js"></script>
<script>
	var saveProvinsi = {
		val: '',
		text: ''
	};
	var saveTahun = {
		val: '',
		text: ''
	};

	var produksi_series = [];
	var produksi_labels = [];
	var konsumsi_series = [];
	var konsumsi_labels = [];
	var dataSatuan = []

	$('#loader').hide()

	$('#post_provinsi').selectize({
		items: ["<?=substr($this->session->userdata('daerah_id'), 0, 2)?>"]
	});
	$('#post_year').selectize({
		items: ["<?= date('Y') ?>"]
	});
	$('#post_komoditi').selectize({
		items: ["Bapok"]
	});

	$('#post_provinsi').change(function() {
		saveProvinsi['val'] = $(this).val();
		saveProvinsi['text'] = $(this).text();
		showData();
	});
	$('#post_year').change(function() {
		saveTahun['val'] = $(this).val();
		saveTahun['text'] = $(this).text();
		showData();
	});

	saveProvinsi['val'] = $('#post_provinsi').val();
	saveProvinsi['text'] = $('#post_provinsi').text();
	saveTahun['val'] = $('#post_year').val();
	saveTahun['text'] = $('#post_year').text();
	showData();

	function showAjax() {
		$('#chart1').remove();
		$('#c_chart1').append('<div id="chart1"></div>');
		$('#chart2').remove();
		$('#c_chart2').append('<div id="chart2"></div>');

		$.ajax({
			url: "<?= site_url() ?>dashboard/perdagangan/monitoring-json/profil",
			type: 'post',
			data: {
				post_daerah: saveProvinsi.val,
				post_year: saveTahun.val,
				post_kelompok: 'Bapok',
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			},
			dataType: 'json',
			beforeSend: function() {
				showLoader();
				$('#loader').show()
				$('#content').hide()
			},
			complete: function() {},
			success: function(response, textStatus, xhr) {
				//console.log(response);
				produksi_series = [];
				produksi_labels = [];
				konsumsi_series = [];
				konsumsi_labels = [];
				dataSatuan = []
				produksikMap = {}
				konsumsikMap = {}
				if (response.kode == 200) {
					$.each(response.data, function(index, value) {

						produksi_labels.push(value.jenis_komoditi);
						if (value.produksi != null){
							dataSatuan[value.jenis_komoditi] = value.satuan_komoditi;
							produksi_series.push(parseInt(value.produksi));
						}else{
							produksi_series.push(0);
						}

						konsumsi_labels.push(value.jenis_komoditi);
						if (value.konsumsi != null){
							dataSatuan[value.jenis_komoditi] = value.satuan_komoditi;
							konsumsi_series.push(parseInt(value.konsumsi));
						}else{
							konsumsi_series.push(0);
						}
					});

					produksikMap['name'] = 'Produksi'
					produksikMap['data'] = produksi_series
					konsumsikMap['name'] = 'Konsumsi'
					konsumsikMap['data'] = konsumsi_series


					$('#loader').LoadingOverlay("hide");
					$('#loader').hide();
					$('#content').show();

					showChart1();
					console.log(produksi_series)

				} else if (response.kode == 404) {
					$('#loader').LoadingOverlay("hide");
					$('#loader').hide();
					$('#content').show();
					$('#dataTitle').html('<h5 class="text-center" style="font-size: 18px">STATISTIK PRODUKSI KONSUMSI BARANG KEBUTUHAN POKOK </h5> <span>PROVINSI ' + saveProvinsi.text.toUpperCase() + ' ' + saveTahun.val + '</span>');
				} else {
					location.reload();
				}
			},
			error: function(request, status, error) {
				location.reload();
			}
		})
	}

	function showData() {
		if (saveProvinsi.val != '' && saveTahun.val != '') {
			$('#dataTitle').html('<h5 class="text-center" style="font-size: 18px">STATISTIK PRODUKSI KONSUMSI BARANG KEBUTUHAN POKOK </h5> <span> PROVINSI ' + saveProvinsi.text.toUpperCase() + ' ' + saveTahun.val + '</span>');
			showAjax();
		}
	}

	function showChart1() {
		var options = {
			series: [produksikMap, konsumsikMap],
			chart: {
				type: 'bar',
				height: 350
			},
			plotOptions: {
				bar: {
					horizontal: false,
				},
			},
			dataLabels: {
				enabled: false
			},
			stroke: {
				show: true,
				width: 2,
				colors: ['transparent']
			},
			yaxis: {
				showAlways: true,
				labels: {
					formatter: function (num) {
						return abbreviateNumber(num)
					}
				}
			},
			labels: produksi_labels,
			title: {
				text: '',
				align: 'center'
			},
			tooltip: {
				y: {
					formatter: function (value) {
						return formatter.format(value);
					},
					title: {
						formatter: function (seriesName,opt) {
							return seriesName + ' (' + dataSatuan[opt.w.globals.labels[opt.dataPointIndex]] + ')';
						}
					}
				}
			},
			legend: {
				position: 'top',
				horizontalAlign: 'center',
				floating: false,
				offsetY: 5,
				offsetX: 0
			}
		};

		var chart = new ApexCharts(document.querySelector("#chart1"), options);
		chart.render();
	}

	//show loading
	function showLoader() {
		$('#loader').LoadingOverlay('show', {
			background: "rgba(253, 253, 255, 0.8)",
			image: "<?= base_url() ?>assets/brand/load-data.gif",
			imageAnimation: 'none',
			text: "Dalam Proses. Mohon Tunggu...",
			textAutoResize: true,
			textResizeFactor: 0.15,
			textColor: "#E70A2B"
		})
	}

	var formatter = new Intl.NumberFormat('en-US');

	//HiDE loading
	function hideLoader() {
		$('#loader').LoadingOverlay('hide')
	}

	var SI_SYMBOL = ["", "RB", "JT", "M", "T", "B"];
	function abbreviateNumber(number){
		var tier = Math.log10(Math.abs(number)) / 3 | 0;
		if(tier == 0) return number.toFixed(1);
		var suffix = SI_SYMBOL[tier];
		var scale = Math.pow(10, tier * 3);
		var scaled = number / scale;
		return scaled.toFixed(0) + suffix;
	}
</script>