<script src="<?= base_url() ?>assets/backend/libs/apexcharts/apexcharts.min.js"></script>
<script>
	var saveProvinsi = {
		val: '',
		text: ''
	};
	var saveTahun = {
		val: '',
		text: ''
	};
	var saveKomoditi = {
		val: '',
		text: ''
	};
	var produksi_series = [];
	var produksi_labels = [];
	var konsumsi_series = [];
	var konsumsi_labels = [];
	var dataSatuan = []

	$('#loader').hide()

	$('#post_provinsi').selectize({
		items: ["<?=substr($this->session->userdata('daerah_id'), 0, 2)?>"]
	});
	$('#post_year').selectize({
		items: ["<?= date('Y') ?>"]
	});
	$('#post_komoditi').selectize({
		items: ["Bapok"]
	});

	$('#post_provinsi').change(function() {
		saveProvinsi['val'] = $(this).val();
		saveProvinsi['text'] = $(this).text();
		showData();
	});
	$('#post_year').change(function() {
		saveTahun['val'] = $(this).val();
		saveTahun['text'] = $(this).text();
		showData();
	});
	$('#post_komoditi').change(function() {
		saveKomoditi['val'] = $(this).val();
		saveKomoditi['text'] = $(this).text();
		showData();
	});

	saveProvinsi['val'] = $('#post_provinsi').val();
	saveProvinsi['text'] = $('#post_provinsi').text();
	saveTahun['val'] = $('#post_year').val();
	saveTahun['text'] = $('#post_year').text();
	saveKomoditi['val'] = $('#post_komoditi').val();
	saveKomoditi['text'] = $('#post_komoditi').text();
	showData();

	function showAjax() {
		$('#chart1').remove();
		$('#c_chart1').append('<div id="chart1"></div>');
		$('#chart2').remove();
		$('#c_chart2').append('<div id="chart2"></div>');

		$.ajax({
			url: "<?= site_url() ?>dashboard/perdagangan/monitoring-json/profil",
			type: 'post',
			data: {
				post_daerah: saveProvinsi.val,
				post_year: saveTahun.val,
				post_kelompok: saveKomoditi.val,
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			},
			dataType: 'json',
			beforeSend: function() {
				showLoader();
				$('#loader').show()
				$('#content').hide()
			},
			complete: function() {},
			success: function(response, textStatus, xhr) {
				console.log(response);
				produksi_series = [];
				produksi_labels = [];
				konsumsi_series = [];
				konsumsi_labels = [];
				dataSatuan = []
				if (response.kode == 200) {
					$.each(response.data, function(index, value) {

						produksi_labels.push(value.jenis_komoditi);
						if (value.produksi != null){
							dataSatuan[value.jenis_komoditi] = value.satuan_komoditi;
							produksi_series.push(parseInt(value.produksi));
						}else{
							produksi_series.push(0);
						}

						konsumsi_labels.push(value.jenis_komoditi);
						if (value.konsumsi != null){
							dataSatuan[value.jenis_komoditi] = value.satuan_komoditi;
							konsumsi_series.push(parseInt(value.konsumsi));
						}else{
							konsumsi_series.push(0);
						}
					});

					$('#loader').LoadingOverlay("hide");
					$('#loader').hide();
					$('#content').show();

					showChart1();
					showChart2();
					//console.log(dataSatuan['Bawang'])

				} else if (response.kode == 404) {
					$('#loader').LoadingOverlay("hide");
					$('#loader').hide();
					$('#content').show();
					$('#dataTitle').html('<h5 class="text-center" style="font-size: 18px">PROFIL PERDAGANGAN ' + saveKomoditi.text.toUpperCase() + ' </h5> <span>PROVINSI ' + saveProvinsi.text.toUpperCase() + ' ' + saveTahun.val + '</span>');
				} else {
					location.reload();
				}
			},
			error: function(request, status, error) {
				location.reload();
			}
		})
	}

	function showData() {
		if (saveProvinsi.val != '' && saveTahun.val != '' && saveKomoditi.val != '') {
			$('#dataTitle').html('<h5 class="text-center" style="font-size: 18px">PROFIL PERDAGANGAN ' + saveKomoditi.text.toUpperCase() + ' </h5> <span> PROVINSI ' + saveProvinsi.text.toUpperCase() + ' ' + saveTahun.val + '</span>');
			showAjax();
		}
	}

	function showChart1(xx) {
		var options = {
			series: produksi_series,
			chart: {
				type: 'pie',
			},
			labels: produksi_labels,
			title: {
				text: 'PRODUKSI',
				align: 'center'
			},
			tooltip: {
				y: {
					formatter: function (value) {
						return formatter.format(value);
					},
					title: {
						formatter: function (seriesName) {
							return seriesName + ' (' + dataSatuan[seriesName] + ')';
						}
					}
				}
			},
			legend: {
				position: 'bottom',
				horizontalAlign: 'center',
				floating: false,
				offsetY: 5,
				offsetX: 0
			}
		};

		var chart = new ApexCharts(document.querySelector("#chart1"), options);
		chart.render();
	}

	function showChart2() {
		var options2 = {
			series: konsumsi_series,
			chart: {
				type: 'pie',
			},
			animations: {
				enabled: true
			},
			labels: konsumsi_labels,
			title: {
				text: 'KONSUMSI',
				align: 'center'
			},
			tooltip: {
				y: {
					formatter: function (value) {
						return formatter.format(value);
					},
					title: {
						formatter: function (seriesName) {
							return seriesName + ' (' + dataSatuan[seriesName] + ')';
						}
					}
				}
			},
			legend: {
				position: 'bottom',
				horizontalAlign: 'center',
				floating: false,
				offsetY: 5,
				offsetX: 0
			}
		};

		var chart2 = new ApexCharts(document.querySelector("#chart2"), options2);
		chart2.render();
	}

	//show loading
	function showLoader() {
		$('#loader').LoadingOverlay('show', {
			background: "rgba(253, 253, 255, 0.8)",
			image: "<?= base_url() ?>assets/brand/load-data.gif",
			imageAnimation: 'none',
			text: "Dalam Proses. Mohon Tunggu...",
			textAutoResize: true,
			textResizeFactor: 0.15,
			textColor: "#E70A2B"
		})
	}

	var formatter = new Intl.NumberFormat('en-US');

	//HiDE loading
	function hideLoader() {
		$('#loader').LoadingOverlay('hide')
	}
</script>