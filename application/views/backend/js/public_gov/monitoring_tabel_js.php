<script type="text/javascript" src="<?= base_url() ?>assets/backend/libs/xls/xlsx.full.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script>
	var daerahSave = '';
	var dataSer = [];

	$('#post_yearx').selectize({});
	$("#post_year").val("<?= date('Y') ?>").change();
	showAjax("<?= date('Y') ?>");

	$('#post_year').change(function() {
		if ($(this).val() != "") {
			if (daerahSave == '')
				showAjax($(this).val());
			else
				showAjax($(this).val(), daerahSave);
		} else {
			$('#data-main').html('<center><img src="<?= base_url() ?>assets/frontend/img/default-nodata.svg"></center>');
		}
	});

	function showBack(tahun) {
		daerahSave = '';
		showAjax(tahun);
	}

	function showAjax(tahun, daerah = '') {
		var postData;
		if (daerah == '') {
			postData = {
				post_year: tahun,
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			};
		} else {
			postData = {
				post_year: tahun,
				daerah_id: daerah,
				csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
			};
			daerahSave = daerah;
		}
		$('#data-main').html('<lottie-player src="<?= base_url() ?>assets/brand/load-data.json" background="transparent" speed="1" style="width:100px" class="mx-auto d-block" loop autoplay></lottie-player>');
		$.ajax({
			url: "<?= site_url() ?>dashboard/perdagangan/monitoring-ajax",
			type: 'post',
			data: postData,
			success: function(response, textStatus, xhr) {
				var inHtml = response.search("PEMANTAUAN");
				if (inHtml > 0) {
					$('#data-main').html(response);
					if (daerah != '')
						showChart();
				} else {
					location.reload();
				}
			},
			error: function(request, status, error) {
				location.reload();
			}
		})
	}

	function showChart() {
		var options = {
			series: dataSer,
			chart: {
				type: 'pie',
			},
			labels: ['INPUT', 'TIDAK INPUT'],
			legend: {
				position: 'bottom',
				horizontalAlign: 'center',
				floating: false,
				offsetY: 5,
				offsetX: 0
			}
		};

		var chart = new ApexCharts(document.querySelector("#chart"), options);
		chart.render();
	}

	function html_table_to_excel(type)
    {
        var data = document.getElementById('main_table');
        var file = XLSX.utils.table_to_book(data, {sheet: "sheet1"});
		var wbout = XLSX.write(file, { bookType: type, bookSST: true, type: 'binary' });
		saveAs(new Blob([s2ab(wbout)], {type: "application/octet-stream"}), "Tabel Pemantauan Provinsi Tahun " + $('#post_yearx').text() + ".xlsx")
    }

	function s2ab (s) {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i = 0; i != s.length; ++i)
            view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }

	if (typeof PerfectScrollbar != 'function') {
		function PerfectScrollbar() {}
	}
</script>