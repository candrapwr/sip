<script>
    $(document).ready(function () {
        showTable('refresh');
        $('#platform-name, #provinsi-name, #kabkota-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">- Pilih Kabupaten/Kota -</option>')

    })

    function fill() {
        showTable('refresh');
    }


    $('#provinsi-name').on('change', function () {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)
        // showTable('refresh');

    })

    $('#kabkota-name').on('change', function () {
        var daerah_id = $("#kabkota-name").val();
        // showTable('refresh');

    })

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    platform: platform,
                    nib: $('#nib').val(),
                    nama: $('#nama').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getD1SIMIRAH',
            },
            columns: [{
                title: 'No',
                data: 'id_produsen',
                className: 'text-center',
                orderable: false,
                render: function (k, v, r, m) {
                    return (m.settings._iDisplayStart) + (m.row + 1)
                }
            },
            {
                title: 'Nama Produsen',
                data: 'nama_produsen',
                orderable: false,
                render: function (k, v, r) {
                    return `<dl class="row mb-0">
                                    <dt class="col-sm-12">Nama</dt>
                                    <dd class="col-sm-12">` + r.nama + `</dd>
                                    <dt class="col-sm-12">NIB</dt>
                                    <dd class="col-sm-12">` + r.nib + `</dd>
                                    <dt class="col-sm-12">NPWP</dt>
                                    <dd class="col-sm-12">` + r.npwp + `</dd>
                                    <dt class="col-sm-12">Type</dt>
                                    <dd class="col-sm-12">` + r.type + `</dd>
                                </dl>`
                }
            },
            {
                title: 'Kontak',
                data: 'id_produsen',
                orderable: false,
                render: function (k, v, r) {
                    return `<dl class="row mb-0">
                                    <dt class="col-sm-12">Telp</dt>
                                    <dd class="col-sm-12">`+ ((r.telp_perusahaan == null) ? '-' : r.telp_perusahaan) + `</dd>
                                    <dt class="col-sm-12">Email</dt>
                                    <dd class="col-sm-12">` + r.email + `</dd>
                                </dl>`
                },
            },
            {
                title: 'Penanggung Jawab',
                data: 'id_produsen',
                orderable: false,
                render: function (k, v, r) {
                    return `<dl class="row mb-0">
                                    <dt class="col-sm-12">Nama</dt>
                                    <dd class="col-sm-12">`+ r.pic + `</dd>
                                    <dt class="col-sm-12">Telp</dt>
                                    <dd class="col-sm-12">` + r.no_pic + `</dd>
                                </dl>`
                }
            },
            {
                title: 'Alamat',
                data: 'alamat',
                orderable: false,
                render: function (k, v, r) {

                    return '<p>' + r.alamat + '</p>'
                }
            },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>