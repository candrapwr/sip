<script src="<?= base_url() ?>assets/backend/libs/xls/xlsx.core.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/FileSaver.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/xls/jhxlsx.js"></script>
<script>
    $(function() {
        $('#commodity-province').selectize()
        $('#commodity-year').selectize()
        $('#commodity-month').selectize()
        $('#province').selectize()
        $('#commodity').selectize()
        $('#unit').selectize()
        $('#post_provinsix').selectize({items: ["31"]});
        $('#post_yearx').selectize({items: ["<?=date('Y') ?>"]});
        $('#post_bulanx').selectize({});

        showData('refresh')

        $('#filter-commodity').click(function() {
            const province = $('#commodity-province').val()
            const year = $('#commodity-year').val()
            const month = $('#commodity-month').val()

            showData('refresh', province, year, month)
        })
    })

    function showData(refresh = null, province = null, year = null, month = null) {
        if ($.fn.DataTable.isDataTable('#table-commodity')) {
            $("#table-commodity").dataTable().fnDestroy();
            $('#table-commodity').empty();
        }

        var table = $('#table-commodity').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            searching: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>dashboard/perdagangan/provinsi/json',
                data: {
                    refresh: refresh,
                    province: province,
                    year: year,
                    month: month,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'Tanggal',
                    data: 'tanggal',
                }, {
                    title: 'Komoditas',
                    data: 'jenis_komoditi',
                },
                {
                    title: 'Produksi',
                    data: 'produksi',
                },
                {
                    title: 'Konsumsi',
                    data: 'konsumsi',
                },
            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function get_export() {
        var htmlTmp = ''
    	$.ajax({
    		type: 'POST',
    		url: '<?= base_url() ?>dashboard/perdagangan/monitoring-json/produksikonsumsi_export',
    		data: {
    			post_daerah: $('#post_provinsix').val(),
    			post_year: $('#post_yearx').val(),
                post_bulan: $('#post_bulanx').val(),
    			csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
    		},
    		dataType: 'json',
    		beforeSend: function () {
                htmlTmp = $('#btn_export').html()
                $('#btn_export').html('<div class="spinner-border text-primary" role="status"></div>')
            },
    		complete: function () {
                $('#btn_export').html(htmlTmp)
            },
    		success: function (response) {
    			if (response.code == 403) {
    				location.reload();
    			} else {
                    //console.log(response)
                    var bulaText = ''
                    if($('#post_bulanx').val() != '-')
                        bulaText = ' Bulan ' + $('#post_bulanx').text()
                    else 
                        bulaText = ''

    				var result = []
                    var paramJudul = "Rekap Data Produksi Konsumsi " + $('#post_provinsix').text() + " Tahun " + $('#post_yearx').val() + bulaText
    				var paramOpt = {"sheetName": "Sheet1"}
    				var label = ['Provinsi', 'Bulan', 'Tahun', 'Komoditas','Produksi','Konsumsi','Satuan']
                    var kolom = ['provinsi', 'bulan', 'tahun', 'jenis_komoditi', 'produksi', 'konsumsi', 'satuan_komoditi']
    				var tmp_field = {}
    				var tmp_data = []
    				var data = []

                    tmp_field = {}
                    tmp_field['merge'] = "0-" + (label.length - 1)
                    tmp_field['style'] = {"font":{"bold":true}}
                    tmp_field['text'] = paramJudul.toUpperCase()
                    tmp_data.push(tmp_field)
                    data.push(tmp_data)

                    tmp_data = []
                    for (var r = 0; r < label.length; r++) {
                    	tmp_field = {}
                        tmp_field['style'] = {"font":{"bold":true}}
                    	tmp_field['text'] = label[r].toUpperCase()
                    	tmp_data.push(tmp_field)
                    }
                    data.push(tmp_data)

    				$.each(response.data, function (i, row) {
    					tmp_data = []
    					for (var r = 0; r < kolom.length; r++) {
    						tmp_field = {}
                            tmp_field['text'] = row[kolom[r]]
    						tmp_data.push(tmp_field)
    					}
    					data.push(tmp_data)
    				})
    				paramOpt['data'] = data
    				result.push(paramOpt)
    				var options = {
    					fileName: paramJudul,
                        header: true,
    				};
    				Jhxlsx.export(result, options);
    				//console.log(JSON.stringify(result));
    			}
    		}
    	})
    }
</script>