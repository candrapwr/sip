<script>
    $(document).ready(function() {
        showTable('refresh');
        $('#platform-name, #provinsi-name, #kabkota-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">-- Pilih Kabupaten/Kota --</option>')

    })



    function fill() {
        showTable('refresh');
    }

    // $('#provinsi-name').on('change', function() {
    //     var daerah_id = $("#provinsi-name").val();
    //     getDaerah('kabkota', daerah_id)
    //     showTable('refresh');

    // })

    // $('#kabkota-name').on('change', function() {
    //     var daerah_id = $("#kabkota-name").val();
    //     showTable('refresh');

    // })

    var kepmendag = ''


    function showTable(refresh = null) {

        var data_post = {
            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
            refresh: refresh,
            tgl_awal: $('#tgl_awal').val(),
            tgl_akhir: $('#tgl_akhir').val(),
            no_dokumen: $('#no_dokumen').val(),
            nama_pengirim: $('#nama_pengirim').val(),
            type: $('#type').val(),
            kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
            title: 'Rekap Jumlah Distribusi SIMIRAH' + (($('#tgl_awal').val() == '') ? '' : ' ' + $('#tgl_awal').val() + ' - ' + $('#tgl_akhir').val() + '')
        }

        var alokasi = {
            'alokasi': true,
            'refresh': true
        }

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }
        var table = $('#tbl_general').DataTable({
            dom: 'Brtip',
            buttons: ((data_post.tgl_awal != '' && data_post.tgl_akhir != '')) ? [{
                text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
                action: function(e, dt, node, config) {
                    $.ajax({
                        url: '<?= base_url() ?>web/mirah-export',
                        type: 'POST',
                        data: data_post,
                        success: function(result) {
                            window.open('<?= base_url() ?>web/mirah-download', '_blank');
                        }
                    })
                }
            }
            // , {
            //     text: 'Ambil Alokasi',
            //     action: function(e, dt, node, config) {
            //         $.ajax({
            //             url: '<?= base_url() ?>perdagangan/migor/getSummarySIMIRAH',
            //             type: 'POST',
            //             beforeSend: function() {
            //                 $('body').LoadingOverlay('show');
            //             },
            //             data: $.extend(alokasi, data_post),
            //             success: function(result) {
            //                 result = JSON.parse(result)
            //                 if (result.kode == 200) {
            //                     Swal.fire({
            //                         title: 'Alokasi Berhasil Diambil',
            //                         icon: 'success',
            //                         confirmButtonColor: '#E70A2B',
            //                         confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
            //                         allowOutsideClick: false,
            //                         allowEscapeKey: false
            //                     }).then(function() {
            //                         window.location.href = '<?= site_url('web/alokasi-minyak-goreng-simirah') ?>'
            //                     })
            //                 } else {
            //                     Swal.fire({
            //                         title: 'Alokasi Periode tersebut sudah diambil<br> Silahkan buka Daftar Alokasi',
            //                         icon: 'error',
            //                         confirmButtonColor: '#E70A2B',
            //                         confirmButtonText: '<i class="uil uil-check-circle me-1"></i> Okay',
            //                         allowOutsideClick: false,
            //                         allowEscapeKey: false
            //                     }).then(function() {
            //                         //  window.location.href = '<?= site_url('perdagangan/provinsi') ?>'
            //                     })
            //                 }

            //             },
            //             complete: function() {
            //                 $('body').LoadingOverlay('hide');
            //             }
            //         })
            //     }
            //}
        ] : [{
                text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
                action: function(e, dt, node, config) {
                    $.ajax({
                        url: '<?= base_url() ?>web/mirah-export',
                        type: 'POST',
                        data: data_post,
                        success: function(result) {
                            window.open('<?= base_url() ?>web/mirah-download', '_blank');
                        }
                    })
                }
            }],
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: $('#no_dokumen').val(),
                    nama_pengirim: $('#nama_pengirim').val(),
                    type: $('#type').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getSummarySIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'nib',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'NIB',
                    data: 'nib',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.nib + '<br>' + r.nama
                    }
                },
                {
                    title: 'Type DMO',
                    data: 'type',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.type + '<br><i class="badge bg-warning">' + r.packaging + '<i>'
                    }
                },
                {
                    title: 'Provinsi',
                    data: 'provinsi',
                    orderable: false,
                },
                {
                    title: 'Jumlah Distribusi',
                    data: 'jml_distribusi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<p>` + ((r.jml_distribusi == null) ? '-' : addCommas(r.jml_distribusi) + ` ` + r.satuan) + `</p>`
                    }
                },
                {
                    title: 'Pengali Regional',
                    data: 'pengali_regional',
                    orderable: false,
                },
                {
                    title: 'Pengali Kemasan',
                    data: 'pengali_kemasan',
                    orderable: false,
                },
                {
                    title: 'Jumlah DMO',
                    data: 'jml',
                    orderable: false,
                    render: function(k, v, r) {
                        if (kepmendag == '') {
                            kepmendag = r.peraturan
                            $('#kepmendag').text(r.peraturan.toString().toUpperCase())
                        }
                        return `<p>` + ((r.jml == null) ? '-' : addCommas(((parseFloat(r.jml) % 1 != 0) ? parseFloat(r.jml).toFixed(2) : r.jml)) + ` ` + r.satuan) + `</p>`
                    }
                }, {
                    title: 'Tanggal',
                    data: 'date',
                    orderable: false,
                    visible: ($('#tgl_awal').val() == '') ? true : false,
                },
                {
                    title: 'Detail',
                    data: 'jml',
                    orderable: false,
                    render: function(k, v, r) {
                        var tgl = ''
                        if ($('#tgl_awal').val() == '') {
                            tgl = r.date
                        }
                        return '<button type="button" class="btn btn-sm btn-dark" onclick="showDetail(\'' + r.nib + '\',\'' + r.type + '\',\'' + tgl + '\',\'' + r.provinsi_id + '\')"><i class="ti ti-search me-1"></i>Detail</button>'
                    },
                }


            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (kabupaten/kota)')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }

    function showDetail(nib, jenis, tgl, provinsi_id) {
        if (jenis == 'Produsen Migor') {
            showMigorDetail(nib, null, tgl, provinsi_id);
        } else if (jenis == 'Produsen Migor (CPO)') {
            console.log(jenis)
            showMigorDetail(nib, null, tgl, provinsi_id, jenis);
        } else if (jenis == 'Produsen CPO') {
            showCPODetail(nib, null, tgl, provinsi_id);
        } else if (jenis == 'Exportir - Produsen CPO') {
            showCPODetail(nib, 'Exportir - Produsen CPO', tgl, provinsi_id);
        } else if (jenis == 'Exportir - Produsen Migor') {
            showMigorDetail(nib, 'Exportir - Produsen Migor', tgl, provinsi_id);
        }


        $('#modal').modal('show')
    }

    function showCPODetail(nib, detail = null, tgl = null, provinsi_id = null) {

        if ($.fn.DataTable.isDataTable('#tbl_detail')) {
            $("#tbl_detail").dataTable().fnDestroy();
            $('#tbl_detail').empty();
        }

        var table = $('#tbl_detail').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    tgl_awal: ((tgl == null || tgl == '') ? $('#tgl_awal').val() : tgl),
                    tgl_akhir: ((tgl == null || tgl == '') ? $('#tgl_akhir').val() : tgl),
                    nib: ((detail == null) ? nib : ''),
                    nib_exportir: ((detail != null) ? nib : ''),
                    kode_daerah: ((provinsi_id == null || provinsi_id == '') ? $('#provinsi-name').val() : provinsi_id),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTrxProdusenCpoSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nama_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        var cooperation = ``
                        if (detail != null) {
                            cooperation = `<dt class="col-12">Exportir</dt>
                            <dd class="col-12">` + r.nib_exportir + ` <br> ` + r.nama_exportir + ` <br>  ` + r.no_dokumen_cooperation + ` </dd>`;
                        }
                        return `<dl class="row mb-0">
                            ` + cooperation + `
                            <dt class="col-12">Nama</dt>
                            <dd class="col-12">` + r.nama_pengirim + `</dd>
                            <dt class="col-12">NIB </dt>
                            <dd class="col-12">` + r.nib_pengirim + `</dd>
                            <dt class="col-12">Tujuan</dt>
                            <dd class="col-12">` + r.kab_kota + `, ` + r.provinsi + `</dd>
                            <dt class="col-12">Tanggal Input</dt>
                            <dd class="col-12">` + r.waktu_terbit + `</dd>
                        </dl>`
                    }
                },
                {
                    title: 'Dikirim',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {

                        var packaging = '<div class="badge bg-warning">' + r.packaging + '</div>'
                        if (r.packaging == 'kemasan') {
                            packaging = '<div class="badge bg-success">' + r.packaging + '</div>'
                        }
                        return `<dl class="row mb-0">
                            <dt class="col-12">No DO</dt>
                            <dd class="col-12">` + r.no_dokumen + `</dd>
                            <dt class="col-12">Tanggal</dt>
                            <dd class="col-12">` + r.tgl_pengiriman + `</dd>
                            <dt class="col-12">Jumlah (CPO) ` + packaging + `</dt>
                            <dd class="col-12">` + ((r.jumlah == null) ? '-' : addCommas(r.jumlah) + ` ` + r.satuan) + `</dd>
                            <dt class="col-12">Harga</dt>
                            <dd class="col-12">` + `Rp. ` + addCommas(r.harga) + `</dd>
                        </dl>`
                    },
                },
                {
                    title: 'Penerima',
                    data: 'nama_penerima',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                            <dt class="col-12">Nama</dt>
                            <dd class="col-12">` + r.nama_penerima + `</dd>
                            <dt class="col-12">NIB</dt>
                            <dd class="col-12">` + r.nib_penerima + `</dd>
                        </dl>`
                    }
                },
                {
                    title: 'Diterima',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                            <dt class="col-12">Tanggal</dt>
                            <dd class="col-12">` + ((r.date_received == null) ? '-' : r.date_received) + `</dd>
                            <dt class="col-12">Jumlah (CPO)</dt>
                            <dd class="col-12">` + ((r.volume_received == null) ? '-' : addCommas(r.volume_received) + ` ` + r.satuan) + `</dd>
                            <dt class="col-12">Jumlah (Konversi CPO->Migor)</dt>
                            <dd class="col-12">` + ((r.volume_conversion == null) ? '-' : addCommas(r.volume_conversion) + ` ` + r.satuan) + `</dd>
                            <dt class="col-12">Harga</dt>
                            <dd class="col-12">` + ((r.price_received == null) ? '-' : `Rp. ` + addCommas(r.price_received)) + `</dd>
                        </dl>`
                    },
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {

                        var tidak_dihitung = ''
                        if (r.status != 'Diterima' && r.status != 'Proses Distribusi' && r.status != 'Selesai') {
                            tidak_dihitung = '<br><div class="badge bg-danger"><i class="ti ti-x me-1"></i>Tidak Dihitung</div>'
                        }

                        return '<p>' + r.status + '</p>' + tidak_dihitung
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    function showMigorDetail(nib, detail = null, tgl = null, provinsi_id = null, jenis = null) {


        if ($.fn.DataTable.isDataTable('#tbl_detail')) {
            $("#tbl_detail").dataTable().fnDestroy();
            $('#tbl_detail').empty();
        }

        var table = $('#tbl_detail').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    tgl_awal: ((tgl == null || tgl == '') ? $('#tgl_awal').val() : tgl),
                    tgl_akhir: ((tgl == null || tgl == '') ? $('#tgl_akhir').val() : tgl),
                    nib: ((detail == null) ? nib : ''),
                    nib_exportir: ((detail != null) ? nib : ''),
                    kode_daerah: ((provinsi_id == null || provinsi_id == '') ? $('#provinsi-name').val() : provinsi_id),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTrxProdusenMigorSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nama_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        var cooperation = ``
                        if (detail != null) {
                            cooperation = `<dt class="col-12">Exportir</dt>
                            <dd class="col-12">` + r.nib_exportir + ` <br> ` + r.nama_exportir + ` <br>  ` + r.no_dokumen_cooperation + ` </dd>`;
                        }
                        return `<dl class="row mb-0">
                            ` + cooperation + `
                            <dt class="col-12">Nama</dt>
                            <dd class="col-12">` + r.nama_pengirim + `</dd>
                            <dt class="col-12">NIB </dt>
                            <dd class="col-12">` + r.nib_pengirim + `</dd>
                            <dt class="col-12">Tujuan</dt>
                            <dd class="col-12">` + r.kab_kota + `, ` + r.provinsi + `</dd>
                            <dt class="col-12">Tanggal Input</dt>
                            <dd class="col-12">` + r.waktu_terbit + `</dd>
                            <dt class="col-12">Terakhir Update</dt>
                            <dd class="col-12">` + r.last_update + `</dd>
                        </dl>`
                    }
                },
                {
                    title: 'Dikirim',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        var packaging = '<div class="badge bg-warning">' + r.packaging + '</div>'
                        if (r.packaging == 'kemasan') {
                            packaging = '<div class="badge bg-success">' + r.packaging + '</div>'
                        }
                        var parent_do = ''
                        if (r.no_dokumen_parent != null) {
                            parent_do = `<div class="badge bg-info">Distribusi dari ` + r.no_dokumen_parent + `</div>`
                        }

                        return `<dl class="row mb-0">
                            <dt class="col-12">No DO ` + parent_do + `</dt>
                            <dd class="col-12">` + r.no_dokumen + `</dd>
                            <dt class="col-12">Tanggal</dt>
                            <dd class="col-12">` + r.tgl_pengiriman + `</dd>
                            <dt class="col-12">Jumlah (` + r.type.toString().toUpperCase() + `) ` + packaging + `</dt>
                            <dd class="col-12">` + ((r.jumlah == null) ? '-' : addCommas(r.jumlah) + ` ` + r.satuan) + `</dd>
                            <dt class="col-12">Harga</dt>
                            <dd class="col-12">` + `Rp. ` + addCommas(r.harga) + `</dd>
                        </dl>`
                    },
                },
                {
                    title: 'Penerima',
                    data: 'nama_penerima',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                            <dt class="col-12">Nama</dt>
                            <dd class="col-12">` + r.nama_penerima + `</dd>
                            <dt class="col-12">NIB</dt>
                            <dd class="col-12">` + r.nib_penerima + `</dd>
                        </dl>`
                    }
                },
                {
                    title: 'Diterima',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                            <dt class="col-12">Tanggal</dt>
                            <dd class="col-12">` + ((r.date_received == null) ? '-' : r.date_received) + `</dd>
                            <dt class="col-12">Jumlah</dt>
                            <dd class="col-12">` + ((r.volume_received == null) ? '-' : addCommas(r.volume_received) + ` ` + r.satuan) + `</dd>
                            <dt class="col-12">Harga</dt>
                            <dd class="col-12">` + ((r.price_received == null) ? '-' : `Rp. ` + addCommas(r.price_received)) + `</dd>
                        </dl>`
                    },
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {
                        var tidak_dihitung = ''
                        if (jenis != 'Produsen Migor (CPO)') {
                            if ((r.status != 'Diterima' && r.status != 'Proses Distribusi' && r.status != 'Selesai') || (r.no_dokumen_parent != null && r.no_dokumen_parent != '')) {
                                tidak_dihitung = '<br><div class="badge bg-danger"><i class="ti ti-x me-1"></i>Belum Dihitung</div>'
                            }

                        } else {
                            if ((r.status != 'Diterima' && r.status != 'Proses Distribusi' && r.status != 'Selesai') || (r.no_dokumen_parent == null || r.no_dokumen_parent == '')) {
                                tidak_dihitung = '<br><div class="badge bg-danger"><i class="ti ti-x me-1"></i>Belum Dihitung</div>'
                            }
                        }



                        return '<p>' + r.status + '</p>' + tidak_dihitung
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 10
        });
    }
</script>