<script>
    $(document).ready(function() {
       // showTableCPO('refresh');
        $('#platform-name, #provinsi-name, #kabkota-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">-- Pilih Kabupaten/Kota --</option>')

    })



    function fill() {
      //  showTableCPO('refresh');
    }

    // $('#provinsi-name').on('change', function() {
    //     var daerah_id = $("#provinsi-name").val();
    //     getDaerah('kabkota', daerah_id)
    //     showTable('refresh');

    // })

    // $('#kabkota-name').on('change', function() {
    //     var daerah_id = $("#kabkota-name").val();
    //     showTable('refresh');

    // })

    function showTableCPO(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_cpo_migor')) {
            $("#tbl_cpo_migor").dataTable().fnDestroy();
            $('#tbl_cpo_migor').empty();
        }

        var table = $('#tbl_cpo_migor').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: (($('#no_dokumen').val() =="")?'<?php echo ((($no_dokumen == '' || $no_dokumen == null)?$this->input->get('do'):$no_dokumen))?>':$('#no_dokumen').val()),
                    nama_pengirim: $('#nama_pengirim').val(),
                    status: $('#f_status').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getTrxProdusenCpoSIMIRAH',
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nama_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Nama</dt>
                                    <dd class="col-12">` + r.nama_pengirim + `</dd>
                                    <dt class="col-12">NIB </dt>
                                    <dd class="col-12">` + r.nib_pengirim + `</dd>
                                    <dt class="col-12">Tujuan</dt>
                                    <dd class="col-12">` + r.kab_kota + `, ` + r.provinsi + `</dd>
                                    <dt class="col-12">Tanggal Input</dt>
                                    <dd class="col-12">` + r.waktu_terbit + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Dikirim',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">No DO</dt>
                                    <dd class="col-12">` + r.no_dokumen + `</dd>
                                    <dt class="col-12">Tanggal</dt>
                                    <dd class="col-12">` + r.tgl_pengiriman + `</dd>
                                    <dt class="col-12">Jumlah (Konversi CPO->Migor)</dt>
                                    <dd class="col-12">` + ((r.jumlah == null) ? '-' : addCommas(r.jumlah) + ` ` + r.satuan) + `</dd>
                                    <dt class="col-12">Harga</dt>
                                    <dd class="col-12">` + `Rp. ` + addCommas(r.harga) + `</dd>
                                </dl>`
                    },
                },
                {
                    title: 'Penerima',
                    data: 'nama_penerima',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Nama</dt>
                                    <dd class="col-12">` + r.nama_penerima + `</dd>
                                    <dt class="col-12">NIB</dt>
                                    <dd class="col-12">` + r.nib_penerima + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'Diterima',
                    data: 'id_transaksi',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-12">Tanggal</dt>
                                    <dd class="col-12">` + ((r.date_received == null) ? '-' : r.date_received) + `</dd>
                                    <dt class="col-12">Jumlah (CPO)</dt>
                                    <dd class="col-12">` + ((r.volume_received == null) ? '-' : addCommas(r.volume_received) + ` ` + r.satuan) + `</dd>
                                    <dt class="col-12">Harga</dt>
                                    <dd class="col-12">` + ((r.price_received == null) ? '-' : `Rp. ` + addCommas(r.price_received)) + `</dd>
                                </dl>`
                    },
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {

                        return '<p>' + r.status + '</p>'
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 5
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>