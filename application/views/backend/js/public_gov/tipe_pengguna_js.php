<script>
    $(document).ready(function() {
        showTable();
    })

    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
      
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            ajax: {
                type: 'POST',
                url: '<?= base_url() ?>master/tipe_pengguna_json',
                data: {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                }
            },
            columns: [{
                    title: 'ID Pengguna',
                    data: 'tipe_pengguna_id',
                },
                {
                    title: 'Tipe Pengguna',
                    data: 'tipe_pengguna',
                },
                {
                    title: 'Flag Publik',
                    data: 'flag_publik',
                    className: 'text-center',
                    render: function(k, v, r) {
                        if (r.flag_publik == 'Y') {
                            return '<div class="badge bg-success"><i class="ti ti-circle-check"></i></div>'
                        } else {
                            return '<div class="badge bg-warning"><i class="ti ti-lock"></i></div>'
                        }
                    }
                },
                {
                    title: 'Aksi',
                    data: 'tipe_pengguna_id',
                    className: 'text-center',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-warning btn-sm  m-1" onclick="edit(\'' + m.row + '\')"> <i class="ti ti-edit"></i></button>' +
                            '<button class="btn btn-danger btn-sm m-1" onclick="update(\'' + r.tipe_pengguna_id + '\',\'Deleted\',\'tbl_tipe_pengguna\',\'tipe_pengguna_id\',\'Hapus tipe pengguna `' + ($.trim(r.tipe_pengguna.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'

                    }
                },


            ],
            scrollX: true,
            displayLength: 10
        });
    }

    function edit(action_index) {
        $.ajax({
            url: '<?= base_url() ?>master/tipe_pengguna_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#tipe_pengguna_id').val(response.tipe_pengguna_id)
                $('#tipe_pengguna').val(response.tipe_pengguna)
                $('#flag_publik').val(response.flag_publik)
            },
            complete: function() {
                $("#modal").modal('show')
            }
        })
    }
</script>