<script>
    $(document).ready(function() {
        showTable();
    })



    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>pengguna/pengguna_json',
                "data": {
                    refresh: refresh,
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'No',
                    data: 'pengguna_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama',
                    data: 'nama_lengkap',
                    orderable: false
                },
                {
                    title: 'Email',
                    data: 'email',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.email + '<br>' + r.no_hp
                    }
                },
                {
                    title: 'Alamat',
                    data: 'alamat',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.alamat + ', ' + r.kecamatan + ', ' + r.kab_kota + ', ' + r.provinsi
                    }
                },
                {
                    title: 'Aksi',
                    data: 'pengguna_id',
                    render: function(k, v, r, m) {
                        return '<button class="btn btn-sm btn-warning m-1" onclick="edit(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"> <i class="ti ti-edit"></i></button>' +
                            '<button class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.pengguna_id + '\',\'Deleted\',\'tbl_pengguna\',\'pengguna_id\',\'Hapus pengguna `' + ($.trim(r.nama_lengkap.replace(/[\t\n]+/g, ''))) + '`\')"> <i class="ti ti-trash"></i></button>'
                    }
                },
            ],
            columnDefs: [{
                targets: 4,
                width: 100,
                className: 'text-center'
            }],
            "scrollX": true,
            "displayLength": 5
        });

    }


    function tambah() {
        $('#pengguna_id').val('')
        $('#nip').val('')
        $('#nama_lengkap').val('')
        $('#email').val('')
        $('#no_hp').val('')
        $('#alamat').val('')
        $('#tipe_pengguna').selectize()[0].selectize.setValue('')
        $('#provinsi').selectize()[0].selectize.setValue('')
        $('#kabupaten').selectize()[0].selectize.setValue('')
        $('#kabupaten').selectize()[0].selectize.setValue('')
        $('#kabupaten')[0].selectize.disable();
        $('#kecamatan')[0].selectize.disable();

        $("#tambahpengguna").modal({
            fadeDuration: 250
        });
    }

    function edit(action_index, start, length) {
        $.ajax({
            url: '<?= base_url() ?>pengguna/pengguna_json',
            type: 'POST',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            success: function(response) {
                response = JSON.parse(response)
                $('#pengguna_id').val(response.pengguna_id)
                $('#nip').val(response.nip)
                $('#nama_lengkap').val(response.nama_lengkap)
                $('#email').val(response.email)
                $('#no_hp').val(response.no_hp)
                $('#alamat').val(response.alamat)
                $('#provinsi').selectize()[0].selectize.setValue(response.daerah_id.toString().substring(0, 2))
                $('#kabupaten').selectize()[0].selectize.addOption({
                    value: response.daerah_id.toString().substring(0, 4),
                    text: response.kab_kota
                })
                $('#kabupaten').selectize()[0].selectize.setValue(response.daerah_id.toString().substring(0, 4))
                $('#kecamatan').selectize()[0].selectize.addOption({
                    value: response.daerah_id.toString().substring(0, 6),
                    text: response.kecamatan
                })
                $('#kecamatan').selectize()[0].selectize.setValue(response.daerah_id.toString().substring(0, 6))
                $('#kecamatan')[0].selectize.enable();
                $('#tipe_pengguna').selectize()[0].selectize.setValue(response.tipe_pengguna)
                // $('#flag_publik').val(response.flag_publik)


            }
        })
        $("#tambahpengguna").modal({
            fadeDuration: 250
        });
    }

    function after_update() {
        showTable('refresh');
    }
</script>