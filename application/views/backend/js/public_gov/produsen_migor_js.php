<script>
    $(document).ready(function() {
        showTable('refresh');
        $('#platform-name, #provinsi-name, #kabkota-name').selectize()
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">- Pilih Kabupaten/Kota -</option>')

    })

    function fill() {
        showTable('refresh');
    }

    $('#provinsi-name').on('change', function() {
        var daerah_id = $("#provinsi-name").val();
        getDaerah('kabkota', daerah_id)
        showTable('refresh');

    })

    $('#kabkota-name').on('change', function() {
        var daerah_id = $("#kabkota-name").val();
        showTable('refresh');

    })

    function showTable(refresh = null) {

        var platform = $("#platform-name").val();

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }

        var table = $('#tbl_general').DataTable({
      
            responsive: true,
            processing: true,
            serverSide: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: refresh,
                    platform: platform,
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getProdusen',
            },
            columns: [{
                    title: 'No',
                    data: 'api_log_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Nama Produsen',
                    data: 'nama_produsen',
                    orderable: false,
                    render: function(k, v, r) {
                        return `<dl class="row mb-0">
                                    <dt class="col-sm-12">Nama</dt>
                                    <dd class="col-sm-12">` + r.nama_produsen + `</dd>
                                    <dt class="col-sm-12">NIB</dt>
                                    <dd class="col-sm-12">` + r.nib_supplier + `</dd>
                                    <dt class="col-sm-12">NPWP</dt>
                                    <dd class="col-sm-12">` + ((platform == 'gurih') ? r.npwp : r.npwp_supplier) + `</dd>
                                </dl>`
                    }
                },
                {
                    title: 'No. Telp',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<a href="tel:'+ ((platform == 'gurih') ? r.no_telepon : '#') +'">' + ((platform == 'gurih') ? r.no_telepon : '-') + '</a>'
                    }
                },
                {
                    title: 'Nama Penanggung Jawab',
                    data: 'api_log_id',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<p>' + ((platform == 'gurih') ? r.nama_penanggung_jawab : '-') + '</p>'
                    }
                },
                {
                    title: 'Alamat',
                    data: 'alamat_produsen',
                    orderable: false,
                    render: function(k, v, r) {

                        if (r.titik_koordinat != '' && r.titik_koordinat != null && r.titik_koordinat != '0,0') {
                            return '<p>' + (r.alamat_produsen == null ? '-' : r.alamat_produsen) + '</p>' +
                                '<a href="http://maps.google.com/maps?q=' + r.titik_koordinat + '" target="_blank"><span class="btn btn-sm btn-primary"><i class="ti ti-map-pin me-1"></i>Lihat di Google Maps</span></a>'
                        } else {
                            return '<p>' + (r.alamat_produsen == null ? '-' : r.alamat_produsen) + '</p>'
                        }
                    }
                },

            ],
        });
    }

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function () {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function (response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function (k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function () {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.disable();
                }
            }
        })
    }
</script>