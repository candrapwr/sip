<script>
    var pemantauan_detail_id = ''
    $(document).ready(function() {
        showTable('refresh');
        $('#provinsi-name, #kabkota-name').selectize()
        showRekapPemantauan()
        // get_pelaku_usaha('produsen')
        getDaerah('provinsi', 1);
        $('#kabkota-name').html('<option value="">-- Pilih Kabupaten/Kota --</option>')


        $('#nama_pu').selectize({
            create:true,
            valueField: 'nama',
            labelField: 'nama',
            searchField: 'nama',
            load: function(query, callback) {
                var url = '<?= base_url() ?>perdagangan/migor/getProdusenSIMIRAH'
                if ($('#jenis_pu').val() == 'distributor1') {
                    url = '<?= base_url() ?>perdagangan/migor/getD1SIMIRAH'
                } else if ($('#jenis_pu').val() == 'distributor2') {
                    url = '<?= base_url() ?>perdagangan/migor/getD2SIMIRAH'
                } else if ($('#jenis_pu').val() == 'pengecer') {
                    url = '<?= base_url() ?>perdagangan/migor/getPengecerSIMIRAH'
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    beforeSend: function() {
                        $('#nama_pu-selectized').LoadingOverlay('show');
                    },
                    data: {
                        nama: query,
                        length: 10,
                        filter: 'filter',
                        refresh: 'refresh',
                        selectize: true,
                        start: 0,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        callback(response)
                    },
                    complete: function() {
                        $('#nama_pu-selectized').LoadingOverlay('hide');
                    }
                })
            },
        })
    })

    function showRekapPemantauan() {
        if ($.fn.DataTable.isDataTable('#tbl_rekap_pemantauan')) {
            $("#tbl_rekap_pemantauan").dataTable().fnDestroy();
            $('#tbl_rekap_pemantauan').empty();
        }
        var table = $('#tbl_rekap_pemantauan').DataTable({
            dom: 'Brtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            buttons: [{
                text: '<i class="ti ti-file-spreadsheet me-1"></i>Export Excel',
                action: function(e, dt, node, config) {
                    $.ajax({
                        url: '<?= base_url() ?>web/pemantauan-export',
                        type: 'POST',
                        beforeSend: function() {
                            $('body').LoadingOverlay('show');
                        },
                        data: {
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                        },
                        success: function(result) {
                            $('body').LoadingOverlay('hide');
                            window.open('<?= base_url() ?>web/mirah-download', '_blank');
                        }
                    })
                }
            }],
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                },
                "url": '<?= base_url() ?>perdagangan/migor/getPemantauanMigorDetail',
            },
            columns: [{
                    title: 'No',
                    data: 'pemantauan_migor_detail_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nib_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.no_dokumen + '<br>' + r.nib_pengirim + '<br>' + r.nama_pengirim
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jml_distribusi',
                    render: function(k, v, r) {
                        return 'Jumlah <br>' + r.jml_distribusi + '<br>Harga Pembelian <br>' + r.harga_pembelian
                    }
                },
                {
                    title: 'Jumlah Sebenarnya',
                    data: 'jml_distribusi',
                    render: function(k, v, r) {
                        var selisih_jumlah = ''
                        var selisih_harga = ''

                        if (r.jml_distribusi != '' && r.jml_distribusi != '-') {
                            selisih_jumlah = ' <b>( ' + (parseFloat(r.jml_distribusi_rill) - parseFloat(r.jml_distribusi)) + ' )</b> '
                            selisih_harga = ' <b>( ' + (parseFloat(r.harga_pembelian_rill) - parseFloat(r.harga_pembelian)) + ' ) </b>'
                        }


                        return 'Jumlah <br>' + r.jml_distribusi_rill + selisih_jumlah + '<br>Harga Pembelian <br>' + r.harga_pembelian_rill + selisih_harga
                    }
                },
                {
                    title: 'Harga Penjualan',
                    data: 'harga_penjualan_rill',
                    orderable: false
                },
                {
                    title: 'Stok',
                    data: 'stok',
                    orderable: false
                },
                {
                    title: 'Lampiran',
                    data: 'stok',
                    orderable: false,
                    render: function(k, v, r) {

                        var button = '-'

                        if (r.lampiran != null) {
                            button = '<a href="<?= base_url() ?>services/download/migor/pelaporan/' + r.lampiran + '"><i class="ti ti-download"></i></a>'
                        }

                        return button

                    }
                },
                {
                    title: 'Periode Pengawasan',
                    data: 'periode',
                    render: function(k, v, r) {
                        var color = 'danger'
                        var kesesuaian = ''
                        if (r.kesesuaian != null) {
                            if (r.kesesuaian == 'sesuai') {
                                color = 'success'
                                kesesuaian = r.kesesuaian.toString().toUpperCase()
                            } else {
                                kesesuaian = 'TIDAK SESUAI'
                            }
                        }

                        return r.periode + '<br><div class="badge bg-' + color + '">' + kesesuaian + '</div><br><i>' + r.keterangan + '</i>'
                    }
                },
            ],
            "scrollX": true,
            "displayLength": 5
        });
    }

    function add() {
        $('#modal_general').modal('show')
    }

    $('#jenis_pu').on('change', function() {
        $('#nama_pu').selectize()[0].selectize.destroy()
        $('#nama_pu').selectize({
            create:true,
            valueField: 'nama',
            labelField: 'nama',
            searchField: 'nama',
            load: function(query, callback) {
                var url = '<?= base_url() ?>perdagangan/migor/getProdusenSIMIRAH'
                if ($('#jenis_pu').val() == 'distributor1') {
                    url = '<?= base_url() ?>perdagangan/migor/getD1SIMIRAH'
                } else if ($('#jenis_pu').val() == 'distributor2') {
                    url = '<?= base_url() ?>perdagangan/migor/getD2SIMIRAH'
                } else if ($('#jenis_pu').val() == 'pengecer') {
                    url = '<?= base_url() ?>perdagangan/migor/getPengecerSIMIRAH'
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    beforeSend: function() {
                        $('#nama_pu-selectized').LoadingOverlay('show');
                    },
                    data: {
                        nama: query,
                        length: 10,
                        filter: 'filter',
                        refresh: 'refresh',
                        selectize: true,
                        start: 0,
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        callback(response)
                    },
                    complete: function() {
                        $('#nama_pu-selectized').LoadingOverlay('hide');
                    }
                })
            },


        })
    })

    $('#kesesuaian').on('change', function() {
        if ($('#kesesuaian :selected').val() == 'sesuai' && $('#terdaftar_simirah_fk').val() == 'Ya') {
            $('#is_sesuai').hide();
            $('#jml_distribusi_rill').val($('#jml_distribusi').val())
            $('#harga_pembelian_rill').val($('#harga_pembelian').val())
        } else {
            $('#is_sesuai').show();
            $('#jml_distribusi_rill').val('')
            $('#harga_pembelian_rill').val('')
        }
    })

    $('#nama_pu').on('change', function() {

        var data = $('#nama_pu').data().selectize.options[$('#nama_pu').val()]
        // console.log(data)
        if (data.nib !== undefined) {
            $('#nib').val(data.nib)
        } else {
            $('#nib').val(data.npwp)
        }
        $('#nama_pemilik').val(data.pic)
        $('#telp_pemilik').val(data.no_pic)
        $('#alamat_pu').val(data.alamat)
    })


    function showTable(refresh = null) {

        if ($.fn.DataTable.isDataTable('#tbl_general')) {
            $("#tbl_general").dataTable().fnDestroy();
            $('#tbl_general').empty();
        }
        var table = $('#tbl_general').DataTable({
            dom: 'rtip',
            // buttons: ((data_post.tgl_awal != '' && data_post.tgl_akhir != '')) ? [{
            //         text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
            //         action: function(e, dt, node, config) {
            //             $.ajax({
            //                 url: '<?= base_url() ?>web/mirah-export',
            //                 type: 'POST',
            //                 data: data_post,
            //                 success: function(result) {
            //                     window.open('<?= base_url() ?>web/mirah-download', '_blank');
            //                 }
            //             })
            //         }
            //     }

            // ] : [{
            //     text: '<i class="ti ti-file-spreadsheet me-1"></i>Excel',
            //     action: function(e, dt, node, config) {
            //         $.ajax({
            //             url: '<?= base_url() ?>web/mirah-export',
            //             type: 'POST',
            //             data: data_post,
            //             success: function(result) {
            //                 window.open('<?= base_url() ?>web/mirah-download', '_blank');
            //             }
            //         })
            //     }
            // }],
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    tgl_awal: $('#tgl_awal').val(),
                    tgl_akhir: $('#tgl_akhir').val(),
                    no_dokumen: $('#no_dokumen').val(),
                    nama_pengirim: $('#nama_pengirim').val(),
                    type: $('#type').val(),
                    kode_daerah: $('#kabkota-name').val() ? $('#kabkota-name').val() : $('#provinsi-name').val(),
                },
                "url": '<?= base_url() ?>perdagangan/migor/getPemantauanMigor',
            },
            columns: [{
                    title: 'No',
                    data: 'nib_pu',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pelaku Usaha',
                    data: 'nib_pu',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.nib_pu + '<br>' + r.nama_pu + '<br><div class="badge bg-dark">' + r.jenis_pu + '</div>'
                    }
                },

                {
                    title: 'Alamat',
                    data: 'alamat_pu',
                    orderable: false,
                    render: function(k, v, r) {

                        var catatan = ''
                        if (r.catatan != null) {
                            catatan = '<br><br><b>Catatan :</b> <br><i>' + r.catatan + '</i>'
                        }
                        return '<div class="badge bg-light" style="text-align:left">Tujuan Distribusi - <b>' + r.provinsi + '</b></div><br>' + r.alamat_pu + catatan
                    }
                },
                {
                    title: 'PIC',
                    data: 'nama_pemilik',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.nama_pemilik + '<br>' + r.telp_pemilik
                    }
                },
                {
                    title: 'Tanggal Pemantauan',
                    data: 'tgl',
                    orderable: false,
                    render: function(k, v, r) {
                        return '<button type="button" class="btn btn-sm btn-warning m-1" onclick="showDetailPengawasan(' + r.pemantauan_migor_id + ')"><b>' + r.jml_laporan + '</b><br> Laporan DO</button><br><br>' + r.tgl + '<br>' + r.petugas
                    },
                    className: 'text-center'
                },

                {
                    title: 'Detail',
                    data: 'jenis_pu',
                    orderable: false,
                    render: function(k, v, r) {

                        return '<button type="button" class="btn btn-sm btn-primary m-1" onclick="showDO(\'' + r.nib_pu + '\',\'' + r.jenis_pu + '\',\'' + r.daerah_id + '\',\'' + r.nama_pu + '\',\'' + r.pemantauan_migor_id + '\')"><i class="ti ti-search me-1"></i>Pasokan & Distribusi</button><button type="button" class="btn btn-sm btn-info m-1" onclick="addPenerimaan(\'' + r.pemantauan_migor_id + '\',\'' + r.nama_pu + '\',\'' + r.jenis_pu + '\',\'' + r.nib_pu + '\')"><i class="ti ti-plus me-1"></i>Penerimaan</button><br><button disabled type="button" class="btn btn-sm btn-warning m-1" onclick="edit(' + r.pemantauan_migor_id + ')"><i class="ti ti-edit"></i></button><button type="button" class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.pemantauan_migor_id + '\',\'Deleted\',\'pemantauan_migor\',\'pemantauan_migor_id\',\'Hapus pemantauan migor ' + r.pemantauan_migor_id + '\',\'mirah\')"><i class="ti ti-trash"></i></button>'
                    }
                }


            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    $('#provinsi-name').on('change', function() {
        getDaerah('kabkota', $('#provinsi-name').val())
    })

    function getDaerah(id, daerah_id) {

        var daerah = '';
        if (id == 'provinsi') {
            daerah = '<option value="">- Pilih Provinsi -</option>';
        } else {
            daerah = '<option value="">- Pilih Kabupaten/Kota -</option>';
        }

        $.ajax({
            url: '<?= base_url() ?>master/daerah',
            type: 'POST',
            data: {
                daerah_id: daerah_id,
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
            },
            beforeSend: function() {
                $('#' + id + '-name').selectize()[0].selectize.destroy();
            },
            success: function(response) {
                response = JSON.parse(response)
                var dt = '';

                $.each(response.data, function(k, v) {

                    if (id == 'provinsi') {
                        dt = v.provinsi;
                    } else {
                        dt = v.kab_kota;
                    }

                    daerah += '<option value="' + v.daerah_id + '">' + dt + '</option>';
                })
            },
            complete: function() {
                $('#' + id + '-name').html(daerah);
                $('#' + id + '-name').selectize()

                if (id == 'provinsi' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas provinsi' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Provinsi)') ?>' || '<?= strtolower($this->session->userdata('role')) ?>' == '<?= strtolower('Kontributor SP2KP (Kabupaten/Kota)') ?>')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.lock();
                } else if (id == 'kabkota' && ('<?= strtolower($this->session->userdata('role')) ?>' == 'dinas kabupaten/kota' || '<?= strtolower($this->session->userdata('role')) ?>' == 'kontributor sp2kp (kabupaten/kota)')) {
                    $('#' + id + '-name').selectize()[0].selectize.setValue('<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>')
                    $('#' + id + '-name').selectize()[0].selectize.lock();
                }
            }
        })
    }

    

    function checkDO() {

        var jenis = $('#jenis_pengirim_temp').val();
        $('#kesesuaian').val('')
        var url = 'perdagangan/migor/getTrxProdusenCpoSIMIRAH'
        var url_pemasok = ''
        var jenis_pengirim = 'produsen-cpo'
        if (jenis == 'produsen-migor') {
            jenis_pengirim = 'produsen-cpo'
            url = 'perdagangan/migor/getTrxProdusenMigorSIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxProdusenCpoSIMIRAH'
        } else if (jenis == 'distributor1') {
            jenis_pengirim = 'produsen-migor'
            url = 'perdagangan/migor/getTrxD1SIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxProdusenMigorSIMIRAH'
        } else if (jenis == 'distributor2') {
            jenis_pengirim = 'distributor1'
            url = 'perdagangan/migor/getTrxD2SIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxD1SIMIRAH'
        } else if (jenis == 'pengecer') {
            jenis_pengirim = 'distributor2'
            url = 'perdagangan/migor/getTrxPengecerSIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxD2SIMIRAH'
        }

        if ($('#no_dokumen').val() == '') {
            $('#tgl_do').val('')
            $('#tgl_status').val('')
            $('#nib_pengirim').val('')
            $('#nama_pengirim').val('')
            $('#jenis_pengirim').val('')
            $('#jml_distribusi').val('')
            $('#harga_pembelian').val('')
            $('#terdaftar_simirah_fk').val('Tidak')
            $('#is_simirah').hide()
            $('#is_kesesuaian').hide()
        } else {
            $.ajax({
                url: '<?= base_url() ?>' + url_pemasok,
                type: 'POST',
                data: {
                    length: 1,
                    start: 0,
                    refresh: 'refresh',
                    no_dokumen: $('#no_dokumen').val(),
                    nib_penerima: $('#nib_temp').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                },
                success: function(response) {
                    response = JSON.parse(response)
                    if (response.data.length > 0) {
                        $('#tgl_do').val(response.data[0].tgl_pengiriman.toString().substr(0, 10))
                        $('#tgl_status').val(response.data[0].last_update.toString().substr(0, 10))
                        $('#nib_pengirim').val(response.data[0].nib_pengirim)
                        $('#nama_pengirim').val(response.data[0].nama_pengirim)
                        $('#jenis_pengirim').val(jenis_pengirim)

                        var jumlah = response.data[0].jumlah
                        if (response.data[0].satuan == 'liter') {
                            jumlah = (parseFloat(response.data[0].jumlah) * 0.9)
                        }

                        $('#jml_distribusi').val(jumlah)
                        $('#harga_pembelian').val(parseFloat(response.data[0].harga))
                        $('#terdaftar_simirah_fk').val('Ya')
                        $('#is_simirah').show()
                        $('#kesesuaian').val('')
                        $('#is_kesesuaian').show()
                        $('#no_do').hide()
                    } else {
                        if (jenis == 'pengecer') {
                            $.ajax({
                                url: '<?= base_url() ?>perdagangan/migor/getTrxD1SIMIRAH',
                                type: 'POST',
                                data: {
                                    length: 1,
                                    start: 0,
                                    refresh: 'refresh',
                                    jenis_penerima: 'pengecer',
                                    no_dokumen: $('#no_dokumen').val(),
                                    nib_penerima: $('#nib_temp').val(),
                                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>'
                                },
                                success: function(response) {
                                    response = JSON.parse(response)
                                    if (response.data.length > 0) {
                                        $('#tgl_do').val(response.data[0].tgl_pengiriman.toString().substr(0, 10))
                                        $('#tgl_status').val(response.data[0].last_update.toString().substr(0, 10))
                                        $('#nib_pengirim').val(response.data[0].nib_pengirim)
                                        $('#nama_pengirim').val(response.data[0].nama_pengirim)
                                        $('#jenis_pengirim').val('distributor1')

                                        var jumlah = response.data[0].jumlah
                                        if (response.data[0].satuan == 'liter') {
                                            jumlah = (parseFloat(response.data[0].jumlah) * 0.9)
                                        }

                                        $('#jml_distribusi').val(jumlah)
                                        $('#harga_pembelian').val(parseFloat(response.data[0].harga))
                                        $('#terdaftar_simirah_fk').val('Ya')
                                        $('#is_simirah').show()
                                        $('#kesesuaian').val('')
                                        $('#is_kesesuaian').show()
                                        $('#no_do').hide()
                                    } else {
                                        $('#tgl_do').val('')
                                        $('#tgl_status').val('')
                                        $('#nib_pengirim').val('')
                                        $('#nama_pengirim').val('')
                                        $('#jenis_pengirim').val('')
                                        $('#jml_distribusi').val('')
                                        $('#harga_pembelian').val('')
                                        $('#terdaftar_simirah_fk').val('Tidak')
                                        $('#kesesuaian').val('sesuai')
                                        $('#is_simirah').hide()
                                        $('#is_kesesuaian').hide()
                                        $('#no_do').show()
                                    }
                                }
                            })

                        } else {
                            $('#tgl_do').val('')
                            $('#tgl_status').val('')
                            $('#nib_pengirim').val('')
                            $('#nama_pengirim').val('')
                            $('#jenis_pengirim').val('')
                            $('#jml_distribusi').val('')
                            $('#harga_pembelian').val('')
                            $('#terdaftar_simirah_fk').val('Tidak')
                            $('#kesesuaian').val('sesuai')
                            $('#is_simirah').hide()
                            $('#is_kesesuaian').hide()
                            $('#no_do').show()
                        }
                    }
                }
            })
        }

    }

    function addPenerimaan(pemantauan_migor_id, nama, jenis_pu, nib) {
        $('#perusahaan').html(nama)
        $('#no_dokumen').val('')
        $('#no_do').hide()
        $('#jenis_pengirim_temp').val(jenis_pu)
        $('#nib_temp').val(nib)
        $('#pemantauan_migor_id_fk').val(pemantauan_migor_id)
        $('#modal_penerimaan').modal('show')
    }

    function showDO(nib, jenis, daerah_id, nama_pu,pemantauan_migor_id=null) {

        var url = 'perdagangan/migor/getTrxProdusenCpoSIMIRAH'
        var url_pemasok = ''
        if (jenis == 'produsen-migor') {
            url = 'perdagangan/migor/getTrxProdusenMigorSIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxProdusenCpoSIMIRAH'
            $('#penerima_dari').text('dari Produsen CPO')
            $('#distribusi_dari').text('dari ' + nama_pu)
            $('#pengecer_penerimaan_d1').hide()
        } else if (jenis == 'distributor1') {
            url = 'perdagangan/migor/getTrxD1SIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxProdusenMigorSIMIRAH'
            $('#penerima_dari').text('dari Produsen Migor')
            $('#distribusi_dari').text('dari ' + nama_pu)
            $('#pengecer_penerimaan_d1').hide()
        } else if (jenis == 'distributor2') {
            url = 'perdagangan/migor/getTrxD2SIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxD1SIMIRAH'
            $('#penerima_dari').text('dari Distributor 1')
            $('#distribusi_dari').text('dari ' + nama_pu)
            $('#pengecer_penerimaan_d1').hide()
        } else if (jenis == 'pengecer') {
            url = 'perdagangan/migor/getTrxPengecerSIMIRAH'
            url_pemasok = 'perdagangan/migor/getTrxD2SIMIRAH'
            $('#penerima_dari').text('dari Distributor 2')
            $('#distribusi_dari').text('dari ' + nama_pu)
            $('#pengecer_penerimaan_d1').show()
            showPenerimaan('pengecer', 'perdagangan/migor/getTrxD1SIMIRAH', nib, daerah_id, '#tbl_penerimaan_pengecer')
        }
        showDistribusi(jenis, url, nib, daerah_id)
        if (url_pemasok != '') {
           
            showPenerimaan(jenis, url_pemasok, nib, daerah_id,null,pemantauan_migor_id)
        }


        $('#modal_distribusi').modal('show')
    }

    function showDetailPengawasan(id) {
        showPemantauanDetail(id);
        pemantauan_detail_id = id
        $('#modal_pengawasan').modal('show')
    }


    function showDistribusi(jenis, url, nib, daerah_id) {

        if ($.fn.DataTable.isDataTable('#tbl_distribusi')) {
            $("#tbl_distribusi").dataTable().fnDestroy();
            $('#tbl_distribusi').empty();
        }
        var table = $('#tbl_distribusi').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    nib: nib,
                    tgl_awal: '<?= date('Y-m-', strtotime('-1 month')) ?>01',
                    tgl_akhir: '<?= date('Y-m-t') ?>',
                    kode_daerah: daerah_id.toString().substr(0, 2)

                },
                "url": '<?= base_url() ?>' + url,
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: ((jenis == 'pengecer') ? 'npwp_penjual' : 'nib_pengirim'),
                    orderable: false,
                    render: function(k, v, r) {

                        var nama_pengirim = ''
                        var nib_pengirim = ''
                        if (jenis == 'pengecer') {
                            nib_pengirim = r.npwp_penjual
                            nama_pengirim = r.nama_penjual
                        } else {
                            nib_pengirim = r.nib_pengirim
                            nama_pengirim = r.nama_pengirim
                        }
                        return nib_pengirim + '<br>' + nama_pengirim + '<br>' + r.no_dokumen
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                    render: function(k, v, r) {
                        var jumlah = r.jumlah
                        var satuan = r.satuan
                        if (r.satuan == 'liter') {
                            jumlah = (parseFloat(r.jumlah) * 0.9)
                            satuan = 'kg'
                        }

                        return jumlah + ' ' + satuan
                    }
                },
                {
                    title: 'Harga',
                    data: 'harga',
                    orderable: false,

                },
                {
                    title: 'Penerima',
                    data: ((jenis == 'pengecer') ? 'nik' : ((jenis != 'distributor1' && jenis != 'distributor2') ? 'nib_penerima' : ((jenis == 'distributor1') ? 'nib_d2_penerima' : 'npwp_pengecer_penerima'))),
                    orderable: false,
                    render: function(k, v, r) {

                        var nib = ''
                        var nama = ''

                        if (jenis == 'produsen-cpo' || jenis == 'produsen-migor') {
                            nib = r.nib_penerima
                            nama = r.nama_penerima
                        } else if (jenis == 'distributor1') {
                            nib = r.nib_d2_penerima
                            nama = r.nama_d2_penerima
                            if (r.nib_d2_penerima == '') {
                                nib = r.npwp_pengecer_penerima
                                nama = r.nama_pengecer_penerima
                            }
                        } else if (jenis == 'pengecer') {
                            nib = r.nik
                            nama = ''
                        }

                        return nib + '<br>' + nama
                    }
                },
                {
                    title: 'Tanggal Pengiriman',
                    data: ((jenis == 'pengecer') ? 'tgl_pembelian' : 'tgl_pengiriman'),
                    render: function(k, v, r) {
                        var tgl_pembelian = ''

                        if (jenis == 'pengecer') {
                            tgl_pembelian = r.tgl_pembelian
                        } else {
                            tgl_pembelian = r.tgl_pengiriman
                        }
                        return r.status + '<br>' + tgl_pembelian + '<br>' + r.provinsi
                    }
                },



            ],
            "scrollX": true,
            "displayLength": 5
        });
    }

    function showPenerimaan(jenis, url, nib, daerah_id, id_table = null,pemantauan_migor_id =null) {

       
        var table_id = '#tbl_penerimaan';
        if (id_table != null && jenis == 'pengecer') {
            table_id = id_table;
        }

        if ($.fn.DataTable.isDataTable(table_id)) {
            $(table_id).dataTable().fnDestroy();
            $(table_id).empty();
        }

        var penerimaan=[];

        var table = $(table_id).DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "beforeSend": function() {
                    $.ajax({
                        "type": 'POST',
                        "data": {
                            csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                            refresh: 'refresh',
                            start:0,
                            length:500,
                            pemantauan_migor_id: pemantauan_migor_id,
                        },
                        "url": '<?= base_url() ?>perdagangan/migor/getPemantauanMigorDetail',
                        success: function(response) {
                            response = JSON.parse(response)
                            penerimaan = []
                            $.each(response.data,function(k,v){
                                penerimaan.push(v.no_dokumen)
                            })
                        }
                    })
                },
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    nib_penerima: nib,
                    jenis_penerima: ((id_table != null && jenis == 'pengecer') ? 'pengecer' : ''),
                    tgl_awal: '<?= date('Y-m-', strtotime('-1 month')) ?>01',
                    tgl_akhir: '<?= date('Y-m-t') ?>',
                    kode_daerah: daerah_id.toString().substr(0, 2)

                },
                "url": '<?= base_url() ?>' + url,
            },
            columns: [{
                    title: 'No',
                    data: 'id_transaksi',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        var pengawasan_done = ''
                        if(jQuery.inArray(r.no_dokumen, penerimaan) !== -1){
                            console.log('in')
                            pengawasan_done = '<span class="text-success fw-bolder"><i class="ti ti-checks"></i></span>'
                        }
                        return (m.settings._iDisplayStart) + (m.row + 1)+'<br>'+pengawasan_done
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nib_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.nib_pengirim + '<br>' + r.nama_pengirim + '<br>' + r.no_dokumen
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jumlah',
                    render: function(k, v, r) {
                        var jumlah = r.jumlah
                        var satuan = r.satuan
                        if (r.satuan == 'liter') {
                            jumlah = (parseFloat(r.jumlah) * 0.9)
                            satuan = 'kg'
                        }
                        return jumlah + ' ' + satuan
                    }
                },
                {
                    title: 'Harga',
                    data: 'harga',
                    orderable: false,

                },
                {
                    title: 'Penerima',
                    data: ((jenis == 'pengecer') ? 'npwp_pengecer_penerima' : ((jenis != 'distributor2') ? 'nib_penerima' : 'nib_d2_penerima')),
                    orderable: false,
                    render: function(k, v, r) {
                        var nib = ''
                        var nama = ''

                        if (jenis == 'produsen-cpo' || jenis == 'produsen-migor' || jenis == 'distributor1') {
                            nib = r.nib_penerima
                            nama = r.nama_penerima
                        } else if (jenis == 'distributor2') {
                            nib = r.nib_d2_penerima
                            nama = r.nama_d2_penerima
                            if (r.nib_d2_penerima == '') {
                                nib = r.npwp_pengecer_penerima
                                nama = r.nama_pengecer_penerima
                            }
                        } else if (jenis == 'pengecer') {
                            nib = r.npwp_pengecer_penerima
                            nama = r.nama_pengecer_penerima
                        }

                        return nib + '<br>' + nama
                    }
                },
                {
                    title: 'Tanggal Pengiriman',
                    data: 'tgl_pengiriman',
                    render: function(k, v, r) {
                        return r.status + '<br>' + r.tgl_pengiriman + '<br>' + r.provinsi
                    }
                },

            ],
            "scrollX": true,
            "displayLength": 5
        });
    }


    function showPemantauanDetail(id) {
        if ($.fn.DataTable.isDataTable('#tbl_pemantauan_detail')) {
            $("#tbl_pemantauan_detail").dataTable().fnDestroy();
            $('#tbl_pemantauan_detail').empty();
        }
        var table = $('#tbl_pemantauan_detail').DataTable({
            dom: 'rtip',
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            "ajax": {
                "type": 'POST',
                "data": {
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                    refresh: 'refresh',
                    pemantauan_migor_id: id,
                },
                "url": '<?= base_url() ?>perdagangan/migor/getPemantauanMigorDetail',
            },
            columns: [{
                    title: 'No',
                    data: 'pemantauan_migor_detail_id',
                    className: 'text-center',
                    orderable: false,
                    render: function(k, v, r, m) {
                        return (m.settings._iDisplayStart) + (m.row + 1)
                    }
                },
                {
                    title: 'Pengirim',
                    data: 'nib_pengirim',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.no_dokumen + '<br>' + r.nib_pengirim + '<br>' + r.nama_pengirim
                    }
                },
                {
                    title: 'Jumlah',
                    data: 'jml_distribusi',
                    render: function(k, v, r) {
                        return 'Jumlah <br>' + r.jml_distribusi + '<br>Harga Pembelian <br>' + r.harga_pembelian
                    }
                },
                {
                    title: 'Jumlah Sebenarnya',
                    data: 'jml_distribusi',
                    render: function(k, v, r) {
                        var selisih_jumlah = ''
                        var selisih_harga = ''

                        if (r.jml_distribusi != '' && r.jml_distribusi != '-') {
                            selisih_jumlah = ' <b>( ' + (parseFloat(r.jml_distribusi_rill) - parseFloat(r.jml_distribusi)) + ' )</b> '
                            selisih_harga = ' <b>( ' + (parseFloat(r.harga_pembelian_rill) - parseFloat(r.harga_pembelian)) + ' ) </b>'
                        }


                        return 'Jumlah <br>' + r.jml_distribusi_rill + selisih_jumlah + '<br>Harga Pembelian <br>' + r.harga_pembelian_rill + selisih_harga
                    }
                },
                {
                    title: 'Harga Penjualan',
                    data: 'harga_penjualan_rill',
                    orderable: false
                },
                {
                    title: 'Stok',
                    data: 'stok',
                    orderable: false
                },
                {
                    title: 'Lampiran',
                    data: 'stok',
                    orderable: false,
                    render: function(k, v, r) {

                        var button = '-'

                        if (r.lampiran != null) {
                            button = '<a href="<?= base_url() ?>services/download/migor/pelaporan/' + r.lampiran + '"><i class="ti ti-download"></i></a>'
                        }

                        return button

                    }
                },
                {
                    title: 'Periode Pengawasan',
                    data: 'periode',
                    render: function(k, v, r) {
                        var color = 'danger'
                        var kesesuaian = ''
                        if (r.kesesuaian != null) {
                            if (r.kesesuaian == 'sesuai') {
                                color = 'success'
                                kesesuaian = r.kesesuaian.toString().toUpperCase()
                            } else {
                                kesesuaian = 'TIDAK SESUAI'
                            }
                        }

                        return r.periode + '<br><div class="badge bg-' + color + '">' + kesesuaian + '</div><br><button type="button" class="btn btn-sm btn-warning m-1" disabled onclick="edit(' + r.pemantauan_migor_id + ')"><i class="ti ti-edit"></i></button><button type="button" class="btn btn-sm btn-danger m-1" onclick="update(\'' + r.pemantauan_migor_detail_id + '\',\'Deleted\',\'pemantauan_migor_detail\',\'pemantauan_migor_detail_id\',\'Hapus pemantauan migor detail ' + r.pemantauan_migor_detail_id + '\',\'mirah\')"><i class="ti ti-trash"></i></button><br><i>' + r.keterangan + '</i>'
                    }
                },
            ],
            "scrollX": true,
            "displayLength": 5
        });
    }

    function after_update() {
        showTable();
        if ($('#modal_pengawasan').is(':visible')) {
            if (pemantauan_detail_id != '') {
                showPemantauanDetail(pemantauan_detail_id)
            }

        }
    }
</script>