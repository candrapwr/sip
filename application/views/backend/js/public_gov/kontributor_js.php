<script>
    $(function() {
        showData('refresh')

        $('#assign_pasar').selectize()
        $('#post_status').selectize()
    })

    $('#post_status').change(function() {
		showData('refresh')
	});

    function showData(refresh = null) {

        if ($.fn.DataTable.isDataTable('#table')) {
            $("#table").dataTable().fnDestroy();
            $('#table').empty();
        }

        var table = $('#table').DataTable({
      
            processing: true,
            language: {
                "processing": "<lottie-player src=\"<?= base_url() ?>assets/brand/load-data.json\" background=\"transparent\" speed=\"1\" style=\"width:100px\" loop autoplay></lottie-player>"
            },
            serverSide: true,
            ordering: false,
            "ajax": {
                "type": 'POST',
                "url": '<?= base_url() ?>dashboard/perdagangan/kontributor/json',
                "data": {
                    refresh: refresh,
                    status:$('#post_status').val(),
                    csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                }
            },
            columns: [{
                    title: 'Nama',
                    data: 'nama_lengkap',
                    orderable: false
                },
                {
                    title: 'Handphone',
                    data: 'no_hp',
                    orderable: false
                },
                {
                    title: 'Email',
                    data: 'email',
                    orderable: false
                },
                {
                    title: 'Alamat',
                    data: 'alamat',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.alamat + ', ' + r.kecamatan + ', ' + r.kab_kota + ', ' + r.provinsi
                    }
                },
                {
                    title: 'Status',
                    data: 'status',
                    orderable: false,
                    render: function(k, v, r) {
                        return r.status == 'Belum Verifikasi' ? '<span class="badge bg-warning">Belum Aktivasi</span>' : '<span class="badge bg-secondary">'+r.status+'</span>'
                    },
                    className: 'text-center'
                },
                {
                    title: 'Aksi',
                    data: 'pengguna_id',
                    render: function(k, v, r, m) {

                        if (r.status == 'Belum Verifikasi') {
                            return ''
                        } else {
                            return r.status == 'Menunggu Verifikasi' ? '<button class="btn btn-sm btn-success" title="Verifikasi Pengguna" onclick="approve(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"><i class="ti ti-refresh me-1"></i>Verif</button>' : '<button class="btn btn-sm btn-primary" title="Assign Pasar" onclick="assign(\'' + m.row + '\',\'' + (m.settings._iDisplayStart) + '\',\'' + (m.settings._iDisplayLength) + '\')"><i class="ti ti-pencil me-1"></i>Assign</button>'
                        }
                    },
                    className: 'text-center'
                },


            ],
            "scrollX": true,
            "displayLength": 10
        });
    }

    $('#form-data-assign').submit(function(e) {
        e.preventDefault()

        $.ajax({
            url: "<?= site_url() ?>dashboard/perdagangan/kontributor/save",
            type: 'post',
            data: $(this).serialize(),
            dataType: 'json',
            success: function(response) {
                if (response.success) {
                    Swal.fire({
                        title: 'Success!',
                        icon: 'success',
                        text: 'Data berhasil disimpan.'

                    })
                }
            }
        })
    })


    function assign(action_index, start, length) {

        $.ajax({
            url: '<?= base_url() ?>dashboard/perdagangan/kontributor/assign_json',
            type: 'POST',
            dataType: 'json',
            data: {
                csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                action_index: action_index,
                start: start,
                length: length
            },
            beforeSend: function() {
                $('#assign_pasar').selectize()[0].selectize.setValue('')
                $('#assign_pasar').selectize()[0].selectize.clearOptions()
            },
            success: function(response) {

                $("#tambahpengguna").modal('show')

                $.each(response.data_pasar, function(i, row) {
                    $('#assign_pasar').selectize()[0].selectize.addOption({
                        value: row.pasar_id,
                        text: row.nama
                    })

                    $('#assign_pasar').selectize()[0].selectize.setValue(response.pasar_id)
                })

                $('#pengguna_id').val(response.userID)
                $('#email').val(response.email)
            }
        })


    }

    function approve(action_index, start, length) {
        Swal.fire({
            title: 'Peringatan!',
            html: '<p>Apakah Anda yakin ingin verifikasi?</p> <p>Sistem otomatis akan mengirimkan email ke pengguna.</p>',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Verifikasi'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '<?= base_url() ?>dashboard/perdagangan/kontributor/approve',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        csrf_baseben: '<?= $this->security->get_csrf_hash() ?>',
                        action_index: action_index,
                        start: start,
                        length: length
                    },
                    success: function(response) {
                        if (response.success) {
                            Swal.fire({
                                title: 'Success!',
                                icon: 'success',
                                text: response.message
                            }).then((result) => {
                                window.location.href = '<?= site_url() ?>dashboard/perdagangan/kontributor'
                            })
                        } else {
                            Swal.fire({
                                title: 'Error!',
                                icon: 'success',
                                text: response.message
                            })
                        }
                    },
                    error: function(xhr, e) {
                        if (xhr.status != '200') {
                            Swal.fire({
                                title: 'Oopsss...!',
                                text: 'Internal Server Error.'
                            })
                        }
                    }
                })
            }
        })

    }
</script>