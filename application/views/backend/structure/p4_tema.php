<div class="right-bar">
    <div data-simplebar class="h-100">
        <div class="rightbar-title d-flex align-items-center px-3 py-4">

            <h5 class="m-0 me-1">Pengaturan Tema</h5>

            <a href="javascript:void(0);" class="right-bar-toggle ms-auto">
                <i class="ti ti-x noti-icon"></i>
            </a>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <div class="p-4">
            <div class="mb-2">
                <img src="<?= base_url() ?>assets/backend/images/layouts/theme-day.png" class="img-fluid img-thumbnail rounded" alt="layout-1">
            </div>

            <div class="form-check form-switch mb-3">
                <input class="form-check-input theme-choice" type="checkbox" id="light-mode-switch" checked>
                <label class="form-check-label" for="light-mode-switch">Terang</label>
            </div>

            <div class="mb-2">
                <img src="<?= base_url() ?>assets/backend/images/layouts/theme-night.png" class="img-fluid img-thumbnail rounded" alt="layout-2">
            </div>
            <div class="form-check form-switch mb-3">
                <input class="form-check-input theme-choice" type="checkbox" id="dark-mode-switch" data-bsStyle="<?= base_url() ?>assets/backend/css/bootstrap-dark.min.css" data-appStyle="<?= base_url() ?>assets/backend/css/app-dark.min.css">
                <label class="form-check-label" for="dark-mode-switch">Gelap</label>
            </div>
        </div>

    </div> <!-- end slimscroll-menu-->
</div>

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>