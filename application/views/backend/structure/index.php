<!DOCTYPE html>
<html lang="id">

<!-- CMS Sistem Informasi Sarana Perdagangan (SISP)
Developed by Pusat Data & Sistem Informasi
Kementerian Perdagangan RI -->

<head>
    <!-- Meta Tag & CSS -->
    <?php $this->load->view('backend/structure/p1_head', $data, $rolemenu); ?>
</head>

<body>
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- Menu Desktop / Mobile -->
        <?php $this->load->view('backend/structure/p2_menu', $data, $rolemenu); ?>

        <div class="main-content">

            <!-- Isi Konten -->
            <?php $this->load->view('backend/contents/' . $rolemenu  . $content); ?>

            <!-- Footer -->
            <?php $this->load->view('backend/structure/p3_footer', $rolemenu); ?>

        </div>
    </div>

    <!-- Tema Gelap / Terang -->
    <?php $this->load->view('backend/structure/p4_tema'); ?>

    <!-- JS -->
    <?php $this->load->view('backend/structure/p5_js', $rolemenu); ?>
</body>

</html>