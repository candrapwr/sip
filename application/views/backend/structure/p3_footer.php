<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">Hak Cipta © <?= date('Y'); ?> <a href="https://www.instagram.com/pdsi.kemendag/" target="_blank"> Pusat Data dan Sistem Informasi</a></div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block"><span class="fw-bold">Kementerian Perdagangan RI</span></div>
            </div>
        </div>
    </div>
</footer>