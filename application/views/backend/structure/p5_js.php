<script>
    // Init Nilai Base URL di JS
    var base_url = '<?= base_url() ?>';
</script>

<!-- JAVASCRIPT -->
<script src="<?= base_url() ?>assets/backend/libs/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/metismenu/metisMenu.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/simplebar/simplebar.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/node-waves/waves.min.js"></script>

<!-- Plugins js-->
<script src="<?= base_url() ?>assets/backend/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?= base_url() ?>assets/backend/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
<!-- <script src="<?= base_url() ?>assets/backend/js/contents/dashboard.init.js"></script> -->
<script src="<?= base_url() ?>assets/backend/js/app.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/js/bootstrap-datepicker.min.js" integrity="sha512-LsnSViqQyaXpD4mBBdRYeP6sRwJiJveh2ZIbW41EBrNmKxgr/LFZIiWT6yr+nycvhvauz8c2nYMhrP80YhG7Cw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js "></script>
<script src="https://cdn.jsdelivr.net/npm/@tabler/icons@1.74.0/icons-react/dist/index.umd.min.js"></script>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.6/js/standalone/selectize.js" integrity="sha512-X6kWCt4NijyqM0ebb3vgEPE8jtUu9OGGXYGJ86bXTm3oH+oJ5+2UBvUw+uz+eEf3DcTTfJT4YQu/7F6MRV+wbA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<!-- Datatable -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.12.1/af-2.4.0/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/cr-1.5.6/date-1.1.2/fc-4.1.0/fh-3.2.4/r-2.3.0/rg-1.2.0/rr-1.2.8/sc-2.0.7/datatables.min.js"></script>


<!-- Additional JS -->
<script src="<?= base_url() ?>assets/backend/js/addons.js"></script>
<script src="https://kit.fontawesome.com/1069895bc6.js" crossorigin="anonymous"></script>

<script>
    //Boostrap Validation
    $(function() {

        AOS.init();

        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
            .forEach(function(form) {
                form.addEventListener('submit', function(event) {
                    if (!form.checkValidity()) {
                        $('.selectize-control .selectize-input.invalid').css('border-color', 'red')
                        $('.selectize-control .selectize-input.has-items').css('border-color', '#2fb344')
                        $('.selectize-control .selectize-input.invalid').parent().parent().find('.invalid-feedback').show()
                        $('.selectize-control .selectize-input.has-items').parent().parent().find('.invalid-feedback').hide()
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
    })

    $.each($('input:not(:checkbox):not(:radio)[required],textarea[required],select[required]').parent().find('label'), function(k, v) {
        $(v).html($(v).html() + ' <b style="color:red">*</b>')
        var params_name = $(v).parent().find('label').text().toString().replace('*', '').toLowerCase();
        var params_text = params_name.toString().split('_').join(' ');
        var invalid_text = 'Parameter ' + params_text + ' diperlukan'

        if (params_name == 'email') {
            invalid_text = 'Parameter ' + params_text + ' diperlukan, mohon masukan alamat email yang valid'
        } else if (params_name == 'url' || params_name == 'link' || params_name == 'website') {
            invalid_text = 'Parameter ' + params_text + ' diperlukan, mohon masukan url yang valid'
        }
        $(v).parent().append('<div class="invalid-feedback">' + invalid_text + '</div>')
    })

    $.each($('input:radio[required],input:checkbox[required]').parent().parent().children('label'), function(k, v) {
        var params_name = $(v).parent().find('label').text().toString().replace('*', '').toLowerCase();
        var params_text = params_name.toString().split('_').join(' ');
        $(v).html($(v).html() + ' <b style="color:red">*</b>')
        $(v).parent().append('<div class="invalid-feedback">Parameter ' + params_text + ' harus di isi </div>')
    })

    $(document).on('shown.bs.modal', function(e) {
        $.fn.dataTable.tables({
            visible: true,
            api: true
        }).columns.adjust();
    });

    function resizeJquerySteps() {
        $('.wizard .content').animate({
            height: $('.body.current').outerHeight()
        }, "slow");
    }

    jQuery.extend(jQuery.validator.messages, {
        required: "Harus di isi"
    })

    function isThisEmpty(value) {
        if (value == null || value == '') {
            return '-';
        }
        return value;
    }

    //show loading
    function showLoader() {
        $('#loader').LoadingOverlay('show', {
            background: "rgba(253, 253, 255, 0.8)",
            image: "<?= base_url() ?>assets/brand/load-data.gif",
            imageAnimation: 'none',
            text: "Sedang memproses...",
            textAutoResize: true,
            textResizeFactor: 0.13,
            textColor: "#E70A2B"
        })
    }

    //HiDE loading
    function hideLoader() {
        $('#loader').LoadingOverlay('hide')
    }

    function reset_form() {

        $(':input', '#myform')
            .not(':button, :submit, :reset, :hidden')
            .val('')
            .prop('checked', false)
            .prop('selected', false);
    };

    function addCommas(nStr) {
        return toFloat(nStr).toLocaleString(['ban', 'id']);
    }

    function fullscreen(id) {
        // if already full screen; exit
        // else go fullscreen
        if (
            document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement
        ) {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        } else {
            element = $('#' + id).get(0);
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            }
        }
    };

    $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function(e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

    function toFloat(string) {
        return Math.round(string * 100) / 100
    }

    function formatTimestamp(timestamp) {
        var date = new Date(timestamp);

        var dayNames = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        var dayName = dayNames[date.getDay()];

        var day = date.getDate();
        var month = date.toLocaleString('default', {
            month: 'long'
        });
        var year = date.getFullYear();
        return dayName + ', ' + day + ' ' + month + ' ' + year;
    }

    function update(id, status, table, primary, keterangan_perubahan, db = null, keterangan = null) {
        if (status == 'Deleted') {
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang telah terhapus tidak bisa dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonText: 'Ya, hapus!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: '<?= base_url() ?>master/update_status',
                        type: 'POST',
                        data: {
                            'key': id,
                            'key_name': primary,
                            'table_name': table,
                            'status': status,
                            'db': db,
                            'keterangan': keterangan,
                            'keterangan_perubahan': keterangan_perubahan,
                            'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                        },
                        success: function(response) {
                            response = JSON.parse(response)
                            console.log(response);
                            if (response.kode == 200) {
                                Swal.fire({
                                    title: 'Sukses!',
                                    icon: 'success',
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                                after_update()
                            } else {
                                Swal.fire({
                                    title: 'Gagal!',
                                    icon: 'warning',
                                    html: 'I will close in <b></b> milliseconds.',
                                    timer: 1000,
                                    timerProgressBar: true,
                                    showClass: {
                                        popup: 'animate__animated animate__jackInTheBox'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutDown'
                                    },
                                    buttonsStyling: false,
                                    customClass: {
                                        popup: 'border-radius-0'
                                    },
                                    didOpen: () => {
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            const content = Swal.getHtmlContainer()
                                            if (content) {
                                                const b = content.querySelector('b')
                                                if (b) {
                                                    b.textContent = Swal.getTimerLeft()
                                                }
                                            }
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                }).then((result) => {
                                    /* Read more about handling dismissals below */
                                    if (result.dismiss === Swal.DismissReason.timer) {
                                        //    console.log('I was closed by the timer')
                                    }
                                })
                            }
                        }
                    })
                }
            })
        } else {
            $.ajax({
                url: '<?= base_url() ?>master/update_status',
                type: 'POST',
                data: {
                    'key': id,
                    'key_name': primary,
                    'table_name': table,
                    'status': status,
                    'db': db,
                    'keterangan': keterangan,
                    'keterangan_perubahan': keterangan_perubahan,
                    'csrf_baseben': '<?= $this->security->get_csrf_hash() ?>'
                },
                success: function(response) {
                    response = JSON.parse(response)
                    if (response.kode == 200) {
                        Swal.fire({
                            title: 'Sukses!',
                            icon: 'success',
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                        after_update()
                    } else {
                        Swal.fire({
                            title: 'Gagal!',
                            icon: 'warning',
                            html: 'I will close in <b></b> milliseconds.',
                            timer: 1000,
                            timerProgressBar: true,
                            showClass: {
                                popup: 'animate__animated animate__jackInTheBox'
                            },
                            hideClass: {
                                popup: 'animate__animated animate__fadeOutDown'
                            },
                            buttonsStyling: false,
                            customClass: {
                                popup: 'border-radius-0'
                            },
                            didOpen: () => {
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    const content = Swal.getHtmlContainer()
                                    if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                            b.textContent = Swal.getTimerLeft()
                                        }
                                    }
                                }, 100)
                            },
                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                        }).then((result) => {
                            /* Read more about handling dismissals below */
                            if (result.dismiss === Swal.DismissReason.timer) {
                                //    console.log('I was closed by the timer')
                            }
                        })
                    }
                }
            })
        }

    }
</script>


<!-- Addons (Tambahan Custom) JS -->
<?php if (isset($js) && !empty($js)) {
    $this->load->view('backend/js/' . $rolemenu . $js);
} ?>