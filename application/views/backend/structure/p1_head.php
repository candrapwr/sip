<!-- Meta Tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="title" content="<?= (isset($title)) ? ucwords($title) . ' | ' : ''; ?> CMS Sistem Informasi Sarana Perdagangan (SISP)">
<meta name="description" content="Website Sistem Informasi Sarana Perdagangan (SISP) <?= (isset($title)) ? ' Halaman ' . $title : ''; ?>">
<meta name="keywords" content="pasar,dashboard,sisp,kementerian,perdagangan,kemendag<?= (isset($title)) ? ',' . $title : ''; ?>">
<meta name="author" content="Pusat Data dan Sistem Informasi Kementerian Perdagangan Republik Indonesia">
<meta name="robots" content="all,index,follow">
<meta name="language" content="Indonesia">
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Content-Language" content="id-ID">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<!-- Judul Halaman & Favicon -->
<title><?= (isset($title)) ? ucwords($title) . ' | ' : ''; ?> CMS Sistem Informasi Sarana Perdagangan (SISP)</title>
<link rel="shortcut icon" href="<?= base_url() ?>assets/brand/favicon.png" type="image/x-icon">

<!-- ThemeColor -->
<meta name="msapplication-TileColor" content="#E70A2B">
<meta name="msapplication-config" content="<?= base_url() ?>assets/frontend/favicon/browserconfig.xml">
<meta name="theme-color" content="#E70A2B">
<meta name="msapplication-navbutton-color" content="#E70A2B">
<meta name="apple-mobile-web-app-status-bar-style" content="#E70A2B">

<!-- CSS -->
<link href="<?= base_url() ?>assets/backend/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/backend/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/backend/css/icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/backend/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
<link href="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.css" rel="stylesheet">

<!-- CDN-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest/tabler-icons.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.10.0/css/bootstrap-datepicker.min.css" rel="stylesheet" />
<link href="https://cdn.datatables.net/v/bs5/jszip-2.5.0/dt-1.12.1/af-2.4.0/b-2.2.3/b-colvis-2.2.3/b-html5-2.2.3/b-print-2.2.3/cr-1.5.6/date-1.1.2/fc-4.1.0/fh-3.2.4/r-2.3.0/rg-1.2.0/rr-1.2.8/sc-2.0.7/datatables.min.css" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/remixicon@3.5.0/fonts/remixicon.css" rel="stylesheet">
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.6/css/selectize.bootstrap5.min.css" integrity="sha512-w4sRMMxzHUVAyYk5ozDG+OAyOJqWAA+9sySOBWxiltj63A8co6YMESLeucKwQ5Sv7G4wycDPOmlHxkOhPW7LRg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .wizard>.steps .done a,
    .wizard>.steps .done a:hover,
    .wizard>.steps .done a:active {
        background: var(--bs-dark) !important;
    }
</style>

<!-- Addons CSS -->
<?php if (isset($css) && !empty($css)) {
    $this->load->view('backend/css/' . $rolemenu . $css);
} ?>