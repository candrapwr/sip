<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">

            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="<?= base_url() ?>" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="<?= base_url() ?>assets/brand/logo-icon.svg" alt="" height="25">
                    </span>
                    <span class="logo-lg">
                        <img src="<?= base_url() ?>assets/brand/logo.svg" alt="" height="35">
                    </span>
                </a>

                <a href="<?= base_url() ?>" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="<?= base_url() ?>assets/brand/logo-icon.svg" alt="" height="25">
                    </span>
                    <span class="logo-lg">
                        <img src="<?= base_url() ?>assets/brand/logo-dark.svg" alt="" height="35">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn">
                <i class="ti ti-menu-deep"></i>
            </button>

            <div class="topbar-social-icon me-3 d-md-block">
                <ul class="list-inline title-tooltip m-0">
                    <li class="list-inline-item">
                        <a href="https://hero.kemendag.go.id/create-ticket/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.Ijci.yMedw4R3dGsxCMOlKS0nZLVGWVTbqH-SOivRlcn9lfIuF6lD-L6iEj3TlFXrYBbt943SpC5OWJBavm20oZ1O0w/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.IjU0Ig.frTpbVf1gdtOth2VJzoO841VXnuduvoasm02A38sE9Jq1ygQPgs4jdq1hLrmPhq-i4uboL7aWit_cEQ3WLF4hA" target="_blank" data-bs-toggle="tooltip" data-placement="top" title="Help Center HERO">
                            <i class="ti ti-lifebuoy"></i>
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <!-- Search input -->
        <div class="search-wrap" id="search-wrap">
            <div class="search-bar">
                <input class="search-input form-control" placeholder="Search" />
                <a href="#" class="close-search toggle-search" data-target="#search-wrap">
                    <i class="ti ti-x-circle"></i>
                </a>
            </div>
        </div>

        <div class="d-flex">
            <?php if (!empty($this->session->userdata('role_lainnya'))) {
                if (is_array($this->session->userdata('role_lainnya')) && count($this->session->userdata('role_lainnya')) > 0) {
            ?>
                    <div class="dropdown d-none d-md-block ms-2">
                        <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <small><i class="ri-loop-right-line me-1"></i>Pilih Role</small><br>
                            <span class="fw-bold text-danger"><?= $this->session->userdata('role') ?></span><i class="ri-arrow-down-s-line ms-1"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <?php foreach ($this->session->userdata('role_lainnya') as $k => $v) : ?>
                                <a href="<?= base_url() ?>web/switch-role/<?= sha1(date('Y-m-d') . $v) ?>" class="dropdown-item notify-item">
                                    <span class="align-middle"><i class="ri-arrow-right-up-line me-1"></i><?= $v ?></span>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
            <?php }
            } ?>
            <div class="dropdown d-none d-lg-inline-block ms-1">
                <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                    <i class="ti ti-arrows-maximize"></i>
                </button>
            </div>
            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="https://ui-avatars.com/api/?name=<?= $this->session->userdata('nama') ?> &background=E70A2B&color=fff&size=512" alt="Header Avatar <?= $this->session->userdata('nama') ?>">
                    <span class="d-none d-xl-inline-block ms-1 align-middle" style="line-height: 1;"><?= $this->session->userdata('nama') ?><br><small class="text-primary"><i class="ti ti-shield-checkered"></i><?= $this->session->userdata('role') ?></small></span>
                    <i class="ti ti-chevron-down d-none d-xl-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a class="dropdown-item" href="<?= site_url() ?>"><i class="ti ti-world font-size-16 align-middle me-1"></i>Web Publik</a>
                    <a class="dropdown-item" href="<?= site_url() ?>web/profil"><i class="ti ti-user font-size-16 align-middle me-1"></i>Profil Saya</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="<?= site_url('user/sign_out') ?>"><i class="ti ti-logout font-size-16 align-middle me-1 text-danger"></i>Keluar Akun</a>
                </div>
            </div>

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item noti-icon right-bar-toggle waves-effect">
                    <i class="ti ti-settings-2 font-size-20"></i>
                </button>
            </div>

        </div>
    </div>
</header>

<!-- ========== Left Sidebar Untuk Menu Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu Utama</li>
                <li>
                    <a href="<?= site_url() ?>web/dashboard" class="waves-effect">
                        <i class="ti ti-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <?php
                $res = $this->session->userdata('menu')['data'];
                foreach ($res as $k => $v) {
                    if ($v['jenis'] == 'Back end') {


                        $url = base_url() . 'web/' . $v['url'];
                        $class = 'waves-effect';

                        if ($v['url'] == '#') {
                            $url = 'javascript: void(0);';
                        }

                        if ($this->session->userdata('tipe_pengguna_id') == 3) {
                            if ($v['sub_menu']['kode'] == 200) {
                                $class = 'has-arrow waves-effect';
                            }
                        } else if (!empty($v['sub_menu'])) {
                            $class = 'has-arrow waves-effect';
                        }

                        $badge = '';
                        if ($v['menu_id'] == 81 && $this->session->userdata('tipe_pengguna_id') == 2) {
                            $badge = '<span class="badge rounded-pill bg-danger float-end">' . count(komplain(substr($this->session->userdata('daerah_id'), 0, 4))) . '</span>';
                        }

                        echo '<li>
                    <a href="' . $url . '" class="' . $class . '">
                        <i class="' . $v['icon'] . '"></i>
                        <span>' . $v['menu'] . '</span>
                        ' . $badge . '
                    </a>';

                        if ($this->session->userdata('tipe_pengguna_id') == 3) {
                            if ($v['sub_menu']['kode'] == 200) {
                                echo '<ul class="sub-menu" aria-expanded="false">';
                                foreach ($v['sub_menu']['data'] as $v => $z) {
                                    echo '<li><a href="' . base_url() . 'web/' . $z['url'] . '" data-menu-id = ' . $z['menu_id'] . '>' . $z['menu'] . '</a></li>';
                                }
                                echo '</ul>';
                            }
                        } else if (!empty($v['sub_menu'])) {
                            echo '<ul class="sub-menu" aria-expanded="false">';
                            foreach ($v['sub_menu'] as $v => $z) {
                                echo '<li><a href="' . base_url() . 'web/' . $z['url'] . '"  data-menu-id = ' . $z['menu_id'] . '>' . $z['menu'] . '</a></li>';
                            }
                            echo '</ul>';
                        }

                        echo '</li>';
                    }
                }
                ?>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->