<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <!-- <div class="col-sm-6">
                <div class="float-end">
                    <a href="" class="btn btn-success">Add Widget</a>
                </div>
            </div> -->
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Manajemen Data Kontributor</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data kontributor digitalisasi Pasar
                                    </p>

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade staticBackdrop" id="tambahpengguna" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Form Assign Kontributor</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="form-data-assign">
                            <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            <input type="hidden" name="pengguna_id" id="pengguna_id">
                            <input type="hidden" name="email" id="email">

                            <div class="modal-body">
                                <div class="mb-3 row">
                                    <label for="tipe_pengguna" class="form-label">Pilih Pasar</label>
                                    <select id="assign_pasar" name="assign_pasar[]" required multiple="multiple">
                                        <option selected value="">-- Input Pasar --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button style="width: 100%" type="submit" id="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Assign</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->