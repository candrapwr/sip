<!-- Section statistik -->
<section id="sec_peta" class="ls-section">
    <div class="auto-container">
        <div class="filters-backdrop"></div>
        <div class="row">
            <!-- Content Column -->
            <div class="content-column col-lg-12">
                <div class="ls-outer">
                    <div class="sec-title text-center">

                        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                            <div class="text">Berikut Daftar Peta Sebaran Pasar di <span class="txt_daerah"><?= $kab ?></span></div>
                        <?php endif; ?>

                    </div>
                    <div class="listing-maps style-two">
                        <div id="map" data-map-zoom="14" data-map-scroll="false">
                            <!-- map goes here -->
                        </div>

                        <input type="text" id="search-maps" class="form-control" placeholder="Select Location">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Section statistik -->