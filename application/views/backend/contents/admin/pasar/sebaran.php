<style>
    .listing-maps {
        position: relative;
    }

    .listing-maps .filter-maps {
        position: absolute;
        bottom: 25px;
        left: 20px;
        height: 35px;
        padding-left: 10px;
        padding-right: 10px;
        border: 1px solid #f1f1f1;
        width: 200px;
        border-radius: 3px;
    }

    .listing-maps.style-two #map {
        position: relative;
        min-height: 500px;
    }

    .listing-maps.style-two .form-outer {
        position: relative;
        background: #fff;
        padding: 30px 0;
    }

    .listing-maps.style-two .form-outer .form-group {
        background: none;
    }

    .listing-maps.style-two .job-search-form.style-two,
    .listing-maps.style-two .job-search-form.style-two form {
        background: none;
    }


    .listing-maps {
        position: relative;
        min-height: 500px;
    }

    .listing-maps #map {
        position: absolute;
        height: 100%;
        width: 100%;
        left: 0;
        top: 0;
    }

    .listing-maps.style-two {
        min-height: 570px;
    }

    .listing-maps .form-outer {
        position: absolute;
        left: 0;
        bottom: 30px;
        width: 100%;
    }

    .listing-maps .form-outer .job-search-form {
        margin-bottom: 0;
    }
</style>

<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Sebaran Pasar</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data Sebaran Pasar digitalisasi Pasar
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="post" action="<?= site_url() ?>dashboard/sebaran-pasar">

                                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                                                <div class="row">
                                                    <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>

                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="kabupaten" id="kabupaten">
                                                                <option value="">Kabupaten/Kota</option>

                                                                <?php foreach ($this->session->userdata('master_kabkota') as $kabkota) : ?>
                                                                    <option value="<?= $kabkota['daerah_id'] ?>" <?= ($kabkota['daerah_id'] == $this->session->flashdata('filter_search_kabupaten')) ? 'selected' : '' ?>><?= $kabkota['kab_kota'] ?></option>
                                                                    <?php endforeach; ?>?>

                                                            </select>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>

                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="provinsi" id="provinsi">
                                                                <option value="">Provinsi</option>

                                                                <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                                                    <option value="<?= $provinsi['daerah_id'] ?>" <?= ($provinsi['daerah_id'] == $this->session->flashdata('filter_search_provinsi')) ? 'selected' : '' ?>><?= $provinsi['provinsi'] ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="kabupaten" id="kabupaten">
                                                                <option value="">Kabupaten/Kota</option>
                                                            </select>
                                                            <input type="hidden" id="filter-search-kabupaten-flash" value="<?= $this->session->flashdata('filter_search_kabupaten') ?>">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                                                <option value="">Pengelola</option>
                                                            </select>
                                                            <input type="hidden" id="filter-search-pengelola-flash" value="<?= $this->session->flashdata('filter_search_pengelola') ?>">
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>

                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                                                <option value="">Semua Pengelola</option>

                                                                <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                                                    <option value="<?= $pp['email'] ?>"><?= $pp['nama_lengkap'] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                            <input type="hidden" id="filter-search-pengelola-flash" value="<?= $this->session->flashdata('filter_search_pengelola') ?>">
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>


                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                                                <option value="">Semua Pengelola</option>

                                                                <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                                                    <option value="<?= $pp['email'] ?>" <?= ($pp['email'] == $this->session->flashdata('filter_search_pengelola')) ? 'selected' : '' ?>><?= $pp['nama_lengkap'] ?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" name="tipe_pasar" id="tipe_pasar">
                                                            <option value="">Semua Tipe</option>

                                                            <?php foreach ($this->session->userdata('tipe_pasar') as $key => $row) : ?>
                                                                <option value="<?= $row['tipe_pasar_id'] ?>" <?= ($row['tipe_pasar_id'] == $this->session->flashdata('filter_search_tipe')) ? 'selected' : '' ?>><?= $row['tipe_pasar'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" name="bentuk" id="bentuk">
                                                            <option value="">Semua Bentuk</option>
                                                            <option value="1" <?= ($this->session->flashdata('filter_search_bentuk') == 1) ? 'selected' : '' ?>>Permanen</option>
                                                            <option value="2" <?= ($this->session->flashdata('filter_search_bentuk') == 2) ? 'selected' : '' ?>>Semi Permanen</option>
                                                            <option value="3" <?= ($this->session->flashdata('filter_search_bentuk') == 3) ? 'selected' : '' ?>>Tanpa Bangunan</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" name="kepemilikan" id="kepemilikan">
                                                            <option value="">Semua Kepemilikan</option>
                                                            <option value="1" <?= ($this->session->flashdata('filter_search_kepemilikan') == 1) ? 'selected' : '' ?>>Pemerintah Daerah</option>
                                                            <option value="2" <?= ($this->session->flashdata('filter_search_kepemilikan') == 2) ? 'selected' : '' ?>>Desa Adat</option>
                                                            <option value="3" <?= ($this->session->flashdata('filter_search_kepemilikan') == 3) ? 'selected' : '' ?>>Swasta</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" name="kondisi" id="kondisi">
                                                            <option value="">Semua Kondisi</option>
                                                            <option value="1" <?= ($this->session->flashdata('filter_search_kondisi') == 1) ? 'selected' : '' ?>>Baik</option>
                                                            <option value="2" <?= ($this->session->flashdata('filter_search_kondisi') == 2) ? 'selected' : '' ?>>Rusak Berat</option>
                                                            <option value="3" <?= ($this->session->flashdata('filter_search_kondisi') == 3) ? 'selected' : '' ?>>Rusak Ringan</option>
                                                            <option value="4" <?= ($this->session->flashdata('filter_search_kondisi') == 4) ? 'selected' : '' ?>>Rusak Sedang</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="d-grid gap-2">
                                                            <button type="button" name="submit" value="submit" class="btn btn-primary btn-filter">
                                                                <i class="ti ti-search me-1"></i>Filter</i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="listing-maps style-two">
                                                <div id="map" data-map-zoom="14">
                                                    <!-- map goes here -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>