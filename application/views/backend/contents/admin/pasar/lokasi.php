<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?=base_url()?>web/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row justify-content-center my-4">
                                        <div class="col-md-12">
                                            <div id="map"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

<div class="modal fade bs-example-modal-lg" id="modal_general" tabindex="-1" role="dialog" aria-labelledby="generalModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="generalModalLabel">Verifikasi Lokasi Pasar</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <form method="POST" action="<?= base_url() ?>pasar/verifikasi">
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th>Nama Pasar</th>
                                <th>:</th>
                                <td id="nama_pasar"></td>
                            </tr>
                            <tr>
                                <th rowspan="2">Alamat</th>
                                <th rowspan="2">:</th>
                                <td id="alamat"></td>
                            </tr>
                            <tr>
                                <td id="prov"></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" class="form-control" name="pasar_id" id="pasar_id">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" class="form-control" name="nama_pasar" id="nama">
                    <input type="hidden" class="form-control" name="deskripsi" id="deskripsi">
                    <input type="hidden" class="form-control" name="email" id="email" value="<?= $this->session->userdata('email') ?>">
                    <input type="hidden" class="form-control" name="alamat" id="alamat_pasar">
                    <input type="hidden" class="form-control" name="daerah_id" id="daerah_id">
                    <input type="hidden" class="form-control" name="kode_pos" id="kode_pos">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="mt-2">
                                <label class="mb-1">Latitude</label>
                                <input type="text" class="form-control" maxlength="25" name="latitude" id="latitude" placeholder="Latitude" required>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="mt-2">
                                <label class="mb-1">Longitude</label>
                                <input type="text" class="form-control" maxlength="25" name="longitude" id="longitude" placeholder="Longtitude" required>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="mt-3">
                                <div class="mt-3">
                                    <button onclick="cek_maps()" id="cek_lokasi" type="button" class="btn btn-warning waves-effect waves-light mt-3"><i class="ti ti-refresh me-1"></i>Cek</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mt-3">
                                <div id="weathermap"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Verifikasi Sekarang</button>
                </div>
            </form>
        </div>
    </div>
</div>