<div class="modal fade bs-example-modal-xl" id="modal_general" tabindex="-1" role="dialog" aria-labelledby="generalModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="title_modal"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <form id="pasar_form" action="<?= base_url() ?>pasar/save_pasar" method="POST" enctype="multipart/form-data">
                    <div id="wizard">
                        <h2><span>Lokasi Pasar</span></h2>
                        <section>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Masukkan Lokasi Pasar... (Contoh : Pujokusuman)">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" onclick="searchLocation()" type="button"><i class="ti ti-search me-1"></i>Cari</button>
                                </div>
                            </div>
                            <div id="weathermap"></div>
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="latitude" class="form-label">Latitude</label>
                                        <input type="text" class="form-control" name="latitude" id="latitude" placeholder="Latitude" required>
                                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                        <input type="hidden" name="asal" value="website">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="longitude" class="form-label">Longitude</label>
                                        <input type="text" class="form-control" name="longtitude" id="longtitude" placeholder="Longitude" required>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h2>
                            <span>Identitas Pasar</span>
                        </h2>
                        <section>
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label for="nama_pasar" class="form-label">Nama Pasar</label>
                                        <input type="text" class="form-control" name="nama_pasar" id="nama_pasar" placeholder="Nama Pasar" required>
                                        <input type="hidden" class="form-control" name="pasar_id" id="pasar_id">
                                        <input type="hidden" class="form-control" name="pasar_detail_id" id="pasar_detail_id">
                                        <input type="hidden" class="form-control" name="pasar_pengelola_id" id="pasar_pengelola_id">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="nama_pasar" class="form-label">Tipe Pasar</label>
                                        <select class="mb-3" name="tipe_pasar" id="tipe_pasar" required>
                                            <option value="">- Pilih Tipe Pasar -</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="klasifikasi" class="form-label">Klasifikasi</label>
                                        <select name="klasifikasi" id="klasifikasi" required>
                                            <option value="">- Pilih Klasifikasi -</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label for="deskripsi" class="form-label">Deskripsi</label>
                                        <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi" required></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="provinsi" class="form-label">Provinsi</label>
                                        <select name="provinsi" id="provinsi" required>
                                            <option value="">- Pilih Provinsi -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="kab_kota" class="form-label">Kabupaten/Kota</label>
                                        <select name="kab_kota" id="kab_kota" required>
                                            <option value="">- Pilih Kabupaten/Kota -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="kecamatan" class="form-label">Kecamatan</label>
                                        <select name="kecamatan" id="kecamatan" required>
                                            <option value="">- Pilih Kecamatan -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="kelurahan" class="form-label">Kelurahan</label>
                                        <select name="daerah_id" id="kelurahan" required>
                                            <option value="">- Pilih Kelurahan -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div class="mb-3">
                                        <label for="alamat" class="form-label">Alamat</label>
                                        <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="kodepos" class="form-label">Kode pos</label>
                                        <input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="Kodepos" required>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">No. telepon</label>
                                        <input type="text" class="form-control my-form-control" name="no_telp" id="no_telp" placeholder="Masukkan Nomor Telepon..." required>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">No. Fax</label>
                                        <input type="text" class="form-control my-form-control" name="no_fax" id="no_fax" placeholder="Masukkan No. Fax...">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h2><span>Informasi Tambahan</span></h2>
                        <section>
                            <div class="row mb-3 gy-3">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jarak Dengan Toko Modern (Km)</label>
                                        <input type="text" class="form-control my-form-control" name="jarak_toko_modern" id="jarak_toko_modern" placeholder="Jarak Dengan Toko Modern (Km)..." required>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Penerapan Digitalisasi Pasar</label>
                                        <select name="penerapan_digitalisasi_pasar" id="penerapan_digital_pasar" required>
                                            <option selected="" value="">Pilih Penerapan Digitalisasi Pasar</option>
                                            <option value="Ya">Ya</option>
                                            <option value="Tidak">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="display:none">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Metode Penerapan Digitalisasi Pasar</label>
                                        <select multiple name="metode_penerapan_digital_pasar[]" id="metode_penerapan_digital_pasar">
                                            <option value="">Pilih Metode Penerapan Digitalisasi Pasar</option>
                                            <option value="Pembayaran">Pembayaran (QRis)</option>
                                            <option value="Pembelian">Pembelian</option>
                                            <option value="Penjualan">Penjualan</option>
                                            <option value="Website">Website</option>
                                            <option value="Pemasaran">Pemasaran</option>
                                            <option value="E-Retribusi">E-Retribusi</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Pembiayaan Lembaga Keuangan</label>
                                        <select name="pembiayaan_lembaga_keuangan" id="pembiayaan_lembaga_keuangan" required>
                                            <option value="">Pilih Pembiayaan Lembaga Keuangan</option>
                                            <option value="Ya">Ya</option>
                                            <option value="Tidak">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="display:none">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Lembaga Keuangan</label>
                                        <input type="text" class="form-control my-form-control" autocomplete="off" name="lembaga_keuangan" id="lembaga_keuangan" placeholder="Masukkan Lembaga Keuangan...">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Sertifikasi SNI Pasar Rakyat</label>
                                        <select name="sertifikasi_sni" id="sertifikasi_sni" required>
                                            <option selected="" value="">Pilih Sertifikasi SNI Pasar Rakyat</option>
                                            <option value="Ya">Ya</option>
                                            <option value="Tidak">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6" style="display:none">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Tahun Sertifikasi SNI Pasar Rakyat</label>
                                        <input type="text" class="form-control my-form-control" name="tahun_sni_pasar_rakyat" id="tahun_sni_pasar_rakyat" placeholder="Masukkan Tahun Sertifikasi SNI Pasar Rakyat...">
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Sarana dan Prasarana</label>
                                        <select name="sarana_prasarana[]" multiple id="sarana_prasarana" required>
                                            <option value="">- Pilih Sarana dan Prasarana -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Dekat dengan pemukiman (&lt; 2km)</label>
                                        <select name="dekat_pemukiman" id="dekat_pemukiman" required>
                                            <option selected="" value="">Pilih Dekat dengan pemukiman (&lt; 2km)</option>
                                            <option value="Ya">Ya</option>
                                            <option value="Tidak">Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Dilalui Angkutan Umum</label>
                                        <textarea type="text" class="form-control my-form-control" autocomplete="off" name="angkutan_umum" id="angkutan_umum" placeholder="Bus, Taxi, Angkot, dll..." aria-label="Penjelasan Angkutan Umum" required></textarea>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h2><span>Operasional Pasar</span></h2>
                        <section>
                            <div class="row mb-3 gy-3">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Waktu Operasional</label>
                                        <select multiple name="waktu_operasional[]" id="waktu_operasional" required>
                                            <option value="">- Pilih Waktu Operasional -</option>
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                            <option value="Minggu">Minggu</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jam Buka<b style="color:red">*</b></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control my-form-control" name="jam_buka" id="jam_buka" placeholder="Jam Buka" required>
                                            <span class="input-group-text" id="basic-addon2">s/d</span>
                                            <input type="text" class="form-control my-form-control" name="jam_tutup" id="jam_tutup" placeholder="Jam Tutup" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jam Sibuk<b style="color:red">*</b></label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control my-form-control" name="jam_mulai_sibuk" id="jam_mulai_sibuk" placeholder="Jam Mulai Sibuk" required>
                                            <span class="input-group-text" id="basic-addon2">s/d</span>
                                            <input type="text" class="form-control my-form-control" name="jam_berakhir_sibuk" id="jam_berakhir_sibuk" placeholder="Jam Berakhir Sibuk" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Komoditas yang dijual</label>
                                        <select multiple name="komoditas[]" id="komoditas" required>
                                            <option value="">- Pilih Komoditas -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Omzet Pasar Tahun Sebelumnya</label>
                                        <input type="text" class="form-control my-form-control" name="omzet" pattern="[0-9]" id="omzet" placeholder="Omzet Pasar Tahun Sebelumnya" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group custom" style="background: #fff;padding: 10px;border-radius: 5px;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input check_anggaran" type="checkbox" id="tp" name="program_pasar[]" value="1">
                                            <input type="hidden" class="form-control" name="pasar_anggaran_id[]" id="pasar_anggaran_tp_id">
                                            <label class="form-check-label" for="tp">Pernah Dapat Dana Tugas Pembantuan (TP)</label>
                                        </div>
                                        <div class="row mt-3" id="content_tp" style="display:none">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Tahun<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="tahun_anggaran[]" id="tahun_tp" placeholder="Tahun TP">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Anggaran<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="jumlah_anggaran[]" id="anggaran_tp" placeholder="Anggaran TP">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Omzet<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="omset_anggaran[]" id="omzet_tp" placeholder="Omzet TP">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group custom" style="background: #fff;padding: 10px;border-radius: 5px;">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input check_anggaran" type="checkbox" name="program_pasar[]" id="dak" value="2">
                                            <input type="hidden" class="form-control" name="pasar_anggaran_id[]" id="pasar_anggaran_dak_id">
                                            <label class="form-check-label" for="dak" style="margin-bottom:0px">Pernah Dapat Dana Alokasi Khusus (DAK)</label>
                                        </div>
                                        <div class="row mt-3" id="content_dak" style="display:none">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Tahun<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="tahun_anggaran[]" id="tahun_dak" placeholder="Tahun DAK">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Anggaran<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="jumlah_anggaran[]" id="anggaran_dak" placeholder="Anggaran DAK">
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-label fw-bold">Omzet<b style="color:red">*</b></label>
                                                    <input type="text" class="form-control my-form-control" name="omset_anggaran[]" id="omzet_dak" placeholder="Omzet DAK">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Rata-Rata Pengunjung Perhari</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_pengunjung" id="jumlah_pengunjung" placeholder="Jumlah Rata-Rata Pengunjung Perhari" required>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h2><span>Bangunan Pasar</span></h2>
                        <section>
                            <div class="row mb-3 gy-3">
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Bentuk Pasar</label>
                                        <select name="bentuk_pasar" id="bentuk_pasar" required>
                                            <option selected="" value="">Pilih Bentuk Pasar</option>
                                            <option value="Permanen">Permanen</option>
                                            <option value="Semi Permanen">Semi Permanen</option>
                                            <option value="Tanpa Bangunan">Tanpa Bangunan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Kepemilikan</label>
                                        <select name="kepemilikan" id="kepemilikan" required>
                                            <option value="">- Pilih Kepemilikan -</option>
                                            <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                            <option value="Desa Adat">Desa Adat</option>
                                            <option value="Swasta">Swasta</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="kondisi_pasar" class="form-label">Kondisi Pasar</label>
                                        <select name="kondisi_pasar" id="kondisi_pasar" required>
                                            <option value="">- Pilih Kondisi Pasar -</option>
                                            <option value="Baik">Baik</option>
                                            <option value="Rusak Berat">Rusak Berat</option>
                                            <option value="Rusak Ringan">Rusak Ringan</option>
                                            <option value="Rusak Sedang">Rusak Sedang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="tata_kelola" class="form-label">Tata Kelola</label>
                                        <select name="tata_kelola" id="tata_kelola" required>
                                            <option value="">- Pilih Tata Kelola Pasar -</option>
                                            <option value="Baik">Baik</option>
                                            <option value="Cukup">Cukup</option>
                                            <option value="Kurang">Kurang</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Luas Tanah (m2)</label>
                                        <input type="text" class="form-control my-form-control" name="luas_tanah" id="luas_tanah" placeholder="Luas Tanah Pasar" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Luas Bangunan (m2)</label>
                                        <input type="text" class="form-control my-form-control" name="luas_bangunan" id="luas_bangunan" placeholder="Luas Tanah Pasar" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Tahun Dibangun</label>
                                        <input type="text" class="form-control my-form-control" name="tahun_dibangun" id="tahun_dibangun" placeholder="<?= date('Y') ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Tahun Renovasi</label>
                                        <input type="text" class="form-control my-form-control" name="tahun_renovasi" id="tahun_renovasi" placeholder="<?= date('Y') ?>" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Pekerja Tetap</label>
                                        <input type="text" class="form-control my-form-control" name="pekerja_tetap" id="pekerja_tetap" placeholder="Jumlah Pekerja Tetap" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Pekerja Non Tetap</label>
                                        <input type="text" class="form-control my-form-control" name="pekerja_nontetap" id="pekerja_nontetap" placeholder="Jumlah Pekerja Non Tetap" required>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Lantai</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_lantai" id="jumlah_lantai" placeholder="Jumlah Lantai" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Kios</label>
                                        <input type="hidden" class="form-control my-form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_kios_id">
                                        <input type="hidden" class="form-control my-form-control" name="jenis[]" id="jenis_kios" value="Kios" required>
                                        <input type="text" class="form-control my-form-control" name="jumlah[]" id="jumlah_kios" placeholder="Jumlah Kios" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Pedagang Kios</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_pedagang[]" id="jumlah_pedagang_kios" placeholder="Jumlah Pedagang Kios" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Los</label>
                                        <input type="hidden" class="form-control my-form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_los_id">
                                        <input type="hidden" class="form-control my-form-control" name="jenis[]" id="jenis_los" value="Los" required>
                                        <input type="text" class="form-control my-form-control" name="jumlah[]" id="jumlah_los" placeholder="Jumlah Los" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Pedagang Los</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_pedagang[]" id="jumlah_pedagang_los" placeholder="Jumlah Pedagang Los" required>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Jongko/Konter/Pelataran</label>
                                        <input type="hidden" class="form-control my-form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_dasaran_id">
                                        <input type="hidden" class="form-control my-form-control" name="jenis[]" id="jenis_dasaran" value="Jongko/Konter/Pelataran" required>
                                        <input type="text" class="form-control my-form-control" name="jumlah[]" id="jumlah_dasaran" placeholder="Jumlah Dasaran" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Pedagang Jongko/Konter/Pelataran</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_pedagang[]" id="jumlah_pedagang_dasaran" placeholder="Jumlah Pedagang Dasaran" required>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h2><span>Pengelola Pasar</span></h2>
                        <section>
                            <div class="row mb-3 gy-3">
                                <div class="col-lg-12 ">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Pengelola</label>
                                        <select name="pengelola" id="pengelola" required>
                                            <option selected="" value="">- Pilih Pengelola -</option>
                                            <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                            <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                            <option value="Badan Usaha Milik Negara">Badan Usaha Milik Negara</option>
                                            <option value="Badan Usaha Milik Daerah">Badan Usaha Milik Daerah</option>
                                            <option value="Koperasi">Koperasi</option>
                                            <option value="Swasta">Swasta</option>
                                            <option value="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Nama Pengelola</label>
                                        <input type="text" class="form-control my-form-control" name="nama_pengelola" id="nama_pengelola" placeholder="Nama Lengkap" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jabatan</label>
                                        <input type="text" class="form-control my-form-control" name="jabatan_pengelola" id="jabatan_pengelola" placeholder="Jabatan Pengelola" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">No. Telpon</label>
                                        <input type="text" class="form-control my-form-control" name="no_telp_pengelola" id="no_telp_pengelola" placeholder="No. Telpon Pengelola" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Email</label>
                                        <input type="email" class="form-control my-form-control" name="email_pengelola" id="email_pengelola" placeholder="Email Pengelola" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Tingkat Pendidikan</label>
                                        <select name="tingkat_pendidikan" id="tingkat_pendidikan" required>
                                            <option value="">- Pilih Tingkat Pendidikan -</option>
                                            <option value="SD">SD</option>
                                            <option value="SMP">SMP</option>
                                            <option value="SMA/SMK">SMA/SMK</option>
                                            <option value="D3">D3</option>
                                            <option value="S1">S1</option>
                                            <option value="S2">S2</option>
                                            <option value="S3">S3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label fw-bold">Jumlah Staf Pengelola</label>
                                        <input type="text" class="form-control my-form-control" name="jumlah_staf" id="jumlah_staf" placeholder="Jumlah Staf Pengelola" required>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-label">Struktur Pengelola</label>
                                        <input type="file" class="form-control my-form-control" name="struktur_pengelola" id="struktur_pengelola" placeholder="Struktur Pengelola" required>
                                        <div id="file_struktur"></div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Nama Koperasi</label>
                                        <input type="text" class="form-control my-form-control" id="nama_koperasi" autocomplete="off" value="" name="nama_koperasi" id="nama_koperasi" placeholder="Nama Koperasi">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Kategori Koperasi</label>
                                        <select class="form-select" aria-label="Kategori Koperasi" id="kategori_koperasi" name="kategori_koperasi">
                                            <option value="">- Pilih Kategori Koperasi -</option>
                                            <option value="Pengelola">Pengelola</option>
                                            <option value="Koperasi Pasar Rakyat">Koperasi Pasar Rakyat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">No Telp. Ketua Koperasi (No. Whatsapp Aktif)</label>
                                        <input type="number" class="form-control my-form-control" autocomplete="off" value="" name="no_telp_koperasi" id="no_telp_koperasi" placeholder="No Telp. Ketua Koperasi ">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Jumlah Anggota Koperasi</label>
                                        <input type="number" class="form-control my-form-control" id="jumlah_anggota_koperasi" autocomplete="off" value="" name="jumlah_anggota_koperasi" id="jumlah_anggota_koperasi" placeholder="Jumlah Anggota Koperasi">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">No. Badan Hukum Koperasi</label>
                                        <input type="text" class="form-control my-form-control" id="no_badan_hukum_koperasi" autocomplete="off" value="" name="no_badan_hukum_koperasi" id="no_badan_hukum_koperasi" placeholder="No. Badan Hukum Koperasi ">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Nomor Induk Koperasi </label>
                                        <input type="text" class="form-control my-form-control" id="no_induk_koperasi" autocomplete="off" value="" name="no_induk_koperasi" id="no_induk_koperasi" placeholder="Nomor Induk Koperasi">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Besaran Retribusi Kios</label>
                                        <input type="number" class="form-control price my-form-control" id="besaran_retribusi_kios" autocomplete="off" value="" name="besaran_retribusi_kios" id="besaran_retribusi_kios" placeholder="Besaran Retribusi Kios">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Besaran Retribusi Los</label>
                                        <input type="number" class="form-control price my-form-control" id="besaran_retribusi_los" autocomplete="off" value="" name="besaran_retribusi_los" id="besaran_retribusi_los" placeholder="Besaran Retribusi Los">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Besaran Retribusi Dasaran</label>
                                        <input type="number" class="form-control price my-form-control" id="besaran_retribusi_dasaran" autocomplete="off" value="" name="besaran_retribusi_dasaran" id="besaran_retribusi_dasaran" placeholder="Besaran Retribusi Dasaran">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label class="form-label">Pendapatan Tahunan Retribusi</label>
                                        <input type="number" class="form-control price my-form-control" id="pendapatan_tahunan_retribusi" value="" autocomplete="off" name="pendapatan_tahunan_retribusi" placeholder="Pendapatan Tahunan Retribusi">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h2>
                            <span>Foto Pasar</span>
                        </h2>
                        <section>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="foto_depan" class="form-label">Foto Pasar Tampak Depan</label>
                                        <div class="upload-file">
                                            <div class="form-group">
                                                <div class="upload-drop-zone" id="upload-drop-zone">
                                                    <div class="upload-icon me-3">
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </div>
                                                    <div class="upload-text text-center">
                                                        Drag &amp; Drop an image or <br>
                                                        click here to browse
                                                    </div>
                                                    <input type="file" class="form-control-file" name="foto_depan" id="upload-input" accept="image/*">
                                                </div>
                                            </div>
                                            <div class="file-view">
                                                <img class="file-preview" id="file-preview" src="#" alt="preview">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="mb-3">
                                        <label for="foto_depan" class="form-label">Foto Situasi Dalam Pasar</label>
                                        <div class="upload-file">
                                            <div class="form-group">
                                                <div class="upload-drop-zone" id="upload-drop-zone-2">
                                                    <div class="upload-icon me-3">
                                                        <i class="fas fa-cloud-upload-alt"></i>
                                                    </div>
                                                    <div class="upload-text text-center">
                                                        Drag &amp; Drop an image or <br>
                                                        click here to browse
                                                    </div>
                                                    <input type="file" class="form-control-file" name="foto_dalam" id="upload-input-2" accept="image/*">
                                                </div>
                                            </div>
                                            <div class="file-view">
                                                <img class="file-preview" id="file-preview-2" src="#" alt="preview">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->