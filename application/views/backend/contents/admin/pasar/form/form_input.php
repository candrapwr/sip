<div class="modal fade bs-example-modal-xl" id="modal_general_simple" tabindex="-1" role="dialog" aria-labelledby="generalModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="modal_title_simple"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">

                </button>
            </div>
            <div class="modal-body">
                <form id="pasar_form_tambah" action="<?= base_url() ?>pasar/save_pasar" method="POST" enctype="multipart/form-data">
                    <div id="wizard_pasar">

                        <div class="card">
                            <div class="card-header">
                                <h6><span>Lokasi Pasar</span></h6>
                            </div>
                            <div class="card-body">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control a_keyword" id="keyword" name="keyword" placeholder="Masukkan Lokasi Pasar... (Contoh : Pujokusuman)">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" onclick="searchLocation('simple')" type="button"><i class="ti ti-search me-1"></i>Cari</button>
                                    </div>
                                </div>

                                <div id="weathermap_simple"></div>

                                <div class="row mt-2">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="latitude" class="form-label">Latitude</label>
                                            <input type="text" class="form-control" name="latitude" id="latitude_pasar" placeholder="Latitude" required>
                                            <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="longitude" class="form-label">Longitude</label>
                                            <input type="text" class="form-control" name="longtitude" id="longtitude_pasar" placeholder="Longitude" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h6><span>Identitas Pasar</span></h6>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="nama_pasar" class="form-label">Nama Pasar</label>
                                            <input type="text" class="form-control" name="nama_pasar" id="a_nama_pasar" placeholder="Nama Pasar" required>
                                            <input type="hidden" class="form-control" name="pasar_id" id="a_pasar_id">
                                            <input type="hidden" class="form-control" name="simple" value="simple">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="mb-3">
                                            <label for="deskripsi" class="form-label">Deskripsi</label>
                                            <textarea type="text" class="form-control" name="deskripsi" id="a_deskripsi" placeholder="Deskripsi" required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="provinsi" class="form-label">Provinsi</label>
                                            <select name="provinsi" id="a_provinsi" required>
                                                <option value="">- Pilih Provinsi -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="kab_kota" class="form-label">Kabupaten/Kota</label>
                                            <select name="kab_kota" id="a_kab_kota" required>
                                                <option value="">- Pilih Kabupaten/Kota -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="kecamatan" class="form-label">Kecamatan</label>
                                            <select name="kecamatan" id="a_kecamatan" required>
                                                <option value="">- Pilih Kecamatan -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="kelurahan" class="form-label">Kelurahan</label>
                                            <select name="daerah_id" id="a_kelurahan" required>
                                                <option value="">- Pilih Kelurahan -</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="mb-3">
                                            <label for="alamat" class="form-label">Alamat</label>
                                            <input type="text" class="form-control" name="alamat" id="a_alamat" placeholder="Alamat Lengkap" required>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="mb-3">
                                            <label for="kodepos" class="form-label">Kode pos</label>
                                            <input type="text" class="form-control" name="kodepos" id="a_kodepos" placeholder="Kodepos" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h6><span>Foto Pasar</span></h6>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="foto_depan" class="form-label">Foto Pasar Tampak Depan</label>
                                            <input type="file" class="form-control" name="foto_depan" id="upload-input" accept="image/*">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-3">
                                            <label for="foto_depan" class="form-label">Foto Situasi Dalam Pasar</label>
                                            <input type="file" class="form-control" name="foto_dalam" id="upload-input-2" accept="image/*">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Simpan Data</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->