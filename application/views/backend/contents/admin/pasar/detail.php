<link href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.Default.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.css" rel="stylesheet" type="text/css" />
<style>
    .main .container {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 1rem;
        justify-content: center;
        align-items: center;
    }

    .main .card {
        color: #252a32;
        border-radius: 2px;
        background: #ffffff;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 3px rgba(0, 0, 0, 0.24);
    }

    .main .card-image {
        position: relative;
        display: block;
        width: 100%;
        padding-top: 70%;
        background: #ffffff;
    }

    .main .card-image img {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    @media only screen and (max-width: 600px) {
        .main .container {
            display: grid;
            grid-template-columns: 1fr;
            grid-gap: 1rem;
        }
    }

    .avatarpasar {
        width: 120px;
        height: 120px;
        object-fit: cover;
    }

    #tbl_kios_length,
    #tbl_kios_info,
    #tbl_kios_paginate {
        display: none;
    }

    p {
        white-space: pre-wrap;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>web/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>web/pasar">Pasar</a></li>
                            <li class="breadcrumb-item active"><?= $index_pasar['nama'] ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <input type="hidden" id="action_index" value="<?= $active_index ?>">
    <input type="hidden" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <div class="card shadow-sm border-0 overflow-hidden">
                                <div class="card-body">
                                    <div class="profile-avatar text-center">
                                        <?php
                                        // Check if $index_pasar['foto_depan'] is empty or null to set a default image path
                                        $foto_depan = (!empty($index_pasar['foto_depan'])) ? $index_pasar['foto_depan'] : base_url().'assets/frontend/img/default-market.svg';
                                        ?>
                                        <img src="<?= $foto_depan ?>" onerror="this.onerror=null; this.src='<?= base_url() ?>assets/frontend/img/default-market.svg';" class="rounded-circle shadow avatarpasar">

                                    </div>
                                    <div class="text-center mt-4">
                                        <h4 class="fw-bold"><?= $index_pasar['nama'] ?></h4>
                                        <p class="text-secondary"><i class="ti ti-map-pin me-1"></i><?= $index_pasar['kecamatan'] ?>, <?= $index_pasar['kab_kota'] ?>, <?= $index_pasar['provinsi'] ?>, <?= $index_pasar['kode_pos'] ?></p>
                                    </div>
                                    <hr>
                                    <div class="text-start">
                                        <span class="fw-bold mb-2">Informasi Pasar</span>
                                        <p class="mb-0 text-justify"><?= $index_pasar['deskripsi'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <?php
                                        $tabs = [
                                            'Identitas Pasar' => 'identitas',
                                            'Lokasi' => 'lokasi',
                                            'Informasi & Operasional' => 'informasi-operasional',
                                            'Bangunan Pasar' => 'bangunan',
                                            'Pengelola Pasar' => 'pengelola',
                                            'Harga' => 'harga',
                                            'Anggaran Pasar' => 'anggaran',
                                            'Omzet Pedagang' => 'omset-pedagang',
                                            'Bongkar Muat' => 'bongkar-muat',
                                            'Paguyuban' => 'paguyuban',
                                            'Regulasi' => 'regulasi',
                                            'Pasokan Bapok' => 'pasokan-bapok',
                                            'Foto Pasar' => 'foto'
                                        ];
                                        foreach ($tabs as $title => $id) {
                                        ?>
                                            <a class="nav-link<?= ($id === 'identitas') ? ' active' : ''; ?>" id="<?= $id; ?>-tab" data-bs-toggle="pill" href="#<?= $id; ?>" role="tab" aria-controls="<?= $id; ?>" aria-selected="<?= ($id === 'identitas') ? 'true' : 'false'; ?>"><?= $title; ?></a>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-9">
                            <div class="tab-content text-muted mt-4 mt-md-0" id="v-pills-tabContent">
                                <!-- Load Partials View -->
                                <?php
                                $views = [
                                    'identitas',
                                    'lokasi',
                                    'informasi_operasional',
                                    'bangunan',
                                    'pengelola',
                                    'harga',
                                    'anggaran',
                                    'omset_pedagang',
                                    'bongkar_muat',
                                    'paguyuban',
                                    'regulasi',
                                    'pasokan_bapok',
                                    'foto'
                                ];

                                foreach ($views as $view) {
                                    $id = str_replace('_', '-', $view);
                                    $viewPath = 'backend/contents/admin/pasar/detail/' . $view;
                                ?>
                                    <div class="tab-pane fade<?= ($view === 'identitas') ? ' show active' : ''; ?>" id="<?= $id; ?>" role="tabpanel" aria-labelledby="<?= $id; ?>-tab">
                                        <?php $this->load->view($viewPath); ?>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!--end row-->
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Modal Srart-->
<?php
$modals = [
    'identitas',
    'informasi',
    'bangunan',
    'pengelola',
    'harga',
    'anggaran',
    'omset_pedagang',
    'bongkar_muat',
    'paguyuban',
    'regulasi',
    'pasokan_bapok',
    'foto',
    'lokasi'
];

foreach ($modals as $modal) {
    $this->load->view('backend/contents/admin/pasar/detail/modal/' . $modal);
}
?>

<!-- Modal End-->