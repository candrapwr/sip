<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="<?= base_url() ?>web/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="float-end">
                                        <a href="<?= base_url() ?>web/pasar/export" class="btn btn-outline-primary me-1"><i class="ti ti-file-export me-1"></i>Export</a>
                                        <button onclick="add()" class="btn btn-primary"><i class="ti ti-plus me-1"></i>Tambah Pasar</button>
                                    </div>
                                    <h4 class="header-title">Pasar</h4>
                                    <p class="card-title-desc">Silahkan lihat, filter, dan kelola data Pasar di digitalisasi Pasar</p>
                                    <div class="row">
                                        <div class="col-3">
                                            <label for="provinsi_filter" class="form-label">Provinsi</label>
                                            <select class="mb-3" id="provinsi_filter" class="form-select">
                                                <option value="">- Pilih Provinsi -</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <label for="kabkota_filter" class="form-label">Kabupaten/Kota</label>
                                            <select class="mb-3" id="kabkota_filter" class="form-select">
                                                <option value="">- Pilih Kabupaten/Kota -</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <label for="tipe_pasar_filter" class="form-label">Tipe Pasar</label>
                                            <select class="mb-3" id="tipe_pasar_filter" class="form-select">
                                                <option value="">- Pilih Tipe Pasar -</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <div class="d-grid gap-2 mt-4">
                                                <button type="button" onclick="filter()" class="btn btn-dark btn-filter">
                                                    <i class="ti ti-search me-1"></i>Filter Data</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="alert alert-info" role="alert">
                                        <h6><i class="ti ti-bulb me-1"></i>Informasi & Keterangan</h6>
                                        <hr>
                                        <ul>
                                            <li>
                                                <span class="text-dark">Data pasar pada tahun berjalan yang <strong>SUDAH LENGKAP</strong> ditandai dengan status <span class="badge bg-success rounded me-1"><i class="ti ti-circle-check me-1"></i>Terverifikasi</span></span>
                                            </li>
                                            <li>
                                                <span class="text-dark">Data pasar pada tahun berjalan yang <strong>BELUM LENGKAP / BELUM TERKINI</strong> ditandai dengan status <span class="badge bg-warning rounded me-1"><i class="ti ti-alert-triangle me-1"></i>Perlu Verifikasi</span></span>
                                            </li>
                                            <li>
                                                <span class="text-dark">Pengelola Pasar dan Dinas yang mengajukan Proposal Revitalisasi Pasar yang dapat melakukan pemutakhiran data</span>
                                            </li>
                                            <li>
                                                <span class="text-dark">Tombol <button class="btn btn-dark btn-sm  m-1"><i class="ti ti-eye"></i></button> untuk melihat dan mengedit data pasar</span>
                                            </li>
                                            <li>
                                                <span class="text-dark">Tombol <button class="btn btn-success btn-sm  m-1"><i class="ti ti-circle-check"></i></button> untuk verifikasi data pasar</span>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    <div class="row justify-content-center">
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card timbul">
                                        <!-- <div id="map" style="height: 400px;"></div> -->
                                        <div class="card-body">
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="table" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        $this->load->view('backend/contents/admin/pasar/form/form_input');
        $this->load->view('backend/contents/admin/pasar/form/verifikasi_kelengkapan');
    ?>

</div>

<div id="modal_lokasi" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="modal_title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning" role="alert">
                    <i class="ti ti-info-circle me-1"></i>Lokasi Rekomendasi ditunjukkan dengan warna merah
                </div>
                <table id="tbl_lokasi" class="table table-striped" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Nama Lokasi</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="lokasi"></tbody>
                </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->