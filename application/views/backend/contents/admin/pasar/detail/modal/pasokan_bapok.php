<style>
    .datepicker {
        z-index: 9999 !important;
    }
</style>

<div class="modal fade staticBackdrop" id="modal_bapok" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Pasokan Bahan Pokok <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/pasokan_bapok" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_pasokan_bapok_id" id="pasar_pasokan_bapok_id">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="bulan_pasokan_bapok" class="form-label">Bulan</label>
                                <select name="bulan" id="bulan_pasokan_bapok" required>
                                    <option value="">- Pilih Bulan -</option>
                                    <option value="Januari">Januari</option>
                                    <option value="Februari">Februari</option>
                                    <option value="Maret">Maret</option>
                                    <option value="April">April</option>
                                    <option value="Mei">Mei</option>
                                    <option value="Juni">Juni</option>
                                    <option value="Juli">Juli</option>
                                    <option value="Agustus">Agustus</option>
                                    <option value="September">Sebtember</option>
                                    <option value="Oktober">Oktober</option>
                                    <option value="November">November</option>
                                    <option value="Desember">Desember</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="tahun_pasokan_bapok" class="form-label">Tahun</label>
                                <select name="tahun" id="tahun_pasokan_bapok" required>
                                    <?php
                                    for ($i = 2000; $i <= date('Y'); $i++) {
                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jenis_komoditi_pasokan_bapok" class="form-label">Jenis Komoditas</label>
                                <select name="jenis_komoditi" id="jenis_komoditi_pasokan_bapok" required>
                                    <option value="">- Pilih Jenis Komoditas -</option>
                                    <?php foreach ($jenis_komoditi as $jk) : ?>
                                        <?php if ($jk['jenis_komoditi_id'] == 1 || $jk['jenis_komoditi_id'] == 3 || $jk['jenis_komoditi_id'] == 6) { ?>
                                            <option value="<?= $jk['jenis_komoditi_id'] ?>|<?= $jk['jenis_komoditi'] ?>"><?= $jk['jenis_komoditi'] ?></option>
                                        <?php } ?>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Varian</label>
                                    <select id="varian_komoditi_pasokan_bapok" name="varian_komoditi_id" required>
                                        <option selected="" value="">Pilih Varian Komoditas</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Jumlah</label>
                                    <input type="text" name="jumlah" id="jumlah_pasokan_bapok" autocomplete="off" class="form-control price my-form-control" placeholder="Jumlah..." required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">Satuan</label>
                                    <select id="satuan_komoditi_pasokan_bapok" name="satuan_komoditi_id" required>
                                        <option selected="" value="">Pilih Satuan Komoditas</option>
                                    </select>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>