<div class="modal fade staticBackdrop" id="modal_foto" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Foto <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" id="form_foto_pasar" method="POST" action="<?= base_url() ?>cu/pasar" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="deskripsi" value="<?= $index_pasar['deskripsi'] ?>">
                                <input type="hidden" class="form-control" name="alamat" value="<?= $index_pasar['alamat'] ?>">
                                <input type="hidden" class="form-control" name="longtitude" value="<?= $index_pasar['longitude'] ?>">
                                <input type="hidden" class="form-control" name="latitude" value="<?= $index_pasar['latitude'] ?>">
                                <input type="hidden" class="form-control" name="daerah_id" value="<?= $index_pasar['daerah_id'] ?>">
                                <input type="hidden" class="form-control" name="kodepos" value="<?= $index_pasar['kode_pos'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-12" id="form_foto_depan">
                            <div class="mb-3">
                                <label for="foto_depan" id="label_foto" class="form-label">Foto Depan</label>
                                <input type="file" class="form-control" name="foto_depan" id="foto_depan" accept="image/png, image/jpeg" required>
                            </div>
                        </div>
                        <div class="col-md-12" id="form_foto_dalam">
                            <div class="mb-3">
                                <label for="foto_dalam" id="label_foto" class="form-label">Foto Dalam</label>
                                <input type="file" class="form-control" name="foto_dalam" id="foto_dalam" accept="image/png, image/jpeg" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>

            <form class="default-form" id="form_foto_lainnya" method="POST" action="<?= base_url() ?>cu/pasar/foto" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="foto_pasar" id="label_foto" class="form-label">Foto Pasar</label>
                                <input type="file" class="form-control" name="foto_pasar[]" id="foto_pasar" accept="image/png, image/jpeg" required>
                                <div id="formAdd"></div>
                            </div>
                            <button type="button" onclick="addForm()" class="btn btn-outline-primary waves-effect waves-light btn-sm"><i class="ti ti-plus me-1"></i>Tambah Form</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>


        </div>
    </div>
</div>