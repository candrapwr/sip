<div class="modal fade staticBackdrop" id="modal_bangunan" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Edit Bangunan Pasar</span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/bangunan">
                <div class="modal-body">
                    <?php
                    $bangunan_kios_id = '';
                    $jumlah_kios = 0;
                    $pedagang_kios = 0;

                    $bangunan_los_id = '';
                    $jumlah_los = 0;
                    $pedagang_los = 0;

                    $bangunan_dasaran_id = '';
                    $jumlah_dasaran = 0;
                    $pedagang_dasaran = 0;

                    if ($bangunan_pasar != '') {
                        foreach ($bangunan_pasar as $k => $v) {
                            if ($v['jenis'] == 'Kios') {
                                $bangunan_kios_id = $v['pasar_bangunan_id'];
                                $jumlah_kios = $v['jumlah'];
                                $pedagang_kios = $v['jumlah_pedagang'];
                            }

                            if ($v['jenis'] == 'Los') {
                                $bangunan_los_id = $v['pasar_bangunan_id'];
                                $jumlah_los = $v['jumlah'];
                                $pedagnag_los = $v['jumlah_pedagang'];
                            }

                            if ($v['jenis'] == 'Dasaran') {
                                $bangunan_dasaran_id = $v['pasar_bangunan_id'];
                                $jumlah_dasaran = $v['jumlah'];
                                $pedagnag_dasaran = $v['jumlah_pedagang'];
                            }
                        }
                    }
                    ?>
                    <div class="row">

                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_detail_id" id="pasar_detail_id" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['pasar_detail_id'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_kios" class="form-label">jumlah Kios</label>
                                <input type="hidden" class="form-control" name="jenis[]" id="jenis_kios" value="Kios">
                                <input type="hidden" class="form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_id" value="<?= $bangunan_kios_id ?>">
                                <input type="text" class="form-control" value="<?= $jumlah_kios ?>" name="jumlah[]" id="jumlah_kios" placeholder="Jumlah Kios" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_kios" class="form-label">Jumlah Pedagang Kios</label>
                                <input type="text" class="form-control" value="<?= $pedagang_kios ?>" name="jumlah_pedagang[]" id="pedagang_kios" placeholder="Jumlah Pedagang Kios" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_los" class="form-label">jumlah Los</label>
                                <input type="hidden" class="form-control" name="jenis[]" id="jenis_los" value="Los">
                                <input type="hidden" class="form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_id" value="<?= $bangunan_los_id ?>">
                                <input type="text" class="form-control" value="<?= $jumlah_los ?>" name="jumlah[]" id="jumlah_los" placeholder="Jumlah Los" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_los" class="form-label">Jumlah Pedagang Los</label>
                                <input type="text" class="form-control" value="<?= $pedagang_los ?>" name="jumlah_pedagang[]" id="pedagang_los" placeholder="Jumlah Pedagang Los" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_dasaran" class="form-label">jumlah Dasaran</label>
                                <input type="hidden" class="form-control" name="jenis[]" id="jenis_dasaran" value="Dasaran">
                                <input type="hidden" class="form-control" name="pasar_bangunan_id[]" id="pasar_bangunan_id" value="<?= $bangunan_dasaran_id ?>">
                                <input type="text" class="form-control" value="<?= $jumlah_dasaran ?>" name="jumlah[]" id="jumlah_dasaran" placeholder="Jumlah Dasaran" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_dasaran" class="form-label">Jumlah Pedagang Dasaran</label>
                                <input type="text" class="form-control" value="<?= $pedagang_dasaran ?>" name="jumlah_pedagang[]" id="pedagang_dasaran" placeholder="Jumlah Pedagang Dasaran" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>