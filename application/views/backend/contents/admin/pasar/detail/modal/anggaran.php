<div class="modal fade staticBackdrop" id="modal_anggaran" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Anggaran <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/anggaran">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_anggaran_id" id="pasar_anggaran_id">
                                <input type="hidden" class="form-control" name="jenis_anggaran" id="jenis_anggaran" value="pasar">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="tahun_anggaran" class="form-label">Tahun</label>
                                <select name="tahun_anggaran" id="tahun_anggaran" required>
                                    <?php
                                    for ($i = 2000; $i <= date('Y'); $i++) {
                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="program_pasar" class="form-label">Program Pasar</label>
                                <select name="program_pasar" id="program_pasar" required>
                                    <option value="">- Pilih Program Pasar -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="omzet" class="form-label">Omzet</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                    <input type="text" class="form-control" name="omset_anggaran" id="omzet_pasar" placeholder="Omzet" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_anggaran" class="form-label">Jumlah Anggaran</label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text" id="basic-addon1">Rp</span>
                                    <input type="text" class="form-control" name="jumlah_anggaran" id="jumlah_anggaran" placeholder="Jumlah Anggaran" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>