<div class="modal fade staticBackdrop" id="modal_identitas" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span id="title_modal_pasar">Edit Pasar</span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" action="<?= base_url() ?>cu/pasar" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="nama_pasar" class="form-label">Nama Pasar</label>
                                <input type="text" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="latitude" id="latitude" value="<?= $index_pasar['latitude'] ?>">
                                <input type="hidden" class="form-control" name="longtitude" id="longtitude" value="<?= $index_pasar['longitude'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="deskripsi" class="form-label">Deskripsi</label>
                                <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi" required><?= $index_pasar['deskripsi'] ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="alamat" class="form-label">Alamat</label>
                                <textarea class="form-control" name="alamat" id="alamat" placeholder="Alamat Lengkap" required><?= !empty($index_pasar['alamat']) ? $index_pasar['alamat'] : '' ?></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="provinsi" class="form-label">Provinsi</label>
                                <select name="provinsi" id="provinsi" required>
                                    <option value="">- Pilih Provinsi -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="kab_kota" class="form-label">Kabupaten/Kota</label>
                                <select name="kab_kota" id="kab_kota" required>
                                    <option value="">- Pilih Kabupaten/Kota -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="mb-3">
                                <label for="kecamatan" class="form-label">Kecamatan</label>
                                <select name="kecamatan" id="kecamatan" required>
                                    <option value="">- Pilih Kecamatan -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="mb-3">
                                <label for="kelurahan" class="form-label">Kelurahan</label>
                                <select name="daerah_id" id="kelurahan" required>
                                    <option value="">- Pilih Kelurahan -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mb-3">
                                <label for="kodepos" class="form-label">Kode Pos</label>
                                <input type="text" class="form-control" value="<?= $index_pasar['kode_pos'] ?>" name="kodepos" id="kodepos" placeholder="Kodepos" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>