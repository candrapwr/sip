<div class="modal fade staticBackdrop" id="modal_pengelola" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Edit Pengelola Pasar</span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/pengelola">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_pengelola_id" id="pasar_pengelola_id" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['pasar_pengelola_id'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="klasifikasi2" class="form-label">Klasifikasi Pengelola</label>
                                <select name="klasifikasi_pengelola" id="klasifikasi2" required>
                                    <option value="">- Pilih Klasifikasi Pengelola -</option>
                                    <option value="Pemerintah Pusat">Pemerintah Pusat</option>
                                    <option value="Pemerintah Daerah">Pemerintah Daerah</option>
                                    <option value="Badan Usaha Milik Negara">Badan Usaha Milik Negara</option>
                                    <option value="Badan Usaha Milik Daerah">Badan Usaha Milik Daerah</option>
                                    <option value="Koperasi">Koperasi</option>
                                    <option value="Swasta">Swasta</option>
                                    <option value="Badan Usaha Milik Desa">Badan Usaha Milik Desa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_kios" class="form-label">Nama Pengelola</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['nama_pengelola'] ?>" name="nama_pengelola" id="nama_pengelola" placeholder="Nama Pengelola" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jabatan" class="form-label">Jabatan</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['jabatan_pengelola'] ?>" name="jabatan_pengelola" id="jabatan_pengelola" placeholder="Jabatan" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_telp" class="form-label">No. Telpon</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['no_telp_pengelola'] ?>" name="no_telp_pengelola" id="no_telp_pengeloa" placeholder="No. Telpon" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="email_pengelola" class="form-label">Email</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['email_pengelola'] ?>" name="email_pengelola" id="email_pengelola" placeholder="Email" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="tingkat_pendidikan_pengelola" class="form-label">Tingkat Pendidikan</label>
                                <select id="tingkat_pendidikan_pengelola" name="tingkat_pendidikan" required>
                                    <option value="">- Pilih Tingkat Pendidikan -</option>
                                    <option value="SD">SD</option>
                                    <option value="SMP">SMP</option>
                                    <option value="SMA/SMK">SMA/SMK</option>
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_staf" class="form-label">Jumlah Staf Pengelola</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['jumlah_staf'] ?>" name="jumlah_staf" id="jumlah_staf" placeholder="Jumlah Staf Pengelola" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="struktur_pengelola" class="form-label">Struktur Pengelola </label>
                                <input type="file" class="form-control" name="struktur_pengelola" id="struktur_pengelola" <?= $pengelola_pasar['kode'] == 404 ? 'required' : '' ?>>

                                <?php if ($pengelola_pasar['kode'] != 404) { ?>
                                    <p>File saat ini : <a target="_blank" href="<?= $pengelola_pasar['data'][0]['file_struktur'] ?>"><?= $pengelola_pasar['data'][0]['struktur_pengelola'] ?></a></p>
                                <?php } ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_koperasi" class="form-label">Nama Koperasi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['nama_koperasi'] ?>" name="nama_koperasi" id="nama_koperasi" placeholder="Nama koperasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="kategori_koperasi" class="form-label">Kategori Koperasi</label>
                                <select id="kategori_koperasi" name="kategori_koperasi">
                                    <option value="">- Pilih Kategori Koperasi -</option>
                                    <option value="Pengelola">Pengelola</option>
                                    <option value="Koperasi Pasar Rakyat">Koperasi Pasar Rakyat</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_telp_koperasi" class="form-label">No Telp. Ketua Koperasi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['no_telp_koperasi'] ?>" name="no_telp_koperasi" id="no_telp_koperasi" placeholder="No Telp. Ketua Koperasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jumlah_anggota_koperasi" class="form-label">Jumlah Anggota Koperasi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['jumlah_anggota_koperasi'] ?>" name="jumlah_anggota_koperasi" id="jumlah_anggota_koperasi" placeholder="Jumlah Anggota Koperasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_badan_hukum_koperasi" class="form-label">No. Badan Hukum Koperasi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['no_badan_hukum_koperasi'] ?>" name="no_badan_hukum_koperasi" id="no_badan_hukum_koperasi" placeholder="No. Badan Hukum Koperasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_induk_koperasi" class="form-label">Nomor Induk Koperasi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['no_induk_koperasi'] ?>" name="no_induk_koperasi" id="no_induk_koperasi" placeholder="Nomor Induk Koperasi">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="besaran_retribusi_kios" class="form-label">Besaran Retribusi Kios</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['besaran_retribusi_kios'] ?>" name="besaran_retribusi_kios" id="besaran_retribusi_kios" placeholder="Besaran Retribusi Kios">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="besaran_retribusi_los" class="form-label">Besaran Retribusi Los</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['besaran_retribusi_los'] ?>" name="besaran_retribusi_los" id="besaran_retribusi_los" placeholder="Besaran Retribusi Los">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="besaran_retribusi_dasaran" class="form-label">Besaran Retribusi Dasaran</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['besaran_retribusi_dasaran'] ?>" name="besaran_retribusi_dasaran" id="besaran_retribusi_dasaran" placeholder="Besaran Retribusi Dasaran">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="pendapatan_tahunan_retribusi" class="form-label">Pendapatan Tahunan Retribusi</label>
                                <input type="text" class="form-control" value="<?= $pengelola_pasar['kode'] == 404 ? '' : $pengelola_pasar['data'][0]['pendapatan_tahunan_retribusi'] ?>" name="pendapatan_tahunan_retribusi" id="pendapatan_tahunan_retribusi" placeholder="Pendapatan Tahunan Retribusi">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>