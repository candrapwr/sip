<div class="modal fade staticBackdrop" id="modal_harga" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Form Harga Komoditas</span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/harga">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="harga_komoditi_id" id="harga_komoditi_id">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="jenis_komoditas" class="form-label">Jenis Komoditas</label>
                                <select name="jenis_komoditi" id="jenis_komoditas_harga" required>
                                    <option value="">- Pilih Jenis Komoditas -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="varian_komoditi" class="form-label">Varian</label>
                                <select name="varian_komoditi" id="varian_komoditi" required>
                                    <option value="">- Pilih Varian Komoditas -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="harga" class="form-label">Harga (Rp)</label>
                                <input type="text" class="form-control"  name="harga" id="harga_komoditi" placeholder="Harga (Rp)" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="satuan_komoditi" class="form-label">Satuan</label>
                                <select name="satuan_komoditi" id="satuan_komoditi" required>
                                    <option value="">- Pilih Satuan Komoditas -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>