<style>
    .datepicker {
        z-index: 9999 !important;
    }
</style>

<div class="modal fade staticBackdrop" id="modal_regulasi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Regulasi <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/regulasi" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_regulasi_id" id="pasar_regulasi_id">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_regulasi" class="form-label">No. Regulasi</label>
                                <input type="text" class="form-control" name="no_regulasi" id="no_regulasi" placeholder="No. Regulasi" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_regulasi" class="form-label">Nama Regulasi</label>
                                <input type="text" class="form-control" name="nama_regulasi" id="nama_regulasi" placeholder="Nama Regulasi" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="pengesah_regulasi" class="form-label">Pengesah Regulasi</label>
                                <input type="text" class="form-control" name="pengesah_regulasi" id="pengesah_regulasi" placeholder="Pengesah Regulasi" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="tgl_regulasi" class="form-label">Tanggal Regulasi <b style="color:red">*</b></label>
                                <div class="form-control-wrap">
                                    <div class="input-group date" id="tgl_piker">
                                        <input type="text" class="form-control border-light text-dark-1 rounded-8" id="tgl_regulasi" name="tgl_regulasi" placeholder="YYYY-mm-dd">
                                        <div class="input-group-addon">
                                            <em class="icon ni ni-calendar"></em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="file_regulasi" class="form-label">File Regulasi <b style="color:red">*</b></label>
                                <input type="file" class="form-control" name="file_regulasi" id="file_regulasi" placeholder="File Regulasi">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>