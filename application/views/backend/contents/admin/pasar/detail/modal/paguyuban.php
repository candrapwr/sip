<div class="modal fade staticBackdrop" id="modal_paguyuban" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Paguyuban <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/paguyuban">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_paguyuban_id" id="pasar_paguyuban_id">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_paguyuban" class="form-label">Nama Paguyuban </label>
                                <input type="text" class="form-control" name="nama_paguyuban" id="nama_paguyuban" placeholder="Nama Paguyuban" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_ketua" class="form-label">Nama Ketua </label>
                                <input type="text" class="form-control" name="nama_ketua" id="nama_ketua" placeholder="Nama Ketua" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_telp_ketua" class="form-label">No. Telp Ketua</label>
                                <input type="text" class="form-control" name="no_telp_ketua" id="no_telp_ketua" placeholder="No. Telp Ketua" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="asosiasi_perdagangan_pasar" class="form-label">Asosiasi Pedagang Pasar</label>
                                <select id="asosiasi_perdagangan_pasar" name="asosiasi_perdagangan_pasar">
                                    <option value="">- Pilih Asosiasi Pedagang Pasar -</option>
                                    <option value="APPSI">APPSI</option>
                                    <option value="APPSINDO">APPSINDO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>