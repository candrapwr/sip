<div class="modal fade staticBackdrop" id="modal_omzet" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Omzet Pedagang <?= $index_pasar['nama'] ?></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?= base_url() ?>cu/pasar/anggaran">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_anggaran_id" id="pasar_omzet_pedagang_id">
                                <input type="hidden" class="form-control" name="jenis_anggaran" value="pedagang">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="tahun_omzet_pedagang" class="form-label">Tahun</label>
                                <select name="tahun_anggaran" id="tahun_omzet_pedagang" required>
                                    <?php
                                    for ($i = 2000; $i <= date('Y'); $i++) {
                                        echo '<option value="' . $i . '">' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="bulan_omzet" class="form-label">Bulan</label>
                                <select name="bulan_anggaran" id="bulan_omzet" required>
                                    <option value="">- Pilih Bulan -</option>
                                    <option value="Januari">Januari</option>
                                    <option value="Februari">Februari</option>
                                    <option value="Maret">Maret</option>
                                    <option value="April">April</option>
                                    <option value="Mei">Mei</option>
                                    <option value="Juni">Juni</option>
                                    <option value="Juli">Juli</option>
                                    <option value="Agustus">Agustus</option>
                                    <option value="September">Sebtember</option>
                                    <option value="Oktober">Oktober</option>
                                    <option value="November">November</option>
                                    <option value="Desember">Desember</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label for="omset_pedagang" class="form-label">Omzet<b style="color:red">*</b>  </label>
                                <div class="input-group mb-3">
                                    <span class="input-group-text">Rp</span>
                                    <input type="text" class="form-control" name="omset_anggaran" id="omset_pedagang" placeholder="Omzet" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>