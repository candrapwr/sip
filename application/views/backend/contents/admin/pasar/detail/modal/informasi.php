<div class="modal fade staticBackdrop" id="modal_informasi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i><span>Edit Informasi & Operasional Pasar</span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" method="POST" action="<?=base_url()?>cu/pasar/detail">
                <div class="modal-body">
                    <div class="row">

                        <div class="col-md-12">
                            <div class="mb-3">
                                <input type="hidden" class="form-control" name="nama_pasar" id="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                <input type="hidden" class="form-control" name="pasar_id" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                <input type="hidden" class="form-control" name="pasar_detail_id" id="pasar_detail_id" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['pasar_detail_id'] ?>">
                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="nama_pasar" class="form-label">Tipe Pasar</label>
                                <select class="mb-3" name="tipe_pasar" id="tipe_pasar" required>
                                    <option value="">- Pilih Tipe Pasar -</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="klasifikasi" class="form-label">Klasifikasi</label>
                                <select name="klasifikasi" id="klasifikasi" required>
                                    <option value="">- Pilih Klasifikasi -</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="kondisi" class="form-label">Kondisi Pasar</label>
                                <select class="mb-3" name="kondisi" id="kondisi" required>
                                    <option value="">- Pilih Kondisi Pasar -</option>
                                    <option value="Baik">Baik</option>
                                    <option value="Rusak Berat">Rusak Berat</option>
                                    <option value="Rusak Ringan">Rusak Ringan</option>
                                    <option value="Rusak Sedang">Rusak Sedang</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="bentuk_pasar" class="form-label">Bentuk Pasar</label>
                                <select class="mb-3" name="bentuk_pasar" id="bentuk_pasar" required>
                                    <option value="">- Pilih Bentuk Pasar -</option>
                                    <option value="Permanen">Permanen</option>
                                    <option value="Semi Permanen">Semi Permanen</option>
                                    <option value="Tanpa Bangunan">Tanpa Bangunan</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_telp" class="form-label">No. Telepon</label>
                                <input type="text" class="form-control" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['no_telp'] ?>" name="no_telp" id="no_telp" placeholder="No. Telepon" required>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="no_fax" class="form-label">No. Fax</label>
                                <input type="text" class="form-control" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['no_fax'] ?>" name="no_fax" id="no_fax" placeholder="No. Fax">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Waktu Operasional</label>
                                <select multiple name="waktu_operasional[]" id="waktu_operasional" required>
                                    <option value="">- Pilih Waktu Operasional -</option>
                                    <option value="Senin">Senin</option>
                                    <option value="Selasa">Selasa</option>
                                    <option value="Rabu">Rabu</option>
                                    <option value="Kamis">Kamis</option>
                                    <option value="Jumat">Jumat</option>
                                    <option value="Sabtu">Sabtu</option>
                                    <option value="Minggu">Minggu</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Jam Buka<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="jam_buka" id="jam_buka" value="<?= $detail_pasar == '' ? date('H:i:s') : $detail_pasar[0]['jam_operasional_awal'] ?>" placeholder="Jam Buka" required>
                                    <span class="input-group-text" id="basic-addon2">s/d</span>
                                    <input type="text" class="form-control my-form-control" name="jam_tutup" id="jam_tutup" value="<?= $detail_pasar == '' ? date('H:i:s') : $detail_pasar[0]['jam_operasional_akhir'] ?>" placeholder="Jam Tutup" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Jam Sibuk<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="jam_sibuk_awal" id="jam_sibuk_awal" value="<?= $detail_pasar == '' ? date('H:i:s') : $detail_pasar[0]['jam_sibuk_awal'] ?>" placeholder="Jam Mulai Sibuk" required>
                                    <span class="input-group-text" id="basic-addon2">s/d</span>
                                    <input type="text" class="form-control my-form-control" name="jam_sibuk_akhir" id="jam_sibuk_akhir" value="<?= $detail_pasar == '' ? date('H:i:s') : $detail_pasar[0]['jam_sibuk_akhir'] ?>" placeholder="Jam Berakhir Sibuk" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Komoditas yang dijual</label>
                                <select multiple name="komoditas[]" id="komoditas" required>
                                    <option value="">- Pilih Komoditas -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Omzet Pasar Tahun Sebelumnya<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="input-group-text">Rp.</span></span>
                                    <input type="text" class="form-control my-form-control" name="omzet_sebelumnya" id="omzet" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['omzet_sebelumnya'] ?>" placeholder="Omzet Pasar Tahun Sebelumnya" required>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Pekerja Tetap<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="pekerja_tetap" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jumlah_pekerja_tetap'] ?>" id="pekerja_tetap" placeholder="Pekerja Tetap" required>
                                    <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="input-group-text">Orang</span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Pekerja Non Tetap<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="pekerja_nontetap" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jumlah_pekerja_nontetap'] ?>" id="pekerja_nontetap" placeholder="Pekerja Non Tetap" required>
                                    <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="input-group-text">Orang</span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Luas Bangunan<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="luas_bangunan" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['luas_bangunan'] ?>" id="luas_bangunan" placeholder="Luas Bangunan" required>
                                    <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="input-group-text">m<sup>2</sup></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Luas Tanah<b style="color:red">*</b></label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control my-form-control" name="luas_tanah" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['luas_tanah'] ?>" id="luas_tanah" placeholder="Luas Tanah" required>
                                    <span class="input-group-addon bootstrap-touchspin-postfix input-group-append"><span class="input-group-text">m<sup>2</sup></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Jumlah Lantai</label>
                                <input type="text" class="form-control my-form-control" name="jumlah_lantai" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jumlah_lantai'] ?>" id="jumlah_lantai" placeholder="Jumlah Lantai" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Tahun Dibangun</label>
                                <input type="text" class="form-control my-form-control" name="tahun_bangun" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['tahun_bangun'] ?>" id="tahun_bangun" placeholder="Tahun Dibangun" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Tahun Renovasi</label>
                                <input type="text" class="form-control my-form-control" name="tahun_renovasi" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['tahun_renovasi'] ?>" id="tahun_renovasi" placeholder="Tahun Renovasi" required>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Jumlah Rata-Rata Pengunjung Perhari</label>
                                <input type="text" class="form-control my-form-control" name="jumlah_pengunjung" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jumlah_pengunjung_harian'] ?>" id="jumlah_pengunjung" placeholder="Jumlah Rata-Rata Pengunjung Perhari">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Jarak Dengan Toko Modern (Km)</label>
                                <input type="text" class="form-control my-form-control" name="jarak_toko_modern" id="jarak" value="<?= $detail_pasar == '' ? '' : $detail_pasar[0]['jarak_toko_modern'] ?>" placeholder="Jarak Dengan Toko Modern (Km)...">
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Penerapan Digitalisasi Pasar</label>
                                <select name="penerapan_digitalisasi_pasar" id="penerapan_digital_pasar">
                                    <option selected="" value="">Pilih Penerapan Digitalisasi Pasar</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6" style="display:none">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Metode Penerapan Digitalisasi Pasar</label>
                                <select multiple name="metode_penerapan_digital_pasar[]" id="metode_penerapan_digital_pasar">
                                    <option value="">Pilih Metode Penerapan Digitalisasi Pasar</option>
                                    <option value="Pembayaran">Pembayaran (QRis)</option>
                                    <option value="Pembelian">Pembelian</option>
                                    <option value="Penjualan">Penjualan</option>
                                    <option value="Website">Website</option>
                                    <option value="Pemasaran">Pemasaran</option>
                                    <option value="E-Retribusi">E-Retribusi</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Pembiayaan Lembaga Keuangan</label>
                                <select name="pembiayaan_lembaga_keuangan" id="pembiayaan_lembaga_keuangan">
                                    <option value="">Pilih Pembiayaan Lembaga Keuangan</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6" style="display:none">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Lembaga Keuangan</label>
                                <input type="text" class="form-control my-form-control" autocomplete="off" name="lembaga_keuangan" id="lembaga_keuangan" placeholder="Masukkan Lembaga Keuangan...">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Sertifikasi SNI Pasar Rakyat</label>
                                <select name="sertifikasi_sni" id="sertifikasi_sni">
                                    <option selected="" value="">Pilih Sertifikasi SNI Pasar Rakyat</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6" style="display:none">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Tahun Sertifikasi SNI Pasar Rakyat</label>
                                <input type="text" class="form-control my-form-control" name="tahun_sni_pasar_rakyat" id="tahun_sni_pasar_rakyat" placeholder="Masukkan Tahun Sertifikasi SNI Pasar Rakyat...">
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Sarana dan Prasarana</label>
                                <select name="sarana_prasarana[]" multiple id="sarana_prasarana">
                                    <option value="">- Pilih Sarana dan Prasarana -</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Dekat dengan pemukiman (&lt; 2km)</label>
                                <select name="dekat_pemukiman" id="dekat_pemukiman">
                                    <option selected="" value="">Pilih Dekat dengan pemukiman (&lt; 2km)</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12 ">
                            <div class="mb-3 form-group">
                                <label class="form-label fw-bold">Dilalui Angkutan Umum</label>
                                <textarea type="text" class="form-control my-form-control" name="angkutan_umum" id="angkutan_umum" placeholder="Bus, Taxi, Angkot, dll..." aria-label="Penjelasan Angkutan Umum"><?= $detail_pasar == '' ? '' : $detail_pasar[0]['angkutan_umum'] ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>