<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <div class="col-md-12">
                    <?php if (has_access(2, 'update')) { ?>
                        <button type="button" onclick="form_identitas()" class="btn btn-outline-primary waves-effect waves-light float-end"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                    <?php } ?>
                    <h4 class="header-title mb-4 fw-bold"><i class="ti ti-info-circle me-1"></i>Identitas Pasar</h4>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Nama Pasar</label>
                            <p><?= !empty($index_pasar['nama']) ? $index_pasar['nama'] : '-' ?></p>
                        </div>
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Deskripsi</label>
                            <p><?= !empty($index_pasar['deskripsi']) ? $index_pasar['deskripsi'] : '-' ?></p>
                        </div>

                        <div class="mb-3">
                            <label class="fw-bold text-primary">Alamat</label>
                            <p><?= !empty($index_pasar['alamat']) ? $index_pasar['alamat'] : '-' ?></p>
                        </div>
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Kelurahan</label>
                            <p><?= !empty($index_pasar['kelurahan']) ? $index_pasar['kelurahan'] : '-' ?></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Kecamatan</label>
                            <p><?= !empty($index_pasar['kecamatan']) ? $index_pasar['kecamatan'] : '-' ?></p>
                        </div>

                        <div class="mb-3">
                            <label class="fw-bold text-primary">Kabupaten/Kota</label>
                            <p><?= !empty($index_pasar['kab_kota']) ? $index_pasar['kab_kota'] : '-' ?></p>
                        </div>
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Provinsi</label>
                            <p><?= !empty($index_pasar['provinsi']) ? $index_pasar['provinsi'] : '-' ?></p>
                        </div>
                        <div class="mb-3">
                            <label class="fw-bold text-primary">Kodepos</label>
                            <p><?= !empty($index_pasar['kode_pos']) ? $index_pasar['kode_pos'] : '-' ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>