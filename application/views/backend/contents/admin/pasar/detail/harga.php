<div class="card bg-dark text-white timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_harga()" class="btn btn-primary waves-effect waves-light float-end"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                <?php } ?>
                <h4 class="header-title mb-4 fw-bold text-light"><i class="ti ti-coin me-1"></i>Harga</h4>
            </div>
        </div>
    </div>
</div>
<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-9">
                <div class="mb-3">
                    <input id="filter_harga_date" type="date" value="<?= date('Y-m-d') ?>" class="form-control" placeholder="-- Pilih Tanggal --">
                </div>
            </div>
            <div class="col-sm-3">
                <div class="d-grid gap-2">
                    <button type="button" onclick="filter_harga()" class="btn btn-dark"><i class="ti ti-search me-1"></i>Filter Data</button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="table-responsive" data-aos="fade-up">
                    <table id="tbl_harga" class="table table-striped table-bordered" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
</div>