<div class="card timbul bg-dark text-white">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_foto('foto_lainnya')" class="btn btn-primary waves-effect waves-light float-end"><i class="ti ti-plus me-1"></i>Tambah Foto</button>
                <?php } ?>
                <h4 class="header-title mb-4 fw-bold text-light"><i class="ti ti-photo me-1"></i>Foto Pasar</h4>
            </div>
        </div>
    </div>
</div>
<ul class="nav nav-pills nav-justified my-3" role="tablist">
    <li class="nav-item waves-effect waves-light">
        <a class="nav-link active" data-bs-toggle="tab" href="#foto_pasar" role="tab">
            <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
            <span class="d-none d-sm-block"><i class="ti ti-camera-selfie me-1"></i>Depan & Dalam</span>
        </a>
    </li>
    <li class="nav-item waves-effect waves-light">
        <a class="nav-link" data-bs-toggle="tab" href="#profile-1" role="tab">
            <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
            <span class="d-none d-sm-block"><i class="ti ti-camera-rotate me-1"></i>Lainnya</span>
        </a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content p-3 text-muted">
    <div class="tab-pane active" id="foto_pasar" role="tabpanel">
        <div class="row">
            <div class="col-lg-6">
                <div class="card timbul">
                    <div class="card-image">
                        <a href="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" data-fancybox="gallery" data-caption="Caption Images 1">
                            <img src="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" onerror="this.onerror=null;this.src='<?= base_url() ?>assets/frontend/img/default-market.svg'" alt="Foto Depan" class="img-fluid rounded">
                        </a>
                    </div>
                    <div class="card-body">
                        <h5>Pasar Tampak Depan</h5>
                        <?php if (has_access(2, 'update')) { ?>
                            <button type="button" onclick="form_foto('foto_depan')" class="my-1 btn btn-outline-primary waves-effect waves-light w-100"><i class="ti ti-edit me-1"></i>Ubah Foto</button>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card timbul">
                    <div class="card-image">
                        <a href="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" data-fancybox="gallery" data-caption="Caption Images 1">
                            <img src="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" onerror="this.onerror=null;this.src='<?= base_url() ?>assets/frontend/img/default-market.svg'" alt="Foto Dalam" class="img-fluid rounded">
                        </a>
                    </div>
                    <div class="card-body">
                        <h5>Foto Pasar Tampak Dalam</h5>
                        <?php if (has_access(2, 'update')) { ?>
                            <button type="button" onclick="form_foto('foto_dalam')" class="my-1 btn btn-outline-primary waves-effect waves-light w-100"><i class="ti ti-edit me-1"></i>Ubah Foto</button>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="profile-1" role="tabpanel">
        <div class="row">
            <?php if (count($index_pasar['foto_lainnya']) > 0) { ?>
                <?php foreach ($index_pasar['foto_lainnya'] as $foto => $vFoto) { ?>
                    <div class="col-lg-6">
                        <div class="card timbul">
                            <div class="card-image">
                                <a href="<?= ($vFoto['foto'] != null) ? $vFoto['foto'] : base_url('assets/frontend/img/default-market.svg') ?>" data-fancybox="gallery" data-caption="Caption Images 1">
                                    <img src="<?= ($vFoto['foto'] != null) ? $vFoto['foto'] : base_url('assets/frontend/img/default-market.svg') ?>" onerror="this.onerror=null;this.src='<?= base_url() ?>assets/frontend/img/default-market.svg'" alt="Foto Pasar Lainnya" class="img-fluid rounded">
                                </a>
                            </div>
                            <div class="card-body">
                                <?php if (has_access(2, 'delete')) { ?>
                                    <button type="button" onclick="hapus_foto('<?= $vFoto['pasar_foto_id'] ?>')" class="my-1 btn btn-outline-primary waves-effect waves-light w-100"><i class="ti ti-trash me-1"></i>Hapus</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } else { ?>
                <div class="col-12 text-center">
                    <lottie-player class="me-3 bx-shadow-lg mx-auto d-block img-fluid" src="<?= base_url() ?>assets/backend/images/no-image.json" background="transparent" speed="1" style="height:200px;" loop autoplay></lottie-player>
                    <h5>Foto Pasar masih kosong</h5>
                    <span>Silahkan tambahkan / upload foto terlebih dahulu</span>
                </div>
            <?php } ?>
        </div>
    </div>
</div>