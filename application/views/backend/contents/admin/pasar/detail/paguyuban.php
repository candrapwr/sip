<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_paguyuban()" class="btn btn-outline-primary waves-effect waves-light float-end"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                <?php } ?>
                <h4 class="header-title mb-4 fw-bold"><i class="ti ti-users-group me-1"></i>Paguyuban Pasar</h4>
            </div>
            <div class="col-md-12">
                <table class="table table-striped mb-0">
                    <tbody>
                        <tr>
                            <th scope="row">Nama Paguyuban</th>
                            <td>:</td>
                            <td><?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['nama_paguyuban'] : '-' ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Nama Ketua</th>
                            <td>:</td>
                            <td>
                                <p><?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['nama_ketua'] : '-' ?></p>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">No. Telp Ketua Paguyuban</th>
                            <td>:</td>
                            <td><?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['no_telp_ketua'] : '-' ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Asosiasi Pedagang Pasar</th>
                            <td>:</td>
                            <td><?= $paguyuban_pasar['kode'] == 200 ? $paguyuban_pasar['data'][0]['asosiasi_perdagangan_pasar'] : '-' ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>