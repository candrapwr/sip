<div class="card bg-dark text-white timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_informasi()" class="btn btn-primary waves-effect waves-light float-end"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                <?php } ?>
                <h4 class="header-title text-light mb-4 fw-bold"><i class="ti ti-send me-1"></i>Informasi & Operasional Pasar</h4>
            </div>
        </div>
    </div>
</div>
<div class="card mb-3 timbul">
    <div class="card-body">
        <h5 class="card-title"><i class="ti ti-phone me-1"></i>Informasi Kontak</h5>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">No. Telepon: <?= ($detail_pasar != null && $detail_pasar[0]['no_telp'] != null) ? $detail_pasar[0]['no_telp'] : '-' ?></li>
            <li class="list-group-item">No. Fax: <?= ($detail_pasar != null && $detail_pasar[0]['no_fax'] != null) ? $detail_pasar[0]['no_fax'] : '-' ?></li>
        </ul>
    </div>
</div>

<div class="card mb-3 timbul">
    <div class="card-body">
        <h5 class="card-title"><i class="ti ti-shape me-1"></i>Ciri-ciri Pasar</h5>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Tipe Pasar: <?= ($detail_pasar != null && $detail_pasar[0]['tipe_pasar'] != null) ? $detail_pasar[0]['tipe_pasar'] : '-' ?></li>
            <li class="list-group-item">Klasifikasi: <?= ($detail_pasar != null && $detail_pasar[0]['jenis_pasar'] != null) ? $detail_pasar[0]['jenis_pasar'] : '-' ?></li>
            <li class="list-group-item">Kondisi Pasar: <?= ($detail_pasar != null && $detail_pasar[0]['kondisi'] != null) ? $detail_pasar[0]['kondisi'] : '-' ?></li>
            <li class="list-group-item">Bentuk Pasar: <?= ($detail_pasar != null && $detail_pasar[0]['bentuk_pasar'] != null) ? $detail_pasar[0]['bentuk_pasar'] : '-' ?></li>
            <li class="list-group-item">Omzet Pasar Tahun Sebelumnya: <?= ($detail_pasar != null && $detail_pasar[0]['omzet_sebelumnya'] != null) ? 'Rp. ' . $detail_pasar[0]['omzet_sebelumnya'] : '-' ?></li>
        </ul>
    </div>
</div>

<div class="card mb-3 timbul">
    <div class="card-body">
        <h5 class="card-title"><i class="ti ti-layers-subtract me-1"></i>Informasi Tambahan</h5>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Jumlah Lantai: <?= ($detail_pasar != null && $detail_pasar[0]['jumlah_lantai'] != null) ? $detail_pasar[0]['jumlah_lantai'] . ' Lantai' : '-' ?></li>
            <li class="list-group-item">Tahun Dibangun: <?= ($detail_pasar != null && $detail_pasar[0]['tahun_bangun'] != null) ? $detail_pasar[0]['tahun_bangun'] : '-' ?></li>
            <li class="list-group-item">Tahun Renovasi: <?= ($detail_pasar != null && $detail_pasar[0]['tahun_renovasi'] != null) ? $detail_pasar[0]['tahun_renovasi'] : '-' ?></li>
            <li class="list-group-item">Jumlah Rata-Rata Pengunjung per Hari: <?= ($detail_pasar != null && $detail_pasar[0]['jumlah_pengunjung_harian'] != null) ? $detail_pasar[0]['jumlah_pengunjung_harian'] . ' Orang' : '-' ?></li>
            <li class="list-group-item">Jarak Dengan Toko Modern: <?= ($detail_pasar != null && $detail_pasar[0]['jarak_toko_modern'] != null) ? $detail_pasar[0]['jarak_toko_modern'] . ' Km' : '-' ?></li>
            <li class="list-group-item">Penerapan Digitalisasi Pasar: <?= ($detail_pasar != null && $detail_pasar[0]['penerapan_digitalisasi_pasar'] != null) ? $detail_pasar[0]['penerapan_digitalisasi_pasar'] : '-' ?></li>
            <li class="list-group-item">Metode Penerapan Digitalisasi Pasar:
                <?php
                if ($detail_pasar != null && $detail_pasar[0]['detail_digitalisasi_pasar'] != null) {
                    echo '<ol style="padding-left: 10px;">';
                    $ddp = explode(',', $detail_pasar[0]['detail_digitalisasi_pasar']);
                    foreach ($ddp as $k => $v) {
                        echo '<li>' . $v . '</li>';
                    }
                    echo '</ol>';
                } else {
                    echo '-';
                }
                ?>
            </li>
            <li class="list-group-item">Sarana dan Prasarana:
                <?php
                if ($detail_pasar != null && $detail_pasar[0]['sarana_prasarana'] != null) {
                    echo '<ol style="padding-left: 10px;">';
                    $sp = explode(',', $detail_pasar[0]['sarana_prasarana']);
                    foreach ($sp as $k => $v) {
                        echo '<li>' . $v . '</li>';
                    }
                    echo '</ol>';
                } else {
                    echo '-';
                }
                ?>
            </li>
            <li class="list-group-item">Dekat dengan Pemukiman (< 2km): <?= ($detail_pasar != null && $detail_pasar[0]['jarak_pemukiman'] != null) ? $detail_pasar[0]['jarak_pemukiman'] : '-' ?></li>
            <li class="list-group-item">Dilalui Angkutan Umum: <?= ($detail_pasar != null && $detail_pasar[0]['angkutan_umum'] != null) ? $detail_pasar[0]['angkutan_umum'] : '-' ?></li>
        </ul>
    </div>
</div>
<div class="card mb-3 timbul">
    <div class="card-body">
        <h5 class="card-title"><i class="ti ti-certificate me-1"></i>Lembaga Keuangan dan Sertifikasi</h5>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Pembiayaan Lembaga Keuangan: <?= ($detail_pasar != null && $detail_pasar[0]['pembiayaan_lk'] != null) ? $detail_pasar[0]['pembiayaan_lk'] : '-' ?></li>
            <li class="list-group-item">Lembaga Keuangan: <?= ($detail_pasar != null && $detail_pasar[0]['lembaga_keuangan'] != null) ? $detail_pasar[0]['lembaga_keuangan'] : '-' ?></li>
            <li class="list-group-item">Sertifikasi SNI Pasar Rakyat: <?= ($detail_pasar != null && $detail_pasar[0]['sertifikasi_sni'] != null) ? $detail_pasar[0]['sertifikasi_sni'] : '-' ?></li>
            <li class="list-group-item">Tahun Sertifikasi SNI Pasar Rakyat: <?= ($detail_pasar != null && $detail_pasar[0]['tahun_sertifikasi'] != null) ? $detail_pasar[0]['tahun_sertifikasi'] : '-' ?></li>
        </ul>
    </div>
</div>