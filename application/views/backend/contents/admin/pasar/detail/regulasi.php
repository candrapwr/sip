<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_regulasi()" class="btn btn-primary waves-effect waves-light float-end"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                <?php } ?>
                <h4 class="header-title mb-4 fw-bold">Regulasi Pasar</h4>
            </div>
            <div class="col-md-12">
                <div class="table-responsive" data-aos="fade-up">
                    <table id="tbl_regulasi" class="table table-striped table-bordered" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
</div>