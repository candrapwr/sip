<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <button type="button" onclick="form_pengelola()" class="btn btn-outline-primary waves-effect waves-light float-end"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                <?php } ?>
                <h4 class="header-title mb-4 fw-bold"><i class="ti ti-user me-1"></i>Pengelola Pasar</h4>
            </div>
            <div class="col-md-12">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Klasifikasi Pengelola: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['klasifikasi_pengelola'] : '-' ?></li>
                    <li class="list-group-item">Nama Pengelola: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['nama_pengelola'] : '-' ?></li>
                    <li class="list-group-item">Jabatan: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['jabatan_pengelola'] : '-' ?></li>
                    <li class="list-group-item">No. Telpon: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['no_telp_pengelola'] : '-' ?></li>
                    <li class="list-group-item">Email: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['email_pengelola'] : '-' ?></li>
                    <li class="list-group-item">Tingkat Pendidikan: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['tingkat_pendidikan'] : '-' ?></li>
                    <li class="list-group-item">Jumlah Staf Pengelola: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['jumlah_staf'] . ' Orang' : '-' ?></li>
                    <li class="list-group-item">Struktur Pengelola: <a target="_blank" href="<?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['file_struktur'] : '#' ?>"><?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['struktur_pengelola'] : '-' ?></a></li>
                    <li class="list-group-item">Nama Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['nama_koperasi'] : '-' ?></li>
                    <li class="list-group-item">Kategori Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['kategori_koperasi'] : '-' ?></li>
                    <li class="list-group-item">No Telp. Ketua Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['no_telp_koperasi'] : '-' ?></li>
                    <li class="list-group-item">Jumlah Anggota Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['jumlah_anggota_koperasi'] . ' Orang' : '-' ?></li>
                    <li class="list-group-item">No. Badan Hukum Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['no_badan_hukum_koperasi'] : '-' ?></li>
                    <li class="list-group-item">Nomor Induk Koperasi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['no_induk_koperasi'] : '-' ?></li>
                    <li class="list-group-item">Besaran Retribusi Kios: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['besaran_retribusi_kios'] : '-' ?></li>
                    <li class="list-group-item">Besaran Retribusi Los: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['besaran_retribusi_los'] : '-' ?></li>
                    <li class="list-group-item">Besaran Retribusi Dasaran: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['besaran_retribusi_dasaran'] : '-' ?></li>
                    <li class="list-group-item">Pendapatan Tahunan Retribusi: <?= $pengelola_pasar['kode'] != 404 ? $pengelola_pasar['data'][0]['pendapatan_tahunan_retribusi'] : '-' ?></li>
                </ul>
            </div>
        </div>
    </div>
</div>