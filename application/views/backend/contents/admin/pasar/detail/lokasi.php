<style>
    #map {
        position: relative;
        min-height: 300px;
        z-index: 1 !important;
        border-radius: 16px;
    }
</style>
<?php if (has_access(2, 'update')) { ?>
    <div class="card timbul">
        <div class="card-body">
            <div class="row gy-3">
                <div class="col-md-12">
                    <h4 class="header-title mb-4 fw-bold"><i class="ti ti-map-pin me-1"></i>Lokasi Pasar</h4>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="keyword" name="keyword" placeholder="Masukkan Lokasi yang diinginkan...">
                        <div class="input-group-append">
                            <button class="btn btn-dark" onclick="searchLocation()" type="button"><i class="ti ti-map-search me-1"></i>Cari Lokasi</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <div id="weathermap"></div>
            </div>
            <div class="col-md-12">
                <?php if (has_access(2, 'update')) { ?>
                    <form class="default-form" action="<?= base_url() ?>cu/pasar" method="POST">
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="latitude" class="form-label">Latitude</label>
                                    <input type="text" class="form-control" value="<?= $index_pasar['latitude'] ?>" name="latitude" id="latitude_pasar" placeholder="Latitude" required>
                                    <input type="hidden" class="form-control" name="nama_pasar" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" required>
                                    <input type="hidden" class="form-control" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                                    <input type="hidden" class="form-control" name="deskripsi" value="<?= $index_pasar['deskripsi'] ?>">
                                    <input type="hidden" class="form-control" name="alamat" value="<?= $index_pasar['alamat'] ?>">
                                    <input type="hidden" class="form-control" name="daerah_id" value="<?= $index_pasar['daerah_id'] ?>">
                                    <input type="hidden" class="form-control" name="kodepos" value="<?= $index_pasar['kode_pos'] ?>">
                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label for="longitude" class="form-label">Longitude</label>
                                    <input type="text" class="form-control" value="<?= $index_pasar['longitude'] ?>" name="longtitude" id="longtitude_pasar" placeholder="Longitude" required>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary waves-effect waves-light w-100"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>