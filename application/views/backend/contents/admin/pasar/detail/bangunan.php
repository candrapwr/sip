<div class="card timbul">
    <div class="card-body">
        <div class="row gy-3">
            <div class="col-md-12">
                <div class="col-md-12">
                    <?php if (has_access(2, 'update')) { ?>
                        <button type="button" onclick="form_bangunan()" class="btn btn-outline-primary waves-effect waves-light float-end"><i class="ti ti-edit me-1"></i>Perbarui Data</button>
                    <?php } ?>
                    <h4 class="header-title mb-4 fw-bold"><i class="ti ti-building-store me-1"></i>Bangunan Pasar</h4>
                </div>
            </div>
            <div class="col-md-12">
                <div class="table-responsive" data-aos="fade-up">
                    <table id="tbl_kios" class="table table-striped table-bordered" style="width:100%">
                        <thead class="table-light">
                            <tr>
                                <th>No</th>
                                <th>Jenis Bangunan</th>
                                <th>Jumlah bangunan</th>
                                <th>Jumlah Pedagang</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $jumlah_kios = 0;
                            $jumlah_los = 0;
                            $jumlah_dasaran = 0;
                            $pedagang_kios = 0;
                            $pedagang_los = 0;
                            $pedagang_dasaran = 0;

                            if ($bangunan_pasar != '') {
                                foreach ($bangunan_pasar as $k => $v) {
                                    if ($v['jenis'] == 'Kios') {
                                        $jumlah_kios = $v['jumlah'];
                                        $pedagang_kios = $v['jumlah_pedagang'];
                                    }

                                    if ($v['jenis'] == 'Los') {
                                        $jumlah_los = $v['jumlah'];
                                        $pedagang_los = $v['jumlah_pedagang'];
                                    }

                                    if ($v['jenis'] == 'Jongko/Konter/Pelataran') {
                                        $jumlah_dasaran = $v['jumlah'];
                                        $pedagang_dasaran = $v['jumlah_pedagang'];
                                    }
                                }
                            }
                            ?>
                            <tr>
                                <th scope="row">1</th>
                                <td>Kios</td>
                                <td><?= $jumlah_kios ?></td>
                                <td><?= $pedagang_kios ?></td>
                            </tr>
                            <tr>
                                <th scope="row">2</th>
                                <td>Los</td>
                                <td><?= $jumlah_los ?></td>
                                <td><?= $pedagang_los ?></td>
                            </tr>
                            <tr>
                                <th scope="row">3</th>
                                <td>Jongko/Konter/Pelataran</td>
                                <td><?= $jumlah_dasaran ?></td>
                                <td><?= $pedagang_dasaran ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>