<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <button onclick="add()" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Satuan Komoditi</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data satuan komoditi digitalisasi Pasar
                                    </p>

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade staticBackdrop" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title">Satuan Komoditi</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="<?= base_url() ?>master/cu_satuan_komoditi" method="POST" class="needs-validation default-form" novalidate>
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="satuan_komoditi_id" id="satuan_komoditi_id" value="">

                        <div class="modal-body">
                            <div class="mb-3 row">
                                <label for="jenis_komoditi" class="col-sm-2 col-form-label">Jenis Komoditi</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="jenis_komoditi" aria-placeholder="Pilih Jenis Komoditi" name="jenis_komoditi" required>
                                        <option value="">- Pilih Jenis komoditi -</option>
                                        <?php
                                            foreach ($jenis_komoditi as $k => $v) {
                                               echo'<option value="'.$v['jenis_komoditi_id'].'">'.$v['jenis_komoditi'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="satuan_komoditi" class="col-sm-2 col-form-label">Satuan Komoditi</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="satuan_komoditi" id="satuan_komoditi" placeholder="Satuan Komoditi" value="">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal_satuan_komoditi" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal_title_v"></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <div class="table-responsive m-5">
                        <table id="table_view" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->