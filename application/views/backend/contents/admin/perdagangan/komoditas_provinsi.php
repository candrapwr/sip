<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <button id="create-commodity" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Produksi Konsumsi</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data Produksi Konsumsi digitalisasi Pasar
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form>
                                                <div class="row">
                                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                                                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                                                        <div class="col-md-3">
                                                            <select class="mb-3" aria-label="Default select example" id="commodity-province">
                                                                <option value="">- Pilih Provinsi -</option>

                                                                <?php foreach ($provinces as $province) : ?>
                                                                    <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" id="commodity-year">
                                                            <option value="">- Pilih Tahun -</option>

                                                            <?php foreach ($years as $year) : ?>
                                                                <option value="<?= $year ?>"><?= $year ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <select class="mb-3" aria-label="Default select example" id="commodity-month">
                                                            <option value="">- Pilih Bulan -</option>

                                                            <option value="1">Januari</option>
                                                            <option value="2">Februari</option>
                                                            <option value="3">Maret</option>
                                                            <option value="4">April</option>
                                                            <option value="5">Mei</option>
                                                            <option value="6">Juni</option>
                                                            <option value="7">Juli</option>
                                                            <option value="8">Agustus</option>
                                                            <option value="9">September</option>
                                                            <option value="10">Oktober</option>
                                                            <option value="11">November</option>
                                                            <option value="12">Desember</option>

                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="d-grid gap-2">
                                                            <button type="button" id="filter-commodity" class="btn btn-primary btn-filter">
                                                                <i class="ti ti-search me-1"></i>Filter</i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table-commodity" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade staticBackdrop" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Komoditas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-tambah">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                <div class="modal-body">
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                            <input class="form-control" type="date" id="date" value="<?= date('Y-m-d') ?>" name="date">
                        </div>
                    </div>

                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                        <label for="province" class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="mb-3 row">
                            <select aria-label="Default select example" name="province" id="province">
                                <option value="">- Pilih Provinsi -</option>

                                <?php foreach ($provinces as $province) : ?>
                                    <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                    <?php endif; ?>

                    <div class="mb-3 row">
                        <label for="commodity" class="col-sm-2 col-form-label">Komoditi</label>
                        <select id="commodity" name="commodity">
                            <option value="">-- Pilih Komoditi --</option>

                            <?php foreach ($jenis_komoditi as $jk) : ?>
                                <optgroup label="<?= $jk['kelompok'] ?>" class="text-center">

                                    <?php foreach ($jk['sub'] as $sub) : ?>
                                        <option value="<?= $sub['id_jenis'] ?>"><?= $sub['jenis_komoditi'] ?></option>
                                    <?php endforeach; ?>

                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="unit" class="col-sm-2 col-form-label">Satuan</label>
                            <select id="unit" name="unit">
                                <option value="">-- Pilih Satuan --</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="produksi" class="col-sm-2 col-form-label">Produksi</label>
                            <input class="form-control number" type="text" id="produksi" name="produksi" autocomplete="off">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="konsumsi" class="col-sm-2 col-form-label">Konsumsi</label>
                            <input class="form-control number" type="text" id="konsumsi" name="konsumsi" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="width: 100%" type="submit" id="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>