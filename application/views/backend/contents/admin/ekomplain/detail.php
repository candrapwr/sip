<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <?php
                        if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1) {
                            echo ' <button onclick="add()" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="d-lg-flex">
                <div class="chat-leftsidebar me-lg-4">
                    <div class="card timbul">
                        <div class="p-4">
                            <div class="chat-leftsidebar-nav">
                                <div class="user-sidebar text-center">
                                    <div class="dropdown">
                                        <div class="user-img">
                                            <img class="rounded-circle" src="https://ui-avatars.com/api/?name=<?= $data[0]['nama'] ?> &background=E70A2B&color=fff&size=512" alt="Header Avatar <?= $data[0]['nama'] ?>">
                                            <span class="avatar-online bg-success"></span>
                                        </div>
                                        <div class="user-info">
                                            <h5 class="mt-3 font-size-16 text-white"><?= $data[0]['nama'] ?></h5>
                                            <span class="font-size-13 text-white-50"><?= $data[0]['no_tiket'] ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-content py-4">
                                    <div class="tab-pane show active" id="chat">
                                        <div class="mail-list mt-4">
                                            <a href="#" class="active"><i class="ti ti-mail me-1"></i><?= $data[0]['email'] ?></a>
                                            <a href="#"><i class="ti ti-star me-1"></i><?= $data[0]['alamat'] ?></a>
                                            <a href="#"><i class="ti ti-phone me-1"></i><?= $data[0]['hp'] ?></a>
                                            <a href="#"><i class="ti ti-clock me-1"></i><?= $data[0]['createdon'] ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-100 user-chat">
                    <div class="card timbul">
                        <div class="p-4 border-bottom ">
                            <div class="row">
                                <div class="col-md-4 col-9">
                                    <h5 class="font-size-15 mb-1 text-truncate"><?= $data[0]['nama_pasar'] ?></h5>
                                    <p class="text-muted mb-0 text-truncate"><?= full_date($data[0]['createdon']) ?></p>
                                </div>
                            </div>
                        </div>


                        <div>
                            <div class="chat-conversation p-3">
                                <ul class="list-unstyled" data-simplebar="init" style="max-height: 600px;">
                                    <div class="simplebar-wrapper" style="margin: 0px;">
                                        <div class="simplebar-height-auto-observer-wrapper">
                                            <div class="simplebar-height-auto-observer"></div>
                                        </div>
                                        <div class="simplebar-mask">
                                            <div class="simplebar-offset" style="right: -20px; bottom: 0px;">
                                                <div class="simplebar-content-wrapper" style="height: auto; padding-right: 20px; padding-bottom: 0px; overflow: hidden scroll;">
                                                    <div class="simplebar-content" style="padding: 0px;">

                                                        <?php
                                                        foreach ($data[0]['tracking'] as $k => $v) {
                                                            if ($v['flag_pengelola'] == 'N') {
                                                                $nama = $data[0]['nama'];
                                                                $posisi = '';
                                                                $arrow = 'arrow-left';
                                                            } else {
                                                                $nama = 'Pengelola ' . $data[0]['nama_pasar'];
                                                                $posisi = 'right';
                                                                $arrow = 'arrow-right';
                                                            }

                                                            if ($k == 0) {
                                                                $pesan = $data[0]['komplain'];
                                                            } else {
                                                                $pesan = $v['keterangan'];
                                                            }

                                                            if ($v['status'] == 'OPEN') {
                                                                $color = 'primary';
                                                            } else if ($v['status'] == 'RESOLVED') {
                                                                $color = 'warning';
                                                            } else {
                                                                $color = 'success';
                                                            }
                                                        ?>

                                                            <li class="<?= $posisi ?>">
                                                                <div class="conversation-list">
                                                                    <div class="media">
                                                                        <?php if ($posisi != 'right') { ?>
                                                                            <img src="https://ui-avatars.com/api/?name=<?= $nama ?> &background=E70A2B&color=fff&size=512" class="rounded-circle avatar-xs" alt="">
                                                                        <?php } ?>
                                                                        <div class="media-body <?= $arrow ?> ms-3">
                                                                            <div class="ctext-wrap">
                                                                                <div class="conversation-name"><?php if ($posisi == 'right') { ?><span class="badge rounded-pill badge-soft-<?= $color ?>"><?= $v['status'] ?></span> <?php } ?> <?= $nama ?> <?php if ($posisi != 'right') { ?><span class="badge rounded-pill badge-soft-<?= $color ?>"><?= $v['status'] ?></span> <?php } ?></div>
                                                                                <p><?= $pesan ?></p>
                                                                                <p class="chat-time mb-0"><i class="bx bx-time-five align-middle me-1"></i> <?= full_date($v['createdon']) ?> <?= date('H:s', strtotime($v['createdon'])) ?></p>
                                                                            </div>
                                                                        </div>
                                                                        <?php if ($posisi == 'right') { ?>
                                                                            <img src="https://ui-avatars.com/api/?name=<?= $nama ?> &background=E70A2B&color=fff&size=512" class="rounded-circle avatar-xs" alt="">
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="simplebar-placeholder" style="width: auto; height: 667px;"></div>
                                    </div>
                                </ul>
                            </div>
                            <?php if (has_access($menu_id, 'update')) { ?>
                                <div class="p-3 chat-input-section">
                                    <form class="default-form col-lg-12" action="<?= base_url() ?>web/e-komplain/save" method="POST">
                                        <div class="row">
                                            <div class="col">
                                                <div class="position-relative">
                                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                                    <input type="hidden" value="<?= $data[0]['no_tiket'] ?>" class="form-control chat-input" name="tiket" />
                                                    <input type="hidden" value="" class="form-control chat-input" name="spam" />
                                                    <input type="text" class="form-control chat-input" name="tanggapan" placeholder="Masukkan Tanggapan Anda Lalu Pilih Status Open/Resolved..." data-ddg-inputtype="unknown">
                                                    <div class="chat-input-links" style="right: 0px;">
                                                        <select class="form-select" id="validationCustom03" name="status" required="" data-ddg-inputtype="identities.addressProvince" style="background: #ebf1f8;border: 1px solid #ebf1f8;border-radius: 50px;border-left: 1px solid #c8c8c8;text-align: center;">
                                                            <option value="OPEN">OPEN</option>
                                                            <option value="RESOLVED">RESOLVED</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" class="btn btn-primary btn-rounded chat-send w-md waves-effect waves-light"><span class="d-none d-sm-inline-block me-1">Kirim</span><i class="ti ti-send"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>
</div>
<!-- End Page-content -->