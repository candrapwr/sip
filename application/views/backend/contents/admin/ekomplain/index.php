<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title"><?= $title ?></h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data <?= $title ?>
                                    </p>

                                    <div class="row">
                                        <div class="col-3">
                                            <label for="provinsi_filter" class="form-label">Provinsi</label>
                                            <select class="mb-3" id="provinsi_filter">
                                                <option value="">- Pilih Provinsi -</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <label for="kabkota_filter" class="form-label">Kabupaten/Kota</label>
                                            <select class="mb-3" id="kabkota_filter">
                                                <option value="">- Pilih Kabupaten/Kota -</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <div class="d-grid gap-2 mt-4">
                                                <button type="button" onclick="filter()" class="btn btn-primary btn-filter">
                                                    <i class="ti ti-search me-1"></i>Filter</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade staticBackdrop" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modal-title"><?= $title ?></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="<?= base_url() ?>web/master/dokumen" method="POST" class="needs-validation default-form" novalidate>
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="dokumen_id" id="dokumen_id" value="">

                        <div class="modal-body">
                            <div class="mb-3 row">
                                <label for="jenis" class="col-sm-2 col-form-label">Jenis</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="jenis" name="jenis" required>
                                        <option value="">Pilih Jenis Dokumen</option>
                                        <option value="revitalisasi_proposal_dokumen_1">Revitalisasi Proposal Dokumen Tahap 1</option>
                                        <option value="revitalisasi_proposal_foto_pasar_1">Revitalisasi Proposal Foto Pasar Tahap 1</option>
                                        <option value="revitalisasi_proposal_dokumen_2">Revitalisasi Proposal Dokumen Tahap 2</option>
                                        <option value="revitalisasi_proposal_pengawasan_1">Revitalisasi Proposal Pengawasan Tahap 1</option>
                                        <option value="revitalisasi_proposal_pengawasan_2">Revitalisasi Proposal Pengawasan Tahap 2</option>
                                    </select>

                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                                <div class="col-sm-10">
                                    <input type="text" name="judul" id="judul" placeholder="Judul Dokumen" class="form-control">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label for="judul" class="col-sm-2 col-form-label">Deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea name="deskripsi" id="deskripsi" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="required" class="col-sm-2 col-form-label">Mandatori ?</label>
                                <div class="col-sm-3">
                                    <select class="form-control" id="required" name="required" required>
                                        <option value="Ya" selected>Ya</option>
                                        <option value="Tidak">Tidak</option>
                                    </select>

                                </div>
                                <label for="max_size" class="col-sm-2 col-form-label">Max Size (MB)</label>
                                <div class="col-sm-5">
                                    <input type="number" name="max" id="max" placeholder="Ukuran Maksimal" class="form-control" required>
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <label for="acceptx" class="col-sm-2 col-form-label">Accept</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="acceptx" name="acceptx[]" multiple>
                                        <option value="">Pilih jenis ekstensi</option>
                                        <?php
                                        $mimes = array_keys(get_mimes());
                                        sort($mimes);
                                        foreach ($mimes as $k => $v) {
                                            echo '<option value="' . $v . '">' . strtoupper($v) . '</option>';
                                        }
                                        ?>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Simpan Data</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->