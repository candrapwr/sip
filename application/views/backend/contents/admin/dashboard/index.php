<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                    </div>
                </div>
                <!-- <div class="col-sm-6">
                <div class="float-end">
                    <a href="" class="btn btn-success">Add Widget</a>
                </div>
            </div> -->
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">

        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-md-10">
                    <div class="card timbul">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-3">
                                    <lottie-player class="me-3 bx-shadow-lg mx-auto d-block img-fluid" src="<?= base_url() ?>assets/backend/images/illust-dashboard.json" background="transparent" speed="1" loop autoplay></lottie-player>
                                </div>
                                <div class="col-12 col-md-9">
                                    <h3 class="mt-3">Hi, <?= $this->session->userdata('nama') ?> !</h3>
                                    <p>Selamat Datang Kembali di CMS Digitalisasi Pasar SISP. Silahkan kunjungi menu-menu yang ada untuk mengakes data yanga anda inginkan.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <?php if (in_array($this->session->userdata('tipe_pengguna_id'), [3, 2, 8, 9, 18, 19, 10, 11, 13, 14])) { ?>
                        <div class="row">
                            <div class="col-md-3 col-12 mb-4">
                                <div class="card timbul h-100" data-aos="flip-up">
                                    <div class="card-body">
                                        <div class="text-center">
                                            <p class="font-size-16 fw-bold">Jumlah<br>Pasar</p>
                                            <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                <span class="avatar-title rounded-circle bg-soft-info">
                                                    <i class="ti ti-building-store text-info font-size-40"></i>
                                                </span>
                                            </div>
                                            <h5 class="font-size-22" id="stat-jumlah-pasar"></h5>
                                            <p class="text-muted">Pasar</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-12 mb-4">
                                <div class="card timbul h-100" data-aos="flip-up">
                                    <div class="card-body">
                                        <div class="text-center">
                                            <p class="font-size-16 fw-bold">Pasar Diinput Bulan Ini</p>
                                            <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                <span class="avatar-title rounded-circle bg-soft-info">
                                                    <i class="ti ti-calendar-month  text-info font-size-40"></i>
                                                </span>
                                            </div>
                                            <h5 class="font-size-22" id="stat-pasar-input-hari-ini"></h5>
                                            <p class="text-muted">Pasar</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-12 mb-4">
                                <div class="card timbul h-100" data-aos="flip-up">
                                    <div class="card-body">
                                        <div class="text-center">
                                            <p class="font-size-16 fw-bold">Pasar Diinput Tahun Ini</p>
                                            <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                <span class="avatar-title rounded-circle bg-soft-info">
                                                    <i class="ti ti-calendar-event  text-info font-size-40"></i>
                                                </span>
                                            </div>
                                            <h5 class="font-size-22" id="stat-pasar-input-tahun-ini"></h5>
                                            <p class="text-muted">Pasar</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php if ($this->session->userdata('tipe_pengguna_id') != 2) { ?>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Pasar Laporan Lengkap</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-success">
                                                        <i class="ti ti-circle-check  text-success font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-pasar-lapor-lengkap"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Pasar Laporan Belum Lengkap</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-warning">
                                                        <i class="ti ti-hourglass-high  text-warning font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-pasar-lapor-belum-lengkap"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Pasar Belum Lapor</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-danger">
                                                        <i class="ti ti-circle-x  text-danger font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-pasar-belum-lapor"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (!in_array($this->session->userdata('tipe_pengguna_id'), [2, 8, 9, 18, 19, 13, 14])) { ?>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Tugas Pembantuan (TP)</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-heart-handshake  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="count-tp"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Dana Alokasi Khusus (DAK)</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-businessplan  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="count-dak"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Pasar Non TP/DAK</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-medical-cross-circle  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="count-nontpdak"></h5>
                                                <p class="text-muted">Pasar</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (!in_array($this->session->userdata('tipe_pengguna_id'), [2, 8, 9, 18, 19, 11, 13, 14])) { ?>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Pengelola Pasar Terdaftar</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-user-square-rounded  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-pengelola-terdaftar"></h5>
                                                <p class="text-muted">Pengelola</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Provinsi Sudah Terdaftar Pengelola</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-brand-asana  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-provinsi-pengelola-terdaftar"></h5>
                                                <p class="text-muted">Provinsi</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12 mb-4">
                                    <div class="card timbul h-100" data-aos="flip-up">
                                        <div class="card-body">
                                            <div class="text-center">
                                                <p class="font-size-16 fw-bold">Kota Sudah Terdaftar Pengelola</p>
                                                <div class="mini-stat-icon mx-auto mb-4 mt-3">
                                                    <span class="avatar-title rounded-circle bg-soft-info">
                                                        <i class="ti ti-building-skyscraper  text-info font-size-40"></i>
                                                    </span>
                                                </div>
                                                <h5 class="font-size-22" id="stat-kabkot-pengelola-terdaftar"></h5>
                                                <p class="text-muted">Kota</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->