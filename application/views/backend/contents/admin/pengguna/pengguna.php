<style>
    #table_filter {
        display: none;
    }
</style>

<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <button id="create-user" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="mb-3 row">
                                        <div class="col-12 col-md-4">
                                            <label for="tipe_pengguna" class="col-sm-12 col-form-label">Tipe Pengguna</label>
                                            <div class="input-group">
                                                <span class="input-group-text bg-transparent"><i class="ti ti-shield-checkered"></i></span>
                                                <select id="tipe_pengguna" name="tipe_pengguna" class="form-control">
                                                    <option value="">-- Pilih Tipe Pengguna --</option>

                                                    <?php foreach ($roles as $role) : ?>
                                                        <option value="<?= $role['tipe_pengguna'] ?>"><?= $role['tipe_pengguna'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <label for="name" class="col-sm-12 col-form-label">Pencarian</label>
                                            <div class="input-group">
                                                <span class="input-group-text bg-transparent"><i class="ti ti-search"></i></span>
                                                <input type="text" class="form-control" id="search_pengguna" placeholder="Pencarian Pengguna">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <label for="btn_filter">&nbsp</label>
                                            <br>
                                            <div class="input-group">
                                            <button type="button" onclick="filterPengguna()" id="btn_filter" class="btn btn-success"><i class="ti ti-search me-1"></i>Filter</button>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 class="header-title">Manajemen Data Pengguna</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data pengguna digitalisasi Pasar
                                    </p>

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
        <div class="modal fade staticBackdrop" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Form Pengguna</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form id="form-tambah">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pengguna_id" id="pengguna_id">

                        <div class="modal-body">
                            <div class="mb-3 row">
                                <div class="col">
                                    <label for="nip" class="col-sm-2 col-form-label">NIP</label>
                                    <div class="input-group mb-2 me-sm-2">
                                        <input class="form-control number" type="text" id="nip" name="nip" autocomplete="off" placeholder="Masukkan NIP">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-warning" type="button" onclick="searchNIP();">
                                            <i class="ti ti-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <small id="loader-nip" style="display: none; font-style: italic ">Sedang memuat...</small>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-lg-6">
                                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                    <input class="form-control number" type="text" id="name" name="name" autocomplete="off" placeholder="Masukkan Nama Lengkap">
                                </div>
                                <div class="col-lg-6">
                                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                                    <div class="input-group mb-3">
                                    <input class="form-control" type="password" id="password" name="password" autocomplete="off" placeholder="Password">
                                            <div class="input-group-append">
                                                <button class="btn  btn-info" id="refresh_pass" type="button"><i class="ti ti-refresh"></i></button>
                                            </div>
                                        </div>
                                    
                                </div>

                                <div class="col-lg-6">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <input class="form-control" type="text" id="email" name="email" autocomplete="off" placeholder="Masukkan Email">
                                </div>

                                <div class="col-lg-6">
                                    <label for="hp" class="col-sm-2 col-form-label">No. HP</label>
                                    <input class="form-control" type="text" id="hp" name="hp" autocomplete="off" placeholder="Masukkan No. HP">
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-lg-6">
                                    <label for="role" class="col-sm-2 col-form-label">Role</label>
                                    <select id="role" name="role" class="form-control">
                                        <option value="">-- Pilih Role --</option>

                                        <?php foreach ($roles as $role) : ?>
                                            <option value="<?= $role['tipe_pengguna'] ?>"><?= $role['tipe_pengguna'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label for="instansi" class="col-sm-2 col-form-label">Instansi</label>
                                    <input class="form-control" type="text" id="instansi" name="instansi" autocomplete="off" placeholder="Masukkan Instansi">
                                </div>
                            </div>

                            <div class="mb-3 row">
                                <div class="col-lg-6">
                                    <label for="daerah_id" class="col-sm-2 col-form-label">Provinsi</label>
                                    <select id="daerah_id" name="daerah_id" class="form-control">
                                        <option value="">-- Pilih Provinsi --</option>

                                        <?php foreach ($provinces as $k=>$v) : ?>
                                            <option value="<?= $v['daerah_id'] ?>"><?= $v['provinsi'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label for="role_lainnya" class="col-sm-12 col-form-label">Role Lainnya</label>
                                    <select id="role_lainnya" name="role_lainnya[]" class="form-control" multiple>
                                        <option value="">-- Pilih Role Lainnya --</option>
                                        <?php 
                                        foreach ($roles as $role) : ?>
                                            <option value="<?= $role['tipe_pengguna_id'] ?>"><?= $role['tipe_pengguna'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                          
                        </div>
                        <div class="modal-footer">
                            <button style="width: 100%" type="submit" id="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->