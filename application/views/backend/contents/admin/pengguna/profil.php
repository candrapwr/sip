<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Profil Saya</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Pengguna</a></li>
                            <li class="breadcrumb-item active">Profil Saya</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">

        <div class="page-content-wrapper">

            <div class="row">
                <div class="col-12">
                    <!-- Left sidebar -->
                    <div class="email-leftbar card">
                        <div class="user-sidebar text-center">
                            <div class="dropdown">
                                <div class="user-img">
                                    <img class="rounded-circle" src="https://ui-avatars.com/api/?name=<?= $this->session->userdata('nama') ?> &background=E70A2B&color=fff&size=512" alt="Header Avatar <?= $this->session->userdata('nama') ?>">
                                    <span class="avatar-online bg-success"></span>
                                </div>
                                <div class="user-info">
                                    <h5 class="mt-3 font-size-16 text-white"><?= $this->session->userdata('nama') ?></h5>
                                    <span class="font-size-13 text-white-50"><?= $this->session->userdata('role') ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="mail-list mt-4">
                            <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <button class="nav-link active" id="v-pills-identitas-tab" data-bs-toggle="pill" data-bs-target="#identitas" type="button" role="tab" aria-controls="v-pills-identitas" aria-selected="true"><i class="ti ti-user-circle me-1"></i>Identitas</button>
                                <button class="nav-link" id="v-pills-password-tab" data-bs-toggle="pill" data-bs-target="#password" type="button" role="tab" aria-controls="v-pills-password" aria-selected="false"><i class="ti ti-lock-open me-"></i>Ganti Password</button>
                            </div>
                        </div>
                    </div>
                    <!-- End Left sidebar -->

                    <!-- Right Sidebar -->
                    <div class="email-rightbar mb-3">
                        <div class="card timbul">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="identitas" role="tabpanel" aria-labelledby="v-pills-identitas-tab" tabindex="0">
                                    <div class="card-header">
                                        <h4 class="header-title">Identitas</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive" data-aos="fade-up">
                                            <table class="table mb-0">
                                                <tbody>
                                                    <tr>
                                                        <th scope="row">Tipe Pengguna</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><span class="badge rounded-pill bg-success"><?= $pengguna[0]['tipe_pengguna'] ?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Nama Lengkap</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['nama_lengkap'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">NIP (Nomor Induk Pegawai)</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['nip'] == null ? '-' : $pengguna[0]['nip'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">NIK (Nomor Induk Kependudukan)</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['nik'] == null ? '-' : $pengguna[0]['nik'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Email</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['email'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">No. Hanphone</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['no_hp'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Nama Dinas</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['nama_dinas'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Instansi</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['instansi'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Alamat</th>
                                                        <td style="width: 3px;">:</td>
                                                        <td><?= $pengguna[0]['alamat'] ?>, <?= $pengguna[0]['kelurahan'] ?>, <?= $pengguna[0]['kecamatan'] ?>, <?= $pengguna[0]['kab_kota'] ?>, <?= $pengguna[0]['provinsi'] ?></td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="password" role="tabpanel" aria-labelledby="v-pills-password-tab" tabindex="0">
                                    <div class="card-header">
                                        <h4 class="header-title">Ganti Password</h4>
                                    </div>
                                    <div class="card-body">
                                        <form action="<?=base_url()?>profil/save_password" method="POST" class="needs-validation" novalidate>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="mb-3 position-relative">
                                                        <label for="password" class="form-label">Password Baru</label>
                                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password Baru" required>
                                                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                                        <div class="invalid-tooltip">
                                                            Password tidak boleh kosong.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="mb-3 position-relative">
                                                        <label for="password" class="form-label">Konfirmasi Password Baru</label>
                                                        <input type="password" class="form-control" name="konfirmasi_password" id="konfirmasi_password" placeholder="Konfirmasi Password Baru" required>
                                                        <div class="invalid-tooltip">
                                                            Konfirmasi password tidak boleh kosong.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button class="btn btn-primary" type="submit">Ganti Password</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>

                        </div> <!-- card -->
                    </div> <!-- end Col-9 -->
                </div>
            </div><!-- End row -->
        </div>
    </div> <!-- container-fluid -->
</div>