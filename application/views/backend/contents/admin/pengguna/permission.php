<?php $this->load->view('backend/css/admin/permission_css'); ?>

<div class="page-content" id="menu_web">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <button onclick="add()" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#home1" role="tab" onclick="tab_menu('Front end')">
                                                <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                <span class="d-none d-sm-block">Halaman Utama</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#profile1" role="tab" onclick="tab_menu('Back end')">
                                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                <span class="d-none d-sm-block">Backend</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#profile2" role="tab" onclick="tab_menu('Mobile')">
                                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                <span class="d-none d-sm-block">Mobile</span>
                                            </a>
                                        </li>

                                    </ul>

                                    <div class="tab-content p-3 text-muted">
                                        <div class="tab-pane active" id="profile1" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="card timbul">
                                                        <div class="card-header">
                                                            <h3 class="card-title font-size-16 mt-0">Form Menu</h3>
                                                        </div>
                                                        <div class="card-body">
                                                            <form id="form_menu" enctype="multipart/form-data" class="needs-validation default-form" novalidate>
                                                                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                                                <input type="hidden" name="menu_id" id="menu_id" value="">
                                                                <input type="hidden" name="urutan" id="urutan" value="999">

                                                                <div class="modal-body row">
                                                                    <div class="col-lg-12 mb-3">
                                                                        <label for="nama_menu" class="form-label">Nama Menu</label>
                                                                        <input type="text" class="form-control" id="nama_menu" placeholder="Nama Menu" name="nama_menu" required>
                                                                        <div class="valid-feedback">
                                                                            Nama menu tidak boleh kosong
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-3">
                                                                        <label for="jenis" class="form-label">Jenis</label>
                                                                        <input type="text" class="form-control" id="jenis" placeholder="Jenis" name="jenis" required readonly>
                                                                        <div class="valid-feedback">
                                                                            Jenis tidak boleh kosong
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-3">
                                                                        <label for="controller" class="form-label">Controller/Url</label>
                                                                        <input type="text" class="form-control" id="controller" placeholder="Controller" name="controller" required>
                                                                        <div class="valid-feedback">
                                                                            Controller tidak boleh kosong
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 mb-3">
                                                                        <label for="publik" class="form-label">Publik</label>
                                                                        <select class="form-select" id="publik" name="publik" required>
                                                                            <option value="">- Pilih Publik -</option>
                                                                            <option value="Y">Ya</option>
                                                                            <option value="N">Tidak</option>
                                                                        </select>
                                                                        <div class="valid-feedback">
                                                                            Publik tidak boleh kosong
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-12 mb-3">
                                                                        <label for="icon" class="form-label">Icon</label>
                                                                        <input type="text" class="form-control" id="icon" placeholder="Icon" name="icon" required>
                                                                        <div class="valid-feedback">
                                                                            Icon tidak boleh kosong
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" onclick="reset()" class="btn btn-danger">Reset</button>
                                                                    <button type="submit" class="btn btn-primary">Simpan Data</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8">
                                                    <h3 class="card-title font-size-16 mt-0">Manajemen Menu</h3>
                                                    <div class="pt-4" id="list-menu">
                                                        <div class="dd" id="nestable">
                                                            <ol class="dd-list">
                                                            </ol>
                                                        </div>
                                                        <center>
                                                            <button type="button" onclick="update_urutan()" class="btn btn-primary mt-4">Simpan Urutan</button>
                                                        </center>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->