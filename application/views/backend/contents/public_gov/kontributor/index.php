<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Perdagangan - Manajemen Data Kontributor</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Perdagangan - Manajemen Data Kontributor</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row gy-3">
                                        <div class="col-md-12">
                                            <span class="mb-0">Status</span>
                                            <select class="mb-3" id="post_status" name="post_status" aria-label="Default select example">
                                                <option value=" ">Semua Data</option>
                                                <option value="Aktif">Aktif</option>
                                                <option value="Menunggu Verifikasi">Menunggu Verifikasi</option>
                                                <option value="Belum Verifikasi">Belum Aktivasi</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade staticBackdrop" id="tambahpengguna" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Assign Kontributor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-data-assign">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <input type="hidden" name="pengguna_id" id="pengguna_id">
                <input type="hidden" name="email" id="email">

                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="tipe_pengguna" class="col-sm-2 col-form-label">Pilih Pasar</label>
                        <select id="assign_pasar" name="assign_pasar[]" required multiple="multiple">
                            <option selected value="">Pilih Pasar</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="width: 100%" type="submit" id="submit" class="btn btn-primary">Assign</button>
                </div>
            </form>
        </div>
    </div>
</div>