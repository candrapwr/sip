<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Harga Barang Pokok</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Harga Barang Pokok</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <form>
                                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                        <div class="row">
                                            <?php if (strtolower($this->session->userdata('role')) != 'pengelola/petugas' && strtolower($this->session->userdata('role')) != 'dinas kabupaten/kota') : ?>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="filter-bahan-pokok-provinsi" class="form-label">Provinsi</label>
                                                        <select class="mb-3" aria-label="Default select example" id="filter-bahan-pokok-provinsi">
                                                            <option value="1|Nasional">Nasional</option>

                                                            <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                                                <option value="<?= $provinsi['daerah_id'] ?>|<?= $provinsi['provinsi'] ?>"><?= $provinsi['provinsi'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="filter-bahan-pokok-kabupaten" class="form-label">Kabupaten/Kota</label>
                                                        <select class="mb-3" aria-label="Default select example" id="filter-bahan-pokok-kabupaten">
                                                            <option value="">Kabupaten/Kota</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="filter-bahan-pokok-pasar" class="form-label">Pasar</label>
                                                        <select class="mb-3" aria-label="Default select example" id="filter-bahan-pokok-pasar">
                                                            <option value="">Pasar</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="filter-bahan-pokok-provinsi" class="form-label">Provinsi</label>
                                                        <select class="mb-3" aria-label="Default select example" id="filter-bahan-pokok-provinsi">
                                                            <option value="1|Nasional">Nasional</option>
                                                            <option value="<?= substr($this->session->userdata('daerah_id'), 0, 2) ?>|<?= $provinsi ?>"><?= $provinsi ?></option>
                                                            <option value="<?= substr($this->session->userdata('daerah_id'), 0, 4) ?>|<?= $kab ?>"><?= $kab ?></option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="filter-bahan-pokok-pasar" class="form-label">Pasar</label>
                                                        <select class="mb-3" aria-label="Default select example" id="filter-bahan-pokok-pasar">
                                                            <option value="">Pasar</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>


                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Tanggal Harga</label>
                                                    <input type="date" style="height: 35px; font-size: 14px" value="<?= $val_date2 ?>" class="form-control mb-3" id="filter-bahan-pokok-date2">
                                                </div>
                                            </div>

                                            <div class="col-md-14">
                                                <div class="form-group">
                                                    <label for="" class="form-label">Tanggal Perbandingan Harga</label>
                                                    <input type="date" style="height: 35px; font-size: 14px" value="<?= $val_date1 ?>" class="form-control" id="filter-bahan-pokok-date1">
                                                </div>
                                            </div>

                                            <div class="col-md-4">

                                                <div class="d-grid gap-2">
                                                    <button type="button" id="search-bahan-pokok" class="btn btn-primary btn-filter">
                                                        <i class="ti ti-search me-1"></i>Filter Data
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <th>Komoditas</th>
                                                <th>Satuan</th>
                                                <th id="date1" class="text-center"><?= $date1 ?></th>
                                                <th id="date2" class="text-center"><?= $date2 ?></th>
                                                <th>(%)</th>
                                                <th>Ket</th>
                                            </thead>

                                            <tbody id="harga-bahan-pokok"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </div>
    </div>
</div>