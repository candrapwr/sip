<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Perdagangan - Manajemen Data Rekap Absensi</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Perdagangan - Manajemen Data Rekap Absensi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="mb-3" aria-label="Default select example" id="filter-province">
                                                <option value="">Provinsi</option>
                                                <?php foreach ($provinces as $province) : ?>
                                                    <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="mb-3" aria-label="Default select example" id="filter-city">
                                                <option value="">Kabupaten/Kota</option>

                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="mb-3" aria-label="Default select example" id="filter-name">
                                                <option value="">Nama</option>
                                                <?php foreach ($kontributor as $k) : ?>
                                                    <option value="<?= $k['pengguna_id'] ?>|<?= $k['email'] ?>"><?= $k['nama_lengkap'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" value="<?= $start_date ?>" id="start-date">
                                        </div>
                                        <div class="col-md-4">
                                            <input type="date" class="form-control" value="<?= $end_date ?>" id="end-date">
                                        </div>
                                        <div class="col-md-4">
                                            <div class="d-grid gap-2">
                                                <button type="button" id="filter-search" class="btn btn-primary btn-filter">
                                                    <i class="ti ti-search me-1"></i>Filter Data
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </div>
    </div>
</div>