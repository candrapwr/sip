<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Manajemen Data Rekap Absensi</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Manajemen Data Rekap Absensi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                                        <div class="col-3">
                                            <select class="mb-3" aria-label="Default select example" id="filter-name">
                                                <option value="">Nama</option>

                                                <?php foreach ($kontributor as $k) : ?>
                                                    <option value="<?= $k['pengguna_id'] ?>|<?= $k['email'] ?>"><?= $k['nama_lengkap'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <select class="mb-3" aria-label="Default select example" id="filter-month">
                                                <option value="">Bulan</option>

                                                <option value="1" <?= (date('m') == 1) ? 'selected' : '' ?>>Januari</option>
                                                <option value="2" <?= (date('m') == 2) ? 'selected' : '' ?>>Februari</option>
                                                <option value="3" <?= (date('m') == 3) ? 'selected' : '' ?>>Maret</option>
                                                <option value="4" <?= (date('m') == 4) ? 'selected' : '' ?>>April</option>
                                                <option value="5" <?= (date('m') == 5) ? 'selected' : '' ?>>Mei</option>
                                                <option value="6" <?= (date('m') == 6) ? 'selected' : '' ?>>Juni</option>
                                                <option value="7" <?= (date('m') == 7) ? 'selected' : '' ?>>Juli</option>
                                                <option value="8" <?= (date('m') == 8) ? 'selected' : '' ?>>Agustus</option>
                                                <option value="9" <?= (date('m') == 9) ? 'selected' : '' ?>>September</option>
                                                <option value="10" <?= (date('m') == 10) ? 'selected' : '' ?>>Oktober</option>
                                                <option value="11" <?= (date('m') == 11) ? 'selected' : '' ?>>November</option>
                                                <option value="12" <?= (date('m') == 12) ? 'selected' : '' ?>>Desember</option>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <select class="mb-3" aria-label="Default select example" id="filter-year">
                                                <option value="">Tahun</option>

                                                <?php foreach ($years as $year) : ?>
                                                    <option value="<?= $year ?>" <?= (date('Y') == $year) ? 'selected' : '' ?>><?= $year ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="col-3">
                                            <div class="d-grid gap-2">
                                                <button type="button" id="filter-search" class="btn btn-primary btn-filter">
                                                    <i class="ti ti-search me-1"></i>Filter Data
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row -->
            </div>
        </div>
    </div>
</div>