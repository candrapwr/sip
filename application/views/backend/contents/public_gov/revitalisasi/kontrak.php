<style>
    .dropdown-menu {
        min-width: 10px;
    }

    .dropdown-item {
        padding-left: 10px;
        background-color: aliceblue;
    }

    .dropdown-menu.show {
        display: contents;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-8 col-12">
                                            <?php
                                            if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1) {
                                                // echo ' <button onclick="add()" class="btn btn-success"><i class="ti ti-plus me-1"></i>Buat Kontrak</button>';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-4 col-12 " style="text-align:right">
                                            <a href="<?= base_url() ?>web/revitalisasi/proposal" class="btn btn-dark text-right"><i class="ti ti-arrow-back me-1"></i>Kembali</a>
                                        </div>
                                    </div>


                                </div>
                                <div class="card-body">

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_general" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah / Edit <b id="title_md"></b></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form method="POST" action="<?= base_url() ?>web/revitalisasi/kontrak" id="form_general" class="needs-validation" enctype="multipart/form-data">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="proposal_lelang_id" id="proposal_lelang_id" value="">
                    <input type="hidden" name="proposal_kontrak_id" id="proposal_kontrak_id" value="">
                    <input type="hidden" name="uuid" value="<?= $uuid ?>">
                   
                    <div class="row">
                        <div class="col-md-6 col-12">
                        <div class="form-group mb-3">
                                <label class="form-label">Tanggal Awal</label>
                                <input type="date" class="form-control" id="tgl_awal" name="tgl_awal" placeholder="Masukkan tgl awal kontrak" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Tanggal Akhir</label>
                                <input type="date" class="form-control" id="tgl_akhir" name="tgl_akhir" placeholder="Masukkan tgl akhir kontrak" required>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mb-3">
                        <label class="form-label">Pemenang</label>
                        <input type="text" class="form-control" id="pemenang" name="pemenang" placeholder="Masukkan pemenang kontrak" required>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Nomor Kontrak</label>
                                <input type="text"  class="form-control" id="no_kontrak" name="no_kontrak" placeholder="Masukkan no kontrak" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Nilai Kontrak</label>
                                <input type="number" step="any" class="form-control" id="nilai" name="nilai" placeholder="Masukkan nilai kontrak" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Tanggal DIPA</label>
                                <input type="date"  class="form-control" id="tgl_dipa" name="tgl_dipa" placeholder="Masukkan tanggal DIPA" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Nomor DIPA</label>
                                <input type="number" step="any" class="form-control" id="no_dipa" name="no_dipa" placeholder="Masukkan no DIPA" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">Dokumen Kontrak (PDF)</label>
                        <div id="last_file"></div>
                        <input type="file" class="form-control" id="dok_kontrak" accept="application/pdf" name="dok_kontrak" placeholder="Masukkan dokumen kontrak" required>
                    </div>
                    <div class="form-group mb-3" style="display: none;">
                        <label class="form-label">Dokumen Jaminan Pelaksanaan (PDF)</label>
                        <div id="last_file_jaminan"></div>
                        <input type="file" class="form-control" id="dok_jaminan" accept="application/pdf" name="dok_jaminan" placeholder="Masukkan dokumen jaminan pelaksanaan" required>
                    </div>

                   
                    <hr>
                        <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                        </div>
                    </div>

                </form>

            </div>




        </div>
    </div>
</div>

