<style>
    .dropdown-menu {
        min-width: 10px;
    }

    .dropdown-item {
        padding-left: 10px;
        background-color: aliceblue;
    }

    .dropdown-menu.show {
        display: contents;
    }

    .table-sticky th:last-child,
    td:last-child {
        position: sticky;
        right: 0px;
        color: white;
        vertical-align: middle;
        text-align: center;

    }

    .table-sticky th:last-child,
    td:last-child {
        background-color: #001d59;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                           
                                        </div>
                                        <div class="col-md-6 col-12 " style="text-align:right">
                                        <?php
                                            if (((($menu_id != '')) ? has_access($menu_id, 'update') : false)  == 1 &&  is_verifikator1_k5()  == 1 && ($proposal_jadwal['status'] == 'Perlu Verifikasi' ||$proposal_jadwal['status'] == 'Revisi') ) {
                                                 echo ' <button onclick="verifyJadwal()" class="btn btn-success"><i class="ti ti-search me-1"></i>Verifikasi</button>';
                                            }

                                            if (((($menu_id != '')) ? has_access($menu_id, 'update') : false)  == 1 &&  is_verifikator2_k5()  == 1 && ($proposal_jadwal['status'] == 'Disetujui Staff K5') ) {
                                                echo ' <button onclick="verifyJadwal()" class="btn btn-success"><i class="ti ti-search me-1"></i>Verifikasi</button>';
                                           }
                                            ?>
                                            <a href="<?= base_url() ?>web/revitalisasi/proposal" class="btn btn-dark text-right"><i class="ti ti-arrow-back me-1"></i>Kembali</a>
                                        </div>
                                    </div>


                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="form-group mb-3">
                                                <label>Status Input</label><br>
                                                <b><div class="badge bg-<?=((isset($proposal_jadwal['status'])) && $proposal_jadwal['status'] == 'Revisi'?'danger':(($proposal_jadwal['status'] == 'Disetujui')?'success':'warning'))?>">
                                                <?php
                                                 if(isset($proposal_jadwal['status'])){

                                                    $status = $proposal_jadwal['status'];
                                                    if($proposal_jadwal['status'] == 'Perlu Verifikasi'){
                                                        $status = 'Menunggu Verifikasi Verifikator 1 - K5';
                                                    }
                                                    if($proposal_jadwal['status'] == 'Disetujui Staff K5'){
                                                        $status = 'Menunggu Verifikasi Verifikator 2 - K5';
                                                    }

                                                    echo '<i class="ti ti-refresh"></i>'.$status;
                                                }else{
                                                    echo 'Baru';
                                                }
                                                  ?></div>
                                                  
                                                </b>
                                                <br>
                                                <?=((isset($proposal_jadwal['status'])) && $proposal_jadwal['status'] == 'Revisi'?'<i class="text-danger">'.$proposal_jadwal['keterangan'].'</i>':'')?>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Proposal</label><br>
                                                <b><?= $proposal['judul'] ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Kode Lelang</label><br>
                                                <b><?= $kontrak['kode_lelang'] ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Pemenang</label><br>
                                                <b><?= $kontrak['pemenang'] ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Pelaksanaan</label><br>
                                                <b><?= date('d M Y', strtotime($kontrak['tgl_awal'])) . ' - ' . date('d M Y', strtotime($kontrak['tgl_akhir'])) ?></b>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="form-group mb-3">
                                                <label>Tanggal Input Jadwal</label><br>
                                                <b><?= (isset($proposal_jadwal['createdon'])?(!empty($proposal_jadwal['updatedon'])?$proposal_jadwal['updatedon']:$proposal_jadwal['createdon']):'-') ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Pasar</label><br>
                                                <b><?= $proposal['nama_pasar'] ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Nomor Kontrak</label><br>
                                                <b><?= $kontrak['no_kontrak'] ?></b>
                                            </div>
                                            <div class="form-group mb-3">
                                                <label>Nilai Project</label><br>
                                                <b>Rp. <?= number_format($kontrak['nilai']) ?></b>
                                            </div>
                                            <?php
                                              $startDate = new DateTime($kontrak['tgl_awal']);
                                              $endDate = new DateTime($kontrak['tgl_akhir']);

                                              $interval = new DateInterval('P1W'); // 1 week interval
                                              $date = clone $startDate;
                                              $weekArray = [];

                                              while ($date <= $endDate) {
                                                  $weekEndDate = clone $date;
                                                  $weekEndDate->add($interval)->sub(new DateInterval('P1D'));

                                                  $weekArray[] = $date->format('d/m/y') . ' - ' . $weekEndDate->format('d/m/y');

                                                  $date->add($interval);
                                              }
                                            ?>
                                            <div class="form-group mb-3">
                                                <label>Lama Pengerjaan</label><br>
                                                <b><?= count($weekArray) ?> Minggu</b>
                                            </div>
                                        </div>
                                    </div>
                                    <hr> <form method="POST" id="frm_kurva" action="<?= base_url() ?>web/revitalisasi/jadwal" id="form_general" class="needs-validation" enctype="multipart/form-data">
                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                    <input type="hidden" name="proposal_jadwal_id" value="<?= $proposal_jadwal['proposal_jadwal_id'] ?>">
                                    <input type="hidden" name="uuid" value="<?= $uuid ?>">
                                    <input type="hidden" name="proposal_id" value="<?= $proposal['proposal_id'] ?>">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-sticky" style="width: 100%;table-layout:fixed;">
                                            <thead>
                                                <tr class="fw-bold bg-primary text-white text-center" style="font-size:12px; vertical-align:middle;">
                                                    <th class="py-0" width="50">No</th>
                                                    <th class="py-0" width="300">Uraian Kegiatan</th>
                                                    <th class="py-0" width="130">Bobot</th>
                                                    <th class="py-0" width="200">Biaya</th>

                                                    <?php
                                                  

                                                    $counter = 1;
                                                    foreach ($weekArray as $kk=>$vv) { ?>
                                                        <th class="py-0" width="130">
                                                            <span>Minggu ke <?= $counter ?> (%)<br><i style="font-size:10px"><?= $vv ?></i></span>
                                                        </th>
                                                    <?php
                                                        $counter++;
                                                    }
                                                    ?>
                                                    <th class="py-0" width="70">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach ($ms_jadwal as $k => $v) { ?>
                                                 <input type="hidden" class="form-control" value="<?=$kurva_id[$v['master_jadwal_id']]?>" name="kurva_id[<?=$v['master_jadwal_id']?>]">
                                                    <tr style="vertical-align:middle;">
                                                        <td><?= ($k + 1) ?></td>
                                                        <td class="fw-bold"><?= $v['kegiatan'] ?></td>
                                                        <td><input type="number" class="form-control" data-key="<?=$k?>" onkeyup="total(this)" name="bobot[<?=$v['master_jadwal_id']?>]" value="<?=$kurva_bobot[$v['master_jadwal_id']]?>" <?= $status == 'Disetujui' ? 'readonly':'' ?>></td>
                                                        <td><input type="number" class="form-control" data-key="<?=$k?>" onkeyup="total(this)" value="<?=$kurva_biaya[$v['master_jadwal_id']]?>" name="biaya[<?=$v['master_jadwal_id']?>]" <?= $status == 'Disetujui' ? 'readonly':'' ?>></td>
                                                       
                                                        <?php
                                                        foreach ($weekArray as $kk=>$vv) { ?>
                                                            <td><input type="number" class="form-control" data-key="<?=$kk?>" onkeyup="total(this)" value="<?=$kurva_pengerjaan[$v['master_jadwal_id']][$kk]?>" name="week[<?=$v['master_jadwal_id']?>][]" <?= $status == 'Disetujui' ? 'readonly':'' ?>>
                                                            
                                                        </td>
                                                        <?php
                                                        }
                                                        ?>
                                                        <td id="h_total_<?=($k+1)?>"></td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>

                                                <tr class="fw-bold bg-primary text-white text-center" style="font-size:12px; vertical-align:middle;">
                                                    <th class="py-0" width="300" colspan="2">Total (%)</th>
                                                    <th class="py-0" width="100" id="bobot_total">0</th>
                                                    <th class="py-0" width="100" id="biaya_total">0</th>
                                                    <?php
                                                        foreach ($weekArray as $k=>$v) { ?>
                                                            <td id="total_<?=$k?>">0</td>
                                                        <?php
                                                        }
                                                        ?>
                                                   <th class="py-0" width="100" id="total_all">0</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                            
                                    <div class="form-group">
                                        <label>Dokumen Jadwal Kurva (PDF)</label>
                                        <?php
                                        if(!empty($proposal_jadwal['dok_kurva'])){
                                            echo '<br><a href="'.base_url().'download/revitalisasi/proposal/jadwal/'.$proposal_jadwal['dok_kurva'].'" class="btn btn-primary mb-2"><i class="ti ti-file-type-pdf"></i> Dokumen Kurva</a>';
                                        }
                                        ?>
                                        <?php if($status != 'Disetujui'): ?>
                                        <input class="form-control" name="dok_kurva" id="dok_kurva" type="file" <?=(!empty($proposal_jadwal['dok_kurva']))?'':'required'?>>
                                        <?php endif;?>
                                    </div>
                                    
                                    <?php if($status != 'Disetujui'): ?>
                                    <div class="form-group mt-3 text-center">
                                        <button class="btn btn-warning" type="submit" name="submit_draft" value="Y"><i class="ti ti-list me-1"></i>Draft</button>
                                        <button class="btn btn-success" type="submit" name="submit_draft" value="N"><i class="ti ti-device-floppy me-1"></i>Simpan dan Submit</button>
                                    </div>
                                    <?php endif;?>
                                    </form>
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>
