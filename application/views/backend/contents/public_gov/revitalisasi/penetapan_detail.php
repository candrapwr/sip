<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<style>
    /* Custom CSS for larger checkbox */
    .checkbox-lg input {
        width: 1.5rem;
        height: 1.5rem;
    }

    .checkbox-lg::before {
        width: 1.5rem;
        height: 1.5rem;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-md-12">
                    <div class="card timbul">
                        <div class="card-body">
                            <div class="float-end">
                                <?php
                                if (is_k1_ketuatim() == 1) { ?>
                                    <?php if ($penetapan['status'] != 'Ditetapkan') { ?>
                                        <button onclick="tetapkanProposal(true)" class="btn btn-success me-1"><i class="ti ti-circle-check me-1"></i>Tetapkan</button>
                                    <?php } else { ?>
                                        <button onclick="tetapkanProposal(false)" class="btn btn-primary"><i class="ti ti-x me-1"></i>Batalkan Ketetapan</button>
                                    <?php } ?>
                                <?php } ?>
                                <a href="<?= base_url() ?>web/revitalisasi/penetapan" class="btn btn-outline-primary"><i class="ti ti-arrow-back me-1"></i>Kembali</a>
                            </div>
                            <h4 class="header-title">Penetapan</h4>
                            <p class="card-title-desc">Berikut merupakan progres dari proposal revitalisasi</p>
                            <div class="row gy-3">
                                <div class="col-md-4 col-12">
                                    <div class="form-group">
                                        <label class="text-muted">Tahun Anggaran</label><br>
                                        <label><?= $penetapan['tahun_anggaran'] ?></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="form-group">
                                        <label class="text-muted">Tanggal</label><br>
                                        <label><?= $penetapan['tgl'] ?></label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="form-group">
                                        <label class="text-muted">No Permendag</label><br>
                                        <label><?= $penetapan['no_permendag'] ?></label>
                                    </div>
                                </div>
                                <div class="col-md-12 col-12">
                                    <div class="form-group">
                                        <label class="text-muted">Deskripsi</label><br>
                                        <label><?= $penetapan['keterangan'] ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card timbul">
                        <div class="card-body">
                            <h4 class="header-title">Tabel Data Penetapan</h4>
                            <div class="row justify-content-center gy-3">
                                <div class="col-12 col-lg-3">
                                    <label>Provinsi</label>
                                    <select class="mb-3" aria-label="Default select example" id="provinsi-name">
                                        <option value="">-- Pilih Provinsi --</option>
                                    </select>
                                </div>
                                <div class="col-12 col-lg-3">
                                    <label>Kabupaten / Kota</label>
                                    <select class="mb-3" aria-label="Default select example" id="kabkota-name">
                                        <option value="">-- Pilih Kabupaten/Kota --</option>
                                    </select>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <label>Pasar</label>
                                    <select id="f_pasar_id">
                                    </select>
                                </div>
                                <div class="col-12 col-lg-2">
                                    <label>Status</label>
                                    <select class="form-control" id="f_status">
                                        <option value="">Filter Status </option>
                                        <option value="Belum Ditetapkan">Belum Ditetapkan</option>
                                        <option value="Ditetapkan">Ditetapkan</option>
                                    </select>
                                </div>
                                <div class=" col-12 col-md-12">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>