<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<style>
    .progressbar {
        margin-bottom: 30px;
        overflow: hidden;
    }

    .wizard>.content>.body {
        width: 100%;
        position: inherit;
    }

    .progressbar li {
        list-style-type: none;
        color: #99a2a8;
        font-size: 9px;
        width: calc(100%/4);
        float: left;
        position: relative;
        font: 500 13px/1 'Roboto', sans-serif;
    }

    #myForm {
        text-align: center;
    }

    #myForm .progressbar li::before {
        content: "\f375";
        font: normal normal normal 30px/50px Ionicons;
        line-height: 50px;
        width: 50px;
        height: 50px;
        line-height: 50px;
        display: block;
        background: #eaf0f4;
        border-radius: 50%;
        margin: 0 auto 10px;
        position: inherit;
        z-index: 2;
    }


    #myForm .progressbar li.active::before,
    #myForm .progressbar li.active::after {
        background: #5cb85c;
        color: #fff;
    }

    #myForm .progressbar li::after {
        content: '';
        width: 100%;
        height: 10px;
        background: #eaf0f4;
        position: absolute;
        left: -50%;
        top: 21px;
        z-index: 1;
    }
</style>

<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8 col-12">
                                            <h4 class="header-title">Progres Proposal Revitalisasi</h4>
                                            <p class="card-title-desc">Berikut merupakan progres dari proposal revitalisasi
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-12" style="text-align:right">

                                            <?php
                                            if (is_dinas() == 1 && $proposal['status'] == 'Diundang Review') {
                                                echo '<button onclick ="addPengelola()" class="btn btn-info"> <i class="fa fa-plus"></i> Usulan Pengelola Keuangan</button>';
                                            }
                                            if (is_k1_ketuatim() == 1) {
                                                if ($proposal['jml_dokumen_perlu_verify'] == 0) {
                                                    if ($proposal['status'] == 'Diajukan') {
                                                        echo '<button onclick ="verifyProposal(\'Undang Review / Ditolak\',\'Undang Review\',\'Tolak\',\'' . $proposal_id . '\',true)" class="btn btn-success"> <i class="ti ti-search me-1"></i>Undang Review / Tolak</button>';
                                                    }

                                                    if ($proposal['status'] == 'Diundang Review') {
                                                        echo '<button id="btn_v_review" onclick ="verifyProposal(\'Selesaikan Review / Tolak\',\'Selesai Review\',\'Tolak\',\'' . $proposal_id . '\',true)" class="btn btn-success"> <i class="ti ti-search me-1"></i>Selesaikan Review</button>';
                                                    } else if ($proposal['status'] == 'Selesai Review') {
                                                        // echo '<button onclick ="verifyProposal(\'Penetapan / Tolak\',\'Penetapan\',\'Tolak\',\'' . $proposal_id . '\',true)" class="btn btn-success"> <i class="ti ti-search me-1"></i>Lanjutkan Penetapan</button>';
                                                    }
                                                }
                                            }
                                            ?>
                                            <a href="<?= base_url() ?>web/revitalisasi/proposal" class="btn btn-outline-primary"><i class="ti ti-arrow-back me-1"></i>Kembali</a>
                                        </div>

                                    </div>

                                    <div id="myForm">
                                        <ul class="progressbar">
                                            <li class="<?= ((in_array($proposal['status'], ['Diajukan', 'Diundang Review', 'Selesai Review', 'Penetapan', 'Lelang', 'Pelaporan']) ? 'active' : '')) ?>">Diajukan</li>
                                            <li class="<?= ((in_array($proposal['status'], ['Diundang Review', 'Selesai Review', 'Penetapan', 'Lelang', 'Pelaporan']) ? 'active' : '')) ?>">Di Undang Review</li>
                                            <li class="<?= ((in_array($proposal['status'], ['Selesai Review', 'Penetapan', 'Lelang', 'Pelaporan']) ? 'active' : '')) ?>">Selesai Review</li>
                                            <li class="<?= ((in_array($proposal['status_penetapan'], ['Ditetapkan']) ? 'active' : '')) ?>">Penetapan</li>

                                        </ul>


                                    </div>
                                    <?php
                                    if ($proposal['status'] == 'Ditolak') {
                                    ?>
                                        <div class="alert alert-danger">
                                            <h4><i class="ti ti-alert-triangle me-1"></i> Proposal Ditolak</h4>
                                            <h6><?= $proposal['keterangan'] ?></h6>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div>
                    <ul class="nav nav-tabs" id="mainTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="proposal-tab" data-bs-toggle="tab" href="#proposal" role="tab" aria-controls="proposal" aria-selected="false">Surat Pengantar + Proposal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " id="dokumen-tab" data-bs-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="true"><i class="ti ti-file-analytics me-1"></i>Dokumen Awal</a>
                        </li>
                        <?php if (in_array($proposal['status'], ['Diundang Review', 'Selesai Review', 'Penetapan'])) { ?>
                            <li class="nav-item">
                                <a class="nav-link " id="sk_pengelola-tab" data-bs-toggle="tab" href="#sk_pengelola" role="tab" aria-controls="sk_pengelola" aria-selected="true">Usulan Pengelola Keuangan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " id="dokumen-lanjutan-tab" data-bs-toggle="tab" href="#dokumen_lanjutan" role="tab" aria-controls="dokumen_lanjutan" aria-selected="true">Dokumen Setelah Review</a>
                            </li>
                        <?php  } ?>
                    </ul>

                    <div class="tab-content" id="mainTabContent">

                        <div class="tab-pane p-3 active" id="proposal" role="tabpanel" aria-labelledby="proposal-tab">
                            <div class="row">
                                <div class="col-md-4 col-12">
                                    <div class="card timbul">
                                        <div class="card-header bg-dark text-white text-center">
                                            <h5 class="text-light mb-0"><?= $nama_pasar ?><br><small><?= $pasar_detail['tipe_pasar'] ?></small></h5>
                                            <p class="text-light mb-0"><?= $pasar['provinsi'] ?>, <?= $pasar['kab_kota'] ?><br><?= $pasar['alamat'] ?><br>Telp. <?= $pasar_detail['no_telp'] ?><br>Fax. <?= $pasar_detail['no_fax'] ?></p>
                                        </div>
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label class="text-muted mb-0">Komoditas yang dijual</label><br>
                                                <label><?= $pasar_detail['komoditas_dijual'] ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="text-muted mb-0">Waktu Operasional</label><br>
                                                <label><?= implode(', ', explode(',', $pasar_detail['waktu_operasional'])) ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="text-muted mb-0">Jam Operasional</label><br>
                                                <label><?= substr($pasar_detail['jam_operasional_awal'], 0, 5) . ' - ' . substr($pasar_detail['jam_operasional_akhir'], 0, 5) ?></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="text-muted mb-0">Jam Sibuk</label><br>
                                                <label><?= substr($pasar_detail['jam_sibuk_awal'], 0, 5) . ' - ' . substr($pasar_detail['jam_sibuk_akhir'], 0, 5) ?></label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="card timbul">
                                        <div class="card-body">
                                            <div class="row gy-3">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Pengusul</label><br>
                                                        <label><?= $proposal['pengusul'] ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Tingkat Pengusul</label><br>
                                                        <label><?= $proposal['tingkat_pengusul'] ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Kode Satker</label><br>
                                                        <label><?= $proposal['kode_satker'] ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Kode Lokasi</label><br>
                                                        <label><?= $proposal['kode_lokasi'] ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Kode KPPN</label><br>
                                                        <label><?= $proposal['kode_kppn'] ?></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="text-muted mb-0">Nama Dinas</label><br>
                                                        <label><?= $proposal['nama_dinas'] ?></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-12">
                                    <div class="card timbul">
                                        <div class="card-body">
                                            <h4 class="header-title text-primary"><?= $proposal['judul'] ?></h4>
                                            <table class="table">
                                                <tr>
                                                    <td>Tanggal</td>
                                                    <td class="fw-bold"><?= $proposal['tgl'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Anggaran</td>
                                                    <td class="fw-bold"><?= $proposal['tahun_anggaran'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Latar Belakang</td>
                                                    <td class="fw-bold"><?= $proposal['latar_belakang'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Maksud dan Tujuan</td>
                                                    <td class="fw-bold"><?= $proposal['maksud_tujuan'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Permohonan Anggaran</td>
                                                    <td class="fw-bold">Rp. <?= number_format($proposal['anggaran']) ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Pernah Dapat TP</td>
                                                    <td class="fw-bold"><?= (count($dana_tp) > 0 && $dana_tp['kode'] != '404') ? 'Ya, Tahun ' . date('Y', strtotime($dana_tp['data'][0]['createdon'])) . ', Anggaran : Rp. ' . number_format($dana_tp['data'][0]['anggaran']) : 'Tidak' ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Pernah Dapat DAK</td>
                                                    <td class="fw-bold"><?= (count($dana_dak) > 0 && $dana_dak['kode'] != '404') ? 'Ya, Tahun ' . date('Y', strtotime($dana_dak['data'][0]['createdon'])) . ', Anggaran : Rp. ' . number_format($dana_dak['data'][0]['anggaran']) : 'Tidak' ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Bangun</td>
                                                    <td class="fw-bold"><?= $pasar_detail['tahun_bangun'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun Renovasi</td>
                                                    <td class="fw-bold"><?= $pasar_detail['tahun_renovasi'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Luas Lahan</td>
                                                    <td class="fw-bold"><?= $pasar_detail['luas_tanah'] ?> m3</td>
                                                </tr>
                                                <tr>
                                                    <td>Luas Bangunan</td>
                                                    <td class="fw-bold"><?= $pasar_detail['luas_bangunan'] ?> m3</td>
                                                </tr>
                                                <tr>
                                                    <td>Bentuk Bangunan</td>
                                                    <td class="fw-bold"><?= $pasar_detail['bentuk_pasar'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Kepemilikan</td>
                                                    <td class="fw-bold"><?= $pasar_detail['kepemilikan'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Kondisi</td>
                                                    <td class="fw-bold"><?= $pasar_detail['kondisi'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Tata Kelola</td>
                                                    <td class="fw-bold"><?= $pasar_detail['tata_kelola'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Pengelola</td>
                                                    <td class="fw-bold"><?= $pengelola_pasar['nama_pengelola'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Omzet Tahun Sebelumnya</td>
                                                    <td class="fw-bold">Rp. <?= number_format($pasar_detail['omzet_sebelumnya']) ?></td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end row -->


                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row">
                                        <?php
                                        foreach ($proposal_foto_pasar as $k => $v) {
                                            $status_color = 'warning';
                                            $icon = 'spinner';

                                            if ($v['status'] == 'Disetujui') {
                                                $status_color = 'success';
                                                $icon = 'check';
                                            }

                                            $keterangan = '';

                                            if ($v['status'] == 'Ditolak') {
                                                $status_color = 'danger';
                                                $icon = 'times';
                                                $keterangan = '<br><i class="text-danger"> ' . $v['file_keterangan'] . '</i>';
                                            }

                                            $button_verify = '';

                                            if ($v['status'] != 'Disetujui') {
                                                if(is_k1_pelaksana() == 1){
                                                    $button_verify = '<button class="btn btn-sm btn-success m-1" onclick="verifyProposalDokumen(' . $v['proposal_dokumen_id'] . ',true)"><i class="ti ti-search me-1"></i>Verifikasi</button>';
                                                }
                                            }
                                        ?>
                                            <div class="col-md-2 col-6 text-center" style="margin-left:30px">
                                                <img class="img-thumbnail" width="100%" src="<?= $v['file_mime'] . ' ' . $v['file_base64'] ?>"><br><br>
                                                <div class="badge bg-<?= $status_color ?> mt-1"><i class="ti ti-<?= $icon ?>"></i> <?= (($v['status'] == 'Perlu Verifikasi') ? 'Menunggu Di Verifikasi K1' : $v['status']) ?></div><br>
                                                <i class="text-muted"><?= $v['keterangan'] ?></i><br><br>
                                                <?= $button_verify ?>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row gy-3">
                                        <div class="col-md-12">
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="sk_pengelola" role="tabpanel" aria-labelledby="sk_pengelola-tab">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row gy-3">
                                        <div class="col-md-12">
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_sk_pengelola" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane p-3" id="dokumen_lanjutan" role="tabpanel" aria-labelledby="dokumen-lanjutan-tab">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row gy-3">
                                        <div class="col-md-12">
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_dokumen_lanjutan" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Proposal-->
<div class="modal fade" id="modal_tugaskan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="ti ti-edit me-1"></i>Tugaskan Staff</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>web/revitalisasi/proposal/dokumen/tugaskan" method="POST">
                    <input type="hidden" name="proposal_dokumen_id" id="t_proposal_dokumen_id">
                    <input type="hidden" name="dokumen_id" id="t_dokumen_id">
                    <input type="hidden" name="uuid" id="t_uuid" value="<?= $uuid ?>">
                    <input type="hidden" name="proposal_id" id="t_proposal_id" value="<?= $proposal_id ?>">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <div class="form-group">
                        <label>Pilih Staf</label>
                        <select name="staf_ditugaskan" id="t_staf_ditugaskan" required>
                            <option value="">Pilih Staf</option>
                            <?php
                            if (isset($stafk1)) {
                                foreach ($stafk1 as $k => $v) {
                                    echo '<option value="' . $v['email'] . '">' . $v['nama_lengkap'] . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success" style="float:right"><i class="ti ti-user me-1"></i>Tugaskan</button>
                </form>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_sk" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="mdi mdi-circle-edit-outline mr-2"></i>Tambah / Ubah Usulan Pengelola Keuangan </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>web/revitalisasi/proposal/pengelola" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="proposal_pengelola_id" id="proposal_pengelola_id">
                    <input type="hidden" name="uuid" id="t_uuid" value="<?= $uuid ?>">
                    <input type="hidden" name="proposal_id" id="t_proposal_id" value="<?= $proposal_id ?>">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <div class="form-group mb-3">
                        <label>Pilih Usulan Pengelola</label>
                        <select name="sk_operator" id="sk_operator" class="form-control" required>
                            <option value="">Pilih Usulan Pengelola</option>
                            <option value="KPA">KPA</option>
                            <option value="PPK">PPK</option>
                            <option value="SPM">SPM</option>
                            <option value="Bendahara">Bendahara</option>
                        </select>
                    </div>
                    <div class="form-group mb-3">
                        <label>NIP</label>
                        <input type="text" class="form-control" name="nip" id="nip" pattern="[0-9]*" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Jabatan</label>
                        <input type="text" class="form-control" name="jabatan" id="jabatan" required>
                    </div>
                    <div class="form-group mb-3">
                        <label>Dokumen Lampiran</label>
                        <input type="file" class="form-control" name="lampiran_usulan" id="lampiran_usulan"  required>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success" style="float:right"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </form>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_lihat" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="lihat_judul"><i class="ti ti-edit me-1"></i>Dokumen Unggahan </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="container_lihat">

            </div>
        </div>
    </div>
</div>