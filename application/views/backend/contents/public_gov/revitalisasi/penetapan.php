<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <?php
                                    if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1 ) {
                                        echo ' <button onclick="add()" class="btn btn-success"><i class="mdi mdi-plus"></i> Buat Penetapan</button>';
                                    }
                                    ?>
                                </div>
                                <div class="card-body">
                                    <?php
                                    if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1 && count($penetapan) == 0) { ?>
                                        <div class="float-end">
                                            <button onclick="add()" class="btn btn-primary"><i class="ti ti-plus me-1"></i>Buat Penetapan</button>
                                        </div>
                                    <?php } ?>
                                    <h4 class="header-title"><?= $title ?></h4>
                                    <p class="card-title-desc">Silahkan lihat, filter, dan kelola data <?= $title ?> di digitalisasi Pasar</p>
                                    <div class="row gy-3">
                                        <div class="col-12 col-md-6">
                                            <select class="form-select" id="f_tahun_anggaran" placeholder="Tahun Anggaran">
                                                <option value="">Tahun </option>
                                                <?php
                                                $range = range(2023, date('Y', strtotime('+1 year')));
                                                foreach ($range as $k => $v) {
                                                    $selected = '';
                                                    if ($v == date('Y', strtotime('+1 year'))) {
                                                        $selected = 'selected';
                                                    }
                                                    echo ' <option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-12 col-md-4">
                                            <select class="form-select" id="f_status" placeholder="Status">
                                                <option value="">Filter Status </option>
                                                <option value="Draft">Draft</option>
                                                <option value="Belum Ditetapkan">Belum Ditetapkan</option>
                                                <option value="Ditetapkan">Ditetapkan</option>

                                            </select>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <button class="btn btn-dark btn-block" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari Data</button>
                                            <button class="btn btn-light d-none" onclick="reset_form()" type="button"><i class="ti ti-x me-1"></i>Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row gy-3">
                                        <div class="col-md-12">
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Proposal-->
<div class="modal fade" id="modal_proposal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="ti ti-edit-circle me-1"></i>Tambah / Edit Proposal
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="proposal_form" method="POST" action="<?= base_url() ?>web/revitalisasi/penetapan">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="penetapan_id" id="penetapan_id" value="">
                    <div class="row mb-3">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control" id="tgl" name="tgl" min="<?= date('Y') . '-01-01' ?>" max="<?= date('Y-m-d') ?>" value="<?= date('Y-m-d') ?>" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label class="form-label">Tahun Anggaran</label>
                                <select class="form-control" name="tahun_anggaran" id="tahun_anggaran" required>
                                    <?php
                                    foreach ($range as $k => $v) {
                                        $selected = ($v == date('Y', strtotime('+1 year'))) ? 'selected' : '';
                                        echo '<option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">No Permendag / Referensi</label>
                        <input type="text" class="form-control" id="no_permendag" name="no_permendag" placeholder="Masukkan no permendag" required>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">Deskripsi</label>
                        <textarea class="form-control" id="deskripsi" name="deskripsi" required></textarea>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">
                                <i class="ti ti-device-floppy me-1"></i>Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>