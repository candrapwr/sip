<style>
    .dropdown-menu {
        min-width: 10px;
    }

    .dropdown-item {
        padding-left: 10px;
        background-color: aliceblue;
    }

    .dropdown-menu.show {
        display: contents;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-header">
                                    <div class="row">
                                        <div class="col-md-8 col-12">
                                            <?php
                                            if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1) {
                                                echo ' <button onclick="add()" class="btn btn-success"><i class="ti ti-plus me-1"></i>Buat Lelang</button>';
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-4 col-12 " style="text-align:right">
                                            <a href="<?= base_url() ?>web/revitalisasi/proposal" class="btn btn-dark text-right"><i class="ti ti-arrow-back me-1"></i>Kembali</a>
                                        </div>
                                    </div>


                                </div>
                                <div class="card-body">

                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_general" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah / Edit <?= $title ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form method="POST" action="<?= base_url() ?>web/revitalisasi/lelang" id="form_general" class="needs-validation" enctype="multipart/form-data">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="proposal_lelang_id" id="proposal_lelang_id" value="">
                    <input type="hidden" name="proposal_id" value="<?= $proposal_id ?>">
                    <input type="hidden" name="uuid" value="<?= $uuid ?>">
                    <div class="row mb-3">
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label class="form-label">Tipe Pengadaan</label>
                                <select class="form-control" name="tipe_pengadaan" id="tipe_pengadaan" required>
                                    <option value="">Pilih Tipe Pengadaan</option>
                                    <option value="Pelaksanaan">Pelaksanaan</option>
                                    <option value="Pengawasan">Pengawasan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group">
                                <label class="form-label">Tahun Anggaran</label>
                                <select class="form-control" name="tahun_anggaran" id="tahun_anggaran" required>
                                    <?php
                                    $range = range(2023, date('Y', strtotime('+1 year')));
                                    foreach ($range as $k => $v) {
                                        $selected = '';
                                        if ($v == date('Y', strtotime('+1 year'))) {
                                            $selected = 'selected';
                                        }
                                        echo ' <option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Kode Pengadaan</label>
                                <input type="text" class="form-control" id="kode" name="kode" placeholder="Masukkan kode pengadaan" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Tanggal</label>
                                <input type="date" class="form-control" id="tgl" name="tgl" placeholder="Masukkan tgl pengadaan" required>
                            </div>
                        </div>
                    </div>


                    <div class="form-group mb-3">
                        <label class="form-label">Judul Pengadaan</label>
                        <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan judul pengadaan" required>
                    </div>

                    <div class="form-group mb-3">
                        <label class="form-label">Lingkup Pengadaan</label>
                        <textarea class="form-control" id="lingkup" name="lingkup" required></textarea>
                    </div>

                    <div class="form-group mb-3">
                        <label class="form-label">Instansi</label>
                        <input type="text" class="form-control" id="instansi" name="instansi" placeholder="Masukkan instansi pengadaan" required>
                    </div>

                    <div class="alert alert-info text-center">
                        <b>Anggaran yang ditetapkan adalah Rp. <?= number_format($proposal['anggaran_ditetapkan']) ?><br>
                        <b>Anggaran yang digunakan adalah Rp. <?= number_format($total_pagu_digunakan) ?><br><strong class="text-warning"> Total Pagu Kedua Lelang tidak boleh melebihi Rp. <?= number_format($proposal['anggaran_ditetapkan']) ?> </strong></b>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">HPS</label>
                                <input type="number" step="any" class="form-control" id="hps" name="hps" placeholder="Masukkan HPS pengadaan" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label class="form-label">Pagu</label>
                                <input type="number" step="any" class="form-control" id="pagu" name="pagu" placeholder="Masukkan pagu pengadaan" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <label class="form-label">Dokumen Lelang (PDF)</label>
                        <div id="last_file"></div>
                        <input type="file" class="form-control" id="dokumen" accept="application/pdf" name="dokumen" placeholder="Masukkan dokumen lelang" required>
                    </div>

                    <hr>
                    <div class="table-responsive" data-aos="fade-up">
                        <table id="tbl_tahapan_pengadaan" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                    </div>

                    <hr>
                        <div class="row">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                        </div>
                    </div>

                </form>

            </div>




        </div>
    </div>
</div>

<div class="modal fade" id="modal_tahapan_lelang" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Selesaikan Lelang <?= $title ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="table-responsive" data-aos="fade-up">
                    <table id="tbl_tahapan_lelang" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                </div>

            </div>
        </div>
    </div>
</div>