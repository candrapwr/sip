<style>
    table .dropdown-menu.show {
        position: fixed !important;
        top: 60% !important;
        left: 100% !important;
        transform: translate(-92%, -50%) !important;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row gy-3">
                <div class="col-md-12">
                    <div class="card timbul">
                        <div class="card-body">
                            <?php
                            if (((($menu_id != '')) ? has_access($menu_id, 'create') : false)  == 1) { ?>
                                <div class="float-end">
                                    <button onclick="add()" class="btn btn-primary"><i class="ti ti-plus me-1"></i>Buat Proposal</button>
                                </div>
                            <?php } ?>
                            <h4 class="header-title">Revitalisasi Pasar</h4>
                            <p class="card-title-desc">Silahkan lihat, filter, dan kelola data Revitalisasi Pasar di digitalisasi Pasar</p>

                            <div class="row gy-3 justify-content-center">

                                <div class="col-12 col-md-6">
                                    <label>Provinsi</label>
                                    <select aria-label="Default select example" id="provinsi-name">
                                        <option value="">-- Pilih Provinsi --</option>
                                    </select>
                                </div>

                                <div class="col-12 col-md-6">
                                    <label>Kabupaten / Kota</label>
                                    <select aria-label="Default select example" id="kabkota-name">
                                        <option value="">-- Pilih Kabupaten/Kota --</option>
                                    </select>
                                </div>

                                <div class="col-12 col-md-6">
                                    <label>Pasar</label>
                                    <select id="f_pasar_id">
                                    </select>
                                </div>
                                <div class="col-6 col-md-3">
                                    <label>Tahun Anggaran</label>
                                    <select class="form-select" id="f_tahun_anggaran">
                                        <option value="">Tahun </option>
                                        <?php
                                        $range = range(2023, date('Y', strtotime('+1 year')));
                                        foreach ($range as $k => $v) {
                                            $selected = '';
                                            if ($v == date('Y', strtotime('+1 year'))) {
                                                $selected = 'selected';
                                            }
                                            echo ' <option value="' . $v . '" ' . $selected . '>' . $v . '</option>';
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="col-6 col-md-3">
                                    <label>Status</label>
                                    <select class="form-select" id="f_status">
                                        <option value="">Filter Status </option>
                                        <option value="Diajukan">Diajukan</option>
                                        <option value="Diundang Review">Di Undang Review</option>
                                        <option value="Selesai Review">Selesai Review</option>
                                        <option value="Ditetapkan">Ditetapkan</option>
                                    </select>

                                </div>

                            </div>
                            <div class="row justify-content-center my-3">
                                <div class="col-12 text-center">
                                    <button class="btn btn-dark m-1" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari Data</button>
                                    <button class="btn btn-outline-danger m-1" onclick="reset_form()" type="button"><i class="ti ti-x me-1"></i>Reset</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card timbul">
                        <div class="card-body">
                            <div class="table-responsive" data-aos="fade-up">
                                <table id="tbl_general" class="table table-striped table-bordered" style=" width: 100%;"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Proposal-->
<div class="modal fade" id="modal_proposal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah / Edit Proposal</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <form id="proposal_form">
                    <div id="wizard">
                        <h2>
                            <strong>Pilih Pasar</strong>
                            <p>Pilih pasar yang akan diajukan revitalisasi</p>
                        </h2>
                        <section>
                            <div class="row g-3">
                                <div class="col-12">
                                    <label class="form-label">Pasar</label>
                                    <select class="form-control" id="pasar_id" name="pasar_id" required>
                                        <option value="">Pilih pasar</option>
                                    </select>
                                </div>
                                <div id="container_detail_pasar">
                                    <div class="card timbul">
                                        <div class="card-body text-center">
                                            <h5>Silahkan pilih pasar yang akan diajukan Revitalisasi</h5>
                                            <h6>Jika data pasar tidak ada , Mohon pastikan Pasar yang akan diajukan proposal sudah <strong>Terverifikasi</strong> di menu <strong> Pasar</strong></h6>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </section>

                        <h2>
                            <strong>Proposal</strong>
                            <p>Silahkan lengkapi data proposal berikut</p>
                        </h2>
                        <section id="section2">
                            <div class="row mb-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Tanggal</label>
                                        <input type="date" class="form-control" id="tgl" name="tgl" min="<?= date('Y') . '-01-01' ?>" max="<?= date('Y-m-d') ?>" value="<?= date('Y-m-d') ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Pengusul</label>
                                        <input type="text" class="form-control" value="<?= $this->session->userdata('nama') ?>" id="pengusul" name="pengusul" placeholder="Masukkan pengusul" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Tingkat Pengusul</label>
                                        <select class="form-control" name="tingkat_pengusul" id="tingkat_pengusul" required>
                                            <option value="">Pilih tingkat pengusul</option>
                                            <?php
                                            $provinsi_selected = '';
                                            $kab_kota_selected = '';
                                            if ((in_array($this->session->userdata('role'), ['Dinas Provinsi', 'Kontributor SP2KP (Provinsi)']))) {
                                                $provinsi_selected = 'selected';
                                            } else {
                                                $kab_kota_selected = 'selected';
                                            }

                                            echo ' <option value="Kabupaten/Kota" ' . $kab_kota_selected . '>Kabupaten/Kota</option>
                                                <option value="Provinsi" ' . $provinsi_selected . '>Provinsi</option>'
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Nama Dinas</label>
                                        <input type="text" class="form-control" id="nama_dinas" value="<?= ((in_array($this->session->userdata('role'), ['Dinas Provinsi', 'Kontributor SP2KP (Provinsi)'])) ? 'Dinas Provinsi ' . $this->session->userdata('provinsi') : 'Dinas Kabupaten/Kota ' . $this->session->userdata('kab_kota')) ?>" name="nama_dinas" placeholder="Masukkan nama dinas" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Judul Proposal</label>
                                <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan judul proposal" required>
                                <input type="hidden" class="form-control" id="proposal_id" name="proposal_id">
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Latar Belakang</label>
                                <textarea class="form-control" id="latar_belakang" name="latar_belakang" required></textarea>
                            </div>
                            <div class="form-group mb-3">
                                <label class="form-label">Maksud dan Tujuan</label>
                                <textarea class="form-control" id="maksud_tujuan" name="maksud_tujuan" required></textarea>
                            </div>

                            <div class="row mb-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Kode Satker (contoh:10000)</label>
                                        <input type="number" step="any" class="form-control" id="kode_satker" name="kode_satker" placeholder="Masukkan kode satker" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Kode Lokasi (contoh:10.10)</label>
                                        <input type="number" step="any" class="form-control" id="kode_lokasi" name="kode_lokasi" placeholder="Masukkan kode lokasi" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Kode KPPN (contoh:111)</label>
                                        <input type="number" step="any" class="form-control" id="kode_kppn" name="kode_kppn" placeholder="Masukkan kode KPPN" required>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label class="form-label">Anggaran (Rp.)</label>
                                        <input type="number" step="any" class="form-control" id="anggaran" name="anggaran" placeholder="Masukkan jumlah anggaran" required>
                                    </div>
                                </div>
                            </div>

                        </section>
                        <h2>
                            <strong>Unggah Dokumen </strong>
                            <p>Silahkan unggah dokumen berikut</p>
                        </h2>
                        <section>
                            <div class="card timbul">
                                <div class="card-body document_container" style="min-height:500px">


                                </div>
                            </div>
                        </section>
                        <h2>
                            <strong>Unggah Foto Pasar</strong>
                            <p>Silahkan unggah foto pasar saat ini</p>
                        </h2>
                        <section>
                            <div class="card timbul">
                                <div class="card-body document_container" style="min-height:500px">


                                </div>
                            </div>
                        </section>
                    </div>
                </form>

            </div>


        </div>
    </div>
</div>


<div class="modal fade" id="modal_sk" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Unggah SK Operator </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url() ?>web/revitalisasi/proposal/sk" method="POST" id="frm_sk">
                    <input type="hidden" name="proposal_id" id="sk_proposal_id">
                    <input type="hidden" name="step" id="sk">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <div class="form-group mb-3">
                        <label>Proposal</label><br>
                        <strong id="sk_proposal"></strong>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label>Tanggal Efektif</label>
                                <input type="date" class="form-control" name="tgl_sk_efektif" id="sk_tgl_efektif" required>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="form-group mb-3">
                                <label>Tanggal Selesai</label>
                                <input type="date" class="form-control" name="tgl_sk_selesai" value="<?= date('Y-12-t') ?>" id="sk_tgl_selesai" required>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="table-responsive" data-aos="fade-up">
                        <table id="tbl_sk" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                    </div>

                    <div class="invalid-feedback" id="sk_error">

                    </div>
                    <hr>
                    <button type="submit" class="btn btn-success" style="float:right"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </form>

            </div>
        </div>
    </div>
</div>