<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Produksi Konsumsi</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Produksi Konsumsi</li>
                        </ol>
                    </div>
                </div>
                <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                    <div class="col-sm-6">
                        <div class="float-end">
                            <button id="create-commodity" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><i class="ti ti-filter me-1"></i>Fiter Data</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="ti ti-download me-1"></i>Unduh Data</button>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="pills-tabContent">
                                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                            <h4 class="header-title">Filter Data</h4>
                                            <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                            </p>
                                            <form>
                                                <div class="row">

                                                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                                                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'sardislog') : ?>
                                                        <div class="col">
                                                            <select class="mb-3" aria-label="Default select example" id="commodity-province">
                                                                <option value="">- Pilih Provinsi -</option>

                                                                <?php foreach ($provinces as $province) : ?>
                                                                    <option value="<?= $province['daerah_id'] ?>" <?= ($province['daerah_id'] == 31) ? 'selected' : '' ?>><?= $province['provinsi'] ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                        </div>
                                                    <?php endif; ?>

                                                    <div class="col">
                                                        <select class="mb-3" aria-label="Default select example" id="commodity-month">
                                                            <option value="">- Pilih Bulan -</option>

                                                            <option value="1" <?= (date('m') == 1) ? 'selected' : '' ?>>Januari</option>
                                                            <option value="2" <?= (date('m') == 2) ? 'selected' : '' ?>>Februari</option>
                                                            <option value="3" <?= (date('m') == 3) ? 'selected' : '' ?>>Maret</option>
                                                            <option value="4" <?= (date('m') == 4) ? 'selected' : '' ?>>April</option>
                                                            <option value="5" <?= (date('m') == 5) ? 'selected' : '' ?>>Mei</option>
                                                            <option value="6" <?= (date('m') == 6) ? 'selected' : '' ?>>Juni</option>
                                                            <option value="7" <?= (date('m') == 7) ? 'selected' : '' ?>>Juli</option>
                                                            <option value="8" <?= (date('m') == 8) ? 'selected' : '' ?>>Agustus</option>
                                                            <option value="9" <?= (date('m') == 9) ? 'selected' : '' ?>>September</option>
                                                            <option value="10" <?= (date('m') == 10) ? 'selected' : '' ?>>Oktober</option>
                                                            <option value="11" <?= (date('m') == 11) ? 'selected' : '' ?>>November</option>
                                                            <option value="12" <?= (date('m') == 12) ? 'selected' : '' ?>>Desember</option>

                                                        </select>
                                                    </div>

                                                    <div class="col">
                                                        <select class="mb-3" aria-label="Default select example" id="commodity-year">
                                                            <option value="">- Pilih Tahun -</option>

                                                            <?php foreach ($years as $year) : ?>
                                                                <option value="<?= $year ?>" <?= ($year == date('Y')) ? 'selected' : '' ?>><?= $year ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">

                                                        <div class="d-grid gap-2">
                                                            <button type="button" id="filter-commodity" class="btn btn-primary btn-filter">
                                                                <i class="ti ti-search me-1"></i>Filter Data
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                            <h4 class="header-title">Unduh Data</h4>
                                            <p class="card-title-desc">Silahkan unduh data Pasar yang diinginkan sesuai parameter yang anda masukkan
                                            </p>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'sardislog') : ?>
                                                        <select class="mb-3" id="post_provinsix" name="post_provinsix" aria-label="Default select example">
                                                        <?php else : ?>
                                                            <select class="mb-3" id="post_provinsix" name="post_provinsix" disabled="true" aria-label="Default select example">
                                                            <?php endif; ?>
                                                            <option value="">- Pilih Provinsi -</option>
                                                            <?php foreach ($provinces as $province) : ?>
                                                                <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                            <?php endforeach; ?>
                                                            </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <select class="bm-3" id="post_yearx" name="post_yearx" aria-label="Default select example">
                                                        <option value="">- Pilih Tahun -</option>
                                                        <?php foreach ($years as $year) : ?>
                                                            <option value="<?= $year ?>"><?= $year ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <select class="mb-3" id="post_bulanx" name="post_bulanx" aria-label="Default select example">
                                                        <option value="-">- Semua Bulan -</option>
                                                        <option value="1">Januari</option>
                                                        <option value="2">Februari</option>
                                                        <option value="3">Maret</option>
                                                        <option value="4">April</option>
                                                        <option value="5">Mei</option>
                                                        <option value="6">Juni</option>
                                                        <option value="7">Juli</option>
                                                        <option value="8">Agustus</option>
                                                        <option value="9">September</option>
                                                        <option value="10">Oktober</option>
                                                        <option value="11">November</option>
                                                        <option value="12">Desember</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="d-grid gap-2">
                                                        <button onClick="get_export()" type="button" id="btn_xls" class="btn btn-primary btn-filter">
                                                            <i class="ti ti-file-spreadsheet me-1"></i>Excel
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="table-commodity" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div class="modal fade staticBackdrop" id="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Komoditas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-tambah">
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <input type="hidden" id="komoditas_id" name="komoditas_id">

                <div class="modal-body">
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="date" class="col-sm-2 col-form-label">Tanggal</label>
                            <input class="form-control" type="month" id="date" min="2021-12" value="<?= date('Y-m') ?>" name="date">
                        </div>
                    </div>

                    <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>
                        <label for="province" class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="mb-3 row">
                            <select aria-label="Default select example" name="province" id="province">
                                <option value="">- Pilih Provinsi -</option>

                                <?php foreach ($provinces as $province) : ?>
                                    <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                    <?php endif; ?>

                    <div class="mb-3 row">
                        <label for="commodity" class="col-sm-2 col-form-label">Komoditi</label>
                        <select id="commodity" name="commodity">
                            <option value="">-- Pilih Komoditi --</option>

                            <?php foreach ($jenis_komoditi as $jk) : ?>
                                <optgroup label="<?= $jk['kelompok'] ?>" class="text-center">

                                    <?php foreach ($jk['sub'] as $sub) : ?>
                                        <option value="<?= $sub['id_jenis'] ?>"><?= $sub['jenis_komoditi'] ?></option>
                                    <?php endforeach; ?>

                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="unit" class="col-sm-2 col-form-label">Satuan</label>
                            <select id="unit" name="unit">
                                <option value="">-- Pilih Satuan --</option>
                            </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="produksi" class="col-sm-2 col-form-label">Produksi</label>
                            <input class="form-control number" type="text" id="produksi" name="produksi" autocomplete="off">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <div class="col">
                            <label for="konsumsi" class="col-sm-2 col-form-label">Konsumsi</label>
                            <input class="form-control number" type="text" id="konsumsi" name="konsumsi" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button style="width: 100%" type="submit" id="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>