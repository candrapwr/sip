<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Monitoring Input Harga Bahan Pokok</h4>
                                    <p class="card-title-desc">Silahkan masukkan filter dan inputan yang dibutuhkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select aria-label="Default select example myNewClass" id="filter-province">
                                                            <option value="">- Pilih Provinsi -</option>

                                                            <?php foreach ($provinces as $k => $v) { ?>
                                                                <?php
                                                                if (strtolower($this->session->userdata('role')) == 'dinas provinsi' ||  strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota' || strtolower($this->session->userdata('role')) == 'kontributor sp2kp (provinsi)' || strtolower($this->session->userdata('role')) == 'kontributor sp2kp (kabupaten/kota)') {
                                                                ?>

                                                                    <option value="<?= $v['daerah_id'] ?>|<?= $v['provinsi'] ?>" <?= ($v['daerah_id'] == substr($this->session->userdata('daerah_id'), 0, 2)) ? 'selected' : '' ?>><?= $v['provinsi'] ?></option>

                                                                <?php } else { ?>
                                                                    <option value="<?= $v['daerah_id'] ?>|<?= $v['provinsi'] ?>" <?= ($v['daerah_id'] == 31) ? 'selected' : '' ?>><?= $v['provinsi'] ?></option>

                                                            <?php }
                                                            } ?>

                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select class="mb-3" id="post_kab" name="post_kab">
                                                            <option value="">- Pilih Kabupaten/Kota -</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select aria-label="Default select example" id="filter-month">
                                                            <option value="">- Pilih Bulan -</option>

                                                            <option value="1" <?= (date('m') == 1) ? 'selected' : '' ?>>Januari</option>
                                                            <option value="2" <?= (date('m') == 2) ? 'selected' : '' ?>>Februari</option>
                                                            <option value="3" <?= (date('m') == 3) ? 'selected' : '' ?>>Maret</option>
                                                            <option value="4" <?= (date('m') == 4) ? 'selected' : '' ?>>April</option>
                                                            <option value="5" <?= (date('m') == 5) ? 'selected' : '' ?>>Mei</option>
                                                            <option value="6" <?= (date('m') == 6) ? 'selected' : '' ?>>Juni</option>
                                                            <option value="7" <?= (date('m') == 7) ? 'selected' : '' ?>>Juli</option>
                                                            <option value="8" <?= (date('m') == 8) ? 'selected' : '' ?>>Agustus</option>
                                                            <option value="9" <?= (date('m') == 9) ? 'selected' : '' ?>>September</option>
                                                            <option value="10" <?= (date('m') == 10) ? 'selected' : '' ?>>Oktober</option>
                                                            <option value="11" <?= (date('m') == 11) ? 'selected' : '' ?>>November</option>
                                                            <option value="12" <?= (date('m') == 12) ? 'selected' : '' ?>>Desember</option>

                                                        </select>
                                                    </div>

                                                    <div class="col-md-3">
                                                        <select aria-label="Default select example" id="filter-year">
                                                            <option value="">- Pilih Tahun -</option>

                                                            <?php foreach ($years as $year) : ?>
                                                                <option value="<?= $year ?>" <?= (date('Y') == $year) ? 'selected' : '' ?>><?= $year ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <button type="button" id="filter" class="btn btn-primary btn-filter px-3">
                                                            <i class="ti ti-search"></i><span style="font-size: 14px">Cari</span>
                                                        </button>
                                                        <span id="btn_export">
                                                            <button onClick="html_table_to_excel('xlsx')" type="button" id="btn_xls" class="btn btn-primary btn-filter px-3">
                                                                <i class="ti ti-file-spreadsheet me-1"></i><span style="font-size: 14px;">Excel</span>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="text-center">
                                        <h5 class="mb-1 text-uppercase">Tabel Monitoring Input Harga Bahan Pokok</h5>
                                        <p style="text-transform: uppercase"> <span id="province-title"></span></p>
                                    </div>

                                    <div class="col-md-12 pt-2" id="loader" style="height: 350px;">
                                    </div>

                                    <div class="col-md-12 pt-2" id="content">
                                        <span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>Lengkap</span>
                                        <span class="badge bg-warning"><i class="ti ti-hourglass-high me-1"></i>Belum Lengkap</span>
                                        <span class="badge bg-danger"><i class="ti ti-alert-triangle me-1"></i>Belum Lapor</span>

                                        <div class="table-responsive mt-1">
                                            <table id="main_table" class="table table-bordered" style="width:100%">
                                                <thead id="thead">
                                                </thead>
                                                <tbody id="tbody">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>