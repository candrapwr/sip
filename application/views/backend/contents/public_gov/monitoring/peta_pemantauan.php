<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Peta Stok Provinsi</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Peta Stok Provinsi</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="mb-0">Tahun Data</span>
                                            <select class="bm-3" id="post_year" name="post_year" aria-label="Default select example">
                                                <option value="">- Pilih Tahun -</option>
                                                <?php foreach ($years as $year) : ?>
                                                    <option value="<?= $year ?>"><?= $year ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="mb-0">Jenis Komoditi</span>
                                            <select class="bm-3" id="post_komoditi" name="post_komoditi" aria-label="Default select example">
                                                <option value="">- Bahan Pokok -</option>
                                                <?php foreach ($komoditi_bapok as $value) : ?>
                                                    <option value="<?= $value['id'] ?>"><?= $value['name'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card" id="loader">
                                <div class="card-body" style="height: 500px;">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card" id="content">
                                <div class="card-body">
                                    <div id="container"></div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
</div>