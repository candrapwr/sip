<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Tabel Pemantuan</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Tabel Pemantuan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select class="form-control" id="post_year" name="post_year" aria-label="Default select example">
                                                <option value="">- Pilih Tahun -</option>
                                                <?php foreach ($years as $year) : ?>
                                                    <option value="<?= $year ?>"><?= $year ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12" id="data-main">
                                            <img class=" mx-auto d-block" src="<?= base_url() ?>assets/frontend/img/default-nodata.svg">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>