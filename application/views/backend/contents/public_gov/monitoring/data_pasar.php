<div class="page-content">
	<!-- start page title -->
	<div class="page-title-box">
		<div class="container-fluid">
			<div class="row align-items-center">
				<div class="col-sm-6">
					<div class="page-title">
						<h4>Data Pasar Diinput</h4>
						<ol class="breadcrumb m-0">
							<li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
							<li class="breadcrumb-item active">Data Pasar Diinput</li>
						</ol>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end page title -->
	<div class="container-fluid">
		<div class="page-content-wrapper">
			<div class="row  justify-content-center">
				<div class="col-xl-12">
					<div class="row">
						<div class="col-12">
							<div class="card timbul">
								<div class="card-body">
									<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
										<li class="nav-item" role="presentation">
											<button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><i class="ti ti-filter me-1"></i>Fiter Data</button>
										</li>
										<li class="nav-item" role="presentation">
											<button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><i class="ti ti-download me-1"></i>Unduh Data</button>
										</li>
									</ul>
									<div class="tab-content" id="pills-tabContent">
										<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
											<h4 class="header-title">Filter Data</h4>
											<p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
											</p>
											<div class="row">
												<div class="col-md-3">
													<select class="mb-3" id="post_provinsi" name="post_provinsi" aria-label="Default select example">
														<option value="">- Pilih Provinsi -</option>
														<?php foreach ($provinces as $province) : ?>
															<option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
														<?php endforeach; ?>
													</select>
												</div>
												<div class="col-md-3">
													<select class="mb-3" id="post_kab" name="post_kab" aria-label="Default select example">
														<option value="">- Pilih Kabupaten/Kota -</option>
													</select>
												</div>
												<div class="col-md-3">
													<select class="bm-3" id="post_year" name="post_year" aria-label="Default select example">
														<option value="">- Pilih Tahun -</option>
														<?php 
															foreach ($years as $k => $v){
																if($v == date('Y')){
																	echo'<option value="'.$v.'" selected>'.$v.'</option>';
																}else{
																	echo'<option value="'.$v.'">'.$v.'</option>';
																}
															} 
														?>
													</select>
												</div>
												<div class="col-md-3">
													<select class="mb-3" id="post_month" name="post_month" aria-label="Default select example">
														<option value="">- Pilih Bulan -</option>
														<?php
															$bln=array(1=>"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
															for($bulan=1; $bulan<=12; $bulan++){
																$mount = '';
																if($bulan<=9) { 
																	$mount = '0'.$bulan; 
																} else { 
																	$mount = $bulan;
																}

																if(date('m') == $mount){
																	echo '<option value="'.$mount.'" selected>'.$bln[$bulan].'</option>'; 
																}else{
																	echo '<option value="'.$mount.'" '.$selected.'>'.$bln[$bulan].'</option>'; 
																}
															}
														?>

													</select>
												</div>
												<div class="col-md-12">
													<button type="button" id="btn_cari" class="btn btn-primary btn-filter px-3">
														<i class="ti ti-search me-1"></i><span style="font-size: 14px;">Cari</span>
													</button>
												</div>


											</div>
										</div>
										<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
											<h4 class="header-title">Unduh Data</h4>
											<p class="card-title-desc">Silahkan unduh data Pasar yang diinginkan sesuai parameter yang anda masukkan
											</p>
											<div class="row">
												<div class="col-md-5">
													<select class=" mb-3" aria-label="Default select example" id="post_provinsix" name="post_provinsix">
													<option value="">- Pilih Provinsi -</option>
													<?php foreach ($provinces as $province) : ?>
														<option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
													<?php endforeach; ?>
													</select>
												</div>
												<div class="col-md-5">
													<select class="bm-3" aria-label="Default select example" id="post_yearx" name="post_yearx">
														<option value="">- Pilih Tahun -</option>
														<?php foreach ($years as $year) : ?>
															<option value="<?= $year ?>"><?= $year ?></option>
														<?php endforeach; ?>
													</select>
												</div>
												<div class="col-md-2">
													<div class="d-grid gap-2">
															<button onClick="get_export()" type="button" id="btn_xls" class="btn btn-primary btn-filter">
																<i class="ti ti-file-spreadsheet me-1"></i>Excel
															</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div> <!-- end col -->
					</div> <!-- end row -->
					<div class="row">
						<div class="col-md-12">
							<div class="card timbul">
								<div class="card-body">
									<div class="row">
										<div class="col-md-12 text-center">
											<div id="dataTitle"></div>
										</div>
									</div>
									<div class="table-responsive mt-2" id="content">
										<div id="divtable">
											<table id="table" class="table table-striped table-bordered" style="width:100%"></table>
										</div>
									</div>
									<div class="card" id="loader">
										<div class="card timbul">
											<div class="card-body" style="height: 300px;">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>