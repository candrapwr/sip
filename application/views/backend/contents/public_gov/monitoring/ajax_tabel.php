<style>
	.halu {
		background-color: #F23939 !important;
		color: #F23939 !important;
	}
</style>


<?php if ($this->input->post('post_year') != '' && $this->input->post('daerah_id') == '') : ?>
	<div id="data-main">
		<h5 class="text-center" style="font-size: 18px">TABEL PEMANTAUAN TAHUN <?= $this->input->post('post_year') ?></h5>
	</div>
	<div class="col-md-12 mt-2">
		<div class="alert border-0 border-warning border-start border-4 bg-light-warning alert-dismissible fade show py-2">
			<div class="d-flex align-items-center">
				<div class="fs-3 text-warning"><i class="ti ti-info-circle"></i>
				</div>
				<div class="ms-3">
					<div class="text-warning"><strong>Keterangan</strong> Kolom yang berlatar belakang <strong>merah</strong> belum diinput. Kolom yang sudah terinput dapat dilihat jumlah angkanya.</div>
				</div>
			</div>
		</div>
	</div>
	<span id="btn_export">
		<button onClick="html_table_to_excel('xlsx')" type="button" id="btn_xls" class="btn btn-primary btn-filter px-3">
			<i class="ti ti-file-spreadsheet me-1"></i><span style="font-size: 14px;">Export Excel</span>
		</button>
	</span>
	<div class="col-md-12 pt-2">
		<div class="table-responsive" data-aos="fade-up">
			<table id="main_table" class="table table-striped table-bordered mb-0 ableq" style="width: 100%;">
				<thead class="bg-primary text-white">
					<tr>
						<th width="30%">NAMA PROVINSI</th>
						<th>JAN</th>
						<th>FEB</th>
						<th>MAR</th>
						<th>APR</th>
						<th>MEI</th>
						<th>JUN</th>
						<th>JUL</th>
						<th>AGU</th>
						<th>SEP</th>
						<th>OKT</th>
						<th>NOV</th>
						<th>DES</th>
					</tr>
				</thead>
				<tbody>
					<?php if ($data) : foreach ($data as $value) : ?>
							<tr class="item">
								<td><a class="fw-bold" href="javascript:void(0);" onclick="showAjax('<?= $this->input->post('post_year') ?>','<?= $value['daerah_id'] ?>');"><?= strtoupper($value['provinsi']); ?><i class="fadeIn animated bx bx-link ms-1"></i></a></td>
								<td class="<?= ($value['jan'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['jan'] ?></td>
								<td class="<?= ($value['feb'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['feb'] ?></td>
								<td class="<?= ($value['mar'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['mar'] ?></td>
								<td class="<?= ($value['apr'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['apr'] ?></td>
								<td class="<?= ($value['mei'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['mei'] ?></td>
								<td class="<?= ($value['jun'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['jun'] ?></td>
								<td class="<?= ($value['jul'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['jul'] ?></td>
								<td class="<?= ($value['agu'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['agu'] ?></td>
								<td class="<?= ($value['sep'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['sep'] ?></td>
								<td class="<?= ($value['okt'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['okt'] ?></td>
								<td class="<?= ($value['nov'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['nov'] ?></td>
								<td class="<?= ($value['des'] != "0") ? 'text-center' : 'halu' ?>"><?= $value['des'] ?></td>
							</tr>
					<?php endforeach;
					endif; ?>
				</tbody>
			</table>
		</div>
	</div>
<?php elseif ($this->input->post('post_year') != '' && $this->input->post('daerah_id') != '') : ?>

	<div class="row">
		<div class="col-lg-12">
			<div id="data-main">
				<div class="card-header py-3">
					<div class="row align-items-center g-3">
						<div class="col-12 col-lg-6">
							<h5 class="mb-0">DETAIL PEMANTAUAN <br>PROVINSI <?= strtoupper($data[0]['provinsi']) ?> TAHUN <?= $this->input->post('post_year') ?></h5>
						</div>
						<div class="col-12 col-lg-6 text-md-end">
							<button onclick="showBack('<?= $this->input->post('post_year') ?>');" class="btn btn-primary"><i class="ti ti-arrow-back me-1"></i>Kembali</button>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	<div class="row pt-3">
		<div class="col-md-6">
			<div id="chart"></div>
		</div>
		<div class="col-md-6">
			<div class="table-responsive" data-aos="fade-up">
				<table id="orochimaru" class="table table-striped table-bordered" style="width: 100%;">
					<thead class="bg-primary text-white">
						<tr>
							<th>BULAN</th>
							<th>KETERANGAN</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>JANUARI</td>
							<td class="text-center"><?php if ($data[0]['jan'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>FEBRUARI</td>
							<td class="text-center"><?php if ($data[0]['feb'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>MARET</td>
							<td class="text-center"><?php if ($data[0]['mar'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>APRIL</td>
							<td class="text-center"><?php if ($data[0]['apr'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>MEI</td>
							<td class="text-center"><?php if ($data[0]['mei'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>JUNI</td>
							<td class="text-center"><?php if ($data[0]['jun'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>JULI</td>
							<td class="text-center"><?php if ($data[0]['jul'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>AGUSTUS</td>
							<td class="text-center"><?php if ($data[0]['agu'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>SEPTEMBER</td>
							<td class="text-center"><?php if ($data[0]['sep'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>OKTOBER</td>
							<td class="text-center"><?php if ($data[0]['okt'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>NOVEMBER</td>
							<td class="text-center"><?php if ($data[0]['nov'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
						<tr>
							<td>DESEMBER</td>
							<td class="text-center"><?php if ($data[0]['des'] > 0) : ?><span class="badge bg-success"><i class="ti ti-circle-check me-1"></i>SUDAH INPUT</span><?php else : ?><span class="badge bg-danger"><i class="ti ti-x me-1"></i>TIDAK INPUT</span><?php endif; ?></td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>
	</div>
	<script>
		dataSer = [<?= $vInput ?>, <?= $vTidakInput ?>]
	</script>
<?php endif; ?>