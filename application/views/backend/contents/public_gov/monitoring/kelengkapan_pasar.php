<style>
    .dataTables_filter{
        float: right;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Data Kelengkapan Pasar</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Data Kelengkapan Pasar</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <h4 class="header-title">Filter Data</h4>
                                        <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                        </p>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <span class="mb-0">Provinsi</span>
                                                <select class="mb-3" id="post_provinsi" name="post_provinsi" aria-label="Default select example">
                                                    <option value="">Semua Provinsi</option>
                                                    <?php foreach ($provinces as $province) : ?>
                                                        <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <span class="mb-0">Kabupaten/Kota</span>
                                                <select class="mb-3" id="post_kab" name="post_kab" aria-label="Default select example">
                                                    <option value="">- Semua Kabupaten/Kota -</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-primary waves-effect waves-light mt-3" onclick="filter()" style="width: 100%;">Filter</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body" id="body_kelengkapan">

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <div id="dataTitle"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 pt-2" id="loader" style="height: 350px;">
                                    </div>

                                    <div id="content" class="table-responsive">
                                        <!-- <div class="btn-group mt-2" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-primary" onclick="get_export()"><i class="ti ti-file-spreadsheet me-1"></i>Export Data Pasar</button>
                                        </div> -->
                                        <table id="table" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>