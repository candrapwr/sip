<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Grafik Statistik Pemantauan Barang Penting</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Grafik Statistik Pemantauan Barang Penting</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title">Filter Data</h4>
                                    <p class="card-title-desc">Silahkan masukkan parameter yang diinginkan untuk menampilkan data
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="mb-0">Provinsi</span>
                                            <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator' || strtolower($this->session->userdata('role')) == 'sardislog') : ?>
                                                <select class="mb-3" id="post_provinsi" name="post_provinsi" aria-label="Default select example">
                                                <?php else : ?>
                                                    <select class="mb-3" id="post_provinsi" name="post_provinsi" disabled="true" aria-label="Default select example">
                                                    <?php endif; ?>
                                                    <option value="">- Pilih Provinsi -</option>
                                                    <?php foreach ($provinces as $province) : ?>
                                                        <option value="<?= $province['daerah_id'] ?>"><?= $province['provinsi'] ?></option>
                                                    <?php endforeach; ?>
                                                    </select>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="mb-0">Tahun Data</span>
                                            <select class="bm-3" id="post_year" name="post_year" aria-label="Default select example">
                                                <option value="">- Pilih Tahun -</option>
                                                <?php foreach ($years as $year) : ?>
                                                    <option value="<?= $year ?>"><?= $year ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card" id="loader">
                                <div class="card-body" style="height: 500px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card" id="content">
                                <div class="card-body">
                                    <div class="row">
                                        <div id="dataTitle" class="text-center"></div>
                                    </div>
                                    <div class="row pt-3">
                                        <div class="col-md-12" id="c_chart1">
                                            <div id="chart1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>