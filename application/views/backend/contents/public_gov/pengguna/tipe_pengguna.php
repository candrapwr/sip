<h6 class="mb-0 text-uppercase">Manajemen Data Tipe Pengguna</h6>
<hr>
<div class="row row-cols-12 row-cols-sm-12 row-cols-md-12 row-cols-xl-12 row-cols-xxl-12">

    <div class="col-12 d-flex">
        <div class="card radius-10 w-100">
            <div class="card-body">
                <div class="table-responsive" data-aos="fade-up">
                    <table id="table" class="table table-striped table-bordered" style="width:100%"></table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tipe Pengguna</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url() ?>master/cu_tipe_pengguna" method="POST" class="needs-validation default-form" novalidate>
                <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                <input type="hidden" name="tipe_pengguna_id" id="tipe_pengguna_id" value="">

                <div class="modal-body">
                    <div class="mb-3 row">
                        <label for="tipe_pengguna" class="col-sm-2 col-form-label">Tipe Pengguna</label>
                        <div class="col-sm-10">
                            <input type="text" name="tipe_pengguna" id="tipe_pengguna" class="form-control">
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="tipe_pengguna" class="col-sm-2 col-form-label">Flag Publik</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="flag_publik" name="flag_publik" required>
                                <option value="Y">Ya</option>
                                <option value="N">Tidak</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>