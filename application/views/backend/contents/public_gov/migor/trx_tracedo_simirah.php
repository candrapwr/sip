<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <form id="myForm" method="POST" action="<?php base_url() ?>trx-tracedo-minyak-goreng-simirah">
                                    <h4 class="header-title"><?= $title ?></h4>
                                    <p class="card-title-desc">Silahkan lihat data distribusi Minyak Goreng
                                    </p>
                
                                    <div class="row justify-content-center">
                                     
                                        <div class="col-6">
                                            <label>No DO</label>
                                            <input type="text" name="no_dokumen" placeholder="Tuliskan no dokumen" class="form-control" value="<?=$no_dokumen?>" id="no_dokumen">
                                            <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                                        </div>
                                       
                                        <div class="col-3">
                                            <label>&nbsp</label><br>
                                            <button class="btn btn-success btn-block" type="submit"><i class="ti ti-search me-1"></i>Cari</button>
                                            <button class="btn btn-light btn-block d-none" onclick="reset_form()"  type="button" ><i class="ti ti-x me-1"></i>Reset</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <?php if(!empty($no_dokumen)){ ?>
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                <h4 class="header-title">Data DO <b>"<?=$no_dokumen?>"</b> </h4>
                                <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Pengirim (Nama / NIB / Jumlah CPO) </label><br>
                                                <p><?=$cpo['nama_pengirim'].' / '.$cpo['nib_pengirim'].' / '.number_format($cpo['volume_received']).' '.$cpo['satuan']?></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Penerima (Nama / NIB / Jumlah CPO) </label><br>
                                                <p><?=$cpo['nama_penerima'].' / '.$cpo['nib_penerima'].' / '.number_format($cpo['volume_received']).' '.$cpo['satuan']?></p>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <b>Sumber :  SIMIRAH KEMENPERIN (Diolah Pusat Data dan Sistem Informasi KEMENDAG,2022)</b>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>