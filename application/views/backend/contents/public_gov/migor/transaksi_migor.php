<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Transaksi Minyak Goreng Curah</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data Transaksi Minyak Goreng Curah di digitalisasi Pasar
                                    </p>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-6">
                                            <label>Platform</label>
                                            <select class="mb-3" aria-label="Default select example" id="platform-name">
                                                <option value="">- Pilih Platform -</option>
                                                <option value="gurih" selected>Gurih</option>
                                                <option value="warung_pangan">Warung Pangan</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-6">
                                            <label>Produsen</label>
                                            <select class="mb-3" aria-label="Default select example" id="produsen-name">
                                            </select>
                                        </div>

                                        <?php if (strtolower($this->session->userdata('role')) != 'kontributor') { ?>
                                            <div class="col-lg-6">
                                                <label>Provinsi</label>
                                                <select class="mb-3" aria-label="Default select example" id="provinsi-name">
                                                    <option value="">-- Pilih Provinsi --</option>


                                                </select>
                                            </div>

                                            <div class="col-lg-6">
                                                <label>Kabupaten/Kota</label>
                                                <select class="mb-3" aria-label="Default select example" id="kabkota-name">
                                                    <option value="">-- Pilih Kabupaten/Kota --</option>
                                                </select>
                                            </div>
                                        <?php } ?>
                                        <div class="col-lg-6">
                                            <label>Tanggal Awal</label>
                                            <input type="date" class="form-control mb-3" value="<?= date('Y-m-d') ?>" id="start-date" placeholder="-- Pilih Tanggal Awal --">
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Tanggal Akhir</label>
                                            <input type="date" class="form-control mb-3" value="<?= date('Y-m-d') ?>" id="end-date" placeholder="-- Pilih Tanggal Akhir --">
                                        </div>
                                        <div class="col-lg-1">
                                            <label>&nbsp</label><br>
                                            <button class="btn btn-success btn-block" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari</button>
                                            <button class="btn btn-light btn-block d-none" onclick="reset_form()" type="button"><i class="ti ti-x me-1"></i>Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade staticBackdrop" id="modal-detail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Transaksi</h5>
            </div>
            <div class="modal-body">
                <table>
                    <tbody>
                        <tr>
                            <th scope="row">Kode Transaksi</th>
                            <td>:</td>
                            <td id="kode_transaksi"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade staticBackdrop" id="modal-ktp" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="nik"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="file_nik">

            </div>
        </div>
    </div>
</div>