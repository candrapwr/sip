<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active">Perdagangan "<?= $title ?>"</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                
                                <div class="card-body">

                                    <h4 class="header-title"><?= $title ?></h4>
                                    <p class="card-title-desc">Silahkan lihat dan akses data <?= $title ?> secara lengkap dan interaktif
                                    <button type="button" class="btn btn-light" style="float:right" onclick="fullscreen('dashboard')"><i class="ti ti-arrows-maximize"></i>Fullscreen</button>
                                    </p>
                                  
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="ls-outer">
                                                <iframe id="dashboard" src="https://analitik.kemendag.go.id/trusted/<?= $token . $view_tableau ?>?<?= $urlparams ?>" width="100%" height="950px" frameborder="0">
                                                </iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>