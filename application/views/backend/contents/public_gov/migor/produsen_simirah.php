<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                             <div class="card-body">
                                    <form id="myForm">
                                        <h4 class="header-title"><?= $title ?></h4>
                                        <p class="card-title-desc">Silahkan lihat dan kelola data Produsen Minyak Goreng
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-lg-2">
                                                <label>Provinsi</label>
                                                <select class="mb-3" aria-label="Default select example" id="provinsi-name">
                                                    <option value="">-- Pilih Provinsi --</option>


                                                </select>
                                            </div>

                                            <div class="col-12 col-lg-3">
                                                <label>Kabupaten / Kota</label>
                                                <select class="mb-3" aria-label="Default select example" id="kabkota-name">
                                                    <option value="">-- Pilih Kabupaten/Kota --</option>
                                                </select>
                                            </div>

                                            <div class="col-12 col-lg-3">
                                                <label>NIB</label>
                                                <input type="text" name="nib" placeholder="Tuliskan nib" class="form-control" id="nib">
                                            </div>
                                            <div class="col-12 col-lg-3">
                                                <label>Nama</label>
                                                <input type="text" name="nama" placeholder="Tuliskan nama" class="form-control" id="nama">
                                            </div>

                                            <div class="col-12 col-lg-1">
                                                <label>&nbsp</label><br>
                                                <button class="btn btn-success btn-block" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari</button>
                                                <button class="btn btn-light btn-block d-none" onclick="reset_form()" type="button"><i class="ti ti-x me-1"></i>Reset</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                        </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>