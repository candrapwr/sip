<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <form id="myForm">
                                        <h4 class="header-title"><?= $title ?></h4>
                                        <p class="card-title-desc">Silahkan lihat data distribusi Minyak Goreng
                                        </p>
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-lg-3">
                                                <label>Periode Awal Pengiriman</label>
                                                <input type="date" name="tgl_awal" class="form-control" id="tgl_awal">
                                            </div>
                                            <div class="col-12 col-lg-3">
                                                <label>Periode Akhir Pengiriman</label>
                                                <input type="date" name="tgl_akhir" class="form-control" id="tgl_akhir">
                                            </div>
                                            <div class="col-12 col-lg-3">
                                                <label>Provinsi</label>
                                                <select class="mb-3" aria-label="Default select example" id="provinsi-name">
                                                    <option value="">-- Pilih Provinsi --</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-12 col-lg-3">
                                                <label>Type</label>
                                                <select class="form-control" name="type" aria-label="Default select example" id="type">
                                                    <option value="">-- Pilih Type --</option>
                                                    <option value="Produsen CPO">Produsen CPO</option>
                                                    <option value="Produsen Migor">Produsen Migor</option>
                                                    <option value="Produsen Migor (CPO)">Produsen Migor (CPO)</option>
                                                    <option value="Exportir - Produsen CPO">Exportir - Produsen CPO</option>
                                                    <option value="Exportir - Produsen Migor">Exportir - Produsen Migor</option>
                                                    
                                                </select>
                                            </div>
                                                <div class="col-12 col-lg-3">
                                                    <label>Nama</label>
                                                    <input type="text" name="nama_pengirim" placeholder="Tuliskan nama pengirim" class="form-control" id="nama_pengirim">
                                                </div>
                                                <div class="col-12 col-lg-3">
                                                    <label>&nbsp</label><br>
                                                    <!--a class="btn btn-light btn-block" href="<?=base_url()?>web/alokasi-minyak-goreng-simirah" style="float: right; margin-left:5px"><i class="ti ti-table me-1"></i>Alokasi DMO</a-->
                                                    <button class="btn btn-success btn-block" style="float: right;" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari</button>
                                                    
                                                </div>
                                            </div>
                                    </form>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                
                                    <div class="table-responsive" data-aos="fade-up">
                                        <div class="row" style="justify-content: right !important;">
                                            <div class="col-lg-2 col-12">
                                                 Pengali Berdasarkan :
                                            </div>
                                            <div class="col-lg-3 col-12">
                                            <b id="kepmendag"></b>
                                            </div>
                                        </div>
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                    <b>Sumber : SIMIRAH KEMENPERIN (Diolah Pusat Data dan Sistem Informasi KEMENDAG,2022)</b>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table" id="tbl_detail" width="100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>