<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">

                    <div class="card timbul">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <div class="col-md-6 col-12">
                                    <h4>Pemantauan Distribusi Minyak Goreng</h4>
                                </div>
                                <div class="col-md-3 col-12">
                                    <button type="button" onclick="add()" class="btn btn-info" style="float:right"><i class="ti ti-plus me-1"></i>Laporkan Pemantauan</button>
                                </div>
                            </div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pelaporan_-tab" data-bs-toggle="tab" href="#pelaporan_" role="tab" aria-controls="pelaporan_" aria-selected="false">Pelaporan</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " id="rekap-tab" data-bs-toggle="tab" href="#rekap" role="tab" aria-controls="rekap" aria-selected="true">Rekap Pelaporan</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane p-1 active" id="pelaporan_" role="tabpanel" aria-labelledby="pelaporan-tab">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                                <div class="tab-pane p-1" id="rekap" role="tabpanel" aria-labelledby="rekap-tab">
                                <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_rekap_pemantauan" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                    </div>
                                </div>
                            </div>

                            <b>Sumber : SIMIRAH KEMENPERIN (Diolah Pusat Data dan Sistem Informasi KEMENDAG,2022)</b>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_general" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Pelaporan Pemantauan Distribusi Minyak Goreng</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="myForm" method="POST" action="<?= base_url() ?>perdagangan/migor/laporkan-distribusi">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-4">
                            <label>Tanggal Pemantauan</label>
                            <input type="date" name="tgl_pemantauan" placeholder="dd-mm-yyyy" value="<?= date('Y-m-d') ?>" class="form-control" id="tgl_pemantauan" required>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Provinsi</label>
                            <select class="mb-3" aria-label="Default select example" name="provinsi_id" id="provinsi-name" required>
                                <option value="">-- Pilih Provinsi --</option>
                            </select>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Kabupaten</label>
                            <select class="mb-3" aria-label="Default select example" name="daerah_id" id="kabkota-name" required>
                                <option value="">-- Pilih Kabupaten --</option>
                            </select>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-12">
                            <label>Petugas</label>
                            <textarea type="text" name="petugas" placeholder="Tuliskan petugas pemantauan" class="form-control" id="petugas" required></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-4">
                            <label>Terdaftar SIMIRAH ?</label>
                            <select class="form-control" name="terdaftar" id="terdaftar" required>
                                <option value="Ya">Ya</option>
                                <option value="Tidak">Tidak</option>
                            </select>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Jenis Pelaku Usaha ?</label>
                            <select class="form-control" name="jenis_pu" id="jenis_pu" required>
                                <option value="">Pilih Jenis Pelaku Usaha</option>
                                <option value="produsen-cpo">Produsen CPO</option>
                                <option value="produsen-migor">Produsen Migor</option>
                                <option value="distributor1">Distributor 1</option>
                                <option value="distributor2">Distributor 2</option>
                                <option value="pengecer">Pengecer</option>

                            </select>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Nama Pelaku Usaha</label>
                            <select class="form-control" name="nama_pu" id="nama_pu" required>
                                <option value="">Tuliskan Nama Pelaku Usaha </option>

                            </select>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-4">
                            <label>NIB/NPWP</label>
                            <input type="text" name="nib" class="form-control" id="nib" placeholder="NIB" required>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Nama Pemilik / Penanggung Jawab</label>
                            <input type="text" name="nama_pemilik" class="form-control" id="nama_pemilik" placeholder="Nama Pemilik / Penanggung Jawab" required>
                        </div>
                        <div class="col-12 col-lg-4">
                            <label>Telp Pemilik / Penanggung Jawab</label>
                            <input type="text" name="telp_pemilik" class="form-control" id="telp_pemilik" placeholder="Telpon Pemilik / Penanggung Jawab" required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-12">
                            <label>Alamat</label>
                            <input type="text" name="alamat_pu" placeholder="" class="form-control" id="alamat_pu" required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-12">
                            <label>Catatan</label>
                            <textarea type="text" name="catatan" placeholder="" class="form-control" id="catatan"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-3">
                            <button type="submit" class="btn btn-dark"><i class="ti ti-device-floppy me-1"></i>Simpan Pelaporan</button>
                        </div>
                    </div>
                    <hr>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_penerimaan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Pelaporan Penerimaan Minyak Goreng <b id="perusahaan"></b></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="myForm" method="POST" enctype="multipart/form-data" action="<?= base_url() ?>perdagangan/migor/penerimaan-distribusi">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                    <input type="hidden" name="pemantauan_migor_id" id="pemantauan_migor_id_fk" value="">
                    <input type="hidden" name="terdaftar_simirah" id="terdaftar_simirah_fk" value="Tidak">
                    <input type="hidden" name="jenis_pengirim_temp" id="jenis_pengirim_temp" value="">
                    <input type="hidden" name="nib_temp" id="nib_temp" value="">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-9">
                            <label>No Dokumen</label>
                            <input type="text" name="no_dokumen" class="form-control" id="no_dokumen" required>
                            <div class="invalid-feedback" id="no_do">
                                <b>&nbsp DO tidak ditemukan</b>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <label>&nbsp</label><br>
                            <button type="button" onclick="checkDO()" class="btn btn-success btn-block"><i class="ti ti-search me-1"></i>Check</button>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-6">
                            <label>Tanggal DO</label>
                            <input type="date" name="tgl_do" placeholder="dd-mm-yyyy" value="" class="form-control" id="tgl_do" required>
                        </div>
                        <div class="col-12 col-lg-6">
                            <label>Tanggal Status (Proses Distribusi / Selesai)</label>
                            <input type="date" name="tgl_status" placeholder="dd-mm-yyyy" value="" class="form-control" id="tgl_status" required>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-6">
                            <label>Jenis Pemasok / Pengirim</label>
                            <select class="form-control" name="jenis_pengirim" id="jenis_pengirim" required>
                                <option value="">Pilih Jenis Pengirim</option>
                                <option value="produsen-cpo">Produsen CPO</option>
                                <option value="produsen-migor">Produsen Migor</option>
                                <option value="distributor1">Distributor 1</option>
                                <option value="distributor2">Distributor 2</option>
                                <option value="pengecer">Pengecer</option>

                            </select>
                        </div>
                        <div class="col-12 col-lg-6">
                            <label>NIB Pemasok / Pengirim</label>
                            <input type="text" name="nib_pengirim" class="form-control" id="nib_pengirim" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-12 col-lg-12">
                            <label>Nama Pemasok / Pengirim</label>
                            <input type="text" name="nama_pengirim" class="form-control" id="nama_pengirim" required>
                        </div>

                    </div>
                    <br>
                    <div id="is_simirah" style="display: none;">

                        <div class="row ">
                            <div class="col-12 col-lg-6">
                                <label>Jumlah Distribusi (kg)</label>
                                <input type="number" name="jml_distribusi" step="any" class="form-control" id="jml_distribusi">
                            </div>

                            <div class="col-12 col-lg-6">
                                <label>Harga Pembelian (Rp.)</label>
                                <input type="number" name="harga_pembelian" step="any" class="form-control" id="harga_pembelian">
                            </div>
                        </div>
                        <br>
                        <div class="col-12 col-lg-6" id="is_kesesuaian" style="display: none;">
                            <label>Kesesuaian</label>
                            <select class="form-control" name="kesesuaian" id="kesesuaian" required>
                                <option value="">Pilih Kesesuaian</option>
                                <option value="sesuai" selected>Sesuai</option>
                                <option value="tidak sesuai">Tidak Sesuai</option>
                            </select>
                        </div>
                        <br>
                    </div>
                    <hr>
                    <div id="is_sesuai" style="display: inline;">
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-6">
                                <label>Jumlah Distribusi Sebenarnya (kg)</label>
                                <input type="number" name="jml_distribusi_rill" step="any" class="form-control" id="jml_distribusi_rill" required>
                            </div>
                            <div class="col-12 col-lg-6">
                                <label>Harga Pembelian Sebenarnya (Rp.)</label>
                                <input type="number" name="harga_pembelian_rill" step="any" class="form-control" id="harga_pembelian_rill" required>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row justify-content-center">

                        <div class="col-12 col-lg-6">
                            <label>Harga Penjualan (Rp.)</label>
                            <input type="number" name="harga_penjualan_rill" class="form-control" id="harga_penjualan_rill" required>
                        </div>

                        <div class="col-12 col-lg-6">
                            <label>Stok (kg)</label>
                            <input type="number" name="stok" class="form-control" id="stok" required>
                        </div>
                    </div>

                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-12">
                            <label>Lampiran (jpg,png)</label>
                            <input type="file" class="form-control" accept="image/*" name="lampiran" id="lampiran">
                        </div>

                    </div>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-12">
                            <label>Keterangan</label>
                            <textarea type="text" name="keterangan" placeholder="Tuliskan keterangan" class="form-control" id="keterangan"></textarea>
                        </div>
                    </div>

                    <br>
                    <br>
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-3">
                            <button type="submit" class="btn btn-dark"><i class="ti ti-device-floppy me-1"></i>Simpan Pelaporan</button>
                        </div>
                    </div>
                    <hr>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_distribusi" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Penerimaan dan Distribusi Minyak Goreng</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="pengecer_penerimaan_d1" style="display: none">
                    <h4>Penerimaan <span class="fw-bolder"> dari Distributor 1</span> (SIMIRAH)</h4>
                    <table class="table table-striped" id="tbl_penerimaan_pengecer" width="100%">
                        <thead></thead>
                        <tbody></tbody>
                    </table>
                    <hr>
                </div>
                <h4>Penerimaan <span id="penerima_dari" class="fw-bolder"></span> (SIMIRAH)</h4>
                <table class="table table-striped" id="tbl_penerimaan" width="100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
                <hr>
                <h4>Distribusi <span id="distribusi_dari" class="fw-bolder"></span> (SIMIRAH)</h4>
                <table class="table table-striped" id="tbl_distribusi" width="100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>


            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_pengawasan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pemantauan Distribusi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="tbl_pemantauan_detail" width="100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>

            </div>
        </div>
    </div>
</div>