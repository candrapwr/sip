<style>
    table th {
        text-align: center;
        vertical-align: middle;
    }
</style>
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h4 class="header-title"><?= $title ?></h4>
                                    <p class="card-title-desc">Silahkan lihat data distribusi Minyak Goreng </p>
                                    <ul class="nav nav-tabs" id="mainTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="statistik-tab" data-bs-toggle="tab" href="#statistik" role="tab" aria-controls="statistik" aria-selected="false">Statistik</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " id="perusahaan-tab" data-bs-toggle="tab" href="#perusahaan" role="tab" aria-controls="perusahaan" aria-selected="true">Alokasi Perusahaan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link " id="data-tab" data-bs-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Riwayat</a>
                                        </li>

                                    </ul>

                                    <div class="tab-content" id="mainTabContent">

                                        <div class="tab-pane p-1 active" id="statistik" role="tabpanel" aria-labelledby="statistik-tab">
                                            <div class="row justify-content-center mt-2 mb-2">
                                                <div class="col-12 col-lg-3">
                                                    <select name="periode_awal" id="periode_awal">
                                                        <?php
                                                        $awal = new DatePeriod(
                                                            new DateTime('2022-09-08'),
                                                            new DateInterval('P1D'),
                                                            new DateTime((string)date('Y-m-d', strtotime('-1 days')))
                                                        );
                                                        foreach ($awal as $k => $v) {
                                                            echo '<option value="' . $v->format('Y-m-d')  . '" ' . (($k == 0) ? 'selected' : '') . '>' . $v->format('Y-m-d')  . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-lg-3">
                                                    <select name="periode_akhir" id="periode_akhir">
                                                        <?php
                                                        $akhir = new DatePeriod(
                                                            new DateTime('2022-09-09'),
                                                            new DateInterval('P1D'),
                                                            new DateTime((string)date('Y-m-d'))
                                                        );
                                                        $dates = array();
                                                        foreach ($akhir as $dt) {
                                                            $dates[] = $dt->format("Y-m-d");
                                                        }

                                                        foreach (array_reverse($dates) as $k => $v) {
                                                            echo '<option value="' . $v  . '" ' . (($k == 0) ? 'selected' : '') . '>' . $v  . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <hr>
                                            <canvas id="myChart" width="400" height="100"></canvas>
                                            <hr>
                                            <button type="button" onclick="exportStatAlokasi()" class="btn btn-outline-dark btn-md" style="float: right; margin:10px"><i class="ti ti-file-spreadsheet me-1"></i>Export Excel</button>
                                            <table class="table table-bordered mt-2" id="tbl_stat">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Periode</th>
                                                        <th>Tanggal Data</th>
                                                        <th>Jumlah CPO</th>
                                                        <th>Jumlah NON CPO</th>
                                                        <th>Satuan</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body_tbl_stat">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane p-2 " id="perusahaan" role="tabpanel" aria-labelledby="perusahaan-tab">
                                            <form id="myForm" class="mt-2">

                                                <div class="row justify-content-center">
                                                    <div class="col-12 col-lg-2">
                                                        <label>Periode Awal</label>
                                                        <select name="perusahaan_periode_awal" id="perusahaan_periode_awal">
                                                            <option value="">Pilih Periode Awal</option>
                                                            <?php

                                                            foreach ($dates as $k => $v) {
                                                                if ($k == 0) {
                                                                    echo '<option value="' . $v . '" selected>' . $v . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $v . '">' . $v . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 col-lg-2">
                                                        <label>Periode Akhir</label>
                                                        <select name="perusahaan_periode_akhir" id="perusahaan_periode_akhir">
                                                            <option value="">Pilih Periode Akhir</option>
                                                            <?php

                                                            foreach (array_reverse($dates) as $k => $v) {
                                                                if ($k == 0) {
                                                                    echo '<option value="' . $v . '" selected>' . $v . '</option>';
                                                                } else {
                                                                    echo '<option value="' . $v . '">' . $v . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 col-lg-3">
                                                        <label>NIB</label>
                                                        <input type="text" name="perusahaan_nib" id="perusahaan_nib" value="" class="form-control" placeholder="NIB">
                                                    </div>
                                                    <div class="col-12 col-lg-3">
                                                        <label>Nama</label>
                                                        <input type="text" name="perusahaan_nama" id="perusahaan_nama" value="" class="form-control" placeholder="Nama Perusahaan">
                                                    </div>
                                                    <div class="col-12 col-lg-2">
                                                        <label>&nbsp</label><br>

                                                        <button class="btn btn-success btn-block" style="float: right;" type="button" onclick="showAlokasiPerusahaan('refresh')"><i class="ti ti-list me-1"></i>Tampilkan</button>

                                                    </div>

                                                </div>

                                            </form>
                                            <hr>
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_alokasi_perusahaan" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                        <div class="tab-pane p-2 " id="data" role="tabpanel" aria-labelledby="data-tab">
                                            <form id="myForm" class="mt-2">

                                                <div class="row justify-content-center">
                                                    <div class="col-12 col-lg-3">
                                                        <label>Periode Awal Pengiriman</label>
                                                        <select name="tgl_awal" id="tgl_awal">
                                                            <option value="">Pilih Tgl Awal</option>
                                                            <?php
                                                            foreach ($periode['data'] as $k => $v) {
                                                                echo '<option value="' . $v['tgl_awal'] . '">' . $v['tgl_awal'] . '</option>';
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 col-lg-3">
                                                        <label>Periode Akhir Pengiriman</label>
                                                        <select name="tgl_akhir" id="tgl_akhir">
                                                            <option value="">Pilih Tgl Akhir</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-12 col-lg-3 d-none">
                                                        <label>Data Series (Default 2 Terakhir)</label>
                                                        <input type="number" name="series" id="series" value="2" class="form-control" readonly>
                                                    </div>
                                                    <div class="col-12 col-lg-2">
                                                        <label>&nbsp</label><br>

                                                        <button class="btn btn-success btn-block" style="float: right;" type="button" onclick="showSummary()"><i class="ti ti-list me-1"></i>Tampilkan</button>

                                                    </div>

                                                </div>

                                            </form>
                                            <hr>
                                            <div class="table-responsive" data-aos="fade-up">
                                                <table id="tbl_general" class="table table-striped table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;"></table>
                                            </div>
                                        </div>
                                    </div>
                                    <b>Sumber : SIMIRAH KEMENPERIN (Diolah Pusat Data dan Sistem Informasi KEMENDAG,2022)</b>

                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">

                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Summary Alokasi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="isi_isi" style=" height:500px;overflow-x:auto;overflow-y:auto;">

                <div class="m-3">
                    <button class="btn btn-light btn-block" style="float:right" type="button" onclick="exportExcel()"><i class="ti ti-file-spreadsheet me-1"></i>Export Excel</button>
                    <button class="btn btn-warning btn-block d-none" style="float:right" id="btn_kirimalokasi" type="button" onclick="kirimAlokasi()"><i class="ti ti-file-spreadsheet me-1"></i>Kirim Alokasi</button>
                    <input type="hidden" id="k_alokasi_id" value="">
                    <input type="hidden" id="k_tgl_awal" value="">
                    <input type="hidden" id="k_tgl_akhir" value="">

                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="produsen-cpo-tab" data-bs-toggle="tab" href="#produsen-cpo" role="tab" aria-controls="produsen-cpo" aria-selected="true">Produsen CPO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="produsen-migor-tab" data-bs-toggle="tab" href="#produsen-migor" role="tab" aria-controls="produsen-migor" aria-selected="false">Produsen Migor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="produsen-migor-cpo-tab" data-bs-toggle="tab" href="#produsen-migor-cpo" role="tab" aria-controls="produsen-migor-cpo" aria-selected="false">Produsen Migor (CPO)</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" id="exportir-cpo-tab" data-bs-toggle="tab" href="#exportir-cpo" role="tab" aria-controls="exportir-cpo" aria-selected="false">Exportir - Produsen CPO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="exportir-migor-tab" data-bs-toggle="tab" href="#exportir-migor" role="tab" aria-controls="exportir-migor" aria-selected="false">Exportir - Produsen Migor</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane  active" id="produsen-cpo" role="tabpanel" aria-labelledby="produsen-cpo-tab"></div>
                        <div class="tab-pane " id="produsen-migor" role="tabpanel" aria-labelledby="produsen-migor-tab"></div>
                        <div class="tab-pane " id="produsen-migor-cpo" role="tabpanel" aria-labelledby="produsen-migor-cpo-tab"></div>
                        <div class="tab-pane " id="exportir-cpo" role="tabpanel" aria-labelledby="exportir-cpo-tab"></div>
                        <div class="tab-pane " id="exportir-migor" role="tabpanel" aria-labelledby="exportir-migor-tab"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_payload" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alokasi Terkirim <div id="summary_periode"></div>
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="isi_isi" style=" height:500px;overflow-x:auto;overflow-y:auto;">

                <div class="m-3">
                    <button class="btn btn-light btn-block" style="float:right" type="button" onclick="exportAlokasi()"><i class="ti ti-file-spreadsheet me-1"></i>Export Excel</button>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="main-tab" data-bs-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Main</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="payload-tab" data-bs-toggle="tab" href="#payload" role="tab" aria-controls="payload" aria-selected="false">Payload Kirim</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane  active" id="main" role="tabpanel" aria-labelledby="main-tab">
                            <table class="table table-responsive" style="margin-top:10px" id="tbl_data_alokasi">
                                <tbody id="tbl_main">

                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane " id="payload" role="tabpanel" aria-labelledby="payload-tab">
                            <div class="card timbul">
                                <div class="card-body">
                                    <h5>Data dikirim</h5>
                                    <div id="response_body"></div>
                                </div>
                            </div>

                            <div class="card timbul">
                                <div class="card-body">
                                    <h5>Response</h5>
                                    <div id="response_output"></div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_perusahaan" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alokasi Perusahaan
                </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"  style=" min-height:200px;">
            <button type="button" onclick="exportPerusahaan()" class="btn btn-outline-dark btn-md" style="float: right; margin:10px"><i class="ti ti-file-spreadsheet me-1"></i>Export Excel</button>
            <hr>
                <table class="table table-responsive table-stripped" style="margin-top:10px">
                  <tbody  id="tbl_perusahaan">

                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>