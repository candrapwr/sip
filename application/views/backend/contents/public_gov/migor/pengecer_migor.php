<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4><?= $title ?></h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Digitalisasi Pasar SISP</a></li>
                            <li class="breadcrumb-item active"><?= $title ?></li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="float-end">
                        <button id="create-commodity" class="btn btn-success"><i class="ti ti-plus me-1"></i>Tambah Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row  justify-content-center">
                <div class="col-xl-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="card timbul">
                                <div class="card-body">

                                    <h4 class="header-title">Pengecer Minyak Goreng Curah</h4>
                                    <p class="card-title-desc">Silahkan lihat dan kelola data Pengecer Minyak Goreng Curah di digitalisasi Pasar
                                    </p>
                                    <div class="row">
                                        <div class="col">
                                            <label>Platform</label>
                                            <select class="mb-3" aria-label="Default select example" id="platform-name">
                                                <option value="">- Pilih Platform -</option>
                                                <option value="gurih" selected>Gurih</option>
                                                <option value="warung_pangan">Warung Pangan</option>
                                            </select>
                                        </div>

                                        <?php if (strtolower($this->session->userdata('role')) != 'kontributor') { ?>
                                            <div class="col">
                                                <label>Provinsi</label>
                                                <select class="mb-3" aria-label="Default select example" id="provinsi-name">
                                                    <option value="">- Pilih Provinsi -</option>
                                                </select>
                                            </div>

                                            <div class="col">
                                                <label>Kabupaten/Kota</label>
                                                <select class="mb-3" aria-label="Default select example" id="kabkota-name">
                                                    <option value="">- Pilih Kabupaten/Kota -</option>
                                                </select>
                                            </div>
                                        <?php } ?>
                                        <div class="col">
                                            <label>&nbsp</label><br>
                                            <button class="btn btn-success btn-block" type="button" onclick="fill()"><i class="ti ti-search me-1"></i>Cari</button>
                                            <button class="btn btn-light btn-block d-none" onclick="reset_form()" type="button"><i class="ti ti-x me-1"></i>Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-md-12">
                            <div class="card timbul">
                                <div class="card-body">
                                    <div class="table-responsive" data-aos="fade-up">
                                        <table id="tbl_general" class="table table-striped table-bordered" style="width:100%"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end row -->
                </div>
            </div>
        </div>
    </div>
</div>