<h6 class="mb-0 text-uppercase">Data Pasar</h6>
<hr>
<div class="row row-cols-12 row-cols-sm-12 row-cols-md-12 row-cols-xl-12 row-cols-xxl-12 animate__animated animate__rubberBand">
    <div class="col-12">
        <div class="card radius-10 w-100">
            <div class="card-body">
                <form method="post" action="<?= site_url() ?>administrator/pasar?filter=<?= sha1(md5($this->session->userdata('username'))) ?>">
                    <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">

                    <div class="row">
                        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="kabupaten" id="kabupaten">
                                    <option value="">Kabupaten/Kota</option>

                                    <?php foreach ($this->session->userdata('master_kabkota') as $kabkota) : ?>
                                        <option value="<?= $kabkota['daerah_id'] ?>" <?= ($kabkota['daerah_id'] == (!empty($this->session->userdata('filter_pasar')['filter_search_kabupaten']) ? $this->session->userdata('filter_pasar')['filter_search_kabupaten'] : '')) ? 'selected' : '' ?>><?= $kabkota['kab_kota'] ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'eksekutif pdn' || strtolower($this->session->userdata('role')) == 'teknis pdn' || strtolower($this->session->userdata('role')) == 'eksekutif' || strtolower($this->session->userdata('role')) == 'administrator') : ?>

                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="provinsi" id="provinsi">
                                    <option value="">Provinsi</option>

                                    <?php foreach ($this->session->userdata('master_provinsi') as $provinsi) : ?>
                                        <option value="<?= $provinsi['daerah_id'] ?>" <?= ($provinsi['daerah_id'] == (!empty($this->session->userdata('filter_pasar')['filter_search_provinsi']) ? $this->session->userdata('filter_pasar')['filter_search_provinsi'] : '')) ? 'selected' : '' ?>><?= $provinsi['provinsi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="kabupaten" id="kabupaten">
                                    <option value="">Kabupaten/Kota</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                <input type="hidden" id="filter-search-kabupaten-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_kabupaten']) ? $this->session->userdata('filter_pasar')['filter_search_kabupaten'] : '' ?>">
                            </div>
                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                    <option value="">Pengelola</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                                <input type="hidden" id="filter-search-pengelola-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_pengelola']) ? $this->session->userdata('filter_pasar')['filter_search_pengelola'] : '' ?>">
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'dinas provinsi') : ?>
                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                    <option value="">Pengelola</option>

                                    <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                        <option value="<?= $pp['email'] ?>"><?= $pp['nama_lengkap'] ?></option>
                                    <?php endforeach; ?>

                                </select>
                                <input type="hidden" id="filter-search-pengelola-flash" value="<?= !empty($this->session->userdata('filter_pasar')['filter_search_pengelola']) ? $this->session->userdata('filter_pasar')['filter_search_pengelola'] : '' ?>">
                            </div>
                        <?php endif; ?>

                        <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'dinas kabupaten/kota') : ?>
                            <div class="col-3">
                                <select class="mb-3" aria-label="Default select example" name="pengelola" id="pengelola">
                                    <option value="">Pengelola</option>

                                    <?php foreach ($this->session->userdata('pengelola_pasar') as $pp) : ?>
                                        <option value="<?= $pp['email'] ?>"><?= $pp['nama_lengkap'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif; ?>

                        <div class="col-3">
                            <select class="mb-3" aria-label="Default select example" name="tipe_pasar" id="tipe_pasar">
                                <option value="">Semua Tipe</option>

                                <?php foreach ($this->session->userdata('tipe_pasar') as $key => $row) : ?>
                                    <option value="<?= $row['tipe_pasar_id'] ?>" <?= ($row['tipe_pasar_id'] == (!empty($this->session->userdata('filter_pasar')['filter_search_tipe']) ? $this->session->userdata('filter_pasar')['filter_search_tipe'] : '')) ? 'selected' : '' ?>><?= $row['tipe_pasar'] ?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                        <div class="col-3">
                            <select class="mb-3" aria-label="Default select example" name="bentuk" id="bentuk">
                                <option value="">Semua Bentuk</option>

                                <option value="1" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_bentuk']) ? $this->session->userdata('filter_pasar')['filter_search_bentuk'] : '') == 1) ? 'selected' : '' ?>>Permanen</option>
                                <option value="2" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_bentuk']) ? $this->session->userdata('filter_pasar')['filter_search_bentuk'] : '') == 2) ? 'selected' : '' ?>>Semi Permanen</option>
                                <option value="3" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_bentuk']) ? $this->session->userdata('filter_pasar')['filter_search_bentuk'] : '') == 3) ? 'selected' : '' ?>>Tanpa Bangunan</option>

                            </select>
                        </div>
                        <div class="col-3">
                            <select class="mb-3" aria-label="Default select example" name="kepemilikan" id="kepemilikan">
                                <option value="">Semua Kepemilikan</option>

                                <option value="1" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kepemilikan']) ? $this->session->userdata('filter_pasar')['filter_search_kepemilikan'] : '') == 1) ? 'selected' : '' ?>>Pemerintah Daerah</option>
                                <option value="2" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kepemilikan']) ? $this->session->userdata('filter_pasar')['filter_search_kepemilikan'] : '') == 2) ? 'selected' : '' ?>>Desa Adat</option>
                                <option value="3" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kepemilikan']) ? $this->session->userdata('filter_pasar')['filter_search_kepemilikan'] : '') == 3) ? 'selected' : '' ?>>Swasta</option>

                            </select>
                        </div>
                        <div class="col-3">
                            <select class="mb-3" aria-label="Default select example" name="kondisi" id="kondisi">
                                <option value="">Semua Kondisi</option>

                                <option value="1" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kondisi']) ? $this->session->userdata('filter_pasar')['filter_search_kondisi'] : '') == 1) ? 'selected' : '' ?>>Baik</option>
                                <option value="2" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kondisi']) ? $this->session->userdata('filter_pasar')['filter_search_kondisi'] : '') == 2) ? 'selected' : '' ?>>Rusak Berat</option>
                                <option value="3" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kondisi']) ? $this->session->userdata('filter_pasar')['filter_search_kondisi'] : '') == 3) ? 'selected' : '' ?>>Rusak Ringan</option>
                                <option value="4" <?= ((!empty($this->session->userdata('filter_pasar')['filter_search_kondisi']) ? $this->session->userdata('filter_pasar')['filter_search_kondisi'] : '') == 4) ? 'selected' : '' ?>>Rusak Sedang</option>

                            </select>
                        </div>

                        <div class="col-3">
                            <button type="submit" name="submit" value="submit" class="btn btn-primary btn-filter">
                                <i class="ti ti-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="row row-cols-12 row-cols-sm-12 row-cols-md-12 row-cols-xl-12 row-cols-xxl-12 animate__animated animate__slideInUp">

    <div class="col-12 d-flex">
        <div class="card radius-10 w-100">
            <div class="card-body">
                <div class="row">
                    <?php if (isset($pasar[$page]) && $pasar[$page]['data'] != null) : ?>
                        <?php foreach ($pasar[$page]['data'] as $key => $p) : ?>
                            <div class="col-md-4">
                                <div class="card-body">
                                    <div class="card-group">
                                        <div class="card border-end">
                                            <img src="<?= ($p['foto_depan'] != null) ? $p['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" class="card-img-top" alt="<?= $p['nama'] ?>">
                                            <div class="card-body">
                                                <h5 class="card-title"><?= $p['nama'] ?></h5>
                                                <p class="card-text"><?= $p['alamat'] ?></p>
                                                <p class="card-text"><?= $p['kab_kota'] ?>, <?= $p['provinsi'] ?>, <?= $p['kode_pos'] ?></p>
                                                <a href="<?= base_url() ?>administrator/pasar/detail/<?= $p['pasar_id'] ?>" class="btn btn-primary">Lihat Selengkapnya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

            </div>

        </div>

    </div>

    <div class="col-12">
        <?= (isset($pagination)) ? $pagination : ''  ?>
    </div>

</div>