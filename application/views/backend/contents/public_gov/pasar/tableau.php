<div class="row row-cols-12 row-cols-sm-12 row-cols-md-12 row-cols-xl-12 row-cols-xxl-12">

    <div class="col-12 d-flex">
        <div class="card radius-10 w-100">
            <div class="card-body">
                <iframe src="https://analitik.kemendag.go.id/trusted/<?= $token . $view_tableau ?>?<?= $urlparams ?>" width="100%" height="900px" frameborder="0">
                </iframe>
            </div>
        </div>
    </div>
</div>