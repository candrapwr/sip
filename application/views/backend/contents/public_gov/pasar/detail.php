<style>
    .main .container {
        display: grid;
        grid-template-columns: repeat(3, 1fr);
        grid-gap: 1rem;
        justify-content: center;
        align-items: center;
    }

    .main .card {
        color: #252a32;
        border-radius: 2px;
        background: #ffffff;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 3px rgba(0, 0, 0, 0.24);
    }

    .main .card-image {
        position: relative;
        display: block;
        width: 100%;
        padding-top: 70%;
        background: #ffffff;
    }

    .main .card-image img {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    @media only screen and (max-width: 600px) {
        .main .container {
            display: grid;
            grid-template-columns: 1fr;
            grid-gap: 1rem;
        }
    }
</style>


<!--breadcrumb-->
<div class="page-breadcrumb d-none d-sm-flex align-items-center">
    <div class="breadcrumb-title pe-3 text-white">Detail Pasar</div>
    <div class="ps-3">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0 p-0">
                <li class="breadcrumb-item"><a href="javascript:;"><i class="fadeIn animated bx bx-shopping-bag text-white"></i></a>
                </li>
                <li class="breadcrumb-item active text-white" aria-current="page"><?= $index_pasar['nama'] ?></li>
            </ol>
        </nav>
    </div>
    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
        <div class="ms-auto">
            <div class="dropdown">
                <button class="btn btn-light dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fadeIn animated bx bx-cog me-1"></i> Pengaturan</button>
                <ul class="dropdown-menu">
                    <li><button class="dropdown-item" data-bs-toggle="modal" data-id="<?= $active_index ?>" id="update-pasar"><i class="fadeIn animated bx bx-edit"></i> Edit Info</button>
                    </li>
                    <li><button class="dropdown-item" id="update-detail" data-id="<?= $active_index ?>" data-bs-toggle="modal"><i class="fadeIn animated bx bx-edit"></i> Edit Detail</button>
                    </li>
                    <!-- <li><a class="dropdown-item text-danger" href="<?= site_url() ?>pasar/delete/<?= $index_pasar['pasar_id'] ?>"><i class="fadeIn animated bx bx-trash"></i> Hapus</a>
                    </li> -->
                </ul>
            </div>
        </div>
    <?php endif; ?>
</div>
<!--end breadcrumb-->

<div class="profile-cover bg-dark"></div>

<div class="row">
    <div class="col-12 col-lg-8">
        <div class="card shadow-sm border-0">
            <div class="card-body">
                <input type="hidden" id="action_index" value="<?= $active_index ?>">
                <input type="hidden" id="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">

                <span class="mb-0"><i class="fadeIn animated bx bx-data me-1"></i> Informasi Data</span>
                <hr>
                <ul class="nav nav-tabs nav-primary" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" data-bs-toggle="tab" href="#tab_1" role="tab" aria-selected="true">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class='fadeIn animated bx bx-money font-18 me-1'></i>
                                </div>
                                <div class="tab-title">Harga</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-bs-toggle="tab" href="#tab_2" role="tab" aria-selected="false">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class='fadeIn animated bx bx-store font-18 me-1'></i>
                                </div>
                                <div class="tab-title">Kios</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-bs-toggle="tab" href="#tab_3" role="tab" aria-selected="false">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class='fadeIn animated bx bx-coin-stack font-18 me-1'></i>
                                </div>
                                <div class="tab-title">Anggaran</div>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" data-bs-toggle="tab" href="#tab_4" role="tab" aria-selected="false">
                            <div class="d-flex align-items-center">
                                <div class="tab-icon"><i class='fadeIn animated bx bx-images font-18 me-1'></i>
                                </div>
                                <div class="tab-title">Foto Kios</div>
                            </div>
                        </a>
                    </li>
                </ul>
                <div class="tab-content py-3">
                    <div class="tab-pane fade show active" id="tab_1" role="tabpanel">
                        <div class="row align-items-center g-3 mt-2 mb-2">
                            <div class="col-12 col-lg-6">
                                <h5 class="mb-0">Data Harga Komoditi</h5>
                                <p id="title-date-harga"></p>
                            </div>

                            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas' || strtolower($this->session->userdata('role')) == 'teknis pdn') : ?>
                                <div class="col-12 col-lg-6 text-md-end">
                                    <a id="btn_filter2" class="btn btn-sm btn-secondary me-1"><i class="ti ti-search"></i></a>

                                    <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
                                        <button class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#mdl_harga"><<i class="ti ti-plus me-1"></i>Tambah</button>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div id="box_filter2">
                            <div class="row">
                                <div class="card timbul">
                                    <div class="card-body">
                                        <div class="border p-4 rounded">
                                            <div class="row mb-3">
                                                <label class="col-sm-3 col-form-label">Tanggal</label>
                                                <div class="col-sm-9">
                                                    <input type="date" value="<?= date('Y-m-d') ?>" id="filter-harga-komoditi-date1" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <label class="col-sm-3 col-form-label"></label>
                                                <div class="col-sm-9">
                                                    <button style="font-size: 14px;" type="submit" id="filter-harga-komoditi" class="btn btn-primary px-5"><i class="ti ti-search me-1"></i>Cari</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="table-responsive" data-aos="fade-up">
                            <table id="tbl_harga" class="table table-striped table-bordered" style="width:100%"></table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_2" role="tabpanel">
                        <div class="row align-items-center g-3 mt-2 mb-2">
                            <div class="col-12 col-lg-6">
                                <h5 class="mb-0">Data Kios</h5>
                            </div>

                            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
                                <div class="col-12 col-lg-6 text-md-end">
                                    <button class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#mdl_kios"><<i class="ti ti-plus me-1"></i>Tambah</button>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <div class="table-responsive" data-aos="fade-up">
                                <table id="tbl_kios" class="table table-striped table-bordered" style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_3" role="tabpanel">
                        <div class="row align-items-center g-3 mt-2 mb-2">
                            <div class="col-12 col-lg-6">
                                <h5 class="mb-0">Data Anggaran</h5>
                            </div>

                            <?php if (strtolower($this->session->userdata('role')) == 'pengelola/petugas') : ?>
                                <div class="col-12 col-lg-6 text-md-end">
                                    <button class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#mdl_anggaran"><<i class="ti ti-plus me-1"></i>Tambah</button>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="row">
                            <div class="table-responsive" data-aos="fade-up">
                                <table id="tbl_anggaran" class="table table-striped table-bordered" style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab_4" role="tabpanel">

                        <div class="row">
                            <main class="main">
                                <div class="container">
                                    <div class="card timbul">
                                        <div class="card-image">
                                            <a href="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" data-fancybox="gallery" data-caption="Caption Images 1">
                                                <img src="<?= ($index_pasar['foto_depan'] != null) ? $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" alt="Foto Depan">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="card timbul">
                                        <div class="card-image">
                                            <a href="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" data-fancybox="gallery" data-caption="Caption Images 1">
                                                <img src="<?= ($index_pasar['foto_dalam'] != null) ? $index_pasar['foto_dalam'] : base_url('assets/frontend/img/default-market.svg') ?>" alt="Foto Dalam">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </main>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-4">
        <div class="card shadow-sm border-0 overflow-hidden">
            <div class="card-body">
                <div class="profile-avatar text-center">
                    <img src="<?= ($index_pasar['foto_depan'] != null) ? base_url() . 'services/res/upload/pasar/2021/' . $index_pasar['foto_depan'] : base_url('assets/frontend/img/default-market.svg') ?>" class="rounded-circle shadow" width="120" height="120" alt="">
                </div>

                <div class="text-center mt-4">
                    <h4 class="mb-1"><?= $index_pasar['nama'] ?></h4>
                    <p class="mb-0 text-secondary"></p>
                    <div class="mt-4"></div>
                    <h6 class="mb-1"><i class="ti ti-map-pin me-1"></i>Lokasi</h6>
                    <p class="mb-0 text-secondary"><?= $index_pasar['kecamatan'] ?>, <?= $index_pasar['kab_kota'] ?>, <?= $index_pasar['provinsi'] ?>, <?= $index_pasar['kode_pos'] ?></p>
                </div>
                <hr>
                <div class="text-start">
                    <h5><i class="fadeIn animated bx bx-store me-1"></i> Tentang</h5>
                    <p class="mb-0 text-justify"><?= $index_pasar['deskripsi'] ?></p>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent border-top">
                    <span class="fw-bold">Pengelola</span>
                    <span><?= ($detail_pasar != null) ? $detail_pasar[0]['pengelola_pasar'] : '-' ?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent border-top">
                    <span class="fw-bold">Jenis Pasar</span>
                    <span><?= ($detail_pasar != null && $detail_pasar[0]['jenis_pasar'] != null) ? $detail_pasar[0]['jenis_pasar'] : '-' ?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                    <span class="fw-bold"> Tipe Pasar </span>
                    <span><?= ($detail_pasar != null && $detail_pasar[0]['tipe_pasar'] != null) ? $detail_pasar[0]['tipe_pasar'] : '-' ?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                    <span class="fw-bold"> Bentuk Pasar </span>
                    <span><?= ($detail_pasar != null && $detail_pasar[0]['bentuk_pasar'] != null) ? $detail_pasar[0]['bentuk_pasar'] : '-' ?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                    <span class="fw-bold"> Kepemilikan </span>
                    <span><?= ($detail_pasar != null && $detail_pasar[0]['kepemilikan'] != null) ? $detail_pasar[0]['kepemilikan'] : '-' ?></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center bg-transparent">
                    <span class="fw-bold"> Kondisi </span>
                    <span><?= ($detail_pasar != null && $detail_pasar[0]['kondisi'] != null) ? $detail_pasar[0]['kondisi'] : '-' ?></span>
                </li>
            </ul>
        </div>
        <div class="card shadow-sm border-0 overflow-hidden">
            <div class="card-body">
                <div class="d-flex align-items-center mb-4">
                    <div>
                        <span class="mb-0"><i class="ti ti-info-circle me-1"></i>Detail Lainnya</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Waktu Operasional</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? $detail_pasar[0]['waktu_operasional'] : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Jam Sibuk</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? ($detail_pasar[0]['jam_sibuk_awal'] != null ? date('H:i', strtotime($detail_pasar[0]['jam_sibuk_awal'])) : '-') : '-' ?> - <?= ($detail_pasar != null) ? ($detail_pasar[0]['jam_sibuk_akhir'] != null ? date('H:i', strtotime($detail_pasar[0]['jam_sibuk_akhir'])) : '-') : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Jumlah Pekerja Tetap</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_pekerja_tetap']) : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Jumlah Pekerja Non-Tetap</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_pekerja_nontetap']) : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Luas Bangunan</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['luas_bangunan']) : '-' ?> m2</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Luas Tanah</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['luas_tanah']) : '-' ?> m2</span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Jumlah Lantai</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? rupiah($detail_pasar[0]['jumlah_lantai']) : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Tahun Dibangun</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? $detail_pasar[0]['tahun_bangun'] : '-' ?></span>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-3">
                    <div>
                        <p class="mb-0 fw-bold">Tahun Renovasi</p>
                    </div>
                    <div class="ms-auto">
                        <span class="mb-0"><?= ($detail_pasar != null) ? $detail_pasar[0]['tahun_renovasi'] : '-' ?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end row-->

<!-- Modal Edit Pasar Basic-->
<div class="modal fade" id="modalpasaredit" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Edit Informasi Pasar</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="default-form" id="form-data-update">
                <div class="modal-body">
                    <div class="row g-3">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_update_index" id="pasar_update_index">
                        <input type="hidden" name="pasar_id_updatee" id="pasar_id_updatee">
                        <input type="hidden" name="daerah_id_update" id="daerah_id_update">
                        <input type="hidden" name="uri_type" id="uri_type" value="<?= $uri_segment ?>">
                        <div class="col-12">
                            <label class="form-label">Nama Pasar</label>
                            <input type="text" class="form-control" id="name_update" name="name_update" placeholder="Masukkan Nama Lengkap Pasar...">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Deskripsi</label>
                            <textarea class="form-control" id="description_update" name="description_update" placeholder="Masukkan Deskripsi Lengkap..."></textarea>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Alamat</label>
                            <textarea class="form-control" id="address_update" name="address_update" placeholder="Masukkan Alamat Pasar..."></textarea>
                        </div>
                        <input type="hidden" id="latitude_update" name="latitude_update">
                        <input type="hidden" id="longitude_update" name="longitude_update">
                        <div class="col-12">
                            <label class="form-label">Kode Pos</label>
                            <input class="form-control" type="number" id="post_code_update" name="post_code_update" placeholder="Masukkan Kode Pos...">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><<i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit Pasar Advanced-->
<div class="modal fade" id="mdl_pasar_advanced" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Edit Informasi Lengkap Pasar</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form class="needs-validation default-form" id="form-data-detail" novalidate>
                <div class="modal-body">
                    <div class="row g-3">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="pasar_nama" value="<?= $index_pasar['nama'] ?>">
                        <input type="hidden" name="pasar_detail" value="<?= ($detail_pasar != null) ? $detail_pasar[0]['pasar_detail_id'] : '' ?>">
                        <div class="col-6">
                            <label class="form-label">No. Telepon</label>
                            <select aria-label="Jenis Komoditi" name="tipe_pasar" id="tipe_pasar">
                                <option selected="" value="">Tipe Pasar</option>
                                <?php foreach ($tipe_pasar as $tp) : ?>
                                    <option value="<?= $tp['tipe_pasar_id'] ?>"><?= $tp['tipe_pasar'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label">No. Fax</label>
                            <select aria-label="Varian Komoditi" name="bentuk_pasar" id="bentuk_pasar">
                                <option selected="" value="">Bentuk Pasar</option>
                                <option value="1">Permanen</option>
                                <option value="2">Semi Permanen</option>
                                <option value="3">Tanpa Bangungan</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Tipe</label>
                            <select aria-label="Satuan Komoditi" name="kepemilikan" id="kepemilikan">
                                <option selected="" value="">Kepemilikan</option>
                                <option value="1">Pemerintah Daerah</option>
                                <option value="2">Desa Adat</option>
                                <option value="3">Swasta</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Bentuk</label>
                            <select aria-label="Satuan Komoditi" name="kondisi" id="kondisi">
                                <option selected="" value="">Kondisi</option>
                                <option value="1">Baik</option>
                                <option value="2">Rusak Berat</option>
                                <option value="3">Rusak Ringan</option>
                                <option value="4">Rusak Sedang</option>
                            </select>
                        </div>
                        <div class="col-12">
                            <label class="form-label">Pengelola</label>
                            <select aria-label="Pengelola Pasar" name="pengelola" id="pengelola">
                                <option selected="" value="">Pengelola Pasar</option>
                                <option value="1">Pemerintah</option>
                                <option value="2">BUMD</option>
                                <option value="3">Swasta</option>
                            </select>
                        </div>

                        <div class="col-12">
                            <label class="form-label">Waktu Operasional</label>
                            <select aria-label="Waktu Operasional" name="waktu_operasional[]" id="waktu_operasional" multiple="multiple">
                                <option selected="" value="">Waktu Operasional</option>
                                <option value="1">Senin</option>
                                <option value="2">Selasa</option>
                                <option value="3">Rabu</option>
                                <option value="4">Kamis</option>
                                <option value="5">Jumat</option>
                                <option value="6">Sabtu</option>
                                <option value="7">Minggu</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jam Buka</label>
                            <input type="text" class="timepicker form-control datepicker" name="jam_buka" id="jam_buka" placeholder="Jam Buka" data-theme="light">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jam Tutup</label>
                            <input type="text" class="timepicker form-control datepicker" name="jam_tutup" id="jam_tutup" placeholder="Jam Tutup" data-theme="light">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jam Mulai Sibuk</label>
                            <input type="text" class="timepicker form-control datepicker" name="jam_sibuk_awal" id="jam_sibuk_awal" placeholder="Jam Mulai Sibuk" data-theme="light">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jam Akhir Sibuk</label>
                            <input type="text" class="timepicker form-control datepicker" name="jam_sibuk_akhir" id="jam_sibuk_akhir" placeholder="Jam Berakhir Sibuk" data-theme="light">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jum. Pekerja Tetap</label>
                            <input type="text" class="form-control price" name="pekerja_tetap" id="pekerja_tetap" placeholder="Masukkan Pekerja Tetap..." aria-label="Jumlah Pekerja Tetap">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Jum. Pekerja Non Tetap</label>
                            <input type="text" class="form-control price" name="pekerja_nontetap" id="pekerja_nontetap" placeholder="Masukkan Pekerja Non Tetap..." aria-label="Jumlah Pekerja Non Tetap">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Luas Bangunan (m2)</label>
                            <input type="text" class="form-control price" name="luas_bangunan" id="luas_bangunan" placeholder="Masukkan Luas Bangunan..." aria-label="Harga">
                        </div>
                        <div class="col-6">
                            <label class="form-label">Luas Tanah (m2)</label>
                            <input type="text" class="form-control price" name="luas_tanah" id="luas_tanah" placeholder="Masukkan Luas Tanah..." aria-label="Harga">
                        </div>

                        <div class="col-4">
                            <label class="form-label">Jum. Lantai</label>
                            <input type="text" class="form-control price" name="jumlah_lantai" id="jumlah_lantai" placeholder="Masukkan Jumlah Lantai..." aria-label="Jumlah Lantai">
                        </div>
                        <div class="col-4">
                            <label class="form-label">Tahun Dibangun</label>
                            <input type="text" class="form-control" name="tahun_bangun" id="tahun_bangun" placeholder="Masukkan Tahun Dibangun..." aria-label="Tahun Dibangun">
                        </div>
                        <div class="col-4">
                            <label class="form-label">Tahun Renovasi</label>
                            <input type="text" class="form-control" name="tahun_renovasi" id="tahun_renovasi" placeholder="Masukkan Tahun Revonasi..." aria-label="Tahun Renovasi">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><<i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Harga-->
<div class="modal fade" id="mdl_harga" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah/Edit Harga</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-data-harga" class="needs-validation default-form" novalidate>
                <div class="modal-body">
                    <div class="row g-3">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="harga_komoditi_id" id="harga_komoditi_id">

                        <div class="col col-12">
                            <label class="form-label">Nama Pasar</label>
                            <input type="text" class="form-control" id="name" name="name" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                        </div>
                        <div class="col col-6">
                            <div class="form-group">
                                <label class="form-label">Jenis Komoditi</label>
                                <select id="jenis_komoditi" name="jenis_komoditi">
                                    <option selected="" value="">Jenis Komoditi</option>
                                    <?php foreach ($jenis_komoditi as $jk) : ?>
                                        <option value="<?= $jk['jenis_komoditi_id'] ?>|<?= $jk['jenis_komoditi'] ?>"><?= $jk['jenis_komoditi'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col col-6">
                            <div class="form-group">
                                <label class="form-label">Varian</label>
                                <select id="varian_komoditi" name="varian_komoditi">
                                    <option selected="" value="">Varian</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-6">
                            <div class="form-group">
                                <label class="form-label">Harga</label>
                                <input type="text" name="harga" id="harga" class="form-control price" placeholder="Harga" aria-label="Harga">
                            </div>
                        </div>
                        <div class="col col-6">
                            <div class="form-group">
                                <label class="form-label">Satuan</label>
                                <select id="satuan_komoditi" name="satuan_komoditi">
                                    <option selected="" value="">Satuan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><<i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Kios-->
<div class="modal fade" id="mdl_kios" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah/Edit Kios</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-data-kios" class="needs-validation default-form" novalidate>
                <div class="modal-body">
                    <div class="row g-3">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_bangunan_id" id="pasar_bangunan_id">

                        <div class="col-12">
                            <label class="form-label">Nama Pasar</label>

                            <input type="text" class="form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Masukkan Nama Pasar..." aria-label="Nama Pasar" readonly="readonly">
                        </div>
                        <div class="col-12">
                            <label class="form-label">Jenis Bangunan</label>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="1" checked="checked">
                                        <label class="form-check-label" for="los">
                                            Los
                                        </label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="2">
                                        <label class="form-check-label" for="kios">
                                            Kios
                                        </label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="jenis_bangunan" id="jenis_bangunan" value="3">
                                        <label class="form-check-label" for="dasaran">
                                            Dasaran
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col col-6">
                            <label class="form-label">Jumlah Bangunan</label>
                            <input type="text" class="form-control price" name="jumlah_bangunan" id="jumlah_bangunan" placeholder="Jumlah Bangunan" aria-label="Jumlah Bangunan">
                        </div>
                        <div class="col col-6">
                            <label class="form-label">Jumlah Pedagang</label>
                            <input type="text" class="form-control price" name="jumlah_pedagang" id="jumlah_pedagang" placeholder="Jumlah Pedagang" aria-label="Jumlah Pedagang">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><<i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Anggaran -->
<div class="modal fade" id="mdl_anggaran" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><<i class="ti ti-edit-circle me-1"></i>Tambah/Edit Anggaran</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-data-anggaran" class="needs-validation default-form" novalidate>
                <div class="modal-body">
                    <div class="row g-3">
                        <input type="hidden" name="pasar_id" value="<?= $index_pasar['pasar_id'] ?>">
                        <input type="hidden" name="csrf_baseben" value="<?= $this->security->get_csrf_hash() ?>">
                        <input type="hidden" name="pasar_anggaran_id" id="pasar_anggaran_id">

                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-label">Nama Pasar</label>
                                <input type="text" class="form-control" name="nama" value="<?= $index_pasar['nama'] ?>" placeholder="Nama Pasar" aria-label="Nama Pasar" readonly="readonly">
                            </div>
                        </div>
                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-label">Program Pasar</label>
                                <select aria-label="Jenis Komoditi" name="program_pasar" id="program_pasar">
                                    <option selected="" value="">Program Pasar</option>

                                    <?php foreach ($program_pasar as $pp) : ?>
                                        <option value="<?= $pp['program_pasar_id'] ?>"><?= $pp['program_pasar'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-label">Tahun</label>
                                <input type="text" class="form-control" name="tahun_anggaran" id="tahun_anggaran" placeholder="Tahun" aria-label="Tahun">
                            </div>
                        </div>
                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-label">Omzet</label>
                                <input type="text" class="form-control price" placeholder="Omzet" name="omset_anggaran" id="omset_anggaran" aria-label="Omzet">
                            </div>
                        </div>
                        <div class="col col-12">
                            <div class="form-group">
                                <label class="form-label">Jumlah Anggaran</label>
                                <input type="text" class="form-control price" placeholder="Jumlah Anggaran" name="jumlah_anggaran" id="jumlah_anggaran" aria-label="Jumlah Anggaran">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><<i class="ti ti-x me-1"></i>Tutup</button>
                    <button type="submit" class="btn btn-primary"><i class="ti ti-device-floppy me-1"></i>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        $("#btn_filter2").click(function() {
            $("#box_filter2").slideToggle();
        });

        // Fancybox Configuration
        $('[data-fancybox="gallery"]').fancybox({
            buttons: [
                "slideShow",
                "thumbs",
                "zoom",
                "fullScreen",
                "share",
                "close"
            ],
            loop: false,
            protect: true
        });

    });
</script>