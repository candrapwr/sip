<link rel="stylesheet" href="<?= base_url() ?>assets/backend/libs/jquery-steps/jquery.steps.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/backend/libs/pdsi-upload/pdsi.upload.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="https://unpkg.com/leaflet@1.0.3/dist/leaflet.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.markercluster@1.5.3/dist/MarkerCluster.Default.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/leaflet.fullscreen@2.4.0/Control.FullScreen.css" rel="stylesheet" type="text/css" />

<style>
    .custom .form-check-input {
        width: 1.4em;
        height: 1.4em;
        margin-right: 10px;
    }

    .custom label {
        margin-bottom: 0px;
        font-size: 14px;
        font-weight: 700;
    }

    #map {
        position: relative;
        min-height: 300px;
        z-index: 1 !important;
        border-radius: 15px;
    }

    #weathermap {
        background: #fff;
        padding: 15px;
        border-radius: 15px;
    }

    .jum {
        background: #fff;
        padding: 10px;
        border-radius: 15px;
    }
</style>