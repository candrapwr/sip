<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = 'home/buntu';
$route['translate_uri_dashes'] = FALSE;

$route['download/(:any)']['get'] = 'dashboard/downloadAsset/$1';
$route['download/(:any)/(:any)']['get'] = 'dashboard/downloadAsset/$1/$2';
$route['download/(:any)/(:any)/(:any)']['get'] = 'dashboard/downloadAsset/$1/$2/$3';
$route['download/(:any)/(:any)/(:any)/(:any)']['get'] = 'dashboard/downloadAsset/$1/$2/$3/$4';

$route['public_download/(:any)']['get'] = 'auth/downloadAsset/$1';
$route['public_download/(:any)/(:any)']['get'] = 'auth/downloadAsset/$1/$2';
$route['public_download/(:any)/(:any)/(:any)']['get'] = 'auth/downloadAsset/$1/$2/$3';
$route['public_download/(:any)/(:any)/(:any)/(:any)']['get'] = 'auth/downloadAsset/$1/$2/$3/$4';

// Auth
$route['user/sign_in'] = 'auth/loginForm';
$route['user/sign_out'] = 'auth/logout';

// Dashboard Page
$route['register'] = 'auth/registerForm';
$route['terimaekomplain'] = 'home/terimaekomplain';
$route['account/verify/(:any)'] = 'auth/verify/$1';
$route['account/token/resend/(:any)'] = 'auth/regenerateToken/$1';
$route['sebaran-pasar'] = 'public_government/pasar/sebaran';

$route['kab/json']['post'] = 'daerah/getKabupaten';
$route['kec/json']['post'] = 'daerah/getKecamatan';
$route['master/kab/json'] = 'daerah/getKabupaten1';
$route['master/kec/json'] = 'daerah/getKecamatan1';
$route['master/daerah'] = 'master/getDaerah';

$route['ewskab/json'] = 'daerah/getEWSKabupaten';

$route['pasar/add'] = 'pasar/save';
$route['pasar/update'] = 'pasar/update';
$route['pasar/json']['post'] = 'public/pasar/getPasarJSON';
$route['pasar/delete/(:num)'] = 'pasar/deletePasar/$1';
$route['pasar/detail/add'] = 'pasar/saveDetail';
$route['pasar/detail/json'] = 'pasar/getDetailJSON';
$route['pasar/kios/json'] = 'pasar/getBangunan';
$route['pasar/kios/add'] = 'pasar/saveKios';
$route['pasar/kios/delete'] = 'pasar/deleteKios';
$route['pasar/anggaran/json'] = 'pasar/getAnggaran';
$route['pasar/anggaran/add'] = 'pasar/saveAnggaran';
$route['pasar/anggaran/delete'] = 'pasar/deleteAnggaran';
$route['pasar/pengelola/json'] = 'pasar/getPengelolaJSON';
$route['pasar/sum-tpdak/json'] = 'dashboard/getSummaryProgramJSON';
$route['sebaran-pasar/detail/(:num)'] = 'pasar/detail/$1';
$route['harga/json'] = 'harga/getHargaJSON';
$route['harga/add'] = 'harga/save';
$route['pasar/harga/json'] = 'harga/getHargaJSON';
$route['pasar/harga/delete'] = 'harga/deleteHarga';
$route['harga/bahan-pokok/json'] = 'harga/getHargaBahanPokok';

$route['komoditi/varian/json'] = 'pasar/getVarianKomoditi';
$route['komoditi/satuan/json'] = 'pasar/getSatuanKomoditi';

$route['tableau'] = 'dashboard/tableau';

$route['dashboard/e-komplain/detail/(:any)'] = 'public_government/complaint/detail/$1';
$route['dashboard/e-komplain/detail/tracking/json'] = 'public_government/complaint/getTracking';
$route['dashboard/e-komplain/reply'] = 'public_government/complaint/save';

// Public Routes
$route['pasar-mgcr'] = 'public/pasar/sebaran_bps';
$route['migor-14rb'] = 'public/pasar/sebaran_pujle';
$route['migor-simirah-14rb'] = 'public/pasar/sebaran_simirah';
$route['public/bahan-pokok/json']['post'] = '/public/harga/getHargaBahanPokok';
$route['public/bahan-pokok-ews/json']['post'] = '/public/harga/getHargaBahanPokokEWS';
$route['public/markets/json']['post'] = 'public/complaint/getPasarJSON';
$route['e-komplain'] = 'public/complaint';
$route['e-komplain/search'] = 'public/complaint/search';
$route['e-komplain/search/(:any)'] = 'public/complaint/generateTicket/$1';
$route['public/e-komplain/detail/tracking/json'] = 'public/complaint/getTracking';
$route['pasar/p/(:any)'] = 'public/pasar/detail/$1';
$route['public/pasar/kios/json'] = 'public/pasar/getKiosJSON';
$route['public/e-komplain/reply'] = 'public/complaint/save';
$route['kebijakan-privasi'] = 'home/kebijakan_privasi';

$route['perdagangan/provinsi'] = 'komoditas/index';
$route['perdagangan/provinsi/json'] = 'komoditas/getCommodityJSON';
$route['perdagangan/provinsi/add'] = 'komoditas/save';
$route['perdagangan/monitoring'] = 'dashboard/perdaganganmonitoring';

// dashboard page
$route['dashboard']   = 'public_government/dashboard/index';
$route['dashboard/pasar/update'] = 'public_government/pasar/update';
$route['dashboard/pasar/json'] = 'public_government/pasar/getPasarJSON';
$route['dashboard/pasar/detail/(:any)'] = 'public_government/pasar/detail/$1';
$route['dashboard/pasar/save-detail'] = 'public_government/pasar/saveDetail';
$route['dashboard/pasar/get-detail/json'] = 'public_government/pasar/getDetailJSON';
$route['dashboard/kab/json'] = 'daerah/getKabupaten';
$route['dashboard/harga/bahan-pokok/json'] = 'public_government/harga/getHargaBahanPokok';
$route['dashboard/pasar/sum-tpdak/json'] = 'public_government/dashboard/getSummaryProgramJSON';
$route['dashboard/pasar/pengelola/json'] = 'public_government/pasar/getPengelolaJSON';
$route['dashboard/sebaran-pasar/detail/(:num)'] = 'public_government/pasar/detail/$1';
$route['dashboard/harga/json'] = 'public_government/harga/getHargaJSON';
$route['dashboard/pasar/kios/json'] = 'public_government/pasar/getKiosJSON';
$route['dashboard/pasar/anggaran/json'] = 'public_government/pasar/getAnggaranJSON';
$route['dashboard/harga/delete'] = 'public_government/harga/deleteHarga';
$route['dashboard/kios/delete'] = 'public_government/pasar/deleteKios';
$route['dashboard/pasar/anggaran/delete'] = 'public_government/pasar/deleteAnggaran';
$route['dashboard/tableau'] = 'public_government/dashboard/tableau';
$route['dashboard/komoditi/varian/json'] = 'public_government/pasar/getVarianKomoditi';
$route['dashboard/komoditi/satuan/json'] = 'public_government/pasar/getSatuanKomoditi';
$route['dashboard/harga/add'] = 'public_government/harga/save';
$route['dashboard/detail/add'] = 'public_government/pasar/saveDetail';
$route['dashboard/pasar/kios/add'] = 'public_government/pasar/saveKios';
$route['dashboard/pasar/anggaran/add'] = 'public_government/pasar/saveAnggaran';
$route['dashboard/pasar/harga/json'] = 'public/pasar/getHargaJSON';
$route['dashboard/pasar/harga/delete'] = 'public_government/harga/deleteHarga';
$route['dashboard/pasar/kios/delete'] = 'public_government/pasar/deleteKios';
$route['dashboard/pasar/anggaran/delete'] = 'public_government/pasar/deleteAnggaran';
$route['dashboard/perdagangan'] = 'public_government/perdagangan';
$route['dashboard/perdagangan/provinsi/json'] = 'public_government/stok/getCommodityJSON';
$route['pasar/getBongkar_muat'] = 'public_government/pasar/getBongkar_muat';
$route['pasar/setBongkar_muat'] = 'public_government/pasar/saveBongkar_muat';
$route['pasar/deleteBongkar_muat'] = 'public_government/pasar/deleteBongkar_muat';
$route['pasar/savePasar_pengelola'] = 'public_government/pasar/savePasar_pengelola';
$route['pasar/savePaguyuban'] = 'public_government/pasar/savePaguyuban';
$route['pasar/getPasokan_bapok'] = 'public_government/pasar/getPasokan_bapok';
$route['pasar/savePasokan_bapok'] = 'public_government/pasar/savePasokan_bapok';
$route['pasar/getRegulasi'] = 'public/pasar/getRegulasi';
$route['pasar/saveRegulasi'] = 'public_government/pasar/saveRegulasi';
$route['pasar/update_data'] = 'public_government/pasar/update_data';

$route['dashboard/perdagangan/absensi']   = 'public_government/absensi/rekap';
$route['dashboard/perdagangan/absensi/rekap/json']   = 'public_government/absensi/rekapJSON';
$route['dashboard/perdagangan/provinsi/add'] = 'public_government/stok/save';
$route['dashboard/perdagangan/provinsi/delete'] = 'public_government/stok/delete';
$route['dashboard/perdagangan/kontributor']   = 'public_government/kontributor/index';
$route['dashboard/perdagangan/kontributor/json']   = 'public_government/kontributor/kontributor_json';
$route['dashboard/perdagangan/kontributor/assign_json']   = 'public_government/kontributor/assign_json';
$route['dashboard/perdagangan/kontributor/save']   = 'public_government/kontributor/save';
$route['dashboard/perdagangan/kontributor/approve']   = 'public_government/kontributor/approve';
$route['dashboard/perdagangan/absensi-harga']   = 'public_government/absensi/rekapHarga';
$route['dashboard/perdagangan/absensi/rekap-absensi-harga/json']   = 'public_government/absensi/rekapHargaJSON';
$route['dashboard/perdagangan/inputHarga']   = 'public_government/monitoring/inputHargaJSON';
$route['dashboard/summary/pasar-json'] = 'public_government/dashboard/getSummaryPasarJson';
$route['dashboard/summary/lapor-json'] = 'public_government/dashboard/getSummaryLaporHargaJson';
$route['dashboard/summary/pengelola-json'] = 'public_government/dashboard/getSummaryPengelolaJson';
$route['dashboard/summary/program-json'] = 'public_government/dashboard/getSumProgramJson';
$route['dashboard/ajax/pasar-dasboard'] = 'public_government/dashboard/getPasarDashboar';
$route['dashboard/pasar/save-foto'] = 'public_government/pasar/saveFoto';
$route['dashboard/pasar/del-foto'] = 'public_government/pasar/delFoto';
$route['dashboard/bahan-pokok-ews/json'] = 'public_government/harga/getHargaBahanPokokEWS';

//Monitoring

$route['dashboard/perdagangan/monitoring-ajax']   = 'public_government/monitoring/tabel_ajax';
$route['dashboard/perdagangan/monitoring-json/(:any)']   = 'public_government/monitoring/json/$1';

// Minyak Goreng

$route['perdagangan/migor/getPengecer']   = 'public_government/migor/getPengecer';
$route['perdagangan/migor/getTansaksicurah']   = 'public_government/migor/getTansaksicurah';
$route['perdagangan/migor/getProdusen']   = 'public_government/migor/getProdusen';

// cms page

$route['administrator/sebaran-pasar']   = 'pasar/sebaran';
$route['administrator/sebaran-pasar/detail/(:num)'] = 'pasar/detail/$1';
$route['administrator/harga-barang-pokok']   = 'harga/index';

$route['administrator/komoditas-provinsi'] = 'komoditas/index';

$route['administrator/absensi/rekap/json']   = 'absensi/rekapJSON';
$route['administrator/minyak-goreng']   = 'migor/index';
$route['web/pengguna-kontributor']   = 'kontributor/index';
$route['administrator/master/pengguna/nip']   = 'pengguna/searchNIP';
$route['administrator/pengguna/add']   = 'pengguna/save';


//------------------------------------- Back End -------------------------------------
$route['web/dashboard']   = 'dashboard/index';
$route['web/tableau'] = 'dashboard/tableau';
$route['web/switch-role/(:any)']['get']   = 'dashboard/switch_role/$1';

// DATA PASAR
$route['web/pasar']   = 'pasar/index';
$route['web/pasar/detail/(:any)'] = 'pasar/detail/$1';
$route['cu/pasar'] = 'pasar/save_identitas';
$route['web/pasar/(:num)/(:any)']   = 'pasar/detail/$1/$2';
$route['cu/pasar/detail'] = 'pasar/save_informasi';
$route['cu/pasar/bangunan'] = 'pasar/save_bangunan';
$route['cu/pasar/pengelola'] = 'pasar/save_pengelola';
$route['cu/pasar/harga'] = 'pasar/save_harga';
$route['cu/pasar/anggaran'] = 'pasar/save_anggaran';
$route['cu/pasar/bongkar_muat'] = 'pasar/saveBongkar_muat';
$route['cu/pasar/paguyuban'] = 'pasar/savePaguyuban';
$route['cu/pasar/regulasi'] = 'pasar/saveRegulasi';
$route['cu/pasar/pasokan_bapok'] = 'pasar/savePasokan_bapok';
$route['cu/pasar/foto'] = 'pasar/saveFoto';
$route['delete/pasar/foto'] = 'pasar/delFoto';
$route['web/pasar/lokasi']   = 'pasar/lokasi';
$route['web/pasar/export']   = 'pasar/rekap_pasar';
$route['web/absensi']   = 'absensi/rekap';

$route['web/pengguna']   = 'pengguna/index';
$route['web/menu']   = 'pengguna/permission';

$route['web/cpo-skema-pasal-18']   = 'public_government/migor/pe_pasal18';
$route['web/cpo-program-percepatan']   = 'public_government/migor/pe_percepatan';
$route['web/cpo-program-mgcr']   = 'public_government/migor/pe_mgcr';
$route['web/pe-cpo-inatrade']   = 'public_government/migor/pe_cpo_inatrade';
$route['web/produsen-minyak-goreng-curah']   = 'public_government/migor/produsen_curah';
$route['web/minyak-goreng-kemasan']   = 'public_government/migor/index';
$route['web/minyak-goreng-curah']   = 'public_government/migor/curah';
$route['web/transaksi-minyak-goreng-curah']   = 'public_government/migor/transaksi_curah';
$route['web/pengecer-minyak-goreng-curah']   = 'public_government/migor/pengecer_curah';
$route['web/monitoring-kelengkapanpasar']   = 'public_government/monitoring/pasar';
$route['web/monitoring-datapasar']   = 'public_government/monitoring/datapasar';
$route['web/monitoring-input-harga']   = 'public_government/monitoring/inputHarga';
$route['web/produksi-konsumsi'] = 'public_government/stok/produksi_konsumsi';
$route['web/monitoring-profil']   = 'public_government/monitoring/profil';
$route['web/monitoring-stat-bapok']   = 'public_government/monitoring/statbapok';
$route['web/monitoring-stat-bating']   = 'public_government/monitoring/statbating';
$route['web/monitoring-tabel']   = 'public_government/monitoring/index';
$route['web/monitoring-grafik']   = 'public_government/monitoring/grafik';
$route['web/monitoring-peta']   = 'public_government/monitoring/peta';
$route['web/pengguna-kontributor']   = 'public_government/kontributor/index';
$route['web/profil']   = 'profil';


//MASTER
$route['web/varian_komoditi']   = 'master/varian_komoditi';
$route['web/satuan_komoditi']   = 'master/satuan_komoditi';
$route['web/tipe-pengguna']   = 'master/tipe_pengguna';
$route['web/jenis-komoditi']   = 'master/jenis_komoditi';
$route['web/varian-komoditi']   = 'master/varian_komoditi';
$route['web/satuan-komoditi']   = 'master/satuan_komoditi';
$route['web/klasifikasi']   = 'master/klasifikasi';
$route['web/sarana-prasarana']   = 'master/sarana_prasarana';
$route['web/program-pasar']   = 'master/program_pasar';
$route['web/tipe-pasar']   = 'master/tipe_pasar';
$route['web/filter-harga']   = 'master/filter_harga';
$route['web/master/dokumen']['get']   = 'master/dokumen';
$route['web/master/dokumen/json']['post']   = 'master/getDokumen';
$route['web/master/dokumen']['post']   = 'master/cu_dokumen';


//SIMIRAH
$route['web/dashboard-simirah']   = 'public_government/migor/dashboard_simirah';
$route['web/dashboard-daerah']   = 'public_government/migor/dashboard_daerah';
$route['web/dashboard-dmo']   = 'public_government/migor/dashboard_dmo';
$route['perdagangan/migor/getExportirSIMIRAH']['post']   = 'public_government/migor/getExportirSIMIRAH';
$route['web/exportir-minyak-goreng-simirah']   = 'public_government/migor/exportir_simirah';
$route['perdagangan/migor/getProdusenSIMIRAH']['post']   = 'public_government/migor/getProdusenSIMIRAH';
$route['web/produsen-minyak-goreng-simirah']   = 'public_government/migor/produsen_simirah';
$route['perdagangan/migor/getD1SIMIRAH']['post']    = 'public_government/migor/getD1SIMIRAH';
$route['web/d1-minyak-goreng-simirah']   = 'public_government/migor/d1_simirah';
$route['perdagangan/migor/getD2SIMIRAH']['post']   = 'public_government/migor/getD2SIMIRAH';
$route['web/d2-minyak-goreng-simirah']   = 'public_government/migor/d2_simirah';
$route['perdagangan/migor/getPengecerSIMIRAH']['post']    = 'public_government/migor/getPengecerSIMIRAH';
$route['web/pengecer-minyak-goreng-simirah']   = 'public_government/migor/pengecer_simirah';

//SIMIRAH TRANSAKSI
$route['perdagangan/migor/getTrxCooperationSIMIRAH']['post']    = 'public_government/migor/getTrxCooperationSIMIRAH';
$route['web/trx-cooperation-minyak-goreng-simirah']   = 'public_government/migor/trx_cooperation_simirah';

$route['perdagangan/migor/getTrxProdusenCpoSIMIRAH']['post']    = 'public_government/migor/getTrxProdusenCpoSIMIRAH';
$route['web/trx-produsencpo-minyak-goreng-simirah']   = 'public_government/migor/trx_produsencpo_simirah';

$route['perdagangan/migor/getTrxProdusenMigorSIMIRAH']['post']    = 'public_government/migor/getTrxProdusenMigorSIMIRAH';
$route['web/trx-produsenmigor-minyak-goreng-simirah']   = 'public_government/migor/trx_produsenmigor_simirah';

$route['perdagangan/migor/getTrxD1SIMIRAH']['post']    = 'public_government/migor/getTrxD1SIMIRAH';
$route['web/trx-d1-minyak-goreng-simirah']   = 'public_government/migor/trx_d1_simirah';

$route['perdagangan/migor/getTrxD2SIMIRAH']['post']    = 'public_government/migor/getTrxD2SIMIRAH';
$route['web/trx-d2-minyak-goreng-simirah']   = 'public_government/migor/trx_d2_simirah';

$route['perdagangan/migor/getTrxPengecerSIMIRAH']['post']    = 'public_government/migor/getTrxPengecerSIMIRAH';
$route['web/trx-pengecer-minyak-goreng-simirah']   = 'public_government/migor/trx_pengecer_simirah';

$route['perdagangan/migor/getSummarySIMIRAH']['post']    = 'public_government/migor/getSummarySIMIRAH';
$route['web/summary-minyak-goreng-simirah']   = 'public_government/migor/summary_simirah';
$route['web/pengawasan-distribusi-simirah']['get']   = 'public_government/migor/pengawasan_distribusi_simirah';
$route['perdagangan/migor/laporkan-distribusi']['post']   = 'public_government/migor/laporkan_distribusi';
$route['perdagangan/migor/penerimaan-distribusi']['post']   = 'public_government/migor/penerimaan_distribusi';
$route['perdagangan/migor/getPemantauanMigor']['post']    = 'public_government/migor/getPemantauanMigor';
$route['perdagangan/migor/getPemantauanMigorDetail']['post']    = 'public_government/migor/getPemantauanMigorDetail';

$route['web/mirah-export']['post']   = 'public_government/migor/mirah_export';
$route['web/pemantauan-export']['post']   = 'public_government/migor/pemantauan_export';
$route['web/mirah-download']['get']   = 'public_government/migor/mirah_download';

$route['web/alokasidmo-export']['post']   = 'public_government/migor/alokasidmo_export';
$route['web/summary-alokasi-export']['post']   = 'public_government/migor/summary_alokasi_dmo_export';
$route['web/summary-alokasi-export/(:any)']['post']   = 'public_government/migor/summary_alokasi_dmo_export/$1';
$route['web/delete-alokasi']['post']   = 'public_government/migor/deleteAlokasi';
$route['perdagangan/migor/getSummaryAlokasiSIMIRAH']['post']    = 'public_government/migor/getSummaryAlokasiSIMIRAH';
$route['perdagangan/migor/getPeriodeAlokasiSIMIRAH']['post']    = 'public_government/migor/getPeriodeAlokasiSIMIRAH';
$route['perdagangan/migor/getAlokasiPerusahaanSIMIRAH']['post']    = 'public_government/migor/getAlokasiPerusahaanSIMIRAH';

$route['perdagangan/migor/kirim-alokasi']['post']    = 'public_government/migor/kirimAlokasi';
$route['web/alokasi-minyak-goreng-simirah']   = 'public_government/migor/alokasi_simirah';

$route['web/trx-tracedo-minyak-goreng-simirah']   = 'public_government/migor/trx_tracedo_simirah';
//-----------------------------------------------------------------------------------------------

//REVITALISASI PASAR

//Proposal
$route['web/revitalisasi/proposal']['get']   = 'public_government/revitalisasi';
$route['web/revitalisasi/proposal']['post']   = 'public_government/revitalisasi/setProposal';
$route['web/revitalisasi/proposal/(:any)']['get']   = 'public_government/revitalisasi/proposal_detail/$1';
$route['web/revitalisasi/proposal/pasar']['post']   = 'public_government/revitalisasi/getPasar';
$route['web/revitalisasi/proposal/json']['post']    = 'public_government/revitalisasi/getProposal';
$route['web/revitalisasi/proposal/dokumen/json']['post']    = 'public_government/revitalisasi/getProposalDokumen';
$route['web/revitalisasi/proposal/dokumen']['post']    = 'public_government/revitalisasi/setProposalDokumen';
$route['web/revitalisasi/proposal/dokumen/tugaskan']['post']    = 'public_government/revitalisasi/tugaskanStaf';
$route['web/revitalisasi/proposal/submit']['post']    = 'public_government/revitalisasi/submitProposal';
$route['web/revitalisasi/proposal/pengelola']['post']    = 'public_government/revitalisasi/setProposalPengelola';
$route['web/revitalisasi/proposal/pengelola/json']['post']    = 'public_government/revitalisasi/getProposalPengelola';

//Penetapan
$route['web/revitalisasi/penetapan']['get']   = 'public_government/revitalisasi/penetapan';
$route['web/revitalisasi/penetapan/(:any)']['get']   = 'public_government/revitalisasi/penetapan_detail/$1';
$route['web/revitalisasi/penetapan']['post']   = 'public_government/revitalisasi/setPenetapan';
$route['web/revitalisasi/penetapan/json']['post']   = 'public_government/revitalisasi/getPenetapan';
$route['web/revitalisasi/penetapan/pasar']['post']   = 'public_government/revitalisasi/getProposalPasar';
$route['web/revitalisasi/penetapan/proposal']['post']   = 'public_government/revitalisasi/getPenetapanProposal';
$route['web/revitalisasi/penetapan/tetapkan-proposal']['post']   = 'public_government/revitalisasi/setPenetapanProposal';


//SK Operator
$route['web/revitalisasi/proposal/sk']['post']   = 'public_government/revitalisasi/setProposal/sk';

//Lelang
$route['web/revitalisasi/lelang/(:any)']['get']   = 'public_government/revitalisasi/lelang/$1';
$route['web/revitalisasi/lelang']['post']   = 'public_government/revitalisasi/setProposalLelang';
$route['web/revitalisasi/lelang/json']['post']   = 'public_government/revitalisasi/getProposalLelang';
$route['web/revitalisasi/lelang/tahapan/json']['post']   = 'public_government/revitalisasi/getProposalLelangTahapan';

//Kontrak
$route['web/revitalisasi/kontrak/(:any)']['get']   = 'public_government/revitalisasi/kontrak/$1';
$route['web/revitalisasi/kontrak/json']['post']   = 'public_government/revitalisasi/getProposalLelangKontrak';
$route['web/revitalisasi/kontrak']['post']   = 'public_government/revitalisasi/setProposalKontrak';

//Jadwal 
$route['web/revitalisasi/jadwal/(:any)']['get']   = 'public_government/revitalisasi/jadwal/$1';
$route['web/revitalisasi/jadwal']['post']   = 'public_government/revitalisasi/setProposalJadwal';

//E-Komplain 
$route['web/e-komplain']['get']   = 'ekomplain';
$route['web/e-komplain/get']['post']   = 'ekomplain/getEkomplain';
$route['web/e-komplain/detail/(:any)']['get']   = 'ekomplain/detail/$1';
$route['web/e-komplain/save']['post']   = 'ekomplain/save_tanggapan';

//Bantuan Publik
$route['public/bantu'] = 'public/bantu';
$route['reset-password'] = 'home/reset_password';
$route['reset-password/(:any)'] = 'home/reset_password/$1';
$route['cek_reset_password'] = 'home/cek_reset_password';
$route['save_reset_password'] = 'home/save_password';

//DUMMY EMAIL
$route['bobby/email'] = 'dashboard/emailexample';
$route['bobby/email2'] = 'dashboard/emailexample2';

//Pantuan Fisik
$route['web/revitalisasi/pantauan_fisik/(:any)']['get']   = 'public_government/revitalisasi/pantauanFisik/$1';
$route['web/revitalisasi/pantauan_fisik_add/(:any)']['get']   = 'public_government/revitalisasi/pantauanFisikAdd/$1';
$route['web/revitalisasi/pantauan_fisik_add']['post']   = 'bantu/service/setPantauanFisikAdd';
$route['web/revitalisasi/pantauan_fisik']['post']   = 'bantu/service/setPantauanFisik';
$route['web/revitalisasi/pantauan_fisik_add/kurva']['post']   = 'bantu/service/setPantauanFisikAddKurva';
$route['web/revitalisasi/pantauan_fisik/kurva']['post']   = 'bantu/service/setPantauanFisikKurva';
$route['web/revitalisasi/haha']['get']   = 'bantu/service/coba';