# Sistem Informasi Sarana Pasar (SISP)

<img src="assets/brand/banner.png" width="800">

## Description

Sistem Informasi Sarana Pasar (SISP) adalah Website Tentang Sebaran Pasar Seluruh Indonesia, Harga Bahan Pokok,
Untuk Pengelola dan Dinas serta Eksekutif agar lebih dapat memantau perkembangan dan Detail Pasar beserta harganya.


## Project Information

- Developing Year       : 2022, 2023
- Programming Language  : PHP
- Framework             : Codeigniter 3

## Assets Template

- Backend               : https://elements.envato.com/morvin-admin-dashboard-template-HNUTDK2
- Frontend              : https://silicon.madrasthemes.com/

